package com.datalyxt.webscraper.model.sharecount;

public class GooglePlusStats {
	public String id = "";
	public int plusoneCount;
	public int circleByCount;
	public Result result = new Result();

	class Result {
		public String kind= "";
		public String id= "";
		public boolean isSetByViewer;
		
		public Metadata metadata = new Metadata();
		public String abtk= "";
		class Metadata {
			public String type= "";
			public GlobalCounts globalCounts = new GlobalCounts();
			class GlobalCounts {
				public float count;
			}
		}

	}
    public float getCount(){
    	return result.metadata.globalCounts.count;
    }
	

}
