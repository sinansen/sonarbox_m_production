package com.datalyxt.webscraper.model.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.datalyxt.webmodel.scan.TextContext;

public class TextContextUtil {
	public static Collection<String> getNearestLink(TextContext textContext,
			int distanceThreshold) {
		if (textContext.link.isEmpty())
			return textContext.link.keySet();
		
		List<String> links = new ArrayList<String>();
		List<Integer> distances = new ArrayList<Integer>();
		distances.addAll(textContext.link.values());
		Collections.sort(distances);
		int distance = distances.get(0);
		distance = Math.min(distance, distanceThreshold);
		for (String link : textContext.link.keySet()) {
			if (textContext.link.get(link) <= distance) {
				links.add(link);
			}
		}
		return links;
	}
}
