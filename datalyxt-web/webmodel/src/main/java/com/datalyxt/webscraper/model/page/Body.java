package com.datalyxt.webscraper.model.page;

import java.util.ArrayList;
import java.util.List;

public class Body {
	public List<String> links = new ArrayList<String>();

	private String structureSelen = null;
	private String pageTextSelen = null;
	private String htmlSelen = null;

	private int structureSelenHash = 0;
	private int pageTextSelenHash = 0;
	private int htmlSelenHash = 0;

	public String getStructureSelen() {
		return structureSelen;
	}

	public void setStructureSelen(String structureSelen) {
		this.structureSelen = structureSelen;
		this.structureSelenHash = structureSelen.hashCode();
	}

	public String getPageTextSelen() {
		return pageTextSelen;
	}

	public void setPageTextSelen(String pageTextSelen) {
		this.pageTextSelen = pageTextSelen;
		this.pageTextSelenHash = pageTextSelen.hashCode();
	}

	public String getHtmlSelen() {
		return htmlSelen;
	}

	public void setHtmlSelen(String htmlSelen) {
		this.htmlSelen = htmlSelen;
		this.htmlSelenHash = htmlSelen.hashCode();
	}

	public int getStructureSelenHash() {
		return structureSelenHash;
	}

	public void setStructureSelenHash(int structureSelenHash) {
		this.structureSelenHash = structureSelenHash;
	}

	public int getPageTextSelenHash() {
		return pageTextSelenHash;
	}

	public void setPageTextSelenHash(int pageTextSelenHash) {
		this.pageTextSelenHash = pageTextSelenHash;
	}

	public int getHtmlSelenHash() {
		return htmlSelenHash;
	}

	public void setHtmlSelenHash(int htmlSelenHash) {
		this.htmlSelenHash = htmlSelenHash;
	}
}
