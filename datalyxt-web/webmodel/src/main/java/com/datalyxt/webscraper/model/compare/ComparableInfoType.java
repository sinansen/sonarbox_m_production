package com.datalyxt.webscraper.model.compare;

public enum ComparableInfoType {
	email, phone, vat, plz, register, fax
}
