package com.datalyxt.webscraper.model.monitor;

import java.util.HashMap;

public class DomainFetchingStatistic {
	public long doStatisticAt;
	public int usedTimeout;
	public String hostId;
	public HashMap<String, Long> domainPageFetchingUsedTime = new HashMap<>();
}
