package com.datalyxt.webscraper.model;

import java.util.HashMap;

public class LinkParameter {
	public String baseParameterUrl;
	public String url;
	public String parameterExtension;
	public HashMap<String, String> parameterValue = new HashMap<String, String>();
}
