package com.datalyxt.webscraper.model.sharecount;

import com.datalyxt.webscraper.model.sharecount.FacebookStats.FacebookStatData;

public class SocialStats implements Comparable<SocialStats>{
	public String url;
	public long timestamp;
	public FacebookStatData facebookstats ;
	public TwitterStats twitterStats;
	public YoutubeStats youtubeStats;
	public GooglePlusStats googlePlusStats;
	public LinkInStats linkInStats;
	public PinterestStats pinterestStats;
	@Override
	public int compareTo(SocialStats comp) {
			long diff = comp.timestamp - timestamp;
			if (diff > 0)
				return 1;
			if (diff < 0)
				return -1;
			return 0;
		}
}
