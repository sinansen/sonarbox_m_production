package com.datalyxt.webscraper.model.link;

import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.production.globalsearch.context.ContextType;

public class Link implements Comparable<Link> {
	public HashSet<LinkType> linkType = new HashSet<LinkType>();
	public HashSet<ContextType> contextType = new HashSet<ContextType>();
	public String url;
	public String linkText = "";
	public String domain;
	public String rootURL;
	public HashMap<String, Integer> parentDepthTable = new HashMap<String, Integer>();
	public boolean isRoot = false;
	public String socialId;
	public int depth;
	public boolean isRedirected = false;
	public long parseTime;
	@Override
	public int compareTo(Link comp) {
		int diff = depth - comp.depth ;
		return diff;
	}
	public int getPriority() {
		// TODO Auto-generated method stub
		return 1;
	}
	public void setPriority(int i) {
		// TODO Auto-generated method stub
		
	}
   
}
