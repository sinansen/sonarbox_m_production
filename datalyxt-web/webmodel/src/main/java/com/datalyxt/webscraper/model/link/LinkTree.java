package com.datalyxt.webscraper.model.link;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.domain.DomainMetaInfo;
import com.datalyxt.webscraper.model.TechnologyType;
import com.datalyxt.webscraper.model.page.BlockTemp;
import com.datalyxt.webscraper.model.page.Facts;
import com.datalyxt.webscraper.model.page.Page;

public class LinkTree implements Comparable<LinkTree>, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2658016732834051675L;
	public DomainMetaInfo domainMetaInfo;
	public HashMap<Integer, HashSet<String>> linkLevelsTree = new HashMap<Integer, HashSet<String>>();
	public HashMap<String, Link> treeNodes = new HashMap<String, Link>();

	public ArrayList<String> externalLinks = new ArrayList<String>();
	public ArrayList<String> subDomains = new ArrayList<String>();
	public ArrayList<String> internalLinks = new ArrayList<String>();
	public ArrayList<String> socialLinks = new ArrayList<String>();
	public ArrayList<String> navigationLinks = new ArrayList<String>();
	public HashMap<String, Page> pages = new HashMap<String, Page>();
	public String rootlink;
	public Facts contact = new Facts();
	public HashSet<TechnologyType> techs = new HashSet<TechnologyType>();
	public HashMap<String, List<BlockTemp>> blocks = new HashMap<String, List<BlockTemp>>();
	public long createdAt;

	public int compareTo(LinkTree comp) {
		long diff = comp.createdAt - createdAt;
		if (diff > 0)
			return 1;
		if (diff == 0)
			return 0;
		return -1;
	}
}
