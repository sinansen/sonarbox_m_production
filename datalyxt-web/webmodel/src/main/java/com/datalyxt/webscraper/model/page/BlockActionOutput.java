package com.datalyxt.webscraper.model.page;


public class BlockActionOutput {
	public String pageURL;
	public String blockPath;
	public long createdAt;
	public String followedLink;
}
