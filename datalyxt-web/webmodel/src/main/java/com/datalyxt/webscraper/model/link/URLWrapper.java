package com.datalyxt.webscraper.model.link;

import java.util.Map;
import java.util.SortedMap;


public class URLWrapper {
	public String originalUrl;
	public int port;
	public int defaultPort;
	public String host;
	public String protocol;
	public String path;
	public String query;
	public SortedMap<String, String> sortedParamMap;
	
	public URLWrapper(String url, String protocol, String host, int port, int defaultPort, String path,
			String query) {
		this.originalUrl = url;
		this.protocol = protocol;
		this.host = host;
		this.port = port;
		this.defaultPort = defaultPort;
		this.path = path == null ? "" : path.trim();
		this.query =query == null ? "" : query;
	}

	public String getURL(){
		String portNr = port == -1 ? "" : port+"";
		String queryString = queryMapToString();
		if(queryString!=null && !queryString.isEmpty())
			queryString = "?"+queryString;
		String url = protocol+"://"+host+portNr+path+queryString;
		return url;
	}
	
	private String queryMapToString() {
		if (sortedParamMap == null || sortedParamMap.isEmpty()) {
			return "";
		}

		final StringBuffer sb = new StringBuffer(100);
		for (Map.Entry<String, String> pair : sortedParamMap.entrySet()) {
			if (sb.length() > 0) {
				sb.append('&');
			}
			sb.append(pair.getKey());
			if (!pair.getValue().isEmpty()) {
				sb.append('=');
				sb.append(pair.getValue());
			}
		}
		return sb.toString();
	}

}
