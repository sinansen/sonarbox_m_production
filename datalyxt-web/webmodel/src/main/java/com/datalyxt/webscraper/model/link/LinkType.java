package com.datalyxt.webscraper.model.link;

public enum LinkType {
internal, external, internalHtml, navigation, redirected, pressreport, product, subdomain, domain, sociallink, facebook, twitter, youtube, linkedin, xing, googleplus,mda_Word, mda_Excel, mda_PDF, mda_Powerpoint, mda_NA,agb,contact,imprint,team,aboutus,faq,blog,privacy,tou,news,newsletter,linkType_na, image, html, iframeSource, css, jsfile, video}
