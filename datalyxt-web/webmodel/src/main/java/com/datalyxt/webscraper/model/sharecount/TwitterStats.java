/**
 * 
 */
package com.datalyxt.webscraper.model.sharecount;

/**
 * @author Bijan Fahimi
 *
 */
public class TwitterStats {
	public int count;
	public String url;
	public int followers;
	public int tweets;
	public int favouritesCount;
	public int friendsCount;
}
