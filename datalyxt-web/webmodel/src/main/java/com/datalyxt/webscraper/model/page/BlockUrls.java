package com.datalyxt.webscraper.model.page;

public class BlockUrls implements Comparable<BlockUrls>{

	public String url;
	public int amount;
	@Override
	public int compareTo(BlockUrls cop) {
		
		return cop.amount - amount;
	}

}
