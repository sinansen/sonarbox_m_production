package com.datalyxt.webscraper.model;

public enum LogLevel {
info, warning, debug, error
}
