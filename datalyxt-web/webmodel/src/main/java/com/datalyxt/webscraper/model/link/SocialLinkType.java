package com.datalyxt.webscraper.model.link;

import java.util.Arrays;
import java.util.HashSet;

public class SocialLinkType {
	
	
public static final HashSet<LinkType> sociallinkType = new HashSet<LinkType>(Arrays.asList(LinkType.facebook, LinkType.twitter, LinkType.xing, LinkType.googleplus, LinkType.youtube, LinkType.linkedin));
}
