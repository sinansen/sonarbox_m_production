package com.datalyxt.webscraper.model.page;

public class MetaTag {
	
	private String attribute=null;
	private String value=null;
	
	public MetaTag(String attr, String value){
		setAttribute(attr);
		setValue(value);
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
