package com.datalyxt.webscraper.model.storage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.webscraper.model.TechnologyType;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.BlockTemp;
import com.datalyxt.webscraper.model.page.Facts;

public class SimpleLinkTree {
	public HashMap<Integer, HashSet<String>> linkLevelsTree = new HashMap<Integer, HashSet<String>>();
	public HashMap<String, Link> treeNodes = new HashMap<String, Link>();
	public Facts contact = new Facts();
	public HashSet<TechnologyType> techs = new HashSet<TechnologyType>();
	public HashMap<String, List<BlockTemp>> blocks = new HashMap<String, List<BlockTemp>>();
}
