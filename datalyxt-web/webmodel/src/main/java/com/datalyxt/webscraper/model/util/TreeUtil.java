package com.datalyxt.webscraper.model.util;

import com.datalyxt.webscraper.model.link.LinkTree;
import com.datalyxt.webscraper.model.storage.SimpleLinkTree;

public class TreeUtil {
	public static SimpleLinkTree getSimpleLinkTree(LinkTree tree) {
		SimpleLinkTree sim = new SimpleLinkTree();
		sim.contact = tree.contact;
		sim.linkLevelsTree.putAll(tree.linkLevelsTree);
		sim.techs.addAll(tree.techs);
		sim.treeNodes.putAll(tree.treeNodes);
		sim.blocks.putAll(tree.blocks);
		return sim;
	}
}
