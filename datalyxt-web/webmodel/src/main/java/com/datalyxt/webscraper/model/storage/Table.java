package com.datalyxt.webscraper.model.storage;

import java.util.ArrayList;
import java.util.List;

public class Table {
	public List<String> columnNames = new ArrayList<String>();
	public List<List<String>> data = new ArrayList<List<String>>();

}
