package com.datalyxt.webscraper.model.util;

import com.datalyxt.webmodel.user.TrackBlock;

public class TrackBlockUtil {
	public static String getKey(TrackBlock trackblock) {
		String key = trackblock.pageId + ":" + trackblock.blockId;
		return key;
	}
}
