package com.datalyxt.webscraper.model.monitor;

import java.io.Serializable;

import com.datalyxt.webscraper.model.link.LinkType;

public class TrackBlockProcessingState implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3665269714910337219L;

	public String pageURL;
	public String blockCssPath;
	public LinkType socialType;
	public String accountId;
	public String domainUrl;
	public String globalQuery;
	public String searchEngine;
	public Long inQueueTime;
	
	public OutQueueState outQueue ;

}
