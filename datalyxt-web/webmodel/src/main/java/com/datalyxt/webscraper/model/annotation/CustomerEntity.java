package com.datalyxt.webscraper.model.annotation;

import java.util.ArrayList;

public class CustomerEntity {
	public String entityId; //should be invisible for user
	public Header header = new Header();
	public AnnotationTag tag = new AnnotationTag();
	public ArrayList<String> matchingRuleIds=new ArrayList<String>(); //should be invisible for user
	public ArrayList<String> matchingRules=new ArrayList<String>();
}
