package com.datalyxt.webscraper.model.compare;

import java.util.HashMap;

import com.datalyxt.webscraper.model.link.LinkType;

public class DomainComparison implements Comparable<DomainComparison> {
	public String domain;
	public long datetime;
	public LinkTreeComparison linktreeComparison;
	public SocialCountComparison socialCountComparison;
	public HashMap<LinkType, SocialMsgComparison> socialMsgComparisons = new HashMap<LinkType, SocialMsgComparison>();
	public int compareTo(DomainComparison comp) {
		long diff = comp.datetime - datetime;
		if (diff > 0)
			return 1;
		if (diff == 0)
			return 0;
		return -1;
	}
}
