package com.datalyxt.webscraper.model.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.webscraper.model.TechnologyType;
import com.datalyxt.webscraper.model.link.Link;

public class Page {

	public Link pageUrl; // link url
	public String pageTitle = "";
	public String language = "";

	public PageType pageType = PageType.notchecked;

	public long visitedTime;

	public ArrayList<MetaTag> metaTags = new ArrayList<MetaTag>();
	public HashSet<String> externalLinks = new HashSet<String>();
	public HashSet<String> internalLinks = new HashSet<String>();
	public HashSet<String> socialLinks = new HashSet<String>();
	public HashSet<String> subdomains = new HashSet<String>();
	public HashMap<String, Link> images = new HashMap<String, Link>();
	public HashSet<TechnologyType> techs;
	public HashSet<String> dynamicParameters = new HashSet<>();

	public HashMap<String, Link> linkList = new HashMap<String, Link>();
	private String structureSelen = null;
	private String pageTextSelen = null;
	private String htmlSelen = null;

	private int structureSelenHash = 0;
	private int pageTextSelenHash = 0;
	private int htmlSelenHash = 0;
	public Facts facts = null;

	public void setTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public String getTitle() {
		return pageTitle;
	}

	public void setMetaTags(ArrayList<MetaTag> metaTags) {
		this.metaTags.addAll(metaTags);
	}

	public String getStructureSelen() {
		return structureSelen;
	}

	public void setStructureSelen(String structureSelen) {
		this.structureSelen = structureSelen;
		this.setStructureSelenHash(structureSelen.hashCode());
	}

	public String getPageTextSelen() {
		return pageTextSelen;
	}

	public void setPageTextSelen(String pageTextSelen) {
		this.pageTextSelen = pageTextSelen;
		this.setPageTextSelenHash(pageTextSelen.hashCode());
	}

	public String getHtmlSelen() {
		return htmlSelen;
	}

	public void setHtmlSelen(String htmlSelen) {
		this.htmlSelen = htmlSelen;
		this.setHtmlSelenHash(htmlSelen.hashCode());
	}

	public int getStructureSelenHash() {
		return structureSelenHash;
	}

	public void setStructureSelenHash(int structureSelenHash) {
		this.structureSelenHash = structureSelenHash;
	}

	public int getPageTextSelenHash() {
		return pageTextSelenHash;
	}

	public void setPageTextSelenHash(int pageTextSelenHash) {
		this.pageTextSelenHash = pageTextSelenHash;
	}

	public int getHtmlSelenHash() {
		return htmlSelenHash;
	}

	public void setHtmlSelenHash(int htmlSelenHash) {
		this.htmlSelenHash = htmlSelenHash;
	}

	public PageType getPageType() {
		return pageType;
	}

	public void setPageType(PageType pageType) {
		this.pageType = pageType;
	}

	public HashSet<String> getExternalLinks() {
		return externalLinks;
	}

	public void setExternalLinks(ArrayList<String> externalLinks) {
		this.externalLinks = new HashSet<String>(externalLinks);
	}

	public void setInternalLinks(ArrayList<String> internalLinks) {
		this.internalLinks = new HashSet<String>(internalLinks);

	}

	public void setSocialLinks(ArrayList<String> socialLinks) {
		this.socialLinks = new HashSet<String>(socialLinks);
	}

	public HashSet<String> getInternalLinks() {
		// TODO Auto-generated method stub
		return internalLinks;
	}

	public HashSet<String> getSocialLinks() {
		// TODO Auto-generated method stub
		return socialLinks;
	}
}
