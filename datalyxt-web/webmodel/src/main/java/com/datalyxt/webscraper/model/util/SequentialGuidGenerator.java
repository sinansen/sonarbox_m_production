package com.datalyxt.webscraper.model.util;
import java.security.SecureRandom;
import java.util.UUID;

public class SequentialGuidGenerator {
	
	   public final static int BYTE_OFFSET_CLOCK_LO = 0;
	    public final static int BYTE_OFFSET_CLOCK_MID = 4;
	    public final static int BYTE_OFFSET_CLOCK_HI = 6;

	    // note: clock-hi and type occupy same byte (different bits)
	    public final static int BYTE_OFFSET_TYPE = 6;

	    // similarly, clock sequence and variant are multiplexed
	    public final static int BYTE_OFFSET_CLOCK_SEQUENCE = 8;
	    public final static int BYTE_OFFSET_VARIATION = 8;
	
	public static UUID newSequentialGuid() {
		
		// 10 random bytes
		byte[] randBytes = new byte[6];
		SecureRandom rand = new SecureRandom();
		rand.nextBytes(randBytes);
		
		byte[] uuidBytes = new byte[16];
		
		System.arraycopy(randBytes, 
                0,
                uuidBytes,
                10,
                6);
		
		
		final long rawTimestamp = System.currentTimeMillis();
		int clockSeq = (int) (rawTimestamp & 0xFFFF);
        uuidBytes[BYTE_OFFSET_CLOCK_SEQUENCE] = (byte) (clockSeq >> 8);
        uuidBytes[BYTE_OFFSET_CLOCK_SEQUENCE+1] = (byte) clockSeq;
		
		long l2 = gatherLong(uuidBytes, 8);
		long _uuidL2 = initUUIDSecondLong(l2);

		// Time field components are kind of shuffled, need to slice:
		int clockHi = (int) (rawTimestamp >>> 32);
		int clockLo = (int) rawTimestamp;
		// and dice
		int midhi = (clockHi << 16) | (clockHi >>> 16);
		// need to squeeze in type (4 MSBs in byte 6, clock hi)
		midhi &= ~0xF000; // remove high nibble of 6th byte
		midhi |= 0x1000; // type 1
		long midhiL = (long) midhi;
		midhiL = ((midhiL << 32) >>> 32); // to get rid of sign extension
		// and reconstruct
		long l1 = (((long) clockLo) << 32) | midhiL;
		// last detail: must force 2 MSB to be '10'
		return new UUID(l1, _uuidL2);

	}

	public static String newSequentialGuidString() {
		return newSequentialGuid().toString()
				.replaceAll("-", "");
	}
	
	private static long initUUIDSecondLong(long l2) {
		l2 = ((l2 << 2) >>> 2); // remove 2 MSB
		l2 |= (2L << 62); // set 2 MSB to '10'
		return l2;
	}

	private final static long gatherLong(byte[] buffer, int offset) {
		long hi = ((long) _gatherInt(buffer, offset)) << 32;
		// long lo = ((long) _gatherInt(buffer, offset+4)) & MASK_LOW_INT;
		long lo = (((long) _gatherInt(buffer, offset + 4)) << 32) >>> 32;
		return hi | lo;
	}

	private final static int _gatherInt(byte[] buffer, int offset) {
		return (buffer[offset] << 24) | ((buffer[offset + 1] & 0xFF) << 16)
				| ((buffer[offset + 2] & 0xFF) << 8)
				| (buffer[offset + 3] & 0xFF);
	}
	
	

}