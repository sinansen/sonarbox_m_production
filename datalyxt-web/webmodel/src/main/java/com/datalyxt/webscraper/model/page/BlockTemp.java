package com.datalyxt.webscraper.model.page;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.webscraper.model.util.DateTimeUtil;

public class BlockTemp {
	
	public long timestamp;
	public HashSet<String> matchedKeywords = new HashSet<String>();
	private String html=null;
	private String type=null;
	private String text=null;
	private int textHash=0;
	private Date date=null;
	private String parentStruckture=null;
	private String url;
	public String cssPath;
	public HashMap<String, BlockUrls> urls = new HashMap<String , BlockUrls>();
	//private int parentStructure;
	
	public String getDateTime(){
		return DateTimeUtil.getDatetime(timestamp);
	}
	
	public String getHtml() {
		return html;
	}
	public String getParentStruckture() {
		return parentStruckture;
	}
	public void setParentStruckture(String parentStruckture) {
		this.parentStruckture = parentStruckture;
	}
	public void setHtml(String html) {
		this.html = html;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getTextHash() {
		return textHash;
	}
	public void setTextHash(int textHash) {
		this.textHash = textHash;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public void setUrl(String url) {
		this.url = url;
		
	}
	
	public String getUrl() {
		return url;
		
	}

}
