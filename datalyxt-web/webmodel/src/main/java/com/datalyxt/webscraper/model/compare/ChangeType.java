package com.datalyxt.webscraper.model.compare;

public enum ChangeType {
 added, removed, initial, updated, pagetitlechanged, languageChanged, hostnamechanged, hmlJsoupHashChanged, htmlSelenHashChanged, pageTextJsoupHashChanged, pageTextSelenHashChanged, structureJsoupChanged, structureSelenChanged, metatagchanged, linkchanged, imageLinkChanged, medialinkchanged, internalLinkChanged, externalLinkChanged, subdomainChanged, sociallinkChanged, internetproviderchanged, IpChanged, pangerankchanged 
}
