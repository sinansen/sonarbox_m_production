package com.datalyxt.webscraper.model.compare;

import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.webscraper.model.TechnologyType;
import com.datalyxt.webscraper.model.link.Link;

public class LinkTreeComparison implements Comparable<LinkTreeComparison> {
	public long datetime;
	public boolean changed = false;
	public long currentVersionTimestamp;
	public long comparedVersionTimestamp;
	public String comparedRootlink;
	public HashSet<ChangeType> changes = new HashSet<ChangeType>();
	public HashSet<String> addedLinks = new HashSet<String>();
	public HashSet<String> removedLinks = new HashSet<String>();
	public String rootlink;
	public HashMap<ComparableInfoType, HashSet<String>> addedInfo = new HashMap<ComparableInfoType, HashSet<String>>();
	public HashMap<ComparableInfoType, HashSet<String>> removedInfo = new HashMap<ComparableInfoType, HashSet<String>>();
	public HashSet<TechnologyType> removedTechs = new HashSet<TechnologyType>();
	public HashSet<TechnologyType> addedTechs = new HashSet<TechnologyType>();
	public HashMap<String, Link> changedLinks = new HashMap<String, Link>();
	public HashMap<String, PageComparison> pageComparisons = new HashMap<String, PageComparison>();

	public int compareTo(LinkTreeComparison comp) {
		long diff = comp.datetime - datetime;
		if (diff > 0)
			return 1;
		if (diff == 0)
			return 0;
		return -1;
	}

}
