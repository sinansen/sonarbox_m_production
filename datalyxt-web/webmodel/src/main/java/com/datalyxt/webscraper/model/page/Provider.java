package com.datalyxt.webscraper.model.page;

public class Provider {
	public String ip;
	public String hostname;
	public String city;
	public String region;
	public String country;
	public String loc;
	public String org;
	public String postal;
}
