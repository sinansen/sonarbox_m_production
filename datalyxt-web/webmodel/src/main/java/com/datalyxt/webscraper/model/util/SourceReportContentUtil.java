package com.datalyxt.webscraper.model.util;

import com.datalyxt.production.execution.monitoring.model.M_InProcessPage;
import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.google.gson.JsonObject;

public class SourceReportContentUtil {
	public static String generateContent(long sourceId,
			long fetchFromDBTime, M_InProcessPage page, String error) {
		JsonObject value = new JsonObject();
		value.addProperty("reportType", ReportType.SourceVisitReport.name());
		value.addProperty("fetchFromDBTime", fetchFromDBTime);
		value.addProperty("sourceId", sourceId);
		value.addProperty("domain", page.pageURL.domain);
		value.addProperty("pageNormUrl", page.pageURL.url);
		value.addProperty("error", error);
		return value.toString();
	}
}
