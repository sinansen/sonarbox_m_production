package com.datalyxt.webscraper.model.util;

import com.datalyxt.production.execution.monitoring.model.M_InProcessPage;
import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.google.gson.JsonObject;

public class RuleReportContentUtil {
	public static String generateContent(M_Rule rule, long sourceId,
			long fetchFromDBTime, M_InProcessPage page, String error) {
		JsonObject value = new JsonObject();
		value.addProperty("reportType", ReportType.RuleExecutionReport.name());
		value.addProperty("fetchFromDBTime", fetchFromDBTime);
		value.addProperty("ruleId", rule.id);
		value.addProperty("ruleName", rule.name);
		value.addProperty("sourceId", sourceId);
		value.addProperty("domain", page.pageURL.domain);
		value.addProperty("pageNormUrl", page.pageURL.url);
		value.addProperty("error", error);
		return value.toString();
	}
}
