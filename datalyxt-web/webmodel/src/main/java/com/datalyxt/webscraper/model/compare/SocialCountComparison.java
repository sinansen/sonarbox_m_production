package com.datalyxt.webscraper.model.compare;

import com.datalyxt.webscraper.model.sharecount.SocialStats;

public class SocialCountComparison implements Comparable<SocialCountComparison> {

	public long datetime;
	public String link;
	public long currentVersionTimestamp;
	public long comparedVersionTimestamp;
	public int linkinIncrease;

	public SocialStats current;
	public SocialStats last;
	public int facebookTotalCountIncrease;
	public int facebookTaklaboutMeIncrease;
	public int facebookLikesIncrease;
	public float googleplusCountIncrease;
	public int googleplusCircleByCountIncrease;
	public int googleplusPlusoneCountIncrease;
	public int twitterCountIncrease;
	public int twitterTweetsIncrease;
	public int twitterFriendsCountIncrease;
	public int twitterFollowersIncrease;
	public int twitterFavouritesCountIncrease;
	public int youtubeSubscriberCountIncrease;
	public int youtubeTotalUploadViewsIncrease;

	public int compareTo(SocialCountComparison comp) {
		long diff = comp.datetime - datetime;
		if (diff > 0)
			return 1;
		if (diff == 0)
			return 0;
		return -1;
	}
}
