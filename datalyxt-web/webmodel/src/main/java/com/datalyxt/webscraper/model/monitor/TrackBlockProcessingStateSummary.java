package com.datalyxt.webscraper.model.monitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TrackBlockProcessingStateSummary implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7449042704625340657L;
	public List<TrackBlockProcessingState> states = new ArrayList<>();
	public long submittime;
	public ProcessingType processingType;
}
