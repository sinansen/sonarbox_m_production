package com.datalyxt.webscraper.model.util;

import java.sql.SQLNonTransientConnectionException;

public class MysqlExceptionUtil {
	public static boolean isConnectionKilled(Exception e) {
		String SQLNonTransientConnectionException = SQLNonTransientConnectionException.class
				.getName();

		Throwable cause = e.getCause();
		int maxDeep = 10;
		int counter = 0;
		while (cause != null && counter < maxDeep) {
			if (SQLNonTransientConnectionException.equals(cause.getClass().getName()))
				return true;

			cause = cause.getCause();
			counter++;
		}
		return false;
	}
}
