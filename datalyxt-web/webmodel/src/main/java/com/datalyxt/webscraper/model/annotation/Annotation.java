package com.datalyxt.webscraper.model.annotation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Annotation {
public HashMap<String, Boolean> matchedRule= new HashMap<String, Boolean>();
public List<CustomerEntity> annotedEntities = new ArrayList<CustomerEntity>();
public String annotatedText = "";
}
