package com.datalyxt.webscraper.model.monitor;

import java.io.Serializable;
import java.util.List;

import com.datalyxt.webmodel.user.TrackBlock;

public class WebSearchResultStats implements Serializable {
	public TrackBlock trackblock;
	public String trackblockId;
	public long scanFinishedAt;
	public long scanStartAt;
	public boolean isSuccessful;
	public List<String> errors;
	public String workerPath;
	public ExtractionStateMonitor metric;
	public long fetchTimeFromDB;

}
