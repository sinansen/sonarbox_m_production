package com.datalyxt.webscraper.model.monitor;

import java.util.HashMap;
import java.util.List;

import com.datalyxt.webmodel.globalsearch.QueryRule;

public class GlobalSearchMonitor {

	public QueryRule queryRule;
	public long searchtime;
	public int foundResults;
	public HashMap<String, List<String>> failedMsg = new HashMap<String, List<String>>();

}
