package com.datalyxt.webscraper.model.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {
	public static String getDatetime(long timestamp) {
		Date curDate = new Date(timestamp);
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String dateToStr = format.format(curDate);
		return dateToStr;
	}
}
