package com.datalyxt.webscraper.model.monitor;

import java.util.HashMap;

public class OutQueueState {
	public long outQueueBegin;
	public long outQueueEnd;
	public boolean failedToProcess = false;
	public HashMap<Long, String> failedMessage = new HashMap<Long, String>();
	public ExtractionStateMonitor extractionStateMonitor;
	public ScanSocialMediaMonitor scanSocialMediaMonitor;
	
	public boolean pageUrlError = false;
	public boolean cssPathError = false;
	public GlobalSearchMonitor globalsearchmonitor;
	
}
