package com.datalyxt.webscraper.model.page;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.webscraper.model.link.Link;

public class CheckedContent {
	public boolean pageblockChanged = false;
	public List<Link> links = new ArrayList<>();
	public String text;

}
