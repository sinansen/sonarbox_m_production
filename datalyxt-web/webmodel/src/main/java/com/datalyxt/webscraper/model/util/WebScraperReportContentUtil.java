package com.datalyxt.webscraper.model.util;

import com.datalyxt.production.execution.monitoring.model.TraceReportMeta;
import com.google.gson.JsonObject;

public class WebScraperReportContentUtil {
	public static String generateContent(TraceReportMeta traceMeta) {
		JsonObject value = new JsonObject();
		value.addProperty("message", traceMeta.message);
		value.addProperty("scanId", traceMeta.scanId);
		value.addProperty("sourceId", traceMeta.sourceId);
		value.addProperty("ruleId", traceMeta.ruleId);
		value.addProperty("url", traceMeta.url);
		return value.toString();
	}
}
