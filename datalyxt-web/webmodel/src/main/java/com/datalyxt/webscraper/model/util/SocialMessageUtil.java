package com.datalyxt.webscraper.model.util;

import com.datalyxt.webmodel.user.SocialAccount;

public class SocialMessageUtil {
	public static String getKey(SocialAccount account) {
		String key;
		if (account.accountId.equals("0"))
			key = account.socialType + ":" + account.accountName;
		else
			key = account.socialType + ":" + account.accountId;
		return key;
	}
}
