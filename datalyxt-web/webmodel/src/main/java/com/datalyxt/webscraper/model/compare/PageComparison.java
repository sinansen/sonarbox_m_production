package com.datalyxt.webscraper.model.compare;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.BlockTemp;
import com.datalyxt.webscraper.model.page.MetaTag;

public class PageComparison implements Comparable<PageComparison> {
	public String pageUrl = "";

	public HashSet<ChangeType> changed = new HashSet<>();
	public long datetime;
	public String comparedPageUrl = "";
	public long currentVersionTimestamp;
	public long comparedVersionTimestamp;

	public HashMap<ChangeType, List<BlockTemp>> pageBlocks = new HashMap<ChangeType, List<BlockTemp>>();

	public String lastTitle = null;
	public String currentTitle = null;
	public int lastPageRank = 0;
	public int currentPageRank = 0;
	public String lastLanguage = null;
	public String currentLanguage = null;
	
	public String structureJsoupChanged = null;
	public String pageTextJsoupChanged = null;
	public String htmlJsoupChanged = null;

	public String structureSelenChanged = null;
	public String pageTextSelenChanged = null;
	public String htmlSelenChanged = null;
	
	public List<String> removedLinks = new ArrayList<String>();
	public List<String> addedLinks = new ArrayList<String>();
	
	public HashSet<String> removedImages = new HashSet<String>();
	public HashSet<String> addedImages = new HashSet<String>();
	
	public HashMap<String, Link> changedLinks = new HashMap<String, Link>();
	
	public ArrayList<MetaTag> updatedMetaTags = new ArrayList<MetaTag>();
	public ArrayList<MetaTag> removedMetaTags = new ArrayList<MetaTag>();
	public ArrayList<MetaTag> addedMetaTags = new ArrayList<MetaTag>();
	public String lastHostName = null;
	public String currentHostName = null;

	public String currenHtmlJsoup;

	public String lastHtmlJsoup;

	public String currenHtmlSelen;

	public String lastHtmlSelen;

	public String lastPageTextJsoup;

	public String currenPageTextJsoup;

	public String lastStructureSelen;

	public String currenStructureSelen;

	public String currenStructureJsoup;

	public String lastStructureJsoup;

	public String currenPageTextSelen;

	public String lastPageTextSelen;

	public String lastIp;

	public String currenIp;

	public String lastInternetProvider;

	public String currenInternetProvider;
	
	public int compareTo(PageComparison comp) {
		long diff = comp.datetime - datetime;
		if (diff > 0)
			return 1;
		if (diff == 0)
			return 0;
		return -1;
	}

}
