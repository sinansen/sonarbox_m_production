package com.datalyxt.webscraper.model.page;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.webscraper.model.link.Link;
import com.google.gson.annotations.Expose;

public class PageBlock {
	@Expose 
	public long timestamp;
	@Expose 
	public String pageUrl;
	@Expose 
	public String blockCssPath;
	@Expose 
	public String blockStructure = null;
	@Expose 
	public String blockText = null;
	@Expose 
	public String blockHtml = null;
	@Expose 
	public int blockStructureHash = 0;
	@Expose 
	public int blockTextHash = 0;
	@Expose 
	public int blockHtmlHash = 0;
	@Expose 
	public HashMap<String, Link> linkList = new HashMap<String, Link>();
	@Expose
	public Set<String> screenshotUrls = new HashSet<String>();
	@Expose
	public Set<String> blockTextUrls = new HashSet<String>();
//	public Map<String, ByteBuffer> screenshots = new HashMap<String, ByteBuffer>();
	@Expose
	public String ruleContentId;
	public long sourceId;
	public long ruleId;
	public long blockId;
}
