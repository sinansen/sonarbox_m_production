package com.datalyxt.webscraper.model.compare;

import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.webmodel.social.Message;
import com.datalyxt.webscraper.model.link.LinkType;

public class SocialMsgComparison implements Comparable<SocialMsgComparison> {

	public long datetime;
	public long currentversionTimestamp;
	public long lastVersionTimestamp;
	public String link;
	
	public LinkType socialType;
	public HashSet<String> removedMsgs = new HashSet<String>();
	public HashSet<String> addedMsgs = new HashSet<String>();
	public HashMap<String, Message> changedMsgs = new HashMap<String, Message>();

	public int compareTo(SocialMsgComparison comp) {
		long diff = comp.datetime - datetime;
		if (diff > 0)
			return 1;
		if (diff == 0)
			return 0;
		return -1;
	}
}

