package com.datalyxt.webscraper.model.util;

import java.util.List;

import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.filter.BlockData;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.KeyvalueBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.TimeBlock;

public class BlockDataConverter {
	public void convertToBlockData(Block block, List<BlockData> blockdata) {
		if (block.type.equals(BlockType.timeBlock)) {
			TimeBlock timeBlock = (TimeBlock) block;
			BlockData data = new BlockData();
			data.blockId = block.blockId;
			data.name = timeBlock.name;
			data.numValue = timeBlock.date.getTime();
			blockdata.add(data);
			return;
		}

		if (block.type.equals(BlockType.keyvalueBlock)) {
			KeyvalueBlock keyvalueBlock = (KeyvalueBlock) block;
			BlockData data = new BlockData();
			data.blockId = block.blockId;
			data.name = keyvalueBlock.name;
			data.textValue = keyvalueBlock.blockText;
			blockdata.add(data);
			return;
		}
		if (block.type.equals(BlockType.linkBlock)) {
			LinkBlock linkBlock = (LinkBlock) block;
			BlockData data = new BlockData();
			data.blockId = block.blockId;
			data.name = linkBlock.name;
			data.textValue = linkBlock.blockText;
			blockdata.add(data);
			return;
		}
		if (block.type.equals(BlockType.datasetBlock)) {
			DatasetBlock datasetBlock = (DatasetBlock) block;
			if (datasetBlock.blocks != null)
				for (Block subblock : datasetBlock.blocks) {
					subblock.blockId = block.blockId;
					convertToBlockData(subblock, blockdata);
				}
		}
	}
}
