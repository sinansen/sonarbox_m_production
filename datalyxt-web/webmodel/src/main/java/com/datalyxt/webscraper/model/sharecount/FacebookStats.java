/**
 * 
 */
package com.datalyxt.webscraper.model.sharecount;

import java.io.Serializable;
import java.util.List;

/**
 * @author Bijan Fahimi
 *
 */
public class FacebookStats implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 397866310884288327L;
	public List<FacebookStatData> data;

	
	public class FacebookStatData {

		public String id;
		public int talking_about_count;
		public String category;
		public String name;
		public int likes;
		
		public int share_count;
	    public int like_count;
	    public int comment_count;
	    public int total_count;
	    public String url;
	    public String normalized_url;
	    public String commentsbox_count;
	    public String comments_fbid;
	    public String click_count;
	    
	}

	public FacebookStatData getData() {
		if(!data.isEmpty()){
			return data.get(0);
		}
		return null;		
	}
	

}
