package com.datalyxt.webscraper.model.storage;

import com.datalyxt.webscraper.model.sharecount.FacebookStats;
import com.datalyxt.webscraper.model.sharecount.GooglePlusStats;
import com.datalyxt.webscraper.model.sharecount.LinkInStats;
import com.datalyxt.webscraper.model.sharecount.PinterestStats;
import com.datalyxt.webscraper.model.sharecount.TwitterStats;

public class ShareCount implements Comparable<ShareCount> {
	public String url;
	public long timestamp;
	public FacebookStats fbk;
	public TwitterStats twitter;
	public LinkInStats linkin;
	public GooglePlusStats googleplus;
	public PinterestStats pinterest;

	public int compareTo(ShareCount comp) {
		long diff = comp.timestamp - timestamp;
		if (diff > 0)
			return 1;
		if (diff < 0)
			return -1;
		return 0;
	}
}
