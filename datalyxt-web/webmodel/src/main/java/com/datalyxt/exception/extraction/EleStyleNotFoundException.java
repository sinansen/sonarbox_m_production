package com.datalyxt.exception.extraction;

public class EleStyleNotFoundException extends Exception {
	public EleStyleNotFoundException(String msg) {
		super(msg);
	}

	public EleStyleNotFoundException(String msg, Exception e) {
		super(msg, e);
	}

	public EleStyleNotFoundException(Exception e) {
		super(e);
	}

}
