package com.datalyxt.exception.extraction;

public class CandidateContextException extends Exception {
	public CandidateContextException(String msg) {
		super(msg);
	}

	public CandidateContextException(String msg, Exception e) {
		super(msg, e);
	}

	public CandidateContextException(Exception e) {
		super(e);
	}
}
