package com.datalyxt.exception.db;

public class DAOFactoryException extends Exception {
	public DAOFactoryException(String msg) {
		super(msg);
	}

	public DAOFactoryException(Exception e) {
		super(e);
	}

	public DAOFactoryException(String msg, Exception e) {
		super(msg, e);
	}
}
