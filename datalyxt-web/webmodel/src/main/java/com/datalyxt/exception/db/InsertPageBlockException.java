package com.datalyxt.exception.db;

public class InsertPageBlockException extends DataBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2415699550294620805L;

	public InsertPageBlockException(String message) {
		super(message);
	}
	public InsertPageBlockException(String message, Exception e) {
		super(message, e);
	}
}
