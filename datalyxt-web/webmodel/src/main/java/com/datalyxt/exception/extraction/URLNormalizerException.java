package com.datalyxt.exception.extraction;

public class URLNormalizerException extends Exception {

	public URLNormalizerException(String msg) {
		super(msg);
	}

	public URLNormalizerException(String msg, Exception e) {
		super(msg, e);
	}

	public URLNormalizerException(Exception e) {
		super(e);
	}

}
