package com.datalyxt.exception.db;

public class SearchEngineQueryDBException extends DataBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7529684857512245442L;

	public SearchEngineQueryDBException(String msg) {
		super(msg);
	}
}
