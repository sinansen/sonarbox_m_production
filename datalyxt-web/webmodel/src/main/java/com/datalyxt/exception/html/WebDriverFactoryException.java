package com.datalyxt.exception.html;

public class WebDriverFactoryException extends Exception {
	public WebDriverFactoryException(String msg, Exception e) {
		super(msg, e);
	}
}
