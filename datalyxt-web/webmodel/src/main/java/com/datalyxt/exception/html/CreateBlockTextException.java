package com.datalyxt.exception.html;


public class CreateBlockTextException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 791026469158566279L;

	public CreateBlockTextException(String message) {
		super(message);
	}

	public CreateBlockTextException(String message, Exception e) {
		super(message, e);
	}
}
