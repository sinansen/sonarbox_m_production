package com.datalyxt.exception.db;

public class SystemEventsDBException extends DataBaseException{
	public SystemEventsDBException(String msg) {
		super(msg);
	}

	public SystemEventsDBException(String msg, Exception e) {
		super(msg, e);
	}
}
