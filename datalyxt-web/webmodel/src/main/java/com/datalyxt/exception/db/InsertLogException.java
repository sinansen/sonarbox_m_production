package com.datalyxt.exception.db;

public class InsertLogException extends DataBaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1742027952099173125L;
	public InsertLogException(String message) {
		super(message);
	}
}
