package com.datalyxt.exception.extraction;

public class DecoderException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1532980059987161015L;

	public DecoderException(String msg) {
		super(msg);
	}
}
