package com.datalyxt.exception.db;

public class QueryPageBlockActionException extends DataBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1079611737527373455L;

	public QueryPageBlockActionException(String message) {
		super(message);
	}
	
	public QueryPageBlockActionException(String message, Exception e) {
		super(message, e);
	}
}
