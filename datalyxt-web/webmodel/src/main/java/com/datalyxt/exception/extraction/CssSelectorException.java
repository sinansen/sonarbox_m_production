package com.datalyxt.exception.extraction;

public class CssSelectorException extends Exception {
	public CssSelectorException(String msg) {
		super(msg);
	}

	public CssSelectorException(String msg, Exception e) {
		super(msg, e);
	}

	public CssSelectorException(Exception e) {
		super(e);
	}
}
