package com.datalyxt.exception.extraction;

public class WebDriverUnavailableException extends Exception{
	public WebDriverUnavailableException(String msg) {
		super(msg);
	}

	public WebDriverUnavailableException(Exception e) {
		super(e);
	}

	public WebDriverUnavailableException(String msg, Exception e) {
		super(msg, e);
	}
}
