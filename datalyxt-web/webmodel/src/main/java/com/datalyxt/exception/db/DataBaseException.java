package com.datalyxt.exception.db;

public class DataBaseException extends DAOFactoryException{
	public DataBaseException(String msg) {
		super(msg);
	}

	public DataBaseException(Exception e) {
		super(e);
	}

	public DataBaseException(String msg, Exception e) {
		super(msg, e);
	}
}
