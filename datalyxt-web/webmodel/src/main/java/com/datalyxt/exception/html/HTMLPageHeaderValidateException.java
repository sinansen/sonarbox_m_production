package com.datalyxt.exception.html;

public class HTMLPageHeaderValidateException extends Exception {

	public HTMLPageHeaderValidateException(String msg) {
		super(msg);
	}

	public HTMLPageHeaderValidateException(String msg, Exception e) {
		super(msg, e);
	}
}
