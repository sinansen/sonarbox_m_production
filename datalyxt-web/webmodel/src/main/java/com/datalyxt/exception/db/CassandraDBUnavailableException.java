package com.datalyxt.exception.db;

public class CassandraDBUnavailableException extends Exception{
	public CassandraDBUnavailableException(String msg) {
		super(msg);
	}

	public CassandraDBUnavailableException(Exception e) {
		super(e);
	}

	public CassandraDBUnavailableException(String msg, Exception e) {
		super(msg, e);
	}
}
