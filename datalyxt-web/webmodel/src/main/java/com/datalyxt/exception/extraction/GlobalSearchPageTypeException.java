package com.datalyxt.exception.extraction;

public class GlobalSearchPageTypeException extends Exception {
	public GlobalSearchPageTypeException(String msg) {
		super(msg);
	}

	public GlobalSearchPageTypeException(String msg, Exception e) {
		super(msg, e);
	}

	public GlobalSearchPageTypeException(Exception e) {
		super(e);
	}
}
