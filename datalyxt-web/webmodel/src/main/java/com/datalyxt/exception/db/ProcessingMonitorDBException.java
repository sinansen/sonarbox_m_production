package com.datalyxt.exception.db;

public class ProcessingMonitorDBException extends DataBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8932345306049612572L;

	public ProcessingMonitorDBException(String msg) {
		super(msg);
	}
	public ProcessingMonitorDBException(String msg, Exception e) {
		super(msg, e);
	}
}
