package com.datalyxt.exception.extraction;

public class PageFetchException extends Exception{
	
	public PageFetchException(String msg){
		super(msg);
	}
	
	public PageFetchException(Exception e){
		super(e);
	}
	public PageFetchException(String msg, Exception e){
		super(msg, e);
	}

}
