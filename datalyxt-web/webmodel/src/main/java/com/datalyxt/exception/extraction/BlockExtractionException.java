package com.datalyxt.exception.extraction;

public class BlockExtractionException extends Exception {
	public BlockExtractionException(String msg) {
		super(msg);
	}

	public BlockExtractionException(String msg, Exception e) {
		super(msg, e);
	}

	public BlockExtractionException(Exception e) {
		super(e);
	}
}
