package com.datalyxt.exception.db;

public class MongoDBUnavailbleException extends DataBaseException{

	public MongoDBUnavailbleException(Exception e) {
		super(e);
	}
	public MongoDBUnavailbleException(String msg, Exception e) {
		super(msg, e);
	}
	public MongoDBUnavailbleException(String msg) {
		super(msg);
	}

}
