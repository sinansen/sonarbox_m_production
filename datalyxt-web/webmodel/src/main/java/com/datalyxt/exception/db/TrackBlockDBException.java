package com.datalyxt.exception.db;

public class TrackBlockDBException extends DataBaseException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8873482370582610384L;
	public TrackBlockDBException(String message) {
		super(message);
	}
	public TrackBlockDBException(String message, Exception e) {
		super(message, e);
	}
	public TrackBlockDBException(Exception e) {
		super(e);
	}
}
