package com.datalyxt.exception.extraction;

public class CreateURLWrapperException extends Exception {
	public CreateURLWrapperException(Exception e) {
		super(e);
	}
}
