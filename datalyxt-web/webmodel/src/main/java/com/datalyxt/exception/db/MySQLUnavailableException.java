package com.datalyxt.exception.db;

public class MySQLUnavailableException extends Exception{
	public MySQLUnavailableException(String msg) {
		super(msg);
	}

	public MySQLUnavailableException(Exception e) {
		super(e);
	}

	public MySQLUnavailableException(String msg, Exception e) {
		super(msg, e);
	}

}
