package com.datalyxt.exception.extraction;

/**
 * Created by Avi Hayun on 12/8/2014.
 *
 * Thrown when there is a problem with the parsing of the content - this is a
 * tagging exception
 */
public class PageParseException extends Exception {

	public PageParseException(String msg) {
		super(msg);
	}

	public PageParseException(String msg, Exception e) {
		super(msg, e);
	}

	public PageParseException(Exception e) {
		super(e);
	}
}