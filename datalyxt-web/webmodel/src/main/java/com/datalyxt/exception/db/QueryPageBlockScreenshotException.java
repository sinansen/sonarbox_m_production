package com.datalyxt.exception.db;

public class QueryPageBlockScreenshotException extends DataBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6350043968229839242L;

	public QueryPageBlockScreenshotException(String message) {
		super(message);
	}
	public QueryPageBlockScreenshotException(String message, Exception e) {
		super(message, e);
	}
}
