package com.datalyxt.exception.db;

public class PageLinkDBException extends DataBaseException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8034699230320141396L;

	public PageLinkDBException(String msg){
		super(msg);
	}

}
