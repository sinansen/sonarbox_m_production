package com.datalyxt.exception.db;

public class QueryLastPageBlockException extends DataBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2415699550294620805L;

	public QueryLastPageBlockException(String message) {
		super(message);
	}
	
	public QueryLastPageBlockException(String message, Exception e) {
		super(message, e);
	}
}
