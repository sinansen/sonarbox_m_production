package com.datalyxt.exception.html;

public class CreateScreenshotException extends Exception {

	public CreateScreenshotException(String message) {
		super(message);
	}
	
	public CreateScreenshotException(Exception e) {
		super(e);
	}
	public CreateScreenshotException(String message, Exception e) {
		super(message, e);
	}
}
