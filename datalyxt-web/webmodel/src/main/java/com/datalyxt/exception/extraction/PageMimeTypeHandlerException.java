package com.datalyxt.exception.extraction;

public class PageMimeTypeHandlerException extends Exception{
	public PageMimeTypeHandlerException(String msg) {
		super(msg);
	}

	public PageMimeTypeHandlerException(String msg, Exception e) {
		super(msg, e);
	}

	public PageMimeTypeHandlerException(Exception e) {
		super(e);
	}
}
