package com.datalyxt.exception.db;

public class UserStartUrlDBException extends DataBaseException {

	public UserStartUrlDBException(Exception e) {
		super(e);
	}
	public UserStartUrlDBException(String msg, Exception e) {
		super(msg, e);
	}
	public UserStartUrlDBException(String msg) {
		super(msg);
	}

}
