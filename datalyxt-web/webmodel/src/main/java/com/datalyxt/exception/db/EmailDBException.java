package com.datalyxt.exception.db;


public class EmailDBException extends DataBaseException{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7347687578726511671L;

	public EmailDBException(String msg) {
		super(msg);
	}

	public EmailDBException(String msg, Exception e) {
		super(msg, e);
	}
}
