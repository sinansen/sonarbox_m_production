package com.datalyxt.exception.html;

public class TargetElementNotFoundException extends Exception {
	public TargetElementNotFoundException(String msg) {
		super(msg);
	}

	public TargetElementNotFoundException(String msg, Exception e) {
		super(msg);
	}

	public TargetElementNotFoundException(Exception e) {
		super(e);
	}
}
