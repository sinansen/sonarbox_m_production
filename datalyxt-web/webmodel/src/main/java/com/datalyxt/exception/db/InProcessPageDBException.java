package com.datalyxt.exception.db;

/**
 * Created by Avi Hayun on 12/8/2014.
 *
 * Thrown when there is a problem with the content fetching - this is a tagging exception
 */
public class InProcessPageDBException extends Exception {

	public InProcessPageDBException(String msg, Exception e) {
		super(msg, e);
	}
	public InProcessPageDBException(String msg) {
		super(msg);
	}
}