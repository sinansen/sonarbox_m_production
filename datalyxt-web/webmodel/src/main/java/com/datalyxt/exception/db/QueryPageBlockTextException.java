package com.datalyxt.exception.db;

public class QueryPageBlockTextException extends DataBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7414039700094381614L;

	public QueryPageBlockTextException(String message) {
		super(message);
	}
	public QueryPageBlockTextException(String message, Exception e) {
		super(message, e);
	}
}
