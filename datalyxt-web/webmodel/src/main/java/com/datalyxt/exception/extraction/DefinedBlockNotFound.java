package com.datalyxt.exception.extraction;

public class DefinedBlockNotFound extends Exception {

	public DefinedBlockNotFound (String msg){
		super(msg);
	}

}
