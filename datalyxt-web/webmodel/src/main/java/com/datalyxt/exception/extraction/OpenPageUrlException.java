package com.datalyxt.exception.extraction;

public class OpenPageUrlException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -775098032537035281L;

	public OpenPageUrlException(String msg) {
		super(msg);
	}
	public OpenPageUrlException(String msg, Exception e) {
		super(msg, e);
	}
}
