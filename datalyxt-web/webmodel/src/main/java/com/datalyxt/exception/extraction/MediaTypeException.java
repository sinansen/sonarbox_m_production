package com.datalyxt.exception.extraction;

public class MediaTypeException extends Exception {
	public MediaTypeException(String msg) {
		super(msg);
	}

	public MediaTypeException(String msg, Exception e) {
		super(msg, e);
	}

	public MediaTypeException(Exception e) {
		super(e);
	}
}
