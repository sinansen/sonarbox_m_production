package com.datalyxt.exception.extraction;

public class ContentSeenExcpetion extends Exception {
	public ContentSeenExcpetion(String msg) {
		super(msg);
	}

	public ContentSeenExcpetion(Exception e) {
		super(e);
	}

	public ContentSeenExcpetion(String msg, Exception e) {
		super(msg, e);
	}
}
