package com.datalyxt.exception.extraction;

public class PageHeaderFetchException extends Exception {

	public PageHeaderFetchException(String msg) {
		super(msg);
	}

	public PageHeaderFetchException(Exception e) {
		super(e);
	}

	public PageHeaderFetchException(String msg, Exception e) {
		super(msg, e);
	}

}
