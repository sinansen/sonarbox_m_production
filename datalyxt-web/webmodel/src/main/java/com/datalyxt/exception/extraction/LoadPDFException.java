package com.datalyxt.exception.extraction;

public class LoadPDFException extends Exception {
	public LoadPDFException(String msg) {
		super(msg);
	}

	public LoadPDFException(String msg, Exception e) {
		super(msg, e);
	}

	public LoadPDFException(Exception e) {
		super(e);
	}

}
