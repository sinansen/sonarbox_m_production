package com.datalyxt.exception.db;

public class SocialMessageDBException extends DataBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5282091896677576785L;

	public SocialMessageDBException(String msg) {
		super(msg);
	}
	public SocialMessageDBException(Exception e) {
		super(e);
	}
	public SocialMessageDBException(String msg, Exception e) {
		super(msg,e);
	}
}
