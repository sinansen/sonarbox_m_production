package com.datalyxt.exception;

public class GoogleWorkerException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -964860826237398574L;

	public GoogleWorkerException(String msg){
		super(msg);
	}

}
