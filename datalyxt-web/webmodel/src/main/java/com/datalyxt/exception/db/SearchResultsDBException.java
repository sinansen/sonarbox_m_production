package com.datalyxt.exception.db;

public class SearchResultsDBException extends DataBaseException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5424791509634243597L;

	public SearchResultsDBException(String msg){
		super(msg);
	}
	public SearchResultsDBException(Exception e){
		super(e);
	}
	public SearchResultsDBException(String msg, Exception e){
		super(msg, e);
	}

}
