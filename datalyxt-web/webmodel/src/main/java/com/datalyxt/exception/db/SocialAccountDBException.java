package com.datalyxt.exception.db;

public class SocialAccountDBException extends DataBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 540067869089623653L;

	public SocialAccountDBException(String msg) {
		super(msg);
	}

	public SocialAccountDBException(String message, Exception e) {
		super(message, e);
	}
}
