package com.datalyxt.exception.extraction;

public class StemmingException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3492023129192133492L;

	public StemmingException(String msg) {
		super(msg);
	}
}
