package com.datalyxt.production.webscraper.model.config;

public enum ActionCondition {
	link_url_change, dataset_change , block_text_change
}
