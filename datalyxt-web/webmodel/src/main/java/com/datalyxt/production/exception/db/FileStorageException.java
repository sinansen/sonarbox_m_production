package com.datalyxt.production.exception.db;

public class FileStorageException extends Exception {

	public FileStorageException(String msg, Exception e) {
		super(msg, e);
	}

	public FileStorageException(String msg) {
		super(msg);
	}
}
