package com.datalyxt.production.exception.rule;

public class NullBlockLocationException extends Exception {
	public NullBlockLocationException(String msg) {
		super(msg);
	}

}
