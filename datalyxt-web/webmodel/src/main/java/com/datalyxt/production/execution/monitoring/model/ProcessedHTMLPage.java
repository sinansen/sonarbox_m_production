package com.datalyxt.production.execution.monitoring.model;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.globalsearch.page.HTMLPage;

public class ProcessedHTMLPage {
	public HTMLPage htmlPage;
	public List<Exception> exceptions = new ArrayList<Exception>();
}
