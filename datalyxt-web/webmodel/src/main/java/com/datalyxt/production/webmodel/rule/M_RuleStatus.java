package com.datalyxt.production.webmodel.rule;

public enum M_RuleStatus {
	added, validated, activated, paused, deleted, error
}
