package com.datalyxt.production.exception.db;

public class M_ReceiverGroupDBException extends Exception{
public M_ReceiverGroupDBException(String msg){
	super(msg);
}

public M_ReceiverGroupDBException(String msg, Exception e){
	super(msg, e);
}
}
