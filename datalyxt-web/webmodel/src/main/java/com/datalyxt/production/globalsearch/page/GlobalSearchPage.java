package com.datalyxt.production.globalsearch.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.datalyxt.production.globalsearch.context.GlobalSearchContext;
import com.datalyxt.webscraper.model.link.LinkType;

public abstract class GlobalSearchPage {
	public String link;
	public LinkType mimeType;
	public String language;
	public HashMap<String, HashMap<Integer, String>> matchingPosition=new HashMap<String,HashMap<Integer,String>>();
	public HashMap<String, HashMap<Integer, Double>> keyWordDistribution = new HashMap<String, HashMap<Integer, Double>>();
	public boolean isIFramePage = false;
	public Set<String> iFrameEmbeddedInPage;
	//used by Json
	public String type;
	public HashMap<String,HashSet<String>> kw2ConID = new HashMap<String,HashSet<String>>();
	public HashMap<String,GlobalSearchContext> conID2Con = new HashMap<String,GlobalSearchContext>();
	public ArrayList<GlobalSearchContext> pageContext;
}
