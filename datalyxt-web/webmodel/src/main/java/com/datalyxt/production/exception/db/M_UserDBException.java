package com.datalyxt.production.exception.db;

public class M_UserDBException extends Exception {
	public M_UserDBException(String msg, Exception e) {
		super(msg, e);
	}
}
