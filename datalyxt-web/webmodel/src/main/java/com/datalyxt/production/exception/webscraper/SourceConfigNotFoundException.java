package com.datalyxt.production.exception.webscraper;

public class SourceConfigNotFoundException extends Exception {
	public SourceConfigNotFoundException(String msg, Exception e) {
		super(msg, e);
	}

	public SourceConfigNotFoundException(String msg) {
		super(msg);
	}
}
