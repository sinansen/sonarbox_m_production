package com.datalyxt.production.webmodel.rule;

import java.io.Serializable;
import java.util.HashSet;

public class M_Rule implements Serializable {
	public long id;
	public long ownerId;
	public HashSet<Long> sourceIds = new HashSet<Long>();
	public M_RuleStatus status;
	public String name;
	public M_RuleContent ruleContent;

	public M_RuleType ruleType;
	public long createdAt;
	public String feedback;
	public long lastModified;
}
