package com.datalyxt.production.webscraper.model.runtime;

public class PageChange {
	public long id;
	public String nUrlMd5;
	public String message;
	public int confirmed;
	public long timestamp;

}
