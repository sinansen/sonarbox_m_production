package com.datalyxt.production.log;

public class M_Activity {
	public long id;
	public M_ActivityType type;
	public long userId;
	public long objectId;
	public M_ObjectType objectType;
	public String content;
	public long timestamp;
	public String userName;

	public M_Activity(M_ActivityType type, long objectId,
			M_ObjectType objectType) {
		this.type = type;
		this.objectId = objectId;
		this.objectType = objectType;
		this.timestamp = System.currentTimeMillis();
	}
}
