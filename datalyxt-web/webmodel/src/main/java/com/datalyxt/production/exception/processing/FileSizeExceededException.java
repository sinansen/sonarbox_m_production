package com.datalyxt.production.exception.processing;

public class FileSizeExceededException extends Exception {
	public FileSizeExceededException(String msg) {
		super(msg);
	}
}
