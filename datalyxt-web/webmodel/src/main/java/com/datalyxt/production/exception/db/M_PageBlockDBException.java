package com.datalyxt.production.exception.db;

public class M_PageBlockDBException  extends Exception {
	public M_PageBlockDBException(String msg) {
		super(msg);
	}

	public M_PageBlockDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_PageBlockDBException(Exception e) {
		super(e);
	}

}
