package com.datalyxt.production.exception.db;

public class M_ActivityDBException extends Exception {

	public M_ActivityDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_ActivityDBException(String msg) {
		super(msg);
	}

}
