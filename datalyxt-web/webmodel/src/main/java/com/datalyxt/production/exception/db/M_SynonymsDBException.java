package com.datalyxt.production.exception.db;

public class M_SynonymsDBException  extends Exception {
	public M_SynonymsDBException(String msg) {
		super(msg);
	}

	public M_SynonymsDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_SynonymsDBException(Exception e) {
		super(e);
	}
}
