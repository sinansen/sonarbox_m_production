package com.datalyxt.production.webscraper.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.BlockMeta;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;

public class BlockRuntime {

	public Block block;
	public BlockMeta blockMeta;
	public HashMap<String, LinkBlock> newLinks;
	public List<ActionResultText> actionResultTexts = new ArrayList<>();
}
