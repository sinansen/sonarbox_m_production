package com.datalyxt.production.exception.db;

public class M_GlobalSearchPageDBException  extends Exception {
	public M_GlobalSearchPageDBException(String msg) {
		super(msg);
	}

	public M_GlobalSearchPageDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_GlobalSearchPageDBException(Exception e) {
		super(e);
	}

}
