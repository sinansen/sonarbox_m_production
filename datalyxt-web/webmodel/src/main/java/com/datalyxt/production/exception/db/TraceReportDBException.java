package com.datalyxt.production.exception.db;

public class TraceReportDBException extends Exception {
	public TraceReportDBException(String msg) {
		super(msg);
	}

	public TraceReportDBException(String msg, Exception e) {
		super(msg, e);
	}

	public TraceReportDBException(Exception e) {
		super(e);
	}

}
