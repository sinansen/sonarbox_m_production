package com.datalyxt.production.exception.processing;

public class HTMLPageProcessingException extends Exception {
	public HTMLPageProcessingException(String msg) {
		super(msg);
	}

	public HTMLPageProcessingException(String msg, Exception e) {
		super(msg, e);
	}

	public HTMLPageProcessingException(Exception e) {
		super(e);
	}

}
