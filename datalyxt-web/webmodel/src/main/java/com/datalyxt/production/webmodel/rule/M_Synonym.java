package com.datalyxt.production.webmodel.rule;

import java.util.HashSet;

public class M_Synonym {
	public String indexword;
	public String indexword_stem;
	public HashSet<String> synonyms;
	public HashSet<String> synonym_stems;
}
