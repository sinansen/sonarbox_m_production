package com.datalyxt.production.receiver;

import java.util.HashSet;

public class M_Receiver {
	public long id;
	public String address;
	public String firstName;
	public String lastName;
	public String email;
	public M_ReceiverStatus status;
	public String company;
	public long createdAt;
	public long lastModified;
	public long ownerId;
	public HashSet<Long> groups = new HashSet<>() ;
	public String function;
	public String department;
	public String category;
}
