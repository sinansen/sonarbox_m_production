package com.datalyxt.production.globalsearch.page;

import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.production.block.model.EleStyle;
import com.datalyxt.production.globalsearch.context.ContextType;

public class HTMLPage extends GlobalSearchPage{
	public double minFontSize;
	public double maxFontSize;
	public double avgFontSize;
	boolean pageStats;
	public String pageTitle;
	public String pageSource;
	public HashMap<String, EleStyle> elestyls = new HashMap<String, EleStyle>();
	public HashMap<String, HashSet<ContextType>> notUsedMatchingLinks = new HashMap<>();
	
	public HTMLPage(){
		//used by Json
		type = GlobalSearchPageClassType.html.name();
	}
}
