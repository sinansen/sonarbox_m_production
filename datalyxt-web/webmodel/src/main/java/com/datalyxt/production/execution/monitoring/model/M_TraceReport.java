package com.datalyxt.production.execution.monitoring.model;

public class M_TraceReport {
	private String supervisor;
	public long time;
	public ReportType type;
	public String message = "";
	private long scanId;
	public long ruleId;
	public long sourceId;
	public String url = "";

	public String error = "";

	public M_TraceReport(String supervisor, long scanId) {
		this.supervisor = supervisor;
		this.scanId = scanId;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public long getScanId() {
		return scanId;
	}
}
