/**
 * 
 */
package com.datalyxt.production.webmodel.email;

import java.util.HashSet;

public class Email {

	public long id;
	public long created;
	public long lastUpdateDate;

	public String subject;
	public HashSet<Long> messageIds = new HashSet<Long>();
	public HashSet<Long> attachementFileIds = new HashSet<Long>();
}
