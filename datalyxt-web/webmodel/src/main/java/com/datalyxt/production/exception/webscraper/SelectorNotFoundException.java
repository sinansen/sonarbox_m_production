package com.datalyxt.production.exception.webscraper;

public class SelectorNotFoundException extends Exception {
	public SelectorNotFoundException(String msg) {
		super(msg);
	}

}
