package com.datalyxt.production.user;

public class M_User {
	public M_User(String uemail, String upwd) {
		eMail = uemail;
		password = upwd;
	}
	public M_User() {
	}
	public long userId;
	public String firstName = "";
	public String lastName = "";
	public String eMail = "";
	public String password = "";
	public boolean status = false;
	public long createdAt;
	public long lastModified;
}
