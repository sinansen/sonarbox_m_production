package com.datalyxt.production.exception.webscraper;

public class UnsupportedBlockLocationException extends Exception {

	public UnsupportedBlockLocationException(String msg) {
		super(msg);
	}
}
