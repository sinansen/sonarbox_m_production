package com.datalyxt.production.webscraper.model;

import com.datalyxt.webscraper.model.link.Link;

public class M_ProcessedWebScraperPage {
	public long fetchUsedTime;
	public long sourceId;
	public String error;
	public Link pageURL;
	public long ruleId;
	public long visitedAt;
	public long fetchFromDBTime;

}
