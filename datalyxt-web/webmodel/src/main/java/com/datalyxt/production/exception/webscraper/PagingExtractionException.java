package com.datalyxt.production.exception.webscraper;

public class PagingExtractionException extends Exception {
	public PagingExtractionException(String msg) {
		super(msg);
	}

	public PagingExtractionException(String msg, Exception e) {
		super(msg);
	}
}
