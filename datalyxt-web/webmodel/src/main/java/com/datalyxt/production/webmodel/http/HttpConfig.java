package com.datalyxt.production.webmodel.http;

public class HttpConfig {
	public String proxyHost;
	public boolean proxyEnabled = false;
	public int proxyPort;
	public int connectionTimeout = 20000;
}
