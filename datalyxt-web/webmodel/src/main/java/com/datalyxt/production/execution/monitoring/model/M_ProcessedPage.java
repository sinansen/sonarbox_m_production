package com.datalyxt.production.execution.monitoring.model;

import java.io.Serializable;

import com.datalyxt.webscraper.model.link.Link;

public class M_ProcessedPage implements Serializable{

	public long fetchUsedTime;
	public long sourceId;
	public String error;
	public Link pageURL;
	public long visitedAt;
	public long fetchFromDBTime;

}
