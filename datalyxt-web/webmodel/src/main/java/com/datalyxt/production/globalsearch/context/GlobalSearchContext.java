package com.datalyxt.production.globalsearch.context;


public abstract class GlobalSearchContext {

	public String ruleId;
	public String contextID;
	public String keyWord;
	
	//used by Json
	String type;
}
