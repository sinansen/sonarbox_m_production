package com.datalyxt.production.execution.monitoring.model;

public enum RuleExecutionStatus {
execution_started, execution_resumed, execution_finished, source_scan_finished
}
