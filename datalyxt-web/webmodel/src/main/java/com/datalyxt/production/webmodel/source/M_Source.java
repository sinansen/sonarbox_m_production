package com.datalyxt.production.webmodel.source;

import java.util.HashSet;

public class M_Source {
public long sourceId;
public String sourceName;
public String sourceUrl;
public String sourceUrlNormalized;
public M_SourceStatus sourceStatus = M_SourceStatus.added;
public long sourceCreatedAt;
public String feedback = "not validated";
public SourceConfig sourceConfig =new SourceConfig() ;
public String sourceDomain;
public String sourceNUrlMd5;
public HashSet<M_SourceType> sourceTypes = new HashSet<M_SourceType>();
public long sourceLastModified;
}
