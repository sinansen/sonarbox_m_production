package com.datalyxt.production.webmodel.source;

import java.util.Arrays;
import java.util.HashSet;

public class QT1V1SourceConfig {
	public HashSet<String> dynamicParameters = new HashSet<>();
	public HashSet<LanguageISO_639_1> languages = new HashSet<>(Arrays.asList(LanguageISO_639_1.de, LanguageISO_639_1.en));
	public int timesOfLastFetchUsedTime = 5;
	public int maxHTMLSize = 10000000;
	public int maxPDFSize = 10000000;
	public int maxBrowerRetry = 3;
	public int maxHeaderFetchRetry = 3;
	public boolean enableUseLastFetchUsedTime = true;
	public int pageLoadTimeout = 5000;
	public int maxRetry = 3;
}
