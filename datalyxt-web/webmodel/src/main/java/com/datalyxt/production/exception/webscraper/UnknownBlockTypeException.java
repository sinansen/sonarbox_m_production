package com.datalyxt.production.exception.webscraper;

public class UnknownBlockTypeException extends Exception {

	public UnknownBlockTypeException(String msg) {
		super(msg);
	}
}
