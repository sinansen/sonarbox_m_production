package com.datalyxt.production.webscraper.model.runtime;

public class PageMeta {
	public long id;
	public String nUrlMd5;
	public String domTree;
	public String html;
	public long cleanedDomHash;
	public long timestamp;
	public long scanId;
	public String nUrl;
}
