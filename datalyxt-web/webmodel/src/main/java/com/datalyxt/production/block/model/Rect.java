package com.datalyxt.production.block.model;

public class Rect {
	public Rect(int top, int bottom, int left, int right){
		this.top=top;
		this.bottom=bottom;
		this.left=left;
		this.right=right;
	}
	public Rect(){}
	public float top;
	public float bottom;
	public float left;
	public float right;
}
