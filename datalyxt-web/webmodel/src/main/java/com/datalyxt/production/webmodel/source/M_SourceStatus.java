package com.datalyxt.production.webmodel.source;

public enum M_SourceStatus {
	added, activated, paused, deleted ,error, validated, blackList
}
