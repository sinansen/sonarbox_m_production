package com.datalyxt.production.webscraper.model.runtime;

import com.datalyxt.webscraper.model.link.Link;

public class LinkBlock extends ContentBlock{
	public Link link;
	public String text;
	public boolean follow = true;
}
