package com.datalyxt.production.webscraper.model.action;

public class ActionResultFile {
	public long id;
	public String[] path;
	public String name;
	public String host;
	public long resultId;
	public int status;
	public int size;
	public int type;
	public long createdAt;
	public byte[] content;
	public String dName;
	public String url;
}
