package com.datalyxt.production.execution.monitoring.model;

public class HostHeapEntry implements Comparable<HostHeapEntry> {
	public long lastVisitedAt;
	public String hostId;
	public long politeVisitDelay;
	public long sourceId;

	public long getEarliestContactTime() {
	   long	earliestContactTime = lastVisitedAt + politeVisitDelay;
	   return earliestContactTime;
	}
	

	@Override
	public int compareTo(HostHeapEntry entry) {
		long diff = getEarliestContactTime() - entry.getEarliestContactTime();
		if (diff > 0)
			return 1;
		if (diff < 0)
			return -1;
		return 0;
	}
}
