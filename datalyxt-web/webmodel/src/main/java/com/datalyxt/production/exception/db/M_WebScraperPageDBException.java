package com.datalyxt.production.exception.db;

public class M_WebScraperPageDBException extends Exception {
	public M_WebScraperPageDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_WebScraperPageDBException(String msg) {
		super(msg);
	}

}
