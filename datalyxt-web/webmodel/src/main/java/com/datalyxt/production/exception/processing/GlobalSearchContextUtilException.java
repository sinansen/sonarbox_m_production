package com.datalyxt.production.exception.processing;

public class GlobalSearchContextUtilException extends Exception {
	public GlobalSearchContextUtilException(String msg) {
		super(msg);
	}

	public GlobalSearchContextUtilException(String msg, Exception e) {
		super(msg, e);
	}

	public GlobalSearchContextUtilException(Exception e) {
		super(e);
	}
}
