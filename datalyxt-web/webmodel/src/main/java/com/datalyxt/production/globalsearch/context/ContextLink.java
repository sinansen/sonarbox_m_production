package com.datalyxt.production.globalsearch.context;

import java.util.ArrayList;

import com.datalyxt.production.block.model.Rect;

public class ContextLink {
	public int distance=0;
	public String link;
	public String linkText;
	public ArrayList<String> textLines = new ArrayList<String>();
	public ArrayList<String> normalizedTextLines = new ArrayList<String>();
	public ATagType linkType;
	public Rect rect;
}
