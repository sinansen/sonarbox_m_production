package com.datalyxt.production.exception.webscraper;

public class BlockConfigValidationException extends Exception {
	public BlockConfigValidationException(String msg) {
		super(msg);
	}

	public BlockConfigValidationException(String msg, Exception e) {
		super(msg, e);
	}
}
