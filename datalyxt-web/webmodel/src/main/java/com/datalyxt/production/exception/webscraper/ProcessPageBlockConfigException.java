package com.datalyxt.production.exception.webscraper;

public class ProcessPageBlockConfigException extends Exception {
	public ProcessPageBlockConfigException(String msg) {
		super(msg);
	}

	public ProcessPageBlockConfigException(String msg, Exception e) {
		super(msg, e);
	}
}
