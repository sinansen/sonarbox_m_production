package com.datalyxt.production.webscraper.model.config;

public class DatasetFilter {
	public String name;
	public String operator;
	public String value;
	public FilterValueType type = FilterValueType.text;
}
