package com.datalyxt.production.exception.db;

public class M_QT1V1ResultDBException extends Exception {
	public M_QT1V1ResultDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_QT1V1ResultDBException(String msg) {
		super(msg);
	}

}
