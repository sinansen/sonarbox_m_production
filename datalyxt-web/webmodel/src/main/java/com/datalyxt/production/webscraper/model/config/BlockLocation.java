package com.datalyxt.production.webscraper.model.config;

public class BlockLocation {
	public BlockLocationType type = BlockLocationType.cssSelector;
	public String value;
	public String valueWithID;
}
