package com.datalyxt.production.execution.monitoring.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.globalsearch.page.GlobalSearchPage;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.datalyxt.webscraper.model.link.Link;

public class M_InProcessPage implements Serializable{
	public Link pageURL;
	public PageContentProcessStatus status;
	public HashMap<String, Link> pagelinks = new HashMap<String, Link>();
	public HashSet<String> contextpagelinks = new HashSet<String>();
	public long fetchUsedTime;
	public long visitedAt;
	public long extractedAt;
	public long sourceId;
	public SourceConfig sourceConfig;
	public long fetchFromDBTime;
	public GlobalSearchPage globalSearchPage;
	public Set<String> usedKeywords;
	public int crawlerHops;
	public int contextHops;
	public String domain;
	public int fetchHeaderRetry;
	public int fetchPageRetry;
	public String sourceDomain;
	public long processstarttime;
	public String linkId;
	public boolean isContextPage = false;
	public boolean fromContextPage = false;
}
