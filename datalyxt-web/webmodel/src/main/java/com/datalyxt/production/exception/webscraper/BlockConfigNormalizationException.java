package com.datalyxt.production.exception.webscraper;

public class BlockConfigNormalizationException extends Exception {
	public BlockConfigNormalizationException(String msg) {
		super(msg);
	}

	public BlockConfigNormalizationException(String msg, Exception e) {
		super(msg, e);
	}
}
