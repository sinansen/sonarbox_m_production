package com.datalyxt.production.webscraper.model.runtime;

public abstract class ContentBlock extends Block {
	@Invisible
	public String locationValue;
	@Invisible
	public String blockHtml;
	@Invisible
	public int blockStructureHash;

	@Invisible
	public int blockHtmlHash;
	@Invisible
	public String blockStructure;
	public String name;
	public int column;
}
