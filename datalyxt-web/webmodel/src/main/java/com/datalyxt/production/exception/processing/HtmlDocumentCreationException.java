package com.datalyxt.production.exception.processing;

public class HtmlDocumentCreationException extends Exception {
	
	public HtmlDocumentCreationException(String msg){
		super(msg);
	}
	
	public HtmlDocumentCreationException(String msg, Exception e){
		super(msg, e);
	}

}
