package com.datalyxt.production.exception.webscraper;

public class LanguageCheckException extends Exception {
	public LanguageCheckException(String msg, Exception e) {
		super(msg, e);
	}

	public LanguageCheckException(String msg) {
		super(msg);
	}

}
