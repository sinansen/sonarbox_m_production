package com.datalyxt.production.webscraper.model.action;

public class ActionMetaConstant {
	//type
	public static final int pdf = 1;
	public static final int screenshot_img = 2;
	
	//status
	public static int file_created = 1 ;
	public static int file_storage_error = 2;
	
	public static String getFileSuffix(int type){
		if(type == pdf)
			return ".pdf";
		if(type == screenshot_img)
			return ".png";
		return "";
	}
	
}