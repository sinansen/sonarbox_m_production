package com.datalyxt.production.webscraper.model.action;

public class ActionResultText {
	public long id;
	public long resultId;
	public long blockId;
	public String type;
	public String content;
	public long createdAt;
}
