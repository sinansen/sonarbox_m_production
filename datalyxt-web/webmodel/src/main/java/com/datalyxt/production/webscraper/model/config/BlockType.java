package com.datalyxt.production.webscraper.model.config;

public enum BlockType {
dataBlock, linkBlock, timeBlock, keyvalueBlock, datasetBlock, pagingBlock
}
