package com.datalyxt.production.exception.webscraper;

public class FilterFailedException extends Exception {
	public FilterFailedException(String msg, Exception e) {
		super(msg, e);
	}
}
