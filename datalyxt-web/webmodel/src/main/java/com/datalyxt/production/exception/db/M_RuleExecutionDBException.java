package com.datalyxt.production.exception.db;

public class M_RuleExecutionDBException extends Exception {
	public M_RuleExecutionDBException(String msg) {
		super(msg);
	}

	public M_RuleExecutionDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_RuleExecutionDBException(Exception e) {
		super(e);
	}
}
