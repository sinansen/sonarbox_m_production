package com.datalyxt.production.globalsearch.context;


import java.util.HashSet;
import java.util.LinkedHashSet;

import com.datalyxt.production.block.model.EleStyle;
import com.datalyxt.production.block.model.Rect;


public class HTMLContext extends GlobalSearchContext{
	
	
	public HashSet<ContextType> contextTypes = new HashSet<>();
	public ContextType contextType;
	public String contextElementCssSelector;
	public String contextFullPhrase;
	public String contextDirektLink;
	public ATagType direktLinkType;
	public boolean contextIsaLink;
	public boolean contextIsInPageTitle;
	
	
	public boolean contextIsaBlock;
	public EleStyle elementStyle;
	public Rect elementLocation;
	public int pagehop;
	public double phraseFontSize; 
	public boolean contextIsVisible;
	
	public int contextOccurrenceSamePage=0;

		
	
	public LinkedHashSet<ContextLink> prevLinks=new LinkedHashSet<ContextLink>();
	public LinkedHashSet<ContextLink> nextLinks=new LinkedHashSet<ContextLink>();
	
	
	public int px2em=16;//1 em = 16 pixels.
	public double pt2em=11.955167;
	public String synonym;


	
	public void setFontSize(String fontSize){
		if(fontSize.contains("px")){
			fontSize=fontSize.replace("px", "");//.replace(".", "").replace(",", "");
			phraseFontSize=Double.parseDouble(fontSize)/px2em;
		}else if(fontSize.contains("em")){
			fontSize=fontSize.replace("px", "");//.replace(".", "").replace(",", "");
			phraseFontSize=Double.parseDouble(fontSize);
		}else if(fontSize.contains("pt")){
			fontSize=fontSize.replace("pt", "");//.replace(".", "").replace(",", "");
			phraseFontSize=Double.parseDouble(fontSize)/pt2em;
		}else if(fontSize.contains("rem")){
			fontSize=fontSize.replace("rem", "");//.replace(".", "").replace(",", "");
			phraseFontSize=Double.parseDouble(fontSize);
		}
	}
	
	public HTMLContext(){
		//used by json
		type = GlobalSearchContextClassType.html_context.name();
	}
}
