package com.datalyxt.production.exception.processing;

public class HtmlSourceCleanException extends Exception {
	public HtmlSourceCleanException(String msg) {
		super(msg);
	}

	public HtmlSourceCleanException(String msg, Exception e) {
		super(msg, e);
	}

	public HtmlSourceCleanException(Exception e) {
		super(e);
	}
}
