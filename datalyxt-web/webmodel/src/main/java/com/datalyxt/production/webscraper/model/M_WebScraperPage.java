package com.datalyxt.production.webscraper.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.datalyxt.production.webscraper.model.action.ActionResult;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.runtime.PageMeta;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.webscraper.model.link.Link;

public class M_WebScraperPage {

	public Link pageURL;
	public long sourceId;
	public String sourceDomain;
	public PageContentProcessStatus status;
	public long fetchFromDBTime;
	public long extractedAt;
	public QT1_V1_RuleContent ruleContent;
	public int pageHops;
	public String linkId;
	public long processAt;
	public long processstarttime;
	public int fetchHeaderRetry;
	public SourceConfig sourceConfig;
	public int fetchPageRetry;
	public long fetchUsedTime;
	public HashMap<String, Link> followLinks = new HashMap<>();
	public long ruleId;
	public String language;
	public long pageId;
	public HashMap<String, PagingBlock> visitedPaing = new HashMap<String, PagingBlock>();
	public int pages;
	public boolean isRedirect;
	public String redirectFrom;
	
	public ActionResult actionResult;
	public List<ActionResultFile> actionResultFiles = new ArrayList<>();
	
	public List<BlockRuntime> blocks = new ArrayList<>();
	
	public int retry; 
	
	public List<M_WebScraperPage> createdNewPages = new ArrayList<>();
	public PageMeta pageMeta;
	
	public String getLinkTimeValue(){
		String link_time_value = pageURL.url
				+ fetchFromDBTime + ruleId;
		return link_time_value;
	}

}
