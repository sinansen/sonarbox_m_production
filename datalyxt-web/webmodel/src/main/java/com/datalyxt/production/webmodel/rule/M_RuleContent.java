package com.datalyxt.production.webmodel.rule;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = QT1_V1_RuleContent.class),
		@Type(value = QT1_V2_RuleContent.class), })
public abstract class M_RuleContent implements Serializable {
	public String contentId;
}
