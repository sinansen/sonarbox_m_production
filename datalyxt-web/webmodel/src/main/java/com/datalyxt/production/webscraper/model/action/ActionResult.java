package com.datalyxt.production.webscraper.model.action;

import java.util.ArrayList;
import java.util.List;

public class ActionResult {
	public long id;
	public long ruleId;
	public long sourceId;
	public long scanId;
	public String normalizedURL;
	public int hop;
	public long createdAt;
	public List<ActionResultFile> files = new ArrayList<ActionResultFile>();
	public List<ActionResultText> texts = new ArrayList<ActionResultText>();
}
