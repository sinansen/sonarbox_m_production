package com.datalyxt.production.webscraper.model.runtime;

import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.webscraper.model.paging.PagingIndex;

public class PagingBlock extends ContentBlock {
	public List<PagingIndex> indexPages;
	public HashMap<String, LinkBlock> linkBlocks = new HashMap<>();
	public int pages;
}
