package com.datalyxt.production.exception.db;

public class M_PageBlockLinkDBException extends Exception{
	public M_PageBlockLinkDBException(String msg) {
		super(msg);
	}

	public M_PageBlockLinkDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_PageBlockLinkDBException(Exception e) {
		super(e);
	}
}
