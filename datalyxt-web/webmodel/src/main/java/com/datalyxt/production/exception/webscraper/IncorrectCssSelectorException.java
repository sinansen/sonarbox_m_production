package com.datalyxt.production.exception.webscraper;

import java.util.HashSet;

public class IncorrectCssSelectorException extends Exception {
	public HashSet<String> cssSelectors;
	public String pageURL;

	public IncorrectCssSelectorException(String msg) {
		super(msg);
	}

	public IncorrectCssSelectorException(String msg, HashSet<String> selectors,
			String pageURL) {
		super(msg);
		cssSelectors = selectors;
		this.pageURL = pageURL;
	}

	public IncorrectCssSelectorException(String msg, Exception e) {
		super(msg, e);
	}

	public IncorrectCssSelectorException(String msg, Exception e,
			HashSet<String> selectors, String pageURL) {
		super(msg, e);
		cssSelectors = selectors;
		this.pageURL = pageURL;
	}
}
