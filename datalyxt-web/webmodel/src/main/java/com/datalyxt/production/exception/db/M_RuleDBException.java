package com.datalyxt.production.exception.db;

public class M_RuleDBException extends Exception {
	public M_RuleDBException(String msg) {
		super(msg);
	}

	public M_RuleDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_RuleDBException(Exception e) {
		super(e);
	}
}
