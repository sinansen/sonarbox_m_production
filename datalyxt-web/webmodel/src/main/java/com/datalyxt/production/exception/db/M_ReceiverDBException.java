package com.datalyxt.production.exception.db;

public class M_ReceiverDBException extends Exception{
public M_ReceiverDBException(String msg){
	super(msg);
}

public M_ReceiverDBException(String msg, Exception e){
	super(msg, e);
}
}
