package com.datalyxt.production.exception.webscraper;

public class InvalidHTMLPageException extends Exception {
	public InvalidHTMLPageException(String msg) {
		super(msg);
	}

	public InvalidHTMLPageException(String msg, Exception e) {
		super(msg, e);
	}
}
