package com.datalyxt.production.webscraper.model.config;

public enum BlockActionType {
screenshot, cleanText, formattedText, fileStorage, imageStorage, linkStorage, structure, followLinks
}
