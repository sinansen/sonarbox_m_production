package com.datalyxt.production.webscraper.model.paging;


public class PagingElement {
	public String href="";
	public String hrefDescription="";
	public String parameterFree="";
	public String anchorId="";
	public ResultTarget rt;
	public PagingHrefType pht;
	public String selector;
}
