package com.datalyxt.production.execution;

public enum PageContentProcessStatus {
	page_created, page_submitted_to_process, page_process_started,visited , start_assess_cc , assess_cc_finished
}
