package com.datalyxt.production.exception.processing;

public class DownloadFileException extends Exception {

	public DownloadFileException(String message) {
		super(message);
	}

	public DownloadFileException(Exception e) {
		super(e);
	}

	public DownloadFileException(String message, Exception e) {
		super(message, e);
	}
}
