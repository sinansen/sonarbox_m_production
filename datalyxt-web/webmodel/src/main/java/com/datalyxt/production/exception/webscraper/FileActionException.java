package com.datalyxt.production.exception.webscraper;

public class FileActionException extends Exception {
	public FileActionException(String msg) {
		super(msg);
	}

	public FileActionException(String msg, Exception e) {
		super(msg, e);
	}

}
