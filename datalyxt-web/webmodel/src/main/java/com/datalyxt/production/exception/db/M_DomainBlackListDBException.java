package com.datalyxt.production.exception.db;

public class M_DomainBlackListDBException extends Exception {
	public M_DomainBlackListDBException(String msg) {
		super(msg);
	}

	public M_DomainBlackListDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_DomainBlackListDBException(Exception e) {
		super(e);
	}
}
