package com.datalyxt.production.exception.webscraper;

public class ColumnTemplateException extends Exception {

	public ColumnTemplateException(String msg) {
		super(msg);
	}
	public ColumnTemplateException(String msg,Exception e) {
		super(msg, e);
	}
}
