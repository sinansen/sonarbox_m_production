package com.datalyxt.production.execution.monitoring.model;

public enum Supervisor {
	CrawlerConfigSchedulerActor, FrontierActor, QT1V2Actor, QT1V2Service, CandidateContextAssesmentActor, WebScraperConfigSchedulerActor, QT1V1Actor, WebScraperPageDispatchActor, QT1V1FileService, QT1V1HtmlService, sendEmail

}
