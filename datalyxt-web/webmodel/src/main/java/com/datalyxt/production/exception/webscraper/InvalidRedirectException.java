package com.datalyxt.production.exception.webscraper;

public class InvalidRedirectException extends Exception {
	public InvalidRedirectException(String msg) {
		super(msg);
	}

}
