package com.datalyxt.production.webscraper.model.action;

import java.util.HashMap;

import com.datalyxt.production.webscraper.model.runtime.LinkBlock;

public class FollowLinkActionResult extends ActionResultText {
	public HashMap<String, LinkBlock> linkBlocks = new HashMap<>();

}
