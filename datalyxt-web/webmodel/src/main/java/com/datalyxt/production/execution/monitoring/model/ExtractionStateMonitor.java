package com.datalyxt.production.execution.monitoring.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

public class ExtractionStateMonitor implements Serializable{

	public int extractLinkAmount;
	public int followedLinkAmount;
	public int refBlockLinkAmount;
	public int newLinkAmount;
	public HashSet<String> createdScreenshots = new HashSet<String>();
	public HashMap<String,String> failedCreatedScreenshots = new HashMap<String, String>();
	public HashSet<String> createdTexts = new HashSet<String>();
	public HashMap<String, String> failedCreatedTexts = new HashMap<String,String>();
	public boolean failedToProcess;
	public HashMap<String, String> failedMessage = new HashMap<String,String>();
	public boolean pageUrlError;
	public boolean cssPathError;
}