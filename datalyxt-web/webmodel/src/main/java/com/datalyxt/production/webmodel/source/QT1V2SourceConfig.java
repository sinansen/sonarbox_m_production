package com.datalyxt.production.webmodel.source;

import java.util.Arrays;
import java.util.HashSet;

import com.datalyxt.webscraper.model.link.LinkType;

public class QT1V2SourceConfig {
	public int crawlerHops = 3;
	public int contextHops = 5;
	public int pageLoadTimeout = 5000;
	public HashSet<String> excludedLinkParts = new HashSet<String>();
	public HashSet<LinkType> excludedTypes = new HashSet<LinkType>();
	public HashSet<String> excludedLinks = new HashSet<>();
	public boolean enableUseLastFetchUsedTime = true;
	public int timesOfLastFetchUsedTime = 10;
	public int maxHTMLSize = 10000000;
	public int maxPDFSize = 10000000;
	public int maxBrowerRetry = 3;
	public int maxHeaderFetchRetry = 3;
	public LinkVisitStrategy linkVisitStrategy = new LinkVisitStrategy();
	public int nearLinkSize = 2;
	public HashSet<LanguageISO_639_1> languages = new HashSet<>(Arrays.asList(LanguageISO_639_1.de, LanguageISO_639_1.en));

}

