package com.datalyxt.production.exception.db;

public class M_PageDBException extends Exception {

	public M_PageDBException(String msg) {
		super(msg);
	}

	public M_PageDBException(String msg, Exception e) {
		super(msg, e);
	}

}
