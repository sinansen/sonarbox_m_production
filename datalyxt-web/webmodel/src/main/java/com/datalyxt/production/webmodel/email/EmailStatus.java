/**
 * 
 */
package com.datalyxt.production.webmodel.email;

/**
 * @author Bijan Fahimi
 *
 */
public class EmailStatus {
	public static final int CREATED = 1;
	public static final int SEND_OK = 2;
	public static final int ERROR = 3;
	public static final int TOO_MANY_RETRIES = 4;
	public static final int QUEUED = 5;
}
