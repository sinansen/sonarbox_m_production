package com.datalyxt.production.globalsearch.ranking;

import java.util.ArrayList;
import java.util.HashMap;

import com.datalyxt.production.globalsearch.context.HTMLContext;

public class HTMLRankingObject {

	public String keyWord;
	public String lang;
	public String kwID;
	
	public int incomingDirektLinks=0;
	public int incomingNearLinks=0;
	public int outgoingNLinks=0;
	public int outgoingDLinks=0;
	public int outgoingDFileLinks=0;
	public int outgoingNFileLinks=0;
	
	public int title=0;
	public int totNCon=0;
	public int totDCon=0;
	public int totVisNCon=0;
	public int totVisDCon=0;
	
	public double avgFontContext;
	public double maxFontContext;
	public double minFontContext;
	
	public double avgFontContextAll;
	public double maxFontContextAll;
	public double minFontContextAll;
	
	
	public double avgFontPage;
	public double maxFontPage;
	public double minFontPage;
	public double distribution;
	
	public int duplicateGlobalDContext=0;
	public int duplicateGlobalNContext=0;
	public int duplicateLocalDContext=0;
	public int duplicateLocalNContext=0;
	
	public int conInDoc=0;
	public int conInHTML=0;

	
	public ArrayList<String> incomingDLinkSources = new ArrayList<String> ();
	public ArrayList<String> incomingNLinkSources = new ArrayList<String> ();
	
	public ArrayList<String> outgoingNLinkTarget = new ArrayList<String> ();
	public ArrayList<String> outgoingDLinkTarget = new ArrayList<String>();
	public ArrayList<String> outgoingDFileTarget = new ArrayList<String>();
	public ArrayList<String> outgoingNFileTarget = new ArrayList<String>();
	
	public HashMap<String,ArrayList<String>> includedInDoc = new HashMap<String,ArrayList<String>>();
	public HashMap<String,ArrayList<String>> includedInHTML = new HashMap<String,ArrayList<String>>();

}
