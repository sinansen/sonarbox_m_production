package com.datalyxt.production.receiver;

import java.util.HashSet;

public class M_ReceiverGroup {
	public long id;
	public String name;
	public HashSet<Long> member;
	public M_ReceiverGroupStatus status;
	public long createdAt;
	public long lastModified;
	public String description;
	public long owner;

}
