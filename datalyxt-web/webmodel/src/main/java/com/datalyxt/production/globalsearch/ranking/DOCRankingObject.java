package com.datalyxt.production.globalsearch.ranking;

import java.util.ArrayList;
import java.util.HashMap;

import com.datalyxt.production.globalsearch.context.HTMLContext;

public class DOCRankingObject {
	public String keyWord;
	public String lang;
	public String kwID;
	
	public int incomingDirektLinks=0;
	public int incomingNearLinks=0;
	public int equalDocuments=0;
	
	public int title=0;
	public int totalOccurences=0;
	public double distribution;
	public int includedHTMLContext=0;
	public ArrayList<String> equal2Document = new ArrayList<String> ();
	public ArrayList<String> incomingDLinkSources = new ArrayList<String> ();
	public ArrayList<String> incomingNLinkSources = new ArrayList<String> ();
	public HashMap<String,ArrayList<String>> includedContextFromHTML = new HashMap<String,ArrayList<String>>();
}
