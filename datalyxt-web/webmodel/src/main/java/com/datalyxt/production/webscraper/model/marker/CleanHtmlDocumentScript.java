package com.datalyxt.production.webscraper.model.marker;

import java.util.HashSet;

public class CleanHtmlDocumentScript {
	public static final String datalyxt_span = "dlyxtspan";
	public boolean enrichTextNode = true;
	public boolean removeElementIdAttr = true;
	public boolean removeElementCssClassAttr = true;
	public boolean removeHeadJavascriptLink = false;
	public boolean removeJavascriptElementContent = false;
	public boolean enableEnrichTextNodeWithJs = false;
	public HashSet<String> cleanElementTags = new HashSet<String>();
	public boolean removeTextNode = false;
}
