package com.datalyxt.production.execution.monitoring.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.globalsearch.page.GlobalSearchPage;
import com.datalyxt.webscraper.model.link.Link;

public class M_SourceAssessment {

	public String lang;
	public long fetchTimeFromDB;
	public long sourceId;
	public HashMap<String, List<GlobalSearchPage>> globalpages = new HashMap<>();
	public HashMap<String, HashSet<String>> langKeywords = new HashMap<>();
	public List<Link> inProcessPageLinks = new ArrayList<Link>();

}
