package com.datalyxt.production.webmodel.email;


/**
 * 
 * @author Bijan Fahimi
 *
 */
public class Attachement {
	
	public byte[] attachement;
	public String mimetype;
	public String name;
	
	public Attachement(byte[] attachement, String mimetype) {
		super();
		this.attachement = attachement;
		this.mimetype = mimetype;
	}	
	
}
