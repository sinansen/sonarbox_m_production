package com.datalyxt.production.execution.monitoring.model;

public class TraceReportMeta {
	public String scanId;
	public String sourceId;
	public String ruleId;
	public String url;
	public String message;
}
