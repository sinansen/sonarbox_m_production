package com.datalyxt.production.exception.db;

public class M_SourceDBException extends Exception {
	public M_SourceDBException(String msg) {
		super(msg);
	}

	public M_SourceDBException(String msg, Exception e) {
		super(msg, e);
	}

	public M_SourceDBException(Exception e) {
		super(e);
	}
}
