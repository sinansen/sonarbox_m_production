package com.datalyxt.production.webscraper.model.paging;


public 	class PagingIndex implements Comparable<PagingIndex> {

	public String indexSelector;
	public int indexSelectorSiblingPosition;
	public String iteratedElement;
	public PagingElement pagingElement;

	@Override
	public int compareTo(PagingIndex comp) {
		return indexSelectorSiblingPosition
				- comp.indexSelectorSiblingPosition;
	}

}