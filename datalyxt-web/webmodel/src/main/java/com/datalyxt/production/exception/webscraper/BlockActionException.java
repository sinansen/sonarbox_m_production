package com.datalyxt.production.exception.webscraper;

public class BlockActionException extends Exception {
	public BlockActionException(String msg) {
		super(msg);
	}

	public BlockActionException(String msg, Exception e) {
		super(msg, e);
	}
}
