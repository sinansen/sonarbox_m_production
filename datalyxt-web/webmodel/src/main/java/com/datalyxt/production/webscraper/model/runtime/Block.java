package com.datalyxt.production.webscraper.model.runtime;

import com.datalyxt.production.webscraper.model.config.BlockType;

public abstract class Block {
	@Invisible
	public BlockType type;
	@Invisible
	public long blockId;
	@Invisible
	public int blockTextHash;

	public String blockText;
	
}
