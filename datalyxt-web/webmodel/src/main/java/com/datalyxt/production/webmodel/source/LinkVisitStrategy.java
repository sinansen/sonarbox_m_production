package com.datalyxt.production.webmodel.source;

public class LinkVisitStrategy {
	public boolean visitContextDirectLink = true;
	public boolean visitContextNearLink = true;
	public boolean visitPossibleContextLink = true;
	public boolean visitAllLink = false;
	
	public boolean visitContextFrameLink = true;
	public boolean visitContextExternalLink = true;
}
