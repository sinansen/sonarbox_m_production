package com.datalyxt.production.globalsearch.page;

public class DocumentPage extends GlobalSearchPage {
	public int documentPages;
	public String documentText;
	public int documentLength;
	public String documentProducer;

	public DocumentPage() {
		// used by Json
		type = GlobalSearchPageClassType.document.name();
	}
}
