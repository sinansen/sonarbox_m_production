package com.datalyxt.production.execution.monitoring.model;

import java.io.Serializable;

import com.datalyxt.production.webmodel.rule.M_RuleType;

public class M_RuleExecutionReport implements Serializable {
	
	public long ruleId;
	public long executionFinishTime;
	public long executionStartTime;
	public RuleExecutionStatus status;
	public String errors;
	public long fetchFromDbTime;
	public M_RuleType ruleType;
	public long id;
	public long sourceId;
}
