package com.datalyxt.production.webscraper.model.config;

import java.util.ArrayList;
import java.util.List;

public class BlockConfig {
	public BlockType type;
	public BlockLocation location;
	public Filter filter;
	public ActionCondition condition;
	public List<BlockAction> action = new ArrayList<>();
	public boolean multiRow = false;
	public String contentOrganisation;
	public List<BlockConfig> blocks;
	public String name;
	public boolean follow = true;  //used by link block
	
	public List<BlockLocation> supervised; // used by multi row detection
	public int column = -1;
	
	public int pages = 0; // used by paging block

}
