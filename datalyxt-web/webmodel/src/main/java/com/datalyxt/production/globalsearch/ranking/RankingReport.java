package com.datalyxt.production.globalsearch.ranking;

import java.util.ArrayList;
import java.util.HashMap;

public class RankingReport {
	public HashMap<String,HTMLRankingObject> rankingHTML = new HashMap<String,HTMLRankingObject>();
	public HashMap<String,DOCRankingObject> rankingDOC = new HashMap<String,DOCRankingObject>();
	public HashMap<String, HashMap<String, ArrayList<String>>> localDInstance = new HashMap<String, HashMap<String, ArrayList<String>>>();
	public HashMap<String, HashMap<String, ArrayList<String>>> localNInstance = new HashMap<String, HashMap<String, ArrayList<String>>>();
	public HashMap<String, HashMap<String, ArrayList<String>>> globalDInstance = new HashMap<String, HashMap<String, ArrayList<String>>>();
	public HashMap<String, HashMap<String, ArrayList<String>>> globalNInstance = new HashMap<String, HashMap<String, ArrayList<String>>>();
	public String sourceId;
	public String keyword;
	public String lang;
	public long fetchFromDBTime;
	public long timestamp;

}
