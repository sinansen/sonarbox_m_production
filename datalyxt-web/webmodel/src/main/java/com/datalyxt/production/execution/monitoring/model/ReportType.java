package com.datalyxt.production.execution.monitoring.model;

public enum ReportType {
	DatabaseReport,
	SourceVisitReport, SourceVisitReport_FetchPage,
	RuleExecutionReport, RankingReport, ActorReport, EmailSendReport, SourceVisitReport_processing, UnknownType

}
