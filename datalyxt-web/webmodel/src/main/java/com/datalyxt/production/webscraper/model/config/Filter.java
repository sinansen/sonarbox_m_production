package com.datalyxt.production.webscraper.model.config;

import java.util.HashSet;
import java.util.List;

import com.datalyxt.webscraper.model.link.LinkType;

public class Filter {
	public boolean enableBlackList = true;
	public HashSet<String> blacklist = new HashSet<String>();
	public HashSet<LinkType> domainFilter = new HashSet<LinkType>();
	public HashSet<LinkType> internalLinkTypeFilter = new HashSet<LinkType>();
	public HashSet<LinkType> externalLinkTypeFilter = new HashSet<LinkType>();
	public HashSet<String> urlFilter = new HashSet<String>();
	public int fileSizeInByte =2000;
	public int imageSizeInByte = 2000;
	public List<DatasetFilter> datasetFilter ;
}
