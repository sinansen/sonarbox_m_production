package com.datalyxt.production.log;

public enum M_ActivityType {
	   created, deleted, modified, validated, activated, paused, error
}
