package com.datalyxt.production.webmodel.rule;

import java.util.HashSet;

import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webscraper.model.config.BlockActionType;

public class QT1_V2_RuleContent extends M_RuleContent {

	public String searchRule;
	public HashSet<BlockActionType> actions = new HashSet<BlockActionType>();
	public boolean followMatchedNewLinks = false;
	public HashSet<LanguageISO_639_1> lang;


}
