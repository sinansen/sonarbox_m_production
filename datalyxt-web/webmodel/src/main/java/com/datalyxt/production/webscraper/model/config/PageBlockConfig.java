package com.datalyxt.production.webscraper.model.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class PageBlockConfig {
	public int hop;
	public String url;
	public List<BlockConfig> block = new ArrayList<BlockConfig>();
	public HashSet<String> multiRowPreidentifiers = new HashSet<>();
	
}
