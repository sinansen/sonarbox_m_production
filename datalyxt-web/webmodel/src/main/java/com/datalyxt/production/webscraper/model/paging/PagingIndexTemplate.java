package com.datalyxt.production.webscraper.model.paging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PagingIndexTemplate implements Comparable<PagingIndexTemplate> {
	public List<String> preElements = new ArrayList<String>();
	public List<String> postElements = new ArrayList<String>();
	public int iteratedElementPosition;
	public HashMap<String, PagingIndex> indexes = new HashMap<String, PagingIndex>();

	@Override
	public int compareTo(PagingIndexTemplate comp) {
		return comp.indexes.size() - indexes.size();
	}

}
