package com.datalyxt.backend;

public class WebSearchDataStorageUnavailableException extends Exception{
	public WebSearchDataStorageUnavailableException(String msg) {
		super(msg);
	}

	public WebSearchDataStorageUnavailableException(Exception e) {
		super(e);
	}

	public WebSearchDataStorageUnavailableException(String msg, Exception e) {
		super(msg, e);
	}
}
