package com.datalyxt.backend;

public enum SystemEventType {
	scheduler_terminated, websearchRoot_terminated, websearchRoot_tryRestart, websearchWatcher_heartbeat, databaseException, dbheartbeat, websearchWatcher_terminated, databaseWatcher_terminated
}
