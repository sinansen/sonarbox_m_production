package com.datalyxt.backend;

public class WebSearchMonitoryStorageUnavaialbeException extends Exception{
	public WebSearchMonitoryStorageUnavaialbeException(String msg) {
		super(msg);
	}

	public WebSearchMonitoryStorageUnavaialbeException(Exception e) {
		super(e);
	}

	public WebSearchMonitoryStorageUnavaialbeException(String msg, Exception e) {
		super(msg, e);
	}
}
