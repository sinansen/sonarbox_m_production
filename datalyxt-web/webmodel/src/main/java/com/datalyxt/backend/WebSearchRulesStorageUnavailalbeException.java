package com.datalyxt.backend;

public class WebSearchRulesStorageUnavailalbeException extends Exception{
	public WebSearchRulesStorageUnavailalbeException(String msg) {
		super(msg);
	}

	public WebSearchRulesStorageUnavailalbeException(Exception e) {
		super(e);
	}

	public WebSearchRulesStorageUnavailalbeException(String msg, Exception e) {
		super(msg, e);
	}
}
