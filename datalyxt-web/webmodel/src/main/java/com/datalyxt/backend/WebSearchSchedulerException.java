package com.datalyxt.backend;

public class WebSearchSchedulerException extends Exception{
	public WebSearchSchedulerException(String msg) {
		super(msg);
	}

	public WebSearchSchedulerException(Exception e) {
		super(e);
	}

	public WebSearchSchedulerException(String msg, Exception e) {
		super(msg, e);
	}
}
