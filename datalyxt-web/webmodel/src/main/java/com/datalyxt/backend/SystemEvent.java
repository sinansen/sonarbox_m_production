package com.datalyxt.backend;

import java.io.Serializable;

public class SystemEvent implements Serializable{
	public SystemEventType eventType;
	public long timestamp;
	public String details;

}
