package com.datalyxt.backend;

public class SystemEventStorageUnavailableException extends Exception{
	public SystemEventStorageUnavailableException(String msg) {
		super(msg);
	}

	public SystemEventStorageUnavailableException(Exception e) {
		super(e);
	}

	public SystemEventStorageUnavailableException(String msg, Exception e) {
		super(msg, e);
	}
}
