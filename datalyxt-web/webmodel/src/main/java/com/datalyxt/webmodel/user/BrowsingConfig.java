package com.datalyxt.webmodel.user;

public class BrowsingConfig {
	public String USER_AGENT = "Mozilla/5.0";
	public int maxRetry = 1;
	public int pageLoadTimeout = 8000;
}
