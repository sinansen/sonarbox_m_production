package com.datalyxt.webmodel.user;

import java.io.Serializable;

public class TrackBlock implements Serializable{
	public String domainURL;
	public String pageURL;
	public String blockCssPath;
	public String name;
	public BrowsingConfig browsingConfig;
	public TrackConfig trackconfig;
	public String pageId;
	public String blockId;
	public long createdAt;
	public ScanConfig scanConfig;
	public ScanType scanType;
}
