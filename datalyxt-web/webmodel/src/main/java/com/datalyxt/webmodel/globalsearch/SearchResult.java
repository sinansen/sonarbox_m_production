package com.datalyxt.webmodel.globalsearch;

public class SearchResult {
	public long searchTime;
	public String title;
	public String description;
	public String titleHtml;
	public String descriptionHtml;
	public String link;
}
