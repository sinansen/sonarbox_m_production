package com.datalyxt.webmodel.user;

import java.util.HashMap;

public class User {
    public String username;
    public String userid;
    public String email;
    public String password;
    public String company;
    public HashMap<String, Domain> tracking = new HashMap<String, Domain>();
}
