package com.datalyxt.webmodel.social;

import com.datalyxt.webscraper.model.link.LinkType;

public class Message {
	public String id;
	public LinkType socialtype;
	public String text;
	public long createdAt;
	public String domainurl;
	public String accountId;
	public boolean matchedRule = false;
	public String searchRule;
}
