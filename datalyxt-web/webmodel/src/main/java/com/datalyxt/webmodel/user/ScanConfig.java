package com.datalyxt.webmodel.user;

import java.io.Serializable;
import java.util.HashSet;

import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.webscraper.model.link.LinkType;

public class ScanConfig implements Serializable{
	public int depth;
	public HashSet<LinkType> excludedTypes = new HashSet<LinkType>();
	public long maxWaitTime;
	public BrowsingConfig browsingConfig = new BrowsingConfig();
	public String blockCssPath;
	public String searchRule ;
	public HashSet<BlockActionType> actions = new HashSet<BlockActionType>();
	public boolean followMatchedNewLinks = false;
}
