package com.datalyxt.webmodel.scan;

import java.util.HashSet;

import com.datalyxt.webscraper.model.link.LinkType;

public class DomainScanConfig {
	public String domainUrl;
	public HashSet<LinkType> excludedTypes;

}
