package com.datalyxt.webmodel.social;

import java.util.HashMap;

import com.datalyxt.webscraper.model.link.LinkType;

public class SocialMsgCheckpoint implements Comparable<SocialMsgCheckpoint> {
	public long checkTime;
	public HashMap<String, Message> msgs = new HashMap<String, Message>();
	public long msgsSince;
	public LinkType type;
	public String domainurl;

	public int compareTo(SocialMsgCheckpoint comp) {
		long diff = comp.checkTime - checkTime;
		if (diff > 0)
			return 1;
		if (diff < 0)
			return -1;
		return 0;
	}
}
