package com.datalyxt.webmodel.scan;

import java.util.HashMap;

public class TextContext {
	public String regex; //keyword
	public String text;
	public boolean isInLink=false;
	public boolean isPartofLink=false;
	public HashMap<String,Integer> link=new HashMap<String,Integer>();
	public String txtElementCssPath;
}
