package com.datalyxt.webmodel.scan;

import java.io.Serializable;

import com.datalyxt.webmodel.user.ScanConfig;
import com.datalyxt.webscraper.model.link.Link;

public class PageContentExtractionConfig implements Serializable,
		Comparable<PageContentExtractionConfig> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8038085941314501065L;
	public long scanStartAt;
	public Link pagelink;
	public ScanConfig scanconfig;

	@Override
	public int compareTo(PageContentExtractionConfig comp) {
		long diff = pagelink.parseTime - comp.pagelink.parseTime ;
		if(diff >0)
			return 1;
		if(diff < 0)
			return -1 ;
		return 0;
	}

}
