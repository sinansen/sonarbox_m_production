package com.datalyxt.webmodel.scan;

import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.webmodel.user.ScanConfig;
import com.datalyxt.webscraper.model.link.LinkTree;

public class ScannedDoaminPages {

	public String rootURL;
	public LinkTree linkTree = new LinkTree();
	public HashSet<String> visited = new HashSet<String>();
	public long scanStartAt;
	public long scanFinishAt;
	public ScanConfig scanConfig;
	public long lastVisitLinkTime;
	
	public HashMap<String, PageDensityMetrics> linkPageDensity = new HashMap<>();
}
