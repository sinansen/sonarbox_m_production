package com.datalyxt.webmodel.scan;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.webmodel.user.TrackBlock;
import com.datalyxt.webscraper.model.link.Link;

public class InProcessPage {
	public Link pageURL;
	public PageContentProcessStatus status;
	public TrackBlock trackblock;
	public List<Link> matchednewLinks = new ArrayList<Link>();
	public List<Link> pagelinks = new ArrayList<Link>();
	public long fetchUsedTime;
	public long crawlingStartAt;
	public long visitedAt;
	public String trackblockId;
	public long extractedAt;
}
