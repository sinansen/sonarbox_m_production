package com.datalyxt.webmodel.social;

import java.util.Hashtable;

public class Company {

	public String name;
	public String webURL;
	public String twitterID;
	public String facebookID;
	public String googleplusID;

	public Hashtable<String, Boolean> channelEnabled = new Hashtable<String, Boolean>();

	public boolean hasTwitter() {
		if (channelEnabled.containsKey("twitter")) {
			Boolean enabled = channelEnabled.get("twitter");
			return enabled;
		}
		return false;
	}
	public boolean hasGoogleplus() {
		if (channelEnabled.containsKey("googleplus")) {
			Boolean enabled = channelEnabled.get("googleplus");
			return enabled;
		}
		return false;
	}
	public boolean hasFacebook() {
		if (channelEnabled.containsKey("facebook")) {
			Boolean enabled = channelEnabled.get("facebook");
			return enabled;
		}
		return false;
	}
}
