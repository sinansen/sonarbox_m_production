package com.datalyxt.webmodel.user;

import java.util.HashSet;

import com.datalyxt.production.webscraper.model.config.BlockActionType;

public class TrackConfig {
	public NavigationRule navigationRule = new NavigationRule();
	public HashSet<BlockActionType> actions = new HashSet<BlockActionType>();
}
