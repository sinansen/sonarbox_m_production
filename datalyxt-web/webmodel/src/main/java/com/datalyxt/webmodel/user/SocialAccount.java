package com.datalyxt.webmodel.user;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.webscraper.model.link.LinkType;

public class SocialAccount {
	public String domainUrl;
	public LinkType socialType;
	public String accountId;
	public String accountName;
	public String accountUrl;
	public List<String> searchRules =new ArrayList<String>();
}
