package com.datalyxt.webmodel.user;

import java.util.HashSet;

import com.datalyxt.webscraper.model.link.LinkState;
import com.datalyxt.webscraper.model.link.LinkType;

public class NavigationRule {
	public boolean followLinks = false;
	public HashSet<LinkType> followedLinkType = new HashSet<LinkType>();
	public LinkState followedLinkState = null;
	public HashSet<String> excludedLinks = new HashSet<String>();

}
