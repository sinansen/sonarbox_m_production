package com.datalyxt.domain;

import com.datalyxt.webscraper.model.page.Provider;

public class DomainMetaInfo {
	public Provider domainProvider;
	public String domainIp;
	public int domainPageRank;
	
	public DomainMetaInfo(Provider domainProvider, int domainPageRank, String domainIp){
		this.domainProvider=domainProvider;
		this.domainIp=domainIp;
		this.domainPageRank=domainPageRank;
	}
}
