package com.datalyxt.language;

import java.util.ArrayList;
import java.util.List;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

public class LanguageUtil {
	private static LanguageUtil instance;
	Detector detector;

	private LanguageUtil() {

	}

	public static LanguageUtil getInstance(String path) {
		if (instance == null) {
			instance = new LanguageUtil();
			if (path == null)
				instance.init();
			else
				try {
					instance.init(path);
				} catch (LangDetectException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return instance;
	}

	private void init() {
		try {
			List<String> langJsons = new ArrayList<String>();
			langJsons.add(Profile.de);
			langJsons.add(Profile.en);
			DetectorFactory.loadProfile(langJsons);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public String getLanguage(String text) throws LangDetectException {
		detector = DetectorFactory.create();
		detector.append(text);
		String lang = detector.detect();
		return lang;
	}

	private void init(String profileDirectory) throws LangDetectException {
		DetectorFactory.loadProfile(profileDirectory);
	}
}
