package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;


public interface Normalizer {
	public void execute(URLWrapper url) throws URLNormalizerException;
}
