package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_1 : Converting the scheme and host to lower case
 *
 * @author junma
 *
 */
public class LowerCaseNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		url.host = url.host.toLowerCase();
		url.protocol = url.protocol.toLowerCase();
	}

}
