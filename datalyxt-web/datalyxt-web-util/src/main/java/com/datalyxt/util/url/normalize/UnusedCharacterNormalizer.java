package com.datalyxt.util.url.normalize;

import java.util.SortedMap;
import java.util.TreeMap;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.util.url.URLUtil;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_19 : Removing unused '&'
 * 
 * @author junma
 *
 */
public class UnusedCharacterNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		if (url.sortedParamMap == null)
			url.sortedParamMap = URLUtil.createParameterMap(url.query);
		if (url.sortedParamMap != null) {
			SortedMap<String, String> prams = new TreeMap<>();
			for (String key : url.sortedParamMap.keySet()) {
				if (!key.startsWith("&"))
					prams.put(key, url.sortedParamMap.get(key));
			}
			url.sortedParamMap = prams;
		}
	}

}
