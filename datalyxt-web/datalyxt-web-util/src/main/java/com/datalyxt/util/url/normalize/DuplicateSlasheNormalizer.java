package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_11 : Removing duplicate slashes
 * 
 * @author junma
 *
 */
public class DuplicateSlasheNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {

		int idx = url.path.indexOf("//");
		while (idx >= 0) {
			url.path = url.path.replace("//", "/");
			idx = url.path.indexOf("//");
		}

		
	}



}
