package com.datalyxt.util;

import java.net.MalformedURLException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetHost {

	private static final Logger logger = LoggerFactory.getLogger(GetHost.class);

	public static String getHost(String url) {
		try {
			return new URL(url).getHost();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.error("can't get host: " + url, e);
			return null;
		}
	}

	public static String getAnchorFree(String url) {
		try {

			if (url.contains("#")) {
				String tmp = url.substring(0, url.indexOf("#"));
				url = tmp;
				return url;
			} else {
				return url;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("can't generate anchor free URL: " + url, e);
			return null;
		}
	}

	public static String getParameterFree(String url) {
		try {

			if (url.contains("?")) {
				String tmp = url.substring(0, url.indexOf("?"));
				url = tmp;
				return url;
			} else {
				return url;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("can't generate parameter free URL: " + url, e);
			return null;
		}
	}

	public static String getDomain(String url) {
		try {
			URL linkurl = new URL(url);
			String domain = linkurl.getHost().replace("www.", "");
			return domain;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.error("can't get domain: " + url, e);
			return null;
		}
	}

	public static String getProtocalHost(String url) {
		try {
			URL linkurl = new URL(url);
			String value = linkurl.getProtocol() + "://" + linkurl.getHost();
			return value;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			logger.error("can't get ProtocalHost: " + url, e);
			return null;
		}
	}

	public static String guessBaseUrl(String curUrl) {
		URL url;
		try {
			url = new URL(curUrl);
			int index = url.getFile().lastIndexOf('/');
			String base = url.getProtocol() + "://" + url.getHost() + "/";
			if (index > 0) {
				String path = url.getFile().substring(0, index);
				base = url.getProtocol() + "://" + url.getHost() + path + "/";
			}
			return base;
		} catch (MalformedURLException e) {
			logger.error("can't get base url: " + curUrl, e);
		}
		return "";
	}
}
