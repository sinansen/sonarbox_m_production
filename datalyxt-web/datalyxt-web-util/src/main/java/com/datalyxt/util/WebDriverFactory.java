package com.datalyxt.util;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.html.WebDriverFactoryException;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class WebDriverFactory {

	private static final Logger logger = LoggerFactory
			.getLogger(WebDriverFactory.class);

	public static WebDriver createDriver(BrowserDriverType driverType) {
		WebDriver driver = null;
		if (driverType.equals(BrowserDriverType.firefox_unix)) {

			String Xport = System.getProperty("lmportal.xvfb.id", ":1");
			final File firefoxPath = new File(System.getProperty(
					"lmportal.deploy.firefox.path", "/usr/bin/firefox"));
			FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);
			firefoxBinary.setEnvironmentProperty("DISPLAY", Xport);

			// Start Firefox driver
			driver = new FirefoxDriver(firefoxBinary, null);
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		} else if (driverType.equals(BrowserDriverType.firefox_windows))
			driver = new FirefoxDriver();

		driver.manage().window().setSize(new Dimension(1920, 1200));

		return driver;
	}

	public static WebDriver createDriver(BrowserDriverType driverType,
			Properties proxyProperties) throws WebDriverFactoryException {
		WebDriver driver = null;
		try {
			String network_proxy_http = proxyProperties.getProperty("network_proxy_http");
			int network_proxy_port = Integer.valueOf(proxyProperties.getProperty("network_proxy_port"));
			int network_proxy_type = Integer.valueOf(proxyProperties.getProperty("network_proxy_type"));
			logger.info("use network_proxy_http: [" + network_proxy_http + "] network_proxy_port: [" + network_proxy_port
					+ "] network_proxy_type: ["+ network_proxy_type	+ "]");
			FirefoxProfile profile = new FirefoxProfile();
			
			profile.setPreference("network.proxy.type", network_proxy_type);
			
			profile.setPreference("network.proxy.http", network_proxy_http);
			profile.setPreference("network.proxy.http_port", network_proxy_port);

			profile.setPreference("network.proxy.ssl", network_proxy_http);
			profile.setPreference("network.proxy.ssl_port", network_proxy_port);
			
			
			profile.setPreference("network.proxy.ftp", network_proxy_http);
			profile.setPreference("network.proxy.ftp_port", network_proxy_port);
			
			profile.setPreference("network.proxy.socks", network_proxy_http);
			profile.setPreference("network.proxy.socks_port", network_proxy_port);
			

			if (driverType.equals(BrowserDriverType.firefox_unix)) {

				String Xport = System.getProperty("lmportal.xvfb.id", ":1");
				final File firefoxPath = new File(System.getProperty(
						"lmportal.deploy.firefox.path", "/usr/bin/firefox"));
				FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);
				firefoxBinary.setEnvironmentProperty("DISPLAY", Xport);

				// Start Firefox driver
				driver = new FirefoxDriver(firefoxBinary, profile);
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

			} else if (driverType.equals(BrowserDriverType.firefox_windows)) {
				driver = new FirefoxDriver(profile);
			}
			driver.manage().window().setSize(new Dimension(1920, 1200));

			return driver;
		} catch (Exception e) {
			throw new WebDriverFactoryException("create web driver failed: "
					+ driverType.name() + " " + e.getMessage(), e);
		}
	}
}
