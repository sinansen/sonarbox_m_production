package com.datalyxt.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;

public class HtmlDocumentUtil {
	public static Document getDocumentFromSource(String pageURL,
			String pageSource, CleanHtmlDocumentScript script)
			throws HtmlDocumentCreationException {
		try {
			String baseUri = "";
			Document doc = Jsoup.parse(pageSource);
			baseUri = doc.baseUri();
			String currentUrl = pageURL;
			if (baseUri == null || baseUri.isEmpty())
				baseUri = GetHost.guessBaseUrl(currentUrl);
			doc.setBaseUri(baseUri);

			if (script == null)
				return doc;

			if (script.enrichTextNode) {
				HtmlTextNodeUtil htmlTextNodeUtil = new HtmlTextNodeUtil();
				doc = htmlTextNodeUtil.enrichMarkableTextNode(doc);
			}
			if (script.removeElementIdAttr) {
				Elements el = doc.getAllElements();
				for (Element e : el) {
					e.removeAttr("id");
					e.removeAttr("class");
				}

			}
			if (script.removeElementCssClassAttr) {
				Elements el = doc.getAllElements();
				for (Element e : el) {
					e.removeAttr("class");
				}

			}
			if (script.removeHeadJavascriptLink) {
				Elements heads = doc.getElementsByTag("head");
				if (!heads.isEmpty()) {
					Element head = heads.first();
					Elements scripts = head.getElementsByTag("script");
					scripts.remove();
				}

			}
			if (script.removeJavascriptElementContent) {
				Element body = doc.body();
				if (body != null) {
					Elements scripts = body.getElementsByTag("script");
					scripts.empty();
					for(Element ele : scripts){
						ele.removeAttr("src");
						ele.removeAttr("type");
					}
				}
			}

			for (String tagName : script.cleanElementTags) {
				Elements eles = doc.getElementsByTag(tagName);
				eles.remove();
			}
			return doc;
		} catch (Exception e) {
			throw new HtmlDocumentCreationException(
					"create html document failed: " + pageURL + " . "
							+ e.getMessage(), e);
		}
	}

}
