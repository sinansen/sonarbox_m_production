package com.datalyxt.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.StringTokenizer;

public class CleanSession {
	public static String cleanSession(String url,
			HashSet<String> dynamicParameters) {
		String cleanUrl = url;
		ArrayList<String> delim = new ArrayList<String>();

		if (dynamicParameters == null || dynamicParameters.isEmpty()) {
			return url;
//			dynamicParameters = new HashSet<String>();
//
//			dynamicParameters.add("PHPSESSID");// NORMA
//			dynamicParameters.add("jsessionid");
//			dynamicParameters.add("vt");// Tschibo
//			dynamicParameters.add("cache");
		}
		delim.add("?");
		delim.add("&");
		delim.add(";");
		delim.add("-"); // TODO check.
		delim.add(":");
		StringTokenizer token;
		String tok;
		String delims = "?&;:";
		token = new StringTokenizer(cleanUrl, delims);
		while (token.hasMoreElements()) {
			tok = (String) token.nextElement();
			int index = cleanUrl.indexOf(tok);
			String delimiter = "";
			if (index > 0)
				delimiter = cleanUrl.substring(index - 1, index);
			for (String sessionElement : dynamicParameters) {
				if (tok.contains(sessionElement)) {
					cleanUrl = cleanUrl.replace(delimiter + tok, "");
				}
			}
		}
		return cleanUrl;
	}
	public static String cleanSession(String url){
		String cleanUrl = url;
		ArrayList<String> delim = new ArrayList<String>();
		HashSet<String> sessionParameter = new HashSet<String>();

//			sessionParameter.add("PHPSESSID");// NORMA
			sessionParameter.add("jsessionid");
//			sessionParameter.add("vt");// Tschibo
//			sessionParameter.add("cache");
	
		delim.add("?");
		delim.add("&");
		delim.add(";");
		delim.add("-"); // TODO check.
		delim.add(":");
		StringTokenizer token;
		String tok;
		String delims = "?&;:";
		token = new StringTokenizer(cleanUrl, delims);
		while (token.hasMoreElements()) {
			tok = (String) token.nextElement();
			int index = cleanUrl.indexOf(tok);
			String delimiter = "";
			if (index > 0)
				delimiter = cleanUrl.substring(index - 1, index);
			for (String sessionElement : sessionParameter) {
				if (tok.contains(sessionElement)) {
					cleanUrl = cleanUrl.replace(delimiter + tok, "");
				}
			}
		}
		return cleanUrl;
	}
	
}
