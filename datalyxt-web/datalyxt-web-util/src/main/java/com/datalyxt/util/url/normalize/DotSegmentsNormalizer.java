package com.datalyxt.util.url.normalize;

import java.net.URI;
import java.net.URISyntaxException;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_6 : Removing dot-segments
 * 
 * @author junma
 *
 */
public class DotSegmentsNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {

		try {
			url.path = new URI(url.path).normalize().toString();
			while (url.path.startsWith("/../")) {
				url.path = url.path.substring(3);
			}
		} catch (URISyntaxException e) {
			throw new URLNormalizerException("DotSegmentsNormalizer error: "
					+ e.getMessage(), e);
		}

	}

}
