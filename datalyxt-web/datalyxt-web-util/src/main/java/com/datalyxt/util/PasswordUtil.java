package com.datalyxt.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordUtil {
	
	private final static Pattern whitespace = Pattern.compile("\\s");

	public static boolean isValidPassword(String password) {
		if (password == null)
			return false;
		Matcher matcher = whitespace.matcher(password);
		if(matcher.find())
			return false;
		if (password.length() < 6 || password.length()>10)
			return false;
		return true;

	}

}
