package com.datalyxt.util;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.datalyxt.production.exception.processing.HtmlSourceCleanException;

public class HtmlSourceValidationUtil {
	// TODO stop by "
	private static final Pattern p = Pattern
			.compile("((class=\"[^\"]*\\s[0-9]+(\\s|\")[^\"]*\"))");

	private static final Pattern cssClass = Pattern.compile("class=\"[^\"]*\"");

	// String c1 = "class=\"[^\"]*\"";
	// String c2 = "(\\s[0-9]+(\\s|\"))";
	// private static final Pattern p = Pattern
	// .compile("class=\"[^\"]*\"");

	// do removeSpacesInClassStart first
	public static String removeInvalidCssRefFromClass(String html) {

		Matcher m = p.matcher(html);
		StringBuffer sb = new StringBuffer();

		while (m.find()) {
			String replacement = removeInvalidPart(m.group());
			m.appendReplacement(sb, replacement);
		}

		m.appendTail(sb);

		html = sb.toString();

		return html;
	}

	private static String removeInvalidPart(String source) {
		source = source.replaceAll("[ ]+[0-9]+[ ]+", " ");
		source = source.replaceAll("[ ]+[0-9]+\"", "\"");
		return source;
	}

	public static String removeInvalidCharsInCssRefFromClass(String html) {

		Matcher m = cssClass.matcher(html);
		StringBuffer sb = new StringBuffer();
		while (m.find()) {
			String replacement;
			if (m.group().contains("%20"))
				replacement = "";
			else
				replacement = m.group();

			m.appendReplacement(sb, replacement);
		}

		m.appendTail(sb);

		html = sb.toString();

		return html;
	}

	public static String removeSpaces(String text) {
		return text.replaceAll("\\s+", " ");
	}

	// TODO fehler text in Bilder, redirect zur Startseite ,
	public static boolean is404page(String html) {
		// TODO
		if (html == null || html.isEmpty())
			return true;
		String lowercaseHtml = html.toLowerCase();
		boolean has404 = lowercaseHtml.contains("404");
		boolean notfound = lowercaseHtml.contains("[ ]+not[ ]+found[ ]+");
		boolean validSize = lowercaseHtml.length() < 700;

		return validSize;
	}

	public static boolean is403page(String html) {
		// TODO
		if (html == null || html.isEmpty())
			return true;
		String lowercaseHtml = html.toLowerCase();
		boolean has403 = lowercaseHtml.contains("403");
		boolean forbidden = lowercaseHtml.contains("forbidden+");
		boolean validSize = lowercaseHtml.length() < 700;

		return validSize;
	}

	// public static String cleanInvalidCss(String pageSource) {
	// pageSource = HtmlSourceValidationUtil.removeSpaces(pageSource);
	//
	// pageSource = HtmlSourceValidationUtil
	// .removeInvalidCssRefFromClass(pageSource);
	// pageSource = HtmlSourceValidationUtil
	// .removeInvalidCharsInCssRefFromClass(pageSource);
	// return pageSource;
	// }

	public static String cleanInvalidCss(String pageSource) throws HtmlSourceCleanException {
		try {
			pageSource = HtmlSourceValidationUtil.removeSpaces(pageSource);

			Pattern pattern1 = Pattern.compile("class\\s*=\\s*\"[^\"]*\"");
			Pattern pattern2 = Pattern.compile("\"(.)*\"");

			Pattern pattern3 = Pattern.compile("^[[-]*[	0-9]*]+.*");
			Pattern pattern4 = Pattern.compile("^.*[[%]*].*$");

			Matcher m = pattern1.matcher(pageSource);
			String delimiter = " ";
			String res;

			ArrayList<String> cleanedClasses = new ArrayList<String>();

			while (m.find()) {
				Matcher m1 = pattern2.matcher(m.group());
				while (m1.find()) {
					cleanedClasses.clear();
					res = "class=\"";

					String result = m1.group().replace("\"", "").trim();
					// System.out.println(result);
					String[] tokens = result.split(delimiter);

					boolean found;
					for (String token : tokens) {
						found = false;
						Matcher m2 = pattern3.matcher(token);
						if (m2.find()) {
							if (m2.group().equals(token)) {
								found = true;
								// System.out.println(m2.group());
							}
						}

						m2 = pattern4.matcher(token);
						if (m2.find()) {
							if (m2.group().equals(token)) {
								// System.out.println(m2.group());
								found = true;
							}
						}
						if (!found) {
							cleanedClasses.add(token);
						}
					}
					for (String classValue : cleanedClasses) {
						res += classValue + " ";
					}
					res = res.trim() + "\"";
					pageSource = pageSource.replace(m.group(), res);
				}

			}
			return pageSource;
		} catch (Exception e) {
			throw new HtmlSourceCleanException(
					"HtmlSourceValidationUtil -> cleanInvalidCss: can't clean source!! "
							+ e.getMessage(), e);
		}
	}

}
