package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_5 : Adding trailing / 
 * 
 * @author junma
 *
 */
public class TrailingNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {

	     if (url.path.length() == 0) {
	         url.path = "/";
	       }
	}




}
