package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_14 : Removing unused query variables
 * 
 * @author junma
 *
 */
public class UnusedQueryNormalizer implements Normalizer{

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
	}



}
