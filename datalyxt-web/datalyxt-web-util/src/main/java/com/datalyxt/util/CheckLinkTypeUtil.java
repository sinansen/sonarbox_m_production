package com.datalyxt.util;

import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.link.LinkType;

public class CheckLinkTypeUtil {
	public static void checkLinkType(Link link, String domain) {
		if (link.url.contains("http://") || link.url.contains("https://")) {

			if (InternalLink.isInternalLink(link.url, domain)) {
				link.linkType.add(LinkType.internal);

				if (SubDomain.subDomain(link.url, domain)) {
					link.linkType.add(LinkType.subdomain);
				}

			} else {
				link.linkType.add(LinkType.external);
			}

			if (ImageLink.isImageLink(link.url))
				link.linkType.add(LinkType.image);
			if(link.url.contains(".js"))
				link.linkType.add(LinkType.jsfile);
			if(link.url.contains(".css"))
				link.linkType.add(LinkType.css);
			if(link.url.contains(".mp4"))
				link.linkType.add(LinkType.video);
		}
	}
}
