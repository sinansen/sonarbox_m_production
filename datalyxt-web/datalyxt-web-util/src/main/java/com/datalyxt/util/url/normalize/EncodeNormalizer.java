package com.datalyxt.util.url.normalize;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.util.url.URLUtil;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_2 : Capitalizing letters in escape sequences
 * url_norm_18: Encode unsafe and reserved characters: '+', ' ' and '*' 


 * @author junma
 *
 */
public class EncodeNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		if(url.sortedParamMap == null)
			url.sortedParamMap = URLUtil.createParameterMap(url.query);
		if (url.sortedParamMap != null && url.sortedParamMap.size() > 0) {
			url.sortedParamMap = normalize(url.sortedParamMap);
		} 

	}



	/**
	 * Normalize the query string.
	 *
	 * @param sortedParamMap
	 *            Parameter name-value pairs in lexicographical order.
	 * @return Canonical form of query string.
	 */
	private static SortedMap<String, String> normalize(
			final SortedMap<String, String> sortedParamMap) {
		if (sortedParamMap == null || sortedParamMap.isEmpty()) {
			return sortedParamMap;
		}
		SortedMap<String, String> normalized = new TreeMap<String, String>();
		for (Map.Entry<String, String> pair : sortedParamMap.entrySet()) {
			normalized.put(percentEncodeRfc3986(pair.getKey()), percentEncodeRfc3986(pair.getValue()));
		}
		return normalized;
	}

	/**
	 * Percent-encode values according the RFC 3986. The built-in Java
	 * URLEncoder does not encode according to the RFC, so we make the extra
	 * replacements.
	 *
	 * @param string
	 *            Decoded string.
	 * @return Encoded string per RFC 3986.
	 */
	private static String percentEncodeRfc3986(String string) {
		try {
			string = string.replace("+", "%2B");
			string = URLDecoder.decode(string, "UTF-8");
			string = URLEncoder.encode(string, "UTF-8");
			return string.replace("+", "%20").replace("*", "%2A")
					.replace("%7E", "~");
		} catch (Exception e) {
			return string;
		}
	}

}
