package com.datalyxt.util;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;

public class HtmlTextNodeUtil {

	public Document enrichMarkableTextNode(Document document) {
		Element body = document.body();
		 List<TextNode> textnodes = new ArrayList<TextNode>();
		 visitNode(body, textnodes);
		 for(TextNode textNode : textnodes){
			 boolean markable = true;
			 if(!textNode.siblingNodes().isEmpty())
				 markable = false;
			 if(!markable){
				 Node parent = textNode.parentNode();
				 
				 if(!textNode.text().trim().isEmpty()){
					 if(parent != null && parent instanceof Element){
						 if(parent.nodeName().toLowerCase() != "dlyxtspan")
							 textNode.wrap("<dlyxtspan></dlyxtspan>");
					 }
				 }
			 }
		 }
		return document;
	}

	private void visitNode(Node node, List<TextNode> textnodes) {
		if (node instanceof TextNode) {
			textnodes.add((TextNode) node);
			return;
		}
		List<Node> children = node.childNodes();
		for (Node child : children) {
			visitNode(child, textnodes);

		}
	}
	
}
