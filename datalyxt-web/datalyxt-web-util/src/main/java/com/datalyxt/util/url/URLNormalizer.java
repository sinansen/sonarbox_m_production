package com.datalyxt.util.url;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.extraction.CreateURLWrapperException;
import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.util.url.normalize.DefaulPortNormalizer;
import com.datalyxt.util.url.normalize.DotSegmentsNormalizer;
import com.datalyxt.util.url.normalize.DuplicateSlasheNormalizer;
import com.datalyxt.util.url.normalize.EncodeNormalizer;
import com.datalyxt.util.url.normalize.EncodedOctetNormalizer;
import com.datalyxt.util.url.normalize.FragmentNormalizer;
import com.datalyxt.util.url.normalize.LowerCaseNormalizer;
import com.datalyxt.util.url.normalize.Normalizer;
import com.datalyxt.util.url.normalize.QuerySortingNormalizer;
import com.datalyxt.util.url.normalize.QuestionMarkNormalizer;
import com.datalyxt.util.url.normalize.TrailingNormalizer;
import com.datalyxt.util.url.normalize.UnusedCharacterNormalizer;
import com.datalyxt.webscraper.model.link.URLWrapper;

public class URLNormalizer {
	private static List<Normalizer> normalizers;
	private static DefaulPortNormalizer defaulPortNormalizer;
	private static DotSegmentsNormalizer dotSegmentsNormalizer;
	private static DuplicateSlasheNormalizer duplicateSlasheNormalizer;
	private static EncodedOctetNormalizer encodedOctetNormalizer;
	private static EncodeNormalizer encodeNormalizer;
	private static FragmentNormalizer fragmentNormalizer;
	private static LowerCaseNormalizer lowerCaseNormalizer;
	private static QuerySortingNormalizer querySortingNormalizer;
	private static QuestionMarkNormalizer questionMarkNormalizer;
	private static TrailingNormalizer trailingNormalizer;
	private static UnusedCharacterNormalizer unusedCharacterNormalizer;

	private static void init() {
		normalizers = new ArrayList<>();
		defaulPortNormalizer = new DefaulPortNormalizer();
		dotSegmentsNormalizer = new DotSegmentsNormalizer();
		duplicateSlasheNormalizer = new DuplicateSlasheNormalizer();
		encodedOctetNormalizer = new EncodedOctetNormalizer();
		encodeNormalizer = new EncodeNormalizer();
		fragmentNormalizer = new FragmentNormalizer();
		lowerCaseNormalizer = new LowerCaseNormalizer();
		querySortingNormalizer = new QuerySortingNormalizer();
		questionMarkNormalizer = new QuestionMarkNormalizer();
		trailingNormalizer = new TrailingNormalizer();
		unusedCharacterNormalizer = new UnusedCharacterNormalizer();

		normalizers.add(defaulPortNormalizer);
		normalizers.add(duplicateSlasheNormalizer);
		normalizers.add(fragmentNormalizer);
		normalizers.add(lowerCaseNormalizer);
		normalizers.add(encodedOctetNormalizer);
		normalizers.add(encodeNormalizer);
		normalizers.add(querySortingNormalizer);
		normalizers.add(questionMarkNormalizer);
		normalizers.add(trailingNormalizer);
		normalizers.add(unusedCharacterNormalizer);
		normalizers.add(dotSegmentsNormalizer);
	}

	public static String getNormalizedURL(String url) throws URLNormalizerException {
		return getNormalizedURL(url, null);
	}

	public static String getNormalizedURL(String href, String context)
			throws URLNormalizerException {
		if(normalizers == null)
			init();
		String url = UrlResolver.resolveUrl(context == null ? "" : context,
				href);
		URLWrapper urlWrapper;
		try {
			urlWrapper = URLUtil.createURLWrapper(url);
			for(Normalizer normalizer : normalizers){
				normalizer.execute(urlWrapper);
			}
			return urlWrapper.getURL();
		} catch (Exception e) {
			throw new URLNormalizerException("error in normalize url: "+href+" -"+e.getMessage(), e);
		}
		
	

	}
}
