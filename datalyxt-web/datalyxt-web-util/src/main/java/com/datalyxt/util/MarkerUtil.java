package com.datalyxt.util;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.webmodel.user.BrowsingConfig;

public class MarkerUtil {
	public static Document getCleanedPageSource(String url, WebDriver driver,
			CleanHtmlDocumentScript script, String dlyxtbaseuri, BrowsingConfig browsingConfig)
			throws HtmlDocumentCreationException, OpenPageUrlException {

		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();


		pageNavigationUtil.navigateToPage(driver, browsingConfig, url);
		JavascriptExecutor jsdriver = (JavascriptExecutor)driver;
		String pageSource = (String)jsdriver.executeScript("return document.documentElement.outerHTML;");

		Document doc = HtmlDocumentUtil.getDocumentFromSource(url, pageSource,
				script);

		Elements heads = doc.getElementsByTag("head");
		Element head = null;
		if (heads.isEmpty()) {
			head = doc.createElement("head");
		} else
			head = heads.first();
        Elements bases = head.getElementsByTag("base");
        if(bases.isEmpty()){
        	Element first = head.children().first();
        	if(first!=null)
        		 first.before("<base href=\""+doc.baseUri()+"\">");
        	else head.appendElement("base").attr("href", doc.baseUri());
        }

		String jqueryJSLocation = dlyxtbaseuri + "/js/jquery-1.11.3.min.js";
		head.appendElement("script").attr("src", jqueryJSLocation);
		String markerCssLocation = dlyxtbaseuri + "/css/marker.css";
		head.appendElement("link").attr("href", markerCssLocation)
				.attr("rel", "stylesheet");
		String markerDataJSLocation = dlyxtbaseuri + "/js/markerdata.js";
		head.appendElement("script").attr("src", markerDataJSLocation);
		String markerJSLocation = dlyxtbaseuri + "/js/marker.js";
		head.appendElement("script").attr("src", markerJSLocation);
		pageSource = doc.html();
		if (script.enableEnrichTextNodeWithJs) {
			String code = "document.addEventListener('onload', check());";
			doc.body().appendElement("script").attr("type", "text/javascript")
					.appendText(code);
		}
		return doc;
	}

	public static String getCharset(Document doc) {
		String charset = "UTF-8";

		Elements els = doc.select("meta[content*=charset]");

		for (Element link : els) {
			String[] values = link.attr("content").split(";");
			for (String value : values) {
				if (value.contains("charset")) {
					charset = value.replace("charset", "").replace("=", "")
							.trim();
				}
			}
		}
		return charset;
	}

}
