package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_16 : Removing the "?" when the query is empty
 * 
 * @author junma
 *
 */
public class QuestionMarkNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		//already done during instancing URLWarpper
	}

}
