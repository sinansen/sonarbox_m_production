package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;


/**
 * url_norm_3 : Decoding percent-encoded octets of unreserved characters
 *
 * @author junma
 *
 */
public class EncodedOctetNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {

		url.path = url.path.replace("%7E", "~").replace(" ", "%20");

		
	}


}
