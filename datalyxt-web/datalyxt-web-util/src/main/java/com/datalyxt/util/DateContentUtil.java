package com.datalyxt.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.datalyxt.production.exception.webscraper.TimeBlockException;

public class DateContentUtil {

	public static final String seperator = "/";

	private String format(String date) {
		String formated = date.trim();
		if (date.contains("."))
			formated = formated.replaceAll("\\.", seperator);
		return formated;
	}

	private List<SimpleDateFormat> formats = new ArrayList<SimpleDateFormat>();

	public DateContentUtil() {

		formats.add(new SimpleDateFormat("dd/MM/yy", Locale.GERMAN));
		formats.add(new SimpleDateFormat("dd/MMMM/yy", Locale.GERMAN));
		formats.add(new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH));
		formats.add(new SimpleDateFormat("dd/MMMM/yy", Locale.ENGLISH));
	}

	public Date parseDate(String date) throws TimeBlockException {
		String formatted = format(date);
		for (SimpleDateFormat simpleDateFormat : formats) {
			try {
				Date parsedDate = simpleDateFormat.parse(formatted);
				return parsedDate;
			} catch (Exception e) {
				;
			}
		}
		throw new TimeBlockException("can not parse date string :" + date);

	}
}
