package com.datalyxt.util;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Vector;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;

import com.datalyxt.production.webscraper.model.paging.PagingElement;
import com.datalyxt.production.webscraper.model.paging.PagingHrefType;
import com.datalyxt.production.webscraper.model.paging.ResultTarget;
import com.datalyxt.webscraper.model.BrowserDriverType;



public class PagingExtractorBeta {
	public static void main(String args[]){
		new PagingExtractorBeta().start();
	}
	public void start(){
		
		//SUPERVISION
		/**Wie sieht das Paging aus, damit keine irrelevanten Ergebnisse extrahiert werden. Daher muss auch für das Pagin ein Template generiert werden.
		*/
		String url="http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank.html";
		
		String pagingSelector="#main > div > div:nth-child(2) > p:nth-child(10) > span:nth-child(2) > span";
		url="http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";
		pagingSelector="#tabs > ul";
//		url="http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
//		pagingSelector="#searchResultIndex";

		int index=10000;
		int counter=0;
		String nextLink=url;
		boolean done=false;
		
		do{
			nextLink=CleanSession.cleanSession(nextLink);
			String pageSource=connect(nextLink);
			System.out.println("Visit "+nextLink);

			ArrayList<PagingElement> links = getPagingLinks(nextLink,GetHost.guessBaseUrl(nextLink),GetHost.getParameterFree(nextLink),pagingSelector,pageSource);
			visited.add(nextLink);
			
			done=true;
			for(PagingElement link: links){
				if(!visited.contains(link.href)){
					nextLink=link.href;
					done=false;
					break;
				}
			}
			counter++;
		}while(counter<=index&&!done);
		
		
		driverClose();
	}
	
	Vector<String> visited = new Vector<String>();
	
	public ArrayList<PagingElement> getPagingLinks(String url,String baseUri,String parameterFreeUrl,String pagingSelector,String pageSource){
		System.out.println("Paging Selector = "+pagingSelector);
 
		Document doc = Jsoup.parse(pageSource);
		
		if(doc.baseUri()==null||doc.baseUri().trim().isEmpty())
			doc.setBaseUri(baseUri);
		
		Element pagingArea = doc.select(pagingSelector).first();
		if(pagingArea == null)
			System.out.println("!!!!! null !!!!!");
		Elements links = pagingArea.getElementsByTag("a");
		
		String aTag="";

		String anchorFreeUrl=GetHost.getAnchorFree(url);
		ArrayList<PagingElement> pe = new ArrayList<PagingElement>();
		
		for(Element link: links){
			PagingElement p=new PagingElement();
			
			aTag="";
			aTag=link.attr("href").trim();
			p.hrefDescription=link.text();
			
			if(!aTag.isEmpty()){ 
				if(aTag.startsWith("#")){
					aTag=anchorFreeUrl+aTag;
					
				}else{
					aTag=link.absUrl("href").trim();
				}
			}else{
				//ERROR
			}
			aTag=CleanSession.cleanSession(aTag);
			p.href=aTag;
			p.parameterFree=GetHost.getParameterFree(aTag);
			if(GetHost.getParameterFree(aTag).equals(parameterFreeUrl)){
				p.rt=ResultTarget.InSamePage;
				if(aTag.contains("#")){
					String anchorId=aTag.substring(aTag.indexOf("#"),aTag.length());
					p.pht=PagingHrefType.Anchor;
					p.anchorId=anchorId;
				}else{
					p.pht=PagingHrefType.Regular;
				}
			}else{
				p.rt=ResultTarget.InNewPage;
				if(aTag.contains("#")){
					String anchorId=aTag.substring(aTag.indexOf("#"),aTag.length());
					p.anchorId=anchorId;
					p.pht=PagingHrefType.Anchor;
				}else{
					p.pht=PagingHrefType.Regular;
				}
			}
			pe.add(p);
		}
		for(PagingElement p: pe){
			System.out.println("****************************");
			System.out.println("HREF\t:"+p.href);
			System.out.println("PFREE\t:"+p.parameterFree);
			System.out.println("ANC-ID\t:"+p.anchorId);
			System.out.println("HREF-T\t:"+p.pht);
			System.out.println("RES-Tar\t:"+p.rt);
		}
		return pe;
	}
	WebDriver driver = WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
	
	public void driverClose(){
		driver.close();
	}
	public String connect(String url){
		driver.get(url);
		String pageSource=driver.getPageSource();
		return pageSource;
	}
	 public static void pressKey() {
			System.out.println("Press a key...");
			try {
				Scanner scan = new Scanner(System.in);
				scan.nextLine();

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	}
}


