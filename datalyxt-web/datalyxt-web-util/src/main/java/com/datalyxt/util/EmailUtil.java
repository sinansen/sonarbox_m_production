package com.datalyxt.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailUtil {
	public static boolean isValidEmail(String eMail) {
		if (eMail == null)
			return false;

		String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(eMail);
		return matcher.matches();

	}
}
