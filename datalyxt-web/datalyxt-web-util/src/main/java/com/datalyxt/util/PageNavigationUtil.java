package com.datalyxt.util;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.webmodel.user.BrowsingConfig;

public class PageNavigationUtil {
	public synchronized void navigateToPage(WebDriver driver, BrowsingConfig browsingConfig,
			String pageURL) throws OpenPageUrlException {
		try {
			if (browsingConfig == null)
				browsingConfig = new BrowsingConfig();
			driver.manage()
					.timeouts()
					.pageLoadTimeout(browsingConfig.pageLoadTimeout,
							TimeUnit.MILLISECONDS);
			
 
			
			driver.get(pageURL);
		} catch (Exception e) {
			throw new OpenPageUrlException("url: " + pageURL + " "
					+ e.getMessage(), e);
		}
	}
	public int checkStatus(String pageURL) throws OpenPageUrlException{
		int timeout = 5;
		RequestConfig config = RequestConfig.custom()
		  .setConnectTimeout(timeout * 1000)
		  .setConnectionRequestTimeout(timeout * 1000)
		  .setSocketTimeout(timeout * 1000).build();
		CloseableHttpClient client = 
		  HttpClientBuilder.create().setDefaultRequestConfig(config).build();
		
		HttpGet request = new HttpGet(pageURL);
		int hardTimeout = 5; // seconds
		TimerTask task = new TimerTask() {
		    @Override
		    public void run() {
		        if (request != null) {
		        	request.abort();
		        }
		    }
		};
		new Timer(true).schedule(task, hardTimeout * 1000);
		HttpResponse response;
		int statuscode = 0;
		try {
			response = client.execute(request);
			statuscode = response.getStatusLine().getStatusCode();
		} catch (Exception e) {
			System.out.println(e.toString());
			throw new OpenPageUrlException("open url: " + pageURL + " "
					+ " failed: "+e.toString());
		}
		
		if(statuscode != 200)
			throw new OpenPageUrlException("statuscode "+statuscode);
		return statuscode;
	}
}
