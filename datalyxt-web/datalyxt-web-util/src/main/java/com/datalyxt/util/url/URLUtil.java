package com.datalyxt.util.url;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import com.datalyxt.exception.extraction.CreateURLWrapperException;
import com.datalyxt.webscraper.model.link.URLWrapper;

public class URLUtil {
	/**
	 * Takes a query string, separates the constituent name-value pairs, and
	 * stores them in a SortedMap ordered by lexicographical order.
	 *
	 * @return Null if there is no query string.
	 */
	public static SortedMap<String, String> createParameterMap(
			final String queryString) {
		if (queryString == null || queryString.isEmpty()) {
			return null;
		}

		final String[] pairs = queryString.split("&");
		final Map<String, String> params = new HashMap<>(pairs.length);

		for (final String pair : pairs) {
			if (pair.length() == 0) {
				continue;
			}

			String[] tokens = pair.split("=", 2);
			switch (tokens.length) {
			case 1:
				if (pair.charAt(0) == '=') {
					params.put("", tokens[0]);
				} else {
					params.put(tokens[0], "");
				}
				break;
			case 2:
				params.put(tokens[0], tokens[1]);
				break;
			}
		}
		return new TreeMap<>(params);
	}

	public static URLWrapper createURLWrapper(String originalUrl)
			throws CreateURLWrapperException {
		URL url = null;
		try {
			url = new URL(originalUrl);
			URLWrapper urlWrapper = new URLWrapper(originalUrl,
					url.getProtocol(), url.getHost(), url.getPort(),
					url.getDefaultPort(), url.getPath(), url.getQuery());
			return urlWrapper;
		} catch (MalformedURLException e) {
			throw new CreateURLWrapperException(e);
		}

	}
}
