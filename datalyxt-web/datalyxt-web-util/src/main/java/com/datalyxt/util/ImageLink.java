package com.datalyxt.util;

public class ImageLink {
	public static boolean isImageLink(String url) {
		if (url.endsWith(".png"))
			return true;
		if (url.endsWith(".jpg"))
			return true;
		if(url.endsWith(".gif"))
			return true;
		return false;
	}
}
