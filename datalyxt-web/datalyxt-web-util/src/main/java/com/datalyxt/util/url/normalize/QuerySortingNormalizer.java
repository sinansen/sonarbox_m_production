package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.util.url.URLUtil;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_13 : Sorting the query parameters
 * 
 * @author junma
 *
 */
public class QuerySortingNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		if (url.sortedParamMap == null)
			url.sortedParamMap = URLUtil.createParameterMap(url.query);
	}

}
