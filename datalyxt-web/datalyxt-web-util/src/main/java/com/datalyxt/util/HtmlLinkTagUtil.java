package com.datalyxt.util;

import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;

import com.datalyxt.webscraper.model.link.LinkType;

public class HtmlLinkTagUtil {
	public static LinkType getType(Element element) {
		
		if (element.tagName().equals("img"))
			return LinkType.image;
		else if (element.tagName().equals("iframe"))
			return  LinkType.iframeSource;
		else if (element.tagName().equals("link")) {
			Attributes attributes = element.attributes();
			for (Attribute attribute : attributes) {
				String key = attribute.getKey();
				if(key.equals("type")){
					String value = attribute.getValue();
					if(value!=null){
						if(value.equals("text/css"))
							return LinkType.css;
						if(value.equals("text/javascript"))
							return LinkType.jsfile;
					}
				
				}
				if(key.equals("rel")){
					String value = attribute.getValue();
					if(value!=null){
						if(value.equals("stylesheet"))
							return LinkType.css;
			
					}
				}
			}
		} else if (element.tagName().equals("script")) {
				return (LinkType.jsfile);
		}
		return null;
	}
}
