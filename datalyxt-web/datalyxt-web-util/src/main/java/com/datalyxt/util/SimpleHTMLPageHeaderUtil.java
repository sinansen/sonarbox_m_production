/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datalyxt.util;

import java.io.IOException;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.html.HTMLPageHeaderValidateException;
import com.datalyxt.util.url.URLNormalizer;
import com.datalyxt.webscraper.model.page.SimpleHttpHeader;

public class SimpleHTMLPageHeaderUtil {

	protected static final Logger logger = LoggerFactory
			.getLogger(SimpleHTMLPageHeaderUtil.class);

	protected RequestConfig requestConfig;
	RegistryBuilder<ConnectionSocketFactory> connRegistryBuilder;
	public static String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.16 Safari/537.36";

	public SimpleHTMLPageHeaderUtil(int socketTimeout, int connectionTimeout) {
		requestConfig = RequestConfig.custom().setExpectContinueEnabled(false)
				.setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
				.setRedirectsEnabled(false).setSocketTimeout(socketTimeout)
				.setConnectTimeout(connectionTimeout).build();

		connRegistryBuilder = RegistryBuilder.create();
		connRegistryBuilder.register("http",
				PlainConnectionSocketFactory.INSTANCE);
		try { // Fixing:
				// https://code.google.com/p/crawler4j/issues/detail?id=174
			// By always trusting the ssl certificate
			SSLContext sslContext = SSLContexts.custom()
					.loadTrustMaterial(null, new TrustStrategy() {
						@Override
						public boolean isTrusted(final X509Certificate[] chain,
								String authType) {
							return true;
						}
					}).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
					sslContext, NoopHostnameVerifier.INSTANCE);

			connRegistryBuilder.register("https", sslsf);
		} catch (Exception e) {
			logger.warn("Exception thrown while trying to register https");
			logger.debug("Stacktrace", e);
		}

	}

	public SimpleHttpHeader fetchPageHeader(String url)
			throws HTMLPageHeaderValidateException {
		SimpleHttpHeader simpleHttpHeader = new SimpleHttpHeader();

		String toFetchURL = url;
		CloseableHttpClient httpClient = null;
		HttpGet get = null;
		try {

			Registry<ConnectionSocketFactory> connRegistry = connRegistryBuilder
					.build();
			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					connRegistry);

			HttpClientBuilder clientBuilder = HttpClientBuilder.create();
			clientBuilder.setDefaultRequestConfig(requestConfig);
			clientBuilder.setConnectionManager(connectionManager);
			clientBuilder.setUserAgent(USER_AGENT);

			httpClient = clientBuilder.build();
			get = null;

			get = new HttpGet(toFetchURL);

			HttpResponse response;
			response = httpClient.execute(get);

			simpleHttpHeader.statusCode = response.getStatusLine()
					.getStatusCode();

			Header header = response.getFirstHeader("Location");
			if (header != null) {
				simpleHttpHeader.isRedirect = true;
				simpleHttpHeader.movedToUrl = URLNormalizer.getNormalizedURL(
						header.getValue(), toFetchURL);
			}

			return simpleHttpHeader;
		} catch (Exception e) {
			if (e instanceof UnknownHostException) {
				simpleHttpHeader.statusCode = -2;
				return simpleHttpHeader;
			}
			throw new HTMLPageHeaderValidateException(
					"can't check page header " + toFetchURL + "  error: "
							+ e.getMessage(), e);
		} finally {
			if (get != null)
				get.abort();
			if (httpClient != null)
				try {
					httpClient.close();
				} catch (IOException e) {
					throw new HTMLPageHeaderValidateException(
							"can't close httpclient when checking page header "
									+ toFetchURL + "  error: " + e.getMessage(),
							e);
				}
		}

	}

}