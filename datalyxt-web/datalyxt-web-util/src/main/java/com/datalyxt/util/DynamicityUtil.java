package com.datalyxt.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import com.datalyxt.webscraper.model.Dynamicity;
import com.datalyxt.webscraper.model.LinkParameter;

public class DynamicityUtil {
	

	public Dynamicity checkDynamicity(String url,  HashMap<String, LinkParameter> linkFirstRun, HashMap<String, LinkParameter> linkSecondRun) {
		Dynamicity dynamicty = new Dynamicity();

		HashSet<String> parameterV1 = new HashSet<String>();
		HashSet<String> parameterV2 = new HashSet<String>();

		 HashSet<String> newLink = new HashSet<String>();
		 HashSet<String> addedParameter = new HashSet<String>();
		 HashSet<String> removedParameter = new HashSet<String>();
		
		for (String s : linkFirstRun.keySet()) {
			for (String lp : linkFirstRun.get(s).parameterValue.keySet()) {
				parameterV1.add(lp + "="
						+ linkFirstRun.get(s).parameterValue.get(lp));
			}
		}

		for (String s : linkSecondRun.keySet()) {
			if (!linkFirstRun.containsKey(s)) {
				newLink.add(s);
			}
			for (String lp : linkSecondRun.get(s).parameterValue.keySet()) {
				parameterV2.add(lp + "="
						+ linkSecondRun.get(s).parameterValue.get(lp));
			}
		}

		for (String s : parameterV2) {
			if (!parameterV1.contains(s)) {
				addedParameter.add(s);
			}
		}

		for (String s : parameterV1) {
			if (!parameterV2.contains(s)) {
				removedParameter.add(s);
			}
		}
		dynamicty.dynamicity = (double) newLink.size() / linkSecondRun.size();

		for (String s : addedParameter) {
			String par = getParameterName(s);
			dynamicty.dynamicParameters.add(par);
		}

		for (String s : removedParameter) {
			String par = getParameterName(s);
			dynamicty.dynamicParameters.add(par);
		}
		return dynamicty;
	}

	private static String getParameterName(String value) {
		if (value == null)
			return "";
		String[] parts = value.split("=");
		if (parts.length != 2)
			return "";
		return parts[0];
	}

	public LinkParameter seperateLinkParameter(LinkParameter param) {

		String token = "";
		StringTokenizer st = null;
		StringTokenizer stparameter = null;
		String v = "", p = "";

		String[] result = param.url.split("\\&", 2);

		if (result.length == 2) {

			st = new StringTokenizer(result[1], sep);
			param.baseParameterUrl = result[0];
			param.parameterExtension = result[1];

			while (st.hasMoreTokens()) {

				token = st.nextToken();
				// TODO not more than 1x= failure otherwise
				if (token.contains("=")) {
					stparameter = new StringTokenizer(token, "=");

					if (stparameter.countTokens() == 2) {
						p = stparameter.nextToken();
						v = stparameter.nextToken();

					} else if (stparameter.countTokens() == 1) {
						p = stparameter.nextToken();
						v = "";
					}
					param.parameterValue.put(p, v);
				} else {
					System.out.println("NO TOKEN");
				}
			}
		}
		return param;
	}

	public final static String sep = "&";

	public HashMap<String, LinkParameter> getLinkSet(Set<String> links) {
		HashMap<String, LinkParameter> urlLinkObject = new HashMap<String, LinkParameter>();

		for (String link : links) {
			LinkParameter lp = new LinkParameter();
			link = link.replace(";", sep).replace("&", sep).replace("?", sep);
			lp.url = link;
			seperateLinkParameter(lp);
			urlLinkObject.put(link, lp);
		}
		return urlLinkObject;
	}

}

