package com.datalyxt.util;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidLink {
	public static boolean validLink(String url) {
		String cleanUrl = url;
		ArrayList<String> delim = new ArrayList<String>();
		delim.add("/");
		StringTokenizer token;
		String tok = "";

		// if(url.contains("grundsaetze/s1246")){
		// pressButton();
		// }

		for (String delimiter : delim) {

			token = new StringTokenizer(cleanUrl, delimiter);
			while (token.hasMoreElements()) {
				tok = (String) token.nextElement();
			}
		}
		if (url.contains("lidl.de/")) {
			String regex = "(c|p|b)[0-9]{1,9}";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(tok);

			if (matcher.find() || url.contains("/filialen/")
					|| url.contains("/filiale/") || url.contains("karriere")
					|| url.contains("onlineshop")
					|| url.contains("question-id") || url.contains("search?")) {
				return false;
			} else {
				return true;
			}
		} else if (url.contains("rewe.de/")) {
			if (url.contains("foto.rewe.de") || url.contains("shop.rewe.de")
					|| url.contains("/produkte/")
					|| url.contains("/zutaten-a-z/")
					|| url.contains("/eigenmarken") || url.contains("/rezept")
					|| url.contains("unsere-marken")
					|| url.contains("/services/")) {
				return false;
			} else {
				return true;
			}
		} else if (url.contains("ernstings-family.de")) {
			String regex = "[,]{4}[0-9][1-100]";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(tok);

			if (matcher.find() || url.contains("/kat/") || url.contains(",,,,")) {
				return false;
			} else {
				return true;
			}

		} else if (url.contains("mann-hummel.de")) {

			if (!url.contains("mann-hummel.com/de/")) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public static boolean isEmptyURL(String url) {
		if (url == null || url.trim().isEmpty())
			return true;
		return false;
	}

	public static boolean isURLLengthValid(String url, int maxLength) {
		if (url.length() > maxLength)
			return false;
		return true;
	}

	public static boolean isHttp(String url) {
		if (isEmptyURL(url))
			return false;

		int lastIndex = -1;
		int count = 0;
		for (count = 0; count < 3; count++) {
			if (lastIndex < url.length()) {
				lastIndex = url.indexOf("/", lastIndex + 1);
				if (lastIndex < 0)
					break;
			} else {
				lastIndex = -1;
				break;
			}
		}
		if (count == 3 && lastIndex > 0)
			url = url.substring(0, lastIndex);

		Pattern p = Pattern.compile("(http:\\/\\/|https:\\/\\/)(www.)?");

		Matcher m = p.matcher(url);
		if (m.find()) {
			int index = url.lastIndexOf(".");
			if (index > 0) {
				String suffix = url.substring(index);
				if (suffix.length() > 10 || suffix.length() < 3)
					return false;
				return true;
			}
		}
		return false;
	}
}
