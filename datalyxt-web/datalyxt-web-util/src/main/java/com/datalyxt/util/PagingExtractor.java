package com.datalyxt.util;

import java.util.ArrayList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.webscraper.model.paging.PagingElement;
import com.datalyxt.production.webscraper.model.paging.PagingHrefType;
import com.datalyxt.production.webscraper.model.paging.ResultTarget;
import com.datalyxt.util.url.URLNormalizer;

public class PagingExtractor {

	public ArrayList<PagingElement> getPagingLinks(String url, Document doc,
			String pagingSelector) throws PagingExtractionException {

		try {
			Element pagingArea = doc.select(pagingSelector).first();
			if (pagingArea == null)
				throw new PagingExtractionException(
						"pageblock selector is incorrect. url: " + url
								+ "  selector: " + pagingSelector);
			Elements links = pagingArea.getElementsByTag("a");

			String aTag = "";

			ArrayList<PagingElement> pe = new ArrayList<PagingElement>();
			String anchorFreeUrl = GetHost.getAnchorFree(url);
			String parameterFreeUrl = GetHost.getParameterFree(url);
			for (Element link : links) {
				PagingElement p = new PagingElement();

				aTag = "";
				aTag = link.attr("href").trim();
				p.hrefDescription = link.text();
				p.selector = link.cssSelector();
				
				if (!aTag.isEmpty()) {
					if (aTag.startsWith("#")) {
						aTag = anchorFreeUrl + aTag;

					} else {
						aTag = link.absUrl("href").trim();
					}

					aTag = CleanSession.cleanSession(aTag);
					p.href = aTag;
					p.parameterFree = GetHost.getParameterFree(aTag);
					if (p.parameterFree.equals(parameterFreeUrl)) {
						p.rt = ResultTarget.InSamePage;
						if (aTag.contains("#")) {
							String anchorId = aTag.substring(aTag.indexOf("#"),
									aTag.length());
							p.pht = PagingHrefType.Anchor;
							p.anchorId = anchorId;
						} else {
							p.pht = PagingHrefType.Regular;
						}
					} else {
						p.rt = ResultTarget.InNewPage;
						if (aTag.contains("#")) {
							String anchorId = aTag.substring(aTag.indexOf("#"),
									aTag.length());
							p.anchorId = anchorId;
							p.pht = PagingHrefType.Anchor;
						} else {
							p.pht = PagingHrefType.Regular;
						}
					}
//					p.href = URLNormalizer.getNormalizedURL(p.href);
					pe.add(p);
				}
			}

			return pe;
		} catch (Exception e) {
			throw new PagingExtractionException(
					"paging extraction failed. url: " + url + "  selector: "
							+ pagingSelector, e);
		}
	}

}
