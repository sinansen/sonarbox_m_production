package com.datalyxt.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;

import com.datalyxt.webscraper.model.BrowserDriverType;

class DataColumn{
	public String url;
	public HashMap<Integer,String> cssSelectorColumn = new HashMap<Integer,String>(); //Integer = Position, String = Diff : Replace String through EleStyle?
	public String tagName;
	public String dataType;
	public Vector<String> refTokens = new Vector<String>();
	
	public HashMap<String,Vector<String>> selectorMap = new HashMap<String,Vector<String>>();
	public String refSelector; 
	
	private String delim=">";
	
	private int tokenSize=0;
	public void addSelectorInstance(String cssSelector){
		cssSelector=cssSelector.replace(" ", "");
		if(selectorMap.containsKey(cssSelector)){
			System.out.println("Selektor existiert bereits");
		}else{
			Vector <String> tok = new Vector<String>();
			 StringTokenizer st = new StringTokenizer(cssSelector,delim);
		     while (st.hasMoreTokens()) {
		    	 tok.add(st.nextToken());
		     }
		    
		     boolean add=false;
	    	 if(tokenSize==0){
	    		 tokenSize=tok.size();
	    		 add=true;
	    	 }else{
	    		 if(tok.size()!=tokenSize){
	    			 System.out.println(" Unterschiedliche Selektoren ");
	    		 }else{
	    			 add=true;
	    		 }
	    	 }

	    	 if(add){
	    		 System.out.println("Selektor hinzugefügt");
			     selectorMap.put(cssSelector,tok);
			     checkCssSelector(cssSelector,tok,true);
	    	 }
		}
	}
	
	public boolean checkCssSelector(String cssSelector,Vector<String> tok,boolean update){
		
		
		int position;
		int changeTracking=0;
		int maxChangeSize=1;
		boolean valid=false;
		if(update){
			refTokens.clear();
			for(String t: tok){
				    refTokens.add(t);
			}
		}
		for(String value: selectorMap.keySet()){
			position=0;
			changeTracking=0;
			
			for(String t: tok){
				if(!selectorMap.get(value).contains(t)){
					changeTracking++;
					if(changeTracking<=maxChangeSize){

						if(update){
							System.out.println("ADD "+position +" - "+t);
							cssSelectorColumn.put(position, t);
							refTokens.remove(t);
						}
						valid= true;
						break;
					}else{
						valid=false;
//						System.out.println("Eine Änderung pro Datenspaltenelement ist erlaubt");
						break;
					}
				}
				position++;
			}
		}
		return valid;
	}

	public boolean validateCss(String cssSelector){
		cssSelector=cssSelector.replace(" ", "");
		String regex = "\\(\\d*\\)";
		StringTokenizer st = new StringTokenizer(cssSelector,delim);
		Vector<String> tok= new Vector<String>();
		int index=0;
		
		while (st.hasMoreTokens()) {
		 index++;
		 tok.add(st.nextToken());
	    }
		
		if(index!=tokenSize){
		 return false;
		}else{
			
		
		
		 if(checkCssSelector(cssSelector,tok,false)){
			 for(Integer i: cssSelectorColumn.keySet()){
				 
				 if(tok.containsAll(refTokens)){
					if(!(tok.get(i).equals(cssSelectorColumn.get(i)))&&(tok.get(i).replaceAll(regex, "").equals((cssSelectorColumn.get(i).replaceAll(regex,""))))){
						return true;
					}
				 }
			 }
		 }
		}
		
		return false;
	}
}
public class MultiRowDetection {
	public static void main(String args[]){
		new MultiRowDetection().start();
	}
	
	
		
	Vector<String> result = new Vector<String>();
	public void start(){
		driver= WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
		result.clear();
		String url="http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
		String pageSource=connect(url);
		//LEARNING
		DataColumn dc = new DataColumn();
		String cssSelector1="#searchResult > li:nth-child(1) > div.resource-col > span > a";
		Element element = getElement(url,cssSelector1,pageSource);
		cssSelector1=element.cssSelector();
		String tagName=element.tagName();
		dc.addSelectorInstance(cssSelector1);
		
		cssSelector1="#searchResult > li:nth-child(2) > div.resource-col > span > a";
		element = getElement(url,cssSelector1,pageSource);
		cssSelector1=element.cssSelector();

		dc.addSelectorInstance(cssSelector1);
		
		
//		String dataType="date";
//		String dimension="td.nth-child()";
//		url="http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
//		cssSelector1="#searchResult > li:nth-child(1) > p:nth-child(1)";
//		tagName="p";
//		cssSelector1="#searchResult > li:nth-child(1) > div.resource-col > span > a";
//		tagName="a";
		
		boolean multirow=true;
		
//		url="http://www.spiegel.de";
//		cssSelector="#js-column-dynamic-ref > div:nth-child(2) > div > h2 > a > span.headline";
//		tagName="span";
		
//		url="http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";
//		cssSelector="#tabs-2016 > table > tbody > tr > td:nth-child(1) > ul > li:nth-child(1) > span > a";
//		tagName="a";
//		
//		url="http://www.cleankids.de/magazin/produktrueckrufe/ruckrufe-lebensmittel";
//		cssSelector="#post-59783 > div.post_content > h2 > a";
//		tagName="a";

		
		String prevTag="";
		String prevCss="";
		String nextTag="";
		String nextCss="";
		
		String parentTag="";
		String parentCss="";
		
		cssSelector1="#searchResult > li:nth-child(1) > div.resource-col > span > a";
		element = getElement(url,cssSelector1,pageSource);
		cssSelector1=element.cssSelector();
		
		Elements elem=getElements(url,pageSource,tagName);
		for(Element el: elem){
			if(dc.validateCss(el.cssSelector())){
				System.out.println("- "+el.text());
			}
		}
		driver.close();
		System.exit(-1);
		checkSelector(cssSelector1,element);
		
		
		boolean prevElement=false,nextElement=false,parentElement=false;
		if(element.previousElementSibling()!=null){
			prevTag = element.previousElementSibling().tagName();
			prevCss = element.previousElementSibling().cssSelector();
			prevElement=true;
		}
		if(element.nextElementSibling()!=null){
			nextTag = element.nextElementSibling().tagName();
			nextCss = element.nextElementSibling().cssSelector();
			nextElement=true;
		}
		
		if(element.parent()!=null){
			parentTag = element.parent().tagName();
			parentCss = element.parent().cssSelector();
			parentElement=true;
		}
		
		boolean done=false;
		if(multirow){
			Elements e=getElements(url,pageSource,tagName);
			int contextFound=0;
			int totalContext=0;
			int contextDiff=0;

			for(Element ele: e){
				contextFound=0;
				totalContext=0;
				contextDiff=0;
				if(prevElement){
					totalContext++;
					if(ele.previousElementSibling()!=null)
						if(prevTag.equals(ele.previousElementSibling().tagName())){
//							System.out.println("Same prev sibling");
							contextFound++;
						}
				}else{
					if(ele.previousElementSibling()!=null){
						contextDiff++;
//						System.out.println("Diff found");
					}
				}
				if(nextElement){
					totalContext++;
					if(ele.nextElementSibling()!=null)
						if(nextTag.equals(ele.nextElementSibling().tagName())){
//							System.out.println("Same next sibling");
							contextFound++;
						}
				}else{
					if(ele.nextElementSibling()!=null){
						contextDiff++;
//						System.out.println("Diff found");
					}
				}
				if(parentElement){
					totalContext++;
					if(ele.parent()!=null)
						if(parentTag.equals(ele.parent().tagName())){
//							System.out.println("Same parent");
							contextFound++;
						}
				}else{
					if(ele.parent()!=null){
						contextDiff++;
//						System.out.println("Diff found");
					}
				}
				//get candidates based on structure
				//filter candidates based on dimension selection
				//filter candidates based on format
				//filter candidates based on distance
				//filter candidates based on termination criteria
				//lastelement, firstelemtn, midelement handling. Es kann sein, dass die Struktur 
				//des ersten Elementes von dem restlichen unterschiedlich ist. Dies gilt auch für die mittleren und das letzte Element.
				
				if(totalContext!=0&&contextFound!=0){
					if((totalContext/contextFound)==1&&contextDiff==0){
						//checkFormat
//						System.out.println(totalContext +" - "+contextFound);
//
						System.out.println(ele.cssSelector());
						System.out.println(cssSelector1);
						if(ele.cssSelector().equals(cssSelector1)){
							System.out.println("CSS FOUND");
						}
						result.add(ele.text());
						
				
//						if(nextRowElement(ele.cssSelector(),cssSelector1)){
//							result.add(ele.text());
//						}
//						if(ele.cssSelector().replaceAll("\\(\\d*\\)", "").equals(cssSelector.replaceAll("\\(\\d*\\)", ""))){
//							if(this.determineDateFormat(ele.text())!=null){
//								result.add(ele.text());
//							}
//						}
						//checkCssSimilarity
						
					}
				}
			}
			/*sibling
			children
			sibling
			children
			</html>
			</body>
			*/
		}
		for(String s: result){
			System.out.println("V = "+s);
		}
		driverClose();
	}
	public void checkSelector(String selector, Element element){
		//List
		//Table
		
		String regex = "\\(\\d*\\)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(selector);
		HashMap<Integer,Integer> matchingRes=new HashMap<Integer,Integer>();
		
		while (matcher.find()) {
			 System.out.print("Start index: " + matcher.start()+" ");
			 System.out.print(" End index: " + matcher.end()+" ");
			 matchingRes.put(matcher.start(), matcher.end());
			 System.out.println(matcher.group());
		}
		System.out.println(selector);
		if(matchingRes.size()>1){
			System.out.println("It's a kind of Table");
		}else if(matchingRes.size()==1){
			System.out.println("It's a kind of List");
		}else{
			System.out.println("It's a reqular element");
		}
		pressKey();
		
	}
	public Elements getElements(String baseUri,String pageSource,String tagName){
		Document doc = Jsoup.parse(pageSource);
		if(doc.baseUri()==null||doc.baseUri().trim().isEmpty())
			doc.setBaseUri(baseUri);
		
		Elements el = doc.select(tagName);
		//Formatcheck - new vs. initial format
		return el;
	}
	
	
	public boolean nextRowElement(String refCssSelector, String curCssSelector){
		//column similarity row similarity

		String regex = "\\(\\d*\\)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(refCssSelector);
		HashMap<Integer,Integer> ref=new HashMap<Integer,Integer>();
		HashMap<Integer,Integer> cur=new HashMap<Integer,Integer>();
		String tmp1="",tmp2="";
		
		while (matcher.find()) {
		
			 System.out.print("Start index: " + matcher.start()+" ");
			 System.out.print(" End index: " + matcher.end()+" ");
			 ref.put(matcher.start(), matcher.end());
			 System.out.println(matcher.group());
		}
		
		
		matcher = pattern.matcher(curCssSelector);
		
		while (matcher.find()) {
			 System.out.print("Start index: " + matcher.start()+" ");
			 System.out.print(" End index: " + matcher.end()+" ");
			 System.out.println(matcher.group());
			 cur.put(matcher.start(), matcher.end());
		}
		
		
		if(ref.size()>=2&&cur.size()>=2&&ref.size()==cur.size()){
			int counter=ref.size()-1;
			int iterator=0;
			int start=0,end=0;
			for(int i: ref.keySet()){
				iterator++;
				
				if(iterator==counter){
					start=i;
					end=ref.get(i);
					break;
				}
			}
			tmp1 = refCssSelector.substring(0,start)+"()"+refCssSelector.substring(end,refCssSelector.length());

			iterator=0;
			start=0;end=0;
			for(int i: cur.keySet()){
				iterator++;
				
				if(iterator==counter){
					start=i;
					end=cur.get(i);
					break;
				}
			}
			tmp2 = curCssSelector.substring(0,start)+"()"+curCssSelector.substring(end,curCssSelector.length());

		}
		
		if(tmp1!=null&&tmp2!=null&&!tmp1.isEmpty()&&!tmp2.isEmpty()){
			if(tmp1.equals(tmp2)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public boolean nextColumnElement(String refCssSelector, String curCssSelector){
		//column similarity row similarity

		String regex = "\\(\\d*\\)";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(refCssSelector);
		HashMap<Integer,Integer> ref=new HashMap<Integer,Integer>();
		HashMap<Integer,Integer> cur=new HashMap<Integer,Integer>();
		String tmp1="",tmp2="";
		
		
		while (matcher.find()) {
		
			 System.out.print("Start index: " + matcher.start()+" ");
			 System.out.print(" End index: " + matcher.end()+" ");
			 ref.put(matcher.start(), matcher.end());
			 System.out.println(matcher.group());
		}
		
		
		matcher = pattern.matcher(curCssSelector);
		
		while (matcher.find()) {
			 System.out.print("Start index: " + matcher.start()+" ");
			 System.out.print(" End index: " + matcher.end()+" ");
			 System.out.println(matcher.group());
			 cur.put(matcher.start(), matcher.end());
		}
		
		
		if(ref.size()>=2&&cur.size()>=2&&ref.size()==cur.size()){
			int counter=ref.size();
			int iterator=0;
			int start=0,end=0;
			for(int i: ref.keySet()){
				iterator++;
				
				if(iterator==counter){
					start=i;
					end=ref.get(i);
					break;
				}
			}
			tmp1 = refCssSelector.substring(0,start)+"()"+refCssSelector.substring(end,refCssSelector.length());
			System.out.println("TMP "+tmp1);
			iterator=0;
			start=0;end=0;
			for(int i: ref.keySet()){
				iterator++;
				
				if(iterator==counter){
					start=i;
					end=ref.get(i);
					break;
				}
			}
			tmp2 = refCssSelector.substring(0,start)+"()"+refCssSelector.substring(end,refCssSelector.length());
			System.out.println("TMP "+tmp1);
		}
		
		if(tmp1!=null&&tmp2!=null&&!tmp1.isEmpty()&&!tmp2.isEmpty()){
			if(tmp1.equals(tmp2)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public boolean sameFormat(String ref, String cur){
		boolean sameFormat=false;
		
		return sameFormat;
	}
	
	public Element getElement(String baseUri,String pagingSelector,String pageSource){
		Document doc = Jsoup.parse(pageSource);
		if(doc.baseUri()==null||doc.baseUri().trim().isEmpty())
			doc.setBaseUri(baseUri);
		
		Element el = doc.select(pagingSelector).first();
		//Formatcheck - new vs. initial format
		return el;
	}
	WebDriver driver = null; 
	
	public void driverClose(){
		driver.close();
	}
	public String connect(String url){
		HashSet <String> urls= new HashSet<String>();
		driver.get(url);
		String pageSource=driver.getPageSource();
		return pageSource;
	}
	
	public static String determineDateFormat(String dateString) {
        for (String regexp : DATE_FORMAT_REGEXPS.keySet()) {
            if (dateString.toLowerCase().matches(regexp)) {
                return DATE_FORMAT_REGEXPS.get(regexp);
            }
        }
        return null; // Unknown format.
    }
	
	 private static final HashMap<String, String> DATE_FORMAT_REGEXPS = new HashMap<String, String>() {{
	        put("^\\d{8}$", "yyyyMMdd");
	        put("^\\d{1,2}-\\d{1,2}-\\d{4}$", "dd-MM-yyyy");
	        put("^\\d{1,2}.\\d{1,2}.\\d{4}$", "dd.MM.yyyy");
	        put("^\\d{4}-\\d{1,2}-\\d{1,2}$", "yyyy-MM-dd");
	        put("^\\d{1,2}/\\d{1,2}/\\d{4}$", "MM/dd/yyyy");
	        put("^\\d{4}/\\d{1,2}/\\d{1,2}$", "yyyy/MM/dd");
	        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}$", "dd MMM yyyy");
	        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}$", "dd MMMM yyyy");
	        put("^\\d{12}$", "yyyyMMddHHmm");
	        put("^\\d{8}\\s\\d{4}$", "yyyyMMdd HHmm");
	        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}$", "dd-MM-yyyy HH:mm");
	        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy-MM-dd HH:mm");
	        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}$", "MM/dd/yyyy HH:mm");
	        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}$", "yyyy/MM/dd HH:mm");
	        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMM yyyy HH:mm");
	        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}$", "dd MMMM yyyy HH:mm");
	        put("^\\d{14}$", "yyyyMMddHHmmss");
	        put("^\\d{8}\\s\\d{6}$", "yyyyMMdd HHmmss");
	        put("^\\d{1,2}-\\d{1,2}-\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd-MM-yyyy HH:mm:ss");
	        put("^\\d{4}-\\d{1,2}-\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy-MM-dd HH:mm:ss");
	        put("^\\d{1,2}/\\d{1,2}/\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "MM/dd/yyyy HH:mm:ss");
	        put("^\\d{4}/\\d{1,2}/\\d{1,2}\\s\\d{1,2}:\\d{2}:\\d{2}$", "yyyy/MM/dd HH:mm:ss");
	        put("^\\d{1,2}\\s[a-z]{3}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMM yyyy HH:mm:ss");
	        put("^\\d{1,2}\\s[a-z]{4,}\\s\\d{4}\\s\\d{1,2}:\\d{2}:\\d{2}$", "dd MMMM yyyy HH:mm:ss");
	    }};
	    
	    public static void pressKey() {
			System.out.println("Press a key...");
			try {
				Scanner scan = new Scanner(System.in);
				scan.nextLine();

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

}
