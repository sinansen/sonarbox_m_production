package com.datalyxt.util;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetBase {

	public String domain = "http://www.getknit.de/";
	public String currentDirectory = "http://www.getknit.de/a/b/";
	public String url = "http://www.getknit.de/a/b/a.html#test";
	public String hrefValue = "#hallo";

	public String hrefValueTmp = "";
	public String directoryTmp = "";

	// Extern
	// Intern
	// Extern mit JS
	// Intern mit JS

	public String getCalculatedBase(String hrefValue, String directory,
			String domain, String url) {

		if (hrefValue.startsWith("http://") || hrefValue.startsWith("https://")) {
			return hrefValue;
		} else if (hrefValue.startsWith("#")) {
			if (url.contains("#")) {
				url = url.substring(0, url.indexOf("#"));
			}
			return url + hrefValue;

		} else if (hrefValue.startsWith("/")) {
//			System.out.println("Absolute URL found");
			{
				if (domain.endsWith("/")) {
					return domain + hrefValue.substring(1);
				} else {
					return domain + hrefValue;
				}
			}
		} else {
			if (hrefValue.startsWith("./") || !hrefValue.startsWith("../")) {
//				System.out.println("Relative URL v1 found");

				String regex = "\\./";
				Pattern pattern = Pattern.compile(regex); // case insensitive,
															// use [g] for only
															// lower
				Matcher matcher = pattern.matcher(hrefValue);
				int count = 0;
				while (matcher.find()) {
					count++;
				}
				if (count == 0) {
					return directory + hrefValue;
				} else if (count == 1) {
					return directory + hrefValue.replace("./", "");
				} else {
					// ERROR
					System.out.println("ERROR");
					return "";
				}

			} else if (hrefValue.startsWith("../")
					&& !hrefValue.contains("/./")) {
//				System.out.println("Relative URL v2 found");
				String replaced = hrefValue.replace("../", "");
				hrefValueTmp = hrefValue;
				directoryTmp = directory;

				String regex = "\\.\\./";
				Pattern pattern = Pattern.compile(regex); // case insensitive,
															// use [g] for only
															// lower
				Matcher matcher = pattern.matcher(hrefValue);

				int cdCounter = 0;

				while (matcher.find()) {
					cdCounter++;
				}
				System.out.println("cdCounter " + cdCounter);

				if (!domain.endsWith("/")) {
					domain += "/";
				}

				ArrayList<String> dir = new ArrayList<String>();
				String dirOnly = currentDirectory.replace(domain, "");
				StringTokenizer st = new StringTokenizer(dirOnly, "/");

				while (st.hasMoreTokens()) {
					dir.add(st.nextToken());
				}
				String newDir = "";
				if (cdCounter > dir.size()) {
					// ERROR
					System.out.println("ERROR");
					return "";
				} else {
					for (int i = 0; i < dir.size() - cdCounter; i++) {
						newDir += dir.get(i) + "/";
					}
					return domain + newDir + replaced;
				}
			} else {
				System.out.println("ERROR");
				return "";
			}
		}
	}

	public static void main(String args[]) {
		new GetBase().start();
	}

	public void start() {
		System.out.println(getCalculatedBase(hrefValue, currentDirectory,
				domain, url));
	}

}
