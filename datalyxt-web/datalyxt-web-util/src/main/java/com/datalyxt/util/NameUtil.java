package com.datalyxt.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameUtil {
	public static boolean isValidName(String name) {
		if (name == null || name.isEmpty())
			return false;
		String regex = "^[A-Za-z_-]{2,100}$";

		Pattern pattern = Pattern.compile(regex);

		Matcher matcher = pattern.matcher(name);
		return matcher.matches();

	}
}
