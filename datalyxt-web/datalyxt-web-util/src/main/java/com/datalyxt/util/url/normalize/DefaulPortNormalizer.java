package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_4 : Removing the default port
 * 
 * @author junma
 *
 */
public class DefaulPortNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		if (url.port == url.defaultPort) {
			url.port = -1;
		}
	}

}
