package com.datalyxt.util.url.normalize;

import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.util.url.URLUtil;
import com.datalyxt.webscraper.model.link.URLWrapper;

/**
 * url_norm_8 : Removing the fragment
 * 
 * @author junma
 *
 */
public class FragmentNormalizer implements Normalizer {

	@Override
	public void execute(URLWrapper url) throws URLNormalizerException {
		// fragement in path has been removed by java.net.url during creating an
		// instance of URL
		if (url.sortedParamMap == null)
			url.sortedParamMap = URLUtil.createParameterMap(url.query);
		if (url.sortedParamMap != null) {
			for (String key : url.sortedParamMap.keySet()) {
				String value = url.sortedParamMap.get(key);
				if(value.contains("#")){
				     int index = value.indexOf("#");
				     value = value.substring(0, index);
				     url.sortedParamMap.replace(key, value);
				}
			}
		}
	}
}
