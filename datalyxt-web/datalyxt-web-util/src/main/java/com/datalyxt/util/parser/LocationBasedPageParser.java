package com.datalyxt.util.parser;

import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.CheckLinkTypeUtil;
import com.datalyxt.util.CleanSession;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.HtmlLinkTagUtil;
import com.datalyxt.util.ValidLink;
import com.datalyxt.util.url.URLNormalizer;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.link.LinkType;
import com.datalyxt.webscraper.model.page.Page;
import com.datalyxt.webscraper.model.page.PageBlock;

public class LocationBasedPageParser {

	public PageBlock parse(Document doc, Page page, String blockCssPath,
			HashSet<String> dynamicParameters) throws BlockExtractionException,
			DefinedBlockNotFound {
		boolean blockNotFound = false;
		PageBlock pageBlock = new PageBlock();
		try {

			pageBlock.pageUrl = page.pageUrl.url;
			pageBlock.blockCssPath = blockCssPath;

			String domain = page.pageUrl.domain;

			Elements blocks = doc.select(blockCssPath);
			if (blocks != null && !blocks.isEmpty()) {
				Element blockRoot = blocks.first();
				pageBlock.blockHtml = blockRoot.html();
				pageBlock.blockText = blockRoot.text();
				Elements elements = blockRoot.getAllElements();
				for (Element el : elements) {
					pageBlock.blockStructure += el.tagName();
				}
				pageBlock.blockHtmlHash = pageBlock.blockHtml.hashCode();
				pageBlock.blockTextHash = pageBlock.blockText.hashCode();
				pageBlock.blockStructureHash = pageBlock.blockStructure
						.hashCode();
				int pageBlockDepth = page.pageUrl.depth;
				for (Element element : elements) {
					String link = element.absUrl("abs:href").trim();
					if (link == null || link.isEmpty())
						link = element.absUrl("src");

					link = CleanSession.cleanSession(link, dynamicParameters);
					if (link != null && ValidLink.isHttp(link)) {

						link = URLNormalizer.getNormalizedURL(link);

						if (!pageBlock.linkList.containsKey(link)) {
							Link linkObj = new Link();
							linkObj.url = link;
							linkObj.depth = pageBlockDepth + 1;
							linkObj.domain = GetHost.getDomain(link);
							linkObj.linkText = element.text();
							linkObj.parentDepthTable.put(pageBlock.pageUrl,
									pageBlockDepth);

							LinkType ltype = HtmlLinkTagUtil.getType(element);
							if (ltype != null)
								linkObj.linkType.add(ltype);

							CheckLinkTypeUtil.checkLinkType(linkObj, domain);
							pageBlock.linkList.put(linkObj.url, linkObj);
						}

					}
				}

			} else
				blockNotFound = true;

		} catch (Exception e) {
			throw new BlockExtractionException(e.getMessage(), e);
		}

		if (blockNotFound) {
			throw new DefinedBlockNotFound("can't find block by css path. "
					+ page.pageUrl.url + "  " + blockCssPath);
		} else
			return pageBlock;

	}

	public PageBlock parse(Page page, String blockCssPath,
			HashSet<String> dynamicParameters, CleanHtmlDocumentScript script)
			throws DefinedBlockNotFound, BlockExtractionException,
			HtmlDocumentCreationException {

		Document doc = HtmlDocumentUtil.getDocumentFromSource(page.pageUrl.url,
				page.getHtmlSelen(), script);
		return parse(doc, page, blockCssPath, dynamicParameters);
	}

}
