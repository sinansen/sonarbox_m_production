package com.datalyxt.email;


import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class TestJavaMail {
   public static void main(String[] args) {
      // Recipient's email ID needs to be mentioned.
      String to = "jun.ma@datalyxt.com";

      // Sender's email ID needs to be mentioned
      String from = "info@getknit.de";

      final String username = "wp10568800-infoknit";//change accordingly
      final String password = "in77dlyxt";//change accordingly

      // Assuming you are sending email through relay.jangosmtp.net
      String host = "wp247.webpack.hosteurope.de";

      Properties props = new Properties();
      props.put("mail.smtp.auth", "true");
//      props.put("mail.smtp.starttls.enable", "true");
      props.put("mail.smtp.host", host);
      props.put("mail.smtp.port", "587");

      // Get the Session object.
      Session session = Session.getInstance(props,
         new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(username, password);
            }
         });

      try {
         // Create a default MimeMessage object.
         Message message = new MimeMessage(session);

         // Set From: header field of the header.
         message.setFrom(new InternetAddress(from));

         // Set To: header field of the header.
         message.setRecipients(Message.RecipientType.TO,
            InternetAddress.parse(to));

         // Set Subject: header field
         message.setSubject("Testing Subject");

         // Create the message part
         BodyPart messageBodyPart = new MimeBodyPart();

         // Now set the actual message
         messageBodyPart.setText("This is message body");

         // Create a multipar message
         Multipart multipart = new MimeMultipart("mixed");

         // Set text message part


         // first attachment
//         messageBodyPart = new MimeBodyPart();
//         String filename = "C:/tmp/s2.png";
//         DataSource source = new FileDataSource(filename);
//         messageBodyPart.setDataHandler(new DataHandler(source));
//         messageBodyPart.setFileName("s2.png");
         multipart.addBodyPart(messageBodyPart);

         // second attachment
//         messageBodyPart = new MimeBodyPart();
//         filename = "C:/tmp/s2.png";
//         source = new FileDataSource(filename);
//         messageBodyPart.setDataHandler(new DataHandler(source));
//         messageBodyPart.setFileName("s2.png");
//         multipart.addBodyPart(messageBodyPart);

         // Send the complete message parts
         message.setContent(multipart);

         // Send message
         Transport.send(message);

         System.out.println("Sent message successfully....");
  
      } catch (MessagingException e) {
         throw new RuntimeException(e);
      }
   }
}