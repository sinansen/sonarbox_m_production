/**
 * 
 */
package com.datalyxt.emailpool;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.EmailDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.impl.M_FileStorageDAOImpl;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.email.Email;
import com.datalyxt.production.webmodel.email.EmailAction;
import com.datalyxt.production.webmodel.email.EmailStatus;
import com.datalyxt.production.webscraper.model.action.ActionResult;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

/**
 * @author Bijan Fahimi
 * 
 */
public class EmailCron {
	private static Logger log = LoggerFactory.getLogger(EmailCron.class);

	private static final int MAX_NUM_OF_RETRIES = 5;
	private DAOFactory daoFactory;
	boolean isBatchSending = true;
	private SendEmail emailer;
	private EmailWriter emailWriter = new EmailWriter();
	Properties mailserverProps;

	private M_FileStorageDAO fileStorage;

	String senderEmail = "info@datalyxt.com";
	String senderName = "datalyxt";

	public EmailCron(DAOFactory daoFactory, M_FileStorageDAO fileStorage) {
		initialConf();
		emailer = new SendEmail(mailserverProps);
		this.daoFactory = daoFactory;
		this.fileStorage = fileStorage;

	}

	private void initialConf() {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		mailserverProps = new Properties();
		try (InputStream resourceStream = loader
				.getResourceAsStream("mailserver.properties")) {

			mailserverProps.load(resourceStream);
			senderEmail = mailserverProps.getProperty("email_from");
			senderName = mailserverProps.getProperty("sendername");
			log.info("property: email_from " + senderEmail);
			log.info("property: sender " + senderName);
			resourceStream.close();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

	}

	public void createEmails() {

		try {
			List<M_Receiver> receivers = daoFactory.getM_ReceiverDAO()
					.getReciverByStatus(M_ReceiverStatus.activated);

			List<ActionResult> results = daoFactory.getM_QT1V1ResultDAO()
					.getNewResults(100000, false);
			List<EmailAction> emailActions = new ArrayList<>();

			for (ActionResult result : results) {
				Email email = new Email();
				List<Long> textIds = daoFactory.getM_QT1V1ResultDAO()
						.getActionResultTextIdsByResultId(result.id);
				List<Long> fileIds = daoFactory.getM_QT1V1ResultDAO()
						.getActionResultFileIdsByResultId(result.id);
				email.attachementFileIds.addAll(fileIds);
				email.messageIds.addAll(textIds);
				email.created = System.currentTimeMillis();
				email.lastUpdateDate = email.created;
				email.subject = "notification";
				if (!email.messageIds.isEmpty()
						|| !email.attachementFileIds.isEmpty()) {
					daoFactory.getEmailDAO().createEmail(email,
							new HashSet<Long>(), 1L);

					EmailAction emailAction = new EmailAction();
					emailAction.emailId = email.id;

					emailActions.add(emailAction);
				}

			}
			for (EmailAction emailAction : emailActions) {

				for (M_Receiver receiver : receivers) {
					EmailAction e = new EmailAction();
					e.receiverId = receiver.id;
					e.status = EmailStatus.CREATED;
					e.time = System.currentTimeMillis();
					e.emailId = emailAction.emailId;

					daoFactory.getEmailDAO().saveEmailAction(e, 1L);
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	public void sendEmail() {
		try {

			HashMap<Long, List<EmailAction>> emailActions = daoFactory
					.getEmailDAO().getEmailActionsByStatus(EmailStatus.CREATED,
							1L);
			HashMap<Long, List<EmailAction>> erroremailActions = daoFactory
					.getEmailDAO().getEmailActionsByStatus(EmailStatus.ERROR,
							1L);
			emailActions.putAll(erroremailActions);

			if (emailActions.isEmpty())
				return;
			HashMap<Long, String> receivers = new HashMap<>();
			HashMap<String, String> pageDomains = new HashMap<>();
			for (Long emailId : emailActions.keySet()) {
				List<EmailAction> emailactiongroup = emailActions.get(emailId);
				Email email = daoFactory.getEmailDAO()
						.getEmailById(emailId, 1L);
				if (email != null) {
					try {
						EmailEntity emailEntity = new EmailEntity();

						emailEntity.senderEmail = senderEmail;
						emailEntity.senderName = senderName;

						List<ActionResultText> actionResults = daoFactory
								.getM_QT1V1ResultDAO()
								.getActionResultTextByIds(email.messageIds);

						List<ActionResultFile> fileMeta = daoFactory
								.getM_QT1V1ResultDAO()
								.getActionResultFileByIds(
										email.attachementFileIds);

						emailEntity.attachements = fileStorage
								.readFiles(fileMeta);
						HashSet<Long> resultIds = new HashSet<>();
						for (ActionResultText txt : actionResults)
							resultIds.add(txt.resultId);

						for (ActionResultFile file : fileMeta)
							resultIds.add(file.resultId);

						List<String> pageURLs = daoFactory
								.getM_QT1V1ResultDAO().getActionResultUrlsById(
										resultIds);
						List<String> domains = getPageDomins(pageURLs,
								pageDomains);
						emailEntity.subject = String.join(",", domains);

						emailEntity.text = emailWriter.writeEmailText(email,
								senderEmail, actionResults, pageURLs);

						for (EmailAction emailAction : emailactiongroup) {

							String receiverEmail = receivers
									.get(emailAction.receiverId);
							if (receiverEmail == null) {
								M_Receiver receiver = daoFactory
										.getM_ReceiverDAO().getReceiverById(
												emailAction.receiverId, 1L);
								receiverEmail = receiver.email;
								receivers.put(emailAction.receiverId,
										receiverEmail);
							}
							emailEntity.receiverEmail = receiverEmail;

							trySendEmail(email, emailAction, emailEntity);
						}
					} catch (Exception e) {
						log.error("email sending failed. emailId " + emailId
								+ " " + e.getMessage(), e);
					}
				}

			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}

	private List<String> getPageDomins(List<String> pageURLs,
			HashMap<String, String> pageDomains) {
		List<String> domains = new ArrayList<>();
		for (String url : pageURLs) {
			if (pageDomains.containsKey(url)) {
				String d = pageDomains.get(url);
				if (!domains.contains(d))
					domains.add(d);
			} else {
				String d = getDomain(url);
				if (d == null)
					d = "notification";
				pageDomains.put(url, d);

				if (!domains.contains(d))
					domains.add(d);
			}
		}
		return domains;
	}

	private String getDomain(String url) {
		try {
			URL linkurl = new URL(url);
			String domain = linkurl.getHost().replace("www.", "");
			return domain;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public void run() {
		sendEmail();
	}

	public void trySendEmail(Email email, EmailAction emailAction,
			EmailEntity emailEntity) throws EmailDBException,
			DAOFactoryException, TraceReportDBException {

		if (emailAction.numretries > MAX_NUM_OF_RETRIES) {
			emailAction.status = EmailStatus.TOO_MANY_RETRIES;
			daoFactory.getEmailDAO().updateEmailStatus(emailAction);
			log.info("max send retry has been reached.");
			return;
		}
		try {
			daoFactory.getEmailDAO().incRetry(emailAction, 1L);

			emailer.sendEmail(emailEntity);
			emailAction.status = EmailStatus.SEND_OK;
			daoFactory.getEmailDAO().updateEmailStatus(emailAction);

			log.info("send mail successful. " + emailAction.emailId);
		} catch (Exception e) {
			emailAction.status = EmailStatus.ERROR;
			daoFactory.getEmailDAO().updateEmailStatus(emailAction);

			// M_TraceReport traceReport = new M_TraceReport(
			// Supervisor.sendEmail.name(), System.currentTimeMillis());
			// traceReport.error = ExceptionUtils.getFullStackTrace(e);
			// traceReport.reportType = ReportType.email;
			// traceReport.reportTime = System.currentTimeMillis();
			// traceReport.reportContent = "send email failed";

			// daoFactory.getTraceReportDAO().insertTraceReport(traceReport);
			log.error("send mail failed . " + emailAction.emailId
					+ " send email failed");
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Properties properties = new Properties();
		boolean loadTrack = true;
		try {
			properties.load(EmailCron.class.getClassLoader()
					.getResourceAsStream("db.properties"));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			loadTrack = false;
		}

		if (!loadTrack) {
			System.out.println("load email db failed. ");
			System.exit(0);
		}
		DataSource dataSource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		// PageBlockDB pageBlockDB = new PageBlockDB(dbconnection);
		// SocialMessageDB socialMessageDB = new
		// SocialMessageDBImpl(dbconnection);

		DAOFactory mysqlDAOFactory = new MysqlDAOFactory(dataSource);
		M_FileStorageDAO fileStorageDAO = new M_FileStorageDAOImpl(properties);

		EmailCron cron = new EmailCron(mysqlDAOFactory, fileStorageDAO);
		cron.createEmails();
		cron.run();

		mysqlDAOFactory.closeConnection();
	}
}
