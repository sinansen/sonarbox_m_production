package com.datalyxt.emailpool;

import java.util.List;

import com.datalyxt.production.webmodel.email.Email;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

public class EmailWriter {

	private MailContentGenerator mailContentGenerator = new MailContentGenerator();

	public String writeEmailText(Email email, String receiver,
			List<ActionResultText> text,  List<String> pages) {

		String body = mailContentGenerator.getTable(text);

		String message = mailContentGenerator.getHeader(pages) + body
				+ mailContentGenerator.getFooter("" + email.id, receiver);
		
		return message;
	}
}
