/**
 * 
 */
package com.datalyxt.emailpool;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.datalyxt.production.webmodel.email.Attachement;

/**
 * @author Bijan Fahimi
 * 
 */
public class SendEmail {

	// Assuming you are sending email from localhost
	private Properties properties;
	private Session session;

	public SendEmail(Properties mailServerProperties) {
		// sets SMTP server properties
		boolean withAuth = Boolean.valueOf(mailServerProperties
				.getProperty("auth"));

		String host = mailServerProperties.getProperty("host");
		String port = mailServerProperties.getProperty("port");

		properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.starttls.enable", "true");
		if (withAuth) {
			properties.put("mail.smtp.auth", "true");
			String username = mailServerProperties.getProperty("username");
			String password = mailServerProperties.getProperty("password");
			properties.put("mail.user", username);
			properties.put("mail.password", password);
			// creates a new session with an authenticator
			Authenticator auth = new Authenticator() {
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			};
			session = Session.getInstance(properties, auth);
		}else{
			properties.put("mail.smtp.auth", "false");
			session = Session.getInstance(properties);
		}

	}

	/**
	 * @param host
	 * 
	 */
	public SendEmail(String host) {
		properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);

		session = Session.getDefaultInstance(properties);
	}

	public void sendEmail(EmailEntity emailEntity) throws MessagingException,
			UnsupportedEncodingException {
		// Get the default Session object.

		// Create a default MimeMessage object.
		MimeMessage message = new MimeMessage(session);

		message.setFrom(new InternetAddress(emailEntity.senderEmail,
				emailEntity.senderName));
		// Set To: header field of the header.
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(
				emailEntity.receiverEmail));

		// Set Subject: header field
		message.setSubject(emailEntity.subject, "utf-8");

		// Set current date as send date
		message.setSentDate(new Date());

		// creates message part
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart
				.setContent(emailEntity.text, "text/html; charset=utf-8");

		// creates multi-part
		Multipart multipart = new MimeMultipart("mixed");
		multipart.addBodyPart(messageBodyPart);

		// adds attachments
		for (Attachement attachement : emailEntity.attachements) {
			MimeBodyPart attachPart = new MimeBodyPart();

			ByteArrayDataSource ds = new ByteArrayDataSource(
					attachement.attachement, attachement.mimetype);
			DataHandler dh = new DataHandler(ds);

			attachPart.setDataHandler(dh);
			attachPart.setFileName(attachement.name);
			multipart.addBodyPart(attachPart);
		}

		// sets the multi-part as e-mail's content
		message.setContent(multipart);

		// Send message
		Transport.send(message);

	}

	// public static void sender(String content, String emailTo, String
	// emailFrom,
	// String subject) throws MessagingException,
	// UnsupportedEncodingException {
	// String emailId = SequentialGuidGenerator.newSequentialGuidString();
	// Email email = new Email(emailId);
	// email.receiver = emailTo;
	// email.subject = subject;
	// email.lastUpdateDate = System.currentTimeMillis();
	// email.status = EmailStatus.QUEUED;
	// email.message = content;
	//
	// SendEmail sendMailer = new SendEmail("localhost");
	// sendMailer.sendEmail(email);
	// }
}
