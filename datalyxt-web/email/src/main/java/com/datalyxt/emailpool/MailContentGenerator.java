package com.datalyxt.emailpool;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.ContentBlock;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MailContentGenerator {

	String source = "http://www.aldi-sued.de";

	String id = "300";
	String mail = "sinan.sen@datalyxt.com";

	public static void main(String args[]) {
		MailContentGenerator mailContentGenerator = new MailContentGenerator();
		// String result = mailContentGenerator.getHeader()
		// + mailContentGenerator.getBodyElement("http://www.heise.de")
		// + mailContentGenerator.getBodyElement("http://www.spiegel.de")
		// + mailContentGenerator.getFooter("300slj",
		// "sinan.sen@datalyxt.com");
		// System.out.println(result);
	}

	public MailContentGenerator() {

	}

	public String getHeader(List<String> pageURLs) {
		String pagesDIV = "";
		for (String url : pageURLs) {
			String h = "<h3> " + url + "</h3><br><br>";
			pagesDIV = pagesDIV + h;
		}
		pagesDIV = "  <div> " + pagesDIV + "</div><br><br>";
		String header = "<html lang=\"de\" style=\"box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;text-decoration: none !important;\"><head><title>Sonarbox - Benachrichtigungen</title><meta charset=\"UTF-8\"><link rel=\"important stylesheet\" href=\"chrome://messagebody/skin/messageBody.css\"></head>"
				+ "<body style=\"box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;background-color: #ddd;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;font-size: 13px;line-height: 1.428571429;color: #333333;text-decoration: none !important;\"><br> "
				+ pagesDIV;
		return header;
	}

	// public String getBodyElement(String source) {
	// String bodyElement =
	// "<div class=\"cluster-wrapper normal-description-cluster row\" data-clustertype=\"normal-description\" data-clusterid=\"1X7J1FKJn9\" style=\"box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;margin: 15px auto;width: 60%;text-decoration: none !important;\">	    <div class=\"cluster\" style=\"box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;border-bottom: 1px solid #dadada;background: #fff no-repeat center;overflow: hidden;position: relative;text-decoration: none !important;\">        <div class=\"title\" style=\"box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;width: auto;margin: 0;padding: 0;text-overflow: inherit;-o-text-overflow: inherit;white-space: inherit;font-weight: 700;font-size: 18px;background: #fff;line-height: 1.2;-moz-transition: ease-out .3s;-moz-transition-property: font-size,line-height;-webkit-transition: ease-out .3s;-webkit-transition-property: font-size,line-height;-ms-transition: ease-out .3s;-ms-transition-property: font-size,line-height;-o-transition: ease-out .3s;-o-transition-property: font-size,line-height;text-decoration: none !important;\"> "
	// + source + " hat neue Informationen </div></div></div><br><br>";
	// return bodyElement;
	// }

	public String getFooter(String id, String mail) {
		String footer = "</body></html>";
		return footer;
	}

	class TableTemplate {
		public List<RowTemplate> blocks;
	}

	class RowTemplate {
		public String name;
		public String blockText;
	}

	public HashMap<String, List<ActionResultText>> groupResult(
			List<ActionResultText> results) {
		HashMap<String, List<ActionResultText>> group = new HashMap<String, List<ActionResultText>>();

		for (ActionResultText actionResultText : results) {
			if (actionResultText.type.equals(BlockActionType.structure.name())) {
				List<ActionResultText> l = group.get(BlockActionType.structure
						.name());
				if (l == null)
					l = new ArrayList<>();
				l.add(actionResultText);
				group.put(BlockActionType.structure.name(), l);

			} else if (actionResultText.type.equals(BlockActionType.followLinks
					.name())) {
				List<ActionResultText> l = group
						.get(BlockActionType.followLinks.name());
				if (l == null)
					l = new ArrayList<>();
				l.add(actionResultText);
				group.put(BlockActionType.followLinks.name(), l);

			} else if (actionResultText.type.equals(BlockActionType.linkStorage
					.name())) {
				List<ActionResultText> l = group
						.get(BlockActionType.linkStorage.name());
				if (l == null)
					l = new ArrayList<>();
				l.add(actionResultText);
				group.put(BlockActionType.linkStorage.name(), l);
			} else {
				List<ActionResultText> l = group.get("other");
				if (l == null)
					l = new ArrayList<>();
				l.add(actionResultText);
				group.put("other", l);
			}
		}
		return group;
	}

	public String getTable(List<ActionResultText> textresults) {

		HashMap<String, List<ActionResultText>> group = groupResult(textresults);
		String tables = "";
		for (String key : group.keySet()) {
			List<ActionResultText> results = group.get(key);

			String out = "<table>";
			Gson gson = new Gson();
			List<String> thData = new ArrayList<String>();
			String tb = "";
			for (int i = 0; i < results.size(); i++) {

				ActionResultText txt = results.get(i);

				if (txt.type.equals(BlockActionType.structure.name())) {
					TableTemplate row = gson.fromJson(txt.content,
							TableTemplate.class);
					String tr = "";

					for (RowTemplate block : row.blocks) {
						String name = block.name;
						if (!thData.contains(name))
							thData.add(name);

						String text = block.blockText;

						String td = "<td>" + text + "</td>";
						tr = tr + td;

					}

					tr = "<tr>" + tr + "</tr>";

					tb = tb + tr;

				} else if (txt.type.equals(BlockActionType.followLinks.name())
						|| txt.type.equals(BlockActionType.linkStorage.name())) {

					Type listType = new TypeToken<ArrayList<String>>() {
					}.getType();

					List<String> row = gson.fromJson(txt.content, listType);
					String tr = "";

					for (String l : row) {

						String td = "<td>" + l + "</td>";
						tr = tr + td;

					}

					tr = "<tr>" + tr + "</tr>";

					tb = tb + tr;

				} else {

					String tr = "";

					String td = "<td>" + txt.content + "</td>";
					tr = tr + td;

					tr = "<tr>" + tr + "</tr>";

					tb = tb + tr;

				}

			}
			String th = "";
			for (String h : thData) {
				String td = "<th>" + h + "</th>";
				th = th + td;

			}

			th = "<tr>" + th + "</tr>";

			out = "<table>" + th + tb + "</table>";

			tables = tables + out;
		}
		return tables;

	}
}
