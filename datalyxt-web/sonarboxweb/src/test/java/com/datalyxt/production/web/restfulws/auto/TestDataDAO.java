package com.datalyxt.production.web.restfulws.auto;

import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class TestDataDAO {
	protected DataSource dataSource;

	protected JdbcTemplate jdbcTemplate;

	private static TestDataDAO instance;

	public static final String sourceWSTable = "source_ws_test_data";
	public static final String receiverWSTable = "receiver_ws_test_data";

	private TestDataDAO() {

		String resourceName = "testdb.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		try (InputStream resourceStream = loader
				.getResourceAsStream(resourceName)) {
			props.load(resourceStream);
			dataSource = getMySQLDataSource(props);
			jdbcTemplate = new JdbcTemplate(dataSource);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static TestDataDAO getInstance() {
		if (instance == null)
			instance = new TestDataDAO();
		return instance;
	}

	private DataSource getMySQLDataSource(Properties properties)
			throws SQLException {
		MysqlDataSource mysqlDS = null;
		mysqlDS = new MysqlDataSource();
		mysqlDS.setUrl(properties.getProperty("MYSQL_DB_URL"));
		mysqlDS.setUser(properties.getProperty("MYSQL_DB_USERNAME"));
		mysqlDS.setPassword(properties.getProperty("MYSQL_DB_PASSWORD"));
		return mysqlDS;
	}

	public List<TestData> getWSTestData(String tableName) {
		List<TestData> testdataList = new ArrayList<TestData>();
		try {
			String sql = "select id, input, service_path from " + tableName;
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql);
			while (rows.next()) {
				TestData testData = new TestData();
				testData.testId = rows.getLong("id");
				testData.input = rows.getString("input");
				testData.relativeEndpoint = rows.getString("service_path");
				testdataList.add(testData);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return testdataList;
	}

	public void writeWSTestData(String tableName, List<TestData> dataList) {
		try {
			String sql = "update "
					+ tableName
					+ " set output = ? , last_test_at = ? where id = ? and service_path = ? ";
			List<Object[]> params = new ArrayList<>();
			for (TestData data : dataList) {
				Object[] p = new Object[] { data.output, new Timestamp(data.lastTestAt),
						data.testId, data.relativeEndpoint };
				params.add(p);
			}
			jdbcTemplate.batchUpdate(sql, params);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cleanOutput(String tableName) {
		try {
			String sql = "update "
					+ tableName
					+ " set output = null  ";
			jdbcTemplate.batchUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
