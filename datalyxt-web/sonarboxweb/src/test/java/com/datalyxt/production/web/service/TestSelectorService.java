package com.datalyxt.production.web.service;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestSelectorService {
	@Test
	public void testJson() throws IOException {
		M_Rule rule = new M_Rule();
		QT1_V1_RuleContent content = new QT1_V1_RuleContent();
		rule.name = "test";
		rule.ruleType = M_RuleType.QT1V1;

		rule.sourceIds.add(1L);

		
		ObjectMapper mapper = new ObjectMapper();
		
	
		

		
		
		
		
		QT1_V1_RuleContent subcontent = new QT1_V1_RuleContent();

		subcontent.blockConfigMain = new BlockConfigMain();
		subcontent.blockConfigMain.config = new ArrayList<>();
		rule.ruleContent = subcontent;
		String json = mapper.writeValueAsString(rule);
		System.out.println(json);
		
		rule = mapper.readValue(json, M_Rule.class);
		System.out.println(rule.name);
		
	}
}
