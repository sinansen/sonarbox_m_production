package com.datalyxt.production.web.restfulws.auto;

import java.util.List;

import org.springframework.web.client.RestTemplate;

public class TestRestfulService {
	RestTemplate restTemplate = new RestTemplate();

	public void callWS(String baseURL, TestData testData) {
		try {

			String result = restTemplate.postForObject(baseURL
					+ testData.relativeEndpoint, testData.input, String.class);
			testData.output = result;
			testData.lastTestAt = System.currentTimeMillis();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		String baseURL = "http://localhost:8080/";
		String table = TestDataDAO.sourceWSTable;
		TestDataDAO dataDao = TestDataDAO.getInstance();
		TestRestfulService serviceCaller = new TestRestfulService();
		
		dataDao.cleanOutput(table);
		
		List<TestData> dataList = dataDao
				.getWSTestData(table);
		for (TestData data : dataList)
			serviceCaller.callWS(baseURL, data);

		dataDao.writeWSTestData(table, dataList);
	}
}
