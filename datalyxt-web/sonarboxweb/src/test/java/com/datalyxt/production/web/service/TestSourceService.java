package com.datalyxt.production.web.service;

import org.junit.Test;

import com.datalyxt.production.web.bean.source.SourceUIInput;
import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceType;
import com.google.gson.Gson;

public class TestSourceService {
@Test
public void testJson(){
	SourceUIInput s = new SourceUIInput();
	s.url = "http://www.fzi.de/startseite/";
	s.types.add(M_SourceType.QT1V1);
	s.languages.add(LanguageISO_639_1.de);
	s.languages.add(LanguageISO_639_1.en);
	
	System.out.println(new Gson().toJson(s));
}
}
