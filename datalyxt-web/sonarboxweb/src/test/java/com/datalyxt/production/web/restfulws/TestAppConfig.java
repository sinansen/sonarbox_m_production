package com.datalyxt.production.web.restfulws;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
 
@Configuration
@ComponentScan(basePackages="com.datalyxt.production.web")
@PropertySource("classpath:db.properties")

public class TestAppConfig { 
 
	@Autowired
	private Environment env;

	
	@Bean
	public DataSource getDataSource() throws SQLException {

		MysqlDataSource mysqlDS = null;
		mysqlDS = new MysqlDataSource();
		mysqlDS.setUrl(env.getProperty("MYSQL_DB_URL"));
		mysqlDS.setUser(env.getProperty("MYSQL_DB_USERNAME"));
		mysqlDS.setPassword(env.getProperty("MYSQL_DB_PASSWORD"));
		return mysqlDS;

	}
	
}