/** Enum declarations */
var SortDirection = Object.freeze({ascending: 'sort_direction_asc', descending: 'sort_direction_desc'});

var InteractionType = Object.freeze({
    add: 'interaction_type_add',
    modify: 'interaction_type_modify',
    delete: 'interaction_type_delete',
    manage: 'interaction_type_manage',
    changeStatus: 'interaction_type_change_Status',
    pager: 'interaction_type_pager',
    sort: 'interaction_type_sort',
    filter: 'interaction_type_filter'
    	
});

var SourceStatus = Object.freeze({
    added: 'added',
    activated: 'activated',
    paused: 'paused',
    error: 'error',
    validated: 'validated',
    blacklist: 'blackList'
});

var ResourceAction = Object.freeze({
    sourceEdit: 'source_action_edit',
    sourceActivity: 'source_action_activity',
    sourceDelete: 'source_action_delete',
    sourceError: 'source_action_error',

    receiverEdit: 'receiver_action_edit',
    receiverActivity: 'receiver_action_activity',
    receiverDelete: 'receiver_action_delete',
    receiverError: 'receiver_action_error',
    	
    groupEdit: 'group_action_edit',
    groupActivity: 'group_action_activity',
    groupDelete: 'group_action_delete',
    groupError: 'group_action_error',
    	
    selectorEdit: 'selector_action_edit',
    selectorActivity: 'selector_action_activity',
    selectorDelete: 'selector_action_delete',
    selectorError: 'selector_action_error',
    
    resultEdit: 'result_action_edit',
    resultActivity: 'result_action_activity',
    resultDetail: 'result_detail',
    resultDelete: 'result_action_delete',
    resultError: 'result_action_error',
    
    mailEdit: 'mail_action_edit',
    
    reportDetail: 'report_detail'


});

var SourceType = Object.freeze({
    webScraping: 'QT1V1',
    webSearch: 'QT1V2',
    socialSearch: 'QT2'
});

var FilterType = Object.freeze({
    select: 'filter-type-select',
    selectMulti: 'filter-type-select-multi',
    time: 'filter-type-time'
});

var ActivityType = Object.freeze({
    created: 'created',
    deleted: 'deleted',
    modified: 'modified',
    validated: 'validated',
    updateURL: 'updateURL',
    activated: 'activated',
    paused: 'paused',
    error: 'error'
});

var ActivityDialogTitle = Object.freeze({
    source: 'Quellen Aktivitaet',
    receiver: 'Receiver Aktivitaet',
    selector: 'Selektoren Aktivitaet',
    group: 'Gruppen Aktivitaet'
});

var ObjectType = Object.freeze({
    source: 'source',
    receiver: 'receiver',
    selector: 'selector',
    result: 'result',
    group: 'group',
    mail:'mail',
    monitoring: 'monitoring',
    logbook: 'logbook'
});



var Pages = Object.freeze({
    sources: 'sources',
    receivers: 'receivers',
    groups: 'groups',
    selectors: 'selectors',
    results: 'results',
    mails: 'mails',
    monitoring: 'monitoring',
    logbook:'logbook'
});

var SelectorStatus = Object.freeze({
    added: 'added',
    activated: 'activated',
    paused: 'paused',
    error: 'error',
    validated: 'validated'
});

var StandardStatus = Object.freeze({
    added: 'added',
    activated: 'activated',
    paused: 'paused'
});

var ReportStatus = Object.freeze({
    failed: 'failed',
    success: 'success'
});

var RestfulBase = 'http://localhost:8080/vims';
//var RestfulBase = 'https://www.mnet.markant.de/vims';

var RestfulEndpoint = Object.freeze({

    source: RestfulBase+'/sonarbox/res/sources',
    receiver: RestfulBase+'/sonarbox/res/receivers',
    group: RestfulBase+'/sonarbox/res/groups',
    selector: RestfulBase+'/sonarbox/res/selectors',
    result: RestfulBase+'/sonarbox/res/results',
    resultFile: RestfulBase+'/sonarbox/res/results/files/',
    resultImg: RestfulBase+'/sonarbox/res/results/imgs/',
    mail: RestfulBase+'/sonarbox/res/mails',
    monitoring: RestfulBase+'/sonarbox/res/reports',
    monitoringDetail: RestfulBase+'/sonarbox/res/reports/details',
    activity:  RestfulBase+'/sonarbox/res/activities',
 
});
