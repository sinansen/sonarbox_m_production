function EditorColumn(params) {
    var properties = extend({
    	id: generateUUID(),
        name: '',
        type: '',
        elements: [],
        marked:[],
        color: ''
    }, params);

    if(isEmpty(properties.color)) throw "Error, color is undefined";
    if(isEmpty(properties.id)) throw "Error, id is undefined";

    this.id = properties.id;
    this.name = properties.name;
    this.type = properties.type;
    this.elements = properties.elements;
    this.marked = properties.marked;
    this.color = properties.color;
}

function ColumnElement(params) {
    var properties = extend({
    	path: '',
        text: '',
        html: '',
        link: ''
    }, params);

    if(isEmpty(properties.path)) throw "Error, path is empty";
    
    this.path = properties.path;
    this.text = properties.text;
    this.html = properties.html;
    this.link = properties.link;
}

function ColumnAction(params) {
    var properties = Object.assign({
        valid: true,
        columnId: '',
        actionType: '',
        parameters: {}
    }, params);

    if(isEmpty(properties.columnId)) throw "Error, column is undefined";
    if(isEmpty(properties.actionType)) throw "Error, actionType is undefined";

    this.valid = properties.valid;
    this.columnId = properties.columnId;
    this.actionType = properties.actionType;
    this.parameter = properties.parameter;
    this.actions = [EditorInteractionType.edit, EditorInteractionType.delete];
}

/** Enums */
var EditorInteractionType = Object.freeze({
    edit: 'editor-interaction-type-edit',
    delete: 'editor-interaction-type-delete'
});

/** Renderer */

var actionStatusRenderer = function(cell) {
    var value = '';

    if(cell.data) {
        cell.element.style.color = '#8bc34a';
        value = 'Aktiv';
    } else {
        cell.element.style.color = '#f44336';
        value = 'Inaktiv';
    }

    cell.element.innerHTML = value;
};

var interactionRenderer = function(cell) {
    cell.element.innerHTML = '';

    var children = cell.data.map(function(elem) {
        var child = document.createElement('data-list-item-icon');

        switch (elem) {
            case EditorInteractionType.edit:
                child.setAttribute('icon', 'create');
                child.setAttribute('color', '#2196f3');
                child.setAttribute('tooltip', 'Bearbeiten');
                break;
            case EditorInteractionType.delete:
                child.setAttribute('icon', 'delete');
                child.setAttribute('color', '#2196f3');
                child.setAttribute('tooltip', 'Löschen');
                break;
            default:
                throw 'Error, unknown interaction type';

        }

        child.setAttribute('type', elem);
        child.item = cell.row.data;

        return child;
    });

    children.forEach(function(child) {
        cell.element.appendChild(child)
    });
};

/** Helper functions */

var generateUUID = function() {
    var d = new Date().getTime();

    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 |0x8)).toString(16);
    });

    return uuid;
};