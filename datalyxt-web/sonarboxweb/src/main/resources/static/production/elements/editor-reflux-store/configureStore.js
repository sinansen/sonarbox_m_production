var ReduxThunk = window.ReduxThunk.default;
var store = Redux.createStore(editorReducer, Redux.applyMiddleware(ReduxThunk));

// attach them to the window, as there should only be one instance of the store
var ReduxBehavior = PolymerRedux(store);