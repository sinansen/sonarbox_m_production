function Source(params) {
    var properties = extend({
        id: undefined,
        name: '',
        url: '',
        languages: [],
        types: [],
        dynamicParams: [],
        feedback: undefined,
        status: '',
        createdAt: Date.now(),
        lastModified: Date.now()
    }, params);

    this.id = properties.id;
    this.name = properties.name;
    this.url = properties.url;
    this.languages = properties.languages;
    this.types = properties.types;
    this.dynamicParams = properties.dynamicParams;
    this.feedback = properties.feedback;
    this.status = properties.status;
    this.createdAt = properties.createdAt;
    this.lastModified = properties.lastModified;
    this.actions = [ResourceAction.sourceEdit, ResourceAction.sourceActivity];
}

function Activity(params) {
    var properties = extend({
        id: 0,
        type: '',
        userName:'',
        objectId: 0,
        objectType: '',
        content: '',
        userId: 0,
        timestamp: 0
    }, params);

    if(Object.values(ActivityType).indexOf(properties.type) === -1) throw "Error, unknown activity type " + properties.type;
    if(properties.objectId === 0) throw "Error, no objectId provided";
    if(Object.values(ObjectType).indexOf(properties.objectType) === -1) throw "Error, unknown object type " + properties.objectType;
    if(properties.timestamp === 0) throw "Error, no timestamp provided";

    this.id = properties.id;
    this.type = properties.type;
    this.objectId = properties.objectId;
    this.objectType = properties.objectType;
    this.content = properties.content;
    this.userName = properties.userName;
    this.timestamp = properties.timestamp;
}

function SourceCreateModel(params) {
    var properties = extend({
        name: '',
        url: '',
        blacklist: false,
        languages: [],
        types: [],
        autoDeploy: false
    }, params);

    // check conditions
    if(isEmpty(properties.url)) throw "Error, empty url provided";

    this.name = properties.name;
    this.url = properties.url;
    this.blacklist = properties.blacklist;
    this.languages = properties.languages;
    this.types = properties.types;
    this.autoDeploy = properties.autoDeploy;
}

function FilterSortInformation(params) {
    var properties = extend({
        sortColumn: '',
        sortDirection: SortDirection.descending,
        filter: {}
    }, params);

    // check conditions
    if(isEmpty(properties.sortColumn))  throw "Error, empty sortColumn";
    if(Object.values(SortDirection).indexOf(properties.sortDirection) === -1) throw "Error, unknown sort Direction";

    this.sortColumn = properties.sortColumn;
    this.sortDirection = properties.sortDirection;
    this.filter = properties.filter;
}

function Selector(params) {
    var properties  = extend({
        id: -1,
        status: SelectorStatus.added,
        sourceId: -1,
        sources: [],
        name: '',
        url: '',
        type: '',
        hops: 0,
        createdAt: Date.now(),
        lastModified: Date.now()
    }, params);

    //check conditions
    if(Object.values(SelectorStatus).indexOf(properties.status) === -1) throw "Error, unknown selector status";
    if(isEmpty(properties.name))  throw "Error, empty selector name";
   // if(isEmpty(properties.url))  throw "Error, empty selector url";

    this.id = properties.id;
    this.status = properties.status;
    this.name = properties.name;
    this.url = properties.url;
    this.type = properties.type;
    this.hops = properties.hops;
    this.sources = properties.sources;
    this.createdAt = properties.createdAt;
    this.lastModified = properties.lastModified;
    this.actions = [ResourceAction.selectorEdit, ResourceAction.selectorActivity];
}

function Receiver(params) {
    var properties = extend({
    	id: undefined,
    	address: '',
        firstName: '',
        lastName:'',
        email:'',
        status: '',
        company: '',
        groups: [],
        department: '',
        theFunction: '',
        category: '',
        createdAt: Date.now(),
        lastModified: Date.now()
    }, params);

    this.id = properties.id;
    this.address = properties.address;
    this.lastName = properties.lastName;
    this.firstName = properties.firstName;
    this.company = properties.company;
    this.groups = properties.groups;
    this.email = properties.email;
    this.department = properties.department;
    this.category = properties.category;
    this.status = properties.status;
    this.theFunction = properties.theFunction;
    this.createdAt = properties.createdAt;
    this.lastModified = properties.lastModified;
    this.fullname = properties.address+' '+properties.firstName+' '+properties.lastName;
    this.actions = [ResourceAction.receiverEdit, ResourceAction.receiverActivity];
}

function ReceiverCreateModel(params){
    var properties = extend({
        address: '',
        lastName: '',
        email: '',
        firstName: '',
        company: '',
        groups:[]
    }, params);

    // check conditions
    if(isEmpty(properties.email)) throw "Error, empty email provided";
    this.id = properties.id;
    this.address = properties.address;
    this.lastName = properties.lastName;
    this.firstName = properties.firstName;
    this.company = properties.company;
    this.groups = properties.groups;
    this.email = properties.email;
    this.department = properties.department;
    this.category = properties.category;
    this.status = properties.status;
    this.theFunction = properties.theFunction;

}

function Group(params) {
    var properties = extend({
    	id: undefined,
        name: '',
        description: '',
        member: [],
        status: '',
        createdAt: Date.now(),
        lastModified: Date.now()
    }, params);

    this.id = properties.id;
    this.name = properties.name;
    this.description = properties.description;
    this.status = properties.status;
    this.member = properties.member;
    this.createdAt = properties.createdAt;
    this.lastModified = properties.lastModified;
    this.actions = [ResourceAction.groupEdit, ResourceAction.groupActivity];
}

function GroupCreateModel(params){
    var properties = extend({
        name: '',
        description: '',
        member: []
    }, params);

    // check conditions
    if(isEmpty(properties.name)) throw "Error, empty name provided";

    this.name = properties.name;
    this.description = properties.description;
    this.member = properties.member;
    
}

function Result(params) {
    var properties = extend({
    	id: undefined,
    	sourceName:'',
    	sourceUrl:'',
    	sourceId:'',
    	selectorName: '',
    	selectorId: '',
    	maxHops: 0,
    	statis: {},
    	root: {},
        scanId: Date.now()
    }, params);

    this.id = properties.id;
    this.source = { name : properties.sourceName, sourceId : properties.sourceId, sourceUrl : properties.sourceUrl }
    this.selectorId = properties.selectorId;
    this.selectorName = properties.selectorName;
    this.scanId = properties.scanId;
    this.statis = properties.statis;
    this.maxHops = properties.maxHops;
    this.root = properties.root;
    this.actions = [ResourceAction.resultDetail];
}

function Email(params) {
    var properties = extend({
    	id: undefined,
    	receiverId: undefined,
    	address: '',
        firstName: '',
        lastName:'',
        email:'',
        subject:'',
        status: 1,
        updateType: 0,
        timestamp: Date.now()
    }, params);

    this.id = properties.id;
    this.receiverId = properties.receiverId;
    this.address = properties.address;
    this.lastName = properties.lastName;
    this.firstName = properties.firstName;
    this.email = properties.email;
    this.subject = properties.subject;
    this.status = properties.status;
    this.lastModified = properties.timestamp;
    this.fullname = properties.address+' '+properties.firstName+' '+properties.lastName;
    this.updateType = properties.updateType;
    this.actions = [ResourceAction.mailEdit];
}

function EmailCreateModel() {
	this.id = 0;
    this.receiverIds = [];
    this.groupIds = [];
    this.subject = '';
    this.messageIds = [];
    this.attachementFileIds = [];
}

function EmailDetail(params) {
    var properties = extend({
    	id: undefined,
    	receivers: [],
    	files: [],
    	images: [],
    	rows: [],
        subject:''
    }, params);

    this.id = properties.id;
    this.receivers = properties.receivers;
    this.subject = properties.subject;
    this.rows = properties.rows;
    this.files = properties.files;
    this.images = properties.images;

}

function Report(params) {
    var properties = extend({
    	id: undefined,
    	sourceId: 0,
    	scanId: 0,
        selectorId: 0,
        selectorName:'',
        sourceName:'',
        sourceUrl:'',
        hasError: false,
        scanStart:  Date.now(),
        scanEnd: Date.now()
    }, params);

    this.id = properties.id;
    this.source = { name : properties.sourceName, sourceId : properties.sourceId, sourceUrl : properties.sourceUrl }
    this.scanId = properties.scanId;
    this.selectorId = properties.selectorId;
    this.selectorName = properties.selectorName;
    this.status = 'success';
    
    if(properties.hasError)
    	this.status = 'failed';
    this.scanStart = properties.scanStart;
    this.scanEnd = properties.scanEnd;
    this.actions = [ResourceAction.reportDetail];
}

function ReportDetail(params) {
    var properties = extend({
    	id: undefined,
    	sourceId: 0,
    	scanId: 0,
        selectorId: 0,
        message: '',
        type:  '',
        reporter: '',
        error: '',
        url: '',
        time: Date.now()
    }, params);

    this.id = properties.id;
    this.sourceId = properties.sourceId;
    this.scanId = properties.scanId;
    this.selectorId = properties.selectorId;
    this.message = properties.message;
    this.type = properties.type;
    this.reporter = properties.reporter;
    this.error = properties.error;
    this.url = properties.url;
    this.time = properties.time;
}

function PagingModel(params) {
    var properties = extend({
    	perpage: 2,
    	pages: 7
    }, params);

    this.perpage = properties.perpage;
    this.pages = properties.pages;
    this.offset = 0,
    this.next = true;
    this.pagecount = this.pages;
}