const RENAME_TEMPLATE = 'RENAME_TEMPLATE';
const CHANGE_FETCH_OPTION = 'SET_FETCH_OPTION';
const CHANGE_VIEW_TYPE = 'CHANGE_VIEW_TYPE';
const ADD_COLUMN = 'ADD_COLUMN';
const REMOVE_COLUMN = 'REMOVE_COLUMN';
const RENAME_COLUMN = 'RENAME_COLUMN';
const CHANGE_COLUMN_TYPE = 'CHANGE_COLUMN_TYPE';
const CHANGE_ACTIVE_COLUMN= 'CHANGE_ACTIVE_COLUMN';
const SET_ELEMENTS = 'SET_ELEMENTS';
const REMOVE_ELEMENT = 'REMOVE_ELEMENT';
const SAVE_TEMPLATE = 'SAVE_TEMPLATE';
const ADD_ACTION = 'ADD_ACTION';
const REMOVE_ACTION = 'REMOVE ACTION';
const INVALIDATE_ACTION = 'INVALIDATE_ACTION';
const EDIT_ACTION = 'EDIT_ACTION';
const RESET_EDITOR = 'RESET_EDITOR';
const CHANGE_MARKER_ENABLED = 'CHANGE_MARKER_ENABLED';

const ACTION_DIALOG_CREATE_ACTION = 'ACTION_DIALOG_CREATE_ACTION';
const ACTION_DIALOG_ABORT = 'ACTION_DIALOG_ABORT';
const ACTION_DIALOG_EDIT_ACTION = 'ACTION_DIALOG_EDIT_ACTION';
const ACTION_DIALOG_OPEN = 'ACTION_DIALOG_OPEN';
const ACTION_DIALOG_CLOSE = 'ACTION_DIALOG_CLOSE';
const ACTION_DIALOG_RESET = 'RESET_DIALOG_SELECT';

const ViewType = Object.freeze({website: 'view-type-website', grid: 'view-type-grid'});
const ColumnType = Object.freeze({text: 'column-type-text', date: 'column-type-date', link: 'column-type-link', number: 'column-type-number', image: 'column-type-image'});
const ActionDialogViewType = Object.freeze({edit: 'action-dialog-view-type-edit', list: 'action-dialog-view-type-list'});
const ActionType = Object.freeze({screenshot: 'screenshot', followLinks: 'followLinks', fileStorage: 'fileStorage', cleanText: 'cleanText'});

function renameTemplate(name) {
    return {
        type: RENAME_TEMPLATE,
        name: name
    }
}

function changeFetchOption(option, value) {
    return {
        type: CHANGE_FETCH_OPTION,
        option: option,
        value: value
    }
}

function changeViewType(viewType) {
    switch(viewType) {
        case ViewType.website:
            return {
                type: CHANGE_VIEW_TYPE,
                viewType: viewType,
                isWebsiteView: true,
                isGridView: false
            };
        case ViewType.grid:
            return {
                type: CHANGE_VIEW_TYPE,
                viewType: viewType,
                isWebsiteView: false,
                isGridView: true
            }
    }
}

function propMarkerEnabled(value) {
    return {
        type: CHANGE_MARKER_ENABLED,
        value: value
    }
}

function addColumn(id, name, type, color, elements) {
    return function(dispatch, getState) {

        var add = function(dispatch) {
            dispatch({
                type: ADD_COLUMN,
                id: id,
                name: name,
                columnType: type,
                color: color,
                elements: elements
            });

            return Promise.resolve();
        };

        dispatch(add).then(function() {
            dispatch(changeActiveColumn(getState().editorColumns.length - 1))
        });

        return Promise.resolve();
    }
}

function removeColumn(index) {
    return function(dispatch) {
        var remove = function(dispatch) {
            dispatch({
                type: REMOVE_COLUMN,
                index: index
            });

            return Promise.resolve();
        };

        dispatch(remove).then(function() {
            dispatch(changeActiveColumn(-1));
        });

        return Promise.resolve();
    }
}

function changeActiveColumn(index) {
    return function(dispatch) {
        dispatch({
            type: CHANGE_ACTIVE_COLUMN,
            index: index
        });

        dispatch(propMarkerEnabled(index !== -1));

        return Promise.resolve();
    }
}

function changeColumnName(index, name) {
    return {
        type: RENAME_COLUMN,
        index: index,
        name: name
    }
}

function changeColumnType(index, type) {
    return {
        type: CHANGE_COLUMN_TYPE,
        index: index,
        columnType: type
    }
}

function setElements(index, elements) {
    return {
        type: SET_ELEMENTS,
        index: index,
        elements: elements
    }
}

function removeElement(index, elementIndex) {
    return {
        type: REMOVE_ELEMENT,
        index: index,
        elementIndex: elementIndex
    }
}

function resetEditor() {
    return {
        type: RESET_EDITOR
    }
}

function addAction(columnIndex, actionType, parameters) {
    return {
        type: ADD_ACTION,
        columnIndex: columnIndex,
        actionType: actionType,
        parameters: parameters
    }
}

function removeAction(index) {
    return {
        type: REMOVE_ACTION,
        index: index
    }
}

function invalidateAction(index) {
    return {
        type: INVALIDATE_ACTION,
        index: index
    }
}

function editAction(index, columnIndex, actionType, parameters) {
    return {
        type: EDIT_ACTION,
        index: index,
        columnIndex: columnIndex,
        actionType: actionType,
        parameters: parameters
    }
}

function actionDialogOpen() {
    return {
        type: ACTION_DIALOG_OPEN
    }
}

function actionDialogClose() {
    return {
        type: ACTION_DIALOG_CLOSE
    }
}

function actionDialogCreateAction() {
    return {
        type: ACTION_DIALOG_CREATE_ACTION
    }
}

function actionDialogAbort() {
    return {
        type: ACTION_DIALOG_ABORT
    }
}

function actionDialogEditAction(index) {
    return {
        type: ACTION_DIALOG_EDIT_ACTION,
        index: index
    }
}