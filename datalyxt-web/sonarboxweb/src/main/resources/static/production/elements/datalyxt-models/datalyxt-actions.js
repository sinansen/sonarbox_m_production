var ActionTypes = Object.freeze({
    source_action: 'source-action',
    search_action: 'search-action',
    selector_action: 'selector-action',
    receiver_action: 'receiver-action',
    group_action: 'group-action',
    mail_action: 'mail-action',
    report_action: 'report-action',
    result_action: 'result-action',
    activity_action: 'activity-action',
    list_item_clicked_action: 'list-item-clicked-action',
    filter_panel_filter_action: 'filter-panel-filter-action',
    data_store_ready_action: 'data-store-ready-action',
    data_store_error_action: 'data-store-error-action',
    editor_close_action: 'editor-close-action'
});

function ReceiverAction(params) {
    var properties = extend({
        type: '',
        object:  {}
    }, params);

    // check conditions
    if(isEmpty(properties.type)) throw "Error, empty type provided";
    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.receiver_action;
    this.type = properties.type;
    this.object = properties.object;
}


function GroupAction(params) {
    var properties = extend({
        type: '',
        object:  {}
    }, params);

    // check conditions
    if(isEmpty(properties.type)) throw "Error, empty type provided";
    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.group_action;
    this.type = properties.type;
    this.object = properties.object;
}

function SourceAction(params) {
    var properties = extend({
        type: '',
        object:  {}
    }, params);

    // check conditions
    if(isEmpty(properties.type)) throw "Error, empty type provided";
    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.source_action;
    this.type = properties.type;
    this.object = properties.object;
}

function SearchAction(params) {
    var properties = extend({
        type: '',
        object:  {}
    }, params);

    // check conditions
    if(isEmpty(properties.type)) throw "Error, empty type provided";
    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.search_action;
    this.type = properties.type;
    this.object = properties.object;
}

function SelectorAction(params) {
    var properties = extend({
        type: '',
        object: {}
    }, params);

    // check conditions
    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.selector_action;
    this.type = properties.type;
    this.object = properties.object;
}

function ListItemClickedAction(params) {
    var properties = extend({
        type: '',
        object:  {}
    }, params);

    // check conditions
    if(isEmpty(properties.type)) throw "Error, empty type provided";
    // if(Object.values(ResourceAction).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.list_item_clicked_action;
    this.type = properties.type;
    this.object = properties.object;
}

function FilterPanelFilterAction(params) {
    var properties = extend({
        filter:  []
    }, params);

    // check conditions
    if(isEmpty(properties.filter)) throw "Error, empty filter provided for filter action";

    this.name = ActionTypes.filter_panel_filter_action;
    this.filter = properties.filter;
}

function DataStoreReadyAction(params) {
    var properties = extend({
        id: '',
        message: ''
    }, params);

    if(isEmpty(properties.id)) throw "Error, empty datastore id provided";

    this.name = ActionTypes.data_store_ready_action;
    this.id = properties.id;
    this.message = properties.message;
}

function DataStoreErrorAction(params) {
    var properties = extend({
        id: '',
        errorCode: 0,
        message: ''
    }, params);

    if(isEmpty(properties.id)) throw "Error, empty datastore id provided";
    if(properties.errorCode === 0) throw "Error, no error code provided";

    this.name = ActionTypes.data_store_error_action;
    this.id = properties.id;
    this.errorCode = properties.errorCode;
    this.message = properties.message;
}

function EmailAction(params) {
    var properties = extend({
        type: '',
        object:  {}
    }, params);

    // check conditions
    if(isEmpty(properties.type)) throw "Error, empty type provided";
    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

    this.name = ActionTypes.mail_action;
    this.type = properties.type;
    this.object = properties.object;
}

function ReportAction(params){
	   var properties = extend({
	        type: '',
	        object:  {}
	    }, params);

	    // check conditions
	    if(isEmpty(properties.type)) throw "Error, empty type provided";
	    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

	    this.name = ActionTypes.report_action;
	    this.type = properties.type;
	    this.object = properties.object;
	
}

function ResultAction(params){
	   var properties = extend({
	        type: '',
	        object:  {}
	    }, params);

	    // check conditions
	    if(isEmpty(properties.type)) throw "Error, empty type provided";
	    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

	    this.name = ActionTypes.result_action;
	    this.type = properties.type;
	    this.object = properties.object;
	
}

function ActivityAction(params){
	   var properties = extend({
	        type: '',
	        object:  {}
	    }, params);

	    // check conditions
	    if(isEmpty(properties.type)) throw "Error, empty type provided";
	    if(Object.values(InteractionType).indexOf(properties.type) === -1) throw "Error, false type provided";

	    this.name = ActionTypes.activity_action;
	    this.type = properties.type;
	    this.object = properties.object;
	
}

function EditorCloseAction() {
    this.name = ActionTypes.editor_close_action;
}