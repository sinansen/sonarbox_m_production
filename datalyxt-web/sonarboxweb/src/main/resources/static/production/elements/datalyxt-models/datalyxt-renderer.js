/** Render functions to render the data */
var timeRenderer = function(cell) {
	cell.element.innerHTML = '';

	var child = document.createElement('time-ago');
	child.setAttribute('datetime', cell.data);
	child.setAttribute('locale', 'de');

	// styling
	child.style.color = 'grey';

	cell.element.appendChild(child);
};

var sourceStatusRenderer = function(cell) {
	var value = '';

	switch (cell.data) {
	case SourceStatus.activated:
		value = 'Aktiviert';
		break;
	case SourceStatus.added:
		value = 'Hinzugefügt';
		break;
	case SourceStatus.paused:
		value = 'Pausiert';
		break;
	case SourceStatus.error:
		value = 'Error';
		break;
	case SourceStatus.validated:
		value = 'Validiert';
		break;
	case SourceStatus.blacklist:
		value = 'Geblockt';
		break;
	default:
		throw "Error, invalid source status: " + cell.data;
	}

	cell.element.innerHTML = value;
};

var standardStatusRenderer = function(cell) {
	var value = '';

	switch (cell.data) {
	case SourceStatus.activated:
		value = 'Aktiviert';
		break;
	case SourceStatus.added:
		value = 'Hinzugefügt';
		break;
	case SourceStatus.paused:
		value = 'Pausiert';
		break;
	default:
		throw "Error, invalid status: " + cell.data;
	}

	cell.element.innerHTML = value;
};


var reportStatusRenderer = function(cell) {
	var value = '';

	switch (cell.data) {
	case 'success':
		cell.element.style.color = 'blue';
		value = 'Erfolgreich';
		break;
	case 'failed':
		cell.element.style.color = 'red';
		value = 'Fehlerhaft';
		break;
	default:
		throw "Error, invalid status: " + cell.data;
	}

	cell.element.innerHTML = value;
};

var emailStatusRenderer = function(cell) {
	var value = 0;

	switch (cell.data) {
	case 1:
		value = 'Erstellt';
		break;
	case 2:
		value = 'Geschickt';
		break;
	case 3:
		value = 'Error';
		break;
	default:
		throw "Error, invalid status: " + cell.data;
	}

	cell.element.innerHTML = value;
};

var sourceTypeRenderer = function(cell) {
	var rendered = cell.data.map(function(elem) {
		var value = '';

		switch (elem) {
		case SourceType.webScraping:
			value = 'Web Scraping';
			break;
		case SourceType.webSearch:
			value = 'Web Search';
			break;
		case SourceType.socialSearch:
			value = 'Social Search';
			break;
		default:
			console.log("Error, invalid source type " + cell.data);
		}

		return value;
	});

	cell.element.innerHTML = rendered.join(', ');
};

var linkRenderer = function(cell) {
	cell.element.innerHTML = '';

	var child = document.createElement('a');
	child.setAttribute('href', cell.data);
	child.setAttribute('target', '_blank');
	child.innerHTML = cell.data;

	// styling
	child.style.color = '#2196F3';
	child.style.textDecoration = 'none';
	child.onmouseover = function() {
		this.style.textDecoration = 'underline';
	};

	child.onmouseout = function() {
		this.style.textDecoration = 'none';
	};

	cell.element.appendChild(child);
};

var actionRenderer = function(cell) {
	cell.element.innerHTML = '';

	var children = cell.data.map(function(elem) {
		var child = document.createElement('data-list-item-icon');

		switch (elem) {
		case ResourceAction.sourceEdit:
			child.setAttribute('icon', 'create');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Bearbeiten');
			break;
		case ResourceAction.receiverEdit:
			child.setAttribute('icon', 'create');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Bearbeiten');
			break;
		case ResourceAction.sourceActivity:
			child.setAttribute('icon', 'history');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Änderungshistorie');
			break;
		case ResourceAction.receiverActivity:
			child.setAttribute('icon', 'history');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Änderungshistorie');
			break;
		case ResourceAction.groupEdit:
			child.setAttribute('icon', 'create');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Bearbeiten');
			break;
		case ResourceAction.groupActivity:
			child.setAttribute('icon', 'history');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Änderungshistorie');
			break;
		case ResourceAction.selectorEdit:
			child.setAttribute('icon', 'create');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Bearbeiten');
			break;
		case ResourceAction.selectorActivity:
			child.setAttribute('icon', 'history');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Änderungshistorie');
			break;
		case ResourceAction.resultEdit:
			child.setAttribute('icon', 'create');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Bearbeiten');
			break;
		case ResourceAction.resultDetail:
			child.setAttribute('icon', 'explore');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Weitere Info');
			break;
		case ResourceAction.resultActivity:
			child.setAttribute('icon', 'history');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Änderungshistorie');
			break;
		case ResourceAction.mailEdit:
			child.setAttribute('icon', 'send');
			child.setAttribute('color', '#2196f3');
			child.setAttribute('tooltip', 'Send Email');
			break;
		case ResourceAction.reportDetail:
			child.setAttribute('icon', 'explore');

			if(cell.row.data.status === ReportStatus.success){
				child.disable = true;
			}else{
				child.setAttribute('color', '#2196f3');
				child.setAttribute('tooltip', 'Weitere Info');
			}
			break;
		}

		child.setAttribute('type', elem);
		child.item = cell.row.data;

		return child;
	});

	// add error button if applicable
	var child;

	if (cell.row.data.status === SourceStatus.error) {
		child = document.createElement('data-list-item-icon');
		child.setAttribute('icon', 'announcement');
		child.setAttribute('color', '#f44336');
		child.setAttribute('tooltip', 'Fehler Anzeigen');
		child.setAttribute('type', ResourceAction.error);
		child.item = cell.row.data;
	}else {
		child = document.createElement('div');
		child.style.width = '40px';
	}

	children.unshift(child);
	children.forEach(function(child) {
		cell.element.appendChild(child)
	});
};

var activityTypeRenderer = function(cell) {
	var value = 'Ressource ';

	switch (cell.data) {
	case ActivityType.activated:
		value = value + 'aktiviert';
		break;
	case ActivityType.created:
		value = value + 'hinzugefügt';
		break;
	case ActivityType.deleted:
		value = value + 'gelöscht';
		break;
	case ActivityType.error:
		value = value + 'fehlerhaft';
		break;
	case ActivityType.modified:
		value = value + 'bearbeitet';
		break;
	case ActivityType.paused:
		value = value + 'pausiert';
		break;
	case ActivityType.updateURL:
		value = value + 'Url verändert';
		break;
	case ActivityType.validated:
		value = value + 'validiert';
		break;
	default:
		throw "Error, invalid activity type: " + cell.data;
	}

	cell.element.innerHTML = value;
};

var activityObjectTypeRenderer = function(cell) {

	switch (cell.data) {
	case ObjectType.source:
		value = 'Quellen';
		break;
	case ObjectType.receiver:
		value = 'Empfänger';
		break;
	case ObjectType.group:
		value = 'Gruppen';
		break;
	case ObjectType.selector:
		value = 'Selectoren';
		break;
	case ObjectType.mail:
		value = 'Mails';
		break;
	default:
		throw "Error, invalid Object type: " + cell.data;
	}

	cell.element.innerHTML = value;
};


var groupRenderer = function(cell) {
	var rendered = cell.data.map(function(elem) {
		var value = '';
		if (elem) {
			value = elem.name;
		}

		return value;
	});

	cell.element.innerHTML = rendered.join(', ');
};

var standardTimeRenderer = function(cell) {
	
	var date = new Date(cell.data);
	var rendered =   date.toLocaleString();
	cell.element.innerText = rendered;
};


var memberRenderer = function(cell) {
	var rendered = cell.data.map(function(elem) {
		var value = '';
		if (elem) {
			value = elem.name;
		}

		return value;
	});

	cell.element.innerHTML = rendered.length;
};

var statisRenderer = function(cell) {
	var statis = cell.data;
	
	cell.element.innerHTML = '';

	var child = document.createElement('p');
	child.innerHTML = 'Screenshot: '+statis.screenshot;

	// styling
	if(statis.screenshot>0)
		child.style.color = 'red';
	else child.style.color = '#2196F3';
	cell.element.appendChild(child);
	
	var child = document.createElement('p');
	child.innerHTML = 'Files: '+statis.fileStorage;

	// styling
	if(statis.fileStorage>0)
		child.style.color = 'red';
	else child.style.color = '#2196F3';
	
	child.style.paddingLeft = "20px";
	cell.element.appendChild(child);
	
	var child = document.createElement('p');
	child.innerHTML = ' Data Rows: '+statis.structure;

	// styling
	if(statis.structure>0)
		child.style.color = 'red';
	else child.style.color = '#2196F3';
	child.style.paddingLeft = "20px";
	cell.element.appendChild(child);
	
	var child = document.createElement('p');
	child.innerHTML = ' Followed Links: '+statis.followLinks;

	// styling
	if(statis.followLinks>0)
		child.style.color = 'red';
	else child.style.color = '#2196F3';
	child.style.paddingLeft = "20px";
	cell.element.appendChild(child);
	
	var child = document.createElement('p');
	child.innerHTML = ' Clean Text: '+statis.cleanText;

	// styling
	if(statis.cleanText>0)
		child.style.color = 'red';
	else child.style.color = '#2196F3';
	child.style.paddingLeft = "20px";
	cell.element.appendChild(child);
	
	var child = document.createElement('p');
	child.innerHTML = ' Formatted Text: '+statis.formattedText;

	// styling
	if(statis.formattedText>0)
		child.style.color = 'red';
	else child.style.color = '#2196F3';
	child.style.paddingLeft = "20px";
	cell.element.appendChild(child);
	
	
};

var actionSourceRenderer = function(cell) {
	var rendered = cell.data; 
	
	var link = rendered.sourceUrl;

	cell.element.innerHTML = '';

	var child = document.createElement('a');
	child.setAttribute('href', link);
	child.setAttribute('target', '_blank');
	child.innerHTML =  rendered.name;

	// styling
	child.style.color = '#2196F3';
	child.style.textDecoration = 'none';
	child.onmouseover = function() {
		this.style.textDecoration = 'underline';
	};

	child.onmouseout = function() {
		this.style.textDecoration = 'none';
	};

	cell.element.appendChild(child);
};

var sourceRenderer = function(cell) {
	var rendered = cell.data.map(function(elem) {
		var value = '';
		if (elem) {
			value = elem.url;
		}

		return value;
	});
	
	var link = '';
	if(rendered.length>0)
		link = rendered[0];

	cell.element.innerHTML = '';

	var child = document.createElement('a');
	child.setAttribute('href', link);
	child.setAttribute('target', '_blank');
	child.innerHTML = link;

	// styling
	child.style.color = '#2196F3';
	child.style.textDecoration = 'none';
	child.onmouseover = function() {
		this.style.textDecoration = 'underline';
	};

	child.onmouseout = function() {
		this.style.textDecoration = 'none';
	};

	cell.element.appendChild(child);
};

var reportMessageRenderer = function(cell) {
	var text = cell.data;
	
	cell.element.innerHTML = '';

	var child = document.createElement('textarea');
	child.setAttribute('cols', 50);
	child.setAttribute('rows', 2);
	child.innerHTML = text;

	cell.element.appendChild(child);
};
