/** Helper functions */

var extend = function(defaults, data) {
    return Object.keys(defaults).reduce(function(previous, key) {
        previous[key] = data[key] || defaults[key];
        return previous;
    }, {});
};

var isEmpty = function (obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
};

Object.values = function (obj) {
    var vals = [];
    for( var key in obj ) {
        if ( obj.hasOwnProperty(key) ) {
            vals.push(obj[key]);
        }
    }
    return vals;
};

var getWidth = function(element) {
    var width = element.offsetWidth;
    var padding = parseInt(getCssValue(element, 'padding-right').replace("px", "")) +
        parseInt(getCssValue(element, 'padding-left').replace("px", ""));

    // remove element padding
    return width - padding;
};

var getCssValue = function (oElm, strCssRule){
    var strValue = "";
    if(document.defaultView && document.defaultView.getComputedStyle){
        strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
    }
    else if(oElm.currentStyle){
        strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
            return p1.toUpperCase();
        });
        strValue = oElm.currentStyle[strCssRule];
    }
    return strValue;
};

var resizeGrid = function(grid) {
    // update column width
    var width = getWidth(grid);
    var staticElements = grid.columns.filter(function(column) { return column.flex === -1});
    var widthWithoutStatic = width - staticElements.reduce(function(prev, elem) {return prev + elem.width}, 0);
    var dynamicElements = grid.columns.filter(function(column) { return column.flex !== -1 });
    var combinedFlexWeight = dynamicElements.reduce(function(prev, elem) {return prev + elem.flex}, 0);

    var flexWeight = widthWithoutStatic / combinedFlexWeight;
    var residual = widthWithoutStatic % combinedFlexWeight;

    dynamicElements.forEach(function(column) {
        column.width = column.flex * flexWeight;
    });

    if(dynamicElements) {
        dynamicElements[dynamicElements.length - 1].width += residual;
    }
};

var getObjectTypeForPage = function(page) {
    switch(page) {
        case Pages.sources:
            return ObjectType.source;
        case Pages.receivers:
            return ObjectType.receiver;
        case Pages.groups:
            return ObjectType.group;
        case Pages.selectors:
            return ObjectType.selector;
        case Pages.results:
            return ObjectType.result;
        case Pages.mails:
            return ObjectType.mail;
        case Pages.monitoring:
            return ObjectType.monitoring;
        case Pages.logbook:
            return ObjectType.logbook;
        default:
            throw "Error, unknown page type";
    }
};