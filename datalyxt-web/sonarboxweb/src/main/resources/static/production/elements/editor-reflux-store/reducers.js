function templateName(state, action) {
    state = typeof state !== 'undefined' ?  state : "";

    switch (action.type) {
        case RENAME_TEMPLATE:
            return action.name;
            break;
        case RESET_EDITOR:
            return "";
        default:
            return state;
    }
}

function viewType(state, action) {
    state = typeof state !== 'undefined' ?  state : {
        viewType: viewType.website,
        isWebsiteView: true,
        isGridView: false
    };

    switch(action.type) {
        case CHANGE_VIEW_TYPE:
            return {
                viewType: action.viewType,
                isWebsiteView: action.isWebsiteView,
                isGridView: action.isGridView
            };
        case RESET_EDITOR:
            return {
                viewType: viewType.website,
                isWebsiteView: true,
                isGridView: false
            };
        default:
            return state;
    }
}

function markerEnabled(state, action) {
    state = typeof state !== 'undefined' ?  state : false;

    switch(action.type) {
        case CHANGE_MARKER_ENABLED:
            return action.value;
        case RESET_EDITOR:
            return false;
        default:
            return state;
    }
}

function fetchOptions(state, action) {
    state = typeof state !== 'undefined' ? state : {
        enrichTextNodes: true,
        enrichTextNodesJavascript: false,
        removeCss: false,
        removeIds: false,
        removeHeaderJavascript: true,
        removeBodyJavascript: true
    };

    switch(action.type) {
        case CHANGE_FETCH_OPTION:
            return Object.assign({}, state, {
                [action.option]: action.value
            });
        case RESET_EDITOR:
            return {
                enrichTextNodes: true,
                enrichTextNodesJavascript: false,
                removeCss: false,
                removeIds: false,
                removeHeaderJavascript: true,
                removeBodyJavascript: true
            };
        default:
            return state;
    }

}

function activeColumn(state, action) {
    state = typeof state !== 'undefined' ?  state : -1;

    switch(action.type) {
        case CHANGE_ACTIVE_COLUMN:
            return action.index;
        case RESET_EDITOR:
            return -1;
        default:
            return state;
    }
}

function editorColumn(state, action) {
    state = typeof state !== 'undefined' ?  state : {};

    switch(action.type) {
        case ADD_COLUMN:
            return {
                id: action.id,
                name: action.name,
                type: action.columnType,
                color: action.color,
                elements: action.elements
            };
        case RENAME_COLUMN:
            return Object.assign({}, state, {
                name: action.name
            });
        case CHANGE_COLUMN_TYPE:
            return Object.assign({}, state, {
                type: action.columnType
            });
        case SET_ELEMENTS:
            return Object.assign({}, state, {
                elements: action.elements
            });
        case REMOVE_ELEMENT:
            return Object.assign({}, state, {
                elements: [
                    ...state.element.slice(0, action.index),
                    ...state.elements.slice(action.index + 1)
                ]
            });
        default:
            return state;
    }
}

function editorColumns(state, action) {
    state = typeof state !== 'undefined' ?  state : [];

    switch(action.type) {
        case ADD_COLUMN:
            return [
                ...state,
                editorColumn(undefined, action)
            ];
        case REMOVE_COLUMN:
            return [
                ...state.slice(0, action.index),
                ...state.slice(action.index + 1)
            ];
        case RENAME_COLUMN:
        case CHANGE_COLUMN_TYPE:
        case SET_ELEMENTS:
        case REMOVE_ELEMENT:
            return [
                ...state.slice(0, action.index),
                editorColumn(state[action.index], action),
                ...state.slice(action.index + 1)
            ];
        case RESET_EDITOR:
            return [];
        default:
            return state;
    }
}

function editorAction(state, action) {
    state = typeof state !== 'undefined' ?  state : {};

    switch(action.type) {
        case ADD_ACTION:
            return {
                valid: true,
                columnIndex: action.columnIndex,
                actionType: action.actionType,
                parameters: action.parameters
            };
        case EDIT_ACTION:
            return Object.assign({}, state, {
                columnIndex: action.columnIndex,
                actionType: action.actionType,
                parameters: action.parameters
            });
        case INVALIDATE_ACTION:
            return Object.assign({}, state, {
                valid: false
            });
        default:
            return state;
    }
}

function editorActions(state, action) {
    state = typeof state !== 'undefined' ?  state : [];

    switch(action.type) {
        case ADD_ACTION:
            return [
                ...state,
                editorAction(undefined, action)
            ];
        case REMOVE_ACTION:
            return [
                ...state.slice(0, action.index),
                ...state.slice(action.index + 1)
            ];
        case EDIT_ACTION:
        case INVALIDATE_ACTION:
            return [
                ...state.slice(0, action.index),
                editorAction(state[action.index], action),
                ...state.slice(action.index + 1)
            ];
        default:
            return state;
    }
}

function actionDialogName(state, action) {
    state = typeof state !== 'undefined' ?  state : "Aktionen";

    switch(action.type) {
        case ACTION_DIALOG_CREATE_ACTION:
            return "Neue Aktion Anlegen";
        case ACTION_DIALOG_EDIT_ACTION:
            return "Aktion Bearbeiten";
        case EDIT_ACTION:
        case ADD_ACTION:
        case ACTION_DIALOG_ABORT:
        case ACTION_DIALOG_CLOSE:
            return "Aktionen";
        default:
            return state;
    }
}

function actionDialogOpened(state, action) {
    state = typeof state !== 'undefined' ?  state : false;

    switch(action.type) {
        case ACTION_DIALOG_OPEN:
            return true;
        case ACTION_DIALOG_CLOSE:
            return false;
        default:
            return state;
    }
}

function actionDialogViewType(state, action) {
    state = typeof state !== 'undefined' ?  state : {
        viewType: ActionDialogViewType.list,
        isListView: true,
        isEditView: false
    };

    switch(action.type) {
        case ACTION_DIALOG_CREATE_ACTION:
        case ACTION_DIALOG_EDIT_ACTION:
            return {
                viewType: ActionDialogViewType.edit,
                isListView: false,
                isEditView: true
            };
        case EDIT_ACTION:
        case ADD_ACTION:
        case ACTION_DIALOG_ABORT:
        case ACTION_DIALOG_CLOSE:
            return {
                viewType: ActionDialogViewType.list,
                isListView: true,
                isEditView: false
            };
        default:
            return state;
    }
}

function actionDialogSelected(state, action) {
    state = typeof state !== 'undefined' ?  state : -1;

    switch(action.type) {
        case ACTION_DIALOG_EDIT_ACTION:
            return action.index;
        case EDIT_ACTION:
        case ACTION_DIALOG_ABORT:
        case ACTION_DIALOG_CLOSE:
            return -1;
        default:
            return state;
    }
}

const editorReducer = Redux.combineReducers({
    templateName,
    fetchOptions,
    markerEnabled,
    viewType,
    activeColumn,
    editorColumns,
    editorActions,
    actionDialogName,
    actionDialogOpened,
    actionDialogViewType,
    actionDialogSelected
});