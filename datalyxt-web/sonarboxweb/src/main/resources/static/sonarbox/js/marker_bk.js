prevElement = null;

var mark;

function textNodesUnder(el) {
	var n, a = [], walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT,
			null, false);
	while (n = walk.nextNode())
		a.push(n);
	return a;
}
function check() {
	var all = textNodesUnder(document.body);
	for (i = 0; i < all.length; i++) {
		var node = all[i];
		var markable = true;
		if (node.nextSibling) {

			markable = false;
		}
		if (node.previousSibling) {

			markable = false;
		}
		if (!markable)
			if (node.nodeValue) {
				if (node.nodeValue.trim().length) {
					var parentNode = node.parentNode;
					if (parentNode) {
						if (parentNode.nodeType == 1
								&& parentNode.nodeName.toLowerCase() != "dlyxtspan") {
							$(node).wrap("<dlyxtspan></dlyxtspan>")

						}
					}
				}
			}
	}
}

function activeMark() {
	if (mark == null)
		createMarkDiv();
	else {
		var elementExists = document.getElementById("dlyxtdiv");
		if (elementExists == null)
			document.body.appendChild(mark);
	}
}
function disableMark() {
	if (mark != null)
		mark.remove();
}
function fullPath(el) {
	var names = [];
	while (el.parentElement) {

		if (el == el.ownerDocument.documentElement)
			names.unshift(el.tagName);
		else {
			var theparent = el.parentElement;
			if (theparent != null) {
				var count = $(theparent).children().length;
				if (count > 1) {
					for (var c = 1, e = el; e.previousElementSibling; e = e.previousElementSibling, c++)
						;
					names.unshift(el.tagName.toLowerCase() + ":nth-child(" + c
							+ ")");
				} else
					names.unshift(el.tagName.toLowerCase());
			}

		}
		el = el.parentElement;

	}
	return names.join(" > ");
}

function createMarkDiv() {
	mark = document.createElement("DIV");
	mark.setAttribute("id", "dlyxtdiv");
	mark.style.background = "#FF0000";
	// mark.style.width = "200px";
	// mark.style.height = "50px";
	mark.style.position = "absolute";
	mark.style.zIndex = "10000";
	mark.style.opacity = 0.6;
	mark.style.pointerEvents = "none";
	document.body.appendChild(mark);

}

$(document).mousemove(function(e) {
	if (parent.enableMarker) {
		if (mark == null)
			createMarkDiv();

		var elem = e.target || e.srcElement;

		var position = $(elem).offset();
		var rect;
		rect = elem.getBoundingClientRect();

		var myleft = rect.left + window.pageXOffset;
		var mytop = rect.top + window.pageYOffset;
		var mywidth = rect.right - rect.left;
		var myheight = rect.bottom - rect.top;

		parent.notifyParent(rect.left, rect.top, mywidth, myheight);
		//	 
		//

		$("#dlyxtdiv").css({
			left : myleft,
			top : mytop,
			width : mywidth,
			height : myheight
		});

		prevElement = elem;


	}
});


document.addEventListener('click', function(e) {
	if (parent.enableMarker) {
		if (e.button == 2)
			rightMouseButtonHandler(e);
		else
			leftMouseButtonHandler(e);

	} else {
		var elem = e.target || e.srcElement;

		var url;
		while (elem) {
			if (elem.nodeName.toLowerCase() == 'a') {
				url = $(elem).attr('href');
				break;
			}
			elem = elem.parentElement;
		}
		if (url) {
			parent.setLink(url);
		}
		// e.preventDefault();
		// e.stopPropagation();
	}
}, false);

document.addEventListener('contextmenu', function(e) {
	rightMouseButtonHandler(e);
}, false);

function leftMouseButtonHandler(e) {

	var elem = e.target || e.srcElement;
	var csspath = fullPath(elem);
	var markblock = new markedBlock(elem, elem.style.opacity,
			elem.style.opacity, csspath);

	parent.saveBlock(markblock);
	addCover(elem);

	e.preventDefault();
	e.stopPropagation();
}

function rightMouseButtonHandler(e) {
	var elem = e.target || e.srcElement;
	var csspath = fullPath(elem);

	var markedblock = parent.removeBlock(csspath);
	if (markedblock != null)
		removeCover(markedblock, elem);
	e.preventDefault();
	e.stopPropagation();
}

function addCover(elem) {

	elem.style.opacity = 0.5;
	elem.style.background = "#00FF00";

}

function removeCover(markedblock, elem) {

	elem.style.opacity = markedblock.oldOpacity;
	elem.style.background = markedblock.oldBackground;

}
