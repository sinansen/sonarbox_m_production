//prevElement = null;
//
var mark;
var localTest=false;
var markerEnabled = false;
var templates = [];
var columns = [];
var singleElementPath = [];
var markedElements = [];

var columnTemplates = {
    columnIndex: '',
    rowTemplate: []
}
var tmpSelector;
var selectedIndex;
var showAllColumns = true;
var rowTemplate = {
    post_iden: [],
    iden_index: -1,
    iden: "",
    pre_iden: []
};

var push=false;

function ColumnTemplate(cssPath, preIden, idenIndex, iden, postIden, type, elements) {
    this.cssPath = cssPath;
    this.preIden = preIden;
    this.idenIndex = idenIndex;
    this.iden = iden;
    this.postIden = postIden;
    this.type = type;
    this.elements = elements;
}

/** Message passing */
window.addEventListener('message', function (event) {
    var payload = JSON.parse(event.data);
    switch (payload.action) {
	    case 'update-columns':
	    	unmarkElements();
	    	tmpSelector = $("*");
	    	if(payload.columns.length>0){
	    		columns=payload.columns;
		    	push=false;
		    	updateColumns();
		    	push=true;
	    	}

	    	break;
	    case 'update-active-column':
	    	push=true;
	    	selectedIndex = payload.selectedIndex;
	    	break;
	    case 'change-marker-enabled':
	    	push=true;
	    	markerEnabled = payload.enabled;
	    	if(markerEnabled){
	    		activeMark();
	    	}else{
	    		disableMark();
	    	}
	    	break;
	    case 'init':
	    	unmarkElements()
	    	push=false;
	    	if(payload.columns.length>0){
	    		columns=payload.columns;
		    
		    	updateColumns();
		    	push=true;
		    	markerEnabled = payload.enabled;
		    	if(markerEnabled){
		    		activeMark();
		    	}else{
		    		disableMark();
		    	}
		    	selectedIndex = payload.selectedIndex;
	    	}
	    	break;
	
	    default:
	        throw "Error, unknown parent message received in marker:";
    }

}, false);


// fire message when finished loading
if(localTest){
	parent.postMessage(JSON.stringify({
	    action: 'loading-finished'
	}), '*');
}

function updateColumns() {
    for (var i = 0; i < columns.length; i++) {
        selectedIndex = i;
        if(columns[i].elements!=null){
	        if (columns[i].elements.length > 0) {
	           calculateRowTemplate();
	        }
        }
    }
}

function templateExists(newTemplate, type) {
    for (var i = 0; i < templates.length; i++) {
        if (type === "pip") {
            if (templates[i].preIden.replace(/\(\d+\)/g, "()") === newTemplate.preIden.replace(/\(\d+\)/g, "()") &&
                templates[i].idenIndex === newTemplate.idenIndex &&
                templates[i].iden.replace(/\(\d+\)/g, "()") === newTemplate.iden.replace(/\(\d+\)/g, "()") &&
                templates[i].postIden.replace(/\(\d+\)/g, "()") === newTemplate.postIden.replace(/\(\d+\)/g, "()") &&
                templates[i].type === newTemplate.type) {

                return true;
            }
        } else if (type === "pipost") {
            if (templates[i].preIden.replace(/\(\d+\)/g, "()") === newTemplate.preIden.replace(/\(\d+\)/g, "()") &&
                templates[i].idenIndex === newTemplate.idenIndex &&
                templates[i].iden.replace(/\(\d+\)/g, "()") === newTemplate.iden.replace(/\(\d+\)/g, "()") &&
                templates[i].postIden === newTemplate.postIden &&
                templates[i].type === newTemplate.type) {
                return true;
            }

        } else if (type === "preip") {
            if (templates[i].preIden === newTemplate.preIden &&
                templates[i].idenIndex === newTemplate.idenIndex &&
                templates[i].iden.replace(/\(\d+\)/g, "()") === newTemplate.iden.replace(/\(\d+\)/g, "()") &&
                templates[i].postIden.replace(/\(\d+\)/g, "()") === newTemplate.postIden.replace(/\(\d+\)/g, "()") &&
                templates[i].type === newTemplate.type) {
                return true;
            }

        } else {
            if (templates[i].preIden === newTemplate.preIden &&
                templates[i].idenIndex === newTemplate.idenIndex &&
                templates[i].iden.replace(/\(\d+\)/g, "()") === newTemplate.iden.replace(/\(\d+\)/g, "()") &&
                templates[i].postIden === newTemplate.postIden &&
                templates[i].type === newTemplate.type) {
                return true;
            }
        }
    }
    return false;
}

function singleElementCollision(newTemplate, type, path) {

    if (type === "pip") {
        for (var i = 0; i < columns.length; i++) {

            for (var j = 0; j < columns[i].elements.length; j++) {
                var elements = columns[i].elements[j].path.split(">");
                if (i !== selectedIndex) {
                    if (columns[i].elements[j].path.replace(/\(\d+\)/g, "()") === columns[i].elements[j].path.replace(/\(\d+\)/g, "()")) {
                        alert("Template collision pip");
                        removePath(newTemplate.cssPath);
                        return true;
                    }
                }
                if (elements.length > path.length) {
                    var preIdenE = elements.slice(0, newTemplate.idenIndex).join(">");
                    var preIdenP = path.slice(0, newTemplate.idenIndex).join(">");

                    var tmp1 = preIdenE.replace(/\(\d+\)/g, "()");
                    var tmp2 = preIdenP.replace(/\(\d+\)/g, "()");
                    if (tmp1 === tmp2) {
                        removePath(path);
                        alert("Template collision pip");
                        return true;
                    }
                }
            }
        }
    } else if (type === "pipost") {
        for (var i = 0; i < columns.length; i++) {

            for (var j = 0; j < columns[i].elements.length; j++) {
                var elements = columns[i].elements[j].path.split(">");
                if (i !== selectedIndex) {
                    if (newTemplate.elements.length === elements.length) {
                        var preIden = elements.slice(0, newTemplate.idenIndex + 1).join(">");
                        var iden = elements[newTemplate.indexIndex];
                        var postIden = elements.slice(newTemplate.idenIndex + 1, elements.length).join(">");
                        if (newTemplate.preIden.replace(/\(\d+\)/g, "()") === preIden &&
                            newTemplate.iden.replace(/\(\d+\)/g, "()") === iden.replace(/\(\d+\)/g, "()") &&
                            newTemplate.postIden === newTemplate.postIden) {
                            alert("Template collision pipost");
                            removePath(path);
                            return true;
                        }
                    }
                }
                if (elements.length > path.length) {
                    var preIdenE = elements.slice(0, newTemplate.idenIndex).join(">");
                    var postIdenE = elements.slice(newTemplate.idenIndex + 1, elements.length).join(">");
                    var preIdenP = path.slice(0, newTemplate.idenIndex).join(">");
                    var postIdenP = path.slice(newTemplate.idenIndex + 1, path.length).join(">");

                    var tmp1 = preIdenE.replace(/\(\d+\)/g, "()") + ">" + postIdenE;
                    var tmp2 = preIdenP.replace(/\(\d+\)/g, "()") + ">" + postIdenP;
                    if (tmp1 === tmp2) {
                        removePath(path);
                        alert("Template collision pipost");
                        return true;
                    }
                }
            }

        }
    } else if (type === "preip") {
        for (var i = 0; i < columns.length; i++) {
            for (var j = 0; j < columns[i].elements.length; j++) {
                var elements = columns[i].elements[j].path.split(">");
                if (newTemplate.elements.length === elements.length) {
                    if (i !== selectedIndex) {
                        var preIden = elements.slice(0, newTemplate.idenIndex).join(">");
                        var iden = elements[newTemplate.idenIndex];
                        var postIden = elements.slice(newTemplate.idenIndex, elements.length).join(">");
                        if (newTemplate.preIden === preIden &&
                            newTemplate.iden.replace(/\(\d+\)/g, "()") === iden.replace(/\(\d+\)/g, "()") &&
                            newTemplate.postIden.replace(/\(\d+\)/g, "()") === postIden.replace(/\(\d+\)/g, "()")) {
                            alert("Template collision preip");
                            removePath(path);
                            return true;
                        }
                    }
                }
                if (elements.length > path.length) {
                    var preIdenE = elements.slice(0, newTemplate.idenIndex).join(">");
                    var idenE = elements[newTemplate.idenIndex];


                    var preIdenP = path.slice(0, newTemplate.idenIndex).join(">");
                    var idenP = path[newTemplate.idenIndex];


                    var tmp1 = preIdenE + ">" + idenE.replace(/\(\d+\)/g, "()");
                    var tmp2 = preIdenP + ">" + idenP.replace(/\(\d+\)/g, "()");
                    if (tmp1 === tmp2) {
                        removePath(path);
                        alert("Template collision preip");
                        return true;
                    }
                }
            }
        }
    } else {
        for (var i = 0; i < columns.length; i++) {

            for (var j = 0; j < columns[i].elements.length; j++) {
                var elements = columns[i].elements[j].path.split(">");
                if (i !== selectedIndex) {
                    if (newTemplate.elements.length === elements.length) {
                        var preIden = elements.slice(0, newTemplate.idenIndex).join(">");
                        var iden = elements[newTemplate.idenIndex];
                        var postIden = elements.slice(newTemplate.idenIndex + 1, elements.length).join(">");
                        if (newTemplate.preIden === preIden &&
                            newTemplate.iden.replace(/\(\d+\)/g, "()") === iden.replace(/\(\d+\)/g, "()") &&
                            newTemplate.postIden === postIden) {
                            alert("Template collision preidpost ");
                            removePath(path);
                            return true;
                        }
                    }
                }
                if (elements.length > path.length) {
                    var preIdenE = elements.slice(0, newTemplate.idenIndex).join(">");
                    var idenE = elements[newTemplate.idenIndex];
                    var postIdenE = elements.slice(newTemplate.idenIndex + 1, elements.length).join(">");
                    var preIdenP = path.slice(0, newTemplate.idenIndex).join(">");
                    var idenP = path[newTemplate.idenIndex];
                    var postIdenP = elements.slice(newTemplate.idenIndex + 1, path.length).join(">");
                    var tmp1 = preIdenE + ">" + idenE.replace(/\(\d+\)/g, "()") + ">" + postIdenE;
                    var tmp2 = preIdenP + ">" + idenP.replace(/\(\d+\)/g, "()") + ">" + postIdenP;
                    if (tmp1 === tmp2) {
                        removePath(path);
                        alert("Template collision preidpost");
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

function removePath(cssPath) {
	var index=-1;
	alert("remove path");
	if(columns[selectedIndex].elements!=null){
		for(var i=0;i<columns[selectedIndex].elements.length;i++){
			if(columns[selectedIndex].elements[i].path===cssPath.join('>')){
				alert("removed path");
				index=i;
				break;
			}
		}
		if(index>-1){
			alert("remove path 2");
			columns[selectedIndex].elements.splice(index, 1);
		}
	}
}


function singleElementExists(cssPath) {

	if(singleElementPath.length>0){
		for(var i=0;i<singleElementPath.length;i++){
		    if (singleElementPath[i].path===cssPath) {
		        return true;
		    } else {
		        return false;
		    }
		}
	}else{
		return false;
	}
}

function resetTemplates() {
    singleElementPath = [];
    templates = [];

}

function calculateRowTemplate() {

    var elements=[];
    for(var i=0;i<columns[selectedIndex].elements.length;i++){
    	if(columns[selectedIndex].elements[i].selected){
    		elements.push(columns[selectedIndex].elements[i]);
    	}
    }
    columns[selectedIndex].elements = elements;
    resetTemplates();
    var path1 = [];
    var path2 = [];
    var idenIndex;
    var templateFound = false;;

    var cssPath;
    var preIden = "";
    var idenIndex = -1;
    var iden = "";
    var postIden = "";
    var type = "";
    if (columns[selectedIndex].elements.length >= 2) {
        for (var i = 0; i < columns[selectedIndex].elements.length; i++) {
            path1 = columns[selectedIndex].elements[i].path.split(">");
            templateFound = false;

            cssPath;
            preIden = "";
            idenIndex = -1;
            iden = "";
            postIden = "";
            type = "";

            cssPath = columns[selectedIndex].elements[i].path;
            for (var j = 0; j < columns[selectedIndex].elements.length; j++) {
                path2 = columns[selectedIndex].elements[j].path.split(">");

                if (path1.length === path2.length) {
                    for (var k = 0; k < path1.length; k++) {

                        if (path1[k] !== path2[k]) {


                            if (path1[k].replace(/\(\d+\)/g, "()") === path2[k].replace(/\(\d+\)/g, "()")) {
                                idenIndex = k;
                                iden = path1[k];
                                if (k == 0) {
                                    preIden = path1[k];
                                    if (k === path1.length - 1) {
                                        postIden = path1[k];
                                        type = "pip"; //pre==iden==post Single-Element-Iteration pre, iden and post are the same element

                                        var ct = new ColumnTemplate(cssPath, preIden, idenIndex, iden, postIden, type, path1);
                                        templateFound = true;
                                        if (templateExists(ct, type) === false){
                                        	if(singleElementCollision(ct, type, path2) === false) {
                                                templates.push(ct);
                                        	}else{
                                        		templateFound = false;
                                        	}
                                        }
                                        
                                        break;

                                    } else {
                                        type = "piPost"; //pre=iden,post Pre and Iden are the same element followed by a post element
                                        var samePost = true;
                                        for (var l = k + 1; l < path1.length; l++) {
                                            if (path1[l] !== path2[l]) {
                                                samePost = false;
                                                break;
                                            }
                                        }

                                        if (samePost === true) {
                                            postIden = (path1.slice(k + 1, path1.length - 1)).join(">");
                                            var ct = new ColumnTemplate(cssPath, preIden, idenIndex, iden, postIden, type, path1);
                                            templateFound = true;
                                            if (templateExists(ct, type) === false){
                                            	if(singleElementCollision(ct, type, path2) === false) {
                                                    templates.push(ct);
                                            	}else{
                                            		templateFound = false;
                                            	}
                                            }
                                            break;
                                        }

                                    }
                                } else if (k === path1.length - 1) {
                                    type = "preip"; //pre, iden==post --> iden ist last element and is equal to post element

                                    if (path1[k].replace(/\(\d+\)/g, "()") === path2[k].replace(/\(\d+\)/g, "()")) {
                                        preIden = (path1.slice(0, k)).join(">");
                                        idenIndex = k;
                                        iden = path1[k];
                                        postIden = path1[k];

                                        var ct = new ColumnTemplate(cssPath, preIden, idenIndex, iden, postIden, type, path1);
                                        templateFound = true;
                                        if (templateExists(ct, type) === false){
                                        	if(singleElementCollision(ct, type, path2) === false) {
                                                templates.push(ct);
                                        	}else{
                                        		templateFound = false;
                                        	}
                                        }
                                        break;
                                    }


                                } else {
                                    type = "preIdenPost"; //pre, iden, post are different elements
                                    preIden = (path1.slice(0, k)).join(">");
                                    idenIndex = k;
                                    iden = path1[k];

                                    var samePost = true;
                                    for (var l = k + 1; l < path1.length; l++) {
                                        if (path1[l] !== path2[l]) {
                                            samePost = false;
                                            break;
                                        }
                                    }

                                    if (samePost === true) {
                                        if (k + 1 === path1.length - 1) {
                                            postIden = path1[k + 1];
                                        } else {
                                            postIden = (path1.slice(k + 1, path1.length)).join(">");
                                        }

                                        var ct = new ColumnTemplate(cssPath, preIden, idenIndex, iden, postIden, type, path1);
                                        templateFound = true;
                                        if (templateExists(ct, type) === false){
                                        	if(singleElementCollision(ct, type, path2) === false) {
                                                templates.push(ct);
                                        	}else{
                                        		templateFound = false;
                                        	}
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }

            }
            if (templateFound === false) {
                if (singleElementExists(cssPath) === false) {
                	singleElementPath.push(columns[selectedIndex].elements[i]);
                }
            }
           
        }
    } else if (columns[selectedIndex].elements.length == 1) {
        singleElementPath.push(columns[selectedIndex].elements[0]);
    } else {
        console.log("No elements selected for current column");
    }
    changeCssStyle();
}

function refreshArray(cssPath) {
	var index=-1;
	for(var i=0;i<columns[selectedIndex].elements.length;i++){
		if(columns[selectedIndex].elements[i].path===cssPath){
			index=1;
		}
	}
    if (index > -1) {
        columns[selectedIndex].elements.splice(index, 1);
    }
}



function changeCssStyle() {

    var selector = [];
    var elements = [];
    for (var i = 0; i < templates.length; i++) {
        if (templates[i].type === "pip") {
            var s = templates[i].iden.replace(/\(\d+\)/g, "()");

        } else if (templates[i].type === "pipost") {
            var s = templates[i].iden.replace(/\(\d+\)/g, "()") + ">" + template[i].postIden;

        } else if (templates[i].type === "preip") {
            var s = templates[i].preIden + ">" + templates[i].iden.replace(/\(\d+\)/g, "()");

        } else {
            var s = templates[i].preIden + ">" + templates[i].iden.replace(/\(\d+\)/g, "()") + ">" + templates[i].postIden;

        }
        for (var j = 0; j < tmpSelector.length; j++) {
            var tmpSelElements = getCssPathElements(tmpSelector[j]);
            if (tmpSelElements.length == templates[i].elements.length) {
                var tmpIden = tmpSelElements[templates[i].idenIndex];
                tmpIden = tmpIden.replace(/\(\d+\)/g, "()");
                tmpSelElements[templates[i].idenIndex] = tmpIden;
                var t = tmpSelElements.join(">");
                if (t == s) {
                    selector.push(tmpSelector[j]);
                }
            }
        }
    }
    for (var i = 0; i < selector.length; i++) {
        var cssPath = (getCssPathElements(selector[i])).join(">");
        var index=-1;
        

        var e = document.querySelector(cssPath);
        addCover(e);
        var text = e.textContent;
        var html = $('<div>').append($(e).clone()).html();
        var link = getATag(e);

        var selected=false;
        for(var j=0;j<columns[selectedIndex].elements.length;j++){
        	if(columns[selectedIndex].elements[j].selected===true&&columns[selectedIndex].elements[j].path===cssPath){
        		selected=true;
        		break;
        	}
        	
        }

        var element = {
            path: cssPath,
            text: text,
            html: html,
            link: link,
            selected: selected
        };
	    elements.push(element);
       
    }
    for (var i = 0; i < singleElementPath.length; i++) {
    	var e = document.querySelector(singleElementPath[i].path);
        addCover(e);
        elements.push(singleElementPath[i]);
    }
    columns[selectedIndex].elements=elements;
   
    if(push===true){
    	 for(var i=0;i<elements.length;i++){
    	    	console.log(i +" SEND ELEMENT "+elements[i].path+" - "+elements[i].selected)
    	    }
	    if(elements.length>0){
		    parent.postMessage(JSON.stringify({
		        action: 'set-elements',
		        elements: elements,
		        selectedIndex: selectedIndex
		    }), '*');
	    }
    }
}

//helper functions below

function markedParentElementExists(cssPath) {
    for (var j = 0; j < columns.length; j++) {
    	if(columns[j].elements!=null){
	        for (var k = 0; k < columns[j].elements.length; k++) {
	            if ((columns[j].elements[k].path).length < cssPath.length) {
	                if (cssPath.includes(columns[j].elements[k].path)) {
	                    return true;
	                }
	            }
	        }
    	}
    }
    return false;
}

function markedChildElementExists(cssPath) {
    var element = document.querySelector(cssPath);
    var childElements = element.querySelectorAll("*");

    for (var i = 0; i < childElements.length; i++) {
        var tmp = getCssPathElements(childElements[i]).join(">");


        for (var j = 0; j < columns.length; j++) {
        	if(columns[j].elements!=null){
	        	for(var k=0;k<columns[j].elements.length;k++){
	        		if ((columns[j].elements[k].path===tmp)) {
	                    return true;
	                }	
	        	}
        	}
        }
    }
    return false;
}


function getATag(element) {
    var links = [];
    var href;
    if (element != null) {
        var tagName = element.tagName;
        //href ok
        //other variants like javascript embedded links should be handled

        if (tagName.toLowerCase() === 'a') {

            href = element.getAttribute("href");
            if (href != null) {
                if (href.length !== 0) {
                    links.push(href);
                }
            }
            return links;
        } else {
            var children = element.querySelectorAll('a[href]')
            for (var i = 0; i < children.length; i++) {
                href = children[i].getAttribute("href");
                if (href != null) {
                    if (href.length !== 0) {
                        links.push(href);
                    }
                }
            }
            if (links.length === 0) {
                while (element.tagName.toLowerCase() !== 'body' && element != null) {
                    element = element.parentElement;
                    if (element.tagName.toLowerCase() === 'a') {
                        href = element.getAttribute("href");
                        if (href != null) {
                            if (href.length !== 0) {
                                links.push(href);
                            }
                        }
                    }
                }
            }
        }
    }
    return links;
}

function unmarkElements() {
	for(var j=0;j<markedElements.length;j++){
		$(markedElements[j]).css({
            "color": "",
            "text-shadow": "",
            "border": ""
        });
	}
}

$(document).mouseleave(function (e) {
    hideDiv();
});

$(document).mouseenter(function (e) {
    showDiv();
});

var color;
$(document).mousemove(function (e) {
    var elem = e.target || e.srcElement;
    var path = getCssPathElements(elem).join(">");

    color = "#9aCd32";
    for (var i = 0; i < columns.length; i++) {
    	if(columns[i].elements!=null){
	    	for(var j=0;j<columns[i].elements.length;j++){
		        if (columns[i].elements[j].path===path&&columns[i].elements[j].selected===true) {
		            color = columns[i].color;
		            showMarker(e, columns[i].color);
		            break;
		        }
	    	}
    	}
    }
    showMarker(e, color);

});

function showMarker(e, c) {
    if (markerEnabled == true) {
        if (mark == null)
            createMarkDiv();

        var elem = e.target || e.srcElement;

        var position = $(elem).offset();
        var rect;
        rect = elem.getBoundingClientRect();

        var myleft = rect.left + window.pageXOffset;
        var mytop = rect.top + window.pageYOffset;
        var mywidth = rect.right - rect.left;
        var myheight = rect.bottom - rect.top;

        $("#dlyxtdiv").css({
            left: myleft,
            top: mytop,
            width: mywidth,
            height: myheight,
            border: "3px dotted " + c
        });
        //		prevElement = elem;
    }
}

document.addEventListener('click', function (e) {
    if (markerEnabled == true) {
        if (e.button == 2)
            rightMouseButtonHandler(e);
        else if (e.button == 0) {
            leftMouseButtonHandler(e);
        } else {

        }

    } else {
        var elem = e.target || e.srcElement;

        var url;
        while (elem) {
            if (elem.nodeName.toLowerCase() == 'a') {
                url = $(elem).attr('href');
                break;
            }
            elem = elem.parentElement;
        }
        if (url) {
        	if(localTest){
	            parent.postMessage(JSON.stringify({
	                action: 'url-change',
	                url: url
	            }), '*');
        	}

            e.preventDefault();
            e.stopPropagation();
        }
    }
}, false);

document.addEventListener('contextmenu', function (e) {
    rightMouseButtonHandler(e);
}, false);

function leftMouseButtonHandler(e) {
    var elem = e.target || e.srcElement;
    var cssPathElements = getCssPathElements(elem);
    var cssPath = cssPathElements.join(">");
    var newElement = true;
    var indexElement=-1;
    for(var i=0;i<columns[selectedIndex].elements.length;i++){
    	if(columns[selectedIndex].elements[i].path===cssPath){
    		if(columns[selectedIndex].elements[i].selected){
    			indexElement=i;
    		}
    		newElement=false;
    		break;
    	}
    }
    if (indexElement > -1) {
        curCssPath = cssPath;
        columns[selectedIndex].elements.splice(indexElement, 1);
        removeCover(elem);
        calculateRowTemplate();
        if(push===true){
	        parent.postMessage(JSON.stringify({
	            action: 'set-elements',
	            selectedIndex:selectedIndex,
	            elements: columns[selectedIndex].elements
	        }), '*');
        }
        newElement = false;
    }
    for (var i = 0; i < columns.length; i++) {
    	if(i!=selectedIndex){
	    	if(columns[i].elements!=null){
		    	for(var j=0;j<columns[i].elements.length;j++){
			        if (columns[i].elements[j].path===cssPath) {
			            newElement = false;
			            alert("Element bereits markiert und kann daher nicht mehr ausgewählt werden!");
			            break;
			        }
		    	}
	    	}
    	}
    }
    if (newElement === true) {
        if (markedParentElementExists(cssPath) === false && markedChildElementExists(cssPath) === false) {
        	 var text = elem.textContent;
             var html = $('<div>').append($(elem).clone()).html();
             var link = getATag(elem);

             var selected=true;
             
             var element = {
                 path: cssPath,
                 text: text,
                 html: html,
                 link: link,
                 selected: selected
             };
            columns[selectedIndex].elements.push(element);
            calculateRowTemplate();
        } 
    }
    e.preventDefault();
    e.stopPropagation();
}

function rightMouseButtonHandler(e) {
    e.preventDefault();
    e.stopPropagation();
}

function addCover(elem) {
    var children = $(elem).children();
    var tagName = elem.tagName;
    markedElements.push(elem);
    if ((tagName.toLowerCase()) == "img") {
    	
        $(elem).css({
            "border": "2px solid " + columns[selectedIndex].color
        });
    } else {
    	
        $(elem).css({
            "color": columns[selectedIndex].color,
            "background": "",
            "text-shadow": "-1px 1px 8px " + columns[selectedIndex].color + ", 1px -1px 8px " + columns[selectedIndex].color
        });
    }
    for (var i = 0; i < children.length; i++) {
        var tagName = children[i].tagName;
    	markedElements.push(children[i]);
        if ((tagName.toLowerCase()) == "img") {

            $(children[i]).css({
                "border": "2px solid " + columns[selectedIndex].color
            });
        } else {
            $(children[i]).css({
                "color": columns[selectedIndex].color,
                "text-shadow": "-1px 1px 8px " + columns[selectedIndex].color + ", 1px -1px 8px " + columns[selectedIndex].color
            });
        }
    }
}

function removeCover(elem) {
    $(elem).css({
        "color": "",
        "text-shadow": "",
        "border": ""
    });
    var index=markedElements.indexOf(elem);
    markedElements.splice(index,1);
    var children = $(elem).children();
    for (var i = 0; i < children.length; i++) {
        var tagName = children[i].tagName;
        var index=markedElements.indexOf(children[i]);
        markedElements.splice(index,1);
        
        if ((tagName.toLowerCase()) == "img") {
            $(children[i]).css({
                "border": ""
            });
        } else {
            $(children[i]).css({
                "color": "",
                "text-shadow": ""
            });
        }
    }
}

function showDiv() {
    if (markerEnabled == true) {
        if (document.getElementById("dlyxtdiv")) {
            document.getElementById("dlyxtdiv").style.display = "inline";
        }

    }
}

function hideDiv() {
    if (markerEnabled == true) {
        if (document.getElementById("dlyxtdiv")) {
            document.getElementById("dlyxtdiv").style.display = "none";
        }
    }
}

function activeMark() {
    if (mark == null)
        createMarkDiv();
    else {
        var elementExists = document.getElementById("dlyxtdiv");
        if (elementExists == null)
            document.body.appendChild(mark);
    }
}

function disableMark() {
    if (mark != null)
        mark.remove();
}

function getCssPathElements(el) {
    var pathElements = [];
    while (el.parentElement) {
        if (el == el.ownerDocument.documentElement)
            pathElements.unshift(el.tagName);
        else {
            var theparent = el.parentElement;
            if (theparent != null) {
                var count = $(theparent).children().length;
                if (count > 1) {
                    for (var c = 1, e = el; e.previousElementSibling; e = e.previousElementSibling, c++)
                    ;
                    pathElements.unshift(el.tagName.toLowerCase() + ":nth-child(" + c + ")");
                } else
                    pathElements.unshift(el.tagName.toLowerCase());
            }
        }
        el = el.parentElement;
    }
    return pathElements;
}


function createMarkDiv() {
    mark = document.createElement("div");
    mark.setAttribute("id", "dlyxtdiv");
    mark.style.border = "3px solid  #9aCd32 ";
    mark.style.display = "inline";
    mark.style.position = "absolute";
    mark.style.zIndex = "10000";
    mark.style.pointerEvents = "none";
    document.body.appendChild(mark);
}