package com.datalyxt.production.web.bean.email;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.web.bean.result.DataRowItemBean;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;

public class EmailDetailBean {
	public long id;
	public String subject;
	public List<EmailReceiverBean> receivers = new ArrayList<>();
	public List<List<DataRowItemBean>> rows= new ArrayList<>();
	public List<ActionResultFile> files = new ArrayList<>();
	public List<ActionResultFile> images = new ArrayList<>();

}

