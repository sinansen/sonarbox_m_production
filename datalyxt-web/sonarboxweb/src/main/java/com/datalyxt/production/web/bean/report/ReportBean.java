package com.datalyxt.production.web.bean.report;


public class ReportBean {

	public long id;
	public long scanId;
	public long scanStart;
	public long scanEnd;
	public String selectorName;
	public long selectorId;
	public String sourceName;
	public long sourceId;
	public String status;
	public String sourceUrl;
	public String createdId;
	public boolean hasError = false;

}
