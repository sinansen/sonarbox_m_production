package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.web.bean.receiver.ReceiverQueryScript;

public class ReceiverQueryBuilder {
	public static QueryWrapper buildQueryForSearch(ReceiverQueryScript script) {
		String sqlBasePart = "select * from m_receiver  ";
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( first_name like ? or  last_name like  ? or e_mail like ? or company like ? or function like ? or department like ? or category like ? ) ";
		String conditionWithStatus = " status =  ";
		String conditionWithStartTime = " last_modified >= ? ";
		String conditionWithEndTime = " last_modified <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = false;


		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.status != null && !script.status.isEmpty()) {
			List<String> statusParams = new ArrayList<String>();
			for (M_ReceiverStatus s : script.status) {
				String param = conditionWithStatus + s.name() + " ";
				statusParams.add(param);
			}
			String parms = String.join(" or ", statusParams);
			parms = " ( " + parms + " ) ";
			if (hasWhere)
				sql = sql + combination + parms;
			else {
				sql = sql + where + parms;
				hasWhere = true;
			}
		}
		if (script.endTimeModified > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTimeModified));
			params.add(new Timestamp(script.endTimeModified));
		}

		sql = sql + orderBy;

		if (script.sortByCreated)
			sql = sql + " created_at ";
		else if (script.sortByEmail)
			sql = sql + " e_mail ";
		else if (script.sortByName)
			sql = sql + " name ";
		else
			sql = sql + " last_modified ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

	public static QueryWrapper buildQueryForModify(M_Receiver receiver) {
		String sql = "update  m_receiver set address = ? , first_name = ? , last_name = ? , e_mail = ?, md5 = ?, status = ?, company = ?, function = ? , department = ?, category = ?, last_modified = ? where id = ? ";

		ArrayList<Object> params = new ArrayList<>();
		params.add(receiver.address);
		params.add(receiver.firstName);
		params.add(receiver.lastName);
		params.add(receiver.email);
		String md5 = DigestUtils.md5Hex(receiver.email);
		params.add(md5);
		params.add(receiver.status.name());
		params.add(receiver.company);
		params.add(receiver.function);
		params.add(receiver.department);
		params.add(receiver.category);
		params.add(new Timestamp(receiver.lastModified));

		params.add(receiver.id);

		QueryWrapper wrapper = new QueryWrapper();

	

		Object[] array = params.toArray();

		wrapper.params = array;
		wrapper.sql = sql;

		return wrapper;
	}

}
