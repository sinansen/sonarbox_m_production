package com.datalyxt.production.web.exception;

public class UserDatabaseAccessException extends RuntimeException  {

	public UserDatabaseAccessException(String msg) {
		super(msg);
	}

	public UserDatabaseAccessException(String msg, Exception e) {
		super(msg, e);
	}
}
