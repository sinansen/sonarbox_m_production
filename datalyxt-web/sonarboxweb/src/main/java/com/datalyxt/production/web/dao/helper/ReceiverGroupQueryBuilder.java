package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.receiver.M_ReceiverGroup;
import com.datalyxt.production.receiver.M_ReceiverGroupStatus;
import com.datalyxt.production.web.bean.receiver.ReceiverGroupQueryScript;

public class ReceiverGroupQueryBuilder {

	public static QueryWrapper buildQueryForModify(M_ReceiverGroup receiverGroup) {
		String sql = "update  m_receiver_group set name = ? , description =  ? , status = ? , last_modified = ?  where id = ? and owner_id = ? ";

		ArrayList<Object> params = new ArrayList<>();

		params.add(receiverGroup.name);
		params.add(receiverGroup.description);
		params.add(receiverGroup.status.name());
		params.add(new Timestamp(receiverGroup.lastModified));

		params.add(receiverGroup.id);
		params.add(receiverGroup.owner);

		QueryWrapper wrapper = new QueryWrapper();

		Object[] array = params.toArray();

		wrapper.params = array;
		wrapper.sql = sql;

		return wrapper;
	}

	public static QueryWrapper buildQueryForSearch(
			ReceiverGroupQueryScript script) {
		String sqlBasePart = "select id, name, description , status , created_at, last_modified from m_receiver_group  ";
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( name like ? or  description like  ? ) ";
		String conditionWithStatus = " status =  ";
		String conditionExcludeStatus = " status != ? ";
		String conditionWithStartTime = " last_modified >= ? ";
		String conditionWithEndTime = " last_modified <= ? ";

		String subSql = "group_id in (select group_id from m_receiver_group_mapping where receiver_id = ? )";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = false;

		sql = sql + where + conditionExcludeStatus;
		params.add(M_ReceiverGroupStatus.deleted.name());
		hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.status != null && !script.status.isEmpty()) {
			List<String> statusParams = new ArrayList<String>();
			for (M_ReceiverGroupStatus s : script.status) {
				String param = conditionWithStatus + s.name() + " ";
				statusParams.add(param);
			}
			String parms = String.join(" or ", statusParams);
			parms = " ( " + parms + " ) ";
			if (hasWhere)
				sql = sql + combination + parms;
			else {
				sql = sql + where + parms;
				hasWhere = true;
			}
		}
		if (script.endTimeModified > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTimeModified));
			params.add(new Timestamp(script.endTimeModified));
		}

		if (script.receiverId > 0) {
			if (hasWhere)
				sql = sql + combination + subSql;
			else {
				sql = sql + where + subSql;
				hasWhere = true;
			}
			params.add(script.receiverId);
		}

		sql = sql + orderBy;

		if (script.sortByCreated)
			sql = sql + " created_at ";
		else if (script.sortByEmail)
			sql = sql + " e_mail ";
		else if (script.sortByName)
			sql = sql + " name ";
		else
			sql = sql + " last_modified ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

}
