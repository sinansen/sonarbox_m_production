package com.datalyxt.production.web.service;

import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.report.ReportBean;
import com.datalyxt.production.web.bean.report.ReportDetailBean;
import com.datalyxt.production.web.bean.report.ReportQueryScript;
import com.datalyxt.production.web.dao.TraceReportDAO;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.web.dao.helper.ReportQueryBuilder;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.M_RuleAdminDAO;
import com.datalyxt.production.webdatabase.dao.M_SourceAdminDAO;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ReportService {

	private static Logger logger = LoggerFactory.getLogger(ReportService.class);

	@Autowired
	TraceReportDAO reportDAO;

	@Autowired
	M_SourceAdminDAO sourceDAO;

	@Autowired
	M_RuleAdminDAO selectorDAO;

	@Autowired
	M_FileStorageDAO fileDAO;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	public Response listReports(ReportQueryScript script, long userId) {
		try {
			QueryWrapper wrapper;
			if(script.status == -1)
				 wrapper = ReportQueryBuilder
					.buildQueryForSearchWrong(script);
			else  wrapper = ReportQueryBuilder
					.buildQueryForSearch(script);
			
			List<ReportBean> reports = reportDAO.getReports(wrapper.sql, wrapper.params);
       
			Response res;
			if (reports == null )
				res = ResponseUtil.createResponse(userId, true,
						"Reports nicht gefunden");
			else {
			   
				res = ResponseUtil.createResponse(userId, true,
						"Reports gefunden");

			}
			res.body = reports;
			return res;
		} catch (Exception e) {
			logger.error("exception in get reports from db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Suchen");
			return res;
		}
	}

	public Response listTraceReports(ReportQueryScript script, long userId) {
		try {
			QueryWrapper wrapper = ReportQueryBuilder
					.buildQueryForTraceSearch(script);
			List<ReportDetailBean> reports = reportDAO.getReportDetails(wrapper.sql, wrapper.params);

			Response res;
			if (reports == null )
				res = ResponseUtil.createResponse(userId, true,
						"Reports nicht gefunden");
			else {
				
				res = ResponseUtil.createResponse(userId, true,
						"Reports gefunden");

			}
			res.body = reports;
			return res;
		} catch (Exception e) {
			logger.error("exception in get reports from db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Suchen");
			return res;
		}
	}
	
	public Response getReportDetail(Long id, long userId) {
		try {

			List<ReportDetailBean> detail = 
					reportDAO.getReportDetailByReportId(id);
			Response res;
			if (detail == null || detail.isEmpty())
				res = ResponseUtil.createResponse(userId, true,
						"Report nicht gefunden");
			else {
				
				res = ResponseUtil.createResponse(userId, true,
						"Report gefunden");

			}
			res.body = detail;
			return res;
		} catch (Exception e) {
			logger.error("exception in get reports from db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Suchen");;
			return res;
		}
	}
}
