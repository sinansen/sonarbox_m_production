package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;

import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.web.bean.ActivityQueryScript;
import com.datalyxt.production.webmodel.source.M_SourceStatus;

/**
 * Created by michael on 7/1/16.
 */
public class ActivityQueryBuilder {

    public static QueryWrapper buildQueryForSearch(ActivityQueryScript script) {

        String sqlBasePart = "select m_activity.id, type, user_name, object_id, object_type, timestamp, content from m_activity  join m_user on m_activity.user_id = m_user.id";
        String orderBy = " order by  ";
        String desc = " desc ";
        String asc = " asc ";
        String where = " where ";
        String combination = " and ";

        String conditionWithStartTimestamp = " timestamp >= ? ";
        String conditionWithEndTimestamp = " timestamp <= ? ";

		String conditionWithPaging = " limit ? offset ? ";
		
        String sql = sqlBasePart;

        QueryWrapper wrapper = new QueryWrapper();
        ArrayList<Object> params = new ArrayList<>();

        boolean hasWhere = false;

        if (!script.typeFilter.isEmpty()) {
            String conditionWithTypes = buildConditionWithActivityType(script.typeFilter);

            if (hasWhere)
                sql = sql + combination + conditionWithTypes;
            else {
                sql = sql + where + conditionWithTypes;
                hasWhere = true;
            }

        }

        if (!script.objectTypeFilter.isEmpty()) {
            String condition = buildConditionWithObjectTypes(script.objectTypeFilter);
            if (hasWhere)
                sql = sql + combination + condition;
            else {
                sql = sql + where + condition;
                hasWhere = true;
            }
        }

        if (script.endTimestamp > 0) {
            if (hasWhere)
                sql = sql + combination + conditionWithStartTimestamp + combination + conditionWithEndTimestamp;
            else {
                sql = sql + where + conditionWithStartTimestamp + combination + conditionWithEndTimestamp;
                hasWhere = true;
            }
            params.add(new Timestamp(script.startTimestamp));
            params.add(new Timestamp(script.endTimestamp));

        }

        sql = sql + orderBy;

        if (script.sortByTimestamp)
            sql = sql + " timestamp ";
        else if (script.sortByActivityType)
            sql = sql + " type ";
        else if (script.sortByObjectType)
            sql = sql + " object_type ";
        else if (script.sortByUsername)
            sql = sql + " user_name ";

        if (script.desc)
            sql = sql + desc;
        else
            sql = sql + asc;

    	if(script.paging != null){
			sql = sql + conditionWithPaging ;
			int limit = script.paging.pages * script.paging.perpage ;
			params.add(limit);
			params.add(script.paging.offset);
		}

    	
        wrapper.sql = sql;
        wrapper.params = params.toArray();

        return wrapper;
    }

    private static String buildConditionWithActivityType(HashSet<M_ActivityType> types) {


		String condition = " type = ";

		String[] parmsArray = new String[types.size()];
		int count = 0;
		for (M_ActivityType param : types) {
			parmsArray[count] = condition + "'" + param.name() + "'";
			count = count + 1;
		}

		String paramString = String.join(" or ", parmsArray);

		condition = " ( " + paramString + " ) ";

		return condition;
		


    }

    private static String buildConditionWithObjectTypes(HashSet<M_ObjectType> types) {
		String condition = " object_type = ";

		String[] parmsArray = new String[types.size()];
		int count = 0;
	     for (M_ObjectType param : types) {
			parmsArray[count] = condition + "'" + param.name() + "'";
			count = count + 1;
		}

		String paramString = String.join(" or ", parmsArray);

		condition = " ( " + paramString + " ) ";

		return condition;

    }
}
