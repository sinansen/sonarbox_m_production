package com.datalyxt.production.web.exception;

public class KeyStoreException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7649067767121834045L;

	public KeyStoreException(String msg) {
		super(msg);
	}

	public KeyStoreException(String msg, Exception e) {
		super(msg, e);
	}
}
