package com.datalyxt.production.web.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by michael on 6/12/16.
 */
public class InternalServerError extends RestServiceException {

    public InternalServerError(String msg) { super(msg); }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }
}
