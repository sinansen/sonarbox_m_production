package com.datalyxt.production.web.exception;

public class SourceDatabaseAccessException extends RuntimeException{
	public SourceDatabaseAccessException(String msg) {
		super(msg);
	}

	public SourceDatabaseAccessException(String msg, Exception e) {
		super(msg, e);
	}
}
