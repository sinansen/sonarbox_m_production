package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.Date;
import java.util.HashSet;

import org.joda.time.DateTimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.web.bean.FilterBean;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.result.ScraperResultQueryScript;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ResultService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("vims/sonarbox/res/results")
@RestController
public class ScraperResultController {

	private static Logger logger = LoggerFactory
			.getLogger(ScraperResultController.class);

	@Autowired
	ResultService resultService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@RequestMapping(method = RequestMethod.GET)
	public Response listResults(
			Principal principal,
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "filter", defaultValue = "") String filters,
			@RequestParam(value = "start", defaultValue = "") String startScanFilter,
			@RequestParam(value = "end", defaultValue = "") String endScanFilter) {

		long userId = Long.valueOf(principal.getName());

		logger.info("List Results sort: " + sort + " filter: " + filters);

		ScraperResultQueryScript queryscript = new ScraperResultQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;

			switch (toSwitch) {
			case "source":
				queryscript.sortBySource = true;
				break;
			case "scanId":
				queryscript.sortByScan = true;
				break;
			case "selectorName":
				queryscript.sortByRule = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		} else
			queryscript.sortByScan = true;

		// check filter values
		try {

			if (!filters.isEmpty()) {
				FilterBean[] filterBeans = jacksonObjectMapper.readValue(
						filters, FilterBean[].class);
				// String[] statusParsed = statusFilter.split(", ");
				//

				for (FilterBean s : filterBeans) {
					if (s.name.equals("start"))
						queryscript.startTimeScann = Long
								.parseLong(s.value);
					else if (s.name.equals("end"))
						queryscript.endTimeScan = Long
								.parseLong(s.value);
				}

			}

			long now = System.currentTimeMillis();
			// lastModified
			if ( queryscript.startTimeScann == 0){
				queryscript.startTimeScann = now - 5L
						* DateTimeConstants.MILLIS_PER_DAY;
				queryscript.endTimeScan = now;
			}
			
			logger.info("next startTimeScann: "
					+ new Date(queryscript.startTimeScann).toString()
					+ "  endTimeScan: " + new Date(queryscript.endTimeScan));

			if (queryscript.startTimeScann >= queryscript.endTimeScan)
				throw new WrongParameterException("Fehler: time error");

		} catch (Exception iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return resultService.listResults(queryscript, userId);

	}

	@RequestMapping(value = "/files/{id}", method = RequestMethod.GET, produces = "application/pdf")
	public @ResponseBody byte[] getFile(Principal principal,
			@PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		logger.info("get file " + id);

		byte[] res = resultService.getFile(id, userId);
		// return Response.ok(new ByteArrayInputStream(imageData)).build();
		return res;

	}

	@RequestMapping(value = "/imgs/{id}", method = RequestMethod.GET, produces = "image/png")
	public @ResponseBody byte[] getImg(Principal principal,
			@PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		logger.info("get img " + id);

		byte[] res = resultService.getFile(id, userId);
		// return Response.ok(new ByteArrayInputStream(imageData)).build();
		return res;

	}

}
