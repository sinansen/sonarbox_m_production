package com.datalyxt.production.web.bean.source;

import java.util.HashSet;

import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;

public class SourceQueryScript {
	public boolean fetchAll = true;
	public long startTimeCreated;
	public long endTimeCreated;
	public long startTimeModified;
	public long endTimeModified;
	public HashSet<M_SourceStatus> status = new HashSet<>();
	public HashSet<LanguageISO_639_1> languages = new HashSet<>();
	public String searchTerm;
	public boolean sortByDomain = false;
	public boolean sortByModified = true;
	public boolean sortByCreated = false;
	public boolean sortByName = false;
	public boolean desc = true;
	public HashSet<M_SourceType> types = new HashSet<>();

}
