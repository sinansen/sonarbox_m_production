package com.datalyxt.production.web.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.web.bean.ActivityQueryScript;
import com.datalyxt.production.web.dao.helper.ActivityQueryBuilder;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.webdatabase.dao.M_ActivityAdminDAO;

@Service
public class ActivityService {
	private static Logger logger = LoggerFactory.getLogger(ActivityService.class);

	@Autowired
	M_ActivityAdminDAO activityDAO;

	public void save(M_Activity activity) throws M_ActivityDBException {
		activityDAO.insert(activity);
	}

	public List<M_Activity> list(ActivityQueryScript queryscript) throws M_ActivityDBException {
		QueryWrapper wrapper = ActivityQueryBuilder.buildQueryForSearch(queryscript);
		return activityDAO.list(wrapper.sql, wrapper.params);
	}
}
