package com.datalyxt.production.web.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.web.bean.ActionResultStatis;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.result.ScraperResultBean;
import com.datalyxt.production.web.bean.result.ScraperResultDetailBean;
import com.datalyxt.production.web.bean.result.ScraperResultDetailRootBean;
import com.datalyxt.production.web.bean.result.ScraperResultQueryScript;
import com.datalyxt.production.web.dao.QT1V1ResultBrowsingDAO;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.web.dao.helper.ResultQueryBuilder;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.M_RuleAdminDAO;
import com.datalyxt.production.webdatabase.dao.M_SourceAdminDAO;
import com.datalyxt.production.webmodel.email.Attachement;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class ResultService {
	private static Logger logger = LoggerFactory.getLogger(ResultService.class);

	@Autowired
	QT1V1ResultBrowsingDAO resultDAO;

	@Autowired
	M_SourceAdminDAO sourceDAO;

	@Autowired
	M_RuleAdminDAO selectorDAO;

	@Autowired
	M_FileStorageDAO fileDAO;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	public HashMap<Long, ScraperResultBean> getScraperResultBeans(
			ScraperResultQueryScript queryscript)
			throws M_QT1V1ResultDBException {

		QueryWrapper wrapper = ResultQueryBuilder
				.buildQueryForCombinedTextSearch(queryscript);
		List<ScraperResultBean> textResults = resultDAO.getTextResults(
				wrapper.sql, wrapper.params);

		wrapper = ResultQueryBuilder
				.buildQueryForCombinedFileSearch(queryscript);
		List<ScraperResultBean> fileResults = resultDAO.getFileResults(
				wrapper.sql, wrapper.params);

		HashMap<Long, ScraperResultBean> beans = new HashMap<>();

		for (ScraperResultBean b : textResults) {
			b.amount = b.textResults.size();
			beans.put(b.id, b);
		}

		for (ScraperResultBean b : fileResults) {
			if (beans.containsKey(b.id)) {
				ScraperResultBean bean = beans.get(b.id);
				bean.fileResults = b.fileResults;
				bean.amount = bean.textResults.size() + bean.fileResults.size();
			} else {
				b.amount = b.fileResults.size();
				beans.put(b.id, b);
			}
		}

		return beans;
	}

	class ScanResult {
		public long scanId;
		public HashMap<Long, ScanRuleResult> results = new HashMap<>();
	}

	class ScanRuleResult {
		public int maxHops;
		public long sourceId;
		public String sourceName;
		public long selectorId;
		public String selectorName;
		public HashMap<Long, ScraperResultBean> results = new HashMap<>();
		public String sourceUrl;
	}

	private void sortResult(List<ScraperResultDetailRootBean> result,
			ScraperResultQueryScript queryscript) {
		if (queryscript.desc) {
			if (queryscript.sortByScan) {
				Collections.sort(result,
						new Comparator<ScraperResultDetailRootBean>() {
							public int compare(ScraperResultDetailRootBean o1,
									ScraperResultDetailRootBean o2) {
								long diff = o2.scanId - o1.scanId;
								if (diff > 0)
									return 1;
								if (diff < 0)
									return -1;
								return 0;
							}
						});
			} else if (queryscript.sortByRule) {
				Collections.sort(result,
						new Comparator<ScraperResultDetailRootBean>() {
							public int compare(ScraperResultDetailRootBean o1,
									ScraperResultDetailRootBean o2) {
								return o2.selectorName.compareTo(o1.selectorName);
							
							}
						});
			} else if (queryscript.sortBySource) {
				Collections.sort(result,
						new Comparator<ScraperResultDetailRootBean>() {
							public int compare(ScraperResultDetailRootBean o1,
									ScraperResultDetailRootBean o2) {
								return o2.sourceName.compareTo(o1.sourceName);
							
							}
						});
			}
			
		}else{
			if (queryscript.sortByScan) {
				Collections.sort(result,
						new Comparator<ScraperResultDetailRootBean>() {
							public int compare(ScraperResultDetailRootBean o1,
									ScraperResultDetailRootBean o2) {
								long diff = o1.scanId - o2.scanId;
								if (diff > 0)
									return 1;
								if (diff < 0)
									return -1;
								return 0;
							}
						});
			} else if (queryscript.sortByRule) {
				Collections.sort(result,
						new Comparator<ScraperResultDetailRootBean>() {
							public int compare(ScraperResultDetailRootBean o1,
									ScraperResultDetailRootBean o2) {
								return o1.selectorName.compareTo(o2.selectorName);
							
							}
						});
			} else if (queryscript.sortBySource) {
				Collections.sort(result,
						new Comparator<ScraperResultDetailRootBean>() {
							public int compare(ScraperResultDetailRootBean o1,
									ScraperResultDetailRootBean o2) {
								return o1.sourceName.compareTo(o2.sourceName);
							
							}
						});
			}
		}

	}

	public Response listResults(ScraperResultQueryScript queryscript,
			long userId) {
		try {

			HashMap<Long, ScraperResultBean> beans = getScraperResultBeans(queryscript);
			List<ScraperResultDetailRootBean> details = getDetail(beans
					.values());
			sortResult(details, queryscript);
			Response res;
			if (beans.isEmpty())
				res = ResponseUtil.createResponse(userId, true,
						"Results nicht gefunden");
			else {
				res = ResponseUtil.createResponse(userId, true,
						"Results gefunden");

			}
			res.body = details;
			return res;
		} catch (Exception e) {
			logger.error("exception in get results from db: " + e.getMessage(),
					e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Suchen");
			return res;
		}
	}

	public List<ScraperResultDetailRootBean> getDetail(
			Collection<ScraperResultBean> scraperResultBeans) {

		HashMap<Long, ScanResult> scans = new HashMap<>();
		// group result with scanId and ruleId
		for (ScraperResultBean bean : scraperResultBeans) {
			ScanResult scanResult = scans.get(bean.scanAt);

			if (scanResult == null) {
				scanResult = new ScanResult();
				scanResult.scanId = bean.scanAt;
			}
			ScanRuleResult scanRuleResult = scanResult.results
					.get(bean.selectorId);
			if (scanRuleResult == null) {
				scanRuleResult = new ScanRuleResult();
				scanRuleResult.selectorId = bean.selectorId;
				scanRuleResult.sourceName = bean.sourceName;
				scanRuleResult.sourceId = bean.sourceId;
				scanRuleResult.selectorName = bean.selectorName;
				scanRuleResult.sourceUrl = bean.sourceUrl;
			}

			if (scanRuleResult.maxHops < bean.hop)
				scanRuleResult.maxHops = bean.hop;

			scanRuleResult.results.put(bean.id, bean);
			scanResult.results.put(scanRuleResult.selectorId, scanRuleResult);

			scans.put(scanResult.scanId, scanResult);
		}

		List<ScraperResultDetailRootBean> finalResult = new ArrayList<>();

		// process every group
		for (ScanResult scanResult : scans.values()) {

			ScanResult combinedScan = new ScanResult();
			combinedScan.scanId = scanResult.scanId;
			// process every rule result group
			for (ScanRuleResult scanRuleResult : scanResult.results.values()) {
				HashMap<Integer, HashMap<String, ScraperResultDetailBean>> detailBeans = new HashMap<>();

				for (int hop = 0; hop <= scanRuleResult.maxHops; hop++) {

					// create detail bean for every url at the same hop;
					HashMap<String, ScraperResultDetailBean> detailBeansFromHop = new HashMap<>();
					for (ScraperResultBean bean : scanRuleResult.results
							.values()) {
						if (bean.hop == hop) {
							ScraperResultDetailBean detail = new ScraperResultDetailBean();
							detail.hop = hop;
							detail.scanId = bean.scanAt;
							detail.id = bean.id;
							detail.url = bean.url;
							detailBeansFromHop.put(detail.url, detail);
						}
					}
					// find action result for each detail bean

					for (ScraperResultDetailBean scraperResultDetailBean : detailBeansFromHop
							.values()) {

						for (ScraperResultBean bean : scanRuleResult.results
								.values()) {

							if (bean.hop == hop
									&& bean.id == scraperResultDetailBean.id) {

								for (ActionResultText actionResult : bean.textResults) {
									List<ActionResultText> list = scraperResultDetailBean.textResults
											.get(BlockActionType
													.valueOf(actionResult.type));
									if (list == null)
										list = new ArrayList<>();
									list.add(actionResult);
									scraperResultDetailBean.textResults
											.put(BlockActionType
													.valueOf(actionResult.type),
													list);

									if (actionResult.type
											.equals(BlockActionType.followLinks
													.name())) {
										Type type = new TypeToken<List<String>>() {
										}.getType();

										Gson gson = new Gson();
										List<String> actionLinks = gson
												.fromJson(actionResult.content,
														type);
										scraperResultDetailBean.followedLinks
												.addAll(actionLinks);
									}
								}
								for (ActionResultFile actionResult : bean.fileResults) {
									BlockActionType type = BlockActionType.fileStorage;
									if (actionResult.type == 2)
										type = BlockActionType.screenshot;
									List<ActionResultFile> list = scraperResultDetailBean.fileResults
											.get(type);
									if (list == null)
										list = new ArrayList<>();
									list.add(actionResult);
									scraperResultDetailBean.fileResults.put(
											type, list);
								}
							}
						}
					}
					detailBeans.put(hop, detailBeansFromHop);
				}
				ScraperResultDetailRootBean rootBean = new ScraperResultDetailRootBean();
				rootBean.selectorId = scanRuleResult.selectorId;
				rootBean.sourceName = scanRuleResult.sourceName;
				rootBean.sourceId = scanRuleResult.sourceId;
				rootBean.selectorName = scanRuleResult.selectorName;
				rootBean.maxHops = scanRuleResult.maxHops;
				rootBean.sourceUrl = scanRuleResult.sourceUrl;
				rootBean.scanId = scanResult.scanId;
				rootBean.root = buildTree(scanRuleResult.sourceUrl,
						scanRuleResult.maxHops, detailBeans);
				rootBean.statis = generateStatis(rootBean.root);
				finalResult.add(rootBean);
			}

		}
		return finalResult;
	}

	private ActionResultStatis generateStatis(ScraperResultDetailBean root) {
		ActionResultStatis statis = new ActionResultStatis();
		statis.cleanText = root.textResults.get(BlockActionType.cleanText) == null ? 0
				: root.textResults.get(BlockActionType.cleanText).size();
		statis.fileStorage = root.textResults.get(BlockActionType.fileStorage) == null ? 0
				: root.textResults.get(BlockActionType.fileStorage).size();
		statis.formattedText = root.textResults
				.get(BlockActionType.formattedText) == null ? 0
				: root.textResults.get(BlockActionType.formattedText).size();
		statis.screenshot = root.textResults.get(BlockActionType.screenshot) == null ? 0
				: root.textResults.get(BlockActionType.screenshot).size();
		statis.structure = root.textResults.get(BlockActionType.structure) == null ? 0
				: root.textResults.get(BlockActionType.structure).size();

		statis.followLinks = root.followedLinks.size();

		return statis;
	}

	private ScraperResultDetailBean buildTree(
			String sourceUrl,
			int maxHop,
			HashMap<Integer, HashMap<String, ScraperResultDetailBean>> detailBeans) {

		ScraperResultDetailBean root = detailBeans.get(0).get(sourceUrl);

		if (root != null) {

			for (int hop = 1; hop <= maxHop; hop++) {
				if (hop == 1) {
					HashMap<String, ScraperResultDetailBean> nextHops = detailBeans
							.get(hop);
					root.nextHopUrls.addAll(nextHops.keySet());
					root.nextHops.addAll(nextHops.values());
				} else {
					HashMap<String, ScraperResultDetailBean> preHop = detailBeans
							.get(hop - 1);
					HashMap<String, ScraperResultDetailBean> current = detailBeans
							.get(hop);

					for (ScraperResultDetailBean bean : preHop.values()) {
						for (String link : bean.followedLinks) {
							if (current.containsKey(link)) {

								bean.nextHopUrls.add(link);
								bean.nextHops.add(current.get(link));
							}
						}
					}
				}
			}
		} else
			logger.error("root is null : [" + sourceUrl + "]  "
					+ detailBeans.get(0).keySet().toString());
		return root;
	}

	public byte[] getFile(long id, long uid) {
		ActionResultFile fileMeta;
		byte[] res = new byte[] {};
		try {
			fileMeta = resultDAO.getFileResult(id);
			if (fileMeta == null)
				return res;
			List<ActionResultFile> metas = new ArrayList<>();
			metas.add(fileMeta);
			List<Attachement> files = fileDAO.readFiles(metas);
			if (files.isEmpty())
				return res;
			return files.get(0).attachement;
		} catch (Exception e) {
			logger.error("exception in get files from db: " + e.getMessage(), e);
			return res;
		}

	}

}
