package com.datalyxt.production.web.bean.source;

public class SourceValidationResponse {
	public boolean updateSourceInDB = true;
	public String sourceUrl ;
	public int statuscode = 0;
	public String updateSourceInDBMessage;
	public boolean isUrlSyntaxOK;
	public boolean isRedirect;
	public String movedToUrl;
}
