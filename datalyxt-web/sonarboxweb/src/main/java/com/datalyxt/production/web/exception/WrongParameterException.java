package com.datalyxt.production.web.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by michael on 6/12/16.
 */

public class WrongParameterException extends RestServiceException {

    public WrongParameterException() {
        super("Wrong Parameter for this resource");
    }

    public WrongParameterException(String msg) {
        super("Wrong Parameter for this resource: " + msg);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }

}