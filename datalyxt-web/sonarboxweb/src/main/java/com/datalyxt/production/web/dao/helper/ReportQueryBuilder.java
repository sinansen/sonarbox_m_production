package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;

import com.datalyxt.production.web.bean.report.ReportQueryScript;

public class ReportQueryBuilder {

	public static QueryWrapper buildQueryForSearch(ReportQueryScript script) {
		String sqlBasePart = " SELECT t1.id, start_time, finish_time, source_id, rule_id, scan_id, m_rules.name AS rule_name, m_source.name AS source_name , m_source.url_normalized as source_url , t1.status FROM  m_rules_execution_report t1 "
				+ " JOIN m_rules ON t1.rule_id = m_rules.id "
				+ " JOIN m_source ON t1.source_id = m_source.id ";

		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( url_normalized like ?  or m_rules.name like ? or m_source.name like ? ) ";

		String conditionWithStartTime = " scan_id >= ? ";
		String conditionWithEndTime = " scan_id <= ? ";
		
		String conditionWithPaging = " limit ? offset ? ";
		
		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}
		
		if (script.endTime > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTime));
			params.add(new Timestamp(script.endTime));
		}


		sql = sql + orderBy;

		if (script.sortByScanStart)
			sql = sql + " start_time ";
		else if(script.sortByScanEnd)
			sql = sql + " finish_time ";
		else if(script.sortBySource)
			sql = sql + " source_name ";
		else if(script.sortBySelectorName)
			sql = sql + " rule_name ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		if(script.paging != null){
			sql = sql + conditionWithPaging ;
			int limit = script.paging.pages * script.paging.perpage ;
			params.add(limit);
			params.add(script.paging.offset);
		}
		
		


		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

	
	public static QueryWrapper buildQueryForTraceSearch(ReportQueryScript script) {
		String sqlBasePart = " SELECT * from m_rule_trace_report where scan_id >= ? and scan_id <= ? ";

		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( message like ?  or type like ? or error like ? or supervisor like ? ) ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();
		params.add(script.startTime);
		params.add(script.endTime);

		boolean hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			}
		}

		sql = sql + orderBy;

		sql = sql + " scan_id ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

	public static QueryWrapper buildQueryForSearchWrong(ReportQueryScript script) {
		String sqlBasePart = " SELECT t1.id, start_time, finish_time, t1.source_id, t1.rule_id, t1.scan_id, m_rules.name AS rule_name, m_source.name AS source_name , m_source.url_normalized as source_url , t1.status FROM  m_rules_execution_report t1"
				+ " JOIN (SELECT distinct m_rule_trace_report.scan_id , m_rule_trace_report.rule_id, m_rule_trace_report.source_id FROM m_rule_trace_report  ) AS t2   ON (t1.scan_id, t1.source_id, t1.rule_id ) = (t2.scan_id, t2.source_id, t2.rule_id ) "
				+ " JOIN m_rules ON t1.rule_id = m_rules.id "
				+ " JOIN m_source ON t1.source_id = m_source.id ";

		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( url_normalized like ?  or m_rules.name like ? or m_source.name like ? ) ";

		String conditionWithStartTime = " scan_id >= ? ";
		String conditionWithEndTime = " scan_id <= ? ";
		
		String conditionWithPaging = " limit ? offset ? ";
		
		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}
		
		if (script.endTime > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTime));
			params.add(new Timestamp(script.endTime));
		}


		sql = sql + orderBy;

		if (script.sortByScanStart)
			sql = sql + " start_time ";
		else if(script.sortByScanEnd)
			sql = sql + " finish_time ";
		else if(script.sortBySource)
			sql = sql + " source_name ";
		else if(script.sortBySelectorName)
			sql = sql + " rule_name ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		if(script.paging != null){
			sql = sql + conditionWithPaging ;
			int limit = script.paging.pages * script.paging.perpage ;
			params.add(limit);
			params.add(script.paging.offset);
		}
		
		


		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}


}
