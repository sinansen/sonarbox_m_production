package com.datalyxt.production.web.dao.impl;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.exception.db.EmailDBException;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.web.bean.email.EmailActionBean;
import com.datalyxt.production.web.bean.email.EmailDetail;
import com.datalyxt.production.web.dao.EmailBeanDAO;
import com.datalyxt.production.webmodel.email.Email;
import com.datalyxt.production.webmodel.email.EmailAction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Repository("emailDAO")
public class MysqlEmailDAOImpl implements EmailBeanDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlEmailDAOImpl.class);

	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int updateEmailStatus(EmailAction emailAction)
			throws EmailDBException {
		try {

			final String sql = "update m_email_receiver set numretries = ? , status = ? where email_id = ? and receiver_id = ? ";

			return jdbcTemplate.update(sql, new Object[] {
					emailAction.numretries, emailAction.status,
					emailAction.emailId, emailAction.receiverId });

		} catch (Exception e) {
			throw new EmailDBException("updateEmailStatus failed. "
					+ e.getMessage(), e);
		}
	}

	@Override
	public int incRetry(EmailAction emailAction, long ownerId)
			throws EmailDBException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Email> getEmailsByStatus(int status, long ownerId)
			throws EmailDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Email getEmailById(long id, long ownerId) throws EmailDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Long, List<EmailAction>> getEmailActionsByStatus(int status,
			long ownerId) throws EmailDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveEmailAction(EmailAction e, long ownerId)
			throws EmailDBException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyEmail(Email email, long ownerId) throws EmailDBException {
		try {
			if (email.messageIds.isEmpty()
					&& email.attachementFileIds.isEmpty())
				return;

			final String sql = "update m_email_pool set subject = ? , message = ? , attachment = ?,  lastupdated = ?,  msg_md5 = ?, att_md5 = ? where id = ? and owner_id = ? ";

			List<Long> mIds = new ArrayList<>();
			mIds.addAll(email.messageIds);
			Collections.sort(mIds);
			List<Long> atIds = new ArrayList<>();
			atIds.addAll(email.attachementFileIds);
			Collections.sort(atIds);
			String msg_md5 = DigestUtils.md5Hex(mIds.toString());
			String att_md5 = DigestUtils.md5Hex(atIds.toString());

			// jdbcTemplate.update(sql, new Object[] { md5Id,
			// receiver.firstName,
			// receiver.lastName, receiver.eMail, receiver.company,
			// receiver.status.name(), new Timestamp(receiver.createdAt),
			// new Timestamp(receiver.lastModified) });

			jdbcTemplate.update(sql,
					new Object[] { email.subject, email.messageIds.toString(),
							email.attachementFileIds.toString(),
							email.lastUpdateDate, msg_md5, att_md5, email.id,
							ownerId });

			String delete_map_sql = "delete from  m_email_result_mapping where email_id = ? and owner_id = ? "
					+ " VALUES (?, ?)";

			jdbcTemplate.update(delete_map_sql, new Object[] { email.id,
					ownerId });

			String map_sql = "INSERT INTO m_email_result_mapping (email_id, content_id, content_type, owner_id )"
					+ " VALUES (?, ?, ?, ?)";

			if (!email.messageIds.isEmpty()) {

				List<Object[]> parameters = new ArrayList<Object[]>();

				for (Long contentId : email.messageIds) {
					parameters.add(new Object[] { email.id, contentId, 0,
							ownerId });
				}
				jdbcTemplate.batchUpdate(map_sql, parameters);

			}
			if (!email.attachementFileIds.isEmpty()) {
				List<Object[]> parameters = new ArrayList<Object[]>();

				for (Long contentId : email.attachementFileIds) {
					parameters.add(new Object[] { email.id, contentId, 1,
							ownerId });
				}
				jdbcTemplate.batchUpdate(sql, parameters);

			}

		} catch (Exception e) {
			throw new EmailDBException(
					"modify email failed. " + e.getMessage(), e);
		}

	}

	@Override
	public boolean hasSameEmail(Email email, long userId)
			throws EmailDBException {
		try {
			String sql = "select id from m_email_pool where msg_md5 = ? and att_md5 = ? ";
			List<Long> mIds = new ArrayList<>();
			mIds.addAll(email.messageIds);
			Collections.sort(mIds);
			List<Long> atIds = new ArrayList<>();
			atIds.addAll(email.attachementFileIds);
			Collections.sort(atIds);
			String m_md5 = DigestUtils.md5Hex(mIds.toString());
			String a_md5 = DigestUtils.md5Hex(atIds.toString());
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, m_md5, a_md5);
			if (rows.next())
				return true;
		} catch (Exception e) {
			throw new EmailDBException("hasSameEmail error. " + e.getMessage(),
					e);
		}
		return false;
	}

	@Override
	public void removeEmail(long id, long ownerId) throws EmailDBException {
		try {
			String sql = "delete from m_email_pool where id = ? and owner_id = ? ";
			jdbcTemplate.update(sql, new Object[] { id, ownerId });
		} catch (Exception e) {
			throw new EmailDBException(
					"remove email failed. " + e.getMessage(), e);
		}
	}

	@Override
	public List<EmailActionBean> getEmails(String sql, Object[] params)
			throws EmailDBException {
		try {
			List<EmailActionBean> beans = new ArrayList<>();
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			while (rows.next()) {
				EmailActionBean bean = new EmailActionBean();
				bean.id = rows.getLong("email_id");
				bean.receiverId = rows.getLong("receiver_id");
				bean.numretries = rows.getInt("numretries");
				bean.status = rows.getInt("status");
				bean.timestamp = rows.getTimestamp("time").getTime();
				bean.address = rows.getString("address");
				bean.firstName = rows.getString("first_name");
				bean.lastName = rows.getString("last_name");
				bean.email = rows.getString("e_mail");
				bean.subject = rows.getString("subject");
				beans.add(bean);
			}
			return beans;
		} catch (Exception e) {
			throw new EmailDBException("getEmails failed. " + e.getMessage(), e);
		}

	}

	@Override
	@Transactional
	public void createEmail(Email email, HashSet<Long> receiverIds, long ownerId)
			throws EmailDBException {

		try {
			if (email.messageIds.isEmpty()
					&& email.attachementFileIds.isEmpty())
				return;

			KeyHolder keyHolder = new GeneratedKeyHolder();

			final String sql = "INSERT INTO m_email_pool (subject, message, attachment, created, lastupdated, owner_id, msg_md5, att_md5)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ? , ?)";

			List<Long> mIds = new ArrayList<>();
			mIds.addAll(email.messageIds);
			Collections.sort(mIds);
			List<Long> atIds = new ArrayList<>();
			atIds.addAll(email.attachementFileIds);
			Collections.sort(atIds);
			String msg_md5 = DigestUtils.md5Hex(mIds.toString());
			String att_md5 = DigestUtils.md5Hex(atIds.toString());

			// jdbcTemplate.update(sql, new Object[] { md5Id,
			// receiver.firstName,
			// receiver.lastName, receiver.eMail, receiver.company,
			// receiver.status.name(), new Timestamp(receiver.createdAt),
			// new Timestamp(receiver.lastModified) });

			jdbcTemplate.update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement pst = con.prepareStatement(sql,
							new String[] { "id" });
					int index = 1;
					pst.setString(index++, email.subject);
					pst.setString(index++, email.messageIds.toString());
					pst.setString(index++, email.attachementFileIds.toString());
					pst.setLong(index++, email.created);
					pst.setLong(index++, email.lastUpdateDate);
					pst.setLong(index++, ownerId);
					pst.setString(index++, msg_md5);
					pst.setString(index++, att_md5);
					return pst;
				}
			}, keyHolder);
			email.id = (Long) keyHolder.getKey();

			String map_sql = "INSERT INTO m_email_result_mapping (email_id, content_id, content_type, owner_id)"
					+ " VALUES (?, ?, ?, ?)";

			if (!email.messageIds.isEmpty()) {

				List<Object[]> parameters = new ArrayList<Object[]>();

				for (Long contentId : email.messageIds) {
					parameters.add(new Object[] { email.id, contentId, 0,
							ownerId });
				}
				jdbcTemplate.batchUpdate(map_sql, parameters);

			}
			if (!email.attachementFileIds.isEmpty()) {
				List<Object[]> parameters = new ArrayList<Object[]>();

				for (Long contentId : email.attachementFileIds) {
					parameters.add(new Object[] { email.id, contentId, 1,
							ownerId });
				}
				jdbcTemplate.batchUpdate(map_sql, parameters);

			}

			String receiver_sql = "INSERT IGNORE INTO m_email_receiver (email_id, receiver_id)"
					+ " VALUES (?, ?)";

			if (!receiverIds.isEmpty()) {
				List<Object[]> parameters = new ArrayList<Object[]>();

				for (Long receiverId : receiverIds) {
					parameters.add(new Object[] { email.id, receiverId });
				}
				jdbcTemplate.batchUpdate(receiver_sql, parameters);

			}

		} catch (Exception e) {
			throw new EmailDBException(
					"create email failed. " + e.getMessage(), e);
		}
	}

	
	@Override
	@Transactional
	public void updateEmailReceiver(Email email, HashSet<Long> receiverIds, long ownerId)
			throws EmailDBException {

		try {

			String receiver_sql = "INSERT IGNORE INTO m_email_receiver (email_id, receiver_id)"
					+ " VALUES (?, ?)";

			if (!receiverIds.isEmpty()) {
				List<Object[]> parameters = new ArrayList<Object[]>();

				for (Long receiverId : receiverIds) {
					parameters.add(new Object[] { email.id, receiverId });
				}
				jdbcTemplate.batchUpdate(receiver_sql, parameters);

			}

		} catch (Exception e) {
			throw new EmailDBException(
					"update email receiver failed. " + e.getMessage(), e);
		}
	}
	@Override
	public EmailDetail getEmailDetail(long mailId, long userId)
			throws EmailDBException {
		try {
			EmailDetail emailDetail = new EmailDetail();
			String sql = "SELECT subject, address, first_name, last_name, m_email_pool.message, m_email_pool.attachment, m_email_receiver.email_id,m_email_receiver.numretries,  m_email_receiver.receiver_id, m_email_receiver.status,  m_receiver.e_mail FROM m_email_receiver JOIN m_receiver ON m_email_receiver.receiver_id = m_receiver.id JOIN m_email_pool ON m_email_pool.id = m_email_receiver.email_id  WHERE m_email_receiver.email_id = ? AND m_email_pool.owner_id = ?";
			Gson gson = new Gson();
			Type listType = new TypeToken<ArrayList<Long>>() {
			}.getType();
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, new Object[] {
					mailId, userId });
			while (rows.next()) {
				emailDetail.emailId = rows.getLong("email_id");
				emailDetail.subject = rows.getString("subject");
				String message = rows.getString("message");
				emailDetail.messageIds.addAll(gson.fromJson(message, listType));

				String attach = rows.getString("attachment");
				emailDetail.attachments.addAll(gson.fromJson(attach, listType));

				M_Receiver receiver = new M_Receiver();
				receiver.id = rows.getLong("receiver_id");
				receiver.address = rows.getString("address");
				receiver.firstName = rows.getString("first_name");
				receiver.lastName = rows.getString("last_name");
				receiver.email = rows.getString("e_mail");
				emailDetail.receivers.put(receiver.id, receiver);
				EmailAction emailAction = new EmailAction();
				emailAction.emailId = emailDetail.emailId;
				emailAction.receiverId = rows.getLong("receiver_id");
				emailAction.numretries = rows.getInt("numretries");
				emailAction.status = rows.getInt("status");
				emailDetail.actions.add(emailAction);

			}
			return emailDetail;
		} catch (Exception e) {
			throw new EmailDBException(e.getMessage(), e);
		}
	}

	@Override
	public EmailDetail getSingleEmailDetail(long mailId, long receiverId,
			long userId) throws EmailDBException {
		try {
			EmailDetail emailDetail = new EmailDetail();
			String sql = "SELECT  subject, address, first_name, last_name, m_email_pool.message, m_email_pool.attachment, m_email_receiver.email_id,m_email_receiver.numretries,  m_email_receiver.receiver_id, m_email_receiver.status,  m_receiver.e_Mail FROM m_email_receiver JOIN m_receiver ON m_email_receiver.receiver_id = m_receiver.id JOIN m_email_pool ON m_email_pool.id = m_email_receiver.email_id  WHERE m_email_receiver.email_id = ? AND m_email_pool.owner_id = ? AND m_email_receiver.receiver_id = ? ";
			Gson gson = new Gson();
			Type listType = new TypeToken<ArrayList<Long>>() {
			}.getType();
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, new Object[] {
					mailId, userId, receiverId });
			while (rows.next()) {
				emailDetail.emailId = rows.getLong("email_id");

				String message = rows.getString("message");
				emailDetail.messageIds.addAll(gson.fromJson(message, listType));

				String attach = rows.getString("attachment");
				emailDetail.attachments.addAll(gson.fromJson(attach, listType));

				M_Receiver receiver = new M_Receiver();
				receiver.id = rows.getLong("receiver_id");
				receiver.address = rows.getString("address");
				receiver.firstName = rows.getString("first_name");
				receiver.lastName = rows.getString("last_name");
				receiver.email = rows.getString("e_mail");

				emailDetail.receivers.put(receiver.id, receiver);
				EmailAction emailAction = new EmailAction();
				emailAction.emailId = emailDetail.emailId;
				emailAction.receiverId = rows.getLong("receiver_id");
				emailAction.numretries = rows.getInt("numretries");
				emailAction.status = rows.getInt("status");
				emailDetail.actions.add(emailAction);

			}
			return emailDetail;
		} catch (Exception e) {
			throw new EmailDBException(e.getMessage(), e);
		}
	}

}
