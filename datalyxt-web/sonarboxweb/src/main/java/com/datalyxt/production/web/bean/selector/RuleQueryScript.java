package com.datalyxt.production.web.bean.selector;

import java.util.HashSet;

import com.datalyxt.production.webmodel.rule.M_RuleStatus;

public class RuleQueryScript {

	public boolean fetchAll = true;
	public String searchTerm;
	public HashSet<M_RuleStatus> status;
	public boolean desc = true;
	public boolean sortByName = false;
	public boolean sortByModified = true;
	public boolean sortByCreated = false;
	public long startTimeModified;
	public long endTimeModified;
	
}
