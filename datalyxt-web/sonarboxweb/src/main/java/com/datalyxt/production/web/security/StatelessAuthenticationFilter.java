package com.datalyxt.production.web.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.datalyxt.production.web.service.AuthenticationService;

public class StatelessAuthenticationFilter extends OncePerRequestFilter {

	private static Logger logger = LoggerFactory.getLogger(StatelessAuthenticationFilter.class);

	private final AuthenticationService tokenAuthenticationService;

	public StatelessAuthenticationFilter(AuthenticationService taService) {
		this.tokenAuthenticationService = taService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		logger.info("enter filter ");
		Authentication auth = tokenAuthenticationService.getAuthentication(request);

		if (auth != null) {
			SecurityContextHolder.getContext().setAuthentication(auth);
			logger.info("auth checked. ");
			chain.doFilter(request, response); // always continue
		} else {

			String path = request.getRequestURI();
			logger.info("auth is null! " + path);
			if (path.equals("/vims") && request.getMethod().equals("POST"))
				request.getRequestDispatcher("/vims").forward(request, response);
			else
				request.getRequestDispatcher("/vims/login").forward(request, response);
			// if (isPermitted(path)) {
			// request.getRequestDispatcher().forward(request, response);
			// } else
			// response.sendRedirect("/vims/login");
			// chain.doFilter(request, response);
		}
	}

}