package com.datalyxt.production.web.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.codec.digest.DigestUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.source.SourceQueryScript;
import com.datalyxt.production.web.bean.source.SourceUIBean;
import com.datalyxt.production.web.bean.source.SourceValidationResponse;
import com.datalyxt.production.web.bean.source.SourceValidationScript;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.web.dao.helper.SourceQueryBuilder;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.ResourceAlreadyExistsException;
import com.datalyxt.production.web.exception.ResourceNotFoundException;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.webdatabase.dao.M_SourceAdminDAO;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;
import com.datalyxt.production.webscraper.service.QT1V1DynamicParameterCheckService;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.SimpleHTMLPageHeaderUtil;
import com.datalyxt.util.ValidLink;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.util.url.URLNormalizer;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.datalyxt.webscraper.model.Dynamicity;
import com.datalyxt.webscraper.model.page.SimpleHttpHeader;

@Service
public class SourceService {

	private static Logger logger = LoggerFactory.getLogger(SourceService.class);

	@Autowired
	M_SourceAdminDAO sourceDAO;

	// Create a Source
	public SourceUIBean createSource(M_Source source, long userId)
			throws WrongParameterException, ResourceAlreadyExistsException, InternalServerError {
		logger.info("create source: " + source.sourceUrl);
		try {
			if (!ValidLink.isHttp(source.sourceUrl)) {
				throw new WrongParameterException("Fehler: ");
			}
			if (!ValidLink.isURLLengthValid(source.sourceUrl, 500)) {
				throw new WrongParameterException("Fehler: ");
			}
			String sourceNormURL = null;
			try {

				sourceNormURL = URLNormalizer.getNormalizedURL(source.sourceUrl);
				source.sourceUrlNormalized = sourceNormURL;
				source.sourceDomain = GetHost.getDomain(sourceNormURL);
			} catch (Exception e) {
				throw new InternalServerError("Error normalizing source url");
			}

			source.sourceNUrlMd5 = DigestUtils.md5Hex(sourceNormURL);
			source.sourceCreatedAt = System.currentTimeMillis();
			source.sourceLastModified = source.sourceCreatedAt;
			if (sourceDAO.isSourceExist(source.sourceNUrlMd5)) {
				throw new ResourceAlreadyExistsException();
			}

			// insert into db
			source.sourceId = sourceDAO.insertSource(source, userId);

			return toUIRespresentation(source);

		} catch (M_SourceDBException e) {
			logger.error("exception in creating source in source db: " + e.getMessage(), e);
			throw new InternalServerError("Fehler: Speichern der Ressource fehlgeschlagen");
		}

	}

	// modify source : source_url | source_status
	public Response modifySource(M_Source oldSource, M_Source newSource, long userId)
			throws WrongParameterException, ResourceNotFoundException {

		if (!isValidModifySourceInput(oldSource, newSource)) {
			throw new WrongParameterException();
		}

		logger.info("Modify Source " + oldSource.sourceId);
		newSource.sourceLastModified = System.currentTimeMillis();

		try {
			boolean isModifyURL = false;
			if (!oldSource.sourceUrl.equals(newSource.sourceUrl))
				isModifyURL = true;

			boolean isModifyStatus = false;
			if (!oldSource.sourceStatus.equals(newSource.sourceStatus))
				isModifyStatus = true;

			if (!sourceDAO.isSourceExist(oldSource.sourceNUrlMd5)) {
				throw new ResourceNotFoundException(oldSource.sourceId);
			}

			if (isModifyURL) {

				if (oldSource.sourceStatus.equals(M_SourceStatus.activated)) {
					throw new WrongParameterException("Fehler: Quellen Status muss pausiert sein");
				}

				if (!ValidLink.isHttp(newSource.sourceUrl)) {
					throw new WrongParameterException();
				}
				if (!ValidLink.isURLLengthValid(newSource.sourceUrl, 500)) {
					throw new WrongParameterException();
				}
				String sourceNormURL = null;
				try {

					sourceNormURL = URLNormalizer.getNormalizedURL(newSource.sourceUrl);
					newSource.sourceUrlNormalized = sourceNormURL;
					newSource.sourceDomain = GetHost.getDomain(sourceNormURL);
				} catch (Exception e) {
					throw new InternalServerError("Error normalizing source url");
				}

				newSource.sourceNUrlMd5 = DigestUtils.md5Hex(sourceNormURL);

				if (sourceDAO.isSourceExist(newSource.sourceNUrlMd5)) {
					throw new ResourceAlreadyExistsException();
				}

				newSource.sourceStatus = M_SourceStatus.added;

			} else {
				newSource.sourceUrlNormalized = oldSource.sourceUrlNormalized;
				newSource.sourceDomain = oldSource.sourceDomain;
				newSource.sourceNUrlMd5 = oldSource.sourceNUrlMd5;
			}
			// check status
			if (isModifyStatus) {
				List<M_SourceStatus> enabledStatus = getEnabledStatus(oldSource.sourceStatus);
				if (!enabledStatus.contains(newSource.sourceStatus)) {
					throw new WrongParameterException("Status type not applicable");
				}

			}

			sourceDAO.modifySource(oldSource, newSource, userId);

		} catch (M_SourceDBException e) {
			logger.error("exception in updateSourceConfig in source db: " + e.getMessage(), e);
			throw new InternalServerError("Error saving resource");
		}

		Response res = createResponse(userId, true, "Quelle wurde modifiziert");
		res.body = toUIRespresentation(newSource);

		return res;
	}

	private M_ActivityType getSourceActivityType(M_SourceStatus sourceStatus) {
		M_ActivityType activityType = null;
		if (sourceStatus.equals(M_SourceStatus.added))
			activityType = M_ActivityType.created;
		if (sourceStatus.equals(M_SourceStatus.activated))
			activityType = M_ActivityType.activated;
		if (sourceStatus.equals(M_SourceStatus.deleted))
			activityType = M_ActivityType.deleted;
		if (sourceStatus.equals(M_SourceStatus.paused))
			activityType = M_ActivityType.paused;
		if (sourceStatus.equals(M_SourceStatus.error))
			activityType = M_ActivityType.error;
		return activityType;
	}

	private boolean isValidModifySourceInput(M_Source oldSource, M_Source newSource) {
		if (oldSource == null)
			return false;
		if (oldSource.sourceId <= 0)
			return false;
		if (oldSource.sourceUrl == null)
			return false;
		if (oldSource.sourceStatus == null)
			return false;
		if (oldSource.sourceNUrlMd5 == null)
			return false;
		if (oldSource.sourceUrlNormalized == null)
			return false;
		if (newSource == null)
			return false;
		if (newSource.sourceUrl == null)
			return false;
		if (newSource.sourceStatus == null)
			return false;
		return true;
	}

	// Update source config
	public Response updateSourceConfig(M_Source source, long userId) {
		logger.info("Updating Source Config" + source.sourceId);
		try {

			if (!sourceDAO.isSourceExist(source.sourceNUrlMd5)) {
				Response res = createResponse(userId, false, "Quelle existiert nicht in der Datenbank");
				return res;
			}

			sourceDAO.updateSourceConfig(source, userId);
		} catch (M_SourceDBException e) {
			logger.error("exception in updateSourceConfig in source db: " + e.getMessage(), e);

			Response res = createResponse(userId, false, "Fehler beim Update");
			return res;
		}

		Response res = createResponse(userId, true, "Quelle Konfiguration aktualisiert");
		return res;
	}

	public List<M_SourceStatus> getEnabledStatus(M_SourceStatus sourceStatus) {
		List<M_SourceStatus> statusList = new ArrayList<M_SourceStatus>();
		if (sourceStatus == null)
			return statusList;

		if (sourceStatus.equals(M_SourceStatus.error)) {
			statusList.add(M_SourceStatus.deleted);
			return statusList;
		}

		if (sourceStatus.equals(M_SourceStatus.validated)) {

			statusList.add(M_SourceStatus.activated);
			statusList.add(M_SourceStatus.deleted);
			return statusList;
		}

		if (sourceStatus.equals(M_SourceStatus.activated)) {

			statusList.add(M_SourceStatus.paused);
			return statusList;
		}

		if (sourceStatus.equals(M_SourceStatus.paused)) {

			statusList.add(M_SourceStatus.deleted);
			statusList.add(M_SourceStatus.activated);
			return statusList;
		}
		if (sourceStatus.equals(M_SourceStatus.blackList)) {

			statusList.add(M_SourceStatus.blackList);
			return statusList;
		}
		if (sourceStatus.equals(M_SourceStatus.added)) {

			statusList.add(M_SourceStatus.added);
			return statusList;
		}
		return statusList;
	}

	// List added Sources
	public Response listSources(SourceQueryScript script, long userId) throws InternalServerError {
		try {

			QueryWrapper wrapper = SourceQueryBuilder.buildQueryForSearch(script);
			logger.info(wrapper.sql);
			List<M_Source> sources = sourceDAO.getSources(wrapper.sql, wrapper.params);

			List<SourceUIBean> sourceUIBeans = new ArrayList<SourceUIBean>();
			for (M_Source source : sources) {
				sourceUIBeans.add(toUIRespresentation(source));
			}

			Response res = createResponse(userId, true, "");
			res.body = sourceUIBeans;

			return res;
		} catch (M_SourceDBException e) {
			logger.error("exception in list source from source db: " + e.getMessage(), e);
			throw new InternalServerError("Error loading resources");
		}
	}

	private List<M_Source> filterBySourceType(boolean enableTypeFilter, List<M_Source> sources,
			HashMap<Long, HashSet<M_SourceType>> sourceTypes) {

		List<M_Source> filtered = new ArrayList<M_Source>();

		if (enableTypeFilter) {
			for (int i = 0; i < sources.size(); i++) {
				M_Source source = sources.get(i);
				HashSet<M_SourceType> srcTypes = sourceTypes.get(source.sourceId);
				if (srcTypes != null && !srcTypes.isEmpty()) {
					source.sourceTypes.addAll(srcTypes);
					filtered.add(source);
				}
			}
		} else {
			for (int i = 0; i < sources.size(); i++) {
				M_Source source = sources.get(i);
				HashSet<M_SourceType> srcTypes = sourceTypes.get(source.sourceId);
				if (srcTypes != null && !srcTypes.isEmpty()) {
					source.sourceTypes.addAll(srcTypes);
				}
				filtered.add(source);
			}
		}
		return filtered;
	}

	// validate source url
	@Async
	public Future<SourceValidationResponse> validateSource(SourceValidationScript sourceScript, long userId)
			throws InterruptedException {
		logger.info("Validate source: " + sourceScript.sourceUrl);

		SourceValidationResponse response = new SourceValidationResponse();
		response.sourceUrl = sourceScript.sourceUrl;
		try {
			if (sourceScript.urlSyntaxCheck)
				response.isUrlSyntaxOK = ValidLink.isHttp(sourceScript.sourceUrl);
			else
				response.isUrlSyntaxOK = true;

			if (response.isUrlSyntaxOK) {

				if (sourceScript.normSourceURL == null)
					sourceScript.normSourceURL = URLNormalizer.getNormalizedURL(sourceScript.sourceUrl);

				if (sourceScript.basicHTTPCheck) {
					int socketTimeout = 20000; // milliseconds
					int connectionTimeout = 20000; // milliseconds
					SimpleHTMLPageHeaderUtil util = new SimpleHTMLPageHeaderUtil(socketTimeout, connectionTimeout);
					SimpleHttpHeader header = util.fetchPageHeader(sourceScript.normSourceURL);
					response.statuscode = header.statusCode;
					response.isRedirect = header.isRedirect;
					response.movedToUrl = header.movedToUrl;
				}
			} else
				response.statuscode = -1;

			M_SourceStatus status = M_SourceStatus.validated;
			if (sourceScript.autoDeploy)
				status = M_SourceStatus.activated;

			if (response.statuscode != 200)
				status = M_SourceStatus.error;
			if (sourceScript.checkDynamicParameters && sourceScript.source != null
					&& sourceScript.source.sourceTypes.contains(M_SourceType.QT1V1)) {
				WebDriver driver;
				if (isWindows())
					driver = WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
				else
					driver = WebDriverFactory.createDriver(BrowserDriverType.firefox_unix);

				QT1V1DynamicParameterCheckService qt1v1DynamicParameterCheckService = new QT1V1DynamicParameterCheckService();
				Dynamicity dy = qt1v1DynamicParameterCheckService.getDynamicity(driver, sourceScript.normSourceURL);
				if (!dy.dynamicParameters.isEmpty()) {
					sourceScript.source.sourceConfig.qt1v1SourceConfig.dynamicParameters = dy.dynamicParameters;
					sourceDAO.updateSourceConfig(sourceScript.source, userId);
				}
				driver.close();
			}
			if (sourceScript.updateSourceInDB) {

				String sid = DigestUtils.md5Hex(sourceScript.normSourceURL);
				M_Source oldSource = sourceDAO.getSourceByNmd5(sid);
				if (oldSource == null) {
					response.updateSourceInDBMessage = "Quelle existiert nicht in der Datenbank";
				} else {
					String feedback = "Die Anfrage der URL lieferte einen HTTP Status Code " + response.statuscode;
					if (response.isRedirect)
						feedback = feedback + "\nWebsite leitet weiter auf " + response.movedToUrl;
					response.updateSourceInDBMessage = "feedback: " + feedback;
					sourceDAO.updateSourceStatusAndFeedback(sid, status, feedback, userId);

				}

			}
		} catch (Exception e) {
			logger.error("exception in validateSource: " + e.getMessage(), e);
			response.updateSourceInDBMessage = "Fehler beim Update";
		}

		logger.info("Validate source: " + response.updateSourceInDBMessage);
		return new AsyncResult<SourceValidationResponse>(response);

	}

	public Response createResponse(long uid, boolean success, String msg) {
		Response res = new Response();
		res.userid = uid;
		res.success = success;
		res.message = msg;
		return res;
	}

	public M_Source getSourceByID(long sourceId) {
		M_Source source = null;
		try {
			source = sourceDAO.getSourceById(sourceId);
		} catch (M_SourceDBException e) {
			logger.error("exception in getSourceByID: " + e.getMessage(), e);

		}
		return source;
	}

	public SourceUIBean toUIRespresentation(M_Source source) {
		SourceUIBean sourceUIBean = new SourceUIBean();

		sourceUIBean.id = source.sourceId;
		sourceUIBean.url = source.sourceUrl;
		sourceUIBean.types = source.sourceTypes;
		sourceUIBean.createdAt = source.sourceCreatedAt;
		sourceUIBean.lastModified = source.sourceLastModified;
		sourceUIBean.status = source.sourceStatus;
		sourceUIBean.name = source.sourceName;
		sourceUIBean.feedback = source.feedback;

		if (source.sourceConfig.qt1v1SourceConfig != null) {
			sourceUIBean.languages.addAll(source.sourceConfig.qt1v1SourceConfig.languages);
			sourceUIBean.dynamicParams = source.sourceConfig.qt1v1SourceConfig.dynamicParameters;
		}

		if (source.sourceConfig.qt1v2SourceConfig != null)
			sourceUIBean.languages.addAll(source.sourceConfig.qt1v2SourceConfig.languages);

		return sourceUIBean;
	}

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public Response delete(long id, long userId) {
		try {

			if (!sourceDAO.isSourceExist(id)) {
				Response res = createResponse(userId, false, "Quelle existiert nicht in der Datenbank");
				return res;
			}
			M_SourceStatus delete = M_SourceStatus.deleted;
			sourceDAO.changeSourceStatus(id, delete, userId);

		} catch (M_SourceDBException e) {
			logger.error("exception in changes source status in source db: " + e.getMessage(), e);
			Response res = createResponse(userId, false, "Fehler beim Ändern");
			return res;
		}
		Response res = createResponse(userId, true, "Quelle wurde gelöscht.");
		return res;
	}

	public void changeSourceStatus(long id, M_SourceStatus status, long userId) throws M_SourceDBException {
		sourceDAO.changeSourceStatus(id, status, userId);

	}
}
