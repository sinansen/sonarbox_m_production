package com.datalyxt.production.web.bean.result;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

public class ScraperResultBean {
	
    public long id; 
    public String sourceName ;
    public long scanAt ;
    public int amount;
	public long sourceId;
	public long selectorId;
	public String selectorName;
	public List<ActionResultText> textResults = new ArrayList<>();
	public List<ActionResultFile> fileResults = new ArrayList<>();
	public String url;
	public int hop;
	public String sourceUrl;
}
