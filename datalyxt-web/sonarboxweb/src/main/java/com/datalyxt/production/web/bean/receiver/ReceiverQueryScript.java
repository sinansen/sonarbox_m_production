package com.datalyxt.production.web.bean.receiver;

import java.util.HashSet;

import com.datalyxt.production.receiver.M_ReceiverStatus;

public class ReceiverQueryScript {
	public boolean fetchAll = true;
	public String searchTerm;
	public HashSet<M_ReceiverStatus> status;
	public boolean desc = true;
	public boolean sortByName = false;
	public boolean sortByModified = true;
	public boolean sortByCreated = false;
	public boolean sortByEmail = false;
	public long startTimeModified;
	public long endTimeModified;
}
