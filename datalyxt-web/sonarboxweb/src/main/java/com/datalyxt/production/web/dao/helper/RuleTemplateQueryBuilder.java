package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.web.bean.selector.RuleTemplateQueryScript;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;

public class RuleTemplateQueryBuilder {
	
	public static QueryWrapper buildQueryForSearch(RuleTemplateQueryScript script) {
		String sqlBasePart = "select id, name, nurl , content, status, owner_id, created_at, last_modified  from m_scraper_template  ";
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = "  name like ?  ";
		String conditionWithStatus = " status =  ";
		String conditionExcludeStatus = " status != ? ";
		String conditionWithStartTime = " last_modified >= ? ";
		String conditionWithEndTime = " last_modified <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = false;

		sql = sql + where + conditionExcludeStatus;
		params.add(M_ReceiverStatus.deleted.name());
		hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.status != null && !script.status.isEmpty()) {
			List<String> statusParams = new ArrayList<String>();
			for (M_RuleStatus s : script.status) {
				String param = conditionWithStatus + s.name() + " ";
				statusParams.add(param);
			}
			String parms = String.join(" or ", statusParams);
			parms = " ( " + parms + " ) ";
			if (hasWhere)
				sql = sql + combination + parms;
			else {
				sql = sql + where + parms;
				hasWhere = true;
			}
		}
		if (script.endTimeModified > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTimeModified));
			params.add(new Timestamp(script.endTimeModified));
		}

		sql = sql + orderBy;

		if (script.sortByCreated)
			sql = sql + " created_at ";
		else if (script.sortByName)
			sql = sql + " name ";
		else
			sql = sql + " last_modified ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

}
