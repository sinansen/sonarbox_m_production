package com.datalyxt.production.web.bean.email;

import java.util.HashSet;

public class EmailBean {
	public long id;
	public String subject;
	public HashSet<Long> messageIds = new HashSet<Long>();
	public HashSet<Long> attachementFileIds = new HashSet<Long>();
	public HashSet<Long> receiverIds = new HashSet<Long>();
	public HashSet<Long> groupIds = new HashSet<Long>();
}
