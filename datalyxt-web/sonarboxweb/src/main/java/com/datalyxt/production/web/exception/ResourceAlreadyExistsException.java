package com.datalyxt.production.web.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by michael on 6/12/16.
 */
public class ResourceAlreadyExistsException extends RestServiceException {

    public ResourceAlreadyExistsException() {
        super("Fehler: Die Ressource existiert bereits");
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.CONFLICT;
    }
}
