package com.datalyxt.production.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.webdatabase.dao.M_DomainBlackListDAO;
import com.datalyxt.production.webmodel.source.Blacklist;

@Repository("blacklistDAO")
public class MysqlBlacklistDAOImpl implements M_DomainBlackListDAO {

	private static Logger logger = LoggerFactory.getLogger(MysqlBlacklistDAOImpl.class);

	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource dataSource;
	
	
	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void delete(long id) throws M_DomainBlackListDBException {
		try {
			String sql = "delete from m_link_blacklist where id = ?";

			jdbcTemplate.update(sql, new Object[] {  id });

		} catch (Exception e) {
			throw new M_DomainBlackListDBException("Error in delete url from blacklist: " + e.getMessage(), e);
		}

	}

	@Override
	public List<Blacklist> getDomainBlackList() throws M_DomainBlackListDBException {
		List<Blacklist> blacklist = new ArrayList<>();
		try {
			String sql = "select * from m_link_blacklist ";

			SqlRowSet resultSet = jdbcTemplate.queryForRowSet(sql);
			
			while(resultSet.next()){
				Blacklist b = new Blacklist();
				b.id = resultSet.getLong("id");
				b.url = resultSet.getString("url");
				b.timestamp = resultSet.getLong("timestamp");
				blacklist.add(b);
			}
		} catch (Exception e) {
			throw new M_DomainBlackListDBException("Error in getDomainBlackList: " + e.getMessage(), e);
		}
		return blacklist;
	}

	private PreparedStatementCreator getPreparedStatementCreator(String sql, String url, String md5, long timestamp) {

		PreparedStatementCreator creator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				PreparedStatement pst = con.prepareStatement(sql, new String[] { "id" });
				int index = 1;
				pst.setString(index++, url);
				pst.setString(index++, md5);
				pst.setLong(index++, timestamp);
				return pst;
			}
		};

		return creator;

	}

	@Override
	public long insert(String url) throws M_DomainBlackListDBException {
		try {

			// insert
			KeyHolder keyHolder = new GeneratedKeyHolder();

			String sql = "INSERT INTO m_link_blacklist (url, md5, timestamp)" + " VALUES (?, ?, ?)";
			String md5 = DigestUtils.md5Hex(url);
			long now = System.currentTimeMillis();
			PreparedStatementCreator creator = getPreparedStatementCreator(sql, url, md5, now);

			jdbcTemplate.update(creator, keyHolder);
			long id = (Long) keyHolder.getKey();

			return id;
		} catch (Exception e) {
			throw new M_DomainBlackListDBException("Error in add url into blacklist: " + e.getMessage(), e);
		}

	}

	@Override
	public void update(long id, String url) throws M_DomainBlackListDBException {
		try {
			long now = System.currentTimeMillis();
			String md5 = DigestUtils.md5Hex(url);
			String sql = "update m_link_blacklist set url = ? , md5 = ? , timestamp = ?  where id = ?";

			jdbcTemplate.update(sql, new Object[] { url, md5,
					now, id });

		} catch (Exception e) {
			throw new M_DomainBlackListDBException("Error in update url in blacklist: " + e.getMessage(), e);
		}

	}

}
