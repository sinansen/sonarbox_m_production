package com.datalyxt.production.web.service;

import com.datalyxt.production.web.bean.Response;

public class ResponseUtil {
	public static Response createResponse(long uid, boolean success, String msg) {
		Response res = new Response();
		res.userid = uid;
		res.success = success;
		res.message = msg;
		return res;
	}
}
