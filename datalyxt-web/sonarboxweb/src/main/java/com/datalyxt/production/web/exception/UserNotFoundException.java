package com.datalyxt.production.web.exception;

public class UserNotFoundException extends RuntimeException  {

	public UserNotFoundException(String msg) {
		super(msg);
	}

	public UserNotFoundException(String msg, Exception e) {
		super(msg, e);
	}
}
