package com.datalyxt.production.web.bean.selector;

import java.util.HashSet;

import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.rule.QT1_V2_RuleContent;

public class RuleUpdateScript {
	public boolean deleteAllMapping = false;
	public long ruleId;
	public HashSet<Long> sourceIds = new HashSet<Long>();
	public M_RuleStatus status;
	public String ruleName;
	public QT1_V1_RuleContent qt1_V1_RuleContent;
	public QT1_V2_RuleContent qt1_V2_RuleContent;

	public M_RuleType ruleType;
}
