package com.datalyxt.production.web.bean;

import java.util.HashSet;

import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;

/**
 * Created by michael on 7/1/16.
 */
public class ActivityQueryScript {
    public long startTimestamp;
    public long endTimestamp;
    public HashSet<M_ActivityType> typeFilter = new HashSet<>();
    public HashSet<M_ObjectType> objectTypeFilter = new HashSet<>();
    public long objectIdFilter = -1;
    public long userIdFilter = -1;

    public boolean sortByTimestamp = false;
    public boolean desc = true;
	public boolean sortByObjectType = false;
	public boolean sortByUsername = false;
	public boolean sortByActivityType = false;
	public PagingBean paging;
}
