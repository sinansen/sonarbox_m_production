package com.datalyxt.production.web.bean.email;

import java.util.HashSet;

import com.datalyxt.production.web.bean.PagingBean;

public class EmailQueryScript {

	public boolean desc = true;
	public boolean sortByTime = false;
	public HashSet<Integer> status = new HashSet<>();
	public long startTime;
	public long endTime;
	public String searchTerm;
	public PagingBean paging;
	public boolean sortBySubject = false;
	public boolean sortByEmail = false;
	public boolean sortByFirstName = false;

}
