package com.datalyxt.production.web.bean.result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockActionType;

public class ScraperResultDetailBean {

	public long scanId;
	public int hop;
	public String url;
	public HashMap<BlockActionType, List<ActionResultText>> textResults = new HashMap<>();
	public HashMap<BlockActionType, List<ActionResultFile>> fileResults = new HashMap<>();
	public HashSet<String> nextHopUrls = new HashSet<>();
	public List<ScraperResultDetailBean> nextHops = new ArrayList<>();
	public HashSet<String> followedLinks = new HashSet<>();
	public long id;

}
