package com.datalyxt.production.web.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.web.bean.report.ReportBean;
import com.datalyxt.production.web.bean.report.ReportDetailBean;
import com.datalyxt.production.web.dao.TraceReportDAO;

@Repository("reportDAO")
public class MysqlReportDAOImpl implements TraceReportDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlReportDAOImpl.class);

	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource dataSource;

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				jdbcTemplate);
	}

	@Override
	public List<ReportBean> getReports(String sql, Object[] params)
			throws TraceReportDBException {
		try {
			List<ReportBean> beans = new ArrayList<>();
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			HashSet<Long> scanIds = new HashSet<>();
			while (rows.next()) {
				ReportBean bean = new ReportBean();
				bean.id = rows.getLong("id");
				bean.scanId = rows.getLong("scan_id");
				bean.scanStart = rows.getTimestamp("start_time").getTime();
				bean.scanEnd = rows.getTimestamp("finish_time").getTime();
				bean.selectorName = rows.getString("rule_name");
				bean.selectorId = rows.getLong("rule_id");
				bean.sourceName = rows.getString("source_name");
				bean.sourceId = rows.getLong("source_id");
				bean.sourceUrl = rows.getString("source_url");
				bean.status = rows.getString("status");
				scanIds.add(bean.scanId);
				bean.createdId = "" + bean.scanId + ":" + bean.selectorId + ":"
						+ bean.sourceId;
				beans.add(bean);
			}
			if (!scanIds.isEmpty()) {
				String query = "select id, scan_id, rule_id, source_id from m_rule_trace_report where scan_id in (:scanIds)";

				MapSqlParameterSource parameters = new MapSqlParameterSource();
				parameters.addValue("scanIds", scanIds);

				SqlRowSet resultsets = namedParameterJdbcTemplate
						.queryForRowSet(query, parameters);

				HashSet<Long> systemScanError = new HashSet<>();
				HashSet<String> scraperError = new HashSet<>();
				while (resultsets.next()) {
					long scanId = resultsets.getLong("scan_id");
					long ruleId = resultsets.getLong("rule_id");
					long sourceId = resultsets.getLong("source_id");

					if (ruleId == 0) {
						systemScanError.add(scanId);
					} else {

						String errorId = "" + scanId + ":" + ruleId + ":"
								+ sourceId;

						scraperError.add(errorId);
					}

				}
				for (ReportBean bean : beans) {
					if (systemScanError.contains(bean.scanId)) {
						bean.hasError = true;
					} else if (scraperError.contains(bean.createdId)) {
						bean.hasError = true;
					} else
						bean.hasError = false;
				}
			}
			return beans;
		} catch (Exception e) {
			throw new TraceReportDBException("get reports failed. "
					+ e.getMessage(), e);
		}

	}

	@Override
	public List<ReportDetailBean> getReportDetails(String sql, Object[] params)
			throws TraceReportDBException {
		try {
			List<ReportDetailBean> beans = new ArrayList<>();

			SqlRowSet resultsets = jdbcTemplate.queryForRowSet(sql, params);

			while (resultsets.next()) {
				ReportDetailBean bean = new ReportDetailBean();
				bean.id = resultsets.getLong("id");
				bean.scanId = resultsets.getLong("scan_id");
				bean.selectorId = resultsets.getLong("rule_id");
				bean.sourceId = resultsets.getLong("source_id");
				bean.message = resultsets.getString("message");
				bean.url = resultsets.getString("url");
				bean.reporter = resultsets.getString("supervisor");
				bean.error = resultsets.getString("error");
				bean.type = resultsets.getString("type");
				bean.time = resultsets.getTimestamp("time").getTime();
				beans.add(bean);

			}
			return beans;
		} catch (Exception e) {
			throw new TraceReportDBException("get report details failed. "
					+ e.getMessage(), e);
		}
	}

	@Override
	public List<ReportDetailBean> getReportDetailByReportId(long id)
			throws TraceReportDBException {
		try {
			List<ReportDetailBean> beans = new ArrayList<>();

			String sql = "select scan_id, rule_id, source_id from m_rules_execution_report where id = ? ";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { id });
			long scanId = 0;
			long ruleId = 0;
			long sourceId = 0;
			while (rows.next()) {
				scanId = rows.getLong("scan_id");
				ruleId = rows.getLong("rule_id");
				sourceId = rows.getLong("source_id");
			}

			String query = "select * from m_rule_trace_report where scan_id = ? and (rule_id = 0 or rule_id = ? ) and (source_id = 0 or source_id = ?)";

			SqlRowSet resultsets = jdbcTemplate.queryForRowSet(query,
					new Object[] { scanId, ruleId, sourceId });

			while (resultsets.next()) {
				ReportDetailBean bean = new ReportDetailBean();
				bean.id = resultsets.getLong("id");
				bean.scanId = resultsets.getLong("scan_id");
				bean.selectorId = resultsets.getLong("rule_id");
				bean.sourceId = resultsets.getLong("source_id");
				bean.message = resultsets.getString("message");
				bean.url = resultsets.getString("url");
				bean.reporter = resultsets.getString("supervisor");
				bean.error = resultsets.getString("error");
				bean.type = resultsets.getString("type");
				bean.time = resultsets.getTimestamp("time").getTime();
				beans.add(bean);

			}
			return beans;
		} catch (Exception e) {
			throw new TraceReportDBException("get report details failed. "
					+ e.getMessage(), e);
		}
	}
}
