package com.datalyxt.production.web.restfulws;

import java.security.Principal;

import org.joda.time.DateTimeConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.web.bean.FilterBean;
import com.datalyxt.production.web.bean.PagingBean;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.report.ReportQueryScript;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ReportService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("vims/sonarbox/res/reports")
@RestController
public class ReportController {

	private static Logger logger = LoggerFactory
			.getLogger(ReportController.class);

	@Autowired
	ReportService reportService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;


	@RequestMapping(method = RequestMethod.GET)
	public Response listReports(
			Principal principal,
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "filter", defaultValue = "") String filters,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "paging", defaultValue = "") String paging,
			@RequestParam(value = "start", defaultValue = "") String startTime,
			@RequestParam(value = "end", defaultValue = "") String endTime) {

		long userId = Long.valueOf(principal.getName());

		logger.info("List reports ");

		ReportQueryScript queryscript = new ReportQueryScript();

    	// check filter values
		try {
			
			if (!sort.isEmpty()) {
				// check sort direction
				queryscript.desc = sort.charAt(0) == '-';
				String toSwitch = queryscript.desc ? sort.substring(1) : sort;
				
//				'source', 'selectorName', 'scanStart','scanEnd',
				switch (toSwitch) {
				case "scanStart":
					queryscript.sortByScanStart = true;
					break;
				case "scanEnd":
					queryscript.sortByScanEnd = true;
					break;
				case "source":
					queryscript.sortBySource = true;
					break;
				case "selectorName":
					queryscript.sortBySelectorName = true;
					break;
				default:
					throw new WrongParameterException(
							"Fehler: Unbekannte Sortierung");
				}
				
				logger.info("list report sort param "+toSwitch+" "+queryscript.sortBySource);
			}else 	queryscript.sortByScanEnd = true;
			

			// statusFilter
			if (!filters.isEmpty()) {
				FilterBean[] filterBeans = jacksonObjectMapper.readValue(filters,
						FilterBean[].class);
				
				for (FilterBean s : filterBeans) {
//					queryscript.types.add(.valueOf(s.value));
					if(s.name.equals("reportstatus")){
						queryscript.status = Integer.valueOf(s.value);
					}
				}

			}

			// lastModified
			if (!startTime.isEmpty())
				queryscript.startTime = Long.parseLong(startTime);

			if (!endTime.isEmpty())
				queryscript.endTime = Long.parseLong(endTime);
		
			logger.info("paging obj: " + paging);
			if (!paging.isEmpty()) {
				PagingBean pagingBean = jacksonObjectMapper.readValue(paging,
						PagingBean.class);
				queryscript.paging = pagingBean;
			}
		} catch (Exception iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return reportService.listReports(queryscript, userId);

	}

	@RequestMapping(value = "/details", method = RequestMethod.GET)
	public Response listTraceReports(
			Principal principal,
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "types", defaultValue = "") String types,
			@RequestParam(value = "startLastUpdated", defaultValue = "") String startTime,
			@RequestParam(value = "endLastUpdated", defaultValue = "") String endTime) {

		long userId = Long.valueOf(principal.getName());

		logger.info("List trace reports ");

		ReportQueryScript queryscript = new ReportQueryScript();

    	// check filter values
		try {

			// statusFilter
			if (!types.isEmpty()) {
				String[] statusParsed = types.split(", ");


				for (String s : statusParsed) {
					queryscript.types.add(ReportType.valueOf(s));
				}

			}

			// lastModified
			long now = System.currentTimeMillis();
			// lastModified
			if (!startTime.isEmpty())
				queryscript.startTime = Long.parseLong(startTime);
			else
				queryscript.startTime = now - DateTimeConstants.MILLIS_PER_DAY;
			if (!endTime.isEmpty())
				queryscript.endTime = Long.parseLong(endTime);
			else
				queryscript.endTime = now;

		} catch (IllegalArgumentException iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return reportService.listTraceReports(queryscript, userId);

	}
	

	@RequestMapping(value = "/details/{id}", method = RequestMethod.GET)
	public Response getReport(Principal principal,@PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		logger.info("get report " + id);

		Response res = reportService.getReportDetail(id, userId);

		return res;

	}
}
