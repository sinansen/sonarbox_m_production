package com.datalyxt.production.web.bean.source;

import com.datalyxt.production.webmodel.source.M_Source;

public class SourceValidationScript {
	public boolean urlSyntaxCheck = false;
	public boolean basicHTTPCheck = true;
	public boolean soft404Check = false;
	public boolean updateSourceInDB = true;
	public String sourceUrl ;
	public String normSourceURL;
	public boolean autoDeploy = true;
	public boolean checkDynamicParameters = true;
	public M_Source source;
}
