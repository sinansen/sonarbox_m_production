package com.datalyxt.production.web.bean.selector;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.web.bean.source.SourceBasicBean;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;

public class SelectorBean {
	public long id;
	public M_RuleStatus status;
	public List<SourceBasicBean>  sources = new ArrayList<>();
	public String name;
	public M_RuleType type;
	public int hops;
	public String content;
	public long createdAt;
	public long lastModified;
}
