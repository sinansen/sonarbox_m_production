package com.datalyxt.production.web.dao.helper;

import java.util.ArrayList;

import com.datalyxt.production.web.bean.result.ScraperResultQueryScript;

public class ResultQueryBuilder {

	public static QueryWrapper buildQueryForCombinedTextSearch(
			ScraperResultQueryScript script) {
		String sqlBasePart = " SELECT result_id, source_id, rule_id, n_url, hop, scan_id, text_id, m_rules.name AS rule_name, m_source.name AS source_name , m_source.url_normalized as source_url , action_type, action_content FROM ( SELECT  r.id AS result_id, r.source_id, r.rule_id, r.n_url, r.hop, r.scan_id, t.id AS text_id , t.type as action_type , t.content as action_content FROM  m_qt1v1_result r JOIN m_qt1v1_text t ON  r.id = t.result_id  where r.scan_id >= ? ORDER BY r.scan_id DESC  ) t1 "
				+ " JOIN m_rules ON t1.rule_id = m_rules.id "
				+ " JOIN m_source ON t1.source_id = m_source.id  ";
		
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( n_url like ?  or m_rules.name like ? or m_source.name like ? ) ";
		String conditionWithStartTime = " scan_id >= ? ";
		String conditionWithEndTime = " scan_id <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();
		params.add(script.startTimeScann );
		
		boolean hasWhere = false;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.endTimeScan > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(script.startTimeScann);
			params.add(script.endTimeScan);
		}

		sql = sql + orderBy;

		if (script.sortByScan)
			sql = sql + " scan_id ";
		else if (script.sortBySource)
			sql = sql + " source_name ";
		else if (script.sortByRule)
			sql = sql + " rule_name ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;


		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}
	public static QueryWrapper buildQueryForCombinedFileSearch(
			ScraperResultQueryScript script) {
		String sqlBasePart = " SELECT result_id, hop, source_id, m_source.url_normalized as source_url, rule_id, n_url, scan_id, file_id, m_rules.name AS rule_name, m_source.name AS source_name,  action_type FROM ( SELECT  r.id AS result_id, r.hop, r.source_id, r.rule_id, r.n_url, r.scan_id, f.id AS file_id , f.type as action_type FROM  m_qt1v1_result r JOIN m_qt1v1_file f ON  r.id = f.result_id  where r.scan_id >= ? and r.scan_id <= ? ORDER BY r.scan_id DESC  ) t1 "
				+ " JOIN m_rules ON t1.rule_id = m_rules.id "
				+ " JOIN m_source ON t1.source_id = m_source.id  ";
		
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( n_url like ?  or m_rules.name like ? or m_source.name like ? ) ";
		String conditionWithStartTime = " scan_id >= ? ";
		String conditionWithEndTime = " scan_id <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();
		params.add(script.startTimeScann );
		params.add(script.endTimeScan );
		boolean hasWhere = false;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.endTimeScan > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(script.startTimeScann);
			params.add(script.endTimeScan);
		}

		sql = sql + orderBy;

		if (script.sortByScan)
			sql = sql + " scan_id ";
		else if (script.sortBySource)
			sql = sql + " source_name ";
		else if (script.sortByRule)
			sql = sql + " rule_name ";
		
		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;


		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

}
