package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.web.bean.FilterBean;
import com.datalyxt.production.web.bean.PagingBean;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.email.EmailActionBean;
import com.datalyxt.production.web.bean.email.EmailBean;
import com.datalyxt.production.web.bean.email.EmailQueryScript;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ActivityService;
import com.datalyxt.production.web.service.EmailService;
import com.datalyxt.production.web.service.ResponseUtil;
import com.datalyxt.production.webmodel.email.Email;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("vims/sonarbox/res/mails")
@RestController
public class EmailController {
	private static Logger logger = LoggerFactory
			.getLogger(EmailController.class);

	@Autowired
	EmailService emailService;

	@Autowired
	ActivityService activityService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@RequestMapping(method = RequestMethod.POST)
	public Response createEmail(Principal principal, @RequestBody String json) {
		EmailBean emailBean = new EmailBean();

		long userId = Long.valueOf(principal.getName());

		logger.info("Creating Email " + json);
		try {
			emailBean = jacksonObjectMapper.readValue(json, EmailBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = ResponseUtil.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}
		Email email = new Email();
		email.id = emailBean.id;
		email.attachementFileIds = emailBean.attachementFileIds;
		email.messageIds = emailBean.messageIds;
		email.subject = emailBean.subject;
		email.created = System.currentTimeMillis();
		email.lastUpdateDate = email.created;
		Response res;
		
		if (email.id == 0) {
			res = emailService.createEmail(email, emailBean.groupIds,
					emailBean.receiverIds, userId);
		} else {
			res = emailService.updateEmailReceiver(email, emailBean.groupIds,
					emailBean.receiverIds, userId);
		}
		try {
			if (res.success ) {
				M_Activity activity = new M_Activity(M_ActivityType.created,
						email.id, M_ObjectType.mail);
				activity.userId = userId;
				activity.content = json;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("save email activity failed. " + e.getMessage());
			throw new InternalServerError(" save email activity failed. ");
		}

		return res;

	}

	@RequestMapping(method = RequestMethod.PUT)
	public Response modifyEmail(Principal principal, @RequestBody String json) {
		long userId = Long.valueOf(principal.getName());
		logger.info("modify email " + json);
		EmailActionBean emailActionBean;
		try {
			emailActionBean = jacksonObjectMapper.readValue(json,
					EmailActionBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = ResponseUtil.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}
		if (emailActionBean.updateType == 1) {
			Email email = new Email();

			// TODO get email from emailActionBean ;
			// email.attachementFileIds = emailActionBean.
			Response res = emailService.modifyEmail(email, userId);

			try {
				if (res.success) {
					M_Activity activity = new M_Activity(
							M_ActivityType.modified, email.id,
							M_ObjectType.mail);
					activity.userId = userId;
					activity.content = json;
					activityService.save(activity);
				}
			} catch (M_ActivityDBException e) {
				logger.error("modify email activity failed. " + e.getMessage());
				throw new InternalServerError(" modify email activity failed. ");
			}

			return res;
		}
		if (emailActionBean.updateType == 2) {
			long mailId = emailActionBean.id;
			long receiverId = emailActionBean.receiverId;
			Response res = emailService.mailto(mailId, receiverId, userId);

			try {
				if (res.success) {
					M_Activity activity = new M_Activity(
							M_ActivityType.modified, mailId, M_ObjectType.mail);
					activity.userId = userId;
					activity.content = json;
					activityService.save(activity);
				}
			} catch (M_ActivityDBException e) {
				logger.error("modify email activity failed. " + e.getMessage());
				throw new InternalServerError(" modify email activity failed. ");
			}

			return res;
		}
		Response res = ResponseUtil.createResponse(userId, true,
				"Nichts wurde geändert.");
		return res;
	}

	@RequestMapping(method = RequestMethod.GET)
	public Response listEmails(
			Principal principal,
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "paging", defaultValue = "") String paging,
			@RequestParam(value = "filter", defaultValue = "") String statusFilter,
			@RequestParam(value = "start", defaultValue = "") String startLastUpdatedFilter,
			@RequestParam(value = "end", defaultValue = "") String endLastUpdatedFilter) {

		long userId = Long.valueOf(principal.getName());

		logger.info("List Email ");

		EmailQueryScript queryscript = new EmailQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;
			logger.info("list email sort param " + toSwitch);
			switch (toSwitch) {
			case "lastModified":
				queryscript.sortByTime = true;
				break;
			case "subject":
				queryscript.sortBySubject = true;
				break;
			case "email":
				queryscript.sortByEmail = true;
				break;
			case "fullname":
				queryscript.sortByFirstName = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		} else
			queryscript.sortByTime = true;

		// check filter values
		try {
			logger.info("list email filter param " + statusFilter);
			// statusFilter
			if (!statusFilter.isEmpty()) {
				FilterBean[] filterBeans = jacksonObjectMapper.readValue(
						statusFilter, FilterBean[].class);
				// String[] statusParsed = statusFilter.split(", ");
				//
				HashSet<Integer> status = new HashSet<>();

				for (FilterBean s : filterBeans) {
					if (s.name.equals("emailstatus"))
						status.add(Integer.valueOf(s.value));
				}

				queryscript.status = status;
			}

			if (!startLastUpdatedFilter.isEmpty())
				queryscript.startTime = Long.parseLong(startLastUpdatedFilter);

			if (!endLastUpdatedFilter.isEmpty())
				queryscript.endTime = Long.parseLong(endLastUpdatedFilter);

			logger.info("paging obj: " + paging);
			if (!paging.isEmpty()) {
				PagingBean pagingBean = jacksonObjectMapper.readValue(paging,
						PagingBean.class);
				queryscript.paging = pagingBean;
			}
		} catch (Exception iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return emailService.listEmail(queryscript, userId);

	}

	// remove email
	// @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	// public Response removeEmail(@PathVariable("id") long id) {
	// long userId = 1L;
	//
	// logger.info("remove email " + id);
	//
	// Response res = emailService.removeEmail(id, userId);
	//
	// try {
	// if (res.success) {
	// M_Activity activity = new M_Activity(M_ActivityType.deleted,
	// id, M_ObjectType.mail);
	// activity.userId = userId;
	// activity.content = "email-" + id;
	// activityService.save(activity);
	// }
	// } catch (M_ActivityDBException e) {
	// logger.error("update email activity failed. " + e.getMessage());
	// throw new InternalServerError(" update email activity failed. ");
	// }
	//
	// return res;
	//
	// }

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Response getEmail(Principal principal, @PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		logger.info("get email " + id);

		Response res = emailService.getDetail(id, userId);

		return res;

	}
}
