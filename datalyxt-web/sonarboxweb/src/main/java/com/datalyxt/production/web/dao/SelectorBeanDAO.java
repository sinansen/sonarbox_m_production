package com.datalyxt.production.web.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.web.bean.selector.SelectorBean;
import com.datalyxt.production.web.bean.selector.TemplateBean;
import com.datalyxt.production.webdatabase.dao.M_RuleAdminDAO;

public interface SelectorBeanDAO extends M_RuleAdminDAO{

	public List<SelectorBean> getSelectorBeans(String sql, Object[] params, long userId) throws M_RuleDBException;

	public long addTemplate(TemplateBean template)throws M_RuleDBException;

	public List<TemplateBean> getSelectorTemplateBeans(String sql,
			Object[] params, long userId)throws M_RuleDBException;

}
