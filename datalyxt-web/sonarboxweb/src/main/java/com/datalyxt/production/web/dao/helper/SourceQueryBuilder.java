package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;

import com.datalyxt.production.web.bean.source.SourceQueryScript;
import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;

public class SourceQueryBuilder {

	private static String buildConditionWithTypes(HashSet<M_SourceType> types) {

		String condition = " MATCH (type) AGAINST ( ' ? ' IN BOOLEAN MODE)";

		String[] typeArray = new String[types.size()];
		int count = 0;
		for (M_SourceType type : types) {
			typeArray[count] = type.name();
			count = count + 1;
		}

		String typeString = String.join(" ", typeArray);

		condition = condition.replace("?", typeString);

		return condition;

	}

	private static String buildConditionWithLanguages(HashSet<LanguageISO_639_1> langs) {

		String condition = " languages like ('%\"";

		String[] parmsArray = new String[langs.size()];
		int count = 0;
		for (LanguageISO_639_1 lang : langs) {
			parmsArray[count] = condition + lang.name() + "\"%')";
			count = count + 1;
		}

		String paramString = String.join(" or ", parmsArray);

		condition = " ( " + paramString + " ) ";
		return condition;

	}

	private static String buildConditionWithStatus(HashSet<M_SourceStatus> status) {

		String condition = " status = ";

		String[] parmsArray = new String[status.size()];
		int count = 0;
		for (M_SourceStatus param : status) {
			parmsArray[count] = condition + "'" + param.name() + "'";
			count = count + 1;
		}

		String paramString = String.join(" or ", parmsArray);

		condition = " ( " + paramString + " ) ";

		return condition;

	}

	public static QueryWrapper buildQueryForSearch(SourceQueryScript script) {

		String sqlBasePart = "select id, name, url, url_normalized, nurl_md5, domain, created_at, last_modified, feedback, status, config , type, languages from m_source  ";
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithPartofURL = " ( url like ? or name like ? ) ";

		String conditionWithStartTimeCreated = " created_at >= ? ";
		String conditionWithEndTimeCreated = " created_at <= ? ";

		String conditionWithStartTimeModi = " last_modified >= ? ";
		String conditionWithEndTimeModi = " last_modified <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();
		ArrayList<Object> params = new ArrayList<>();

		if (script.fetchAll && script.endTimeModified <= 0) {

			sql = sqlBasePart;

		}

		boolean hasWhere = false;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			sql = sql + where + conditionWithPartofURL;
			params.add("%" + script.searchTerm + "%");
			params.add("%" + script.searchTerm + "%");
			hasWhere = true;
		}

		String exclusiveStatus = " status !=  ? ";
		if (hasWhere)
			sql = sql + combination + exclusiveStatus;
		else {
			sql = sql + where + exclusiveStatus;
			hasWhere = true;
		}
		params.add(M_SourceStatus.deleted.name());

		if (!script.types.isEmpty()) {
			String conditionWithTypes = buildConditionWithTypes(script.types);

			if (hasWhere)
				sql = sql + combination + conditionWithTypes;
			else {
				sql = sql + where + conditionWithTypes;
				hasWhere = true;
			}

		}

		if (!script.languages.isEmpty()) {
			String condition = buildConditionWithLanguages(script.languages);
			if (hasWhere)
				sql = sql + combination + condition;
			else {
				sql = sql + where + condition;
				hasWhere = true;
			}
		}

		if (!script.status.isEmpty()) {
			String condition = buildConditionWithStatus(script.status);
			if (hasWhere)
				sql = sql + combination + condition;
			else {
				sql = sql + where + condition;
				hasWhere = true;
			}
		}

		if (script.endTimeCreated > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTimeCreated + combination + conditionWithEndTimeCreated;
			else {
				sql = sql + where + conditionWithStartTimeCreated + combination + conditionWithEndTimeCreated;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTimeCreated));
			params.add(new Timestamp(script.endTimeCreated));

		}

		if (script.endTimeModified > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTimeModi + combination + conditionWithEndTimeModi;
			else {
				sql = sql + where + conditionWithStartTimeModi + combination + conditionWithEndTimeModi;
				hasWhere = true;
			}
			params.add(new Timestamp(script.startTimeModified));
			params.add(new Timestamp(script.endTimeModified));

		}

		sql = sql + orderBy;

		if (script.sortByCreated)
			sql = sql + " created_at ";
		else if (script.sortByDomain)
			sql = sql + " domain ";
		else if (script.sortByName)
			sql = sql + " name ";
		else
			sql = sql + " last_modified ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

}
