package com.datalyxt.production.web.exception;

public class AuthenticationException extends RuntimeException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5235561854949897553L;

	public AuthenticationException(String msg) {
		super(msg);
	}

	public AuthenticationException(String msg, Exception e) {
		super(msg, e);
	}
}
