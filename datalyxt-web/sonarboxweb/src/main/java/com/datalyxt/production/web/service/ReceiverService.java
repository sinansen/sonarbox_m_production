package com.datalyxt.production.web.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datalyxt.production.exception.db.M_ReceiverDBException;
import com.datalyxt.production.exception.db.M_ReceiverGroupDBException;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverGroup;
import com.datalyxt.production.receiver.M_ReceiverGroupStatus;
import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.receiver.GroupBasicBean;
import com.datalyxt.production.web.bean.receiver.ReceiverGroupConfigScript;
import com.datalyxt.production.web.bean.receiver.ReceiverGroupQueryScript;
import com.datalyxt.production.web.bean.receiver.ReceiverQueryScript;
import com.datalyxt.production.web.bean.receiver.ReceiverUIBean;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.web.dao.helper.ReceiverGroupQueryBuilder;
import com.datalyxt.production.web.dao.helper.ReceiverQueryBuilder;
import com.datalyxt.production.webdatabase.dao.M_ReceiverDAO;
import com.datalyxt.util.EmailUtil;
import com.datalyxt.util.NameUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ReceiverService {
	private static Logger logger = LoggerFactory
			.getLogger(ReceiverService.class);

	@Autowired
	M_ReceiverDAO receiverDAO;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	// Create a Receiver
	public Response createReceiver(M_Receiver receiver, long userId) {
		try {
			Response res = checkReceiverInput(receiver, userId);
			if (!res.success) {
				return res;
			}

			if (receiverDAO.getReceiverByEmail(receiver.email) != null) {
				res = createResponse(userId, false,
						"Receiver existiert bereits");
				return res;
			}

			receiver.status = M_ReceiverStatus.added;
			receiver.createdAt = System.currentTimeMillis();
			receiver.lastModified = receiver.createdAt;
			receiver.ownerId = userId;
			receiverDAO.insertReceiver(receiver);
			receiverDAO
					.addReceiverToGroup(receiver.groups, receiver.id, userId);
			res = createResponse(userId, true, "Benutzer hinzugefügt");

			return res;
		} catch (M_ReceiverDBException | M_ReceiverGroupDBException e) {
			logger.error(
					"exception in creating receiver in receiver db: "
							+ e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Speichern");
			return res;
		}

	}

	// list Receiver
	public Response listReceiver(ReceiverQueryScript script, long userId) {
		try {

			QueryWrapper wrapper = ReceiverQueryBuilder
					.buildQueryForSearch(script);
			List<M_Receiver> receivers = receiverDAO.getReceiver(wrapper.sql,
					wrapper.params);
			List<ReceiverUIBean> rvs = new ArrayList<>();
			Response res;
			if (receivers.isEmpty())
				res = createResponse(userId, true, "Receiver nicht gefunden");
			else {
				res = createResponse(userId, true, "Receiver gefunden");
				HashSet<Long> groupIds = new HashSet<>();
				for (M_Receiver rec : receivers) {
					groupIds.addAll(rec.groups);
				}
				HashMap<Long, M_ReceiverGroup> groups = receiverDAO
						.getReceiverGroups(groupIds);

			
				for (M_Receiver rec : receivers) {
					ReceiverUIBean receiverUIBean = new ReceiverUIBean();
					receiverUIBean.company = rec.company;
					receiverUIBean.email = rec.email;
					receiverUIBean.id = rec.id;
					receiverUIBean.firstName = rec.firstName;
					receiverUIBean.lastName = rec.lastName;
					receiverUIBean.address = rec.address;
					receiverUIBean.status = rec.status;
					receiverUIBean.createdAt = rec.createdAt;
					receiverUIBean.lastModified = rec.lastModified;
					receiverUIBean.theFunction = rec.function;
					receiverUIBean.category = rec.category;
					receiverUIBean.department = rec.department;

					for (Long gid : rec.groups) {
						M_ReceiverGroup g = groups.get(gid);
						if (g != null) {
							GroupBasicBean b = new GroupBasicBean();
							b.id = g.id;
							b.name = g.name;
							receiverUIBean.groups.add(b);
						}
					}
					rvs.add(receiverUIBean);
				}
				
			}
			res.body = rvs;
			return res;
		} catch (Exception e) {
			logger.error(
					"exception in get receiver from receiver db: "
							+ e.getMessage(), e);
			Response res = createResponse(userId, false, "Fehler beim Suchen");
			return res;
		}

	}

	private Response checkReceiverInput(M_Receiver receiver, long userId) {

		if (receiver.address == null || receiver.address.isEmpty()) {
			Response res = createResponse(userId, false, "Anrede ist falsch");
			return res;
		}

		if (!NameUtil.isValidName(receiver.firstName)) {
			Response res = createResponse(userId, false, "Vorname ist falsch");
			return res;
		}

		if (!NameUtil.isValidName(receiver.lastName)) {
			Response res = createResponse(userId, false, "Nachname ist falsch");
			return res;
		}

		if (!EmailUtil.isValidEmail(receiver.email)
				|| receiver.email.length() > 100) {
			Response res = createResponse(userId, false, "E-Mail ist falsch");
			return res;
		}
		if (receiver.company == null || receiver.company.isEmpty()
				|| receiver.company.length() > 200) {
			Response res = createResponse(userId, false, "Company ist falsch");
			return res;
		}

		if (receiver.function == null || receiver.function.isEmpty()
				|| receiver.function.length() > 500) {
			Response res = createResponse(userId, false, "function ist falsch");
			return res;
		}

		if (receiver.department == null || receiver.department.isEmpty()
				|| receiver.company.length() > 500) {
			Response res = createResponse(userId, false,
					"department ist falsch");
			return res;
		}

		if (receiver.category == null || receiver.category.isEmpty()
				|| receiver.company.length() > 500) {
			Response res = createResponse(userId, false, "category ist falsch");
			return res;
		}

		return createResponse(userId, true, "check ist durch");

	}

	public Response createResponse(long uid, boolean success, String msg) {
		Response res = new Response();
		res.userid = uid;
		res.success = success;
		res.message = msg;
		return res;
	}

	// modify receiver
	public Response modifyReceiver(M_Receiver receiver, long userId) {

		try {
			Response res = checkReceiverInput(receiver, userId);

			if (!res.success)
				return res;

			M_Receiver oldReceiver = receiverDAO.getReceiverById(receiver.id,
					userId);

			if (oldReceiver == null) {
				res = createResponse(userId, false, "Receiver existiert nicht");
				return res;
			}
			// if (receiver.email != null
			// && oldReceiver.email.equals(receiver.email)) {
			// res = createResponse(userId, false,
			// "Receiver Email existiert bereits");
			// return res;
			// }

			receiver.lastModified = System.currentTimeMillis();

			QueryWrapper wrapper = ReceiverQueryBuilder
					.buildQueryForModify(receiver);
			if (wrapper.params == null || wrapper.params.length == 0) {
				res = createResponse(userId, true,
						"Keine Parameter zu aktualisieren");
				return res;
			}

			receiverDAO.modifyReceiver(wrapper.sql, wrapper.params);

			Collection<Long> removedGroupIds = CollectionUtils.subtract(
					oldReceiver.groups, receiver.groups);
			HashSet<Long> gids = new HashSet<Long>();
			gids.addAll(removedGroupIds);
			// if (!receiver.groups.isEmpty())
			receiverDAO
					.addReceiverToGroup(receiver.groups, receiver.id, userId);
			receiverDAO.removeReceiverFromGroup(gids, receiver.id, userId);
			M_Receiver modified = receiverDAO.getReceiverById(receiver.id,
					userId);
			if (modified == null) {
				res = createResponse(userId, false, "Fehler beim Modifizieren");

			} else {
				res = createResponse(userId, true,
						"Receiver wurde aktualisiert");
				res.body = modified;

			}

			return res;

		} catch (Exception e) {
			logger.error("exception in modify receiver: " + e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Modifizieren");
			return res;
		}

	}

	private Response checkModifyReceiverInput(M_Receiver receiver, long userId) {

		if (receiver.email != null) {

			if (!EmailUtil.isValidEmail(receiver.email)) {
				Response res = createResponse(userId, false,
						"E-Mail ist falsch");
				return res;
			}

			if (receiver.email.length() > 100) {
				Response res = createResponse(userId, false,
						"E-Mail ist falsch");
				return res;
			}
		}
		if (receiver.firstName != null) {

			if (!NameUtil.isValidName(receiver.firstName)) {
				Response res = createResponse(userId, false, "Name ist falsch");
				return res;
			}

		}

		if (receiver.lastName != null) {
			if (!NameUtil.isValidName(receiver.lastName)) {
				Response res = createResponse(userId, false,
						"Nachname ist falsch");
				return res;
			}
		}

		if (receiver.company != null) {
			if (receiver.company.length() > 200) {
				Response res = createResponse(userId, false,
						"Company ist falsch");
				return res;
			}
		}

		return createResponse(userId, true, "check ist durch");

	}

	// create group
	public Response createReceiverGroup(M_ReceiverGroup receiverGroup,
			long userId) {
		logger.info("create receiver group: " + receiverGroup.name);
		try {
			Response res = checkReceiverGroupInput(receiverGroup, userId);
			if (!res.success) {
				return res;
			}

			if (receiverDAO.isGroupNameExist(receiverGroup.name, userId)) {
				res = createResponse(userId, false, "Gruppe existiert bereits");
				return res;
			}
			receiverGroup.owner = userId;
			receiverGroup.status = M_ReceiverGroupStatus.added;
			receiverGroup.createdAt = System.currentTimeMillis();
			receiverGroup.lastModified = receiverGroup.createdAt;

			receiverDAO.insertReceiverGroup(receiverGroup);
			receiverDAO.addReceiverToGroup(receiverGroup.id,
					receiverGroup.member, userId);

			res = createResponse(userId, true, "Gruppe wurde angelegt");
			res.body = receiverGroup;
			return res;
		} catch (M_ReceiverGroupDBException e) {
			logger.error(
					"exception in creating receiver group in receiver group db: "
							+ e.getMessage(), e);
			Response res = createResponse(userId, false, "Fehler beim Anlegen");
			return res;
		}
	}

	// modify receiver group
	public Response modifyReceiverGroup(M_ReceiverGroup receiverGroup,
			long userId) {

		try {
			Response res = checkReceiverGroupInput(receiverGroup, userId);

			if (!res.success)
				return res;
			M_ReceiverGroup oldGroup = receiverDAO
					.getReceiverGroupById(receiverGroup.id, userId);
			if (oldGroup == null) {
				res = createResponse(userId, false,
						"Receiver Gruppe existiert nicht");
				return res;
			}

			receiverGroup.lastModified = System.currentTimeMillis();

			QueryWrapper wrapper = ReceiverGroupQueryBuilder
					.buildQueryForModify(receiverGroup);
			if (wrapper.params == null || wrapper.params.length == 0) {
				res = createResponse(userId, true,
						"Keine Parameter zu aktualisieren");
				return res;
			}
			logger.info(wrapper.sql);
			receiverDAO.modifyReceiverGroup(wrapper.sql, wrapper.params);

			Collection<Long> removedReceivers = CollectionUtils.subtract(
					oldGroup.member, receiverGroup.member);
			HashSet<Long> rids = new HashSet<Long>();
			rids.addAll(removedReceivers);
			logger.info("old receivers "+oldGroup.member.toString());
			logger.info("new receivers "+receiverGroup.member.toString());
			logger.info("removed receivers "+rids.toString());
			receiverDAO.addReceiverToGroup(receiverGroup.id,
					receiverGroup.member, userId);
			receiverDAO.removeReceiverFromGroup(receiverGroup.id, rids, userId);

			res = createResponse(userId, true,
					"Receiver Gruppe wurde aktualisiert");
			res.body = receiverGroup;

			return res;

		} catch (Exception e) {
			logger.error(
					"exception in modify receiver group: " + e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Modifizieren");
			return res;
		}

	}

	// list Receiver Group
	public Response listReceiverGroup(ReceiverGroupQueryScript script,
			long userId) {
		try {

			QueryWrapper wrapper = ReceiverGroupQueryBuilder
					.buildQueryForSearch(script);
			List<M_ReceiverGroup> receiverGroup = receiverDAO
					.getReceiverGroups(wrapper.sql, wrapper.params);
			Response res;
			if (receiverGroup.isEmpty())
				res = createResponse(userId, true,
						"Receiver Gruppe nicht gefunden");
			else {
				res = createResponse(userId, true, "Receiver Gruppe gefunden");
			
			}
			res.body = receiverGroup;
			return res;
		} catch (Exception e) {
			logger.error(
					"exception in get receiver group from receiver group db: "
							+ e.getMessage(), e);
			Response res = createResponse(userId, false, "Fehler beim Suchen");
			return res;
		}

	}

	// add receiver to group
	public Response addReceiverToGroup(ReceiverGroupConfigScript script,
			long userId) {
		if (script.groupId == 0) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}
		try {

			int[] changedRecords = receiverDAO.addReceiverToGroup(
					script.groupId, script.groupMember, userId);
			int changedCounter = 0;
			for (int v : changedRecords)
				if (v == 1)
					changedCounter = changedCounter + 1;

			M_ReceiverGroup group = new M_ReceiverGroup();
			group.id = script.groupId;
			group.member = script.groupMember;
			if (changedCounter > 0) {
				Response res = createResponse(userId, true,
						"Receiver hinzufügen OK. " + changedCounter
								+ " Receiver wurden hinzugefügt");
				return res;
			} else {
				Response res = createResponse(userId, true,
						"Receiver exsitieren bereits in die Gruppe");
				return res;
			}

		} catch (Exception e) {
			logger.error("exception in addReceiverToGroup: " + e.getMessage(),
					e);
			Response res = createResponse(userId, false,
					"Fehler beim Receiver hinzuzufügen");
			return res;
		}

	}

	// remove receiver from group
	public Response removeReceiverFromGroup(ReceiverGroupConfigScript script,
			long userId) {
		if (script.groupId == 0) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		try {

			int[] changedRecords = receiverDAO.removeReceiverFromGroup(
					script.groupId, script.groupMember, userId);
			int changedCounter = 0;
			for (int v : changedRecords)
				if (v == 1)
					changedCounter = changedCounter + 1;
			M_ReceiverGroup group = new M_ReceiverGroup();
			group.id = script.groupId;
			group.member = script.groupMember;
			if (changedCounter > 0) {

				Response res = createResponse(userId, true,
						"Receiver Entfernen OK. " + changedCounter
								+ " Receiver wurden entfernt.");
				return res;
			} else {
				Response res = createResponse(userId, true,
						"Receiver existieren nicht in die Gruppe.");
				return res;
			}

		} catch (Exception e) {
			logger.error(
					"exception in removeReceiverFromGroup: " + e.getMessage(),
					e);
			Response res = createResponse(userId, false,
					"Fehler beim Receiver zu entfernen");
			return res;
		}
	}

	// list group member
	public Response listGroupMember(long groupId, long userId) {
		try {

			List<M_Receiver> members = receiverDAO
					.getReceiverByGroupId(groupId);

			Response res = createResponse(userId, true,
					"List Gruppenmitgelieder OK");
			res.body = members;

			return res;

		} catch (Exception e) {
			logger.error("exception in listGroupMember: " + e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Suchen Gruppenmitglieder");
			return res;
		}
	}

	private Response checkReceiverGroupInput(
			M_ReceiverGroup receiverGroup, long userId) {
		if (receiverGroup.name == null || receiverGroup.name.length() < 2
				|| receiverGroup.name.length() > 200) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}
		if (receiverGroup.description == null
				|| receiverGroup.description.length() < 2
				|| receiverGroup.description.length() > 500) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}
		return createResponse(userId, true, "check ist durch");

	}

	public Response removeReceiver(long receiverId, long userId) {
		M_Receiver receiver = new M_Receiver();
		receiver.id = receiverId;
		receiver.status = M_ReceiverStatus.deleted;

		try {
			receiverDAO.removeReceiver(receiver.id, userId);
			receiverDAO.removeReceiverFromAllGroup(receiverId, userId);
		} catch (M_ReceiverDBException e) {
			logger.error("exception in remove Receiver: " + e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Receiver von allen Gruppen zu entfernen");
			return res;
		}

		Response res = createResponse(userId, true,
				"Receiver wurde erfolgreich entfernt.");
		return res;

	}

	public Response removeGroup(long groupId, long ownerId) {
		int result;
		try {
			result = receiverDAO.removeGroup(groupId, ownerId);
			if (result == 1) {
				Response res = createResponse(ownerId, true,
						"Gruppe wurde erfolgreich entfernt.");
				return res;
			} else {
				Response res = createResponse(
						ownerId,
						false,
						"Fehler beim Gruppen zu entfernen : Sie benötigen Berechtigungen zur Durchführung des Vorgangs.");
				return res;
			}
		} catch (M_ReceiverGroupDBException e) {
			Response res = createResponse(ownerId, false,
					"Fehler beim Gruppen zu entfernen");
			return res;
		}

	}

	public Response changeReceiverStatus(long id, M_ReceiverStatus status,
			long ownerId) {
		try {
			receiverDAO.changeReceiverStatus(id, status, ownerId);
			Response res = createResponse(ownerId, true,
					"Status wurde erfolgreich modifiziert.");
			return res;
		} catch (M_ReceiverDBException e) {
			Response res = createResponse(ownerId, false,
					"Fehler beim Status zu modifizieren.");
			return res;
		}

	}

	public Response changeGroupStatus(long id, M_ReceiverGroupStatus status,
			long ownerId) {
		try {
			receiverDAO.changeGroupStatus(id, status, ownerId);
			Response res = createResponse(ownerId, true,
					"Status wurde erfolgreich modifiziert.");
			return res;
		} catch (M_ReceiverGroupDBException e) {
			Response res = createResponse(ownerId, false,
					"Fehler beim Status zu modifizieren.");
			return res;
		}

	}

	public M_Receiver convert(ReceiverUIBean receiver) {
		M_Receiver changed = new M_Receiver();
		changed.id = receiver.id;
		changed.company = receiver.company;
		changed.email = receiver.email;
		changed.firstName = receiver.firstName;
		changed.lastName = receiver.lastName;
		changed.address = receiver.address;
		changed.status = receiver.status;
		changed.createdAt = receiver.createdAt;
		changed.lastModified = receiver.lastModified;
		changed.department = receiver.department;
		changed.category = receiver.category;
		changed.function = receiver.theFunction;
		for (GroupBasicBean b : receiver.groups)
			changed.groups.add(b.id);
		return changed;
	}
}
