package com.datalyxt.production.web.service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.session.Session;
import org.springframework.session.data.redis.RedisOperationsSessionRepository;
import org.springframework.stereotype.Service;

import com.datalyxt.production.web.bean.AuthResponse;
import com.datalyxt.production.web.bean.UserBean;
import com.datalyxt.production.web.dao.UserDAO;
import com.datalyxt.production.web.exception.AuthenticationException;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.security.UserAuthentication;

@Service
public class AuthenticationService {

	private static Logger logger = LoggerFactory
			.getLogger(AuthenticationService.class);

	@Resource(name = "redisTemplate")
	private ValueOperations<String, String> valueOps;

	@Autowired
	RedisOperationsSessionRepository repository;

	@Autowired
	UserDAO userDAO;

	@Value("${localtest}")
	private boolean localtest;

	public String getUserBySession(String sessionId) {
		logger.info("check session " + sessionId);
		if (sessionId == null)
			return null;
		String value = valueOps.get(sessionId);
		if (value == null || value.trim().isEmpty()) {
			logger.info("user info not found. forward to Authentication ."
					+ sessionId);
			return null;
		} else {
			String uid = value;
			logger.info("user is verified. " + uid);
			return uid;
		}
	}

	public void updateAuth(AuthResponse authResponse) {
		valueOps.set(authResponse.inResponseTo, authResponse.userName);
		try {
			UserBean user = userDAO.findByUserName(authResponse.userName);
			if (user == null)
				user = userDAO.saveUser(authResponse);
		} catch (Exception e) {
			logger.error("update user auth failed. " + e.getMessage(), e);
			throw new InternalServerError("update auth failed.");
		}

	}

	public boolean isLogin(String sessionId) {
		Session redisSession = repository.getSession(sessionId);
		if (redisSession == null) {
			logger.info("session is timeout. " + sessionId);
			return false;
		}
		String userInfo = getUserBySession(sessionId);
		if (userInfo == null)
			return false;
		else
			return true;
	}

	public Authentication getAuthentication(HttpServletRequest request)  {
		if (localtest) {
			User user = new User("1", request.getSession().getId(), true, true, true, true,
					AuthorityUtils.createAuthorityList("USER"));
			final UserAuthentication userAuthentication = new UserAuthentication(user);
			return userAuthentication;
		}
		final String token = request.getSession().getId();
		logger.info("request token: " + token);
		if (token != null) {
			String uname = getUserBySession(token);
			UserBean userBean = null;
			try {
				userBean = userDAO.findByUserName(uname);
			} catch (Exception e) {
				logger.error("find user failed. "+e.getMessage() , e);
				throw new AuthenticationException("authticate user failed.");
			}
			if (userBean == null)
				return null;
			
			User user = new User(String.valueOf(userBean.id), token, true, true, true, true, AuthorityUtils.createAuthorityList("USER"));
			final UserAuthentication userAuthentication = new UserAuthentication(user);
			return userAuthentication;
		}
		return null;
	}
}
