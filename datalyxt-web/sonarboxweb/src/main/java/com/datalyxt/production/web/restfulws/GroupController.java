package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.receiver.M_ReceiverGroup;
import com.datalyxt.production.receiver.M_ReceiverGroupStatus;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.receiver.ReceiverGroupConfigScript;
import com.datalyxt.production.web.bean.receiver.ReceiverGroupMemberQueryScript;
import com.datalyxt.production.web.bean.receiver.ReceiverGroupQueryScript;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ActivityService;
import com.datalyxt.production.web.service.ReceiverService;
import com.datalyxt.production.web.service.ResponseUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("vims/sonarbox/res/groups")
@RestController
public class GroupController {

	private static Logger logger = LoggerFactory
			.getLogger(GroupController.class);

	@Autowired
	ReceiverService receiverService;

	@Autowired
	ActivityService activityService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	// Create a receiver group
	@RequestMapping(method = RequestMethod.POST)
	public Response createGroup(Principal principal,@RequestBody String receiverGroupJson) {
		M_ReceiverGroup receiverGroup = new M_ReceiverGroup();
		long userId = Long.valueOf(principal.getName());
		logger.info("Creating Receiver Group " + receiverGroupJson);
		try {
			receiverGroup = jacksonObjectMapper.readValue(receiverGroupJson,
					M_ReceiverGroup.class);
			receiverGroup.name = receiverGroup.name.trim();
			receiverGroup.description = receiverGroup.description.trim();
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		Response res = receiverService.createReceiverGroup(receiverGroup,
				userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.created,
						receiverGroup.id, M_ObjectType.group);
				activity.userId = userId;
				activity.content = receiverGroupJson;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("save receivergroup activity failed. "
					+ e.getMessage());
			throw new InternalServerError(
					" save receivergroup activity failed. ");
		}

		return res;

	}

	@RequestMapping(method = RequestMethod.PUT)
	public Response modifyReceiverGroup(Principal principal, @RequestBody String receiverGroupJson) {
		long userId = Long.valueOf(principal.getName());
		logger.info("modify Receiver group " + receiverGroupJson);
		M_ReceiverGroup receiverGroup;
		try {
			receiverGroup = jacksonObjectMapper.readValue(receiverGroupJson,
					M_ReceiverGroup.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		receiverGroup.owner = userId;
		Response res = receiverService.modifyReceiverGroup(receiverGroup,
				userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.modified,
						receiverGroup.id, M_ObjectType.group);
				activity.userId = userId;
				activity.content = receiverGroupJson;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("modify receivergroup activity failed. "
					+ e.getMessage());
			throw new InternalServerError(
					" modify receivergroup activity failed. ");
		}

		return res;

	}

	@RequestMapping(method = RequestMethod.GET)
	public Response listReceiverGroup(
			Principal principal,
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "status", defaultValue = "") String statusFilter,
			@RequestParam(value = "start", defaultValue = "") String startLastModifiedFilter,
			@RequestParam(value = "end", defaultValue = "") String endLastModifiedFilter) {

		long userId = Long.valueOf(principal.getName());
		
		logger.info("List Group ");

		ReceiverGroupQueryScript queryscript = new ReceiverGroupQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;

			switch (toSwitch) {
			case "name":
				queryscript.sortByName = true;
				break;
			case "lastModified":
				queryscript.sortByModified = true;
				break;
			case "created":
				queryscript.sortByCreated = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		}

		// check filter values
		try {

			// statusFilter
			if (!statusFilter.isEmpty()) {
				String[] statusParsed = statusFilter.split(", ");

				HashSet<M_ReceiverGroupStatus> status = new HashSet<>();

				for (String s : statusParsed) {
					status.add(M_ReceiverGroupStatus.valueOf(s));
				}

				queryscript.status = status;
			}

			// lastModified
			if (!startLastModifiedFilter.isEmpty())
				queryscript.startTimeModified = Long
						.parseLong(startLastModifiedFilter);
			if (!endLastModifiedFilter.isEmpty())
				queryscript.endTimeModified = Long
						.parseLong(endLastModifiedFilter);

		} catch (IllegalArgumentException iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return receiverService.listReceiverGroup(queryscript, userId);

	}

	@RequestMapping(value = "/members", method = RequestMethod.GET)
	public Response listReceiverGroupMember(Principal principal,
			@RequestParam(value = "query", defaultValue = "") String query) {
		long userId = Long.valueOf(principal.getName());
		ReceiverGroupMemberQueryScript script;
		try {
			script = jacksonObjectMapper.readValue(query,
					ReceiverGroupMemberQueryScript.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		logger.info("List Receiver Group" + script.groupId);

		Response res = receiverService.listGroupMember(script.groupId, userId);

		return res;

	}

	// add receiver to group
	@RequestMapping(value = "/members", method = RequestMethod.POST)
	public Response addGroupMember(Principal principal,@RequestBody String json) {
		ReceiverGroupConfigScript script;
		long userId = Long.valueOf(principal.getName());
		try {
			script = jacksonObjectMapper.readValue(json,
					ReceiverGroupConfigScript.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		logger.info("add Group Receiver " + script.groupId);

		Response res = receiverService.addReceiverToGroup(script, userId);
		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.created,
						script.groupId, M_ObjectType.group);
				activity.userId = userId;
				activity.content = json;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("add receivergroup member activity failed. "
					+ e.getMessage());
			throw new InternalServerError(
					" add receivergroup member activity failed. ");
		}
		return res;

	}

	// remove receiver from group
	@RequestMapping(value = "/members", method = RequestMethod.DELETE)
	public Response removeGroupMember(Principal principal,@RequestBody String json) {
		ReceiverGroupConfigScript script;
		long userId = Long.valueOf(principal.getName());
		try {
			script = jacksonObjectMapper.readValue(json,
					ReceiverGroupConfigScript.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		logger.info("remove Group Receiver " + script.groupId);

		Response res = receiverService.removeReceiverFromGroup(script, userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.deleted,
						script.groupId, M_ObjectType.group);
				activity.userId = userId;
				activity.content = json;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("remove receivergroup member activity failed. "
					+ e.getMessage());
			throw new InternalServerError(
					" remove receivergroup member activity failed. ");
		}

		return res;

	}

	// remove group
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Response removeGroup(Principal principal,@PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		logger.info("remove group " + id);

		Response res = receiverService.removeGroup(id, userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.deleted,
						id, M_ObjectType.group);
				activity.userId = userId;
				activity.content = "group-" + id;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("update group activity failed. " + e.getMessage());
			throw new InternalServerError(" update group activity failed. ");
		}

		return res;

	}
	
	@RequestMapping(value = "{id}/status", method = RequestMethod.PUT)
	public Response changeStatus(Principal principal,@PathVariable("id") long id,
			@RequestBody String statusInput) {

		logger.info("Change group status for " + id + " " + statusInput);
		long userId = Long.valueOf(principal.getName());

		// change source status
		try {
			JsonNode object = jacksonObjectMapper.readValue(statusInput, JsonNode.class);
			String sts = object.get("status").textValue();
			
			M_ReceiverGroupStatus status = M_ReceiverGroupStatus
					.valueOf(sts);
			receiverService.changeGroupStatus(id, status, userId);

			// log activity
			M_ActivityType type;

			switch (status) {
			case activated:
				type = M_ActivityType.activated;
				break;
			case paused:
				type = M_ActivityType.paused;
				break;
			default:
				throw new WrongParameterException(
						"Fehler, Diese Modifizierung des Statuses ist nicht möglich");

			}

			M_Activity activity = new M_Activity(type, id,
					M_ObjectType.receiver);
			activity.userId = userId;
			activityService.save(activity);

		} catch (Exception e) {
			throw new InternalServerError(
					"Fehler, Änderung des Receiverstatuses fehlgeschlagen");
		}

		// return response
		return ResponseUtil.createResponse(userId, true,
				"Receiver Status erfolgreich verändert");
	}
}
