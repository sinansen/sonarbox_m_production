package com.datalyxt.production.web.bean.selector;

import com.datalyxt.production.webmodel.rule.M_RuleStatus;

public class TemplateBean {
	public long id;
	public String nurl;
	public String md5;
	public String config;
	public long createdAt;
	public long lastModified;
	public M_RuleStatus status;
	public long ownerId;
	public String name;
}
