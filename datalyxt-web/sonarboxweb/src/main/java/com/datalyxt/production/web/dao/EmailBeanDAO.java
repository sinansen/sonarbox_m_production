package com.datalyxt.production.web.dao;

import java.util.HashSet;
import java.util.List;

import com.datalyxt.exception.db.EmailDBException;
import com.datalyxt.production.web.bean.email.EmailActionBean;
import com.datalyxt.production.web.bean.email.EmailDetail;
import com.datalyxt.production.webdatabase.dao.EmailDAO;
import com.datalyxt.production.webmodel.email.Email;

public interface EmailBeanDAO extends EmailDAO{

	public List<EmailActionBean> getEmails(String sql, Object[] params)throws EmailDBException;

	public EmailDetail getEmailDetail(long mailId, long userId)throws EmailDBException;

	public EmailDetail getSingleEmailDetail(long mailId, long receiverId, long userId)
			throws EmailDBException;

	public void updateEmailReceiver(Email email, HashSet<Long> receiverIds,
			long ownerId) throws EmailDBException;

}
