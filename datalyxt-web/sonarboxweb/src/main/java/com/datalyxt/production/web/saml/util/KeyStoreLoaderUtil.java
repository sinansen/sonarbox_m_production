package com.datalyxt.production.web.saml.util;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Enumeration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.production.web.exception.KeyStoreException;

//KOmmentar
public class KeyStoreLoaderUtil {

	private static Logger logger = LoggerFactory.getLogger(KeyStoreLoaderUtil.class);
	private static RSAPublicKey rsaPublicKeys[] = null;
	private static RSAPrivateKey rsaPrivateKey = null;
	private static RSAPublicKey rsaPublicKeyForSignature = null;

	public KeyStoreLoaderUtil(String jksPath, String keystorePWD, String privateKeyPassword) {
		try {

			// Links:
			// http://www.herongyang.com/Cryptography/keytool-Export-Key-Dump-Private-Key.html
			// http://code.google.com/apis/apps/articles/sso-keygen.html#JavaKeyTool
			// http://www.agentbob.info/agentbob/79-AB.html
			// http://www.exampledepot.com/egs/java.security/ListAliases.html

			// initialize keystore
			KeyStore jks = KeyStore.getInstance("jks");
			// load keystore
			InputStream in = KeyStoreLoaderUtil.class.getClassLoader().getResourceAsStream(jksPath);
			jks.load(in, keystorePWD.toCharArray());
			// System.out.println("JKS loaded");

			// get private key (not needed here should be in class which signs
			// the
			// assertion - just for demo)
			// Key key = jks.getKey("markant.com.private",
			// "markant".toCharArray());
			// System.out.println("PrivateKey loaded: " + key.toString());

			Enumeration jksenum = jks.aliases();

			rsaPublicKeys = new RSAPublicKey[jks.size()];

			for (int i = 0; jksenum.hasMoreElements(); i++) {
				String alias = (String) jksenum.nextElement();

				if (jks.isKeyEntry(alias)) {
					// No Private Key for validation but for signing
					logger.info("Private Key detected: Privat Keys are not needed for validation - alias: " + alias);
					rsaPrivateKey = (RSAPrivateKey) jks.getKey(alias, privateKeyPassword.toCharArray());

				}

				// Does alias refer to a trusted certificate?
				if (jks.isCertificateEntry(alias)) {
					// TODO - set certificates
					rsaPublicKeys[i] = (RSAPublicKey) jks.getCertificate(alias).getPublicKey();
					logger.info("Public Key loaded - alias: " + alias);
					// markant.com-Public Key is needed for xml signature (due
					// to xml sig spec)
					if (alias.equalsIgnoreCase("markant.com")) {
						rsaPublicKeyForSignature = (RSAPublicKey) jks.getCertificate(alias).getPublicKey();
						logger.info("Public Key for signature loaded - alias: " + alias);
					}
				}

			}

		} catch (Exception e) {
			throw new KeyStoreException("load keystore failed. " + e.getMessage(), e);
		}

	}

	public RSAPublicKey[] getRSAPublicKey() {

		return rsaPublicKeys;
	}

	public RSAPrivateKey getRSAPrivateKey() {

		return rsaPrivateKey;
	}

	public RSAPublicKey getRSAPublicKeyForSignature() {

		return rsaPublicKeyForSignature;
	}

}
