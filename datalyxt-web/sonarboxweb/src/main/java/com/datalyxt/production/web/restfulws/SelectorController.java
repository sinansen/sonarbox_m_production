package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.result.DataRowItemBean;
import com.datalyxt.production.web.bean.selector.RuleQueryScript;
import com.datalyxt.production.web.bean.selector.RuleTemplateQueryScript;
import com.datalyxt.production.web.bean.selector.RuleUpdateScript;
import com.datalyxt.production.web.bean.selector.TemplateBean;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ActivityService;
import com.datalyxt.production.web.service.SelectorService;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.util.url.URLNormalizer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RequestMapping("vims/sonarbox/res/selectors")
@RestController
public class SelectorController {
	private static Logger logger = LoggerFactory.getLogger(SelectorController.class);

	@Autowired
	SelectorService selectorService;

	@Autowired
	ActivityService activityService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	// Create a rule
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public Response createRule(Principal principal, @RequestBody String json) {
		M_Rule rule = new M_Rule();
		long userId = Long.valueOf(principal.getName());

		try {
			RuleUpdateScript script = jacksonObjectMapper.readValue(json, RuleUpdateScript.class);
			rule = selectorService.convertScriptToRule(script);

		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = selectorService.createResponse(userId, false, "Convert Rule Object failed.");
			return res;
		}

		logger.info("Creating selector " + rule.ruleType);

		Response res = selectorService.createRule(rule, userId);
		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.created, rule.id, M_ObjectType.rule);
				activity.userId = userId;
				activity.content = json;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("save rule activity failed. " + e.getMessage());
			throw new InternalServerError(" save rule activity failed. ");
		}
		if (res.success && rule.ruleType.equals(M_RuleType.QT1V1))
			try {
				BlockConfigMain confMain = ((QT1_V1_RuleContent) rule.ruleContent).blockConfigMain;
				selectorService.validateSelectorSyntax(confMain);

			} catch (Exception e) {
				logger.error("validate qt1v1 selector syntax failed : " + e.getMessage(), e);
				throw new InternalServerError(" validate qt1v1 selector syntax failed. ");

			}

		return res;

	}

	// list rule

	@RequestMapping(method = RequestMethod.GET)
	public Response listRule(Principal principal, 
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "status", defaultValue = "") String statusFilter,
			@RequestParam(value = "languages", defaultValue = "") String languagesFilter,
			@RequestParam(value = "types", defaultValue = "") String typesFilter,
			@RequestParam(value = "start", defaultValue = "") String startLastModifiedFilter,
			@RequestParam(value = "end", defaultValue = "") String endLastModifiedFilter) {
		
		long userId = Long.valueOf(principal.getName());


		logger.info("List Selectors ");

		RuleQueryScript queryscript = new RuleQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;

			switch (toSwitch) {
			case "name":
				queryscript.sortByName = true;
				break;
			case "lastModified":
				queryscript.sortByModified = true;
				break;
			case "created":
				queryscript.sortByCreated = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		}

		// check filter values
		try {

			// statusFilter
			if (!statusFilter.isEmpty()) {
				String[] statusParsed = statusFilter.split(", ");

				HashSet<M_RuleStatus> status = new HashSet<>();

				for (String s : statusParsed) {
					status.add(M_RuleStatus.valueOf(s));
				}

				queryscript.status = status;
			}

			// lastModified
			if (!startLastModifiedFilter.isEmpty())
				queryscript.startTimeModified = Long
						.parseLong(startLastModifiedFilter);
			if (!endLastModifiedFilter.isEmpty())
				queryscript.endTimeModified = Long
						.parseLong(endLastModifiedFilter);

		} catch (IllegalArgumentException iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return selectorService.listRule(queryscript, userId);


	}

	// modify receiver

	@RequestMapping(method = RequestMethod.PUT)
	public Response modifyRule(Principal principal, @RequestBody String json) {
		
		long userId = Long.valueOf(principal.getName());

		RuleUpdateScript script;
		try {
			script = jacksonObjectMapper.readValue(json, RuleUpdateScript.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = selectorService.createResponse(userId, false, "Parameter sind falsch");
			return res;
		}
		if (script == null || script.ruleId <= 0) {
			Response res = selectorService.createResponse(userId, false, "Parameter sind falsch");
			return res;
		}
		logger.info("modify Rule " + script.ruleId);

		Response res = selectorService.modifyRule(script, script.deleteAllMapping, userId);
		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.modified, script.ruleId, M_ObjectType.rule);
				activity.userId = userId;
				activity.content = json;
				activityService.save(activity);
			}

		} catch (M_ActivityDBException e) {
			logger.error("save rule activity failed. " + e.getMessage());
			throw new InternalServerError(" save rule activity failed. ");
		}
		if (res.success && script.ruleType.equals(M_RuleType.QT1V1))
			try {
				BlockConfigMain confMain = ((QT1_V1_RuleContent) script.qt1_V1_RuleContent).blockConfigMain;
				selectorService.validateSelectorSyntax(confMain);
			} catch (Exception e) {
				logger.error("validate qt1v1 selector syntax failed : " + e.getMessage(), e);
				throw new InternalServerError(" validate qt1v1 selector syntax failed. ");
			}

		return res;

	}

	@RequestMapping(value = "/selectors/{id}", method = RequestMethod.PUT)
	public Response normalizeSelector(Principal principal, @PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		try {
			Future<Response> futureResult = selectorService.normalizeSelector(id, userId);

			Response result = futureResult.get(60, TimeUnit.SECONDS);
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new WrongParameterException(" normalize selector failed. ");
		}

	}

	@RequestMapping(value = "/selectors/{id}", method = RequestMethod.GET)
	public Response liveCheckSelector(Principal principal, @PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		try {
			Future<Response> futureResult = selectorService.checkSelectorLive(id, userId);

			Response result = futureResult.get(120, TimeUnit.SECONDS);
			return result;
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = selectorService.createResponse(userId, false, "Parameter sind falsch");
			return res;
		}

	}
	
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(value = "/templates", method = RequestMethod.POST)
	public Response createTemplate(Principal principal, @RequestBody String json) {
		TemplateBean template = new TemplateBean();
		long userId = Long.valueOf(principal.getName());

		try {
			logger.info("selector: \n"+json);
			ObjectNode data = (ObjectNode) jacksonObjectMapper
					.readTree(json);

			String url = data.get("url").asText();
			template.config = data.get("config").toString();
			template.nurl = URLNormalizer.getNormalizedURL(url);
			template.name = data.get("name").asText();
			
			logger.info("selector: name "+template.name+" url "+template.nurl);
			logger.info("selector content:  "+template.config);

		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = selectorService.createResponse(userId, false, "Convert Template Object failed.");
			return res;
		}

		Response res = selectorService.createTemplate(template, userId);
		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.created, template.id, M_ObjectType.rule);
				activity.userId = userId;
				activity.content = json;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("save rule activity failed. " + e.getMessage());
			throw new InternalServerError(" save rule activity failed. ");
		}
//		if (res.success )
//			try {
//				PageBlockConfig conf = jacksonObjectMapper.readValue(template.config, PageBlockConfig.class);
//				selectorService.validatePageBlockConfig(conf);
//
//			} catch (Exception e) {
//				logger.error("validate qt1v1 selector syntax failed : " + e.getMessage(), e);
//				throw new InternalServerError(" validate qt1v1 selector syntax failed. ");
//
//			}

		return res;

	}
	
	@RequestMapping(value = "/templates", method = RequestMethod.GET)
	public Response listTemplates(Principal principal, 
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "filter", defaultValue = "") String statusFilter) {
		
		long userId = Long.valueOf(principal.getName());


		logger.info("List Selectors ");

		RuleTemplateQueryScript queryscript = new RuleTemplateQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;

			switch (toSwitch) {
			case "name":
				queryscript.sortByName = true;
				break;
			case "created":
				queryscript.sortByCreated = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		}

		// check filter values
		try {

			// statusFilter
			if (!statusFilter.isEmpty()) {
				String[] statusParsed = statusFilter.split(", ");

				HashSet<M_RuleStatus> status = new HashSet<>();

				for (String s : statusParsed) {
					status.add(M_RuleStatus.valueOf(s));
				}

				queryscript.status = status;
			}

		} catch (IllegalArgumentException iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return selectorService.listRuleTemplates(queryscript, userId);


	}

}
