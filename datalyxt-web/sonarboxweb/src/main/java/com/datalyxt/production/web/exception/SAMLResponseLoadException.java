package com.datalyxt.production.web.exception;

public class SAMLResponseLoadException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3790354895850372154L;

	public SAMLResponseLoadException(String msg) {
		super(msg);
	}

	public SAMLResponseLoadException(String msg, Exception e) {
		super(msg, e);
	}
}
