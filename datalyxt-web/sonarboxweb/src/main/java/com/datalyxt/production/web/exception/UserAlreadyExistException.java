package com.datalyxt.production.web.exception;

public class UserAlreadyExistException extends RuntimeException  {

	public UserAlreadyExistException(String msg) {
		super(msg);
	}

	public UserAlreadyExistException(String msg, Exception e) {
		super(msg, e);
	}
}
