package com.datalyxt.production.web.dao;

import java.util.List;

import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.web.bean.report.ReportBean;
import com.datalyxt.production.web.bean.report.ReportDetailBean;

public interface TraceReportDAO {

	public List<ReportBean> getReports(String sql, Object[] params) throws TraceReportDBException;

	public List<ReportDetailBean> getReportDetails(String sql, Object[] params)throws TraceReportDBException;
	public List<ReportDetailBean> getReportDetailByReportId(long scanId)throws TraceReportDBException;

}
