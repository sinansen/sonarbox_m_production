package com.datalyxt.production.web.exception;

/**
 * An exception class for when there's a problem handling SAML messages.
 */
public class SamlException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5185302866177211578L;

	public SamlException(String message) {
		super(message);
	}

	public SamlException(String message, Exception e) {
		super(message, e);
	}
}