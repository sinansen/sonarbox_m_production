package com.datalyxt.production.web.bean.email;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.webmodel.email.Attachement;

public class EmailEntity {
	public String senderName;
	public String senderEmail;
	public String receiverEmail;
	public String subject;
	public String text;
	public List<Attachement> attachements = new ArrayList<>();
}
