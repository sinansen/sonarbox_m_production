package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.web.bean.email.EmailQueryScript;

public class EmailQueryBuilder {

	public static QueryWrapper buildQueryForSearch(EmailQueryScript script) {
		String sqlBasePart = " SELECT subject, email_id, receiver_id, m_email_receiver.status,  numretries, time, address, first_name, last_name, e_mail FROM  m_email_receiver left join m_receiver on m_email_receiver.receiver_id = m_receiver.id join m_email_pool on m_email_receiver.email_id = m_email_pool.id";
		
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( first_name like ? or  last_name like  ? or e_mail like ? or subject like ? ) ";
		String conditionWithStatus = " m_email_receiver.status =  ";
		String conditionWithStartTime = " time >= ? ";
		String conditionWithEndTime = " time <= ? ";
		
		String conditionWithPaging = " limit ? offset ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();
		
		boolean hasWhere = false;


		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.status != null && !script.status.isEmpty()) {
			List<String> statusParams = new ArrayList<>();
			for (Integer s : script.status) {
				String param = conditionWithStatus + s;
				statusParams.add(param);
			}
			String parms = String.join(" or ", statusParams);
			parms = " ( " + parms + " ) ";
			if (hasWhere)
				sql = sql + combination + parms;
			else {
				sql = sql + where + parms;
				hasWhere = true;
			}
		}
		
		if (script.endTime > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTime));
			params.add(new Timestamp(script.endTime));
		}

		sql = sql + orderBy;

		if (script.sortByTime)
			sql = sql + " time ";
		else if(script.sortBySubject)
			sql = sql + " subject ";
		else if(script.sortByEmail)
			sql = sql + " e_mail ";
		else if(script.sortByFirstName)
			sql = sql + " first_name ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		if(script.paging != null){
			sql = sql + conditionWithPaging ;
			int limit = script.paging.pages * script.paging.perpage ;
			params.add(limit);
			params.add(script.paging.offset);
		}

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

	


}
