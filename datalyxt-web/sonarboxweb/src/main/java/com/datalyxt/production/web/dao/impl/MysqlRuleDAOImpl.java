package com.datalyxt.production.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.web.bean.selector.SelectorBean;
import com.datalyxt.production.web.bean.selector.TemplateBean;
import com.datalyxt.production.web.bean.source.SourceBasicBean;
import com.datalyxt.production.web.dao.SelectorBeanDAO;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleContent;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.fasterxml.jackson.databind.ObjectMapper;

@Repository("ruleDAO")
public class MysqlRuleDAOImpl implements SelectorBeanDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlRuleDAOImpl.class);

	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource dataSource;
	@Autowired
	ObjectMapper jacksonObjectMapper;

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				jdbcTemplate);
	}

	private List<M_Rule> mapRow(SqlRowSet rows) {
		List<M_Rule> rules = new ArrayList<M_Rule>();

		while (rows.next()) {
			try {
				M_Rule rule = new M_Rule();
				rule.id = rows.getLong("id");
				rule.name = rows.getString("name");
				String contentJson = rows.getString("content");
				rule.ruleContent = jacksonObjectMapper.readValue(contentJson,
						M_RuleContent.class);
				rule.createdAt = rows.getTimestamp("created_at").getTime();
				rule.lastModified = rows.getTimestamp("last_modified")
						.getTime();
				rule.ownerId = rows.getLong("owner_id");
				rule.status = M_RuleStatus.valueOf(rows.getString("status"));
				rule.ruleType = M_RuleType.valueOf(rows.getString("type"));
				rules.add(rule);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

		}
		return rules;
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		return false;
	}

	private PreparedStatementCreator getPreparedStatementCreator(String sql,
			M_Rule rule, String rule_content_json) {

		PreparedStatementCreator creator = new PreparedStatementCreator() {
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				PreparedStatement pst = con.prepareStatement(sql,
						new String[] { "id" });
				int index = 1;
				pst.setString(index++, rule.name);
				pst.setString(index++, rule.ruleContent.contentId);
				pst.setString(index++, rule_content_json);
				pst.setString(index++, rule.status.name());
				pst.setLong(index++, rule.ownerId);
				pst.setString(index++, rule.ruleType.name());
				pst.setTimestamp(index++, new Timestamp(rule.createdAt));
				pst.setTimestamp(index++, new Timestamp(rule.lastModified));
				pst.setString(index++, rule.feedback);
				return pst;
			}
		};

		return creator;

	}

	@Override
	public long addRule(M_Rule rule) throws M_RuleDBException {
		try {

			// insert
			KeyHolder keyHolder = new GeneratedKeyHolder();

			String sql = "INSERT INTO m_rules (name, content_md5, content, status, owner_id, type, created_at, last_modified, feedback)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ? , ?)";

			String rule_content_json = jacksonObjectMapper
					.writeValueAsString(rule.ruleContent);

			PreparedStatementCreator creator = getPreparedStatementCreator(sql,
					rule, rule_content_json);

			jdbcTemplate.update(creator, keyHolder);
			rule.id = (Long) keyHolder.getKey();

			List<Object[]> params = new ArrayList<Object[]>();
			for (Long sourceId : rule.sourceIds) {
				params.add(new Object[] { sourceId, rule.id, rule.ownerId });
			}
			if (!params.isEmpty()) {
				String subsql = "insert ignore into m_rules_source_mapping (source_id, rule_id, owner_id) values (?, ? ,? )";
				jdbcTemplate.batchUpdate(subsql, params);
			}
			return rule.id;
		} catch (Exception e) {
			throw new M_RuleDBException("Error in add Rule: " + e.getMessage(),
					e);
		}

	}

	@Override
	public void changeRuleStatus(long ruleId, M_RuleStatus status,
			long lastModified) throws M_RuleDBException {
		try {

			String sql = "update m_rules set status = ? , last_modified = ?  where id = ?  ";

			Timestamp now = new Timestamp(lastModified);

			jdbcTemplate.update(sql,
					new Object[] { status.name(), now, ruleId });

		} catch (Exception e) {
			throw new M_RuleDBException("Error in changeRuleStatus: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public List<M_Rule> getRules(String sql, Object[] params, long ownerId)
			throws M_RuleDBException {
		try {
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			List<M_Rule> rules = mapRow(rows);
			enrichRules(rules, ownerId);
			return rules;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}

	private void enrichRules(List<M_Rule> rules, long ownerId)
			throws M_RuleDBException {
		try {

			if (rules.isEmpty())
				return;

			HashMap<Long, HashSet<Long>> ruleIds = new HashMap<Long, HashSet<Long>>();

			for (M_Rule rule : rules) {
				ruleIds.put(rule.id, new HashSet<Long>());
			}

			String sql = "select * from m_rules_source_mapping where owner_id = :owner and  rule_id in (:ruleIds) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("owner", ownerId);
			parameters.addValue("ruleIds", ruleIds.keySet());

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				long ruleId = rows.getLong("rule_id");
				long sourceId = rows.getLong("source_id");
				HashSet<Long> sourceIds = ruleIds.get(ruleId);
				if (sourceIds == null)
					sourceIds = new HashSet<Long>();
				sourceIds.add(sourceId);
				ruleIds.put(ruleId, sourceIds);
			}

			for (M_Rule rule : rules) {
				HashSet<Long> sourceIds = ruleIds.get(rule.id);
				if (sourceIds != null)
					rule.sourceIds.addAll(sourceIds);
			}

		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}

	}

	@Override
	public boolean isRuleExsits(M_Rule rule) throws M_RuleDBException {
		try {
			String sql = "select id from m_rules where content_md5 = ? and owner_id = ?";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					rule.ruleContent.contentId, rule.ownerId);
			if (rows.next())
				return true;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
		return false;
	}

	@Override
	public M_Rule getRuleByRuleId(Long ruleId, Long ownerId)
			throws M_RuleDBException {
		try {
			String sql = "select * from m_rules where id = ? and owner_id = ? ";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, ruleId, ownerId);
			List<M_Rule> rules = mapRow(rows);
			enrichRules(rules, ownerId);

			M_Rule result = null;
			if (!rules.isEmpty()) {

				result = rules.get(0);

			}
			return result;

		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}

	@Override
	public int modifyRule(String sql, Object[] params) throws M_RuleDBException {
		try {
			int changed = jdbcTemplate.update(sql, params);
			return changed;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}

	}

	@Override
	public int[] modifyRuleSourceMapping(M_Rule rule) throws M_RuleDBException {
		try {
			String sql = "insert ignore m_rules_source_mapping set source_id = ? ,  rule_id = ? ,  owner_id = ? ";

			ArrayList<Object[]> params = new ArrayList<>();

			for (Long sourceId : rule.sourceIds) {
				params.add(new Object[] { sourceId, rule.id, rule.ownerId });
			}

			int[] changed = jdbcTemplate.batchUpdate(sql, params);
			return changed;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}

	@Override
	public int[] deleteRuleSourceMapping(Collection<Long> tobeDeletedSourceIds,
			long ownerId, long ruleId) throws M_RuleDBException {
		try {

			String sql = "delete from m_rules_source_mapping where source_id = ? and rule_id = ? and owner_id = ? ";

			ArrayList<Object[]> params = new ArrayList<>();

			for (Long sourceId : tobeDeletedSourceIds) {
				params.add(new Object[] { sourceId, ruleId, ownerId });
			}

			int[] changed = jdbcTemplate.batchUpdate(sql, params);
			return changed;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}

	@Override
	public void updateSelector(long ruleId, BlockConfigMain cfg, long ownerId)
			throws M_RuleDBException {
		try {
			QT1_V1_RuleContent content = new QT1_V1_RuleContent();
			String cfgJson = jacksonObjectMapper.writeValueAsString(cfg);
			String contentId = DigestUtils.md5Hex(cfgJson);
			content.contentId = contentId;
			content.blockConfigMain = cfg;
			String ruleContentJson = jacksonObjectMapper
					.writeValueAsString(content);
			String md5 = DigestUtils.md5Hex(ruleContentJson);
			Timestamp now = new Timestamp(System.currentTimeMillis());
			String sql = "update  m_rules set content_md5 = ? , content =  ? , last_modified = ? where id = ? and owner_id = ?";
			jdbcTemplate.update(sql, new Object[] { md5, ruleContentJson, now,
					ruleId, ownerId });
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}

	}

	@Override
	public HashMap<Long, M_Rule> getRules(HashSet<Long> rids)
			throws M_RuleDBException {
		try {
			HashMap<Long, M_Rule> rules = new HashMap<Long, M_Rule>();
			if (rids == null | rids.isEmpty())
				return rules;

			String sql = "select id, name from m_rules where id in (:ruleIds) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("ruleIds", rids);

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				M_Rule rule = new M_Rule();
				rule.id = rows.getLong("id");
				rule.name = rows.getString("name");
				rules.put(rule.id, rule);
			}
			return rules;

		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}

	@Override
	public List<SelectorBean> getSelectorBeans(String sql, Object[] params,
			long userId) throws M_RuleDBException {
		try {
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			HashMap<Long, List<SourceBasicBean>> relatedSources = new HashMap<>();
			List<SelectorBean> beans = new ArrayList<>();
			while (rows.next()) {
				SelectorBean selectorBean = new SelectorBean();
				selectorBean.id = rows.getLong("rule_id");
				selectorBean.name = rows.getString("rule_name");

				selectorBean.content = rows.getString("content");
				
				selectorBean.createdAt = rows.getTimestamp("created_at").getTime();
				selectorBean.lastModified = rows.getTimestamp("last_modified")
						.getTime();
				selectorBean.status = M_RuleStatus.valueOf(rows.getString("status"));
				selectorBean.type = M_RuleType.valueOf(rows.getString("type"));
				
				SourceBasicBean sourceBasicBean = new SourceBasicBean();
				sourceBasicBean.id = rows.getLong("source_id");
				sourceBasicBean.name = rows.getString("source_name");
				sourceBasicBean.url = rows.getString("url");
				
				List<SourceBasicBean> list = relatedSources.get(selectorBean.id);
				
				if(list != null){
					list.add(sourceBasicBean);
				}else{
					list = new ArrayList<>();
					list.add(sourceBasicBean);
					beans.add(selectorBean);
				}
				relatedSources.put(selectorBean.id, list);
				
			}
			for(SelectorBean b : beans)
				if( relatedSources.containsKey(b.id))
					b.sources = relatedSources.get(b.id);
			
			return beans;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}
	@Override
	public long addTemplate(TemplateBean template) throws M_RuleDBException {
		try {

			// insert
			KeyHolder keyHolder = new GeneratedKeyHolder();

			String sql = "INSERT INTO m_scraper_template (nurl, md5, content, status, owner_id, created_at, last_modified)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ?)";

			PreparedStatementCreator creator = new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement pst = con.prepareStatement(sql,
							new String[] { "id" });
					int index = 1;
					pst.setString(index++, template.nurl);
					pst.setString(index++, template.md5);
					pst.setString(index++, template.config);
					pst.setString(index++, template.status.name());
					pst.setLong(index++, template.ownerId);
					pst.setTimestamp(index++, new Timestamp(template.createdAt));
					pst.setTimestamp(index++, new Timestamp(template.lastModified));
					return pst;
				}
			};
			jdbcTemplate.update(creator, keyHolder);
			template.id = (Long) keyHolder.getKey();

			return template.id;
		} catch (Exception e) {
			throw new M_RuleDBException("Error in add Template: " + e.getMessage(),
					e);
		}

	}

	@Override
	public List<TemplateBean> getSelectorTemplateBeans(String sql,
			Object[] params, long userId) throws M_RuleDBException {
		try {
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			List<TemplateBean> beans = new ArrayList<>();
			while (rows.next()) {
				TemplateBean selectorBean = new TemplateBean();
				selectorBean.id = rows.getLong("id");
				selectorBean.name = rows.getString("name");

				selectorBean.config = rows.getString("content");
				
				selectorBean.createdAt = rows.getTimestamp("created_at").getTime();
				selectorBean.lastModified = rows.getTimestamp("last_modified")
						.getTime();
				selectorBean.status = M_RuleStatus.valueOf(rows.getString("status"));
				
				selectorBean.nurl = rows.getString("nurl");
				
				beans.add(selectorBean);
				
			}
			return beans;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		}
	}
}
