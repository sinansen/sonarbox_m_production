package com.datalyxt.production.web.service;

import java.io.StringWriter;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import com.datalyxt.production.web.bean.AuthResponse;
import com.datalyxt.production.web.exception.AuthenticationException;
import com.datalyxt.production.web.exception.SAMLResponseLoadException;
import com.datalyxt.production.web.saml.Response;
import com.datalyxt.production.web.saml.util.KeyStoreLoaderUtil;

@Service
public class ConsumeSamlResponseService {

	@Value("${keystore.path}")
	private String jksPath;

	@Value("${saml.keystore.pwd}")
	private String keystorePassword;

	@Value("${saml.keystore.privatepwd}")
	private String KeystorePrivatePassword;

	KeyStoreLoaderUtil keyStoreLoaderUtil;

	public AuthResponse consume(String SAMLResponse) {
		if (keyStoreLoaderUtil == null)
			keyStoreLoaderUtil = new KeyStoreLoaderUtil(jksPath,
					keystorePassword, KeystorePrivatePassword);

		Response samlResponse;
		samlResponse = new Response(keyStoreLoaderUtil.getRSAPublicKey());

		try {
			samlResponse.loadXmlFromBase64(SAMLResponse);
		} catch (Exception e) {
			throw new SAMLResponseLoadException("can't parse saml response");
		}
		try {
			if (samlResponse.isValid() && !samlResponse.isTimedOut()) {

				// the signature of the SAML Response is valid and the SAML
				// Response isn't timed out.
				AuthResponse authResponse  = samlResponse.getNameId();
				
				return authResponse;

			} else {

				throw new AuthenticationException("Invalid saml response!");
			}
		} catch (Exception e) {
			throw new SAMLResponseLoadException("check saml response failed!");
		}
	}

	private String getStringFromDoc(Document doc) {

		try {
			DOMSource domSource = new DOMSource(doc);
			Transformer transformer = TransformerFactory.newInstance()
					.newTransformer();
			StringWriter sw = new StringWriter();
			StreamResult sr = new StreamResult(sw);
			transformer.transform(domSource, sr);
			return (sw.toString());
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "no doc converted";
		
	}

}
