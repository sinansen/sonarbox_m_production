package com.datalyxt.production.web.dao;

import com.datalyxt.production.exception.db.M_UserDBException;
import com.datalyxt.production.web.bean.AuthResponse;
import com.datalyxt.production.web.bean.UserBean;

public interface UserDAO {

	public UserBean saveUser(AuthResponse authResponse) throws M_UserDBException;

	public UserBean findByUserName(String userName) throws M_UserDBException;

}
