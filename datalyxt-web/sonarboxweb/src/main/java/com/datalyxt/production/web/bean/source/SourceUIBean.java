package com.datalyxt.production.web.bean.source;

import java.util.HashSet;

import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;

public class SourceUIBean {


	public long id;
	public String url;
	public long createdAt;
	public long lastModified;
	public String feedback;
	public HashSet<M_SourceType> types = new HashSet<>();
	public HashSet<LanguageISO_639_1> languages = new HashSet<>();
	public HashSet<String> dynamicParams = new HashSet<>();
	public M_SourceStatus status;
	public String name;
}
