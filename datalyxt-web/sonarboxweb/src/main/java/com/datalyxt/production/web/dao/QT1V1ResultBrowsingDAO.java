package com.datalyxt.production.web.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.web.bean.email.EmailDetail;
import com.datalyxt.production.web.bean.result.ScraperResultBean;
import com.datalyxt.production.webdatabase.dao.DataBaseDAO;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;

public interface QT1V1ResultBrowsingDAO extends DataBaseDAO {

	public List<ScraperResultBean> getTextResults(String sql, Object[] params)
			throws M_QT1V1ResultDBException;

	public List<ScraperResultBean> getFileResults(String sql, Object[] params)
			throws M_QT1V1ResultDBException;

	public void updateValidation(long id, int code, boolean isFile,
			boolean isSystem) throws M_QT1V1ResultDBException;

	public void setActionResultFiles(EmailDetail emailDetail)
			throws M_QT1V1ResultDBException;

	public void setActionResultTexts(EmailDetail emailDetail)
			throws M_QT1V1ResultDBException;

	public ActionResultFile getFileResult(long id)
			throws M_QT1V1ResultDBException;

}
