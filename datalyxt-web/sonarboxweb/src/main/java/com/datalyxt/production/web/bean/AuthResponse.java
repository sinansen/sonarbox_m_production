package com.datalyxt.production.web.bean;

public class AuthResponse {

	public String userName;
	public String inResponseTo;
	public String email;
	public String language = "de";
	public int role = 1;

}
