package com.datalyxt.production.web.configuration;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableAsync;

import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.impl.M_FileStorageDAOImpl;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

@Configuration
@ComponentScan(basePackages = "com.datalyxt.production.web")
@EnableAsync
public class AppConfig {

	@Autowired
	private Environment env;

	@Bean
	public DataSource getDataSource() throws SQLException {

		MysqlDataSource mysqlDS = null;
		mysqlDS = new MysqlDataSource();
		mysqlDS.setUrl(env.getProperty("MYSQL_DB_URL"));
		mysqlDS.setUser(env.getProperty("MYSQL_DB_USERNAME"));
		mysqlDS.setPassword(env.getProperty("MYSQL_DB_PASSWORD"));
		return mysqlDS;

	}

	@Bean
	public M_FileStorageDAO getM_FileStorageDAO() {
		Properties fsProperties = new Properties();
		fsProperties.setProperty("ssh_port", env.getProperty("ssh_port"));
		fsProperties.setProperty("sftp_directory",
				env.getProperty("sftp_directory"));
		fsProperties.setProperty("sftp_host", env.getProperty("sftp_host"));
		fsProperties.setProperty("sftp_username",
				env.getProperty("sftp_username"));
		fsProperties.setProperty("sftp_password",
				env.getProperty("sftp_password"));
		fsProperties.setProperty("use_sftp", env.getProperty("use_sftp"));
		fsProperties.setProperty("fs_host", env.getProperty("fs_host"));
		fsProperties.setProperty("fs_path", env.getProperty("fs_path"));

		M_FileStorageDAO fileStorage = new M_FileStorageDAOImpl(fsProperties);
		return fileStorage;
	}

	@Bean
	public JavaMailSender getJavaMailSender() {
		JavaMailSenderImpl sender = new JavaMailSenderImpl();
		sender.setHost(env.getProperty("email.host"));
		sender.setPort(Integer.valueOf(env.getProperty("email.port")));
		boolean auth = Boolean.valueOf(env.getProperty("email.auth"));
		if (auth) {
			sender.setUsername(env.getProperty("email.username"));
			sender.setPassword(env.getProperty("email.password"));
		}
		return sender;
	}

}