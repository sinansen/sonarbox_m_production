package com.datalyxt.production.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_ReceiverDBException;
import com.datalyxt.production.exception.db.M_ReceiverGroupDBException;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverGroup;
import com.datalyxt.production.receiver.M_ReceiverGroupStatus;
import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.webdatabase.dao.M_ReceiverDAO;

@Repository("receiverDAO")
public class MysqlReceiverDAOImpl implements M_ReceiverDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlReceiverDAOImpl.class);

	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource dataSource;

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				jdbcTemplate);
	}

	@Override
	public long insertReceiver(M_Receiver receiver)
			throws M_ReceiverDBException {
		try {

			// insert
			KeyHolder keyHolder = new GeneratedKeyHolder();

			String sql = "INSERT INTO m_receiver (md5, first_name, last_name, e_mail, company, status, created_at, last_modified, owner_id, address, function, department, category)"
					+ " VALUES (?, ?, ?, ?, ?, ?, ? , ?, ?, ? ,? ,? , ?)";
			String md5Id = DigestUtils.md5Hex(receiver.email);

			// jdbcTemplate.update(sql, new Object[] { md5Id,
			// receiver.firstName,
			// receiver.lastName, receiver.eMail, receiver.company,
			// receiver.status.name(), new Timestamp(receiver.createdAt),
			// new Timestamp(receiver.lastModified) });

			jdbcTemplate.update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement pst = con.prepareStatement(sql,
							new String[] { "id" });
					int index = 1;
					pst.setString(index++, md5Id);
					pst.setString(index++, receiver.firstName);
					pst.setString(index++, receiver.lastName);
					pst.setString(index++, receiver.email);
					pst.setString(index++, receiver.company);
					pst.setString(index++, receiver.status.name());
					pst.setTimestamp(index++, new Timestamp(receiver.createdAt));
					pst.setTimestamp(index++, new Timestamp(
							receiver.lastModified));
					pst.setLong(index++, receiver.ownerId);
					pst.setString(index++, receiver.address);
					pst.setString(index++, receiver.function);
					pst.setString(index++, receiver.department);
					pst.setString(index++, receiver.category);
					return pst;
				}
			}, keyHolder);
			receiver.id = (Long) keyHolder.getKey();

			// receiver.receiverId = (Long) jdbcTemplate
			// .queryForObject(
			// "select receiver_id from m_receiver where receiver_md5 = ? ",
			// new Object[] { md5Id }, Long.class);
			return receiver.id;
		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in insertReceiver: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public void modifyReceiver(String sql, Object[] params)
			throws M_ReceiverDBException {

		try {

			jdbcTemplate.update(sql, params);

		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in modifyReceiver: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public M_Receiver getReceiverByEmail(String email)
			throws M_ReceiverDBException {

		try {
			if (email == null)
				return null;
			String md5 = DigestUtils.md5Hex(email);
			String sql = "select * from m_receiver where md5 = ? ";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, md5);
			List<M_Receiver> receivers = mapRow(rows);
			if (!receivers.isEmpty())
				return receivers.get(0);

		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in modifyReceiver: "
					+ e.getMessage(), e);
		}
		return null;

	}

	private void enrichReceivers(List<M_Receiver> receivers)
			throws M_ReceiverDBException {
		try {

			if (receivers.isEmpty())
				return;

			HashMap<Long, HashSet<Long>> receiverIds = new HashMap<Long, HashSet<Long>>();

			for (M_Receiver receiver : receivers) {
				receiverIds.put(receiver.id, new HashSet<Long>());
			}

			String sql = "select * from m_receiver_group_mapping where  receiver_id in (:receiverIds) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("receiverIds", receiverIds.keySet());

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				long receiverId = rows.getLong("receiver_id");
				long groupId = rows.getLong("group_id");
				HashSet<Long> groupIds = receiverIds.get(receiverId);
				if (groupIds == null)
					groupIds = new HashSet<Long>();
				groupIds.add(groupId);
				receiverIds.put(receiverId, groupIds);
			}

			for (M_Receiver receiver : receivers) {
				HashSet<Long> groupIds = receiverIds.get(receiver.id);
				if (groupIds != null)
					receiver.groups.addAll(groupIds);
			}

		} catch (Exception e) {
			throw new M_ReceiverDBException(e.getMessage(), e);
		}

	}

	@Override
	public boolean isReceiverExist(Long receiverId)
			throws M_ReceiverDBException {
		try {
			String sql = "select receiver_id from m_receiver where id = ? limit 1";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { receiverId });
			if (rows.next()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in isReceiverExist: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public List<M_Receiver> getReceiver(String sql, Object[] params)
			throws M_ReceiverDBException {
		List<M_Receiver> receivers = new ArrayList<M_Receiver>();

		try {

			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			receivers = mapRow(rows);
			enrichReceivers(receivers);
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in getReceiver: "
					+ e.getMessage(), e);
		}
		return receivers;
	}

	private List<M_Receiver> mapRow(SqlRowSet rows) {
		List<M_Receiver> receiver = new ArrayList<M_Receiver>();

		while (rows.next()) {
			try {
				M_Receiver rec = new M_Receiver();
				rec.id = rows.getLong("id");
				rec.firstName = rows.getString("first_name");
				rec.lastName = rows.getString("last_name");
				rec.email = rows.getString("e_mail");
				rec.company = rows.getString("company");
				rec.address = rows.getString("address");
				rec.function = rows.getString("function");
				rec.department = rows.getString("department");
				rec.category = rows.getString("category");
				rec.createdAt = rows.getTimestamp("created_at").getTime();
				rec.lastModified = rows.getTimestamp("last_modified").getTime();

				rec.status = M_ReceiverStatus.valueOf(rows.getString("status"));
				receiver.add(rec);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

		}
		return receiver;
	}

	@Override
	public M_Receiver getReceiverById(long receiverId, long ownerId)
			throws M_ReceiverDBException {

		try {
			String sql = "SELECT * FROM m_receiver  WHERE id = ? AND owner_id = ?";

			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, receiverId,
					ownerId);
			M_Receiver rec = null;
			HashSet<Long> groupIds = new HashSet<>();
			while (rows.next()) {
				rec = new M_Receiver();
				rec.id = rows.getLong("id");
				rec.firstName = rows.getString("first_name");
				rec.lastName = rows.getString("last_name");
				rec.email = rows.getString("e_mail");
				rec.company = rows.getString("company");
				rec.address = rows.getString("address");
				rec.function = rows.getString("function");
				rec.department = rows.getString("department");
				rec.category = rows.getString("category");
				rec.createdAt = rows.getTimestamp("created_at").getTime();
				rec.lastModified = rows.getTimestamp("last_modified").getTime();

				rec.status = M_ReceiverStatus.valueOf(rows.getString("status"));
			}

			sql = "SELECT * FROM m_receiver_group_mapping  WHERE receiver_id = ? AND owner_id = ?";

			rows = jdbcTemplate.queryForRowSet(sql, receiverId, ownerId);
			while (rows.next()) {
				groupIds.add(rows.getLong("group_id"));
			}
			if (rec != null)
				rec.groups = groupIds;
			return rec;
		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in getReceiverById: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public boolean isGroupNameExist(String groupName, long ownerId)
			throws M_ReceiverGroupDBException {
		try {

			String sql = "select id from m_receiver_group where name = ? and owner_id = ?";
			// create the mysql insert preparedstatement

			SqlRowSet resultSet = jdbcTemplate.queryForRowSet(sql, groupName,
					ownerId);
			if (resultSet.next())
				return true;
			return false;
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(e.getMessage(), e);

		}
	}

	@Override
	public long insertReceiverGroup(M_ReceiverGroup receiverGroup)
			throws M_ReceiverGroupDBException {
		try {
			String sql = "INSERT INTO m_receiver_group (name, description, owner_id, status, created_at, last_modified)"
					+ " VALUES (?, ?, ?, ?, ?, ?)";

			KeyHolder keyHolder = new GeneratedKeyHolder();

			jdbcTemplate.update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement pst = con.prepareStatement(sql,
							new String[] { "id" });
					int index = 1;
					pst.setString(index++, receiverGroup.name);
					pst.setString(index++, receiverGroup.description);
					pst.setLong(index++, receiverGroup.owner);
					pst.setString(index++, receiverGroup.status.name());
					pst.setTimestamp(index++, new Timestamp(
							receiverGroup.createdAt));
					pst.setTimestamp(index++, new Timestamp(
							receiverGroup.lastModified));
					return pst;
				}
			}, keyHolder);

			receiverGroup.id = (Long) keyHolder.getKey();
			return receiverGroup.id;
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in insertReceiverGroup: " + e.getMessage(), e);
		}
	}

	@Override
	public void modifyReceiverGroup(String sql, Object[] params)
			throws M_ReceiverGroupDBException {
		try {
			jdbcTemplate.update(sql, params);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in modifyReceiverGroup: " + e.getMessage(), e);
		}
	}

	@Override
	public M_ReceiverGroup getReceiverGroupById(long groupId, long ownerId)
			throws M_ReceiverGroupDBException {

		try {
			String sql = "SELECT * FROM m_receiver_group WHERE id = ? AND owner_id = ?";

			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, groupId, ownerId);
			M_ReceiverGroup group = null;
			HashSet<Long> member = new HashSet<Long>();
			while (rows.next()) {
				group = new M_ReceiverGroup();
				group.id = rows.getLong("id");
				group.name = rows.getString("name");
				group.description = rows.getString("description");
				group.createdAt = rows.getTimestamp("created_at").getTime();
				group.lastModified = rows.getTimestamp("last_modified")
						.getTime();

				group.status = M_ReceiverGroupStatus.valueOf(rows
						.getString("status"));
			}

			sql = "SELECT * FROM m_receiver_group_mapping  WHERE group_id = ? AND owner_id = ?";

			rows = jdbcTemplate.queryForRowSet(sql, groupId, ownerId);
			while (rows.next())
				member.add(rows.getLong("receiver_id"));

			if (group != null)
				group.member = member;

			return group;
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException("Error in getReceiverGroups: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public List<M_ReceiverGroup> getReceiverGroups(String sql, Object[] params)
			throws M_ReceiverGroupDBException {
		List<M_ReceiverGroup> groups = new ArrayList<M_ReceiverGroup>();

		try {

			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			groups = mapReceiverGroupRow(rows);
			enrichGroups(groups);
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException("Error in getReceiverGroups: "
					+ e.getMessage(), e);
		}
		return groups;
	}

	private void enrichGroups(List<M_ReceiverGroup> groups)
			throws M_ReceiverDBException {
		try {

			if (groups.isEmpty())
				return;

			HashMap<Long, HashSet<Long>> memberIds = new HashMap<Long, HashSet<Long>>();

			for (M_ReceiverGroup group : groups) {
				memberIds.put(group.id, new HashSet<Long>());
			}

			String sql = "select * from m_receiver_group_mapping where group_id in (:memberIds) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("memberIds", memberIds.keySet());

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				long receiverId = rows.getLong("receiver_id");
				long groupId = rows.getLong("group_id");
				HashSet<Long> receiverIds = memberIds.get(groupId);
				if (receiverIds == null)
					receiverIds = new HashSet<Long>();
				receiverIds.add(receiverId);
				memberIds.put(groupId, receiverIds);
			}

			for (M_ReceiverGroup group : groups) {
				HashSet<Long> members = memberIds.get(group.id);
				if (members != null)
					group.member = members;
			}

		} catch (Exception e) {
			throw new M_ReceiverDBException(e.getMessage(), e);
		}

	}

	@Override
	public int[] removeReceiverFromGroup(long groupId,
			HashSet<Long> groupMember, long ownerId)
			throws M_ReceiverGroupDBException {
		try {
			String sql = "delete from m_receiver_group_mapping where group_id = ? and receiver_id = ? and owner_id = ? ";

			List<Object[]> parameters = new ArrayList<Object[]>();

			for (Long receiverId : groupMember) {
				parameters.add(new Object[] { groupId, receiverId, ownerId });
			}
			int[] changedRows = jdbcTemplate.batchUpdate(sql, parameters);
			return changedRows;
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in removeReceiverFromGroup: " + e.getMessage(), e);
		}

	}

	@Override
	public int[] removeReceiverFromGroup(HashSet<Long> groupIds,
			Long groupMember, long ownerId) throws M_ReceiverGroupDBException {
		try {
			String sql = "delete from m_receiver_group_mapping where group_id = ? and receiver_id = ? and owner_id = ? ";

			List<Object[]> parameters = new ArrayList<Object[]>();

			for (Long groupId : groupIds) {
				parameters.add(new Object[] { groupId, groupMember, ownerId });
			}
			int[] changedRows = jdbcTemplate.batchUpdate(sql, parameters);
			return changedRows;
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in removeReceiverFromGroup: " + e.getMessage(), e);
		}

	}

	@Override
	public int[] addReceiverToGroup(long groupId, HashSet<Long> groupMember,
			long ownerId) throws M_ReceiverGroupDBException {
		try {
			String sql = "insert IGNORE into m_receiver_group_mapping (group_id, receiver_id, owner_id)"
					+ " VALUES (? , ?, ?) ";

			List<Object[]> parameters = new ArrayList<Object[]>();

			for (Long receiverId : groupMember) {
				parameters.add(new Object[] { groupId, receiverId, ownerId });
			}
			int[] changedRows = jdbcTemplate.batchUpdate(sql, parameters);

			return changedRows;
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in getReceiverByGroupId: " + e.getMessage(), e);
		}

	}

	@Override
	public List<M_Receiver> getReceiverByGroupId(long groupId)
			throws M_ReceiverGroupDBException {

		List<M_Receiver> receivers = new ArrayList<M_Receiver>();

		try {
			String sql = "select * from m_receiver where id in ( select receiver_id from m_receiver_group_mapping where group_id = ? )";
			SqlRowSet resultSet = jdbcTemplate.queryForRowSet(sql, groupId);

			receivers = mapRow(resultSet);
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in getReceiverByGroupId: " + e.getMessage(), e);
		}
		return receivers;
	}

	@Override
	public List<M_ReceiverGroup> getReceiverGroups(long receiverId)
			throws M_ReceiverGroupDBException {

		List<M_ReceiverGroup> groups = new ArrayList<M_ReceiverGroup>();

		try {
			String sql = "select * from m_receiver_group where id in ( select group_id from m_receiver_group_mapping where receiver_id = ? )";
			SqlRowSet resultSet = jdbcTemplate.queryForRowSet(sql, receiverId);

			groups = mapReceiverGroupRow(resultSet);
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException("Error in getReceivrGroups: "
					+ e.getMessage(), e);
		}
		return groups;
	}

	private List<M_ReceiverGroup> mapReceiverGroupRow(SqlRowSet rows) {
		List<M_ReceiverGroup> receiverGroup = new ArrayList<M_ReceiverGroup>();

		while (rows.next()) {
			try {
				M_ReceiverGroup rec = new M_ReceiverGroup();
				rec.id = rows.getLong("id");
				rec.name = rows.getString("name");
				rec.description = rows.getString("description");
				rec.createdAt = rows.getTimestamp("created_at").getTime();
				rec.lastModified = rows.getTimestamp("last_modified").getTime();

				rec.status = M_ReceiverGroupStatus.valueOf(rows
						.getString("status"));
				receiverGroup.add(rec);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

		}
		return receiverGroup;
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<M_Receiver> getReciverByStatus(M_ReceiverStatus arg0)
			throws M_ReceiverDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeReceiverFromAllGroup(long receiverId, long userId)
			throws M_ReceiverDBException {
		try {
			String sql = "delete from m_receiver_group_mapping where owner_id = ? and receiver_id = ? ";

			jdbcTemplate.update(sql, userId, receiverId);
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverDBException(
					"Error in removeReceiverFromAllGroup: " + e.getMessage(), e);
		}

	}

	@Override
	public boolean isGroupOwner(long groupId, long ownerId)
			throws M_ReceiverDBException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int removeGroup(long groupId, long ownerId)
			throws M_ReceiverGroupDBException {
		try {
			String sql = "delete from m_receiver_group where owner_id = ? and id = ? ";

			int result = jdbcTemplate.update(sql, ownerId, groupId);
			return result;
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException("Error in remove group: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public void changeReceiverStatus(long id, M_ReceiverStatus status,
			long ownerId) throws M_ReceiverDBException {
		try {
			String sql = "update m_receiver set status = ?  where owner_id = ? and id = ? ";

			jdbcTemplate.update(sql, status.name(), ownerId, id);
		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in update receiver status "
					+ e.getMessage(), e);
		}

	}

	@Override
	public void changeGroupStatus(long id, M_ReceiverGroupStatus status,
			long ownerId) throws M_ReceiverGroupDBException {
		try {
			String sql = "update m_receiver_group set status = ?  where owner_id = ? and id = ? ";

			jdbcTemplate.update(sql, status.name(), ownerId, id);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in update group status " + e.getMessage(), e);
		}

	}

	@Override
	public void removeReceiver(long receiverId, long ownerId)
			throws M_ReceiverDBException {
		try {
			String sql = "delete from m_receiver where id = ? and owner_id = ? ";

			jdbcTemplate.update(sql, receiverId, ownerId);
		} catch (Exception e) {
			throw new M_ReceiverDBException("Error in remove receiver "
					+ e.getMessage(), e);
		}

	}

	@Override
	public int[] addReceiverToGroup(HashSet<Long> groupIds, long groupMember,
			long ownerId) throws M_ReceiverGroupDBException {
		try {
			String sql = "insert IGNORE into m_receiver_group_mapping (group_id, receiver_id, owner_id)"
					+ " VALUES (? , ?, ?) ";

			List<Object[]> parameters = new ArrayList<Object[]>();

			for (Long groupId : groupIds) {
				parameters.add(new Object[] { groupId, groupMember, ownerId });
			}
			int[] changedRows = jdbcTemplate.batchUpdate(sql, parameters);

			return changedRows;
			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(
					"Error in getReceiverByGroupId: " + e.getMessage(), e);
		}
	}

	@Override
	public HashMap<Long, M_ReceiverGroup> getReceiverGroups(
			HashSet<Long> groupIds) throws M_ReceiverGroupDBException {
		HashMap<Long, M_ReceiverGroup> groups = new HashMap<Long, M_ReceiverGroup>();
		try {

			if (groupIds == null || groupIds.isEmpty())
				return groups;

			String sql = "select id, name from m_receiver_group where id in (:groupIds) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("groupIds", groupIds);

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				M_ReceiverGroup g = new M_ReceiverGroup();
				g.id = rows.getLong("id");
				g.name = rows.getString("name");

				groups.put(g.id, g);
			}

		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(e.getMessage(), e);
		}
		return groups;
	}

	@Override
	public HashSet<Long> getReceiverByGroupIds(HashSet<Long> groupIds,
			long ownerId) throws M_ReceiverGroupDBException {
		HashSet<Long> receiverIds = new HashSet<>();
		try {

			if (groupIds == null || groupIds.isEmpty())
				return receiverIds;

			String sql = "select receiver_id from m_receiver_group_mapping where group_id in (:groupIds) and owner_id = :owner ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("groupIds", groupIds);
			parameters.addValue("owner", ownerId);

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				long id = rows.getLong("receiver_id");
				receiverIds.add(id);
			}
			return receiverIds;
		} catch (Exception e) {
			throw new M_ReceiverGroupDBException(e.getMessage(), e);
		}
	}
}
