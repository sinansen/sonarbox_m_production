package com.datalyxt.production.web.bean.email;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.webmodel.email.EmailAction;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

public class EmailDetail {
	public long emailId;
	public List<EmailAction> actions = new ArrayList<EmailAction>();
	public HashMap<Long, M_Receiver> receivers = new HashMap<>();
	public HashSet<Long> messageIds = new HashSet<>();
	public HashSet<Long> attachments = new HashSet<>();
	public List<ActionResultFile> fileMeta= new ArrayList<>();
	public HashSet<String> domains = new HashSet<>();
	public List<ActionResultText> actionResults= new ArrayList<>();
	public HashSet<String> pageURLs = new HashSet<>();
	public String subject;
}
