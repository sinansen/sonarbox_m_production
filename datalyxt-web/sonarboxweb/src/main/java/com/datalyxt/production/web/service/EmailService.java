package com.datalyxt.production.web.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.EmailDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.email.EmailActionBean;
import com.datalyxt.production.web.bean.email.EmailDetail;
import com.datalyxt.production.web.bean.email.EmailDetailBean;
import com.datalyxt.production.web.bean.email.EmailEntity;
import com.datalyxt.production.web.bean.email.EmailQueryScript;
import com.datalyxt.production.web.bean.email.EmailReceiverBean;
import com.datalyxt.production.web.bean.result.DataRowItemBean;
import com.datalyxt.production.web.dao.EmailBeanDAO;
import com.datalyxt.production.web.dao.QT1V1ResultBrowsingDAO;
import com.datalyxt.production.web.dao.helper.EmailQueryBuilder;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.web.service.util.MailContentGenerator;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.M_ReceiverDAO;
import com.datalyxt.production.webmodel.email.Attachement;
import com.datalyxt.production.webmodel.email.Email;
import com.datalyxt.production.webmodel.email.EmailAction;
import com.datalyxt.production.webmodel.email.EmailStatus;
import com.datalyxt.production.webscraper.model.action.ActionMetaConstant;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Service
public class EmailService {
	private static Logger logger = LoggerFactory.getLogger(EmailService.class);

	@Autowired
	QT1V1ResultBrowsingDAO resultDAO;

	@Autowired
	M_ReceiverDAO receiverDAO;

	@Autowired
	M_FileStorageDAO fileStorage;

	@Autowired
	EmailBeanDAO emailDAO;

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Value("${email.sendername}")
	private String senderName;

	@Value("${email.from}")
	private String senderEmail;

	@Value("${email.max_retry}")
	private int MAX_NUM_OF_RETRIES;

	public Response createEmail(Email email, HashSet<Long> groupIds,
			HashSet<Long> receiverIds, long userId) {
		try {
			Response res = checkEmailInput(email, userId);
			if (!res.success) {
				return res;
			}

			if (emailDAO.hasSameEmail(email, userId)) {
				res = ResponseUtil.createResponse(userId, false,
						"Email existiert bereits");
				return res;
			}
			HashSet<Long> members = receiverDAO.getReceiverByGroupIds(groupIds,
					userId);
			receiverIds.addAll(members);
			emailDAO.createEmail(email, receiverIds, userId);

			res = ResponseUtil
					.createResponse(userId, true, "Email hinzugefügt");

			if (!receiverIds.isEmpty())
				return mailtoAllRelatedReceiver(email.id, userId);

			return res;
		} catch (Exception e) {
			logger.error(
					"exception in creating email in db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Speichern");
			return res;
		}
	}

	private Response checkEmailInput(Email email, long userId) {
		if (email == null)
			return ResponseUtil.createResponse(userId, false,
					"Email Object ist null.");

		if (email.subject != null && email.subject.length() > 200) {

			return ResponseUtil.createResponse(userId, false,
					"Subject ist zu lang.");

		}

		if (email.messageIds.isEmpty() && email.attachementFileIds.isEmpty()) {
			return ResponseUtil.createResponse(userId, false,
					"Keine Messages oder Anhang in Email definiert.");
		}

		return ResponseUtil.createResponse(userId, true, "check ist durch");
	}

	public Response modifyEmail(Email email, long userId) {
		try {
			Response res = checkEmailInput(email, userId);

			if (!res.success)
				return res;

			Email oldEmail = emailDAO.getEmailById(email.id, userId);

			if (oldEmail == null) {
				res = ResponseUtil.createResponse(userId, false,
						"Email existiert nicht");
				return res;
			}

			email.lastUpdateDate = System.currentTimeMillis();

			emailDAO.modifyEmail(email, userId);

			Email modified = emailDAO.getEmailById(email.id, userId);
			if (modified == null) {
				res = ResponseUtil.createResponse(userId, false,
						"Fehler beim Modifizieren");

			} else {
				res = ResponseUtil.createResponse(userId, true,
						"Email wurde aktualisiert");
				res.body = modified;

			}

			return res;

		} catch (Exception e) {
			logger.error("exception in modify email: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Modifizieren");
			return res;
		}

	}

	public Response removeEmail(long id, long userId) {

		try {
			emailDAO.removeEmail(id, userId);
		} catch (Exception e) {
			logger.error("exception in remove Email: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Email zu entfernen");
			return res;
		}

		Response res = ResponseUtil.createResponse(userId, true,
				"Email wurde erfolgreich entfernt.");
		return res;
	}

	public Response mailtoAllRelatedReceiver(long mailId, long userId) {

		try {

			EmailDetail emailDetail = emailDAO.getEmailDetail(mailId, userId);

			try {
				EmailEntity emailEntity = new EmailEntity();

				emailEntity.senderEmail = senderEmail;
				emailEntity.senderName = senderName;

				resultDAO.setActionResultTexts(emailDetail);

				resultDAO.setActionResultFiles(emailDetail);

				emailEntity.attachements = fileStorage
						.readFiles(emailDetail.fileMeta);

				emailEntity.subject = String.join(",", emailDetail.domains);

				emailEntity.text = writeEmailText(emailDetail.emailId,
						senderEmail, emailDetail.actionResults,
						emailDetail.pageURLs);

				for (EmailAction emailAction : emailDetail.actions) {

					emailEntity.receiverEmail = emailDetail.receivers
							.get(emailAction.receiverId).email;

					trySendEmail(emailAction, emailEntity);
				}
			} catch (Exception e) {
				logger.error("email sending failed. emailId "
						+ emailDetail.emailId + " " + e.getMessage(), e);
			}

		} catch (Exception e) {
			logger.error("exception in remove Email: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Email zu entfernen");
			return res;
		}

		Response res = ResponseUtil.createResponse(userId, true,
				"Email wurde erfolgreich entfernt.");
		return res;
	}

	public Response mailto(long mailId, long receiverId, long userId) {

		try {

			EmailDetail emailDetail = emailDAO.getSingleEmailDetail(mailId,
					receiverId, userId);

			try {
				EmailEntity emailEntity = new EmailEntity();

				emailEntity.senderEmail = senderEmail;
				emailEntity.senderName = senderName;

				resultDAO.setActionResultTexts(emailDetail);

				resultDAO.setActionResultFiles(emailDetail);

				emailEntity.attachements = fileStorage
						.readFiles(emailDetail.fileMeta);

				emailEntity.subject = String.join(",", emailDetail.domains);

				emailEntity.text = writeEmailText(emailDetail.emailId,
						senderEmail, emailDetail.actionResults,
						emailDetail.pageURLs);

				for (EmailAction emailAction : emailDetail.actions) {

					emailEntity.receiverEmail = emailDetail.receivers
							.get(emailAction.receiverId).email;

					trySendEmail(emailAction, emailEntity);
				}
			} catch (Exception e) {
				logger.error("email sending failed. emailId "
						+ emailDetail.emailId + " " + e.getMessage(), e);
			}

		} catch (Exception e) {
			logger.error("exception in remove Email: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Email zu entfernen");
			return res;
		}

		Response res = ResponseUtil.createResponse(userId, true,
				"Email wurde erfolgreich entfernt.");
		return res;
	}

	private void sendEmail(EmailEntity emailEntity) throws MessagingException,
			UnsupportedEncodingException {

		MimeMessage message = mailSender.createMimeMessage();

		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(emailEntity.receiverEmail);
		helper.setFrom(emailEntity.senderEmail);
		helper.setSubject(emailEntity.subject);
		helper.setText(emailEntity.text, true);

		for (Attachement attachement : emailEntity.attachements) {
			ByteArrayResource file = new ByteArrayResource(
					attachement.attachement);
			helper.addAttachment(attachement.name, file, attachement.mimetype);
		}

		mailSender.send(message);

	}

	public void trySendEmail(EmailAction emailAction, EmailEntity emailEntity)
			throws EmailDBException, DAOFactoryException,
			TraceReportDBException {

		try {
			emailAction.numretries = emailAction.numretries + 1;
			emailAction.status = EmailStatus.SEND_OK;

			sendEmail(emailEntity);

			emailDAO.updateEmailStatus(emailAction);

			logger.info("send mail successful. " + emailAction.emailId);
		} catch (Exception e) {
			// if (emailAction.numretries > MAX_NUM_OF_RETRIES)
			// emailAction.status = EmailStatus.TOO_MANY_RETRIES;
			// else
			emailAction.status = EmailStatus.ERROR;

			emailDAO.updateEmailStatus(emailAction);
			logger.error(
					"send mail failed . " + emailAction.emailId
							+ e.getMessage(), e);
		}
	}

	public Response listEmail(EmailQueryScript script, long userId) {
		try {
			QueryWrapper wrapper;
			wrapper = EmailQueryBuilder.buildQueryForSearch(script);

			List<EmailActionBean> emails = emailDAO.getEmails(wrapper.sql,
					wrapper.params);

			logger.info("get results: " + emails.size() + " " + wrapper.sql);

			Response res;
			if (emails.isEmpty())
				res = ResponseUtil.createResponse(userId, true,
						"Email nicht gefunden");
			else {
				res = ResponseUtil.createResponse(userId, true,
						"Email gefunden");

			}
			res.body = emails;
			return res;
		} catch (Exception e) {
			logger.error("exception in get Email from db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Suchen");
			return res;
		}

	}

	private String writeEmailText(long emailId, String receiver,
			List<ActionResultText> text, HashSet<String> pageURLs) {

		MailContentGenerator mailContentGenerator = new MailContentGenerator();
		String body = mailContentGenerator.getTable(text);

		String message = mailContentGenerator.getHeader(pageURLs) + body
				+ mailContentGenerator.getFooter("" + emailId, receiver);

		return message;
	}

	public Response getDetail(long id, long userId) {
		try {

			EmailDetail detail = emailDAO.getEmailDetail(id, userId);
			if (detail != null) {
				resultDAO.setActionResultFiles(detail);
				resultDAO.setActionResultTexts(detail);
			}
			EmailDetailBean bean = convert(detail);
			List<EmailDetailBean> beans = new ArrayList<>();
			Response res;
			if (bean == null)
				res = ResponseUtil.createResponse(userId, true,
						"Email nicht gefunden");
			else {
				beans.add(bean);
				res = ResponseUtil.createResponse(userId, true,
						"Email gefunden");

			}
			res.body = beans;
			return res;
		} catch (Exception e) {
			logger.error("exception in get Email from db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Suchen");
			return res;
		}
	}

	private EmailDetailBean convert(EmailDetail detail)
			throws JsonParseException, JsonMappingException, IOException {

		EmailDetailBean bean = new EmailDetailBean();
		bean.id = detail.emailId;
		bean.subject = detail.subject;

		for (EmailAction emailAction : detail.actions) {
			EmailReceiverBean emailReceiverBean = new EmailReceiverBean();
			emailReceiverBean.id = emailAction.receiverId;
			emailReceiverBean.status = emailAction.status;
			emailReceiverBean.address = detail.receivers
					.get(emailAction.receiverId).address;
			emailReceiverBean.firstName = detail.receivers
					.get(emailAction.receiverId).firstName;
			emailReceiverBean.lastName = detail.receivers
					.get(emailAction.receiverId).lastName;
			emailReceiverBean.email = detail.receivers
					.get(emailAction.receiverId).email;
			emailReceiverBean.timestamp = emailAction.time;
			bean.receivers.add(emailReceiverBean);
		}
		HashMap<String, ActionResultFile> files = new HashMap<>();
		HashMap<String, ActionResultFile> images = new HashMap<>();
		HashSet<String> usedUrls = new HashSet<>();

		for (ActionResultFile file : detail.fileMeta) {
			if (file.type == ActionMetaConstant.pdf)
				files.put(file.url, file);
			else if (file.type == ActionMetaConstant.screenshot_img)
				images.put(file.url, file);
		}

		for (ActionResultText text : detail.actionResults) {
			if (text.type.equals(BlockActionType.structure.name())) {

				ObjectNode dataset = (ObjectNode) jacksonObjectMapper
						.readTree(text.content);
				List<DataRowItemBean> row = new ArrayList<>();
				JsonNode blocks = dataset.get("blocks");
				for (int i = 0; i < blocks.size(); i++) {
					JsonNode block = blocks.get(i);
					DataRowItemBean dataRowItemBean = new DataRowItemBean();
					dataRowItemBean.name = block.get("name").asText();
					dataRowItemBean.value = block.get("blockText").asText();
					if (block.has("link")) {
						JsonNode link = block.get("link");
						String url = link.get("url").asText();
						dataRowItemBean.link = url;
						if (files.containsKey(url)) {
							dataRowItemBean.file =(files.get(url));
							usedUrls.add(url);
						}
						if (images.containsKey(url)) {
							dataRowItemBean.image = (files.get(url));
							usedUrls.add(url);
						}
					}
					row.add(dataRowItemBean);

				}
				bean.rows.add(row);
			}
		}

		for (ActionResultFile file : detail.fileMeta) {
			if (file.type == ActionMetaConstant.pdf && !usedUrls.contains(file.url))
				files.put(file.url, file);
			else if (file.type == ActionMetaConstant.screenshot_img && !usedUrls.contains(file.url))
				images.put(file.url, file);
		}
		
		return bean;
	}

	public Response updateEmailReceiver(Email email, HashSet<Long> groupIds,
			HashSet<Long> receiverIds, long userId) {
		try {
			HashSet<Long> members = receiverDAO.getReceiverByGroupIds(groupIds,
					userId);
			receiverIds.addAll(members);
			emailDAO.updateEmailReceiver(email, receiverIds, userId);

			Response res = ResponseUtil
					.createResponse(userId, true, "Email Receiver hinzugefügt");

			if (!receiverIds.isEmpty())
				return mailtoAllRelatedReceiver(email.id, userId);

			return res;
		} catch (Exception e) {
			logger.error(
					"exception in creating email in db: " + e.getMessage(), e);
			Response res = ResponseUtil.createResponse(userId, false,
					"Fehler beim Speichern");
			return res;
		}
	}
}
