package com.datalyxt.production.web.bean.source;

import java.util.HashSet;

public class SourceCreateInput {
	public String sourceUrl;
	public HashSet<String> sourceTypes = new HashSet<>();
}
