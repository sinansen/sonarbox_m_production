package com.datalyxt.production.web.bean.receiver;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.receiver.M_ReceiverStatus;

public class ReceiverUIBean {
	public String email;
	public String lastName;
	public String firstName;
	public String company;
	public long groupId;
	public String address;
	public long id;
	public M_ReceiverStatus status;
	public List<GroupBasicBean> groups = new ArrayList<GroupBasicBean>();
	public long createdAt;
	public long lastModified;
	public String theFunction;
	public String department;
	public String category;
}
