package com.datalyxt.production.web.saml;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.cert.Certificate;
import java.security.interfaces.DSAPublicKey;
import java.security.interfaces.RSAPublicKey;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.datalyxt.production.web.bean.AuthResponse;

public class Response {
	// private static DateFormat DATE_TIME_FORMAT = new
	// SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	private DateFormat DATE_TIME_FORMAT_MIN = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");
	private DateFormat DATE_TIME_FORMAT_MAX = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");

	// DATE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));

	// static {
	// DATE_TIME_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
	// }
	public Document xmlDoc;
	private AccountSettings accountSettings;
	private Certificate certificate;

	private DSAPublicKey dsaPublicKeys[];

	private RSAPublicKey rsaPublicKeys[];

	// public Response(AccountSettings accountSettings) throws
	// CertificateException {
	public Response(RSAPublicKey rsaPublicKeys[]) {
		this.rsaPublicKeys = rsaPublicKeys;

		// this.accountSettings = accountSettings;
		// certificate = new Certificate();
		// certificate.loadCertificate(this.accountSettings.getCertificate());
	}

	public void loadXml(String xml) throws ParserConfigurationException,
			SAXException, IOException {
		DocumentBuilderFactory fty = DocumentBuilderFactory.newInstance();
		fty.setNamespaceAware(true);
		DocumentBuilder builder = fty.newDocumentBuilder();
		ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
		xmlDoc = builder.parse(bais);
	}

	public void loadXmlFromBase64(String response)
			throws ParserConfigurationException, SAXException, IOException,
			DecoderException {

		Base64 base64 = new Base64();
		byte[] decodedB = base64.decode(response);
		String decodedS = new String(decodedB);
		loadXml(decodedS);
	}

	public boolean isValid() throws Exception {
		NodeList nodes = xmlDoc.getElementsByTagNameNS(XMLSignature.XMLNS,
				"Signature");
		boolean isValid = false;

		if (nodes == null || nodes.getLength() == 0) {
			throw new Exception("Can't find signature in document.");
		}

		// Fix: Aufgrund
		// http://www.nds.rub.de/research/publications/BreakingSAML/
		// wird hier noch geprueft ob nur eine Signatur vorhanden ist
		// da der gesamte SamlResponse gesignt ist, duerfte dieser Fix
		// ausreichen!
		if (nodes.getLength() > 1) {
			throw new Exception("Too many signatures in document. Found "
					+ nodes.getLength() + " signatures");
		}

		// Pruefung danach ob nur eine Assertion vorhanden ist ---
		// NICHT-Standardkonform
		NodeList assertionNodes = xmlDoc.getElementsByTagNameNS(
				"urn:oasis:names:tc:SAML:2.0:assertion", "Assertion");
		if (assertionNodes.getLength() > 1) {
			throw new Exception(
					"Too many assertions found in document (not standard but disabled due to security issues). Found "
							+ assertionNodes.getLength() + " assertions");

		}

		// X509Certificate cert = certificate.getX509Cert();
		// Just the key - TO-DO: Pfadvariable machen
		// TODO Load the path from a keystore

		// String publicKeyFilePath =
		// "E:/Workspace/MNet/markant-saml-demo-was/WebContent/keys/markant.com.public.der.key";
		// DSAPublicKey publicKey = (DSAPublicKey)
		// Util.getPublicKey(publicKeyFilePath, "DSA");
		// RSAPublicKey publicKey = (RSAPublicKey)
		// Util.getPublicKey(publicKeyFilePath, "RSA");

		// DOMValidateContext ctx = new DOMValidateContext(publicKey,
		// nodes.item(0));

		// Go through the public keys (from jks)

		// TODO: ANpassen kann eigentlich das PK-Element aus dem Response
		// genommen werden. Ggf. matching machen
		for (int i = 0; i <= rsaPublicKeys.length; i++) {
			// if not null
			if (rsaPublicKeys[i] != null) {
				// validate against public key
				DOMValidateContext ctx = new DOMValidateContext(
						rsaPublicKeys[i], nodes.item(0));
				XMLSignatureFactory sigF = XMLSignatureFactory
						.getInstance("DOM");
				XMLSignature xmlSignature = sigF.unmarshalXMLSignature(ctx);
				isValid = xmlSignature.validate(ctx);
				if (isValid) {
					// if we got a match (a verified signature) - break loop
					break;
				}
			}
		}

		return isValid;

	}

	public AuthResponse getNameId() throws Exception {

		AuthResponse authResponse = new AuthResponse();

		NodeList nodes = xmlDoc.getElementsByTagNameNS(
				"urn:oasis:names:tc:SAML:2.0:assertion", "NameID");

		if (nodes.getLength() == 0) {
			throw new Exception("No name id found in document");
		}
		for (int i = 0; i < nodes.getLength(); i++) {
			String format = nodes.item(i).getAttributes()
					.getNamedItem("Format").getNodeValue();
			String nodeValue = nodes.item(i).getTextContent().trim();
			if (format
					.equals("urn:oasis:names:tc:SAML:1.1:nameid-format:userid"))
				authResponse.userName = nodeValue;
			else if (format
					.equals("urn:oasis:names:tc:SAML:1.1:nameid-format:userEmailAddress"))
				authResponse.email = nodeValue;
			else if (format
					.equals("urn:oasis:names:tc:SAML:1.1:nameid-format:userLanguage"))
				authResponse.language = nodeValue;
		}
		return authResponse;
	}

	public boolean isTimedOut() throws Exception {

		// Get NotBeforeAfter from SAMLResponse
		NodeList nodes = xmlDoc.getElementsByTagNameNS(
				"urn:oasis:names:tc:SAML:2.0:assertion", "Conditions");
		// Get Current Time (formatted)

		DATE_TIME_FORMAT_MIN.setTimeZone(TimeZone.getTimeZone("UTC"));
		DATE_TIME_FORMAT_MAX.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date now = new Date();
		DATE_TIME_FORMAT_MAX.format(now);
		// Get Timeout Time (formatted)
		Date timeout = DATE_TIME_FORMAT_MAX.parse(nodes.item(0).getAttributes()
				.getNamedItem("NotOnOrAfter").getTextContent());
		System.out.println("Now is: " + now + " - timeout is: " + timeout);

		if (now.after(timeout)) {
			System.out.println("SAMLResponse is timed out");
			return true;

		}

		if (nodes == null || nodes.getLength() == 0) {
			throw new Exception("Can't find Conditions in document.");
		}
		return false;
	}

}
