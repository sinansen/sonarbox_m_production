package com.datalyxt.production.web.marker;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.GetBase;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.MarkerUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;

@RestController
public class Marker {
	
	private static Logger logger = LoggerFactory.getLogger(Marker.class);

	@RequestMapping(value = "/sonarbox/pagemarker", method = { RequestMethod.GET })
	protected String marker(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String pageSource = "<html><body><center><h3>Error: Page nicht gefunden.</h3></center></body></html>";
		WebDriver driver;
		try {

			if (isWindows())
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_windows);
			else
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_unix);
		} catch (Exception e) {
			logger.error("created driver failed", e);
			return getInfoPage("Browser Failed.");
		}
		// TODO Auto-generated method stub
		String pageURL = request.getParameter("link");

		logger.info("get link: " + pageURL);

		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		script.enrichTextNode = true;
		script.enableEnrichTextNodeWithJs = false;
		script.removeElementCssClassAttr = false;
		script.removeElementIdAttr = false;
		script.removeHeadJavascriptLink = true;
		script.removeJavascriptElementContent = false;

		String resourceLocation = "http://localhost:8080/sonarbox";

		try {
			String resloc = request.getParameter("resource");
			if (resloc != null)
				resourceLocation = resloc;

			String enrichTextNode = request.getParameter("erichtn");
			if (enrichTextNode != null)
				script.enrichTextNode = Boolean.valueOf(enrichTextNode);

			String enableEnrichTextNodeWithJs = request
					.getParameter("jserichtn");
			if (enableEnrichTextNodeWithJs != null)
				script.enableEnrichTextNodeWithJs = Boolean
						.valueOf(enableEnrichTextNodeWithJs);

			String removeElementCssClassAttr = request.getParameter("rvcss");
			if (removeElementCssClassAttr != null)
				script.removeElementCssClassAttr = Boolean
						.valueOf(removeElementCssClassAttr);

			String removeElementIdAttr = request.getParameter("rvid");
			if (removeElementIdAttr != null)
				script.removeElementIdAttr = Boolean
						.valueOf(removeElementIdAttr);

			String removeHeadJavascriptLink = request.getParameter("rvhjs");
			if (removeHeadJavascriptLink != null)
				script.removeHeadJavascriptLink = Boolean
						.valueOf(removeHeadJavascriptLink);

			String removeJavascriptElementContent = request
					.getParameter("rvbjsc");
			if (removeJavascriptElementContent != null)
				script.removeJavascriptElementContent = Boolean
						.valueOf(removeJavascriptElementContent);

		} catch (Exception e) {
			logger.error("parameter is wrong!! " + e.getMessage(), e);
			return getInfoPage("Parameter sind falsch.");
		}
	
		try {

			BrowsingConfig browsingConfig = new BrowsingConfig();
			browsingConfig.pageLoadTimeout = 30000;

			GetBase baseUtil = new GetBase();
			String baseURI = GetHost.guessBaseUrl(pageURL);
			String protocalhost = GetHost.getProtocalHost(pageURL);
			Document doc = MarkerUtil.getCleanedPageSource(pageURL, driver,
					script, resourceLocation, browsingConfig);
			String docBase = doc.baseUri();
			boolean hasBase = false;
			if (docBase != null && !docBase.isEmpty())
				hasBase = true;
			Elements atags = doc.select("a[href]");
			for (Element atag : atags) {
				if (hasBase) {
					String absurl = atag.absUrl("href");
					atag.attr("href", absurl);
				} else {
					String href = atag.attr("href");

					String absurl = baseUtil.getCalculatedBase(href, baseURI,
							protocalhost, pageURL);
					atag.attr("href", absurl);
				}
			}
			pageSource = doc.html();
			driver.quit();
			String charset = MarkerUtil.getCharset(doc);
			response.setCharacterEncoding(charset);

			// return value.toString();

		} catch (Exception e) {
			  
			logger.error("page not found. "+e.getMessage(), e);
			return getInfoPage("Seite nicht gefunden.");
		}
		return pageSource;
		// PrintWriter out = response.getWriter();
		// out.println(pageSource);
		// out.close();

	}

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}
	
	private String getInfoPage(String msg){
		String pageSource = "<html><body><center><h3>"+msg+"</h3></center></body></html>";
		return pageSource;
	}
}
