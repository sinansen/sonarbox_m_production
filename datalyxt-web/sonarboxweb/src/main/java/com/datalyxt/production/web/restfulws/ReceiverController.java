package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.receiver.ReceiverQueryScript;
import com.datalyxt.production.web.bean.receiver.ReceiverUIBean;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ActivityService;
import com.datalyxt.production.web.service.ReceiverService;
import com.datalyxt.production.web.service.ResponseUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("vims/sonarbox/res/receivers")
@RestController
public class ReceiverController {

	private static Logger logger = LoggerFactory
			.getLogger(ReceiverController.class);

	@Autowired
	ReceiverService receiverService;

	@Autowired
	ActivityService activityService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	// Create a receiver
	@RequestMapping(method = RequestMethod.POST)
	public Response createReceiver(Principal principal,@RequestBody String receiverJson) {
		ReceiverUIBean receiverBean = new ReceiverUIBean();
		long userId = Long.valueOf(principal.getName());
		try {
			receiverBean = jacksonObjectMapper.readValue(receiverJson,
					ReceiverUIBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		logger.info("Creating Receiver " + receiverJson);
		M_Receiver receiver = receiverService.convert(receiverBean);
		Response res = receiverService.createReceiver(receiver, userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.created,
						receiver.id, M_ObjectType.receiver);
				activity.userId = userId;
				activity.content = receiverJson;
				activityService.save(activity);
			} else
				logger.info("Creating Receiver failed. " + res.message);
		} catch (M_ActivityDBException e) {
			logger.error("save receiver activity failed. " + e.getMessage());
			throw new InternalServerError(" save receiver activity failed. ");
		}

		return res;

	}

	// list receiver

	@RequestMapping(method = RequestMethod.GET)
	public Response listReceiver(
			Principal principal,
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "status", defaultValue = "") String statusFilter,
			@RequestParam(value = "languages", defaultValue = "") String languagesFilter,
			@RequestParam(value = "types", defaultValue = "") String typesFilter,
			@RequestParam(value = "start", defaultValue = "") String startLastModifiedFilter,
			@RequestParam(value = "end", defaultValue = "") String endLastModifiedFilter) {

		long userId = Long.valueOf(principal.getName());

		logger.info("List Receiver ");

		ReceiverQueryScript queryscript = new ReceiverQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;

			switch (toSwitch) {
			case "name":
				queryscript.sortByName = true;
				break;
			case "lastModified":
				queryscript.sortByModified = true;
				break;
			case "domain":
				queryscript.sortByEmail = true;
				break;
			case "created":
				queryscript.sortByCreated = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		}

		// check filter values
		try {

			// statusFilter
			if (!statusFilter.isEmpty()) {
				String[] statusParsed = statusFilter.split(", ");

				HashSet<M_ReceiverStatus> status = new HashSet<>();

				for (String s : statusParsed) {
					status.add(M_ReceiverStatus.valueOf(s));
				}

				queryscript.status = status;
			}

			// lastModified
			if (!startLastModifiedFilter.isEmpty())
				queryscript.startTimeModified = Long
						.parseLong(startLastModifiedFilter);
			if (!endLastModifiedFilter.isEmpty())
				queryscript.endTimeModified = Long
						.parseLong(endLastModifiedFilter);

		} catch (IllegalArgumentException iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return receiverService.listReceiver(queryscript, userId);

	}

	// modify receiver

	@RequestMapping(method = RequestMethod.PUT)
	public Response modifyReceiver(Principal principal,@RequestBody String receiverJson) {
		long userId = Long.valueOf(principal.getName());

		ReceiverUIBean receiver;
		try {
			logger.info("get call!!!! " + receiverJson);
			receiver = jacksonObjectMapper.readValue(receiverJson,
					ReceiverUIBean.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			Response res = receiverService.createResponse(userId, false,
					"Parameter sind falsch");
			return res;
		}

		logger.info("modify Receiver " + receiver.email);
		M_Receiver changed = receiverService.convert(receiver);
		Response res = receiverService.modifyReceiver(changed, userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.modified,
						receiver.id, M_ObjectType.receiver);
				activity.userId = userId;
				activity.content = receiverJson;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("modify receiver activity failed. " + e.getMessage());
			throw new InternalServerError(" modify receiver activity failed. ");
		}

		return res;

	}

	// remove receiver from group
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Response removeReceiver(Principal principal,@PathVariable("id") long id) {
		long userId = Long.valueOf(principal.getName());

		logger.info("remove Receiver " + id);

		Response res = receiverService.removeReceiver(id, userId);

		try {
			if (res.success) {
				M_Activity activity = new M_Activity(M_ActivityType.deleted,
						id, M_ObjectType.receiver);
				activity.userId = userId;
				activity.content = "receiver-" + id;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException e) {
			logger.error("update receiver activity failed. " + e.getMessage());
			throw new InternalServerError(" update receiver activity failed. ");
		}

		return res;

	}

	@RequestMapping(value = "{id}/status", method = RequestMethod.PUT)
	public Response changeStatus(Principal principal,@PathVariable("id") long id,
			@RequestBody String statusInput) {

		logger.info("Change receiver status for " + id + " " + statusInput);
		long userId = Long.valueOf(principal.getName());

		// change source status
		try {
			JsonNode object = jacksonObjectMapper.readValue(statusInput,
					JsonNode.class);
			String sts = object.get("status").textValue();

			M_ReceiverStatus status = M_ReceiverStatus.valueOf(sts);
			receiverService.changeReceiverStatus(id, status, userId);

			// log activity
			M_ActivityType type;

			switch (status) {
			case activated:
				type = M_ActivityType.activated;
				break;
			case paused:
				type = M_ActivityType.paused;
				break;
			default:
				throw new WrongParameterException(
						"Fehler, Diese Modifizierung des Statuses ist nicht möglich");

			}

			M_Activity activity = new M_Activity(type, id,
					M_ObjectType.receiver);
			activity.userId = userId;
			activityService.save(activity);

		} catch (Exception e) {
			throw new InternalServerError(
					"Fehler, Änderung des Receiverstatuses fehlgeschlagen");
		}

		// return response
		return ResponseUtil.createResponse(userId, true,
				"Receiver Status erfolgreich verändert");
	}
}
