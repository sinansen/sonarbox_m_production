package com.datalyxt.production.web.bean.result;

import com.datalyxt.production.web.bean.ActionResultStatis;


public class ScraperResultDetailRootBean {
	public long scanId;
	public int maxHops;
	public long sourceId;
	public String sourceName;
	public long selectorId;
	public String selectorName;
	public String sourceUrl;
	public ScraperResultDetailBean root;
	public ActionResultStatis statis;
}
