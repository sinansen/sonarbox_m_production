package com.datalyxt.production.web.dao.impl;

import java.util.HashSet;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.webdatabase.dao.FilterOperatorDAO;

@Repository("filterOperatorDAO")
public class MysqlFilterOperatorDAOImpl implements FilterOperatorDAO{
	

	private JdbcTemplate jdbcTemplate;
	@Autowired
	private DataSource dataSource;
	
	
	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public HashSet<String> getOperators() throws DataBaseException {
		HashSet<String> operators = new HashSet<>();
		try {
			String sql = "select * from m_filter_operator ";

			SqlRowSet resultSet = jdbcTemplate.queryForRowSet(sql);
			
			while(resultSet.next()){
				operators.add(resultSet.getString("operator"));
			}
		} catch (Exception e) {
			throw new DataBaseException("Error in getOperators: " + e.getMessage(), e);
		}
		return operators;
	}

}
