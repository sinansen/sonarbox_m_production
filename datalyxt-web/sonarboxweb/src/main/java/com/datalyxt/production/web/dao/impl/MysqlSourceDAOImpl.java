package com.datalyxt.production.web.dao.impl;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.webdatabase.dao.M_SourceAdminDAO;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Repository("sourceDAO")
public class MysqlSourceDAOImpl implements M_SourceAdminDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlSourceDAOImpl.class);
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				jdbcTemplate);
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		return false;
	}

	@Override
	public long insertSource(M_Source source, long ownerId)
			throws M_SourceDBException {
		try {
			KeyHolder keyHolder = new GeneratedKeyHolder();
			String sql = "insert into m_source (nurl_md5, name, status, url, url_normalized, config, created_at, last_modified,  feedback, domain, languages, type, owner_id) values (?, ?, ?, ?,?,?,?,?,?,?,?,?, ?) ";

			Gson gson = new Gson();
			String content_json = gson.toJson(source.sourceConfig,
					SourceConfig.class);

			HashSet<LanguageISO_639_1> langs = new HashSet<LanguageISO_639_1>();

			if (source.sourceConfig.qt1v1SourceConfig != null) {
				langs.addAll(source.sourceConfig.qt1v1SourceConfig.languages);
			}

			if (source.sourceConfig.qt1v2SourceConfig != null) {
				langs.addAll(source.sourceConfig.qt1v2SourceConfig.languages);
			}

			String lang_json = gson.toJson(langs);
			String types_json = gson.toJson(source.sourceTypes);

			// jdbcTemplate.update(sql, new Object[] { source.sourceNUrlMd5,
			// source.sourceName, source.sourceStatus.name(),
			// source.sourceUrl, source.sourceUrlNormalized, content_json,
			// new Timestamp(source.sourceCreatedAt),
			// new Timestamp(source.sourceLastModified), source.feedback,
			// source.sourceDomain }, keyHolder);

			jdbcTemplate.update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement pst = con.prepareStatement(sql,
							new String[] { "group_id" });
					int index = 1;
					pst.setString(index++, source.sourceNUrlMd5);
					pst.setString(index++, source.sourceName);
					pst.setString(index++, source.sourceStatus.name());
					pst.setString(index++, source.sourceUrl);
					pst.setString(index++, source.sourceUrlNormalized);
					pst.setString(index++, content_json);
					pst.setTimestamp(index++, new Timestamp(
							source.sourceCreatedAt));
					pst.setTimestamp(index++, new Timestamp(
							source.sourceLastModified));
					pst.setString(index++, source.feedback);
					pst.setString(index++, source.sourceDomain);
					pst.setString(index++, lang_json);
					pst.setString(index++, types_json);
					pst.setLong(index++, ownerId);
					return pst;
				}
			}, keyHolder);

			source.sourceId = (Long) keyHolder.getKey();
			// source.sourceId = (Long) jdbcTemplate
			// .queryForObject(
			// "select source_id from m_source where source_nurl_md5 = ? ",
			// new Object[] { source.sourceNUrlMd5 }, Long.class);

			String subsql = "insert into m_source_type_mapping (source_id, source_type) values (?,?) ";

			List<M_SourceType> types = new ArrayList<M_SourceType>();

			types.addAll(source.sourceTypes);
			if (!types.isEmpty()) {
				jdbcTemplate.batchUpdate(subsql,
						new BatchPreparedStatementSetter() {

							@Override
							public void setValues(PreparedStatement ps, int i)
									throws SQLException {
								M_SourceType sourceType = types.get(i);
								ps.setLong(1, source.sourceId);
								ps.setString(2, sourceType.name());
							}

							@Override
							public int getBatchSize() {
								return types.size();
							}
						});
			}
			return source.sourceId;

		} catch (Exception e) {
			throw new M_SourceDBException("Error in insert source: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public void changeSourceStatus(long sourceId, M_SourceStatus status,
			long ownerId) throws M_SourceDBException {
		try {

			String sql = "update m_source set status = ? , last_modified = ?  where id = ?  and owner_id = ? ";

			Timestamp now = new Timestamp(System.currentTimeMillis());

			jdbcTemplate.update(sql, new Object[] { status.name(), now,
					sourceId, ownerId });

		} catch (Exception e) {
			throw new M_SourceDBException("Error in changeSourceStatus: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public void updateSourceConfig(M_Source source, long ownerId)
			throws M_SourceDBException {
		try {

			String sql = "update m_source set config = ? , last_modified = ?  where id = ? and owner_id = ? ";
			Timestamp now = new Timestamp(System.currentTimeMillis());
			Gson gson = new Gson();
			String content_json = gson.toJson(source.sourceConfig,
					SourceConfig.class);

			jdbcTemplate.update(sql, new Object[] { content_json, now,
					source.sourceId, ownerId });

		} catch (Exception e) {
			throw new M_SourceDBException("Error in updateSourceConfig: "
					+ e.getMessage(), e);
		}

	}

	private List<M_Source> mapRow(SqlRowSet rows) throws SQLException {
		List<M_Source> sources = new ArrayList<M_Source>();

		while (rows.next()) {
			try {
				M_Source source = new M_Source();
				source.sourceCreatedAt = rows.getTimestamp("created_at")
						.getTime();
				source.sourceLastModified = rows.getTimestamp("last_modified")
						.getTime();
				source.sourceStatus = M_SourceStatus.valueOf(rows
						.getString("status"));
				source.sourceId = rows.getLong("id");

				source.sourceName = rows.getString("name");

				source.sourceUrl = rows.getString("url");

				source.sourceUrlNormalized = rows.getString("url_normalized");

				source.sourceDomain = rows.getString("domain");

				source.feedback = rows.getString("feedback");

				source.sourceNUrlMd5 = rows.getString("nurl_md5");

				String typeJson = rows.getString("type");
				if (typeJson != null) {
					Gson gson = new Gson();
					Type type = new TypeToken<List<M_SourceType>>() {
					}.getType();
					List<M_SourceType> inpList = gson.fromJson(typeJson, type);
					source.sourceTypes.addAll(inpList);
				}

				String config = rows.getString("config");

				if (config != null) {
					Gson gson = new Gson();

					source.sourceConfig = gson.fromJson(config,
							SourceConfig.class);
				}
				sources.add(source);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}

		}
		return sources;
	}

	@Override
	public boolean isSourceExist(String md5NormURL) throws M_SourceDBException {
		try {
			String sql = "select id from m_source where nurl_md5 = ? limit 1";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { md5NormURL });
			if (rows.next()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			throw new M_SourceDBException("Error in isSourceExist: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public boolean isSourceExist(long id) throws M_SourceDBException {
		try {
			String sql = "select id from m_source where id = ? limit 1";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { id });
			if (rows.next()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			throw new M_SourceDBException("Error in isSourceExist: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public void updateSourceStatusAndFeedback(String source_nurl_md5,
			M_SourceStatus status, String feedback, long ownerId)
			throws M_SourceDBException {
		try {

			String sql = "update m_source set status = ? , feedback = ?  , last_modified = ? where nurl_md5 = ?  and owner_id = ? ";
			Timestamp now = new Timestamp(System.currentTimeMillis());
			jdbcTemplate.update(sql, new Object[] { status.name(), feedback,
					now, source_nurl_md5, ownerId });

		} catch (Exception e) {
			throw new M_SourceDBException(
					"Error in updateSourceStatusAndFeedback: " + e.getMessage(),
					e);
		}

	}

	@Override
	public void modifySource(M_Source oldSource, M_Source newSource,
			long ownerId) throws M_SourceDBException {
		try {

			Gson gson = new Gson();
			String content_json = gson.toJson(newSource.sourceConfig,
					SourceConfig.class);

			String sql = "update m_source set status = ?, url = ? , nurl_md5 = ?, domain = ?, name = ?, url_normalized = ?, last_modified = ?, config = ? , type = ?, languages= ? where id = ?  ";

			HashSet<LanguageISO_639_1> langs = new HashSet<LanguageISO_639_1>();

			if (newSource.sourceConfig.qt1v1SourceConfig != null) {
				langs.addAll(newSource.sourceConfig.qt1v1SourceConfig.languages);
			}

			if (newSource.sourceConfig.qt1v2SourceConfig != null) {
				langs.addAll(newSource.sourceConfig.qt1v2SourceConfig.languages);
			}

			String lang_json = gson.toJson(langs);
			String types_json = gson.toJson(newSource.sourceTypes);

			Timestamp now = new Timestamp(newSource.sourceLastModified);
			jdbcTemplate.update(sql,
					new Object[] { newSource.sourceStatus.name(),
							newSource.sourceUrl, newSource.sourceNUrlMd5,
							newSource.sourceDomain, newSource.sourceName,
							newSource.sourceUrlNormalized, now, content_json,
							types_json, lang_json, oldSource.sourceId });

		} catch (Exception e) {
			throw new M_SourceDBException("Error in modifySource: "
					+ e.getMessage(), e);
		}

	}

	@Override
	public M_Source getSourceById(long sourceId) throws M_SourceDBException {
		List<M_Source> sources = null;

		try {
			String sql = "select nurl_md5, feedback, domain, config, id, name, status, url, url_normalized, created_at , last_modified  , type from m_source where id = ? ";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { sourceId });
			sources = mapRow(rows);

			// resultSet = preparedStmt.executeQuery();
			// sources = convertResultsets(resultSet);
		} catch (Exception e) {
			throw new M_SourceDBException("Error in getSourceById: "
					+ e.getMessage(), e);
		}
		if (sources == null || sources.isEmpty())
			return null;

		return sources.get(0);
	}

	@Override
	public M_Source getSourceByNmd5(String nurlMd5) throws M_SourceDBException {
		List<M_Source> sources = new ArrayList<M_Source>();
		try {
			String sql = "select nurl_md5, feedback, domain, id, name, config, status, url, url_normalized, created_at , last_modified  , type from m_source where nurl_md5 = ? limit 1";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { nurlMd5 });
			sources = mapRow(rows);
			if (sources.isEmpty())
				return null;
			else
				return sources.get(0);
		} catch (Exception e) {
			throw new M_SourceDBException("Error in isSourceExist: "
					+ e.getMessage(), e);
		}
	}

	@Override
	public List<M_Source> getSources(String sql, Object[] params)
			throws M_SourceDBException {
		try {
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			List<M_Source> sources = mapRow(rows);
			return sources;
		} catch (Exception e) {
			throw new M_SourceDBException(e.getMessage(), e);
		}
	}

	@Override
	public HashMap<Long, M_Source> getSources(HashSet<Long> sourceIds)
			throws M_SourceDBException {
		try {
			HashMap<Long, M_Source> sources = new HashMap<Long, M_Source>();
			if (sourceIds == null | sourceIds.isEmpty())
				return sources;

			String sql = "select id, name, url from m_source where id in (:sourceIds) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("sourceIds", sourceIds);

			SqlRowSet rows = namedParameterJdbcTemplate.queryForRowSet(sql,
					parameters);

			while (rows.next()) {
				long id = rows.getLong("id");
				String name = rows.getString("name");
				String url = rows.getString("url");
				M_Source src = new M_Source();
				src.sourceId = id;
				src.sourceName = name;
				src.sourceUrl = url;
				sources.put(id, src);
			}
			return sources;
		} catch (Exception e) {
			throw new M_SourceDBException("Error in getSources: "
					+ e.getMessage(), e);
		}
	}

}
