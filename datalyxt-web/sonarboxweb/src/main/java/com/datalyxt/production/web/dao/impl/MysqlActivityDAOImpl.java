package com.datalyxt.production.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.webdatabase.dao.M_ActivityAdminDAO;

/**
 * Created by michael on 7/1/16.
 */

@Repository("activityDAO")
public class MysqlActivityDAOImpl implements M_ActivityAdminDAO {

    private static Logger logger = LoggerFactory
            .getLogger(MysqlSourceDAOImpl.class);
    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private DataSource dataSource;

    @PostConstruct
    private void initialize() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
                jdbcTemplate);
    }

    @Override
    public boolean isConnectionClosed() throws DataBaseException {
        return false;
    }

    @Override
    public M_Activity insert(M_Activity activity) throws M_ActivityDBException {
        try {
            KeyHolder keyHolder = new GeneratedKeyHolder();
            String sql = "insert into m_activity (type, user_id, object_id, object_type, timestamp, content) values (?,?,?,?,?,?)";

            jdbcTemplate.update(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                    PreparedStatement pst = con.prepareStatement(sql, new String[] { "group_id" });
                    int index = 1;
                    pst.setString(index++, activity.type.name());
                    pst.setString(index++, String.valueOf(activity.userId));
                    pst.setString(index++, String.valueOf(activity.objectId));
                    pst.setString(index++, activity.objectType.name());
                    pst.setTimestamp(index++, new Timestamp(activity.timestamp));
                    pst.setString(index++, activity.content);

                    return pst;
                }
            }, keyHolder);

            activity.id = (Long) keyHolder.getKey();

        } catch (Exception e) {
            throw new M_ActivityDBException("Error in insert activity: "  + e.getMessage(), e);
        }

        return activity;
    }

    @Override
    public M_Activity getById(long id) throws M_ActivityDBException {
        List<M_Activity> sources = null;

        try {
            String sql = "select id, type, user_id, object_id, object_type, timestamp, content where id = ? ";
            SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, new Object[] { id });

            sources = mapRow(rows);
        } catch (Exception e) {
            throw new M_ActivityDBException("Error in get activity by id: " + e.getMessage(), e);
        }
        if (sources == null || sources.isEmpty())
            return null;

        return sources.get(0);
    }

    @Override
    public List<M_Activity> list(String sql, Object[] params) throws M_ActivityDBException {
        try {
            SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
            List<M_Activity> activities = mapRow(rows);

            return activities;

        } catch (Exception e) {
            throw new M_ActivityDBException(e.getMessage(), e);
        }
    }

    private List<M_Activity> mapRow(SqlRowSet rows) throws SQLException {
        List<M_Activity> activities = new ArrayList<M_Activity>();

        while (rows.next()) {
            try {
                M_Activity activity = new M_Activity(M_ActivityType.valueOf(rows.getString("type")), rows.getLong("object_id"), M_ObjectType.valueOf(rows.getString("object_type")));
                activity.id = rows.getLong("id");
                activity.userName = rows.getString("user_name");
                activity.content = rows.getString("content");
                activity.timestamp = rows.getTimestamp("timestamp").getTime();

                activities.add(activity);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

        }
        return activities;
    }
}
