package com.datalyxt.production.web.bean.report;

public class ReportDetailBean {
	public long id ;
	public long scanId ;
	public long selectorId ;
	public long sourceId;
	public long time;
	public String type;
	public String url;
	public String error;
	public String message;
	public String reporter;
}
