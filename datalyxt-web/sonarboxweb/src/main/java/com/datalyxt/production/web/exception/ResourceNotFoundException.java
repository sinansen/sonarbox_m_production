package com.datalyxt.production.web.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by michael on 6/12/16.
 */
public class ResourceNotFoundException extends RestServiceException {

    public ResourceNotFoundException(long id) {
        super("Resource with id " + id + " not found");
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.NOT_FOUND;
    }
}