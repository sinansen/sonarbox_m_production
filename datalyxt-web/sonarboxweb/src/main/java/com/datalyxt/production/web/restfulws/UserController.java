package com.datalyxt.production.web.restfulws;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.web.bean.AuthResponse;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.service.ConsumeSamlResponseService;

@RequestMapping("vims/sonarbox/res/users")
@RestController
public class UserController {

	@Autowired
	ConsumeSamlResponseService consumeSamlResponseService;


	@RequestMapping(value = "/check", method = RequestMethod.POST)
	public Response checkSaml(HttpServletRequest request) {
		String result = request.getParameter("SAMLResponse");
		AuthResponse authResponse = consumeSamlResponseService.consume(result);
		Response resp = new Response();
		resp.success = true;
		resp.body = authResponse;
		return resp;
	}
}
