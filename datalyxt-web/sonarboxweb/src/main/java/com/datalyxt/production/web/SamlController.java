package com.datalyxt.production.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.view.RedirectView;

import com.datalyxt.production.web.bean.AuthResponse;
import com.datalyxt.production.web.service.AuthenticationService;
import com.datalyxt.production.web.service.ConsumeSamlResponseService;

@Controller
public class SamlController {

	@Value("${saml.idp}")
	private String samlIDP;

	@Autowired
	AuthenticationService authenticationService;
	@Autowired
	ConsumeSamlResponseService consumeSamlResponseService;
	
	private static Logger logger = LoggerFactory.getLogger(SamlController.class);

	@RequestMapping(value = "/vims/login", method = { RequestMethod.GET })
	public RedirectView sendToIDP(HttpServletRequest request, HttpSession session) {

		logger.info("enter login " + session.getId());
		boolean isAlreadyLogIn = authenticationService.isLogin(session.getId());
		
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setHttp10Compatible(false);

		
		if (!isAlreadyLogIn) {
		    session.invalidate();
		    session = request.getSession();
		    logger.info("create new session. "+session.getId());
			redirectView.setUrl(samlIDP);
			logger.info("redircet to saml IDP: " + samlIDP);
			return redirectView;
		}else{
			//redirect to start page;
			String startPage = "/vims/index.html";
			redirectView.setUrl(startPage);
			logger.info("redircet to start page: " + startPage);
			return redirectView;
		}
	}

	@RequestMapping(value = "/vims", method = { RequestMethod.GET })
	public RedirectView index(HttpServletRequest request) {
		logger.info("enter vims " + request.getRequestURL());
		logger.info("sonarbox session: " + request.getSession().getId());
		String userId = authenticationService.getUserBySession(request.getSession().getId());
		logger.info("current user: " + userId);
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setHttp10Compatible(false);
		if (userId == null) {
			redirectView.setUrl("/vims/login");
		} else {
			redirectView.setUrl("/vims/index.html");
		}
		return redirectView;
		// if (userId == null) {
		// return "/vims/login";
		// } else {
		// return "/vims/test.html";
		// }

	}

	@RequestMapping(value = "/vims", method = { RequestMethod.POST })
	public RedirectView getSaml(HttpServletRequest request) {
		logger.info("get saml response call. sonarbox id: " + request.getSession().getId());
		String result = request.getParameter("SAMLResponse");
		AuthResponse authResponse = consumeSamlResponseService.consume(result);
		logger.info("saml user : " + authResponse.userName);
		authResponse.inResponseTo = request.getSession().getId();
		authenticationService.updateAuth(authResponse);
		RedirectView redirectView = new RedirectView();
		redirectView.setContextRelative(true);
		redirectView.setHttp10Compatible(false);
		redirectView.setUrl("/vims/index.html");
		return redirectView;
		// sessionService
		// return "/vims/test.html";
	}

	@RequestMapping(value = {"/app", "/app/**"}, method = {RequestMethod.GET })
	public String displayApp(HttpServletRequest request) {
		return "/index.html";
	}
}
