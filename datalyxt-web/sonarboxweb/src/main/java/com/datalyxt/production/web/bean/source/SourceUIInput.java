package com.datalyxt.production.web.bean.source;

import java.util.HashSet;

import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;

public class SourceUIInput {
	public String url;
	public HashSet<M_SourceType> types = new HashSet<>();
	public HashSet<LanguageISO_639_1> languages = new HashSet<>();
	public boolean autoDeploy = false;
	public boolean blacklist = false;
	public long id;
	public HashSet<String> dynamicParams = new HashSet<>();
	public M_SourceStatus status;
	public String name;
}
