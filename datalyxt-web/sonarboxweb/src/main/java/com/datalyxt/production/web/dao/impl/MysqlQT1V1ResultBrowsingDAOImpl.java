package com.datalyxt.production.web.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.web.bean.email.EmailDetail;
import com.datalyxt.production.web.bean.result.ScraperResultBean;
import com.datalyxt.production.web.dao.QT1V1ResultBrowsingDAO;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

@Repository("resultDAO")
public class MysqlQT1V1ResultBrowsingDAOImpl implements QT1V1ResultBrowsingDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlQT1V1ResultBrowsingDAOImpl.class);
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DataSource dataSource;

	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
		namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(
				jdbcTemplate);
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		return false;
	}

	@Override
	public void updateValidation(long id, int code, boolean isFile,
			boolean isSystem) throws M_QT1V1ResultDBException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<ScraperResultBean> getTextResults(String sql, Object[] params)
			throws M_QT1V1ResultDBException {
		try {
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			HashMap<Long, List<ActionResultText>> relatedTexts = new HashMap<>();
			List<ScraperResultBean> beans = new ArrayList<>();
			while (rows.next()) {
				ScraperResultBean scraperResultBean = new ScraperResultBean();
				scraperResultBean.id = rows.getLong("result_id");
				scraperResultBean.sourceId = rows.getLong("source_id");
				scraperResultBean.scanAt = rows.getLong("scan_id");
				scraperResultBean.url = rows.getString("n_url");
				scraperResultBean.sourceName = rows.getString("source_name");
				scraperResultBean.selectorId = rows.getLong("rule_id");
				scraperResultBean.selectorName = rows.getString("rule_name");
				scraperResultBean.hop = rows.getInt("hop");
				scraperResultBean.sourceUrl = rows.getString("source_url");

				ActionResultText actionResultText = new ActionResultText();
				actionResultText.id = rows.getLong("text_id");
				actionResultText.content = rows.getString("action_content");
				actionResultText.type = rows.getString("action_type");
				List<ActionResultText> list = relatedTexts
						.get(scraperResultBean.id);

				if (list != null) {
					list.add(actionResultText);
				} else {
					list = new ArrayList<>();
					list.add(actionResultText);
					beans.add(scraperResultBean);
				}
				relatedTexts.put(scraperResultBean.id, list);

			}
			for (ScraperResultBean b : beans)
				if (relatedTexts.containsKey(b.id))
					b.textResults = relatedTexts.get(b.id);
			return beans;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("Error in getTextResults : "
					+ e.getMessage(), e);
		}
	}

	@Override
	public List<ScraperResultBean> getFileResults(String sql, Object[] params)
			throws M_QT1V1ResultDBException {
		try {
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql, params);
			HashMap<Long, List<ActionResultFile>> relatedFiles = new HashMap<>();
			List<ScraperResultBean> beans = new ArrayList<>();
			while (rows.next()) {
				ScraperResultBean scraperResultBean = new ScraperResultBean();
				scraperResultBean.id = rows.getLong("result_id");
				scraperResultBean.sourceId = rows.getLong("source_id");
				scraperResultBean.scanAt = rows.getLong("scan_id");
				scraperResultBean.url = rows.getString("n_url");
				scraperResultBean.sourceName = rows.getString("source_name");
				scraperResultBean.selectorId = rows.getLong("rule_id");
				scraperResultBean.selectorName = rows.getString("rule_name");
				scraperResultBean.hop = rows.getInt("hop");
				scraperResultBean.sourceUrl = rows.getString("source_url");

				ActionResultFile actionResultFile = new ActionResultFile();
				actionResultFile.id = rows.getLong("file_id");
				actionResultFile.type = rows.getInt("action_type");

				List<ActionResultFile> list = relatedFiles
						.get(scraperResultBean.id);

				if (list != null) {
					list.add(actionResultFile);
				} else {
					list = new ArrayList<>();
					list.add(actionResultFile);
					beans.add(scraperResultBean);
				}
				relatedFiles.put(scraperResultBean.id, list);

			}
//			for(Long time : relatedFiles.keySet()){
//				System.out.println("time: "+time+" "+relatedFiles.get(time).size() );
//			}
			for (ScraperResultBean b : beans)
				if (relatedFiles.containsKey(b.id))
					b.fileResults = relatedFiles.get(b.id);
			return beans;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("Error in getFileResults : "
					+ e.getMessage(), e);
		}
	}

	@Override
	public void setActionResultFiles(EmailDetail emailDetail)
			throws M_QT1V1ResultDBException {

		try {

			if (emailDetail.attachments == null
					|| emailDetail.attachments.isEmpty())
				return;

			String sql = "select t1.id, t1.created_at, t1.result_id, t1.status, t1.host, t1.path, t1.name, t1.size, t1.type, t1.d_name, t2.n_url  from m_qt1v1_file t1 join m_qt1v1_result t2 on t1.result_id = t2.id where t1.id in (:fileids)  ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("fileids", emailDetail.attachments);

			SqlRowSet resultSet = namedParameterJdbcTemplate.queryForRowSet(
					sql, parameters);

			while (resultSet.next()) {
				ActionResultFile result = new ActionResultFile();
				result.id = resultSet.getLong("id");
				result.createdAt = resultSet.getLong("created_at");
				result.resultId = resultSet.getLong("result_id");
				result.status = resultSet.getInt("status");
				result.host = resultSet.getString("host");
				String path = resultSet.getString("path");
				result.path = path.split("/");
				result.name = resultSet.getString("name");
				result.size = resultSet.getInt("size");
				result.type = resultSet.getInt("type");
				result.dName = resultSet.getString("d_name");
				result.url = resultSet.getString("n_url");
				emailDetail.fileMeta.add(result);
			}
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(e.getMessage(), e);
		}

	}

	@Override
	public void setActionResultTexts(EmailDetail emailDetail)
			throws M_QT1V1ResultDBException {

		try {

			if (emailDetail.messageIds == null
					|| emailDetail.messageIds.isEmpty())
				return;

			String sql = "select t.id, t.content, t.type, result.n_url, m_source.name from m_qt1v1_text t join m_qt1v1_result result on t.result_id = result.id join m_source on m_source.id = result.source_id where t.id in (:msgs) ";

			MapSqlParameterSource parameters = new MapSqlParameterSource();
			parameters.addValue("msgs", emailDetail.messageIds);

			SqlRowSet resultSet = namedParameterJdbcTemplate.queryForRowSet(
					sql, parameters);

			while (resultSet.next()) {
				ActionResultText actionResultText = new ActionResultText();
				actionResultText.id = resultSet.getLong("id");
				actionResultText.content = resultSet.getString("content");
				actionResultText.type = resultSet.getString("type");

				emailDetail.actionResults.add(actionResultText);

				emailDetail.pageURLs.add(resultSet.getString("n_url"));
				emailDetail.domains.add(resultSet.getString("name"));
			}
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(e.getMessage(), e);
		}

	}

	@Override
	public ActionResultFile getFileResult(long id)
			throws M_QT1V1ResultDBException {
		try {
			String sql = "select * from m_qt1v1_file where id = ?";
			SqlRowSet resultSet = jdbcTemplate.queryForRowSet(sql, id);
			ActionResultFile result = null;
			while (resultSet.next()) {
				result = new ActionResultFile();
				result.id = resultSet.getLong("id");
				result.createdAt = resultSet.getLong("created_at");
				result.resultId = resultSet.getLong("result_id");
				result.status = resultSet.getInt("status");
				result.host = resultSet.getString("host");
				String path = resultSet.getString("path");
				result.path = path.split("/");
				result.name = resultSet.getString("name");
				result.size = resultSet.getInt("size");
				result.type = resultSet.getInt("type");
				result.dName = resultSet.getString("d_name");

			}

			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("Error in getFileResult : "
					+ e.getMessage(), e);
		}
	}

}
