package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.web.bean.ActivityQueryScript;
import com.datalyxt.production.web.bean.FilterBean;
import com.datalyxt.production.web.bean.PagingBean;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.RestServiceException;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ActivityService;
import com.datalyxt.production.web.service.AuthenticationService;
import com.datalyxt.production.web.service.ResponseUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("vims/sonarbox/res/activities")
@RestController
public class ActivityController {

	private static Logger logger = LoggerFactory.getLogger(ActivityController.class);

	@Autowired
	ActivityService activityService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Autowired
	private AuthenticationService authenticationService ;

	// List added Sources
	@RequestMapping(method = RequestMethod.GET)
	public Response listSources(Principal principal,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "filter", defaultValue = "") String filters,
			@RequestParam(value = "paging", defaultValue = "") String paging,
			
			@RequestParam(value = "start", defaultValue = "") String startTimestampFilter,
			@RequestParam(value = "end", defaultValue = "") String endTimestampFilter) {

		long userId = Long.valueOf(principal.getName());

		ActivityQueryScript queryscript = new ActivityQueryScript();
		
		logger.info("sort "+sort+" filter "+filters);

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;
			switch (toSwitch) {
				case "timestamp":
					queryscript.sortByTimestamp = true;
					break;
				case "objectType":
					queryscript.sortByObjectType = true;
					break;
				case "user":
					queryscript.sortByUsername= true;
					break;
				case "type":
					queryscript.sortByActivityType = true;
					break;
				default:
					throw new WrongParameterException(
							"Fehler: Unbekannte Sortierung");
			}
		}else 	queryscript.sortByTimestamp = true;

		// check filter values
		try {
			HashSet<M_ActivityType> type = new HashSet<>();
			HashSet<M_ObjectType> objectType = new HashSet<>();
			
			queryscript.typeFilter = type;
			queryscript.objectTypeFilter = objectType;
			
			
			if(!filters.isEmpty()){
				
				FilterBean[] filterBeans = jacksonObjectMapper.readValue(filters,
						FilterBean[].class);
				
				for (FilterBean s : filterBeans) {
					if(s.name.equals("type"))
						type.add(M_ActivityType.valueOf(s.value));
					else if(s.name.equals("activityobjectType"))
						objectType.add(M_ObjectType.valueOf(s.value));
				}

			}


			// lastModified
			if (!startTimestampFilter.isEmpty())
				queryscript.startTimestamp = Long.parseLong(startTimestampFilter);
			if (!endTimestampFilter.isEmpty())
				queryscript.endTimestamp = Long.parseLong(endTimestampFilter);
			
			
			logger.info("paging obj: " + paging);
			if (!paging.isEmpty()) {
				PagingBean pagingBean = jacksonObjectMapper.readValue(paging,
						PagingBean.class);
				queryscript.paging = pagingBean;
			}

		} catch (Exception iae) {
			throw new WrongParameterException("Error in filter values");
		}

		try {
			List<M_Activity> activities = activityService.list(queryscript);

			Response response = ResponseUtil.createResponse(userId, true, "");
			response.body = activities;

			return response;

		} catch(M_ActivityDBException db) {
			logger.error("could not load activities "+db.getMessage(), db);
			throw new InternalServerError("Error, could not load activities");
		}
	}

	@ExceptionHandler(RestServiceException.class)
	public ResponseEntity<Response> handleException(RestServiceException e) {
		long userId = 1L;

		return new ResponseEntity<>(ResponseUtil.createResponse(userId, false, e.getMessage()), e.getStatus());
	}

}
