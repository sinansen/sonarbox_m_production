package com.datalyxt.production.web.bean.report;

import java.util.HashSet;

import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.web.bean.PagingBean;

public class ReportQueryScript {

	public boolean desc = true;
	public boolean sortByScanEnd = false;
	public boolean sortByScanStart = false;
	public boolean sortBySelectorName = false;
	public boolean sortBySource =false;
	public long startTime;
	public long endTime;
	public String searchTerm;
	public HashSet<ReportType> types = new HashSet<>();
	public PagingBean paging;
	public int status;

}
