package com.datalyxt.production.web.exception;


import org.springframework.http.HttpStatus;

/**
 * Created by michael on 6/12/16.
 */
public abstract class RestServiceException extends RuntimeException {
    public RestServiceException() { super(); }
    public RestServiceException(String msg) { super(msg); }

    abstract public HttpStatus getStatus();
}
