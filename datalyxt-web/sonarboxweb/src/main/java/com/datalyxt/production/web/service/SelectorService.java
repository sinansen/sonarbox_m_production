package com.datalyxt.production.web.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.datalyxt.exception.db.DataBaseException;
//import com.datalyxt.jcc.dql.DQLParser;
//import com.datalyxt.jcc.dql.ParseException;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.exception.webscraper.BlockConfigValidationException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.selector.RuleQueryScript;
import com.datalyxt.production.web.bean.selector.RuleTemplateQueryScript;
import com.datalyxt.production.web.bean.selector.RuleUpdateScript;
import com.datalyxt.production.web.bean.selector.SelectorBean;
import com.datalyxt.production.web.bean.selector.TemplateBean;
import com.datalyxt.production.web.dao.SelectorBeanDAO;
import com.datalyxt.production.web.dao.helper.QueryWrapper;
import com.datalyxt.production.web.dao.helper.RuleQueryBuilder;
import com.datalyxt.production.web.dao.helper.RuleTemplateQueryBuilder;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.webdatabase.dao.FilterOperatorDAO;
import com.datalyxt.production.webdatabase.dao.M_SourceAdminDAO;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.rule.QT1_V2_RuleContent;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.service.QT1V1ConfigLiveCheckService;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.production.webscraper.util.QT1V1ConfigValidationUtil;
import com.datalyxt.util.ValidLink;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.util.url.URLNormalizer;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SelectorService {

	private static Logger logger = LoggerFactory
			.getLogger(SelectorService.class);

	@Autowired
	SelectorBeanDAO ruleDAO;
	@Autowired
	M_SourceAdminDAO sourceDAO;
	@Autowired
	FilterOperatorDAO filterOperatorDAO;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	public Response createRule(M_Rule rule, long userId) {
		logger.info("create selector: " + rule.ruleType);
		try {
			Response res = checkRuleInput(rule, userId);
			if (!res.success) {
				return res;
			}

			rule.status = M_RuleStatus.added;
			rule.createdAt = System.currentTimeMillis();
			rule.lastModified = rule.createdAt;
			rule.ownerId = userId;

			if (ruleDAO.isRuleExsits(rule)) {
				res = createResponse(userId, false,
						"Selector existiert bereits");
				return res;
			}

			ruleDAO.addRule(rule);

			res = createResponse(userId, true, "Selector hinzugefügt");

			return res;
		} catch (Exception e) {
			logger.error(
					"exception in creating rule in receiver db: "
							+ e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Hinzufügen");
			return res;
		}

	}

	private Response checkRuleInput(M_Rule rule, long userId) {
		if (rule.ruleType == null) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch: Type ist null");
			return res;
		}
		if (rule.ruleContent == null) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch: Rule-Content ist null");
			return res;
		}

		if (rule.ruleType.equals(M_RuleType.QT1V1)) {
			QT1_V1_RuleContent content = (QT1_V1_RuleContent) rule.ruleContent;
			if (rule.sourceIds.isEmpty()) {
				Response res = createResponse(userId, false,
						"Quelle ist nicht definiert");
				return res;
			}
			HashSet<String> operators = null;
			try {
				operators = filterOperatorDAO.getOperators();
			} catch (DataBaseException e1) {
				Response res = createResponse(userId, false,
						"Quelle validation failed");
				return res;
			}
			QT1V1ConfigValidationUtil qt1v1ConfigValidateService = new QT1V1ConfigValidationUtil(
					operators);
			try {
				qt1v1ConfigValidateService.validate(content.blockConfigMain,
						false);
			} catch (Exception e) {
				Response res = createResponse(userId, false,
						"Selector Inhalt ist falsch: " + e.getMessage());
				return res;
			}
			String ruleContentJson;
			try {
				ruleContentJson = jacksonObjectMapper
						.writeValueAsString(content);
				rule.ruleContent.contentId = DigestUtils
						.md5Hex(ruleContentJson);
			} catch (JsonProcessingException e) {
				Response res = createResponse(userId, false,
						"Selector Inhalt ist falsch: " + e.getMessage());
				return res;
			}

		} else if (rule.ruleType.equals(M_RuleType.QT1V2)) {
			QT1_V2_RuleContent content = (QT1_V2_RuleContent) rule.ruleContent;
			if (content.searchRule == null || content.searchRule.isEmpty()) {
				Response res = createResponse(userId, false,
						"Selector Inhalt ist falsch");
				return res;
			}

			// try {
			// DQLParser parser = new DQLParser(content.searchRule);
			// parser.parse();
			// } catch (ParseException e) {
			// Response res = createResponse(userId, false,
			// "Selector Inhalt ist falsch");
			// return res;
			// }

			rule.ruleContent.contentId = DigestUtils.md5Hex(content.searchRule);

		}

		if (rule.sourceIds.isEmpty()) {
			Response res = createResponse(userId, false,
					"Selector muss mit midestens eine Quelle verbinden. ");
			return res;
		}

		return createResponse(userId, true, "check ist durch");
	}

	// list Rule
	public Response listRule(RuleQueryScript script, long userId) {
		try {

			QueryWrapper wrapper = RuleQueryBuilder
					.buildQueryForRelatedSourceSearch(script);
			List<SelectorBean> results = ruleDAO.getSelectorBeans(wrapper.sql,
					wrapper.params, userId);

			Response res;
			if (results.isEmpty())
				res = createResponse(userId, true, "Selector nicht gefunden");
			else {
				res = createResponse(userId, true, "Selector gefunden");

			}

			res.body = results;
			return res;
		} catch (Exception e) {
			logger.error(
					"exception in get rules from rule db: " + e.getMessage(), e);
			Response res = createResponse(userId, false, "Fehler beim Suchen");
			return res;
		}

	}

	public Response createResponse(long userId, boolean success, String msg) {
		Response res = new Response();
		res.userid = userId;
		res.success = success;
		res.message = msg;
		return res;
	}

	public M_Rule convertScriptToRule(RuleUpdateScript script) {
		M_Rule rule = new M_Rule();
		rule.id = script.ruleId;
		if (script.qt1_V1_RuleContent != null)
			rule.ruleContent = script.qt1_V1_RuleContent;
		if (script.qt1_V2_RuleContent != null)
			rule.ruleContent = script.qt1_V2_RuleContent;
		if (script.ruleName != null)
			rule.name = script.ruleName;
		if (script.ruleType != null)
			rule.ruleType = script.ruleType;
		if (script.status != null)
			rule.status = script.status;
		if (!script.sourceIds.isEmpty())
			rule.sourceIds.addAll(script.sourceIds);
		return rule;
	}

	public Response modifyRule(RuleUpdateScript script,
			boolean deleteAllMapping, long userId) {
		try {
			M_Rule oldRule = ruleDAO.getRuleByRuleId(script.ruleId, userId);
			if (oldRule == null) {
				Response res = createResponse(userId, false,
						"Selector existiert nicht");
				return res;
			}

			M_Rule rule = convertScriptToRule(script);

			Response res = checkRuleInput(rule, userId);

			if (!res.success)
				return res;

			rule.lastModified = System.currentTimeMillis();
			rule.ownerId = userId;

			String ruleContentJson = null;
			if (rule.ruleContent != null)
				ruleContentJson = jacksonObjectMapper
						.writeValueAsString(rule.ruleContent);

			QueryWrapper wrapper = RuleQueryBuilder.buildQueryForModify(rule,
					ruleContentJson);

			if (wrapper.params == null || wrapper.params.length == 0) {
				res = createResponse(userId, true,
						"Keine Parameter zu aktualisieren");
				return res;
			}

			int changed = ruleDAO.modifyRule(wrapper.sql, wrapper.params);

			// update mapping
			if (!script.sourceIds.isEmpty()) {
				ruleDAO.modifyRuleSourceMapping(rule);
			}

			if (deleteAllMapping) {
				ruleDAO.deleteRuleSourceMapping(oldRule.sourceIds, userId,
						script.ruleId);
			} else {
				if (!script.sourceIds.isEmpty()) {
					Collection<Long> tobeDeletedSourceIds = CollectionUtils
							.subtract(oldRule.sourceIds, script.sourceIds);
					if (!tobeDeletedSourceIds.isEmpty()) {
						ruleDAO.deleteRuleSourceMapping(tobeDeletedSourceIds,
								userId, script.ruleId);
					}
				}
			}

			M_Rule modified = ruleDAO.getRuleByRuleId(script.ruleId, userId);

			if (changed > 0) {
				res = createResponse(userId, true,
						"Selector wurde aktualisiert");
				res.body = modified;

				return res;
			} else {
				res = createResponse(userId, true,
						"Keine Parameter zu aktualisieren");
				return res;
			}
		} catch (Exception e) {
			logger.error("exception in modify rule: " + e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Modifizieren");
			return res;
		}

	}

	// private Response checkModifyRuleInput(M_Rule rule, long userId) {
	//
	// if (rule.id <= 0) {
	// Response res = createResponse(userId, false,
	// "Parameter sind falsch");
	// return res;
	// }
	//
	// // modify rule content
	// if (rule.ruleContent != null) {
	//
	// // rule type must have value!!
	// if (rule.ruleType == null) {
	// Response res = createResponse(userId, false,
	// "Parameter sind falsch");
	// return res;
	// }
	//
	// if (rule.ruleType.equals(M_RuleType.QT1V1)) {
	// QT1_V1_RuleContent content = (QT1_V1_RuleContent) rule.ruleContent;
	//
	// if (rule.sourceIds.isEmpty()) {
	// Response res = createResponse(userId, false,
	// "Quelle ist nicht definiert");
	// return res;
	// }
	//
	// HashSet<String> operators = null;
	// try {
	// operators = filterOperatorDAO.getOperators();
	// } catch (DataBaseException e1) {
	// Response res = createResponse(userId, false,
	// "Quelle validation failed");
	// return res;
	// }
	// QT1V1ConfigValidationUtil qt1v1ConfigValidateService = new
	// QT1V1ConfigValidationUtil(
	// operators);
	// try {
	// qt1v1ConfigValidateService.validate(
	// content.blockConfigMain, false);
	// } catch (Exception e) {
	// Response res = createResponse(userId, false,
	// "Selector Inhalt ist falsch: " + e.getMessage());
	// return res;
	// }
	//
	// String ruleContentJson;
	// try {
	// ruleContentJson = jacksonObjectMapper
	// .writeValueAsString(content);
	// rule.ruleContent.contentId = DigestUtils
	// .md5Hex(ruleContentJson);
	// } catch (JsonProcessingException e) {
	// Response res = createResponse(userId, false,
	// "Selector Inhalt ist falsch: " + e.getMessage());
	// return res;
	// }
	//
	// } else if (rule.ruleType.equals(M_RuleType.QT1V2)) {
	// QT1_V2_RuleContent content = (QT1_V2_RuleContent) rule.ruleContent;
	// if (content.searchRule == null || content.searchRule.isEmpty()) {
	// Response res = createResponse(userId, false,
	// "Selector Inhalt ist falsch");
	// return res;
	// }
	//
	// // try {
	// // DQLParser parser = new DQLParser(content.searchRule);
	// // parser.parse();
	// // } catch (ParseException e) {
	// // Response res = createResponse(userId, false,
	// // "Selector Inhalt ist falsch");
	// // return res;
	// // }
	//
	// rule.ruleContent.contentId = DigestUtils
	// .md5Hex(content.searchRule);
	//
	// }
	// }
	//
	// return createResponse(userId, true, "check ist durch");
	// }

	@Async
	public Future<Response> checkSelectorLive(long ruleId, long ownerId)
			throws InterruptedException {
		logger.info("Validate qt1v1 selctor ");

		Response response = new Response();
		QT1V1ConfigLiveCheckService qt1v1ConfigLiveCheckService = new QT1V1ConfigLiveCheckService();
		try {
			WebDriver driver;
			if (isWindows())
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_windows);
			else
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_unix);
			M_Rule rule = ruleDAO.getRuleByRuleId(ruleId, ownerId);
			BlockConfigMain mainConfig = ((QT1_V1_RuleContent) rule.ruleContent).blockConfigMain;
			HashMap<String, List<Block>> results = new HashMap<>();

			HashMap<Integer, HashSet<String>> tobeChecked = new HashMap<>();

			for (PageBlockConfig pageBlockConfig : mainConfig.config) {
				HashSet<String> urls = tobeChecked.get(pageBlockConfig.hop);
				if (urls == null)
					urls = new HashSet<>();
				urls.add(pageBlockConfig.url);
				System.out
						.println(pageBlockConfig.hop + "  " + urls.toString());
				tobeChecked.put(pageBlockConfig.hop, urls);

			}
			for (Integer hop : tobeChecked.keySet()) {
				HashSet<String> urls = tobeChecked.get(hop);

				CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();

				for (String url : urls) {
					logger.info("live check " + hop + " " + url);
					List<Block> extracted = qt1v1ConfigLiveCheckService
							.checkConfig(driver, mainConfig, script, url, hop);
					logger.info("live check " + url + " finished. ");
					results.put(url, extracted);
				}
			}

			driver.close();
			response = createResponse(ownerId, true, "live check finished.");
			logger.info("live check finished. found pageresult "
					+ results.size());
			response.body = results;
		} catch (Exception e) {
			logger.error("exception in validate qt1v1 rule: " + e.getMessage(),
					e);
			response = createResponse(ownerId, false,
					"Error beim qt1v1 Selektor Validieren");
		}

		return new AsyncResult<Response>(response);

	}

	public void updateRuleStatus(long ruleId, M_RuleStatus status, long ownerId) {
		long lastModified = System.currentTimeMillis();

		try {
			ruleDAO.changeRuleStatus(ruleId, status, lastModified);
		} catch (M_RuleDBException e) {
			logger.error("update rule status failed. " + e.getMessage(), e);
			throw new InternalServerError("update rule status failed.");
		}

	}

	public void validateSelectorSyntax(BlockConfigMain confMain) {
		HashSet<String> operators;
		try {
			operators = filterOperatorDAO.getOperators();
			QT1V1ConfigValidationUtil qt1v1ConfigValidationUtil = new QT1V1ConfigValidationUtil(
					operators);
			qt1v1ConfigValidationUtil.validate(confMain, true);
		} catch (DataBaseException | BlockConfigValidationException
				| UnknownBlockTypeException e) {
			logger.error("validate selector syntax failed. " + e.getMessage(),
					e);
			throw new WrongParameterException(
					"validate selector syntax failed.");

		}

	}

	public void updateSelector(long ruleId, BlockConfigMain confMain,
			long ownerId) {
		try {
			ruleDAO.updateSelector(ruleId, confMain, ownerId);
		} catch (M_RuleDBException e) {
			logger.error("update selector failed. " + e.getMessage(), e);
			throw new InternalServerError("update selector failed.");
		}
	}

	@Async
	public Future<Response> normalizeSelector(long ruleId, long ownerId) {
		Response response = new Response();
		QT1V1ConfigService qt1v1ConfigService = new QT1V1ConfigService();
		try {
			WebDriver driver;
			if (isWindows())
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_windows);
			else
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_unix);
			M_Rule rule = ruleDAO.getRuleByRuleId(ruleId, ownerId);
			BlockConfigMain confMain = ((QT1_V1_RuleContent) rule.ruleContent).blockConfigMain;

			BlockConfigMain ncfg = qt1v1ConfigService.getNormalizedMainConfig(
					confMain, driver);
			updateSelector(ruleId, ncfg, ownerId);

			driver.close();
			response = createResponse(ownerId, true,
					"normalizing selector finished.");
		} catch (Exception e) {
			logger.error("normalize selector failed. " + e.getMessage(), e);
			throw new InternalServerError("normalize selector failed. ");
		}
		return new AsyncResult<Response>(response);
	}

	private static String OS = System.getProperty("os.name").toLowerCase();

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public void validatePageBlockConfig(PageBlockConfig conf) {
		HashSet<String> operators;
		try {
			operators = filterOperatorDAO.getOperators();
			QT1V1ConfigValidationUtil qt1v1ConfigValidationUtil = new QT1V1ConfigValidationUtil(
					operators);
			qt1v1ConfigValidationUtil.validatePage(conf, true);
		} catch (DataBaseException | BlockConfigValidationException
				| UnknownBlockTypeException e) {
			logger.error("validate selector syntax failed. " + e.getMessage(),
					e);
			throw new WrongParameterException(
					"validate selector syntax failed.");

		}

	}

	public Response createTemplate(TemplateBean template, long userId) {
		logger.info("create selector template ");
		try {

			
			Response res = checkTemplateInput(template, userId);
			if (!res.success) {
				return res;
			}

			template.status = M_RuleStatus.added;
			template.createdAt = System.currentTimeMillis();
			template.lastModified = template.createdAt;
			template.ownerId = userId;
			template.nurl = URLNormalizer.getNormalizedURL(template.nurl);
			template.md5 = DigestUtils.md5Hex(template.nurl+template.config);

			ruleDAO.addTemplate(template);

			res = createResponse(userId, true, "Selector Template hinzugefügt");

			return res;
		} catch (Exception e) {
			logger.error(
					"exception in creating Template in receiver db: "
							+ e.getMessage(), e);
			Response res = createResponse(userId, false,
					"Fehler beim Hinzufügen");
			return res;
		}
	}

	private Response checkTemplateInput(TemplateBean template, long userId) {
		if (template.config == null) {
			Response res = createResponse(userId, false,
					"Parameter sind falsch: Template-Content ist null");
			return res;
		}
		try {
			if (ValidLink.isEmptyURL(template.nurl))
				throw new BlockConfigValidationException(
						"page url is null or empty!");

			if (!ValidLink.isURLLengthValid(template.nurl, 500))
				throw new BlockConfigValidationException(
						"page url exceeds max length (500)!");

			if (!ValidLink.isHttp(template.nurl))
				throw new BlockConfigValidationException(
						"page url is not valid http or https.");

		} catch (Exception e) {
			Response res = createResponse(userId, false,
					"Selector URL ist ungültig. ");
			return res;
		}

		return createResponse(userId, true, "check ist durch");
	}

	public Response listRuleTemplates(RuleTemplateQueryScript queryscript,
			long userId) {
		try {

			QueryWrapper wrapper = RuleTemplateQueryBuilder
					.buildQueryForSearch(queryscript);
			List<TemplateBean> results = ruleDAO.getSelectorTemplateBeans(wrapper.sql,
					wrapper.params, userId);

			Response res;
			if (results.isEmpty())
				res = createResponse(userId, true, "Selector nicht gefunden");
			else {
				res = createResponse(userId, true, "Selector gefunden");

			}

			res.body = results;
			return res;
		} catch (Exception e) {
			logger.error(
					"exception in get rules from rule db: " + e.getMessage(), e);
			Response res = createResponse(userId, false, "Fehler beim Suchen");
			return res;
		}

	}

}
