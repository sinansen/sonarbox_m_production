package com.datalyxt.production.web.restfulws;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;
import com.datalyxt.production.log.M_ActivityType;
import com.datalyxt.production.log.M_ObjectType;
import com.datalyxt.production.web.bean.Response;
import com.datalyxt.production.web.bean.source.SourceQueryScript;
import com.datalyxt.production.web.bean.source.SourceStatusInput;
import com.datalyxt.production.web.bean.source.SourceUIBean;
import com.datalyxt.production.web.bean.source.SourceUIInput;
import com.datalyxt.production.web.bean.source.SourceValidationResponse;
import com.datalyxt.production.web.bean.source.SourceValidationScript;
import com.datalyxt.production.web.exception.InternalServerError;
import com.datalyxt.production.web.exception.ResourceNotFoundException;
import com.datalyxt.production.web.exception.RestServiceException;
import com.datalyxt.production.web.exception.WrongParameterException;
import com.datalyxt.production.web.service.ActivityService;
import com.datalyxt.production.web.service.AuthenticationService;
import com.datalyxt.production.web.service.ResponseUtil;
import com.datalyxt.production.web.service.SourceService;
import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.M_SourceType;
import com.datalyxt.production.webmodel.source.QT1V1SourceConfig;
import com.datalyxt.production.webmodel.source.QT1V2SourceConfig;
import com.datalyxt.util.ValidLink;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@RequestMapping("vims/sonarbox/res/sources")
@RestController
public class SourceController {

	private static Logger logger = LoggerFactory
			.getLogger(SourceController.class);

	@Autowired
	SourceService sourceService;

	@Autowired
	ActivityService activityService;

	@Autowired
	private ObjectMapper jacksonObjectMapper;

	@Autowired
	private AuthenticationService authenticationService;

	// Create a Source
	@ResponseStatus(HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST)
	public Response createSource(Principal principal, @RequestBody String sourceInputJson) {

		long userId = Long.valueOf(principal.getName());

		SourceUIInput sourceInput;
		try {
			sourceInput = jacksonObjectMapper.readValue(sourceInputJson,
					SourceUIInput.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new WrongParameterException(
					"Fehler: Falsche Formatierung der Inputparameter");
		}

		logger.info("Creating Source " + sourceInput.url);

		// check necessary parameters
		if (sourceInput.url == null || ValidLink.isEmptyURL(sourceInput.url)
				|| !ValidLink.isHttp(sourceInput.url)
				|| !ValidLink.isURLLengthValid(sourceInput.url, 500)) {
			throw new WrongParameterException("Fehler: Url ist fehlerhaft");
		}

		if (!sourceInput.blacklist) {
			if (sourceInput.types == null || sourceInput.types.isEmpty()) {
				throw new WrongParameterException(
						"Fehler: Kein Quellen Typ angegeben.");
			}

			if (sourceInput.languages == null
					|| sourceInput.languages.isEmpty()) {
				throw new WrongParameterException(
						"Fehler: Keine Sprache angegeben ");
			}
		}
		if (sourceInput.name == null || sourceInput.name.equals("")) {
			throw new WrongParameterException(
					"Fehler: Kein Quellen Name angegeben");
		}

		M_Source source = new M_Source();
		source.sourceUrl = sourceInput.url;
		source.sourceName = sourceInput.name;
		source.sourceTypes.addAll(sourceInput.types);
		if (sourceInput.blacklist)
			source.sourceStatus = M_SourceStatus.blackList;
		try {
			for (M_SourceType type : sourceInput.types) {
				if (type.equals(M_SourceType.QT1V1)) {
					source.sourceConfig.qt1v1SourceConfig = new QT1V1SourceConfig();
					source.sourceConfig.qt1v1SourceConfig.languages.clear();
					source.sourceConfig.qt1v1SourceConfig.languages
							.addAll(sourceInput.languages);
				} else if (type.equals(M_SourceType.QT1V2)) {
					source.sourceConfig.qt1v2SourceConfig = new QT1V2SourceConfig();
					source.sourceConfig.qt1v2SourceConfig.languages.clear();
					source.sourceConfig.qt1v2SourceConfig.languages
							.addAll(sourceInput.languages);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new WrongParameterException(
					"Fehler: Die angegebenen Sprachparameter sind falsch");
		}

		SourceUIBean sourceRepr = sourceService.createSource(source, userId);

		try {
			// insert created activity
			M_Activity activity = new M_Activity(M_ActivityType.created,
					sourceRepr.id, M_ObjectType.source);
			activity.userId = userId;
			activity.content = sourceInputJson;
			activityService.save(activity);

			if (!source.sourceStatus.equals(M_SourceStatus.blackList)) {
				SourceValidationScript sourceScript = new SourceValidationScript();
				sourceScript.basicHTTPCheck = true;
				sourceScript.normSourceURL = source.sourceUrlNormalized;
				sourceScript.updateSourceInDB = true;
				sourceScript.sourceUrl = source.sourceUrl;
				sourceScript.urlSyntaxCheck = false;
				sourceScript.source = source;
				sourceScript.autoDeploy = sourceInput.autoDeploy;
				sourceService.validateSource(sourceScript, userId);

				Gson gson = new Gson();
				String activityContentJson = gson.toJson(sourceScript,
						SourceValidationScript.class);

				// insert validation activity
				activity = new M_Activity(M_ActivityType.validated,
						sourceRepr.id, M_ObjectType.source);
				activity.content = activityContentJson;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException ae) {
			throw new InternalServerError(
					"Fehler: Speichern der Quellenaktivität fehlgeschlagen");
		} catch (InterruptedException ie) {
			throw new InternalServerError(
					"Fehler: Validierung der Url fehlgeschlagen");
		}

		// return response
		Response response = ResponseUtil.createResponse(userId, true,
				"Quelle erfolgreich erstellt");
		response.body = sourceRepr;

		return response;

	}

	// Update source config
	@RequestMapping(value = "/configs", method = RequestMethod.POST)
	public Response updateSourceConfig(Principal principal, @RequestBody String sourceJson) {

		long userId = Long.valueOf(principal.getName());

		M_Source source;
		try {
			source = jacksonObjectMapper.readValue(sourceJson, M_Source.class);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new WrongParameterException();
		}

		logger.info("Updating Source Config" + source.sourceId);

		Response res = sourceService.updateSourceConfig(source, userId);
		return res;
	}

	// Modify source
	@RequestMapping(method = RequestMethod.PUT)
	public Response modifySource(Principal principal, @RequestBody SourceUIBean sourceInput) {
		long userId = Long.valueOf(principal.getName());


		logger.info("Source Id for update: " + sourceInput.id);

		M_Source oldSource = sourceService.getSourceByID(sourceInput.id);

		if (oldSource == null) {
			throw new ResourceNotFoundException(sourceInput.id);
		}

		M_Source newSource = new M_Source();
		newSource.sourceId = oldSource.sourceId;
		newSource.sourceConfig = oldSource.sourceConfig;
		newSource.sourceUrl = sourceInput.url;
		newSource.sourceStatus = oldSource.sourceStatus;
		newSource.sourceName = sourceInput.name;
		newSource.sourceTypes.addAll(sourceInput.types);

		for (M_SourceType type : (sourceInput.types)) {
			if (type.equals(M_SourceType.QT1V1)) {
				if (oldSource.sourceConfig.qt1v1SourceConfig == null)
					newSource.sourceConfig.qt1v1SourceConfig = new QT1V1SourceConfig();

				newSource.sourceConfig.qt1v1SourceConfig.languages.clear();
				newSource.sourceConfig.qt1v1SourceConfig.languages
						.addAll(sourceInput.languages);

				newSource.sourceConfig.qt1v1SourceConfig.dynamicParameters
						.clear();
				newSource.sourceConfig.qt1v1SourceConfig.dynamicParameters
						.addAll(sourceInput.dynamicParams);
			} else if (type.equals(M_SourceType.QT1V2)) {
				if (oldSource.sourceConfig.qt1v2SourceConfig == null)
					newSource.sourceConfig.qt1v2SourceConfig = new QT1V2SourceConfig();
				newSource.sourceConfig.qt1v2SourceConfig.languages.clear();
				newSource.sourceConfig.qt1v2SourceConfig.languages
						.addAll(sourceInput.languages);
			}
			newSource.sourceTypes.add(type);
		}

		Response res = sourceService.modifySource(oldSource, newSource, userId);

		try {
			// insert modify activity
			M_Activity activity = new M_Activity(M_ActivityType.modified,
					newSource.sourceId, M_ObjectType.source);
			activity.userId = userId;
			activity.content = jacksonObjectMapper
					.writeValueAsString(sourceInput);
			activityService.save(activity);

			if (!M_SourceStatus.blackList.equals(newSource.sourceStatus)
					&& !oldSource.sourceUrl.equals(newSource.sourceUrl)) {
				SourceValidationScript sourceScript = new SourceValidationScript();
				sourceScript.basicHTTPCheck = true;
				sourceScript.normSourceURL = newSource.sourceUrlNormalized;
				sourceScript.updateSourceInDB = true;
				sourceScript.sourceUrl = newSource.sourceUrl;
				sourceScript.urlSyntaxCheck = false;
				sourceScript.source = newSource;
				sourceScript.autoDeploy = true;
				sourceScript.checkDynamicParameters = false;

				sourceService.validateSource(sourceScript, userId);
				Gson gson = new Gson();
				String activityContentJson = gson.toJson(sourceScript,
						SourceValidationScript.class);

				// insert validation activity
				activity = new M_Activity(M_ActivityType.validated,
						newSource.sourceId, M_ObjectType.source);
				activity.content = activityContentJson;
				activityService.save(activity);
			}
		} catch (M_ActivityDBException ae) {
			throw new InternalServerError(
					"Fehler: Speichern der Quellenaktivität fehlgeschlagen");
		} catch (InterruptedException ie) {
			throw new InternalServerError(
					"Fehler: Validierung der Url fehlgeschlagen");
		} catch (JsonProcessingException je) {
			throw new InternalServerError("Fehler: Json Parsing fehlgeschlagen");
		}

		return res;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Response delete(Principal principal, @PathVariable("id") long id) {
		logger.info("delete Source " + id);
		long userId = Long.valueOf(principal.getName());

		Response res = sourceService.delete(id, userId);

		try {
			M_Activity activity = new M_Activity(M_ActivityType.deleted, id,
					M_ObjectType.source);
			activityService.save(activity);
		} catch (M_ActivityDBException ae) {
			throw new InternalServerError(
					"Fehler: Speichern der Quellenaktivität fehlgeschlagen");
		}

		return res;
	}

	// change source status
	@RequestMapping(value = "{id}/status", method = RequestMethod.PUT)
	public Response changeSourceStatus(Principal principal, @PathVariable("id") long id,
			@RequestBody SourceStatusInput statusInput) {

		logger.info("Change Source status for " + id);
		long userId = Long.valueOf(principal.getName());


		// change source status
		try {
			M_Source source = sourceService.getSourceByID(id);
			List<M_SourceStatus> enabled = sourceService
					.getEnabledStatus(source.sourceStatus);

			if (!enabled.contains(statusInput.status)) {
				throw new WrongParameterException(
						"Fehler, Diese Modifizierung des Statuses ist nicht möglich");
			}

			sourceService.changeSourceStatus(id, statusInput.status, userId);

			// log activity
			M_ActivityType type;

			switch (statusInput.status) {
			case activated:
				type = M_ActivityType.activated;
				break;
			case paused:
				type = M_ActivityType.paused;
				break;
			default:
				throw new WrongParameterException(
						"Fehler, Diese Modifizierung des Statuses ist nicht möglich");

			}

			M_Activity activity = new M_Activity(type, source.sourceId,
					M_ObjectType.source);
			activity.userId = userId;
			activityService.save(activity);

		} catch (Exception e) {
			throw new InternalServerError(
					"Fehler, Änderung des Quellenstatuses fehlgeschlagen");
		}

		// return response
		return ResponseUtil.createResponse(userId, true,
				"Quellen Status erfolgreich verändert");
	}

	// List added Sources
	@RequestMapping(method = RequestMethod.GET)
	public Response listSources(
			Principal principal, 
			@RequestParam(value = "q", defaultValue = "") String searchTerm,
			@RequestParam(value = "sort", defaultValue = "") String sort,
			@RequestParam(value = "status", defaultValue = "") String statusFilter,
			@RequestParam(value = "languages", defaultValue = "") String languagesFilter,
			@RequestParam(value = "types", defaultValue = "") String typesFilter,
			@RequestParam(value = "start", defaultValue = "") String startLastModifiedFilter,
			@RequestParam(value = "end", defaultValue = "") String endLastModifiedFilter) {

		logger.info("list Source");

		long userId = Long.valueOf(principal.getName());

		SourceQueryScript queryscript = new SourceQueryScript();

		// check sorting
		if (!sort.isEmpty()) {
			// check sort direction
			queryscript.desc = sort.charAt(0) == '-';
			String toSwitch = queryscript.desc ? sort.substring(1) : sort;

			switch (toSwitch) {
			case "name":
				queryscript.sortByName = true;
				break;
			case "lastModified":
				queryscript.sortByModified = true;
				break;
			case "domain":
				queryscript.sortByDomain = true;
				break;
			case "created":
				queryscript.sortByCreated = true;
				break;
			default:
				throw new WrongParameterException(
						"Fehler: Unbekannte Sortierung");
			}
		}

		// check filter values
		try {

			// statusFilter
			if (!statusFilter.isEmpty()) {
				String[] statusParsed = statusFilter.split(", ");

				HashSet<M_SourceStatus> status = new HashSet<>();

				for (String s : statusParsed) {
					status.add(M_SourceStatus.valueOf(s));
				}

				queryscript.status = status;
			}

			// languagesFilter
			if (!languagesFilter.isEmpty()) {
				String[] languagesParsed = languagesFilter.split(", ");
				HashSet<LanguageISO_639_1> languages = new HashSet<>();

				for (String s : languagesParsed) {
					languages.add(LanguageISO_639_1.valueOf(s));
				}

				queryscript.languages = languages;
			}

			// typesFilter
			if (!typesFilter.isEmpty()) {
				String[] typesParsed = typesFilter.split(", ");
				HashSet<M_SourceType> types = new HashSet<>();

				for (String s : typesParsed) {
					types.add(M_SourceType.valueOf(s));
				}

				queryscript.types = types;
			}

			// lastModified
			if (!startLastModifiedFilter.isEmpty())
				queryscript.startTimeModified = Long
						.parseLong(startLastModifiedFilter);
			if (!endLastModifiedFilter.isEmpty())
				queryscript.endTimeModified = Long
						.parseLong(endLastModifiedFilter);

		} catch (IllegalArgumentException iae) {
			throw new WrongParameterException(
					"Fehler: Angegebene Filter sind falsch");
		}

		// check searchTerm
		if (!searchTerm.isEmpty())
			queryscript.searchTerm = searchTerm;

		return sourceService.listSources(queryscript, userId);
	}

	// List enabled Source Status
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	public List<M_SourceStatus> listSourceStatus(
			@RequestParam(value = "status") String status) {
		logger.info("list Source Status");
		List<M_SourceStatus> statusList = new ArrayList<M_SourceStatus>();
		if (status == null || status.isEmpty())
			return statusList;

		M_SourceStatus sourceStatus = null;

		try {
			sourceStatus = M_SourceStatus.valueOf(status);
		} catch (Exception e) {
			logger.error(
					"service liststatus: parameter sind falsch. "
							+ e.getMessage(), e);
			return statusList;
		}

		statusList = sourceService.getEnabledStatus(sourceStatus);

		return statusList;
	}

	// validate source url
	@RequestMapping(value = "/validate", method = RequestMethod.POST)
	public Response validateSource(Principal principal, 
			@RequestBody SourceValidationScript sourceScript) {
		logger.info("Validate source " + sourceScript.sourceUrl);
		long userId = Long.valueOf(principal.getName());

		Response res = new Response();
		res.userid = 1L;
		res.success = false;
		res.message = "";
		try {
			Future<SourceValidationResponse> futureResult = sourceService
					.validateSource(sourceScript, userId);
			SourceValidationResponse result = futureResult.get(60,
					TimeUnit.SECONDS);
			res.message = "status code: " + result.statuscode;
			res.success = true;

			M_ActivityType activityType = M_ActivityType.validated;
			Gson gson = new Gson();
			String activityContentJson = gson.toJson(sourceScript,
					SourceValidationScript.class);

			return res;
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			logger.error(
					"exception during validating source : " + e.getMessage(), e);
			res.message = "error in validating source: " + e.getMessage();
			res.success = false;
			return res;
		}
	}

	@ExceptionHandler(RestServiceException.class)
	public ResponseEntity<Response> handleException(Principal principal, RestServiceException e) {
		long userId = Long.valueOf(principal.getName());


		return new ResponseEntity<>(ResponseUtil.createResponse(userId, false,
				e.getMessage()), e.getStatus());
	}

}
