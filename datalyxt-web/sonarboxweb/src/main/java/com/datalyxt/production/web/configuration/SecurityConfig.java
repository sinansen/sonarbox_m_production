package com.datalyxt.production.web.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.datalyxt.production.web.security.StatelessAuthenticationFilter;
import com.datalyxt.production.web.service.AuthenticationService;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	AuthenticationService authService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// http.authorizeRequests().anyRequest().authenticated().and()
		// .httpBasic().and().csrf().disable();

		// http
		// .authorizeRequests()
		// .antMatchers("/sonarbox/user/login").permitAll()
		// .anyRequest().authenticated()
		// .and()
		// .httpBasic().and().csrf().disable();
		http.csrf().disable().headers().frameOptions().sameOrigin();

		// http.exceptionHandling().and().anonymous().and().servletApi().and()
		// .headers().cacheControl().and().httpStrictTransportSecurity();
		http.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/vims")
				.permitAll()
				.antMatchers("/vims/login")
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.addFilterBefore(
						new StatelessAuthenticationFilter(authService),
						UsernamePasswordAuthenticationFilter.class);
		//
		// // custom Token based authentication based on the header
		// // previously given to the client

		// http.addFilterBefore(new
		// StatelessLoginFilter("/login",tokenAuthenticationService,
		// userDetailsService, authenticationManager()),
		// UsernamePasswordAuthenticationFilter.class);
		//
		// // custom Token based authentication based on
		// // the header previously given to the client
		// http.addFilterBefore(new
		// StatelessAuthenticationFilter(tokenAuthenticationService),
		// UsernamePasswordAuthenticationFilter.class);

		// http
		// .sessionManagement()
		// .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}
}