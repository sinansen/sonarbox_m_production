package com.datalyxt.production.web.dao.helper;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.web.bean.selector.RuleActivityQueryScript;
import com.datalyxt.production.web.bean.selector.RuleQueryScript;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;

public class RuleQueryBuilder {
	public static QueryWrapper buildQueryForSearch(RuleQueryScript script) {
		String sqlBasePart = "select id, name, content_md5, content, status, owner_id, type, created_at, last_modified, feedback  from m_rules  ";
		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = "  name like ?  ";
		String conditionWithStatus = " status =  ";
		String conditionExcludeStatus = " status != ? ";
		String conditionWithStartTime = " last_modified >= ? ";
		String conditionWithEndTime = " last_modified <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = false;

		sql = sql + where + conditionExcludeStatus;
		params.add(M_RuleStatus.deleted.name());
		hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.status != null && !script.status.isEmpty()) {
			List<String> statusParams = new ArrayList<String>();
			for (M_RuleStatus s : script.status) {
				String param = conditionWithStatus + s.name() + " ";
				statusParams.add(param);
			}
			String parms = String.join(" or ", statusParams);
			parms = " ( " + parms + " ) ";
			if (hasWhere)
				sql = sql + combination + parms;
			else {
				sql = sql + where + parms;
				hasWhere = true;
			}
		}
		if (script.endTimeModified > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTimeModified));
			params.add(new Timestamp(script.endTimeModified));
		}

		sql = sql + orderBy;

		if (script.sortByCreated)
			sql = sql + " created_at ";
		else if (script.sortByName)
			sql = sql + " name ";
		else
			sql = sql + " last_modified ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;
	}

	public static QueryWrapper buildQueryForRelatedSourceSearch(
			RuleQueryScript script) {

		String sqlBasePart = "SELECT m.rule_id, r.name as rule_name, s.name as source_name, s.url, m.source_id , "
				+ " r.content, r.status , r.owner_id, r.type, r.created_at , r.last_modified, r.feedback "
				+ " from m_rules r "
				+ " JOIN m_rules_source_mapping m ON r.id = m.rule_id "
				+ " JOIN m_source s ON m.source_id = s.id ";

		String orderBy = " order by  ";
		String desc = " desc ";
		String asc = " asc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithSearchTerm = " ( r.name like ?  or s.name like ? or s.url like ? ) ";
		String conditionWithStatus = " r.status =  ";
		String conditionExcludeStatus = " r.status != ? ";
		String conditionWithStartTime = " r.last_modified >= ? ";
		String conditionWithEndTime = " r.last_modified <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();

		sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = false;

		sql = sql + where + conditionExcludeStatus;
		params.add(M_RuleStatus.deleted.name());
		hasWhere = true;

		if (script.searchTerm != null && !script.searchTerm.isEmpty()) {
			if (hasWhere) {
				sql = sql + combination + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
			} else {
				sql = sql + where + conditionWithSearchTerm;
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				params.add("%" + script.searchTerm + "%");
				hasWhere = true;
			}
		}

		if (script.status != null && !script.status.isEmpty()) {
			List<String> statusParams = new ArrayList<String>();
			for (M_RuleStatus s : script.status) {
				String param = conditionWithStatus + s.name() + " ";
				statusParams.add(param);
			}
			String parms = String.join(" or ", statusParams);
			parms = " ( " + parms + " ) ";
			if (hasWhere)
				sql = sql + combination + parms;
			else {
				sql = sql + where + parms;
				hasWhere = true;
			}
		}
		if (script.endTimeModified > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithStartTime + combination
						+ conditionWithEndTime;
			else {
				sql = sql + where + conditionWithStartTime + combination
						+ conditionWithEndTime;
				hasWhere = true;
			}

			params.add(new Timestamp(script.startTimeModified));
			params.add(new Timestamp(script.endTimeModified));
		}

		sql = sql + orderBy;

		if (script.sortByCreated)
			sql = sql + " created_at ";
		else if (script.sortByName)
			sql = sql + " rule_name ";
		else
			sql = sql + " last_modified ";

		if (script.desc)
			sql = sql + desc;
		else
			sql = sql + asc;

		wrapper.sql = sql;
		wrapper.params = params.toArray();
		return wrapper;
	}

	public static QueryWrapper buildQueryForModify(M_Rule rule,
			String ruleContentJson) {
		String sqlBasePart = "update  m_rules set ";
		String where = " where id = ? and owner_id = ? ";
		String combination = " , ";
		String updateName = " name = ?  ";
		String updateContent = "  content_md5 = ? , content =  ? ";
		String updateType = " type = ? ";
		String updateStatus = " status = ? ";
		String updateLastModify = " last_modified = ? ";
		boolean hasCombination = false;

		String sql = sqlBasePart;

		ArrayList<Object> params = new ArrayList<>();

		if (rule.name != null && !rule.name.isEmpty()) {
			sql = sql + updateName;
			hasCombination = true;

			params.add(rule.name);
		}

		if (rule.ruleType != null) {
			if (hasCombination)
				sql = sql + combination + updateType;
			else {
				sql = sql + updateType;
				hasCombination = true;
			}
			params.add(rule.ruleType.name());
		}

		if (rule.status != null) {
			if (hasCombination)
				sql = sql + combination + updateStatus;
			else {
				sql = sql + updateStatus;
				hasCombination = true;
			}
			params.add(rule.status.name());
		}

		if (rule.ruleContent != null) {
			if (hasCombination)
				sql = sql + combination + updateContent;
			else {
				sql = sql + updateContent;
				hasCombination = true;
			}
			params.add(rule.ruleContent.contentId);
			params.add(ruleContentJson);
		}

		QueryWrapper wrapper = new QueryWrapper();

		if (!params.isEmpty()) {
			sql = sql + combination + updateLastModify;

			params.add(new Timestamp(rule.lastModified));

			params.add(rule.id);
			params.add(rule.ownerId);

			sql = sql + where;

			Object[] array = params.toArray();

			wrapper.params = array;
			wrapper.sql = sql;

		}
		return wrapper;
	}

	public static QueryWrapper buildQueryForActivitySearch(
			RuleActivityQueryScript script) {
		String sqlBasePart = "select user_id, rule_id, activity, activity_time , activity_json from m_rules_activity  ";
		String defaultSortPart = " order by activity_time desc ";
		String where = " where ";
		String combination = " and ";
		String conditionWithUserId = " user_id = ? ";
		String conditionWithRuleId = "  rule_id = ? ";
		String conditionWithActivityType = " activity = ? ";
		String conditionWithStartTime = " activity_time >= ? ";
		String conditionWithEndTime = " activity_time <= ? ";

		String sql = sqlBasePart;

		QueryWrapper wrapper = new QueryWrapper();
		wrapper.sql = sql;

		ArrayList<Object> params = new ArrayList<>();

		boolean hasWhere = false;

		if (script.userId > 0) {
			sql = sql + where + conditionWithUserId;
			params.add(script.userId);
			hasWhere = true;
		}
		if (script.ruleId > 0) {
			if (hasWhere)
				sql = sql + combination + conditionWithRuleId;
			else {
				sql = sql + where + conditionWithRuleId;
				hasWhere = true;
			}

			params.add(script.ruleId);
		}

		if (script.activityType != null) {
			if (hasWhere)
				sql = sql + combination + conditionWithActivityType;
			else {
				sql = sql + where + conditionWithActivityType;
				hasWhere = true;
			}
			params.add(script.activityType);
		}

		if (hasWhere)
			sql = sql + combination + conditionWithStartTime + combination
					+ conditionWithEndTime;
		else {
			sql = sql + where + conditionWithStartTime + combination
					+ conditionWithEndTime;
			hasWhere = true;
		}

		params.add(script.startTime);
		params.add(script.endTime);

		sql = sql + defaultSortPart;

		wrapper.sql = sql;
		wrapper.params = params.toArray();

		return wrapper;

	}
}
