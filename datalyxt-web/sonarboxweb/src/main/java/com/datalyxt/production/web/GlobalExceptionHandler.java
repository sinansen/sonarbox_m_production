package com.datalyxt.production.web;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.datalyxt.production.web.exception.AuthenticationException;
import com.datalyxt.production.web.exception.KeyStoreException;
import com.datalyxt.production.web.exception.SAMLResponseLoadException;

@ControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "AuthenticationException occured")
	@ExceptionHandler({ AuthenticationException.class, SAMLResponseLoadException.class, KeyStoreException.class })
	public void handleAuthentificationException(HttpServletRequest request, Exception ex) {
		logger.error("Authentication failed. "+ex.getMessage());
		// returning 404 error code
	}

}
