package com.datalyxt.production.web.bean.email;

public class EmailActionBean {

	public long receiverId;
	public long id;
	public int numretries;
	public int status;
	public long timestamp;
	public String address;
	public String firstName;
	public String lastName;
	public String email;
	public int updateType;
	public String subject;
	public long count;

}
