package com.datalyxt.production.web.bean.result;

import com.datalyxt.production.webscraper.model.action.ActionResultFile;

public class DataRowItemBean {
public String name;
public String value;
public ActionResultFile file;
public ActionResultFile image ;
public String link;
}
