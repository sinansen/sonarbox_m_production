package com.datalyxt.production.web.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import com.datalyxt.production.exception.db.M_UserDBException;
import com.datalyxt.production.web.bean.AuthResponse;
import com.datalyxt.production.web.bean.UserBean;
import com.datalyxt.production.web.dao.UserDAO;

@Repository("userDAO")
public class MysqlUserDAOImpl implements UserDAO {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public UserBean saveUser(AuthResponse authResponse) throws M_UserDBException {
		try {
			// insert
			KeyHolder keyHolder = new GeneratedKeyHolder();
			
			String sql = "INSERT INTO m_user ( user_name, email, lang, role)"
					+ " VALUES (?, ?, ?, ?)";

			jdbcTemplate.update(new PreparedStatementCreator() {
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					PreparedStatement pst = con.prepareStatement(sql,
							new String[] { "id" });
					int index = 1;
					pst.setString(index++, authResponse.userName);
					pst.setString(index++, authResponse.email);
					pst.setString(index++, authResponse.language);
					pst.setInt(index++, authResponse.role);
					return pst;
				}
			}, keyHolder);
			
			UserBean userbean = new UserBean();
			userbean.id = (Long) keyHolder.getKey();
			userbean.email = authResponse.email;
			userbean.userName = authResponse.userName;
			userbean.role = authResponse.role;
			
			return userbean;
			
		} catch (Exception e) {
			throw new M_UserDBException("Error in saveUser: " + e.getMessage(),
					e);
		}
	}

	@Override
	public UserBean findByUserName(String userName) throws M_UserDBException {
		try {
			String sql = "select * from m_user where user_name = ? ";
			SqlRowSet rows = jdbcTemplate.queryForRowSet(sql,
					new Object[] { userName });
			if (rows.next()) {
				UserBean user = new UserBean();
				user.id = rows.getLong("id");
				user.email = rows.getString("email");
				user.userName = rows.getString("user_name");
				user.role = rows.getInt("role");
				return user;
			}
			return null;
		} catch (Exception e) {
			throw new M_UserDBException("Error in isUserExist: "
					+ e.getMessage(), e);
		}
	}
}
