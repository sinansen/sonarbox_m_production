package com.datalyxt.webscraper.util;

import org.junit.Test;

import com.datalyxt.production.exception.processing.HtmlSourceCleanException;
import com.datalyxt.util.HtmlSourceValidationUtil;

public class TestHtmlSourceValidationUtil {
@Test
public void TestRemovespaces(){
	String html = "<div class=    \"       css1 css2 csss3\" >";
	html = HtmlSourceValidationUtil.removeSpaces(html);
	System.out.println(html);
	
	html = "<div class=    \" css1 css2 csss3\" >";
	html = HtmlSourceValidationUtil.removeSpaces(html);
	
	System.out.println(html);
	
	
	html = "<div class     =    \" css1 css2 csss3\" >";
	html = HtmlSourceValidationUtil.removeSpaces(html);
	
	System.out.println(html);
	
}

@Test
public void TestRemoveInvalidCssPart() throws HtmlSourceCleanException{
	String html = "<div class=    \"       css1 css2 4       css3\"  >";
	
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);
	
	System.out.println("result: "+html);
	
	html = "<div class     =    \" css1 css2 csss3    5  \"  alt=\"TO BE Herbst/Winter 2015\">";
	
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);
	
	System.out.println("result: "+html);
	
	html = "<div class     =    \" css1 css2 csss3    6\"   alt=\"TO BE Herbst/Winter 2015\" >";
	
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);
	
	System.out.println("result: "+html);
	
	html = "<div class     =    \" css1 css2 csss3    7\"  height = \" 8 px\" >";
	
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);
	
	System.out.println("result: "+html);
	
	
	html = "<body style=\"padding-right: 17px;\" class=\"modal-open Great%20Britain\">";
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);
	
	html = "<body style=\"padding-right: 17px;\" class=\"modal-open Great%20Britain\">  <div class     =    \" css1 css2 csss3    7\"  height = \" 8 px\" >";
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);

	System.out.println("result: "+html);
}

@Test
public void testString() throws HtmlSourceCleanException{
	String html = "class=\"form-control field{{#if errors.answer1.hasErrors}} error has-errors{{/if}}\"";
	html = HtmlSourceValidationUtil.cleanInvalidCss(html);
	System.out.println(html);
}
}
