package com.datalyxt.webscraper.util;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.webscraper.util.PageScreenshotUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.datalyxt.webscraper.model.page.BlockScreenshot;

public class TestScreenshot {
@Test
public void test() throws OpenPageUrlException, CreateScreenshotException, InterruptedException, IOException{
	BrowsingConfig cfg = new BrowsingConfig();
	cfg.pageLoadTimeout = 100000;
	PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
	WebDriver webdriver = WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
	String pageUrl = "http://www.fzi.de";
	pageNavigationUtil .navigateToPage(webdriver, cfg, pageUrl);
	PageScreenshotUtil	pageScreenshotUtil = new PageScreenshotUtil();
	BlockScreenshot screenshot = pageScreenshotUtil
			.createScreenshot(webdriver);
	System.out.println("byte created");
	Thread.sleep(30000);
	int i = 0;
	File outputfile = new File("c:\\tmp\\my"+i+"image.png");
	FileUtils.writeByteArrayToFile( outputfile, screenshot.img);
	System.out.println("file created");
//	Thread.sleep(30000);
//	pageNavigationUtil .navigateToPage(webdriver, cfg, "http://www.google.de");
//	screenshot = pageScreenshotUtil
//			.createScreenshot(webdriver);
	Thread.sleep(30000);
//	i= 1;
//	outputfile = new File("c:\\tmp\\my"+i+"image.png");
//	FileUtils.writeByteArrayToFile( outputfile, screenshot.img.array());
	webdriver.close();
}
}
