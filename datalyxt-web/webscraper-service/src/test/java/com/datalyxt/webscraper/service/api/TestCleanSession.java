package com.datalyxt.webscraper.service.api;

import java.util.HashSet;

import org.junit.Test;

import com.datalyxt.util.CleanSession;

public class TestCleanSession {
@Test
public void testCleanSession(){
	String url = "http://www.bfarm.de/SharedDocs/Kundeninfos/DE/12/2015/3027-15_Kundeninfo_de.pdf;jsessionid=179C16CCE5586527200B6FD82C95D018.1_cid332?__blob=publicationFile&v=1";
	HashSet<String> parameters = new HashSet<String>();
	parameters.add("jsessionid");
	parameters.add("handOverParams");
	String cleaned = CleanSession.cleanSession(url, parameters);
	System.out.println(cleaned);
}
}
