package com.datalyxt.webscraper.service.api;

import static org.junit.Assert.*;

import org.junit.Test;

import com.datalyxt.util.GetHost;

public class TestGetHost {
@Test
public void testGetHost(){
	String url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";

	String host = "www.bfarm.de";
	String result = GetHost.getHost(url);
	System.out.println(result);
	assertTrue(host.equals(result));
	url = "edition.cnn.com/";
	host = "edition.cnn.com";
	result = GetHost.getHost(url);
	assertNull(result);

}
@Test
public void testGetDomainUrl(){
	String url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
	String domain = "bfarm.de";
	String result = GetHost.getDomain(url);
	System.out.println(result);
	assertEquals(domain, result);
	
	url = "https://edition.cnn.com/";
	domain = "edition.cnn.com";
	result = GetHost.getDomain(url);
	System.out.println(result);
	assertTrue(domain.equals(result));
	
	url = "dition.cnn.com/";
	result = GetHost.getDomain(url);
	assertNull(result);
	
	url = "https://edition.cnn.com:8090/";
	domain = "edition.cnn.com:8090";
	result = GetHost.getDomain(url);
	System.out.println(result);
	assertTrue(domain.equals(result));
}
@Test
public void testGuessBaseUrl(){
	String url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
	String baseUrl = "http://www.bfarm.de/SiteGlobals/Forms/Suche/";
	String result = GetHost.guessBaseUrl(url);
	System.out.println(result);
	assertTrue(baseUrl.equals(result));
	url = "dition.cnn.com/";
	result = GetHost.guessBaseUrl(url);
	assertTrue(result.isEmpty());
}
}
