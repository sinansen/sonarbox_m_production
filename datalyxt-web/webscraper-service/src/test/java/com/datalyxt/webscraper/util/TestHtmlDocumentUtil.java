package com.datalyxt.webscraper.util;

import org.jsoup.nodes.Document;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestHtmlDocumentUtil {
	@Test
	public void test() {
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		BrowsingConfig browsingConfig = new BrowsingConfig();
		browsingConfig.pageLoadTimeout = 60000;
		String pageURL = "http://www.rueckrufe.net/category/ruckrufe/";
		try {

			 pageNavigationUtil.navigateToPage(driver, browsingConfig,
			 pageURL);
			// System.out.println(pagesource);
			CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
			script.removeJavascriptElementContent = true;
			script.removeHeadJavascriptLink = true;
			script.removeElementIdAttr = false;
			script.removeElementCssClassAttr = false;
			
			Document doc = HtmlDocumentUtil.getDocumentFromSource(pageURL, driver.getPageSource(), script);
			System.out.println(doc.html());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
