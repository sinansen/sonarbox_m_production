package com.datalyxt.webscraper.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.BlockConfigValidationException;
import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.FilterFailedException;
import com.datalyxt.production.exception.webscraper.IncorrectCssSelectorException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.ContentBlock;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.service.QT1V1ConfigLiveCheckService;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.production.webscraper.service.block.content.BlockContentService;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.google.gson.Gson;

public class TestQT1V1ConfigLiveCheckService {

	@Test
	public void testValidateJson() throws BlockConfigValidationException,
			PagingExtractionException {
		List<String> files = new ArrayList<>();
//		 files.add("rapex_case1_1.json");
		// files.add("rapex_case1_1.json");
//		files.add("rapex_case1_1n.json");
//		 files.add("rapex_case1_3.json");
		// files.add("rapex_case1_4.json");
		// files.add("rapex_case1_5.json");
		// files.add("rapex_case1_6.json");
		// files.add("baua_case1_1n.json");
		// files.add("baua_case1_2.json");
		// files.add("baua_case1_3.json");
		// files.add("baua_case1_4.json");
		// files.add("baua_case1_5.json");
		// files.add("baua_case1_6.json");
		// files.add("bfarm_case1_1n.json");
		// files.add("ages_case1_1n.json");
//		 files.add("cleankids_case1_1n.json");
		 files.add("test_n.json");
		// files.add("cleankids_case1_2.json");
		// files.add("visbayer_case1_1n.json");
//		 files.add("webgate_case1_1n.json");
//		 files.add("lebensmittel_case1_1n.json");
		String directory = "testcase/";

		QT1V1ConfigService qt1v1ConfigService = new QT1V1ConfigService();

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		List<String> urls = new ArrayList<>();
		// urls.add("http://www.ages.at/produktwarnungen/produktkategorie/lebensmittel/");
		// urls.add("http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank");
//		 urls.add("http://www.cleankids.de/magazin/produktrueckrufe/ruckrufe-lebensmittel");
		// urls.add("http://www.vis.bayern.de/produktsicherheit/herstellerinfos/");
//		 urls.add("https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE");
//		 urls.add("http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/warnung/start/bvllmwde.p_oeffentlicher_bereich.ss_alle_warnungen?_opensaga_navigationId=bvllmwde.p_oeffentlicher_bereich.n_alle_warnungen");
//		urls.add("http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/base_p_error?execution=e20s1");
		 // urls.add("http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/warnung/detail18/17639");
//		urls.add("http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications");
//urls.add("http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.weeklyOverview&selectedTabIdx=1&web_report_id=1861");
		//urls.add("http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.weeklyOverview&selectedTabIdx=1&web_report_id=1821");
//		urls.add("http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.weeklyOverview&selectedTabIdx=1&web_report_id=1791");
		// urls.add("http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html");
		// urls.add("http://www.bfarm.de/SharedDocs/Kundeninfos/DE/14/2016/3970-16_Kundeninfo_de.pdf?__blob=publicationFile&v=1");
//		 urls.add("http://www.rueckrufe.net/category/ruckrufe/");
//		 urls.add("http://www.rueckrufe.net/2016/06/17/tomatensaft-und-gemuesesaft-von-biosphere-wegen-moeglichem-schimmelbefall-im-rueckruf/");
urls.add("http://getknit.de/testcases/test.html");
		BlockContentService blockContentService = new BlockContentService();
		int maxhop = 1;
		Gson gson = new Gson();
		QT1V1ConfigLiveCheckService qt1v1ConfigLiveCheckService = new QT1V1ConfigLiveCheckService();
		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		for (String filename : files) {
			InputStream in = TestQT1V1ConfigLiveCheckService.class.getClassLoader()
					.getResourceAsStream(directory + filename);
			Reader reader = new InputStreamReader(in);
			BlockConfigMain mainConfig = gson.fromJson(reader,
					BlockConfigMain.class);
			System.out.println("\ncheck " + filename);
			try {
				for (int hop = 0; hop <= maxhop; hop++) {

					System.out
							.println("hop: " + hop + "  url " + urls.get(hop));

					List<Block> blocks = qt1v1ConfigLiveCheckService
							.checkConfig(driver, mainConfig, script,
									urls.get(hop), hop);
					System.out.println("rows " + blocks.size());
					for (Block b : blocks) {
						HashMap<String, LinkBlock> ls = blockContentService
								.getLinkBlocks(b);

						for (LinkBlock lk : ls.values()) {
							System.out.println(lk.link.url+"  "+lk.link.linkType.toString());
						}
						if (b.type.equals(BlockType.datasetBlock)) {
							System.out.println("columns "
									+ ((DatasetBlock) b).blocks.size());
							for (Block bl : ((DatasetBlock) b).blocks) {

								if (bl.type.equals(BlockType.datasetBlock)) {

									for (Block blk : ((DatasetBlock) bl).blocks) {
										if (blk.type
												.equals(BlockType.datasetBlock))
											System.out
													.println(blk.blockTextHash);
										else
											System.out
													.println("name "
															+ ((ContentBlock) bl).name
															+ " : "
															+ ((ContentBlock) bl).blockText);
									}
								} else
									System.out.println("name "
											+ ((ContentBlock) bl).name + " : "
											+ ((ContentBlock) bl).blockText);
							}
						} else
							System.out.println(((ContentBlock) b).blockText);
					}
				}
				String updatedJson = gson.toJson(mainConfig,
						BlockConfigMain.class);
				System.out.println("passed: " + filename);
				System.out.println("\n" + updatedJson);
			} catch (OpenPageUrlException | DAOFactoryException
					| DefinedBlockNotFound | BlockExtractionException
					| HtmlDocumentCreationException
					| IncorrectCssSelectorException | UnknownBlockTypeException
					| NullBlockLocationException | TimeBlockException
					| UnsupportedBlockLocationException
					| ColumnTemplateException | M_DomainBlackListDBException
					| FilterFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@Test
	public void testSelector() {
		try {
			Properties dbproperties = new Properties();
			dbproperties.load(TestQT1V1ConfigLiveCheckService.class
					.getClassLoader().getResourceAsStream("db.properties"));

			DataSource datasource = MyDataSourceFactory
					.getMySQLDataSource(dbproperties);

			DAOFactory daoFactory = new MysqlDAOFactory(datasource);
			M_Rule rule = daoFactory.getM_RuleDAO().getRules(M_RuleStatus.activated, 35L).get(0);
			QT1V1ConfigLiveCheckService qt1v1ConfigLiveCheckService = new QT1V1ConfigLiveCheckService();
			CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
			BlockContentService blockContentService = new BlockContentService();

			WebDriver driver = WebDriverFactory
					.createDriver(BrowserDriverType.firefox_windows);
			 String url = ("https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE");
				

			List<Block> blocks = qt1v1ConfigLiveCheckService.checkConfig(
					driver,
					((QT1_V1_RuleContent) rule.ruleContent).blockConfigMain,
					script, url, 0);
			System.out.println("rows " + blocks.size());
			for (Block b : blocks) {
				HashMap<String, LinkBlock> ls = blockContentService
						.getLinkBlocks(b);

				for (String l : ls.keySet()) {
					System.out.println(l);
				}
				if (b.type.equals(BlockType.datasetBlock)) {
					System.out.println("columns "
							+ ((DatasetBlock) b).blocks.size());
					for (Block bl : ((DatasetBlock) b).blocks) {

						if (bl.type.equals(BlockType.datasetBlock)) {

							for (Block blk : ((DatasetBlock) bl).blocks) {
								if (blk.type.equals(BlockType.datasetBlock))
									System.out.println(blk.blockTextHash);
								else
									System.out.println("name "
											+ ((ContentBlock) bl).name + " : "
											+ ((ContentBlock) bl).blockText);
							}
						} else
							System.out.println("name "
									+ ((ContentBlock) bl).name + " : "
									+ ((ContentBlock) bl).blockText);
					}
				} else
					System.out.println(((ContentBlock) b).blockText);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
