package com.datalyxt.webscraper.service.api;

import static org.junit.Assert.*;

import org.junit.Test;

import com.datalyxt.util.GetHost;
import com.datalyxt.util.InternalLink;

public class TestInternalLink {
@Test
public void testInternalLink(){
	String domainURL = "http://www.fzi.de";
	String url = "https://fzi.de/index.html";
	String currentDomain = GetHost.getDomain(domainURL);
	assertTrue(InternalLink.isInternalLink(url, currentDomain));
}
}
