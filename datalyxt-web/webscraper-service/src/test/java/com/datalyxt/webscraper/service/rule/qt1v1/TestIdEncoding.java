package com.datalyxt.webscraper.service.rule.qt1v1;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.MarkerUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestIdEncoding {
	@Test
	public void testEncoding() throws OpenPageUrlException,
			HtmlDocumentCreationException {
		String url = "http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/warnung/start/bvllmwde.p_oeffentlicher_bereich.ss_aktuelle_warnungen";
		String selector = "osForm:id_002ei5i:0:id_002em5i";

		url = "http://www.vis.bayern.de/produktsicherheit/herstellerinfos/";
		selector = "body:nth-child(2) > div:nth-child(1)  > div:nth-child(6) ";

		url = "https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE";
		// selector =
		// "body:nth-child(2) > div:nth-child(3) > section:nth-child(4) > div:nth-child(2) > div > div:nth-child(3) > div > div > div > table > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1) > dlyxtspan:nth-child(2)";
		selector = "body:nth-child(2) > div:nth-child(3) > section:nth-child(4) > div:nth-child(2) > div > div:nth-child(3)  > div > div ";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		pageNavigationUtil.navigateToPage(driver, null, url);
		Document doc = HtmlDocumentUtil.getDocumentFromSource(url,
				driver.getPageSource(), null);
		System.out.println(driver.getPageSource());

		Elements eles = doc.select(selector);
		System.out.println(eles.size() + " : " + eles.first().attr("class"));
	}

	@Test
	public void testMarkerUtil() throws HtmlDocumentCreationException,
			OpenPageUrlException {
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		String url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
		String url1="http://www.bfarm.de/SharedDocs/Kundeninfos/DE/11/2016/3268-16_Kundeninfo_de.html;jsessionid=9167109D76FD8C5765A239F7162AB2D5.1_cid322";
		String url2 = "http://www.vis.bayern.de/produktsicherheit/herstellerinfos/";
		url = "https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE";
		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		script.enrichTextNode = true;
		script.enableEnrichTextNodeWithJs = false;
		script.removeElementCssClassAttr = false;
		script.removeElementIdAttr = false;
		script.removeHeadJavascriptLink = false;
		script.removeJavascriptElementContent = false;
	
		Document doc = MarkerUtil.getCleanedPageSource(url, driver, script, "http://localhost:8080", new BrowsingConfig());
		System.out.println(doc.html());
	}
}
