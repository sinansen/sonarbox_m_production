package com.datalyxt.webscraper.service.rule.qt1v1;

import java.util.ArrayList;
import java.util.Vector;

import org.jsoup.nodes.Document;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.webscraper.model.paging.PagingElement;
import com.datalyxt.util.CleanSession;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.PagingExtractor;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestPagingBlock {
	@Test
	public void testPaging() throws HtmlDocumentCreationException,
			OpenPageUrlException, PagingExtractionException {

		PagingExtractor pagingExtractor = new PagingExtractor();

		String url = "http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank.html";

		String pagingSelector = "#main > div > div:nth-child(2) > p:nth-child(10) > span:nth-child(2) > span";
		 url =
		 "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";
		 pagingSelector = "#tabs > ul";

		 url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html?gtp=3494768_list%253D2";
			 pagingSelector = "body:nth-child(2) > div:nth-child(1) > div > div:nth-child(5) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > ul:nth-child(3)";

		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		int index = 10000;
		int counter = 0;
		String nextLink = url;
		boolean done = false;

		Vector<String> visited = new Vector<String>();

		do {
			nextLink = CleanSession.cleanSession(nextLink);
			pageNavigationUtil.navigateToPage(driver, null, nextLink);
			String pageSource = driver.getPageSource();

			Document doc = HtmlDocumentUtil.getDocumentFromSource(nextLink,
					pageSource, null);
			System.out.println("\n\nVisit " + nextLink);

			ArrayList<PagingElement> links = pagingExtractor.getPagingLinks(
					nextLink, doc, pagingSelector);
			visited.add(nextLink);

			done = true;
			for (PagingElement link : links) {
				if (!visited.contains(link.href)) {
					nextLink = link.href;
					done = false;
					break;
				}
			}
			print(links);
			counter++;
			break;
		} while (counter <= index && !done);

	}

	private void print(ArrayList<PagingElement> pe) {
		for (PagingElement p : pe) {
			System.out.println("****************************");
			System.out.println("HREF\t:" + p.href);
			System.out.println("Text\t:" + p.hrefDescription);
			System.out.println("PFREE\t:" + p.parameterFree);
			System.out.println("ANC-ID\t:" + p.anchorId);
			System.out.println("HREF-T\t:" + p.pht);
			System.out.println("RES-Tar\t:" + p.rt);
		}
	}
}
