package com.datalyxt.webscraper.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestListExtraction {
	
	class Article{
		public String title;
		public long date;
		public String description;
		public String link;
	}
	@Test
public void testListItemExtraction(){
		HashMap<String, String> urls = new HashMap<String, String>();

		String url = "http://36kr.com/";

		WebDriver driver = WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
		driver.get(url);
		Document doc = Jsoup.parse(driver.getPageSource());
		List<Article> articles = new ArrayList<Article>();
		Elements eles = doc.getElementsByTag("article");
		for(Element element : eles){
			Article article = new Article();
		    Elements children = element.getAllElements();
		    for(Element  child : children){
		    	if(child.hasText()){
		    		System.out.println(child.text());
		    	}
		    }
		}
	}
}
