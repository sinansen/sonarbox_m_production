package com.datalyxt.webscraper.util;

import java.util.ArrayList;

import org.junit.Test;

import com.datalyxt.exception.extraction.CreateURLWrapperException;
import com.datalyxt.util.ValidLink;
import com.datalyxt.util.url.URLUtil;
import com.datalyxt.webscraper.model.link.URLWrapper;

public class TestValidLink {
@Test
public void testValidLink(){
	ArrayList<String> urls = new ArrayList<String>();
	urls.add("mail@fzi.de");
	urls.add("htt://fzi.de");
	urls.add("http://fzi.de");
	urls.add("https://fzi.de");
	urls.add("https://www.fzi.de");
	urls.add("http://www.stackoverflow.com");
	urls.add("ftp://fzi.de");
	urls.add("hz://fzi.de");
	urls.add("http://");
	urls.add("http://webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=java%20regex%20match%20www.");
	urls.add("http://a.b.c");
	urls.add("http://wwwa.youb.com");
	urls.add("https://www.google.de/webhp?sourceid=chrome-instant&ion=1&espv=2&es_th=1&ie=UTF-8#q=java%20regex%20match%20www.");
	urls.add("http://www.vogella.com/tutorials/JavaRegularExpressions/article.html");
	urls.add("http://www.ua-bw.de/pub/download_results.asp?subid=0&Dwnld_ID=1&lang=DE");
	urls.add("http://www.ua-bw.de/pub/download_results.asp?subid=0&Dwnld_ID=1&lang=DE");
	urls.add("http://www.ua-bw.de/pub/");
	urls.add("http://www.ua-bw.de/");
	urls.add("http://www.ua-bw.de");
	for(String url : urls){
	System.out.println(url+" : " +ValidLink.isHttp(url));
//	try {
//		URLWrapper wrapper = URLUtil.createURLWrapper(url);
//		System.out.println(wrapper.protocol+"   "+wrapper.host);
//	} catch (CreateURLWrapperException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	}
}
}
