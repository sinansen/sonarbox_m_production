package com.datalyxt.webscraper.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Test;

import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.rule.QT1_V2_RuleContent;
import com.datalyxt.production.webscraper.model.config.ActionCondition;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.util.GetHost;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestRule {
	class RuleUpdateScript {
		public boolean deleteAllMapping = false;
		public long ruleId;
		public HashSet<Long> sourceIds = new HashSet<Long>();
		public M_RuleStatus status;
		public String ruleName;
		public QT1_V1_RuleContent qt1_V1_RuleContent;
		public QT1_V2_RuleContent qt1_V2_RuleContent;
		public M_RuleType ruleType;
	}

	@Test
	public void createRule() {
		RuleUpdateScript script = new RuleUpdateScript();
		String url = "https://www.swissmedic.ch/rueckrufe_medizinprodukte/";
		script.ruleName = GetHost.getDomain(url);
		script.ruleType = M_RuleType.QT1V1;
		QT1_V1_RuleContent ruleContent = new QT1_V1_RuleContent();
		script.sourceIds.add(64L);

		ruleContent.blockConfigMain = new BlockConfigMain();
		ruleContent.blockConfigMain.config = new ArrayList<PageBlockConfig>();
		PageBlockConfig pCfg = new PageBlockConfig();
		pCfg.hop = 0;
		pCfg.url = url;
		BlockConfig blockConfig = new BlockConfig();
		blockConfig.condition = ActionCondition.link_url_change;
		BlockAction a1 = new BlockAction();
		a1.type = BlockActionType.followLinks;
		blockConfig.action.add(a1);

		blockConfig.multiRow = true;
		blockConfig.type = BlockType.datasetBlock;
		blockConfig.blocks = new ArrayList<BlockConfig>();

		BlockConfig d1 = new BlockConfig();
		d1.type = BlockType.timeBlock;
		d1.name = "Date";
		d1.location = new BlockLocation();
		d1.location.type = BlockLocationType.cssSelector;
		d1.location.value = "body:nth-child(2) > div:nth-child(19) > div > section:nth-child(2) > article:nth-child(1) > div:nth-child(4) > table > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(1)";
	
		BlockLocation d1s1 = new BlockLocation();
		d1s1.type = BlockLocationType.cssSelector;
		d1s1.value = "body:nth-child(2) > div:nth-child(19) > div > section:nth-child(2) > article:nth-child(1) > div:nth-child(4) > table > tbody:nth-child(3) > tr:nth-child(2) > td:nth-child(1)";

		d1.supervised = new ArrayList<BlockLocation>();

		d1.supervised.add(d1s1);
		
		BlockConfig d2 = new BlockConfig();
		d2.type = BlockType.keyvalueBlock;
		d2.name = "Hersteller";
		
		d2.location = new BlockLocation();
		d2.location.type = BlockLocationType.cssSelector;
		d2.location.value = "body:nth-child(2) > div:nth-child(19) > div > section:nth-child(2) > article:nth-child(1) > div:nth-child(4) > table > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(2)";
      
		
		BlockLocation d2s1 = new BlockLocation();
		d2s1.type = BlockLocationType.cssSelector;
		d2s1.value = "body:nth-child(2) > div:nth-child(19) > div > section:nth-child(2) > article:nth-child(1) > div:nth-child(4) > table > tbody:nth-child(3) > tr:nth-child(2) > td:nth-child(2)";

		d2.supervised = new ArrayList<BlockLocation>();

		d2.supervised.add(d2s1);

		BlockConfig d3 = new BlockConfig();
		d3.type = BlockType.keyvalueBlock;
		d3.name = "Produktbeschreibung";
		d3.follow = true;
		d3.location = new BlockLocation();
		d3.location.type = BlockLocationType.cssSelector;
		d3.location.value = "body:nth-child(2) > div:nth-child(19) > div > section:nth-child(2) > article:nth-child(1) > div:nth-child(4) > table > tbody:nth-child(3) > tr:nth-child(1) > td:nth-child(4)";

		BlockLocation d3s1 = new BlockLocation();
		d3s1.type = BlockLocationType.cssSelector;
		d3s1.value = "body:nth-child(2) > div:nth-child(19) > div > section:nth-child(2) > article:nth-child(1) > div:nth-child(4) > table > tbody:nth-child(3) > tr:nth-child(2) > td:nth-child(4)";

		d3.supervised = new ArrayList<BlockLocation>();
		d3.supervised.add(d3s1);
		
//		BlockConfig d4 = new BlockConfig();
//		d4.type = BlockType.linkBlock;
//		d4.name = "detail";
//		d4.follow = true;
//		d4.location = new BlockLocation();
//		d4.location.type = BlockLocationType.cssSelector;
//		d4.location.value = "body:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(2)  > article:nth-child(1) > div:nth-child(2) > div:nth-child(3) > a:nth-child(3)";
//
//	
//		BlockLocation d4s1 = new BlockLocation();
//		d4s1.type = BlockLocationType.cssSelector;
//		d4s1.value = "body:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div > div > div:nth-child(1) > div:nth-child(2) > div > div > div:nth-child(2) > div:nth-child(1) > div > div:nth-child(2)  > article:nth-child(2) > div:nth-child(2) > div:nth-child(3) > a:nth-child(3)";
//
//		d4.supervised = new ArrayList<BlockLocation>();
//		d4.supervised.add(d4s1);

		blockConfig.blocks.add(d1);
		blockConfig.blocks.add(d2);
		blockConfig.blocks.add(d3);
//		blockConfig.blocks.add(d4);
		
		pCfg.block.add(blockConfig);

//		PageBlockConfig pCfg1 = new PageBlockConfig();
//		pCfg1.hop = 1;
//		pCfg1.url = "http://www.rueckrufe.net/2016/06/17/tomatensaft-und-gemuesesaft-von-biosphere-wegen-moeglichem-schimmelbefall-im-rueckruf/";
//		BlockConfig blockConfigHop1 = new BlockConfig();
//		BlockAction a1h1 = new BlockAction();
//		a1h1.type = BlockActionType.cleanText;
//		blockConfigHop1.action.add(a1h1);
//
//		blockConfigHop1.multiRow = false;
//		blockConfigHop1.type = BlockType.keyvalueBlock;
//		blockConfigHop1.location = new BlockLocation();
//		blockConfigHop1.location.type = BlockLocationType.cssSelector;
//		blockConfigHop1.location.value = "body:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div > div > div:nth-child(1) > article:nth-child(1) > div:nth-child(1) > h1:nth-child(1)";
//
//		pCfg1.block.add(blockConfigHop1);

		ruleContent.blockConfigMain.config.add(pCfg);
//		ruleContent.blockConfigMain.config.add(pCfg1);

		script.qt1_V1_RuleContent = ruleContent;

		callCreateRule(script);

	}

	private void callCreateRule(RuleUpdateScript script) {
		try {

			URL url = new URL("http://localhost:8080/sonarbox/res/rules");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			ObjectMapper mapper = new ObjectMapper();
			String input = mapper.writeValueAsString(script);
			System.out.println(input);
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {

				System.out.println("Failed : HTTP error code : "
						+ conn.getResponseCode() + " : "
						+ conn.getResponseMessage());
			} else {

				BufferedReader br = new BufferedReader(new InputStreamReader(
						(conn.getInputStream())));

				String output;
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
				}
			}
			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}

	@Test
	public void normkRule() {
		callNormalizeRule(41);
	}

	@Test
	public void liveCheck() {
		callLiveCheckRule(43);
	}
	private void callNormalizeRule(long ruleId) {
		try {

			URL url = new URL(
					"http://localhost:8080/sonarbox/res/rules/selectors/"+ruleId);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("PUT");
		
	
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			System.out.println(" HTTP  code : " + conn.getResponseCode()
					+ " : " + conn.getResponseMessage());
			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}
	private void callLiveCheckRule(long ruleId) {
		try {

			URL url = new URL(
					"http://localhost:8080/sonarbox/res/rules/selectors/"+ruleId);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Content-Type", "application/json");
			
			String output;
			
			System.out.println("Output from Server .... \n");
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}

			System.out.println(" HTTP  code : " + conn.getResponseCode()
					+ " : " + conn.getResponseMessage());
			conn.disconnect();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}
}
