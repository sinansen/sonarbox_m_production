package com.datalyxt.webscraper.util;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;

public class TestCssPath {
	@Test
	public void testPath() throws IOException {
		String url = "http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/warnung;jsessionid=DDB26D4FA288D9467BD6AD30D61061FE?execution=e1s1";

		Document doc = Jsoup.connect(url).get();
		String csspath = "#main-navigation > li:nth-child(4) > a" ;
		csspath = "fb:likebox > a";
//		csspath = "some/id1/id2 > 2";
		csspath = "#osForm:id_002esbj:0:id_002edcj";
		csspath = "#123 > 2#343";
		Elements eles = doc.select(csspath);
		if(eles.isEmpty())
			System.out.println("path is wrong ! no element found! ");
		else System.out.println(eles.first().tagName()+"  "+eles.first().text());
	
	}

}
