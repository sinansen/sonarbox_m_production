package com.datalyxt.webscraper.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.Strings;
import org.junit.Test;

import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.impl.M_FileStorageDAOImpl;

public class TestFileStorageUtil {
	@Test
	public void testWrite() {
		String ts = "Write this content to linux file";
		byte[] str_Content = Strings.toByteArray(ts);
		Properties properties = new Properties();
		properties.setProperty("ssh_port", "22"); // By default SSH port of
													// linux
													// system is 22.
		// Please check yours if it is different.
		properties.setProperty("sftp_directory", "/home/filewriter/");
		properties.setProperty("sftp_host", "h2374001.stratoserver.net");
		properties.setProperty("sftp_username", "filewriter");
		properties.setProperty("sftp_password", "fzi1014");
		String str_File = "file_to_write.txt";
		M_FileStorageDAO obj_JSCHcl = new M_FileStorageDAOImpl(properties);
		String[] paths = new String[] {};
		try {
			obj_JSCHcl.writeFile(str_Content, paths, str_File);
		} catch (FileStorageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void testRead() {
		// Please check yours if it is different.
		Properties properties = new Properties();
		properties.setProperty("ssh_port", "22"); // By default SSH port of
													// linux
													// system is 22.
		// Please check yours if it is different.
		properties.setProperty("sftp_directory", "/home/filewriter/s34/r47");
		properties.setProperty("sftp_host", "h2374001.stratoserver.net");
		properties.setProperty("sftp_username", "filewriter");
		properties.setProperty("sftp_password", "fzi1014");
		String str_File = "58a4b885831f0791812ed69d0f037cd0.png";
		M_FileStorageDAO obj_JSCHcl = new M_FileStorageDAOImpl(properties);

		try {
			byte[] str_FileContent = obj_JSCHcl.readFile("", str_File);
			InputStream in = new ByteArrayInputStream(str_FileContent);
			BufferedImage bImageFromConvert = ImageIO.read(in);

			ImageIO.write(bImageFromConvert, "png", new File(
					"c:/tmp/s2.png"));
		} catch (FileStorageException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
