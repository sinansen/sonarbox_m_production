package com.datalyxt.webscraper.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;

import org.junit.Test;

public class TestDownloadFile {
	@Test
	public void testDownloadFile() throws IOException {
		String fileName = "c:\\tmp\\file.pdf"; // The file that will be saved on your
										// computer
		URL link = new URL("http://www.bfarm.de/SharedDocs/Kundeninfos/DE/12/2015/2367-15_Kundeninfo_de.pdf?__blob=publicationFile&v=1"); // The file that you want
														// to download
		
		URLConnection u = link.openConnection();
		long length = Long.parseLong(u.getHeaderField("Content-Length"));
		String type = u.getHeaderField("Content-Type");
		System.out.println(type);
		// Code to download
		InputStream in = new BufferedInputStream(link.openStream());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buf = new byte[4096];
		int n = 0;
		while (-1 != (n = in.read(buf))) {
			out.write(buf, 0, n);
		}
		out.close();
		in.close();
		byte[] response = out.toByteArray();
		FileOutputStream fos = new FileOutputStream(fileName);
		fos.write(response);
		fos.close();
		// End download code

		System.out.println("Finished");

	}
}
