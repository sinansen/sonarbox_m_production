package com.datalyxt.webscraper.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestCreateScreenshot {
	@Test
	public void testScreenshot() throws IOException {
		WebDriver driver = WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
		driver.get("http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?Year=2016&event=main.weeklyOverview&web_report_id=1841");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		File scrFile = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);

		byte[] imageInByte;
		BufferedImage originalImage = ImageIO.read(scrFile);

		// convert BufferedImage to byte array
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(originalImage, "png", baos);
		baos.flush();
		imageInByte = baos.toByteArray();
		baos.close();
		
		FileUtils.writeByteArrayToFile(new File("c:\\tmp\\1.jpg"), imageInByte);
		
	}
}
