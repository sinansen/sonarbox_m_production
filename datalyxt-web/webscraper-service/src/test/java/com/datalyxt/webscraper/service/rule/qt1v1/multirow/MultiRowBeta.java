package com.datalyxt.webscraper.service.rule.qt1v1.multirow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Vector;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.service.block.content.multirow.BlockConfigWrapper;
import com.datalyxt.production.webscraper.service.block.content.multirow.BlockTrainingData;
import com.datalyxt.production.webscraper.service.block.content.multirow.MultiRowDetectionContext;
import com.datalyxt.production.webscraper.service.block.content.multirow.RowBasedSelectorTemplate;
import com.datalyxt.production.webscraper.service.block.content.multirow.SupervisedMultiRowService;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlTextNodeUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.google.gson.Gson;

public class MultiRowBeta {

	public HashMap<String, List<BlockConfigWrapper>> doProcess(
			List<BlockConfig> configs,
			HashSet<BlockLocation> tobeCheckedCssSelector)
			throws UnsupportedBlockLocationException, ColumnTemplateException {
		SupervisedMultiRowService supervisedMultiRowService = new SupervisedMultiRowService();

		BlockTrainingData blockTrainingData = supervisedMultiRowService
				.addSupervisiedDataColumn(configs);

		HashMap<String, List<BlockConfigWrapper>> data = new HashMap<String, List<BlockConfigWrapper>>();
		MultiRowDetectionContext multiRowDetectionContext = new MultiRowDetectionContext();
		List<RowBasedSelectorTemplate> templates = supervisedMultiRowService
				.createRowBasedTemplate(multiRowDetectionContext, blockTrainingData);

		Gson gson = new Gson();
		
		HashMap<String, BlockConfig> confgs = new HashMap<>();
		for(BlockConfig cfg : configs){
			confgs.put(cfg.name, cfg);
		}

		System.out.println("start to process " + tobeCheckedCssSelector.size()
				+ " css selectors");
		for (BlockLocation cssSelector : tobeCheckedCssSelector) {

			BlockConfigWrapper bloWrapper = supervisedMultiRowService
					.identifyIteratedData(cssSelector, templates);
			if (bloWrapper != null) {
				String key = gson.toJson(bloWrapper.row.toArray());
				List<BlockConfigWrapper> cofs = new ArrayList<BlockConfigWrapper>();
				if (data.containsKey(key))
					cofs = data.get(key);
				cofs.add(bloWrapper);
				data.put(key, cofs);
			}

		}
		return data;
	}

	@Test
	public void testCase1_bfarm() throws ColumnTemplateException,
			OpenPageUrlException, UnsupportedBlockLocationException {

		String url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "#searchResult";
		css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());
		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);
		boolean isEnrichTextNodeRquired = false;

		if (isEnrichTextNodeRquired) {
			HtmlTextNodeUtil textNodeUtil = new HtmlTextNodeUtil();
			doc = textNodeUtil.enrichMarkableTextNode(doc);
		}

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#searchResult > li:nth-child(1) > p:nth-child(1)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		Vector<String> supervision = new Vector<String>();
		supervision.add("#searchResult > li:nth-child(1) > p:nth-child(1)");
		supervision.add("#searchResult > li:nth-child(2) > p:nth-child(1)");
		supervision.add("#searchResult > li:nth-child(3) > p:nth-child(1)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		b = new BlockConfig();
		b.name = "producer";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#searchResult > li:nth-child(1) > div:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision.add("#searchResult > li:nth-child(1) > div:nth-child(2)");
		supervision.add("#searchResult > li:nth-child(2) > div:nth-child(2)");
		supervision.add("#searchResult > li:nth-child(3) > div:nth-child(2)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		b = new BlockConfig();
		b.name = "info";
		b.type = BlockType.linkBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#searchResult > li:nth-child(1) > div:nth-child(3) > span > a";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision
				.add("#searchResult > li:nth-child(1) > div:nth-child(3) > span > a");
		supervision
				.add("#searchResult > li:nth-child(2) > div:nth-child(3) > span > a");
		supervision
				.add("#searchResult > li:nth-child(3) > div:nth-child(3) > span > a");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase2_baua() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());
		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		Vector<String> supervision = new Vector<String>();
		supervision
				.add("#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(1)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		b = new BlockConfig();
		b.name = "producer";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision
				.add("#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(2)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		b = new BlockConfig();
		b.name = "marken";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(3)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision
				.add("#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(3)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase3_baua_singleColumn() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());
		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		Vector<String> supervision = new Vector<String>();
		supervision
				.add("#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(1)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}
		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase4_baua_singleRow() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());
		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		configs.add(b);

		b = new BlockConfig();
		b.name = "producer";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		b = new BlockConfig();
		b.name = "marken";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div:nth-child(2) > table:nth-child(7) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(3)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		System.out.println("total datasets: " + rows.size());
		Collections.sort(rows);
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase5_rapex() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/index.cfm?event=main.weeklyOverview&web_report_id=1841&selectedTabIdx=1";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "land";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#AutoNumber1 > tbody > tr:nth-child(1) > td:nth-child(2) > p:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		configs.add(b);

		b = new BlockConfig();
		b.name = "category";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#AutoNumber1 > tbody > tr:nth-child(1) > td:nth-child(3) > p:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase5_rapex_main() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "land";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#tabs-2016 > table > tbody > tr > td:nth-child(1) > ul > li:nth-child(1) > span";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		configs.add(b);

		b = new BlockConfig();
		b.name = "category";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#AutoNumber1 > tbody > tr:nth-child(1) > td:nth-child(3) > p:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase6_bayern() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.vis.bayern.de/produktsicherheit/herstellerinfos/";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#inhalt_links > div > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(1) > p";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		configs.add(b);

		b = new BlockConfig();
		b.name = "description";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#inhalt_links > div > table:nth-child(3) > tbody > tr:nth-child(1) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase7_webgate() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		    String url = "https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE";

			WebDriver driver = WebDriverFactory
					.createDriver(BrowserDriverType.firefox_windows);

			String css = "#Result";
			PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
			BrowsingConfig browsingConfig = new BrowsingConfig();
			browsingConfig.pageLoadTimeout = 5000;
			pageNavigationUtil.navigateToPage(driver, null, url);
			// pause
			String baseUri = "";
			Document doc = Jsoup.parse(driver.getPageSource());

			baseUri = doc.baseUri();
			String currentUrl = url;
			if (baseUri == null || baseUri.isEmpty())
				baseUri = GetHost.guessBaseUrl(currentUrl);
			doc.setBaseUri(baseUri);

			List<BlockConfig> configs = new ArrayList<BlockConfig>();

			// REFERENCE
			BlockConfig b = new BlockConfig();
			b.name = "datum";
			b.type = BlockType.timeBlock;
			b.location = new BlockLocation();
			b.location.type = BlockLocationType.cssSelector;
			b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)";
			Elements eles = doc.select(b.location.value);
			if(eles == null)
				System.out.println("\n----------------selektor error ----------------\n");
			else System.out.println("\n----------------selektor contains "+eles.size()+" elements");
			Element first = eles.first();
			if(first == null)
				System.out.println("\n----------------selektor first element is null ----------------\n");
			
			String firstcss = first.cssSelector();
			
			b.location.value = firstcss;
			System.out.println("standard: " + b.location.value);
			b.supervised = new ArrayList<BlockLocation>();
			// Vector<String> supervision = new Vector<String>();
			// supervision
			// .add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(1)");
			//
			// for (String selector : supervision) {
			// BlockLocation location = new BlockLocation();
			// selector = doc.select(selector).first().cssSelector();
			// location.type = BlockLocationType.cssSelector;
			// location.value = selector;
			// b.supervised.add(location);
			// }

			configs.add(b);

			b = new BlockConfig();
			b.name = "ref";
			b.type = BlockType.keyvalueBlock;
			b.location = new BlockLocation();
			b.location.type = BlockLocationType.cssSelector;
			b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(2)";
			b.location.value = doc.select(b.location.value).first()
					.cssSelector();
			System.out.println("standard: " + b.location.value);
			b.supervised = new ArrayList<BlockLocation>();
			// SUPERVISION
			// supervision = new Vector<String>();
			// supervision
			// .add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(2)");
			//
			// for (String selector : supervision) {
			// BlockLocation location = new BlockLocation();
			// selector = doc.select(selector).first().cssSelector();
			// location.type = BlockLocationType.cssSelector;
			// location.value = selector;
			// b.supervised.add(location);
			// }
			configs.add(b);

			b = new BlockConfig();
			b.name = "notifyby";
			b.type = BlockType.keyvalueBlock;
			b.location = new BlockLocation();
			b.location.type = BlockLocationType.cssSelector;
			b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(3)";
			b.location.value = doc.select(b.location.value).first()
					.cssSelector();
			System.out.println("standard: " + b.location.value);
			b.supervised = new ArrayList<BlockLocation>();
			// SUPERVISION
			// supervision = new Vector<String>();
			// supervision
			// .add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(3)");
			//
			// for (String selector : supervision) {
			// BlockLocation location = new BlockLocation();
			// selector = doc.select(selector).first().cssSelector();
			// location.type = BlockLocationType.cssSelector;
			// location.value = selector;
			// b.supervised.add(location);
			// }
			configs.add(b);

			b = new BlockConfig();
			b.name = "subject";
			b.type = BlockType.keyvalueBlock;
			b.location = new BlockLocation();
			b.location.type = BlockLocationType.cssSelector;
			b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(4)";
			b.location.value = doc.select(b.location.value).first()
					.cssSelector();
			System.out.println("standard: " + b.location.value);
			b.supervised = new ArrayList<BlockLocation>();
			// SUPERVISION
			// supervision = new Vector<String>();
			// supervision
			// .add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(4)");
			//
			// for (String selector : supervision) {
			// BlockLocation location = new BlockLocation();
			// selector = doc.select(selector).first().cssSelector();
			// location.type = BlockLocationType.cssSelector;
			// location.value = selector;
			// b.supervised.add(location);
			// }
			configs.add(b);

			Element element = doc.select(css).first();
			HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
			for (Element child : element.getAllElements()) {
				BlockLocation lo = new BlockLocation();
				lo.type = BlockLocationType.cssSelector;
				lo.value = child.cssSelector();
				lo.value = lo.value.replace(" ", "");
				tobeCheckedCssSelector.add(lo);
			}

			driver.close();

			HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
					tobeCheckedCssSelector);

			List<String> rows = new ArrayList<>();
			rows.addAll(data.keySet());
			Collections.sort(rows);
			System.out.println("total datasets: " + rows.size());
			for (String key : rows) {
				System.out.println("\nrow: " + key);
				List<BlockConfigWrapper> row = data.get(key);
				for (BlockConfigWrapper blwrapper : row) {

					String value = doc
							.select(blwrapper.blockConfig.location.value)
							.first().text();

					System.out.println("name: " + blwrapper.blockConfig.name
							+ "     value: " + value);

				}
			}
	}

	@Test
	public void testCase8_webgate() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "#Result";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);
		System.out.println(driver.getPageSource());
		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(1)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		System.out.println("standard: " + b.location.value);
		b.supervised = new ArrayList<BlockLocation>();
		Vector<String> supervision = new Vector<String>();
		supervision
				.add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(1)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		b = new BlockConfig();
		b.name = "ref";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		System.out.println("standard: " + b.location.value);
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision
				.add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(2)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}
		configs.add(b);

		b = new BlockConfig();
		b.name = "notifyby";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(3)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		System.out.println("standard: " + b.location.value);
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision
				.add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(3)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}
		configs.add(b);

		b = new BlockConfig();
		b.name = "subject";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#Result > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(4)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		System.out.println("standard: " + b.location.value);
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION
		supervision = new Vector<String>();
		supervision
				.add("#Result > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(4)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}
		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase9_cleankids() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.cleankids.de/magazin/produktrueckrufe";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "text";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#post-59858 > div:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		Vector<String> supervision = new Vector<String>();
		supervision.add("#post-59850 > div:nth-child(2)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase9_cleankids_textnode() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.cleankids.de/magazin/produktrueckrufe";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		HtmlTextNodeUtil textNodeUtil = new HtmlTextNodeUtil();
		doc = textNodeUtil.enrichMarkableTextNode(doc);

		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "text";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#post-59843 > div:nth-child(2) > dlyxtspan:nth-child(3)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		Vector<String> supervision = new Vector<String>();
		supervision
				.add("#post-59839 > div:nth-child(2) > dlyxtspan:nth-child(3)");

		for (String selector : supervision) {
			BlockLocation location = new BlockLocation();
			selector = doc.select(selector).first().cssSelector();
			location.type = BlockLocationType.cssSelector;
			location.value = selector;
			b.supervised.add(location);
		}

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

	@Test
	public void testCase10_ages() throws OpenPageUrlException,
			UnsupportedBlockLocationException, ColumnTemplateException {

		String url = "http://www.ages.at/produktwarnungen/produktkategorie/lebensmittel/4/";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		String css = "body";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		String baseUri = "";
		Document doc = Jsoup.parse(driver.getPageSource());

		baseUri = doc.baseUri();
		String currentUrl = url;
		if (baseUri == null || baseUri.isEmpty())
			baseUri = GetHost.guessBaseUrl(currentUrl);
		doc.setBaseUri(baseUri);

		HtmlTextNodeUtil textNodeUtil = new HtmlTextNodeUtil();
		doc = textNodeUtil.enrichMarkableTextNode(doc);

		
		List<BlockConfig> configs = new ArrayList<BlockConfig>();

		// REFERENCE
		BlockConfig b = new BlockConfig();
		b.name = "title";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div > section:nth-child(1) > div > section:nth-child(3) > header:nth-child(1) > h2:nth-child(3)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();

		configs.add(b);

		b = new BlockConfig();
		b.name = "datum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div > section:nth-child(1) > div > section:nth-child(3) > header:nth-child(1) > span:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		b = new BlockConfig();
		b.name = "grund";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div > section:nth-child(1) > div > section:nth-child(3) > div:nth-child(2) > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		b = new BlockConfig();
		b.name = "producer";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div > section:nth-child(1) > div > section:nth-child(3) > div:nth-child(2) > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		b = new BlockConfig();
		b.name = "ablaufdatum";
		b.type = BlockType.timeBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div > section:nth-child(1) > div > section:nth-child(3) > div:nth-child(2) > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(3) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		b = new BlockConfig();
		b.name = "chargenr";
		b.type = BlockType.keyvalueBlock;
		b.location = new BlockLocation();
		b.location.type = BlockLocationType.cssSelector;
		b.location.value = "#main > div > div > section:nth-child(1) > div > section:nth-child(3) > div:nth-child(2) > div:nth-child(2) > table:nth-child(1) > tbody:nth-child(2) > tr:nth-child(4) > td:nth-child(2)";
		b.location.value = doc.select(b.location.value).first().cssSelector();
		b.supervised = new ArrayList<BlockLocation>();
		// SUPERVISION

		configs.add(b);

		Element element = doc.select(css).first();
		HashSet<BlockLocation> tobeCheckedCssSelector = new HashSet<>();
		for (Element child : element.getAllElements()) {
			BlockLocation lo = new BlockLocation();
			lo.type = BlockLocationType.cssSelector;
			lo.value = child.cssSelector();
			lo.value = lo.value.replace(" ", "");
			tobeCheckedCssSelector.add(lo);
		}

		driver.close();

		HashMap<String, List<BlockConfigWrapper>> data = doProcess(configs,
				tobeCheckedCssSelector);

		List<String> rows = new ArrayList<>();
		rows.addAll(data.keySet());
		Collections.sort(rows);
		System.out.println("total datasets: " + rows.size());
		for (String key : rows) {
			System.out.println("\nrow: " + key);
			List<BlockConfigWrapper> row = data.get(key);
			for (BlockConfigWrapper blwrapper : row) {

				String value = doc.select(blwrapper.blockConfig.location.value)
						.first().text();

				System.out.println("name: " + blwrapper.blockConfig.name
						+ "     value: " + value);

			}
		}

	}

}
