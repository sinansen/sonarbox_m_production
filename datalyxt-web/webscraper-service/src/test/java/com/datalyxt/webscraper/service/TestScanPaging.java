package com.datalyxt.webscraper.service;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.PagingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestScanPaging {
	@Test
	public void testPaing() {
		String pageURL = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
		PagingConfig pagingConfig = new PagingConfig();
		pagingConfig.maxPage = 3;
		pagingConfig.pageLabelCssPath = "#searchResultIndex";
		WebDriver driver = WebDriverFactory.createDriver(BrowserDriverType.firefox_windows);
		List<String> pages = new ArrayList<String>();
		PageNavigationUtil navigationUtil = new PageNavigationUtil();
		try {
			navigationUtil.navigateToPage(driver, null, pageURL);
			String pageSource = driver.getPageSource();
			Document pagedoc = Jsoup.parse(pageSource);
			String baseUri = pagedoc.baseUri();
			String currentUrl = driver.getCurrentUrl();
			if (baseUri == null || baseUri.isEmpty())
				baseUri = GetHost.guessBaseUrl(currentUrl);
			pagedoc.setBaseUri(baseUri);

			Elements elements = pagedoc.select(pagingConfig.pageLabelCssPath);
			System.out.println(elements.size());
			if (!elements.isEmpty()) {
				Element element = elements.first();
				System.out.println(element.cssSelector());
				if (isList(element.tagName())) {

					Elements children = element.children();
					System.out.println(children.size());
					for (Element child : children)
						if (child.tagName().equals("li")) {

							int index = child.elementSiblingIndex();
							System.out.println("page index: " + index);
							Elements links = child.getElementsByAttribute("href");
							if(!links.isEmpty()){
							pages.add(links.first().attr("href"));}
						}
				}
			}
for(String page : pages){
	System.out.println(page);
}
		} catch (OpenPageUrlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean isList(String tagName) {
		if ("ul".equals(tagName) || "ol".equals(tagName))
			return true;
		return false;
	}

	class PageIndex {
		public List<String> pages = new ArrayList<String>();
	}
}
