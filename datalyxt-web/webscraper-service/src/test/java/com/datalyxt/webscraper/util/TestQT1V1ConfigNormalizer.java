package com.datalyxt.webscraper.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.datalyxt.webscraper.model.page.Page;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class TestQT1V1ConfigNormalizer {

	@Test
	public void testValidateJson() {
		List<String> files = new ArrayList<>();
		// files.add("rapex_case1_1.json");
//		files.add("rapex_case1_1n.json");
		files.add("test_n.json");
		// files.add("rapex_case1_3.json");
		// files.add("rapex_case1_4.json");
		// files.add("baua_case1_1n.json");
		// files.add("baua_case1_2.json");
		// files.add("baua_case1_3.json");
		// files.add("baua_case1_4.json");
		// files.add("baua_case1_5.json");
		// files.add("baua_case1_6.json");
		// files.add("bfarm_case1_1n.json");
		// files.add("ages_case1_1.json");
		// files.add("cleankids_case1_1n.json");
//		 files.add("cleankids_case1_1n.json");
		// files.add("visbayer_case1_1.json");
		// files.add("visbayer_case1_1n.json");
//		 files.add("webgate_case1_1n.json");
		// files.add("lebensmittel_case1_1.json");
//		files.add("recall.json");

		String directory = "testcase/";

		InputStream in = TestQT1V1ConfigNormalizer.class.getClassLoader()
				.getResourceAsStream("dataset_filter_operator.json");
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		Reader reader = new InputStreamReader(in);
		Type listType = new TypeToken<HashSet<String>>() {
		}.getType();
		HashSet<String> operators = gson.fromJson(reader, listType);

		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		CleanHtmlDocumentScript scriptEnableID = new CleanHtmlDocumentScript();
		scriptEnableID.removeElementCssClassAttr = false;
		scriptEnableID.removeElementIdAttr = false;

		String pageURL = "http://www.vis.bayern.de/produktsicherheit/herstellerinfos/";
		pageURL = "http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/warnung/start/bvllmwde.p_oeffentlicher_bereich.ss_alle_warnungen?_opensaga_navigationId=bvllmwde.p_oeffentlicher_bereich.n_alle_warnungen";
		pageURL = "http://www.cleankids.de/magazin/produktrueckrufe/ruckrufe-lebensmittel";
		pageURL = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
		pageURL = "http://www.baua.de/de/Produktsicherheit/Produktinformationen/Datenbank/Datenbank";
		pageURL = "http://www.ages.at/produktwarnungen/produktkategorie/lebensmittel/";
		pageURL = "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";
		pageURL = "https://webgate.ec.europa.eu/rasff-window/consumers/?event=getListByCountry&country=DE";
		
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		// driver.get(pageURL);
		// JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
		// String pageSource = (String) jsdriver
		// .executeScript("return document.documentElement.outerHTML;");

		Page page = new Page();
		// Link link = new Link();
		// link.url = pageURL;
		// link.domain = GetHost.getDomain(link.url);
		// page.pageUrl = link;
		//
		// page.setHtmlSelen(pageSource);
		QT1V1ConfigService qt1v1ConfigService = new QT1V1ConfigService();

		for (String filename : files) {
			in = TestQT1V1ConfigNormalizer.class.getClassLoader().getResourceAsStream(
					directory + filename);
			reader = new InputStreamReader(in);
			BlockConfigMain mainConfig = gson.fromJson(reader,
					BlockConfigMain.class);
			System.out.println(gson.toJson(mainConfig, BlockConfigMain.class));
			try {
				System.out.println("\ncheck " + filename);
				
				mainConfig = qt1v1ConfigService.getNormalizedMainConfig(mainConfig, driver);
				
				System.out.println(gson.toJson(mainConfig,
						BlockConfigMain.class));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		driver.close();

	}

}
