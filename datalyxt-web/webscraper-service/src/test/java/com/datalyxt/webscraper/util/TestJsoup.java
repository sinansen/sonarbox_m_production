package com.datalyxt.webscraper.util;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

public class TestJsoup {
	@Test
	public void testLink() throws IOException {
		String url = "http://www.tvmovie.de";
		print("Fetching %s...", url);

		Document doc = Jsoup.connect(url).get();
		Elements links = doc.select("a[href]");
		Elements media = doc.select("[src]");
		Elements imports = doc.select("link[href]");

		print("\nMedia: (%d)", media.size());
		for (Element src : media) {
			if (src.tagName().equals("img"))
				print(" * %s: <%s> %sx%s (%s)", src.tagName(),
						src.attr("abs:src"), src.attr("width"),
						src.attr("height"), trim(src.attr("alt"), 20));
			else
				print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));
		}

		print("\nImports: (%d)", imports.size());
		for (Element link : imports) {
			print(" * %s <%s> (%s)", link.tagName(), link.attr("abs:href"),
					link.attr("rel"));
		}

		print("\nLinks: (%d)", links.size());
		for (Element link : links) {
			print(" * a: <%s>  (%s)", link.attr("abs:href"),
					trim(link.text(), 35));
		}
	}

	private void print(String msg, Object... args) {
		System.out.println(String.format(msg, args));
	}

	private String trim(String s, int width) {
		if (s.length() > width)
			return s.substring(0, width - 1) + ".";
		else
			return s;
	}
	@Test
	public void testCharset() throws IOException {
		String url = "http://www.spiegel.de";
		print("Fetching %s...", url);

		Document doc = Jsoup.connect(url).get();
		Elements els = doc.select("meta[content*=charset]");
	
		for (Element link : els) {
			String[] values = link.attr("content").split(";");
			for(String value : values){
				if(value.contains("charset")){
					String charset = value.replace("charset", "").replace("=", "").trim();
					System.out.println(charset);
				}
			}
		}
	}
	@Test
	public void testMatch() throws IOException{
		String url = "http://www.spiegel.de";
		url = "https://www.migros.ch/de/kontakt/warenrueckrufe.html";
		print("Fetching %s...", url);

		Document doc = Jsoup.connect(url).get();
		String regex = "setzten.*ein";
		regex = "ruft.*zurück";
		Elements eles = doc.select(":matchesOwn("+regex+")");
		if(eles.isEmpty())
			System.out.println("not found");
		else
		   for(Element ele : eles)
			   System.out.println("\n"+ele.tagName()+" : "+ele.ownText());
	}

}
