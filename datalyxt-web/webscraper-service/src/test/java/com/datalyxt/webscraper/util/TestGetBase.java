package com.datalyxt.webscraper.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.util.GetBase;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestGetBase {
	@Test
	public void testGetBase() throws OpenPageUrlException,
			HtmlDocumentCreationException {
		String url = "http://www.lebensmittelwarnung.de/bvl-lmw-de/app/process/warnung/start/bvllmwde.p_oeffentlicher_bereich.ss_aktuelle_warnungen";
//url = "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.weeklyOverview&web_report_id=1851&selectedTabIdx=1";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		pageNavigationUtil.navigateToPage(driver, null, url);
		Document doc1 = Jsoup.parse(driver.getPageSource());
		Document doc2 = HtmlDocumentUtil.getDocumentFromSource(url,
				driver.getPageSource(), null);

		GetBase getbase = new GetBase();
		Elements hrefs = doc1.select("a[href]");
		System.out.println("guess base: "+GetHost.guessBaseUrl(url));
		System.out.println("domain: "+GetHost.getProtocalHost(url));
		for (Element ele : hrefs) {
			System.out.println("\n\nhref: " + ele.attr("href"));
			String absurl = getbase.getCalculatedBase(ele.attr("href"),
					GetHost.guessBaseUrl(url), GetHost.getProtocalHost(url), url);
			System.out.println("cal abs: " + absurl);
			System.out.println("abs: " + ele.absUrl("href"));
		}

		System.out.println("\n\n ------------ with guess base ---------------");
		hrefs = doc2.select("a[href]");
		for (Element ele : hrefs) {

			System.out.println("\n\nhref" + ele.attr("href"));
			String absurl = getbase.getCalculatedBase(ele.attr("href"),
					GetHost.guessBaseUrl(url), GetHost.getDomain(url), url);
			System.out.println("base+ref: " + absurl);
			System.out.println("abs: " + ele.absUrl("href"));
		}

	}
	
	@Test
	public void testGetBaseWithoutparmas() throws OpenPageUrlException,
			HtmlDocumentCreationException {
		String url = "http://www.lebensmittelwarnung.de";

		System.out.println("guess base: "+GetHost.guessBaseUrl(url));
	

	}
}
