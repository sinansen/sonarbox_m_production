package com.datalyxt.webscraper.service.rule.qt1v1.multirow;

import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.paging.PagingElement;
import com.datalyxt.production.webscraper.model.paging.PagingIndex;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.production.webscraper.service.block.BlockFactory;
import com.datalyxt.production.webscraper.service.block.content.paging.PagingBlockVisitStrategy;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.ValidLink;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.Page;

public class SingleRowMultiColumn {

	@Test
	public void testCase1_bfarm() throws ColumnTemplateException,
			OpenPageUrlException, UnsupportedBlockLocationException,
			HtmlDocumentCreationException, PagingExtractionException,
			NullBlockLocationException, DefinedBlockNotFound,
			BlockExtractionException, TimeBlockException,
			UnknownBlockTypeException {

		String url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
		url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html?gtp=3494768_list%253D2";
		String pagingSelector = "body:nth-child(2) > div:nth-child(1) > div > div:nth-child(5) > div:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > ul:nth-child(3)";
		url = "http://www.bfarm.de/SiteGlobals/Forms/Suche/Filtersuche_Produktgruppe_Formular.html";
		url = "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";
		pagingSelector = "body:nth-child(2) > div:nth-child(1) > div:nth-child(8) > div > div > section > div:nth-child(5) > div > div > div:nth-child(7) > ul:nth-child(1)";
		pagingSelector = "body:nth-child(2) > div:nth-child(1) > div:nth-child(8) > div > div > section > div:nth-child(4) > div > div > div:nth-child(7) > ul:nth-child(1)";
		// elementSelector =
		// "body:nth-child(2) > div:nth-child(1) > div:nth-child(8) > div > div > section > div:nth-child(5) > div > div > div:nth-child(7) > ul:nth-child(1) > li:nth-child(1) > a";

		HashMap<String, PagingBlock> visitedPaging = new HashMap<>();
		String currentPageURL = url;

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, null, url);

		PagingBlockVisitStrategy pagingBlockVisitStrategy = new PagingBlockVisitStrategy();

		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		Page htmlPage = new Page();
		htmlPage.setHtmlSelen(driver.getPageSource());
		htmlPage.pageUrl = new Link();
		htmlPage.pageUrl.url = url;

		BlockLocation location = new BlockLocation();
		location.type = BlockLocationType.cssSelector;
		location.value = pagingSelector;
		BlockFactory f = new BlockFactory();
		BlockConfig blockConfig = new BlockConfig();
		blockConfig.type = BlockType.pagingBlock;

		PagingBlock block = (PagingBlock) f.createBlock(blockConfig);
		pagingBlockVisitStrategy.visit(block, blockConfig, htmlPage, script);

		visitedPaging.put(currentPageURL, block);
		PagingElement next = getNextPagingLink(visitedPaging, currentPageURL);
		int count = 0;
		while(next!=null && count <20) {
			count ++;
			System.out.println("txt: "+next.hrefDescription+"  visit: "+next.href);
			pageNavigationUtil.navigateToPage(driver, null, next.href);
			htmlPage = new Page();
			htmlPage.setHtmlSelen(driver.getPageSource());
			htmlPage.pageUrl = new Link();
			htmlPage.pageUrl.url = next.href;
			currentPageURL = htmlPage.pageUrl.url;
			block = (PagingBlock) f.createBlock(blockConfig);
			pagingBlockVisitStrategy.visit(block, blockConfig, htmlPage, script);
			visitedPaging.put(currentPageURL, block);
			next = getNextPagingLink(visitedPaging, currentPageURL);
		}
	}

	private PagingElement getNextPagingLink(
			HashMap<String, PagingBlock> visitedPaging, String currentPageURL) {
		PagingBlock pagingBlock = visitedPaging.get(currentPageURL);
		PagingElement next = null;
		if (visitedPaging.size() == 1) {
			PagingElement firstHref = null;
			for (int i = 0; i < (pagingBlock.indexPages.size() - 1); i++) {
				PagingIndex index = pagingBlock.indexPages.get(i);
				if (firstHref == null && ValidLink.isHttp(index.pagingElement.href)) {
					firstHref = index.pagingElement;
				}

				if (index.pagingElement.href.equals(currentPageURL)) {
					PagingIndex indexRight = pagingBlock.indexPages.get(i + 1);
					String href = indexRight.pagingElement.href;
					if (ValidLink.isHttp(href)) {
						next = indexRight.pagingElement;
						break;
					}
				}
			}

			if (next == null && !pagingBlock.indexPages.isEmpty()) {
				
				next = firstHref;
			}
			return next;
		}

		for (PagingBlock blocks : visitedPaging.values()) {
			for (int i = 0; i < (blocks.indexPages.size() - 1); i++) {
				PagingIndex index = blocks.indexPages.get(i);
				if (index.pagingElement.href.equals(currentPageURL)) {
					PagingIndex indexRight = blocks.indexPages.get(i + 1);
					String href = indexRight.pagingElement.href;
					if (ValidLink.isHttp(href)) {
						next = indexRight.pagingElement;
						break;
					}
				}
			}
		}
		return next;
	}

	private void print(Collection<PagingIndex> collection) {
		for (PagingIndex p : collection) {
			System.out.println("****************************");
			if (p.pagingElement != null) {
				System.out.println("HREF\t:" + p.pagingElement.href);
				System.out.println("Text\t:" + p.pagingElement.hrefDescription);
			}
			System.out.println("Index\t:" + p.indexSelectorSiblingPosition);
			System.out.println("iteratedEle\t:" + p.iteratedElement);
			System.out.println("SELECTOR\t:" + p.indexSelector);
		}
	}
}
