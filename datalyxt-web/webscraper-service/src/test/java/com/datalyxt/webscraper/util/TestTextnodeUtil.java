package com.datalyxt.webscraper.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.extraction.URLNormalizerException;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlTextNodeUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestTextnodeUtil {
	@Test
	public void testEnrichment() throws OpenPageUrlException,
			URLNormalizerException {
		String url = "http://getknit.de/testcases/hallo.html";
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);

		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();

		pageNavigationUtil.navigateToPage(driver, null, url);
		String html = driver.getPageSource();

		Document doc = Jsoup.parse(html);
		String baseURI = doc.baseUri();
		if (baseURI == null || baseURI.isEmpty()) {
			baseURI = GetHost.guessBaseUrl(url);
		}
		doc.setBaseUri(baseURI);

		HtmlTextNodeUtil textNodeUtil = new HtmlTextNodeUtil();

		doc = textNodeUtil.enrichMarkableTextNode(doc);
		Element body = doc.body();

		System.out.println(body.html());

	}


}
