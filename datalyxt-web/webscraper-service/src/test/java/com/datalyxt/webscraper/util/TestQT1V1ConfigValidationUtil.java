package com.datalyxt.webscraper.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Test;

import com.datalyxt.production.exception.webscraper.BlockConfigValidationException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.util.QT1V1ConfigValidationUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class TestQT1V1ConfigValidationUtil {

	@Test
	public void testValidateJson() {
		List<String> files = new ArrayList<>();
//		files.add("rapex_case1_1.json");
//		files.add("rapex_case1_2.json");
//		files.add("rapex_case1_3.json");
//		files.add("rapex_case1_4.json");
//		files.add("baua_case1_1.json");
//		files.add("baua_case1_2.json");
//		files.add("baua_case1_3.json");
//		files.add("baua_case1_4.json");
//		files.add("baua_case1_5.json");
//		files.add("baua_case1_6.json");
		files.add("bfarm_case1_1.json");
//		files.add("ages_case1_1.json");
//		files.add("cleankids_case1_1.json");
//		files.add("cleankids_case1_2.json");
//		files.add("visbayer_case1_1.json");
//		files.add("webgate_case1_1.json");
//		files.add("lebensmittel_case1_1.json");

		String directory = "testcase/";
		
		InputStream in = TestQT1V1ConfigValidationUtil.class.getClassLoader()
				.getResourceAsStream("dataset_filter_operator.json");
		Gson gson = new Gson();
		Reader reader = new InputStreamReader(in);
		Type listType = new TypeToken<HashSet<String>>() {
		}.getType();
		HashSet<String> operators = gson.fromJson(reader, listType);
		QT1V1ConfigValidationUtil qt1v1ConfigValidationUtil = new QT1V1ConfigValidationUtil(
				operators);
		for (String filename : files) {
			in = TestQT1V1ConfigValidationUtil.class.getClassLoader()
					.getResourceAsStream(directory+filename);
			reader = new InputStreamReader(in);
			BlockConfigMain mainConfig = gson.fromJson(reader,
					BlockConfigMain.class);
			try {
				System.out.println("\ncheck "+filename);
				qt1v1ConfigValidationUtil.validate(mainConfig, false);
				System.out.println("passed: "+filename);
			} catch (BlockConfigValidationException | UnknownBlockTypeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
