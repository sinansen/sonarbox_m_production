package com.datalyxt.webscraper.util;

import org.junit.Test;

import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.webmodel.http.HttpConfig;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.service.action.FileDownloader;

public class TestDownloadFile {
	@Test
	public void test() throws DownloadFileException {
		String url = "http://www.chicco.de/content/dam/chicco/de/de/pdf/rueckrufaktionen/Physiological-Chicco.pdf";
		url = "http://cf-prd.cannondale.com/~/media/Files/PDF/Dorel/Cannondale/Common/RECALLS/2001_carbon_lefty_recall_en.ashx?d=20151125T190238Z&la=en&vs=1";
		FileDownloader downloadFileAction = new FileDownloader();
		HttpConfig cof = new HttpConfig();
		cof.proxyEnabled = false;
		ActionResultFile file = downloadFileAction.download(url,cof);
		System.out.println("name "+file.dName);
	}
}
