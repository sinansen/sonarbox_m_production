package com.datalyxt.webscraper.service.api;

import static org.junit.Assert.*;

import org.junit.Test;

import com.datalyxt.util.GetHost;
import com.datalyxt.util.SubDomain;

public class TestSubDomain {
	@Test
	public void testSubdomain() {
		String url = "http://www.exmaple.com";
		String subdomainurl = "http://sub.sub.exmaple.com";
		String domain = GetHost.getDomain(url);
		boolean subdomain = SubDomain.subDomain(subdomainurl, domain);
		assertTrue(subdomain);
	}
}
