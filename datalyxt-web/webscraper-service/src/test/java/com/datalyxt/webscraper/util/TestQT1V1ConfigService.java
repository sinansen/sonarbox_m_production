package com.datalyxt.webscraper.util;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import org.junit.Test;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.google.gson.Gson;

public class TestQT1V1ConfigService {
	@Test
	public void testGetValidTemplate() throws HtmlDocumentCreationException {
		QT1V1ConfigService service = new QT1V1ConfigService();

		String directory = "testcase/";
		String filename = "cleankids_case1_1.json";

		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		String pageURL = "http://www.cleankids.de/magazin/produktrueckrufe/ruckrufe-lebensmittel";
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		try {
			pageNavigationUtil.navigateToPage(driver, new BrowsingConfig(), pageURL);
		} catch (OpenPageUrlException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String pageSource = driver.getPageSource();
		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();

		Gson gson = new Gson();

		InputStream in = TestQT1V1ConfigService.class.getClassLoader()
				.getResourceAsStream(directory + filename);
		Reader reader = new InputStreamReader(in);
		BlockConfigMain mainConfig = gson.fromJson(reader,
				BlockConfigMain.class);
		System.out.println("\ncheck " + filename);

		List<PageBlockConfig> cfgs = service.getBlockConfigByHop(0, mainConfig);
		List<PageBlockConfig> pageBlockConfigs = service
				.selectValidPageBlockConfig(pageURL, pageSource, cfgs, script, false);
		if (pageBlockConfigs.isEmpty())
			System.out.println("no valid cfg found");
		else
			System.out.println("found a valid cfg!");

	}
}
