package com.datalyxt.production.webscraper.service.condition;

import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DataBlock;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.KeyvalueBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.TimeBlock;

public class DatasetTextService {
	public String getDatasetText(DatasetBlock block) {
		String text = "";
		text = combineBlockText(block, text);
		return text;
	}

	public String combineBlockText(Block block, String text) {
		if (block.type.equals(BlockType.datasetBlock)) {
			DatasetBlock datasetBlock = (DatasetBlock) block;
			if (datasetBlock.blocks != null)
				for (Block subblock : datasetBlock.blocks) {
				  text = combineBlockText(subblock, text);
				}
		} else {
			if (block.type.equals(BlockType.dataBlock)) {

				DataBlock parsedBlock = (DataBlock) block;
				text = text + parsedBlock.blockText;

			} else if (block.type.equals(BlockType.timeBlock)) {

				TimeBlock parsedBlock = (TimeBlock) block;

				text = text +parsedBlock.blockText;

			} else if (block.type.equals(BlockType.keyvalueBlock)) {

				KeyvalueBlock parsedBlock = (KeyvalueBlock) block;

				text = text +parsedBlock.blockText;

			} else if (block.type.equals(BlockType.linkBlock)) {
				LinkBlock parsedBlock = (LinkBlock) block;
				text = text + parsedBlock.blockText;
			}

		}
		
		return text;
	}
}
