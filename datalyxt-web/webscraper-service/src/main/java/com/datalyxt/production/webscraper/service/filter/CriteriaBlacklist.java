package com.datalyxt.production.webscraper.service.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;

public class CriteriaBlacklist implements Criteria {
	
	@Override
	public HashMap<String, LinkBlock> meetCriteria(HashMap<String, LinkBlock> linkBlocks, Filter filter) {

		if (!filter.enableBlackList)
			return linkBlocks;

		List<String> removed = new ArrayList<String>();
		for (String links : linkBlocks.keySet()) {
			for (String blackUrl : filter.blacklist) {
				if (links.contains(blackUrl))
					removed.add(links);
			}
		}
		if(!removed.isEmpty()){
			for(String removedLink : removed){
				linkBlocks.remove(removedLink);
			}
		}
		return linkBlocks;
	}

}
