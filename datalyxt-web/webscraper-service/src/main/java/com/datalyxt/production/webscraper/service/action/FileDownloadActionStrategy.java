package com.datalyxt.production.webscraper.service.action;

import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webmodel.http.HttpConfig;
import com.datalyxt.production.webscraper.model.action.ActionMetaConstant;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;

public class FileDownloadActionStrategy implements FileActionStrategy {

	FileDownloader downloadFileAction = new FileDownloader();
	private HttpConfig config;

	public FileDownloadActionStrategy(HttpConfig config) {
		this.config = config;

	}

	@Override
	public ActionResultFile doAction(String pageUrl, BlockAction action, int timeout)
			throws DownloadFileException, UnknownBlockTypeException {
		if (!BlockActionType.fileStorage.equals(action.type))
			return null;
		try {

			ActionResultFile result = downloadFileAction
					.download(pageUrl,config);

			result.createdAt = System.currentTimeMillis();

			result.type = ActionMetaConstant.pdf;
			result.status = ActionMetaConstant.file_created;
			result.size = result.content.length;

			return result;
		} catch (Exception e) {
			throw new DownloadFileException(
					"error in FileStorageActionStrategy->doAction: "
							+ e.getMessage(), e);
		}

	}

}
