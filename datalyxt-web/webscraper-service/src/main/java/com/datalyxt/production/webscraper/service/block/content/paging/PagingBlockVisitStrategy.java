package com.datalyxt.production.webscraper.service.block.content.paging;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.paging.PagingElement;
import com.datalyxt.production.webscraper.model.paging.PagingIndex;
import com.datalyxt.production.webscraper.model.paging.PagingIndexTemplate;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.production.webscraper.service.block.content.multirow.IteratedToken;
import com.datalyxt.production.webscraper.service.block.content.multirow.Selector;
import com.datalyxt.production.webscraper.service.location.ContentBlockVisitStrategy;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PagingExtractor;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.Page;

public class PagingBlockVisitStrategy implements ContentBlockVisitStrategy {

	@Override
	public void visit(Block block, BlockConfig config, Page htmlPage,
			CleanHtmlDocumentScript script) throws NullBlockLocationException,
			DefinedBlockNotFound, BlockExtractionException, TimeBlockException,
			HtmlDocumentCreationException, PagingExtractionException {

		if (!BlockType.pagingBlock.equals(block.type))
			return;

		Document doc = HtmlDocumentUtil.getDocumentFromSource(
				htmlPage.pageUrl.url, htmlPage.getHtmlSelen(), script);

		Element element = doc.select(config.location.value).first();

		HashMap<String, Selector> tobeCheckedCssSelector = new HashMap<>();
		for (Element child : element.getAllElements()) {
			String lo = child.cssSelector();
			lo = lo.replace(" ", "");
			Selector selector = getSelector(lo);
			tobeCheckedCssSelector.put(selector.id, selector);
		}

		List<Selector> s = new ArrayList<Selector>();
		s.addAll(tobeCheckedCssSelector.values());
		List<PagingIndexTemplate> templates = extractPagingIndex(s);
		if (templates.isEmpty())
			throw new PagingExtractionException("no paging element found!");

		Collections.sort(templates);

		PagingIndexTemplate template = templates.get(0);

		PagingExtractor pagingExtractor = new PagingExtractor();
		ArrayList<PagingElement> pagingElements = pagingExtractor
				.getPagingLinks(htmlPage.pageUrl.url, doc, config.location.value);

		for (PagingIndex pagingIndex : template.indexes.values()) {
			boolean found = false;
			for (PagingElement pagingElement : pagingElements) {
				Selector indexLinkSelector = getSelector(pagingElement.selector);
				List<String> prefix = indexLinkSelector.allTokens.subList(0,
						template.iteratedElementPosition);

				if (Arrays.deepEquals(template.preElements.toArray(),
						prefix.toArray())) {
					IteratedToken token = indexLinkSelector.iterationToken
							.get(template.iteratedElementPosition);
					if (token != null
							&& token.value.equals(pagingIndex.iteratedElement)) {
						pagingIndex.pagingElement = pagingElement;
						found = true;
						break;
					}
				}
			}
			if (!found) {
				PagingElement pagingElement = new PagingElement();
				pagingElement.hrefDescription = doc
						.select(pagingIndex.indexSelector).first().text();
				pagingIndex.pagingElement = pagingElement;
			}

		}

		List<PagingIndex> list = new ArrayList<>();
		list.addAll(template.indexes.values());
		Collections.sort(list);

		PagingBlock parsedBlock = (PagingBlock) block;

		parsedBlock.indexPages = list;

		for (int i = 0; i < list.size(); i++) {
			PagingIndex ind = list.get(i);
			if (ind.pagingElement != null) {
				if (ind.pagingElement.href != null) {
					LinkBlock linkBlock = new LinkBlock();
					linkBlock.type = BlockType.linkBlock;

					String link;
					link = ind.pagingElement.href;

					if (!parsedBlock.linkBlocks.containsKey(link)) {
						Link linkObj = new Link();
						linkObj.url = link;
						linkObj.domain = GetHost.getDomain(link);
						linkObj.linkText = ind.pagingElement.hrefDescription;
						linkBlock.link = linkObj;
						linkBlock.text = linkObj.linkText;
						parsedBlock.linkBlocks.put(linkObj.url, linkBlock);
					}

				}
			}
		}

	}

	public List<PagingIndexTemplate> extractPagingIndex(
			List<Selector> candidates) throws PagingExtractionException {

		HashMap<Integer, List<Selector>> selectors = new HashMap<Integer, List<Selector>>();

		for (Selector selector : candidates) {
			int tokenAmount = selector.allTokens.size();
			if (selectors.containsKey(tokenAmount)) {
				List<Selector> s = selectors.get(tokenAmount);
				s.add(selector);
			} else {
				List<Selector> s = new ArrayList<>();
				s.add(selector);
				selectors.put(tokenAmount, s);
			}

		}
		List<PagingIndexTemplate> pagingIndexTemplates = new ArrayList<>();

		for (List<Selector> s : selectors.values()) {
			generateTemplates(s, pagingIndexTemplates);
		}
		return pagingIndexTemplates;
	}

	public void generateTemplates(List<Selector> selectors,
			List<PagingIndexTemplate> pagingIndexTemplates) {
		HashSet<Integer> matchedIndexes = new HashSet<Integer>();

		for (int i = 0; i < selectors.size(); i++) {
			Selector ref = selectors.get(i);
			for (int j = i + 1; j < selectors.size(); j++) {

				Selector comp = selectors.get(j);

				int position = -1;

				for (int k = 0; k < comp.allTokens.size(); k++) {

					String st = comp.allTokens.get(k);

					if (!ref.allTokens.contains(st)) {
						position = k;
						break;
					}
				}
				if (position >= 0) {
					List<String> refPreElements = ref.allTokens.subList(0,
							position);
					List<String> compPreElements = comp.allTokens.subList(0,
							position);
					if (Arrays.deepEquals(refPreElements.toArray(),
							compPreElements.toArray())) {
						List<String> refPostElements = new ArrayList<String>();
						if (position < (ref.allTokens.size() - 1))
							refPostElements = ref.allTokens.subList(
									position + 1, ref.allTokens.size());

						List<String> compPostElements = new ArrayList<String>();
						if (position < (comp.allTokens.size() - 1))
							compPostElements = comp.allTokens.subList(
									position + 1, ref.allTokens.size());
						if (Arrays.deepEquals(refPostElements.toArray(),
								compPostElements.toArray())) {

							if (!matchedIndexes.contains(i)) {

								addToTemplate(pagingIndexTemplates, position,
										refPreElements, refPostElements,
										ref.allTokens.get(position), ref);
								matchedIndexes.add(i);
							}
							if (!matchedIndexes.contains(j)) {

								addToTemplate(pagingIndexTemplates, position,
										compPreElements, compPostElements,
										comp.allTokens.get(position), comp);
								matchedIndexes.add(j);
							}
						}
					}
				}
			}
		}

	}

	private boolean addToTemplate(
			List<PagingIndexTemplate> pagingIndexTemplates, int position,
			List<String> preElements, List<String> postElements,
			String iteratedElement, Selector ele) {
		boolean foundsame = false;

		for (PagingIndexTemplate pagingIndexTemplate : pagingIndexTemplates) {

			if (pagingIndexTemplate.iteratedElementPosition == position)

				if (Arrays.deepEquals(
						pagingIndexTemplate.preElements.toArray(),
						preElements.toArray()))
					if (Arrays.deepEquals(
							pagingIndexTemplate.postElements.toArray(),
							postElements.toArray())) {
						PagingIndex pagingIndex = new PagingIndex();
						pagingIndex.indexSelector = ele.selector;
						pagingIndex.indexSelectorSiblingPosition = getSiblingIndex(iteratedElement);
						pagingIndex.iteratedElement = iteratedElement;

						pagingIndexTemplate.indexes.put(
								pagingIndex.indexSelector, pagingIndex);

						foundsame = true;
					}
		}

		if (!foundsame) {
			PagingIndexTemplate pagingIndexTemplate = new PagingIndexTemplate();

			pagingIndexTemplate.iteratedElementPosition = position;
			pagingIndexTemplate.preElements.addAll(preElements);
			pagingIndexTemplate.postElements.addAll(postElements);

			PagingIndex pagingIndex = new PagingIndex();
			pagingIndex.indexSelector = ele.selector;
			pagingIndex.indexSelectorSiblingPosition = getSiblingIndex(iteratedElement);
			pagingIndex.iteratedElement = iteratedElement;

			pagingIndexTemplate.indexes.put(pagingIndex.indexSelector,
					pagingIndex);

			pagingIndexTemplates.add(pagingIndexTemplate);

		}

		return foundsame;
	}

	private int getSiblingIndex(String iteratedElement) {
		String regex = "\\d+";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(iteratedElement);
		while (matcher.find()) {
			Integer index = Integer.valueOf(matcher.group());
			return index;
		}
		return -1;
	}

	public Selector getSelector(String cssSelector) {
		if (cssSelector != null && !cssSelector.isEmpty()) {

			cssSelector = cssSelector.replace(" ", "");

			String id = DigestUtils.md5Hex(cssSelector);
			Selector s = new Selector(id, cssSelector);
			s.initialize();

			return s;
		}
		return null;
	}
}
