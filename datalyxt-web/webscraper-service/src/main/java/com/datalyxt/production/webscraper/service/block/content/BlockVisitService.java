package com.datalyxt.production.webscraper.service.block.content;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.service.block.BlockFactory;
import com.datalyxt.production.webscraper.service.block.content.multirow.BlockConfigWrapper;
import com.datalyxt.production.webscraper.service.block.content.multirow.BlockTrainingData;
import com.datalyxt.production.webscraper.service.block.content.multirow.MultiRowDetectionContext;
import com.datalyxt.production.webscraper.service.block.content.multirow.RowBasedSelectorTemplate;
import com.datalyxt.production.webscraper.service.block.content.multirow.SupervisedMultiRowService;
import com.datalyxt.production.webscraper.service.location.ContentBlockVisitContext;
import com.datalyxt.webscraper.model.page.Page;
import com.google.gson.Gson;

public class BlockVisitService {

	BlockFactory blockFactory;
	ContentBlockVisitContext contentBlockVisitContext;
	SupervisedMultiRowService supervisedMultiRowService = new SupervisedMultiRowService();
	MultiRowDetectionContext multiRowDetectionContext = new MultiRowDetectionContext();

	public BlockVisitService(BlockFactory blockFactory,
			ContentBlockVisitContext contentBlockVisitContext) {
		this.blockFactory = blockFactory;
		this.contentBlockVisitContext = contentBlockVisitContext;
	}

	public Block visit(BlockConfig config, Page page,
			CleanHtmlDocumentScript script) throws UnknownBlockTypeException,
			NullBlockLocationException, DefinedBlockNotFound,
			BlockExtractionException, TimeBlockException,
			HtmlDocumentCreationException, PagingExtractionException {

		Block block = blockFactory.createBlock(config);

		if (contentBlockVisitContext.isContentBlock(block.type)) {
			block = contentBlockVisitContext.visitBlock(block, config, page,
					script);
			return block;
		} else {
			DatasetBlock datasetBlock = (DatasetBlock) block;
			if (config.multiRow) {

			} else if (config.blocks != null)
				for (BlockConfig subconfig : config.blocks) {
					Block subBlock = visit(subconfig, page, script);
					datasetBlock.blocks.add(subBlock);
				}
		}
		return block;
	}

	public List<DatasetBlock> visitMultiRow(BlockConfig config, Page page,
			CleanHtmlDocumentScript script,
			Collection<BlockLocation> tobeCheckedCssSelector,
			List<RowBasedSelectorTemplate> templates)
			throws UnknownBlockTypeException, NullBlockLocationException,
			DefinedBlockNotFound, BlockExtractionException, TimeBlockException,
			UnsupportedBlockLocationException, ColumnTemplateException,
			HtmlDocumentCreationException, PagingExtractionException {

		if (!config.type.equals(BlockType.datasetBlock))
			throw new UnknownBlockTypeException("cannot create datasetblock");

		List<DatasetBlock> datasets = new ArrayList<DatasetBlock>();

		if (!config.multiRow)
			return datasets;
		if (config.blocks == null || config.blocks.isEmpty())
			return datasets;

		List<List<BlockConfig>> multiRowDataConfigs = processMulitRow(
				templates, config.blocks, tobeCheckedCssSelector);
		for (List<BlockConfig> list : multiRowDataConfigs) {
			DatasetBlock datasetBlock = (DatasetBlock) blockFactory
					.createBlock(config);
			for (BlockConfig cof : list) {
				if (contentBlockVisitContext.isContentBlock(cof.type)) {
					Block blockItem = blockFactory.createBlock(cof);
					blockItem = contentBlockVisitContext.visitBlock(blockItem,
							cof, page, script);
					datasetBlock.blocks.add(blockItem);
				} else
					throw new UnknownBlockTypeException(
							"cannot create datasetblock items for block type "
									+ cof.type);
			}
			datasets.add(datasetBlock);
		}
		return datasets;
	}

	private HashMap<String, BlockConfig> getColumnConfigs(
			List<BlockConfig> blocks) {
		HashMap<String, BlockConfig> columnConfigs = new HashMap<>();
		for (int i = 0; i < blocks.size(); i++) {
			BlockConfig cfg = blocks.get(i);
			if (cfg.column < 0)
				cfg.column = i;
			if (cfg.name != null) {
				columnConfigs.put(cfg.name, cfg);
			}
		}
		return columnConfigs;
	}

	public HashSet<String> getTemplatesParentSelectors(
			List<RowBasedSelectorTemplate> templates) {

		HashSet<String> parents = new HashSet<String>();

		for (RowBasedSelectorTemplate tmp : templates) {
			if (tmp.preIdentifier != null) {
				String s = String.join(">", tmp.preIdentifier);
				parents.add(s);
			}
		}
		return parents;
	}

	public List<RowBasedSelectorTemplate> getTemplates(BlockConfig config)
			throws UnsupportedBlockLocationException, ColumnTemplateException,
			UnknownBlockTypeException {

		if (!config.type.equals(BlockType.datasetBlock))
			throw new UnknownBlockTypeException("cannot create datasetblock");

		List<RowBasedSelectorTemplate> tmps = new ArrayList<RowBasedSelectorTemplate>();

		if (!config.multiRow)
			return tmps;
		if (config.blocks == null || config.blocks.isEmpty())
			return tmps;

		BlockTrainingData blockTrainingData = supervisedMultiRowService
				.addSupervisiedDataColumn(config.blocks);
		List<RowBasedSelectorTemplate> templates = supervisedMultiRowService
				.createRowBasedTemplate(multiRowDetectionContext,
						blockTrainingData);
		return templates;
	}

	private List<List<BlockConfig>> processMulitRow(
			List<RowBasedSelectorTemplate> templates,
			List<BlockConfig> columnConfigs,
			Collection<BlockLocation> tobeCheckedCssSelector)
			throws UnsupportedBlockLocationException, ColumnTemplateException {

		HashMap<String, List<BlockConfigWrapper>> data = new HashMap<String, List<BlockConfigWrapper>>();
		HashMap<String, BlockConfig> configs = getColumnConfigs(columnConfigs);
		Gson gson = new Gson();

		for (BlockLocation cssSelector : tobeCheckedCssSelector) {
			BlockConfigWrapper bloWrapper = supervisedMultiRowService
					.identifyIteratedData(cssSelector, templates);

			if (bloWrapper != null) {

				bloWrapper.blockConfig.follow = configs
						.get(bloWrapper.blockConfig.name).follow;
				bloWrapper.blockConfig.column = configs
						.get(bloWrapper.blockConfig.name).column;

				String key = gson.toJson(bloWrapper.row.toArray());
				List<BlockConfigWrapper> cofs = new ArrayList<BlockConfigWrapper>();
				if (data.containsKey(key))
					cofs = data.get(key);
				cofs.add(bloWrapper);
				data.put(key, cofs);
			}

		}
		List<List<BlockConfig>> rows = new ArrayList<List<BlockConfig>>();
		for (List<BlockConfigWrapper> wrappers : data.values()) {
			List<BlockConfig> cofs = new ArrayList<BlockConfig>();
			Collections.sort(wrappers);
			for (BlockConfigWrapper w : wrappers)
				cofs.add(w.blockConfig);
			rows.add(cofs);
		}
		return rows;
	}
}
