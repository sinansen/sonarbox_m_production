package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.exception.webscraper.ColumnTemplateException;

public class MultiRowDetectionContext {

	HashMap<TrainingDataType, MultiRowDetectionStrategy> strategies = new HashMap<>();

	public MultiRowDetectionContext() {
		strategies.put(TrainingDataType.singleElement,
				new SingleElementStrategy());
		strategies.put(TrainingDataType.singleColumn,
				new SingleColumnStrategy());
		strategies.put(TrainingDataType.singleRow, new SingleRowStrategy());
		strategies.put(TrainingDataType.multiElement,
				new MultiElementStrategy());
	}

	public List<RowBasedSelectorTemplate> createRowTemplate(
			BlockTrainingData blockTrainingData) throws ColumnTemplateException {
		MultiRowDetectionStrategy strategy = strategies
				.get(blockTrainingData.type);
		if (strategy == null)
			throw new ColumnTemplateException("unsupported training data type "
					+ blockTrainingData.type);
		return strategy.createRowTemplate(blockTrainingData);
	}

}
