package com.datalyxt.production.webscraper.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.datalyxt.production.exception.webscraper.BlockConfigValidationException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.config.DatasetFilter;
import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.util.ValidLink;

public class QT1V1ConfigValidationUtil {

	private HashSet<String> operators = new HashSet<>();

	public QT1V1ConfigValidationUtil(HashSet<String> operators) {
		this.operators.addAll(operators);
	}

	public void validatePage(PageBlockConfig pageBlockConfig,
			boolean validateTrainingData)
			throws BlockConfigValidationException, UnknownBlockTypeException {

		if (pageBlockConfig == null)
			throw new BlockConfigValidationException(
					"page block config is null!");

		if (pageBlockConfig.block == null || pageBlockConfig.block.isEmpty())
			throw new BlockConfigValidationException(
					"no block config is found for page "
							+ pageBlockConfig.url);

		validatePageUrl(pageBlockConfig);

		for (BlockConfig blockConfig : pageBlockConfig.block) {
			if (blockConfig.follow
					&& blockConfig.type.equals(BlockType.linkBlock))
			validateBlockConfig(blockConfig, false, pageBlockConfig.hop,
					 validateTrainingData);
		}

	}

	public void validate(BlockConfigMain blockConfigMain,
			boolean validateTrainingData)
			throws BlockConfigValidationException, UnknownBlockTypeException {
		if (blockConfigMain == null)
			throw new BlockConfigValidationException(
					"block config main is null !");

		if (blockConfigMain.config == null)
			throw new BlockConfigValidationException(
					"page block config is null!");

		if (blockConfigMain.config.isEmpty())
			throw new BlockConfigValidationException(
					"no page block config is found!");
		boolean isStartHopDefined = false;

		HashSet<Integer> hops = new HashSet<>();
		HashSet<Integer> follwedHops = new HashSet<>();
		for (PageBlockConfig pageBlockConfig : blockConfigMain.config) {
			if (pageBlockConfig.hop == 0)
				isStartHopDefined = true;
			validatePageUrl(pageBlockConfig);

			if (pageBlockConfig.block == null
					|| pageBlockConfig.block.isEmpty())
				throw new BlockConfigValidationException(
						"no block config is found for page hop "
								+ pageBlockConfig.hop);
			boolean follow = false;
			for (BlockConfig blockConfig : pageBlockConfig.block) {
				if (blockConfig.follow
						&& blockConfig.type.equals(BlockType.linkBlock))
					follow = true;
				validateBlockConfig(blockConfig, false, pageBlockConfig.hop,
						validateTrainingData);
			}
			if (follow)
				follwedHops.add(pageBlockConfig.hop + 1);
		}

		if (!isStartHopDefined)
			throw new BlockConfigValidationException("Start Hop not defined!");

		validateFollow(hops, follwedHops);

	}

	private void validatePageUrl(PageBlockConfig pageBlockConfig)
			throws BlockConfigValidationException {
		if (ValidLink.isEmptyURL(pageBlockConfig.url))
			throw new BlockConfigValidationException(
					"page url is null or empty!");

		if (!ValidLink.isURLLengthValid(pageBlockConfig.url, 500))
			throw new BlockConfigValidationException(
					"page url exceeds max length (500)!");

		if (!ValidLink.isHttp(pageBlockConfig.url))
			throw new BlockConfigValidationException(
					"page url is not valid http or https.");

	}

	private void validateBlockConfig(BlockConfig blockConfig,
			boolean isInDatasetBlock, int hop,
			boolean validateTrainingData)
			throws BlockConfigValidationException, UnknownBlockTypeException {

		if (blockConfig.filter != null)
			validateFilter(blockConfig.filter);

		if (!isInDatasetBlock)
			validateActions(blockConfig.action);

		if (blockConfig.type == null) {
			throw new BlockConfigValidationException("block type not defined!");
		} else {

			if (blockConfig.type.equals(BlockType.datasetBlock))
				validateDatasetBlockConfig(blockConfig, hop,
						validateTrainingData);
			else
				validateLocation(blockConfig.location);

			if (isInDatasetBlock) {
				if (blockConfig.type.equals(BlockType.keyvalueBlock))
					validateKeyValueBlockConfig(blockConfig);

				else if (blockConfig.type.equals(BlockType.timeBlock))
					validateTimeBlockConfig(blockConfig);

				else if (blockConfig.type.equals(BlockType.linkBlock))
					validateLinkBlockConfig(blockConfig);

				else if (blockConfig.type.equals(BlockType.dataBlock))
					validateDataBlockConfig(blockConfig);
				else
					throw new UnknownBlockTypeException(blockConfig.type.name());
			}

			if (blockConfig.supervised != null)
				validateSupervisedData(blockConfig.supervised);
		}

	}

	private void validateSupervisedData(List<BlockLocation> supervised)
			throws BlockConfigValidationException {
		for (BlockLocation location : supervised)
			validateLocation(location);

	}

	private void validateTimeBlockConfig(BlockConfig blockConfig)
			throws BlockConfigValidationException {
		if (blockConfig.name == null)
			throw new BlockConfigValidationException(
					"name of time block not defined!");
	}

	private void validateLinkBlockConfig(BlockConfig blockConfig)
			throws BlockConfigValidationException {
		if (blockConfig.name == null)
			throw new BlockConfigValidationException(
					"name of link block not defined!");
	}

	private void validateKeyValueBlockConfig(BlockConfig blockConfig)
			throws BlockConfigValidationException {
		if (blockConfig.name == null)
			throw new BlockConfigValidationException(
					"name of key-value block not defined!");
	}

	private void validateDatasetBlockConfig(BlockConfig blockConfig, int hop, boolean validateTrainingData)
			throws BlockConfigValidationException, UnknownBlockTypeException {

		if (blockConfig.blocks == null || blockConfig.blocks.isEmpty())
			throw new BlockConfigValidationException(
					"row is not defined for dataset block!");

		for (BlockConfig block : blockConfig.blocks) {
			if (validateTrainingData)
				if (block.supervised == null || block.supervised.isEmpty())
					throw new BlockConfigValidationException(
							"training data for dataset detection is not defined!");
			validateBlockConfig(block, true, hop,
					validateTrainingData);
		}
	}

	private void validateDataBlockConfig(BlockConfig blockConfig)
			throws BlockConfigValidationException {
		if (blockConfig.name == null)
			throw new BlockConfigValidationException(
					"name of data block not defined!");
	}

	private void validateFilter(Filter filter)
			throws BlockConfigValidationException {
		if (filter == null)
			return;
		if (filter.datasetFilter == null)
			return;
		for (DatasetFilter datasetFilter : filter.datasetFilter) {
			if (datasetFilter.name == null
					|| datasetFilter.name.trim().isEmpty())
				throw new BlockConfigValidationException(
						"name in DatasetFilter not defined!");
			if (datasetFilter.name.length() > 500)
				throw new BlockConfigValidationException(
						"name in DatasetFilter exceeds max length (500)!");
			if (datasetFilter.operator == null)
				throw new BlockConfigValidationException(
						"operator in DatasetFilter not defined!");
			if (!operators.contains(datasetFilter.operator))
				throw new BlockConfigValidationException(
						"operator in DatasetFilter is incorrect!");

			if (datasetFilter.value == null
					|| datasetFilter.value.trim().isEmpty())
				throw new BlockConfigValidationException(
						"value in DatasetFilter not defined!");

		}
	}

	private void validateActions(List<BlockAction> actions)
			throws BlockConfigValidationException {
		if (actions == null || actions.isEmpty())
			throw new BlockConfigValidationException(
					"block action not defined!");
		for (BlockAction action : actions)
			if (action.type == null)
				throw new BlockConfigValidationException(
						"block action type not defined!");
	}

	private void validateFollow(HashSet<Integer> hops,
			HashSet<Integer> followedHops)
			throws BlockConfigValidationException {
		Collection<Integer> definedConfigs = CollectionUtils.subtract(
				followedHops, hops);
		if (!definedConfigs.isEmpty()) {
			String notdefinedhops = (Arrays.toString(definedConfigs.toArray()));
			throw new BlockConfigValidationException(
					"no page block config defined for " + notdefinedhops);
		}
	}

	private void validateLocation(BlockLocation location)
			throws BlockConfigValidationException {
		if (location == null)
			throw new BlockConfigValidationException(
					"block location not defined! ");
		if (location.type == null)
			throw new BlockConfigValidationException(
					"block location type not defined!");
		if (location.value == null || location.value.trim().isEmpty())
			throw new BlockConfigValidationException(
					"block location value not defined!");
		if (location.value.length() > 1000)
			throw new BlockConfigValidationException(
					"block location value exceeds max length (1000)!");
	}
}
