package com.datalyxt.production.webscraper.service.filter;

import java.util.HashMap;

import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;

public class LinkBlockFilter {

	private CriteriaDomain criteriaDomain;
	private CriteriaURL criteriaURL;
	private CriteriaTypedLink criteriaTypedLink;
	private CriteriaBlacklist criteriaBlacklist;

	public LinkBlockFilter() {
		this.criteriaDomain = new CriteriaDomain();
		this.criteriaURL = new CriteriaURL();
		this.criteriaBlacklist = new CriteriaBlacklist();
		this.criteriaTypedLink = new CriteriaTypedLink();
	}

	public HashMap<String, LinkBlock> doFilter(
			HashMap<String, LinkBlock> linkBlocks, Filter filter) {
		if (filter == null || linkBlocks.isEmpty())
			return linkBlocks;

		HashMap<String, LinkBlock> filtered = criteriaDomain.meetCriteria(
				linkBlocks, filter);
		filtered = criteriaBlacklist.meetCriteria(filtered, filter);
		filtered = criteriaTypedLink.meetCriteria(filtered, filter);
		filtered = criteriaURL.meetCriteria(filtered, filter);
		return filtered;
	}
}
