package com.datalyxt.production.webscraper.service.location;

import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.webscraper.IncorrectCssSelectorException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.webscraper.model.page.Page;

public class BlockLocationNormalizer {
	public PageBlockConfig normalize(PageBlockConfig pageBlockConfig,
			Page page, CleanHtmlDocumentScript script)
			throws IncorrectCssSelectorException, HtmlDocumentCreationException {
		if (pageBlockConfig == null)
			return pageBlockConfig;
		HashSet<String> incorrectedSelectors = new HashSet<>();
		Document doc = HtmlDocumentUtil.getDocumentFromSource(page.pageUrl.url,
				page.getHtmlSelen(), script);
		PageBlockConfig n_pBlockConfig = new PageBlockConfig();
		n_pBlockConfig.hop = pageBlockConfig.hop;
		n_pBlockConfig.url = pageBlockConfig.url;
		n_pBlockConfig.block = new ArrayList<>();

		for (BlockConfig blockConfig : pageBlockConfig.block) {
			BlockConfig b = visitBlockLocation(blockConfig, doc,
					incorrectedSelectors, false);
			n_pBlockConfig.block.add(b);
		}
		if (!incorrectedSelectors.isEmpty())
			throw new IncorrectCssSelectorException("selectors: "
					+ incorrectedSelectors.toString() + "  pageUrl: "
					+ page.pageUrl.url, incorrectedSelectors, page.pageUrl.url);
		return n_pBlockConfig;
	}

	private BlockConfig visitBlockLocation(BlockConfig blockConfig,
			Document doc, HashSet<String> incorrectedSelectors, boolean withID) {
		if (blockConfig == null)
			return blockConfig;

		if (blockConfig.location != null
				&& blockConfig.location.type
						.equals(BlockLocationType.cssSelector)) {
			Elements elements = doc.select(blockConfig.location.value);
			if (elements == null || elements.isEmpty())
				incorrectedSelectors.add(blockConfig.location.value);
			else {
				String selector = elements.first().cssSelector();
				selector = selector.replaceAll("\\s+", "");
				if (withID)
					blockConfig.location.valueWithID = selector;
				else
					blockConfig.location.value = selector;
			}
		}
		if (blockConfig.supervised != null)

			for (BlockLocation location : blockConfig.supervised) {
				Elements elements = doc.select(location.value);
				if (elements == null || elements.isEmpty())
					incorrectedSelectors.add(location.value);
				else {
					String selector = elements.first().cssSelector();
					selector = selector.replaceAll("\\s+", "");
					if (withID)
						location.valueWithID = selector;
					else
						location.value = selector;
				}
			}
		if (blockConfig.blocks == null)
			return blockConfig;
		for (BlockConfig cfg : blockConfig.blocks) {
			visitBlockLocation(cfg, doc, incorrectedSelectors, withID);
		}
		return blockConfig;
	}

	public PageBlockConfig enrichCssSelector(PageBlockConfig pageBlockConfig,
			Page page, CleanHtmlDocumentScript script)
			throws IncorrectCssSelectorException, HtmlDocumentCreationException {
		if (pageBlockConfig == null)
			return pageBlockConfig;
		HashSet<String> incorrectedSelectors = new HashSet<>();
		Document doc = HtmlDocumentUtil.getDocumentFromSource(page.pageUrl.url,
				page.getHtmlSelen(), script);
		for (BlockConfig blockConfig : pageBlockConfig.block) {
			if (!blockConfig.multiRow)
				visitBlockLocation(blockConfig, doc, incorrectedSelectors, true);
		}
		if (!incorrectedSelectors.isEmpty())
			throw new IncorrectCssSelectorException("selectors: "
					+ incorrectedSelectors.toString() + "  pageUrl: "
					+ page.pageUrl.url, incorrectedSelectors, page.pageUrl.url);
		return pageBlockConfig;
	}

	public HashSet<String> enrichMultiRowPreidentifier(
			HashSet<String> preIdens, Page page,
			CleanHtmlDocumentScript script) throws HtmlDocumentCreationException, IncorrectCssSelectorException {
		Document doc = HtmlDocumentUtil.getDocumentFromSource(page.pageUrl.url,
				page.getHtmlSelen(), script);
		HashSet<String> incorrectedSelectors = new HashSet<>();
		HashSet<String> normalized = new HashSet<>();
		if(preIdens == null)
			return normalized;
		
		for (String cssPath : preIdens) {
			Elements elements = doc.select(cssPath);
			if (elements == null || elements.isEmpty())
				incorrectedSelectors.add(cssPath);
			else {
				String selector = elements.first().cssSelector();
				selector = selector.replaceAll("\\s+", "");
				normalized.add(selector);
			}
		}
		if (!incorrectedSelectors.isEmpty())
			throw new IncorrectCssSelectorException("selectors: "
					+ incorrectedSelectors.toString() + "  pageUrl: "
					+ page.pageUrl.url, incorrectedSelectors, page.pageUrl.url);
		return normalized;
	}

}
