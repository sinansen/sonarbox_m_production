package com.datalyxt.production.webscraper.util.runnable;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;

import javax.sql.DataSource;

import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.util.parser.LocationBasedPageParser;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.link.LinkType;
import com.datalyxt.webscraper.model.page.Page;
import com.datalyxt.webscraper.model.page.PageBlock;

public class PageLinkInitialization {
	public static void main(String[] args) {
		String pageURL = "http://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/main/?event=main.listNotifications";
		WebDriver driver = WebDriverFactory
				.createDriver(BrowserDriverType.firefox_windows);
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		try {
			Properties properties = new Properties();
			properties.load(PageLinkInitialization.class.getClassLoader()
					.getResourceAsStream("db.properties"));
			DataSource datasource = MyDataSourceFactory
					.getMySQLDataSource(properties);
			DAOFactory daoFactory = new MysqlDAOFactory(datasource);
			LocationBasedPageParser parser = new LocationBasedPageParser();
			HashSet<String> dynamicParameters = new HashSet<>();

			pageNavigationUtil.navigateToPage(driver, new BrowsingConfig(),
					pageURL);

			String pageSource = driver.getPageSource();

			CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
			script.enrichTextNode = false;
			script.removeElementCssClassAttr = false;
			script.removeElementIdAttr = false;

			Document doc = HtmlDocumentUtil.getDocumentFromSource(pageURL,
					pageSource, script);
			Page page = new Page();
			Link l = new Link();
			l.url = pageURL;
			l.domain = GetHost.getDomain(pageURL);
			page.pageUrl = l;
			String blockCssPath = "body";
			PageBlock pb = parser.parse(doc, page, blockCssPath, dynamicParameters);
			HashMap<String, LinkBlock> linkBlocks = new HashMap<>();
			for(Link link : pb.linkList.values()){
				if(link.linkType.contains(LinkType.internal)){
					LinkBlock lb = new LinkBlock();
					lb.blockId = 0L;
					lb.link = link;
					linkBlocks.put(link.url, lb);
				}
			}
			long blockId = 0L;
			daoFactory.getM_PageBlockLinkDAO().putLinkBlocks(linkBlocks,
					blockId );

		} catch (OpenPageUrlException | IOException | SQLException | BlockExtractionException | DefinedBlockNotFound | HtmlDocumentCreationException | M_PageBlockLinkDBException | DAOFactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}
}
