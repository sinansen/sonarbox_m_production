package com.datalyxt.production.webscraper.service.action;

import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonStorageActionStrategy implements BlockActionStrategy {

	@Override
	public ActionResultText doAction(Block block, BlockAction action) throws UnknownBlockTypeException {
		if (!BlockActionType.structure.equals(action.type))
			return null;
		if (!block.type.equals(BlockType.datasetBlock))
			return null;
		DatasetBlock datablock = (DatasetBlock) block;
		
		Gson gson = new GsonBuilder()
		.setExclusionStrategies(new JsonExclusionStrategy()).create();
		String actionJson = gson.toJson(datablock, DatasetBlock.class);
		
		ActionResultText result = new ActionResultText();
		result.createdAt = System.currentTimeMillis();
		result.blockId = block.blockId;
		result.content = actionJson;
		result.type = action.type.name();
		
		return result;

	}

}