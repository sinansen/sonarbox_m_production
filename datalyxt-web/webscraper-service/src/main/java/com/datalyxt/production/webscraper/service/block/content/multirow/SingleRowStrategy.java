package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.exception.webscraper.ColumnTemplateException;

public class SingleRowStrategy implements MultiRowDetectionStrategy {

	@Override
	public List<RowBasedSelectorTemplate> createRowTemplate(
			BlockTrainingData blockTrainingData) throws ColumnTemplateException {
		List<RowBasedSelectorTemplate> tmps = checkRow(blockTrainingData.columns);
		return tmps;
	}


	public List<RowBasedSelectorTemplate> checkRow(
			List<SupervisedDataColumn> dataColumns)
			throws ColumnTemplateException {

		List<RowBasedSelectorTemplate> rowBasedSelectorTemplates = new ArrayList<RowBasedSelectorTemplate>();

		int firstDiffLocation = -1;
		for (int i = 0; i < dataColumns.size(); i++) {
			SupervisedDataColumn dc = dataColumns.get(i);
			List<Selector> list = new ArrayList<Selector>();
			list.addAll(dc.superVisionNodes.values());
			Selector ref = list.get(0);

			for (int j = i + 1; j < dataColumns.size(); j++) {
				SupervisedDataColumn dd = dataColumns.get(j);
				list = new ArrayList<Selector>();
				list.addAll(dd.superVisionNodes.values());
				Selector comp = list.get(0);

				for (int m = 0; m < ref.allTokens.size(); m++) {
					if (m < comp.allTokens.size()) {
						String refToken = ref.allTokens.get(m);
						String comToken = comp.allTokens.get(m);
						if (!refToken.equals(comToken)) {
							if (firstDiffLocation == -1)
								firstDiffLocation = m;
							else {
								if (firstDiffLocation > m)
									firstDiffLocation = m;
							}
							break;
						}
					}
				}
			}
		}

		if (firstDiffLocation == -1)
			throw new ColumnTemplateException(
					"row elements have different pre identifiers");

		RowBasedSelectorTemplate rowBasedSelectorTemplate = new RowBasedSelectorTemplate();

		for (int i = 0; i < dataColumns.size(); i++) {
			SupervisedDataColumn dc = dataColumns.get(i);
			List<Selector> list = new ArrayList<Selector>();
			list.addAll(dc.superVisionNodes.values());
			Selector ref = list.get(0);
			IteratedSelectorTemplate iteratedSelectorTemplate = new IteratedSelectorTemplate(
					dc.blockType, dc.columnName);
			iteratedSelectorTemplate.postIdentifier = ref.allTokens.subList(
					firstDiffLocation, ref.allTokens.size());
			iteratedSelectorTemplate.rowIdentifierPosition = firstDiffLocation - 1;
			iteratedSelectorTemplate.rowIdentifier = ref.allTokens
					.get(iteratedSelectorTemplate.rowIdentifierPosition);
			iteratedSelectorTemplate.preIdentifier = ref.allTokens.subList(0,
					iteratedSelectorTemplate.rowIdentifierPosition);
			iteratedSelectorTemplate.cleanedRowIdentifier = ref.iterationToken
					.get(iteratedSelectorTemplate.rowIdentifierPosition).cleanedValue;

			rowBasedSelectorTemplate.columnTemplates
					.add(iteratedSelectorTemplate);
			rowBasedSelectorTemplate.cleanedRowIdentifier = iteratedSelectorTemplate.cleanedRowIdentifier;
			rowBasedSelectorTemplate.preIdentifier = iteratedSelectorTemplate.preIdentifier;
			rowBasedSelectorTemplate.rowIdentifier
					.add(iteratedSelectorTemplate.rowIdentifier);
			rowBasedSelectorTemplate.rowIdentifierPosition = iteratedSelectorTemplate.rowIdentifierPosition;

		}
		
		rowBasedSelectorTemplates.add(rowBasedSelectorTemplate);

		return rowBasedSelectorTemplates;
	}

}
