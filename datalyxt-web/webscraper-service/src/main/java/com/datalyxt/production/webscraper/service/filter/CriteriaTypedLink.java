package com.datalyxt.production.webscraper.service.filter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.webscraper.model.link.LinkType;

public class CriteriaTypedLink implements Criteria {

	@Override
	public HashMap<String, LinkBlock> meetCriteria(
			HashMap<String, LinkBlock> linkBlocks, Filter filter) {
		if (filter.internalLinkTypeFilter.isEmpty()
				&& filter.externalLinkTypeFilter.isEmpty())
			return linkBlocks;

		List<LinkBlock> removed = new ArrayList<LinkBlock>();
		for (LinkBlock linkBlock : linkBlocks.values()) {
			// do internal link filter
			if (linkBlock.link.linkType.contains(LinkType.internal))
				for (LinkType type : filter.internalLinkTypeFilter) {
					if (linkBlock.link.linkType.contains(type))
						removed.add(linkBlock);
				}
			else if (linkBlock.link.linkType.contains(LinkType.external)) // do
																			// external
																			// link
																			// filter
				for (LinkType type : filter.externalLinkTypeFilter) {
					if (linkBlock.link.linkType.contains(type))
						removed.add(linkBlock);
				}

		}
		if (!removed.isEmpty()) {
			for (LinkBlock removedLink : removed) {
				linkBlocks.remove(removedLink.link.url);
			}
		}
		return linkBlocks;
	}

}
