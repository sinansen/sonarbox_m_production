package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.List;

import com.datalyxt.production.webscraper.model.config.BlockConfig;

public class BlockConfigWrapper implements Comparable<BlockConfigWrapper>{
	public BlockConfig blockConfig;
	public IteratedToken rowIdentifier;
	public List<String> row;
	
	@Override
	public int compareTo(BlockConfigWrapper o) {
		// TODO Auto-generated method stub
		return blockConfig.column - o.blockConfig.column;
	}
	
}
