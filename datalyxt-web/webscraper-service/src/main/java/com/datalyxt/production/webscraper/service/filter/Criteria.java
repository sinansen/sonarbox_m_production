package com.datalyxt.production.webscraper.service.filter;

import java.util.HashMap;

import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;

public interface Criteria {
	 public HashMap<String, LinkBlock> meetCriteria(HashMap<String, LinkBlock> linkBlocks, Filter filter);
}
