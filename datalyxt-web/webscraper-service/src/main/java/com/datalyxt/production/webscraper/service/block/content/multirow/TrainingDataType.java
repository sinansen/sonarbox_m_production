package com.datalyxt.production.webscraper.service.block.content.multirow;

public enum TrainingDataType {
	//singleElement: 1x1 , singleRow: 1xn (n>1), singleColumn: nx1 (n>1), multiElement:nxn (n>1)
	singleElement, singleRow, singleColumn, multiElement
}
