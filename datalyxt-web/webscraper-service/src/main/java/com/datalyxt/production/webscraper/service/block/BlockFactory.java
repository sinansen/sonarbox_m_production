package com.datalyxt.production.webscraper.service.block;

import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DataBlock;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.KeyvalueBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.production.webscraper.model.runtime.TimeBlock;

public class BlockFactory {
	public Block createBlock(BlockConfig blockConfig)
			throws UnknownBlockTypeException {
		if (blockConfig.type == null)
			throw new UnknownBlockTypeException(
					"create block failed: block type is null");
		if (blockConfig.type.equals(BlockType.dataBlock)) {
			DataBlock block = new DataBlock();
			block.type = BlockType.dataBlock;
			return block;
		}

		if (blockConfig.type.equals(BlockType.linkBlock)) {
			LinkBlock block = new LinkBlock();
			block.type = BlockType.linkBlock;
			block.name = blockConfig.name;
			return block;
		}

		if (blockConfig.type.equals(BlockType.timeBlock)) {
			TimeBlock block = new TimeBlock();
			block.type = BlockType.timeBlock;
			block.name = blockConfig.name;
			return block;
		}

		if (blockConfig.type.equals(BlockType.keyvalueBlock)) {
			KeyvalueBlock block = new KeyvalueBlock();
			block.name = blockConfig.name;
			block.type = BlockType.keyvalueBlock;
			return block;
		}
		if (blockConfig.type.equals(BlockType.datasetBlock)) {
			DatasetBlock block = new DatasetBlock();
			block.type = BlockType.datasetBlock;
			return block;
		}
		if (blockConfig.type.equals(BlockType.pagingBlock)) {
			PagingBlock block = new PagingBlock();
			block.type = BlockType.pagingBlock;
			return block;
		}
		throw new UnknownBlockTypeException(
				"create block failed: block type is unknown - "
						+ blockConfig.type);
	}
}
