package com.datalyxt.production.webscraper.service.condition;

import java.util.HashMap;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webscraper.model.config.ActionCondition;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;

public class ConditionService {
	LinkSeen linkSeen;
	BlockContentSeen contentSeen;

	public ConditionService(LinkSeen linkSeen, BlockContentSeen contentSeen) {
		this.linkSeen = linkSeen;
		this.contentSeen = contentSeen;

	}

	public boolean isConditionFullfilled(ActionCondition condition,
			String pageURL, Block block, HashMap<String, LinkBlock> linkBlocks,
			DAOFactory daoFactory) throws M_PageBlockLinkDBException,
			DAOFactoryException, M_PageBlockDBException {
		boolean isConditionFullfilled = false;
		if (condition == null)
			isConditionFullfilled = true;
		else if (condition.equals(ActionCondition.link_url_change)) {

			for (LinkBlock linkblock : linkBlocks.values()) {
				if (!linkSeen.hasSeen(linkblock.link,
						daoFactory.getM_PageBlockLinkDAO())) {
					isConditionFullfilled = true;
					break;
				}
			}

		} else if (condition.equals(ActionCondition.block_text_change)
				|| condition.equals(ActionCondition.dataset_change)) {
			if (!contentSeen.hasSeen(pageURL, block, daoFactory)) {
				isConditionFullfilled = true;
			}
		}
		return isConditionFullfilled;
	}

	public HashMap<String, LinkBlock> getNewLinks(
			HashMap<String, LinkBlock> linkBlocks, DAOFactory daoFactory)
			throws M_PageBlockLinkDBException, DAOFactoryException {
		HashMap<String, LinkBlock> newLinks = new HashMap<>();

		for (LinkBlock linkblock : linkBlocks.values()) {
			if (!linkSeen.hasSeen(linkblock.link,
					daoFactory.getM_PageBlockLinkDAO())) {
				newLinks.put(linkblock.link.url, linkblock);
			}
		}
		return newLinks;
	}
}
