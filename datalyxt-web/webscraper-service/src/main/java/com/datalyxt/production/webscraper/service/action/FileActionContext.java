package com.datalyxt.production.webscraper.service.action;

import java.util.HashMap;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.exception.webscraper.FileActionException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;

public class FileActionContext {
	private HashMap<BlockActionType, FileActionStrategy> strategies = new HashMap<BlockActionType, FileActionStrategy>();

	// this can be set at runtime by the application preferences
	public void addActionStrategy(BlockActionType type,
			FileActionStrategy strategy) {
		this.strategies.put(type, strategy);
	}

	// use the strategy
	public ActionResultFile doAction(String pageUrl, BlockAction action, int timeout)
			throws FileActionException, OpenPageUrlException {
		if (action == null || action.type == null)
			throw new FileActionException(
					"file action failed: invalid action type");

		FileActionStrategy strategy = strategies.get(action.type);

		if (strategy == null)
			throw new FileActionException(
					"file action failed: no action strategy found for action type "
							+ action.type);

		ActionResultFile actionResultFile = null;
		try {
			actionResultFile = strategy.doAction(pageUrl, action, timeout);
		} catch (CreateScreenshotException | UnknownBlockTypeException
				| DownloadFileException  e) {
			throw new FileActionException(
					"do action failed: " + e.getMessage(), e);
		}
		if (actionResultFile == null)
			throw new FileActionException(
					"file action failed: invalid action type : " + action.type);
		return actionResultFile;
	}
}
