package com.datalyxt.production.webscraper.util.runnable;

import java.io.StringWriter;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

public class ProxyCheck {
	

	public static void main(String[] args){
		
	
		 AbstractHttpClient httpclient = null;
	
    try {
    	Properties properties = new Properties();
		properties.load(ProxyCheck.class.getClassLoader()
				.getResourceAsStream("proxy.properties"));
		System.out.println("proxy " + "["
				+ properties.getProperty("proxy") + "]");
		System.out.println("port " + "["
				+ properties.getProperty("port") + "]");
		System.out.println("protocol " + "["
				+ properties.getProperty("protocol") + "]");
		System.out.println("proxy " + "["
				+ properties.getProperty("username") + "]");
		System.out.println("proxy " + "["
				+ properties.getProperty("pwd") + "]");

		httpclient = new DefaultHttpClient();
//		httpclient.getCredentialsProvider().setCredentials(
//		    new AuthScope(properties.getProperty("proxy"), 8080),
//		    new UsernamePasswordCredentials(properties.getProperty("username"), properties.getProperty("pwd")));
		
		HttpHost proxy = new HttpHost(properties.getProperty("proxy"), Integer.valueOf(properties.getProperty("port")), properties.getProperty("protocol"));



		
        httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);

        HttpHost target = new HttpHost("www.fzi.de/startseite/");
        HttpGet req = new HttpGet("/");

        System.out.println("executing request to " + target + " via " + proxy);
        HttpResponse rsp = httpclient.execute(target, req);
        StringWriter writer = new StringWriter();
        IOUtils.copy(rsp.getEntity().getContent(), writer, "utf-8");
        String theString = writer.toString();
       System.out.println("c-content "+theString);
       
    } catch(Exception e){e.printStackTrace();}finally {
        // When HttpClient instance is no longer needed,
        // shut down the connection manager to ensure
        // immediate deallocation of all system resources
        httpclient.getConnectionManager().shutdown();
    }
}
}
