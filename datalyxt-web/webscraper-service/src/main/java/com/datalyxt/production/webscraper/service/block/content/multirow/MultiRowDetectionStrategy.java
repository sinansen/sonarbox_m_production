package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.List;

import com.datalyxt.production.exception.webscraper.ColumnTemplateException;

public interface MultiRowDetectionStrategy {
	
	public List<RowBasedSelectorTemplate> createRowTemplate(
			BlockTrainingData blockTrainingData) throws ColumnTemplateException ;

}
