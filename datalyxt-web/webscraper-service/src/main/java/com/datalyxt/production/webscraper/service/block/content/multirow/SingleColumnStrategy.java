package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import com.datalyxt.production.exception.webscraper.ColumnTemplateException;

public class SingleColumnStrategy implements MultiRowDetectionStrategy {

	@Override
	public List<RowBasedSelectorTemplate> createRowTemplate(
			BlockTrainingData blockTrainingData) throws ColumnTemplateException {
		for (SupervisedDataColumn column : blockTrainingData.columns) {
			generateTemplate(column);
		}
		List<RowBasedSelectorTemplate> tmps = checkRow(blockTrainingData.columns);
		return tmps;
	}

	public void generateTemplate(SupervisedDataColumn dataColumn)
			throws ColumnTemplateException {
		String regex = "\\(\\d*\\)";
		Vector<Selector> superVisionNodes = new Vector<Selector>();
		superVisionNodes.addAll(dataColumn.superVisionNodes.values());

		boolean unique = true;
		boolean iteration = false;

		for (int i = 0; i < superVisionNodes.size(); i++) {
			Selector ref = superVisionNodes.get(i);
			ArrayList<String> preElements = new ArrayList<String>();
			ArrayList<String> postElements = new ArrayList<String>();
			ArrayList<String> allElements = new ArrayList<String>();
			int position = 0;
			String iteratedElement = null;

			for (int j = i + 1; j < superVisionNodes.size(); j++) {

				Selector comp = superVisionNodes.get(j);

				// length
				if (ref.allTokens.size() == comp.allTokens.size()) {

//					if (ref.cleanedSelector.equals(comp.cleanedSelector)) {

						int changeCounter = 0;

						for (int k = 0; k < ref.allTokens.size(); k++) {
							String st = ref.allTokens.get(k);

							if (!comp.allTokens.contains(st)) {
								if (changeCounter == 0) {
									iteratedElement = st;
									position = k;
									changeCounter++;
								} else {
									throw new ColumnTemplateException(
											"ERROR: MORE THAN ONE ELEMENT IS ITERATED "
													+ ref.selector + " - "
													+ comp.selector);
								}
							} else {
								if (iteratedElement == null)
									preElements.add(st);
								else if (k > position)
									postElements.add(st);
							}
							allElements.add(st);
						}
						iteration = true;
						unique = false;
						break;
					}
//				}
			}
			// TODO size=2
			if (unique) {
				if (!dataColumn.uniqueSelector.containsKey(ref.id)) {
					UniqueSelectorTemplate ust = new UniqueSelectorTemplate(
							ref.id);
					ust.selector = ref;
					dataColumn.uniqueSelector.put(ref.id, ust);
				}
			}
			if (iteration) {

				boolean knownTemplate = false;
				String cleanedIteratedElement = iteratedElement.replaceAll(
						regex, "");
				for (IteratedSelectorTemplate ist : dataColumn.iteratedSelectorTemplates) {
					boolean isPreIdentifierEqual = Arrays.deepEquals(
							preElements.toArray(), ist.preIdentifier.toArray());
					if (isPreIdentifierEqual) {
						if (position == ist.rowIdentifierPosition
								&& cleanedIteratedElement
										.equals(ist.cleanedRowIdentifier)) {
							knownTemplate = true;

							break;
						}
					}
				}
				if (!knownTemplate) {

					// New
					IteratedSelectorTemplate ist = new IteratedSelectorTemplate(
							dataColumn.blockType, dataColumn.columnName);
					ist.setIteratedElement(iteratedElement,
							cleanedIteratedElement);
					ist.rowIdentifierPosition = position;
					ist.preIdentifier = preElements;
					ist.postIdentifier = postElements;
					dataColumn.iteratedSelectorTemplates.add(ist);

				}
			}
			unique = true;
			iteration = false;
		}
	}

	public List<RowBasedSelectorTemplate> checkRow(
			List<SupervisedDataColumn> dataColumns) {

		List<RowBasedSelectorTemplate> rowBasedSelectorTemplates = new ArrayList<RowBasedSelectorTemplate>();

		for (int i = 0; i < dataColumns.size(); i++) {
			SupervisedDataColumn dc = dataColumns.get(i);
			for (IteratedSelectorTemplate its : dc.iteratedSelectorTemplates) {
				updateList(rowBasedSelectorTemplates, its);
			}

		}
		return rowBasedSelectorTemplates;
	}

	private void updateList(
			List<RowBasedSelectorTemplate> rowBasedSelectorTemplates,
			IteratedSelectorTemplate its) {
		boolean found = false;
		for (RowBasedSelectorTemplate rtmp : rowBasedSelectorTemplates) {
			boolean isPreEqual = Arrays.deepEquals(
					rtmp.preIdentifier.toArray(), its.preIdentifier.toArray());
			if (isPreEqual
					&& rtmp.cleanedRowIdentifier
							.equals(its.cleanedRowIdentifier)
					&& rtmp.rowIdentifierPosition == its.rowIdentifierPosition) {
				found = true;
				rtmp.rowIdentifier.add(its.rowIdentifier);
				rtmp.columnTemplates.add(its);
				break;
			}
		}
		if (!found) {
			RowBasedSelectorTemplate rowtmp = new RowBasedSelectorTemplate();
			rowtmp.cleanedRowIdentifier = its.cleanedRowIdentifier;
			rowtmp.rowIdentifierPosition = its.rowIdentifierPosition;
			rowtmp.preIdentifier = its.preIdentifier;
			rowtmp.rowIdentifier.add(its.rowIdentifier);
			rowtmp.columnTemplates.add(its);

			rowBasedSelectorTemplates.add(rowtmp);
		}

	}

}
