package com.datalyxt.production.webscraper.service.block.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DataBlock;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;

public class BlockContentService {

	public HashMap<String, LinkBlock> getLinkBlocks(Block block) {

		HashMap<String, LinkBlock> linkblocks = new HashMap<String, LinkBlock>();

		visitDataset(block, linkblocks);

		return linkblocks;
	}

	private void visitDataset(Block block, HashMap<String, LinkBlock> linkblocks) {
		if (block.type.equals(BlockType.linkBlock))
			linkblocks.put(((LinkBlock) block).link.url, (LinkBlock) block);
		else if (block.type.equals(BlockType.dataBlock)) {
			linkblocks.putAll(((DataBlock) block).linkBlocks);
		} else if (block.type.equals(BlockType.datasetBlock)) {
			DatasetBlock datasetBlock = (DatasetBlock) block;
			if (datasetBlock.blocks != null)
				for (Block subblock : datasetBlock.blocks)
					visitDataset(subblock, linkblocks);
		} else if (block.type.equals(BlockType.pagingBlock)) {
			linkblocks.putAll(((PagingBlock) block).linkBlocks);
		}
	}

	public Block setLinkBlocks(Block block,
			HashMap<String, LinkBlock> linkBlocks) {
		if (block.type.equals(BlockType.linkBlock)) {
			if (!linkBlocks.containsKey(((LinkBlock) block).link.url))
				return null;
			else
				return block;
		} else if (block.type.equals(BlockType.dataBlock)) {
			((DataBlock) block).linkBlocks.clear();
			((DataBlock) block).linkBlocks.putAll(linkBlocks);
		} else if (block.type.equals(BlockType.datasetBlock)) {
			DatasetBlock datasetBlock = (DatasetBlock) block;
			if (datasetBlock.blocks != null){
				List<Block> blocks = new ArrayList<Block>();
				for (Block subblock : datasetBlock.blocks) {
					Block b = setLinkBlocks(subblock, linkBlocks);
					if(b != null)
						blocks.add(b);
				}
				datasetBlock.blocks = blocks;
			}
		} else if (block.type.equals(BlockType.pagingBlock)) {
			((PagingBlock) block).linkBlocks.clear();
			((PagingBlock) block).linkBlocks.putAll(linkBlocks);
		}

		return block;
	}

}
