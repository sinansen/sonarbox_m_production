package com.datalyxt.production.webscraper.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.webscraper.BlockConfigNormalizationException;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.service.block.BlockFactory;
import com.datalyxt.production.webscraper.service.block.content.BlockVisitService;
import com.datalyxt.production.webscraper.service.block.content.multirow.RowBasedSelectorTemplate;
import com.datalyxt.production.webscraper.service.location.BlockLocationNormalizer;
import com.datalyxt.production.webscraper.service.location.ContentBlockVisitContext;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.Page;

public class QT1V1ConfigService {
	public List<PageBlockConfig> getBlockConfigByHop(int hop,
			BlockConfigMain mainConfig) {
		List<PageBlockConfig> confgs = new ArrayList<PageBlockConfig>();

		for (PageBlockConfig blockConfig : mainConfig.config) {
			if (blockConfig.hop == hop) {
				confgs.add(blockConfig);
			}
		}
		return confgs;
	}

	public boolean isBlockExtractionRequired(PageBlockConfig pageBlockConfig) {
		for (BlockConfig blockConfig : pageBlockConfig.block)
			for (BlockAction baction : blockConfig.action) {
				if (!baction.type.equals(BlockActionType.screenshot)
						|| !baction.type.equals(BlockActionType.fileStorage))
					return true;
			}
		return false;
	}

	public List<PageBlockConfig> selectValidPageBlockConfig(String pageURL,
			String pageSource, List<PageBlockConfig> pageBlockConfigs,
			CleanHtmlDocumentScript script, boolean ignoreMultiRow)
			throws HtmlDocumentCreationException {
		List<PageBlockConfig> cfgs = new ArrayList<>();

		Document doc = HtmlDocumentUtil.getDocumentFromSource(pageURL,
				pageSource, script);

		for (PageBlockConfig pageCfg : pageBlockConfigs) {
			boolean isValid = true;
			for (BlockConfig blockConfig : pageCfg.block) {
				if (blockConfig.type.equals(BlockType.datasetBlock)) {
					if (!ignoreMultiRow)

						for (BlockConfig itemCfg : blockConfig.blocks) {
							isValid = checkBlockConfig(itemCfg, doc);
							if (!isValid)
								break;
						}
					else {
						if (!blockConfig.multiRow)
							for (BlockConfig itemCfg : blockConfig.blocks) {
								isValid = checkBlockConfig(itemCfg, doc);
								if (!isValid)
									break;
							}
					}
				} else {
					isValid = checkBlockConfig(blockConfig, doc);
					if (!isValid)
						break;
				}

			}
			if (!ignoreMultiRow && !pageCfg.multiRowPreidentifiers.isEmpty()) {
				isValid = checkMultiRowPreIdentifiers(pageURL, pageSource,
						pageCfg.multiRowPreidentifiers);
			}
			if (isValid)
				cfgs.add(pageCfg);
		}
		return cfgs;
	}

	private boolean checkMultiRowPreIdentifiers(String pageURL,
			String pageSource, HashSet<String> multiRowPreidentifiers)
			throws HtmlDocumentCreationException {
		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		script.removeElementCssClassAttr = false;
		script.removeElementIdAttr = false;
		Document doc = HtmlDocumentUtil.getDocumentFromSource(pageURL,
				pageSource, script);
		boolean isValid = true;
		for (String css : multiRowPreidentifiers) {
			Elements elements = doc.select(css);
			if (elements == null || elements.isEmpty()) {
				isValid = false;
				break;
			}
		}

		return isValid;
	}

	private boolean checkBlockConfig(BlockConfig blockConfig, Document doc) {
		boolean isValid = true;
		if (blockConfig.location != null
				&& blockConfig.location.type
						.equals(BlockLocationType.cssSelector)) {
			Elements elements = doc.select(blockConfig.location.value);
			if (elements == null || elements.isEmpty()) {
				isValid = false;
			} else {
				if (blockConfig.supervised != null) {
					for (BlockLocation location : blockConfig.supervised) {
						Elements eles = doc.select(location.value);
						if (eles == null || eles.isEmpty()) {
							isValid = false;
							break;
						}
					}
				}
			}
		}
		return isValid;
	}

	private Page createPage(WebDriver driver, String pageURL)
			throws OpenPageUrlException {
		BrowsingConfig browsingConfig = new BrowsingConfig();
		browsingConfig.pageLoadTimeout = 20000;
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		pageNavigationUtil.navigateToPage(driver, browsingConfig, pageURL);
		JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
		String pageSource = (String) jsdriver
				.executeScript("return document.documentElement.outerHTML;");

		Page page = new Page();
		Link link = new Link();
		link.url = pageURL;
		link.domain = GetHost.getDomain(link.url);
		page.pageUrl = link;
		page.setHtmlSelen(pageSource);
		return page;
	}

	public BlockConfigMain getNormalizedMainConfig(BlockConfigMain mainConfig,
			WebDriver driver) throws BlockConfigNormalizationException {
		try {
			BlockFactory blockFactory = new BlockFactory();
			ContentBlockVisitContext contentBlockVisitContext = new ContentBlockVisitContext();
			BlockVisitService blockVisitService = new BlockVisitService(
					blockFactory, contentBlockVisitContext);
			BlockLocationNormalizer blockLocationNormalizer = new BlockLocationNormalizer();

			CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
			CleanHtmlDocumentScript scriptEnableID = new CleanHtmlDocumentScript();
			scriptEnableID.removeElementCssClassAttr = false;
			scriptEnableID.removeElementIdAttr = false;
			Page page = null;

			for (PageBlockConfig cfg : mainConfig.config) {
				if (cfg.url != null)
					page = createPage(driver, cfg.url);
				else
					throw new HtmlDocumentCreationException(
							"no url defined in config for hop " + cfg.hop);
				blockLocationNormalizer.normalize(cfg, page, script);

				// blockLocationNormalizer.enrichCssSelector(cfg, page,
				// scriptEnableID);
				HashSet<String> preidenfiers = new HashSet<String>();
				for (BlockConfig blockConfig : cfg.block) {

					if (blockConfig.multiRow) {
						List<RowBasedSelectorTemplate> templates = blockVisitService
								.getTemplates(blockConfig);
						HashSet<String> pselectors = blockVisitService
								.getTemplatesParentSelectors(templates);
						HashSet<String> preidenfiersPath = blockLocationNormalizer
								.enrichMultiRowPreidentifier(pselectors, page,
										scriptEnableID);
						preidenfiers.addAll(preidenfiersPath);
					}
				}

				cfg.multiRowPreidentifiers = preidenfiers;
			}

		} catch (Exception e) {
			String error = ExceptionUtils.getStackTrace(e);
			throw new BlockConfigNormalizationException(
					"getNormalizedMainConfig : " + error, e);
		}
		return mainConfig;
	}
}
