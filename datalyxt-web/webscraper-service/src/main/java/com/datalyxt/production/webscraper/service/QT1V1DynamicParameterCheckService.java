package com.datalyxt.production.webscraper.service;

import java.util.HashMap;
import java.util.HashSet;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.DynamicityUtil;
import com.datalyxt.util.GetBase;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.Dynamicity;
import com.datalyxt.webscraper.model.LinkParameter;

public class QT1V1DynamicParameterCheckService {

	public Dynamicity getDynamicity(WebDriver driver, String url)
			throws InterruptedException, OpenPageUrlException,
			HtmlDocumentCreationException {
		DynamicityUtil dynamicityUtil = new DynamicityUtil();

		HashSet<String> links = getlinks(url, driver);

		HashMap<String, LinkParameter> first = dynamicityUtil.getLinkSet(links);
		Thread.sleep(2000);
		links = getlinks(url, driver);
		HashMap<String, LinkParameter> second = dynamicityUtil
				.getLinkSet(links);
		Dynamicity dy = dynamicityUtil.checkDynamicity(url, first, second);
		return dy;
	}

	private HashSet<String> getlinks(String url, WebDriver driver)
			throws OpenPageUrlException, HtmlDocumentCreationException {
		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		BrowsingConfig browsingConfig = new BrowsingConfig();
		browsingConfig.pageLoadTimeout = 15000;
		pageNavigationUtil.navigateToPage(driver, browsingConfig, url);

		JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
		String pageSource = (String) jsdriver
				.executeScript("return document.documentElement.outerHTML;");

		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		Document doc = HtmlDocumentUtil.getDocumentFromSource(url, pageSource,
				script);
		GetBase baseUtil = new GetBase();
		String baseURI = GetHost.guessBaseUrl(url);
		String protocalhost = GetHost.getProtocalHost(url);
		Elements atags = doc.select("a[href]");
		HashSet<String> links = new HashSet<String>();
		for (Element atag : atags) {
			String href = atag.attr("href");
			String absurl = baseUtil.getCalculatedBase(href, baseURI,
					protocalhost, url);
			links.add(absurl);
		}
		return links;
	}
}
