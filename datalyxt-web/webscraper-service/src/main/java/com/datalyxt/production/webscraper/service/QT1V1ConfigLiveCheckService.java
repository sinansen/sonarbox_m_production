package com.datalyxt.production.webscraper.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.FilterFailedException;
import com.datalyxt.production.exception.webscraper.IncorrectCssSelectorException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webdatabase.factory.RuntimeDAOFactory;
import com.datalyxt.production.webdatabase.factory.SqliteDAOFactory;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.service.block.BlockFactory;
import com.datalyxt.production.webscraper.service.block.content.BlockContentService;
import com.datalyxt.production.webscraper.service.block.content.BlockVisitService;
import com.datalyxt.production.webscraper.service.block.content.multirow.RowBasedSelectorTemplate;
import com.datalyxt.production.webscraper.service.condition.DatasetTextService;
import com.datalyxt.production.webscraper.service.filter.BlockFilter;
import com.datalyxt.production.webscraper.service.filter.LinkBlockFilter;
import com.datalyxt.production.webscraper.service.location.BlockLocationNormalizer;
import com.datalyxt.production.webscraper.service.location.ContentBlockVisitContext;
import com.datalyxt.production.webscraper.service.location.CssSelectorLocationVisitStrategy;
import com.datalyxt.production.webscraper.service.parser.PageCssSelectorParser;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.Page;

public class QT1V1ConfigLiveCheckService {

	public List<Block> checkConfig(WebDriver driver,
			BlockConfigMain mainConfig, CleanHtmlDocumentScript script,
			String pageURL, int hop) throws OpenPageUrlException,
			DAOFactoryException, DefinedBlockNotFound,
			BlockExtractionException, HtmlDocumentCreationException,
			IncorrectCssSelectorException, UnknownBlockTypeException,
			NullBlockLocationException, TimeBlockException,
			UnsupportedBlockLocationException, ColumnTemplateException,
			M_DomainBlackListDBException, FilterFailedException,
			PagingExtractionException {

		QT1V1ConfigService qt1v1ConfigService = new QT1V1ConfigService();

		List<PageBlockConfig> cfgs = qt1v1ConfigService.getBlockConfigByHop(
				hop, mainConfig);

		PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
		BrowsingConfig browsingConfig = new BrowsingConfig();
		browsingConfig.pageLoadTimeout = 15000;
		pageNavigationUtil.navigateToPage(driver, browsingConfig, pageURL);

		JavascriptExecutor jsdriver = (JavascriptExecutor) driver;
		String pageSource = (String) jsdriver
				.executeScript("return document.documentElement.outerHTML;");

		Page page = new Page();
		Link link = new Link();
		link.url = pageURL;
		link.domain = GetHost.getDomain(link.url);
		page.pageUrl = link;

		page.setHtmlSelen(pageSource);

		CleanHtmlDocumentScript scriptWithID = new CleanHtmlDocumentScript();
		scriptWithID.removeElementCssClassAttr = false;
		scriptWithID.removeElementIdAttr = false;
		List<PageBlockConfig> pageBlockConfigs = qt1v1ConfigService
				.selectValidPageBlockConfig(pageURL, page.getHtmlSelen(), cfgs,
						scriptWithID, false);

		if (pageBlockConfigs.isEmpty())
			throw new BlockExtractionException("no valid pageblockconfig! "
					+ pageURL);

		List<Block> maxBlocks = new ArrayList<Block>();

		for (PageBlockConfig pageBlockConfig : pageBlockConfigs) {
			pageBlockConfig.url = pageURL;

			List<Block> blocks = new ArrayList<Block>();

			BlockFactory blockFactory = new BlockFactory();

			LinkBlockFilter dataBlockFilter = new LinkBlockFilter();

			ContentBlockVisitContext cssCtx = new ContentBlockVisitContext();
			cssCtx.setContentVisitStrategy(new CssSelectorLocationVisitStrategy());

			BlockVisitService blockVisitService = new BlockVisitService(
					blockFactory, cssCtx);
			PageCssSelectorParser pageCssSelectorParser = new PageCssSelectorParser();

			BlockContentService blockContentService = new BlockContentService();

			String urlHashcode = DigestUtils.md5Hex(pageBlockConfig.url);
			int tmpBlockId = 1;

			RuntimeDAOFactory sqliteDaoFactory = SqliteDAOFactory.getInstance();
			sqliteDaoFactory.getM_BlockFilterDAO().createDB();

			BlockFilter blockFilter = new BlockFilter(sqliteDaoFactory,
					dataBlockFilter);
			BlockLocationNormalizer blockLocationNormalizer = new BlockLocationNormalizer();
			DatasetTextService datasetHash = new DatasetTextService();

			// PageBlockConfig pcfg = blockLocationNormalizer.normalize(
			// pageBlockConfig, page, script);
			for (BlockConfig blockConfig : pageBlockConfig.block) {

				if (blockConfig.multiRow) {

					Filter filter = blockConfig.filter;

					List<RowBasedSelectorTemplate> templates = blockVisitService
							.getTemplates(blockConfig);

					HashSet<String> pselectors = blockVisitService
							.getTemplatesParentSelectors(templates);

					Collection<BlockLocation> tobeCheckedCssSelector = pageCssSelectorParser
							.extractChildrenEleSelector(page,
									pselectors.iterator(), script);

					List<DatasetBlock> datasetBlocks = blockVisitService
							.visitMultiRow(blockConfig, page, script,
									tobeCheckedCssSelector, templates);

					int datasetTmpBlockId = 1;

					if (filter != null) {
						datasetBlocks = blockFilter.doDatasetFilter(filter,
								datasetBlocks, urlHashcode);
					}

					for (DatasetBlock datasetBlock : datasetBlocks) {

						datasetBlock.blockId = datasetTmpBlockId++;
						datasetBlock.blockText = datasetHash
								.getDatasetText(datasetBlock);
						datasetBlock.blockTextHash = datasetBlock.blockText
								.hashCode();
						processBlock(urlHashcode, blockContentService,
								datasetBlock, filter, blockFilter, blockConfig);

						if (datasetBlock != null)
							blocks.add(datasetBlock);
					}

				} else {

					Block block = blockVisitService.visit(blockConfig, page,
							script);

					block.blockId = tmpBlockId++;

					Filter filter = blockConfig.filter;

					processBlock(urlHashcode, blockContentService, block,
							filter, blockFilter, blockConfig);
					if (block != null)
						blocks.add(block);

				}

			}
			sqliteDaoFactory.getM_BlockFilterDAO().clearDB(urlHashcode);
			sqliteDaoFactory.closeConnection();
			if (blocks.size() > maxBlocks.size()) {
				maxBlocks.clear();
				maxBlocks.addAll(blocks);
			}
		}
		return maxBlocks;
	}

	private Block processBlock(String urlHashcode,
			BlockContentService blockContentService, Block block,
			Filter filter, BlockFilter blockFilter, BlockConfig blockConfig)
			throws M_DomainBlackListDBException, DAOFactoryException,
			FilterFailedException {
		HashMap<String, LinkBlock> linkBlocks = blockContentService
				.getLinkBlocks(block);

		if (filter != null) {
			HashSet<String> blackList = new HashSet<>();
			linkBlocks = blockFilter.doBlockLinkFilter(filter, linkBlocks,
					blackList);
			block = blockContentService.setLinkBlocks(block, linkBlocks);
			
		}

		return block;

	}

}
