package com.datalyxt.production.webscraper.service.condition;

import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.webdatabase.dao.M_PageBlockLinkDAO;
import com.datalyxt.webscraper.model.link.Link;

public class LinkSeen {

	public synchronized boolean hasSeen(Link link, M_PageBlockLinkDAO processedLinkDAO) throws M_PageBlockLinkDBException   {

		return processedLinkDAO.contains(link);
	}
}
