package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Selector {
	public String id;

	String regex = "\\d";

	public String selector;

	public HashMap<Integer, IteratedToken> iterationToken = new HashMap<Integer, IteratedToken>(); // Alle
																						// elements
																						// tokenized
																						// by
																						// delimiter
																						// ">"
																						// which
																						// contains
																						// (X)
	public List<String> allTokens = new ArrayList<String>();

	public String guessedType;

	String cleanedSelector;

	public Selector(String id, String cssSelector) {
		this.selector = cssSelector;
		this.id = id;
	}

	public void initialize() {

		String delim = ">";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher;
		HashMap<Integer, Integer> matchingRes = new HashMap<Integer, Integer>();

		StringTokenizer tok = new StringTokenizer(selector, delim);
		int index = 0;
		String token;
		while (tok.hasMoreTokens()) {
		
			token = tok.nextToken();
			matcher = pattern.matcher(token);
			allTokens.add(token.trim());
			while (matcher.find()) {
				matchingRes.put(matcher.start(), matcher.end());
				IteratedToken it = createIteratedToken(token);
				iterationToken.put(index, it);
			}
			index++;
		}
		if (iterationToken.size() == 0) {
			guessedType = "regular";
		} else if (iterationToken.size() == 1) {
			guessedType = "list";
		} else if (iterationToken.size() == 2) {
			guessedType = "table";
		} else {
			guessedType = "multiList/Table";
		}

		cleanedSelector = selector.replaceAll(regex, "");

	}

	private IteratedToken createIteratedToken(String token) {
		IteratedToken iteratedToken = new IteratedToken();
		iteratedToken.value = token.trim();
		iteratedToken.cleanedValue = token.replaceAll(regex, "");
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(token);
		if (m.find()) {
			iteratedToken.number = Integer.valueOf(m.group());
		}
		return iteratedToken;

	}
}
