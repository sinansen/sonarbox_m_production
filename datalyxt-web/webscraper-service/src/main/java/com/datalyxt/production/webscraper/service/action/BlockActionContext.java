package com.datalyxt.production.webscraper.service.action;

import java.util.HashMap;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.exception.webscraper.BlockActionException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.runtime.Block;

public class BlockActionContext {
	private HashMap<BlockActionType, BlockActionStrategy> strategies = new HashMap<BlockActionType, BlockActionStrategy>();

	// this can be set at runtime by the application preferences
	public void addBlockActionStrategy(BlockActionType type,
			BlockActionStrategy strategy) {
		this.strategies.put(type, strategy);
	}

	// use the strategy
	public ActionResultText doAction(Block block, BlockAction action)
			throws BlockActionException {
		if (action == null || action.type == null)
			throw new BlockActionException(
					"block action failed: invalid action");

		if (block == null || block.type == null)
			throw new BlockActionException(
					"block action failed: block is null or block type is null");

		BlockActionStrategy strategy = strategies.get(action.type);

		if (strategy == null)
			throw new BlockActionException(
					"block action failed: no action strategy found for action type "
							+ action.type);

		ActionResultText actionResult = null;
		try {
			actionResult = strategy.doAction(block, action);
		} catch (CreateScreenshotException | UnknownBlockTypeException
				| DownloadFileException | OpenPageUrlException e) {
			throw new BlockActionException("do action failed: "
					+ e.getMessage(), e);
		}
		if (actionResult == null)
			throw new BlockActionException(
					"block action failed: invalid action type, no action strategy found: "
							+ action.type);
		return actionResult;
	}
}
