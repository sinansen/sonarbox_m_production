package com.datalyxt.production.webscraper.service.location;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.webscraper.model.page.Page;

public interface ContentBlockVisitStrategy {
	public void visit(Block block, BlockConfig config, Page htmlPage, CleanHtmlDocumentScript script) throws NullBlockLocationException, DefinedBlockNotFound, BlockExtractionException, TimeBlockException, HtmlDocumentCreationException, PagingExtractionException ;
	
}
