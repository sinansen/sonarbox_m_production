package com.datalyxt.production.webscraper.service.action;

import java.util.HashMap;

import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.service.block.content.BlockContentService;
import com.google.gson.Gson;

public class LinkStorageActionStrategy implements BlockActionStrategy {

	BlockContentService blockContentService = new BlockContentService();

	@Override
	public ActionResultText doAction(Block block, BlockAction action) throws UnknownBlockTypeException {
		if (!BlockActionType.linkStorage.equals(action.type))
			return null;

		HashMap<String, LinkBlock> linkBlocks = blockContentService.getLinkBlocks(block);
		
		Gson gson = new Gson();
		String actionLinksJson = gson.toJson(linkBlocks.keySet());

		ActionResultText result = new ActionResultText();
		result.createdAt = System.currentTimeMillis();
		result.blockId = block.blockId;
		result.content = actionLinksJson;
		result.type = action.type.name();
		return result;

	}

}
