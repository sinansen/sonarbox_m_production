package com.datalyxt.production.webscraper.service.condition;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.exception.db.M_PageDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.BlockMeta;
import com.datalyxt.production.webscraper.model.runtime.PageMeta;

public class BlockContentSeen {

	public boolean hasSeen(String pageUrl, Block block, DAOFactory daoFactory)
			throws M_PageBlockDBException, DAOFactoryException {
		BlockMeta blockMeta = new BlockMeta();
		blockMeta.pageUrl = pageUrl;
		boolean isExsit = daoFactory.getM_PageBlockDAO()
				.isExsit(block, blockMeta);
		return isExsit;
	}
	public boolean hasSeenPage(PageMeta pageMeta, DAOFactory daoFactory) throws M_PageDBException, DAOFactoryException{
		boolean isExsit = daoFactory.getM_PageDAO()
				.isExsit(pageMeta);
		return isExsit;
	}

}
