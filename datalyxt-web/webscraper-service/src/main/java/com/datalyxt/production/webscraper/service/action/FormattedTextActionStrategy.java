package com.datalyxt.production.webscraper.service.action;

import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.ContentBlock;

public class FormattedTextActionStrategy implements BlockActionStrategy {
	

	@Override
	public ActionResultText doAction( Block block, BlockAction action) throws UnknownBlockTypeException {
		if (!BlockActionType.formattedText.equals(action.type))
			return null;

		if(block.type.equals(BlockType.datasetBlock))
			return null;
		
		ContentBlock contentBlock = (ContentBlock)block;
		
		ActionResultText result = new ActionResultText();
		result.createdAt = System.currentTimeMillis();
		result.blockId = block.blockId;
		result.content = contentBlock.blockHtml;
		result.type = action.type.name();
		
		return result;

	}

}
