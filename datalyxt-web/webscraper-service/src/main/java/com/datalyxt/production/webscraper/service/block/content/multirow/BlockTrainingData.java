package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.List;

public class BlockTrainingData {
	public TrainingDataType type;
	public List<SupervisedDataColumn> columns;
}
