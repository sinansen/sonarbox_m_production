package com.datalyxt.production.webscraper.service.location;

import java.util.HashSet;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.webscraper.model.page.Page;

public class ContentBlockVisitContext {
	private ContentBlockVisitStrategy contentBlockVisitStrategy;
	private HashSet<BlockType> contentBlockType;

	public ContentBlockVisitContext() {
		contentBlockType = new HashSet<>();
		contentBlockType.add(BlockType.dataBlock);
		contentBlockType.add(BlockType.timeBlock);
		contentBlockType.add(BlockType.linkBlock);
		contentBlockType.add(BlockType.keyvalueBlock);
		contentBlockType.add(BlockType.pagingBlock);
	}

	// this can be set at runtime by the application preferences
	public void setContentVisitStrategy(ContentBlockVisitStrategy strategy) {
		this.contentBlockVisitStrategy = strategy;

	}

	public boolean isContentBlock(BlockType type) {
		return contentBlockType.contains(type);
	}

	// use the strategy
	public Block visitBlock(Block block, BlockConfig blockConfig, Page page, CleanHtmlDocumentScript script)
			throws NullBlockLocationException, DefinedBlockNotFound,
			BlockExtractionException, TimeBlockException, HtmlDocumentCreationException, PagingExtractionException {

		if (block == null)
			throw new BlockExtractionException(
					"extract block failed: invalid location");

		if (isContentBlock(block.type))
			contentBlockVisitStrategy.visit(block, blockConfig, page, script);

		return block;
	}
}
