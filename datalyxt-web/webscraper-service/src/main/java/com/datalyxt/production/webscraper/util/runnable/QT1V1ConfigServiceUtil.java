package com.datalyxt.production.webscraper.util.runnable;

import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class QT1V1ConfigServiceUtil {
	public static void main(String[] args) {

		long sourceId = 35L;
		long ruleId = 48L;
		long ownerId = 1L;
		if (args.length > 0) {
			try {
				sourceId = Long.parseLong(args[0]);
				ruleId = Long.parseLong(args[1]);
				ownerId = Long.parseLong(args[2]);
			} catch (NumberFormatException e) {
				System.err.println("Argument" + " must be an integer."
						+ e.getMessage());
				System.exit(1);
			}
		}

		Properties dbproperties = new Properties();
		try {
			dbproperties.load(QT1V1ConfigService.class.getClassLoader()
					.getResourceAsStream("db.properties"));
			DataSource datasource = MyDataSourceFactory
					.getMySQLDataSource(dbproperties);

			DAOFactory daoFactory = new MysqlDAOFactory(datasource);
			M_Rule rule = daoFactory.getM_RuleDAO().getRuleByRuleId(ruleId,
					ownerId);
			if (rule == null) {
				System.out.println("rule not found!!");
				System.exit(1);
			}

			M_Source source = daoFactory.getM_SourceBackendDAO().getSourceById(
					sourceId);

			if (source == null) {
				System.out.println("source not found!!");
				System.exit(1);
			}

			QT1V1ConfigService qt1v1ConfigService = new QT1V1ConfigService();
			List<PageBlockConfig> pageBlockConfigs = qt1v1ConfigService
					.getBlockConfigByHop(
							0,
							((QT1_V1_RuleContent) rule.ruleContent).blockConfigMain);

			if (pageBlockConfigs.isEmpty()) {
				System.out.println("no pageblockConfig found!");
			}

			WebDriver driver = WebDriverFactory
					.createDriver(BrowserDriverType.firefox_unix);
			String url = source.sourceUrlNormalized;
			driver.manage().window().setSize(new Dimension(1920, 1200));
			// driver.manage().window().maximize();
			Dimension dimension = driver.manage().window().getSize();
			System.out.println("window: h " + dimension.getHeight() + "  w "
					+ dimension.getWidth());
			driver.get(url);
			CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
			script.removeElementCssClassAttr = false;
			script.removeElementIdAttr = false;
			List<PageBlockConfig> pageBlockCfigs = qt1v1ConfigService
					.selectValidPageBlockConfig(url, driver.getPageSource(),
							pageBlockConfigs, script, false);
			if (pageBlockCfigs.isEmpty())
				System.out.println("no valid pageblockConfig found!");
			else
				System.out.println("valid config!");
			driver.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
