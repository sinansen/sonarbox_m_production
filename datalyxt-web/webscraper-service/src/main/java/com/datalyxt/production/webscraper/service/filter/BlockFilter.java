package com.datalyxt.production.webscraper.service.filter;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.exception.webscraper.FilterFailedException;
import com.datalyxt.production.webdatabase.factory.RuntimeDAOFactory;
import com.datalyxt.production.webscraper.model.config.DatasetFilter;
import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.config.FilterValueType;
import com.datalyxt.production.webscraper.model.filter.BlockData;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.util.DateContentUtil;
import com.datalyxt.webscraper.model.util.BlockDataConverter;

public class BlockFilter {

	private RuntimeDAOFactory filterDAOFactory;
	BlockDataConverter blockDataConverter;
	private LinkBlockFilter linkBlockFilter;
	private DateContentUtil dateContentUtil;

	public BlockFilter(RuntimeDAOFactory filterDAOFactory,
			LinkBlockFilter linkBlockFilter) {
		this.filterDAOFactory = filterDAOFactory;
		blockDataConverter = new BlockDataConverter();
		this.linkBlockFilter = linkBlockFilter;
		this.dateContentUtil = new DateContentUtil();
	}

	public List<DatasetBlock> doDatasetFilter(Filter filterConfig,
			List<DatasetBlock> blocks, String urlHashcode)
			throws FilterFailedException {
		List<DatasetBlock> datasetblocks = new ArrayList<DatasetBlock>();
		try {
			if (filterConfig.datasetFilter == null
					|| filterConfig.datasetFilter.isEmpty())
				return blocks;

			HashMap<Long, DatasetBlock> bls = new HashMap<>();
			long base = System.currentTimeMillis();
			for (DatasetBlock dblock : blocks) {
				base = base + 1;
				bls.put(base, dblock);
				dblock.blockId = base;
				List<BlockData> blockdata = new ArrayList<>();
				blockDataConverter.convertToBlockData(dblock, blockdata);
				filterDAOFactory.getM_BlockFilterDAO().saveBlock(blockdata,
						urlHashcode);

			}

			for (DatasetFilter datafilter : filterConfig.datasetFilter) {
				List<Long> blockIds = new ArrayList<Long>();
				if (datafilter.type.equals(FilterValueType.date)) {

					Date date = dateContentUtil.parseDate(datafilter.value);

					blockIds = filterDAOFactory.getM_BlockFilterDAO()
							.queryNum4Include(datafilter.name,
									datafilter.operator, date.getTime(),
									urlHashcode);

				} else if (datafilter.type.equals(FilterValueType.number)) {

					blockIds = filterDAOFactory
							.getM_BlockFilterDAO()
							.queryNum4Include(datafilter.name,
									datafilter.operator,
									Long.valueOf(datafilter.value), urlHashcode);

				} else {
					blockIds = filterDAOFactory.getM_BlockFilterDAO()
							.queryText4Include(datafilter.name,
									datafilter.operator, datafilter.value,
									urlHashcode);
				}
				for (Long blockId : blockIds) {
					DatasetBlock b = bls.get(blockId);
					datasetblocks.add(b);

				}
			}

			return datasetblocks;
		} catch (Exception e) {
			throw new FilterFailedException("doDatasetFilter failed: "
					+ e.getMessage(), e);
		}
	}

	public HashMap<String, LinkBlock> doBlockLinkFilter(Filter filterConfig,
			HashMap<String, LinkBlock> linkBlocks, HashSet<String> blacklist)
			throws FilterFailedException {
		try {
			if (filterConfig != null && filterConfig.enableBlackList)
				// filterConfig.blacklist = daoFactory.getDomainBlackListDAO()
				// .getDomainBlackList();
				filterConfig.blacklist.addAll(blacklist);
			HashMap<String, LinkBlock> filtered = linkBlockFilter.doFilter(
					linkBlocks, filterConfig);
			return filtered;
		} catch (Exception e) {
			throw new FilterFailedException("doBlockLinkFilter failed: "
					+ e.getMessage(), e);
		}
	}
}
