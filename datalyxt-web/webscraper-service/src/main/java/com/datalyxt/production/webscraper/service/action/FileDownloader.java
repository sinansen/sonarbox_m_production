package com.datalyxt.production.webscraper.service.action;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.webmodel.http.HttpConfig;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;

public class FileDownloader {
	public synchronized ActionResultFile download(
			String normalizedUrl, HttpConfig properties)
			throws DownloadFileException {
		InputStream is = null;
		HttpURLConnection connection = null;
		try {
			URL website = new URL(normalizedUrl);
			if (properties.proxyEnabled) {
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(
						properties.proxyHost, properties.proxyPort));

				connection = (HttpURLConnection) website.openConnection(proxy);
			} else{
				connection = (HttpURLConnection) website.openConnection();
			}
			connection.setConnectTimeout(properties.connectionTimeout);
			
			String raw = connection.getHeaderField("Content-Disposition");

			ByteArrayOutputStream baos = new ByteArrayOutputStream();

			is = connection.getInputStream();
			byte[] byteChunk = new byte[4096]; // Or whatever size you want to
												// read in at a time.
			int n;

			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}

			byte[] result = baos.toByteArray();
			ActionResultFile file = new ActionResultFile();
			file.content = result;
			if (raw != null && raw.indexOf("=") != -1) {
				file.dName = raw.split("=")[1]; // getting value after '='
			} else {
				file.dName = null;
			}

			return file;
		} catch (Exception e) {
			throw new DownloadFileException(
					"Failed while reading bytes from s: " + normalizedUrl + " "
							+ e.getMessage(), e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					throw new DownloadFileException(
							"Can't close inputstream : " + e.getMessage(), e);
				}
			}
			if (connection != null) {
				try {
					connection.disconnect();
				} catch (Exception e) {
					throw new DownloadFileException("Can't close connection : "
							+ e.getMessage(), e);
				}
			}
		}
	}
}
