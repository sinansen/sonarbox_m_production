package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.HashMap;
import java.util.UUID;
import java.util.Vector;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.production.webscraper.model.config.BlockType;

public class SupervisedDataColumn {
	public UUID id;
	public HashMap<String, Selector> superVisionNodes = new HashMap<String, Selector>();
	public BlockType blockType;
	public String columnName;
	
	public HashMap<String, UniqueSelectorTemplate> uniqueSelector = new  HashMap<String, UniqueSelectorTemplate>();
	public Vector<IteratedSelectorTemplate> iteratedSelectorTemplates = new Vector<IteratedSelectorTemplate>();


	public SupervisedDataColumn( UUID id, BlockType blockType, String columnName) {
		this.id = id;
		this.blockType = blockType;
		this.columnName = columnName;
	}

	public void addSelector(String cssSelector) {
		if (cssSelector != null && !cssSelector.isEmpty()) {

			cssSelector = cssSelector.replace(" ", "");

			String id = DigestUtils.md5Hex(cssSelector);
			if (superVisionNodes.containsKey(id))
				return;
			Selector s = new Selector(id, cssSelector);
			s.initialize();

			superVisionNodes.put(s.id, s);

		}
	}
}
