package com.datalyxt.production.webscraper.service.action;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.runtime.Block;

public interface BlockActionStrategy {
	public ActionResultText doAction(Block block, BlockAction action)
			throws CreateScreenshotException, UnknownBlockTypeException,
			DownloadFileException, OpenPageUrlException;

}
