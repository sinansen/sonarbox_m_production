package com.datalyxt.production.webscraper.service.action;

import java.util.HashMap;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.action.FollowLinkActionResult;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.service.block.content.BlockContentService;
import com.google.gson.Gson;

public class FollowLinkActionStrategy implements BlockActionStrategy {

	BlockContentService blockContentService = new BlockContentService();

	@Override
	public ActionResultText doAction(Block block, BlockAction action)
			throws CreateScreenshotException, UnknownBlockTypeException,
			DownloadFileException, OpenPageUrlException {
		if (!BlockActionType.followLinks.equals(action.type))
			return null;

		HashMap<String, LinkBlock> linkBlocks = blockContentService
				.getLinkBlocks(block);

		Gson gson = new Gson();
		String actionLinksJson = gson.toJson(linkBlocks.keySet());

		FollowLinkActionResult result = new FollowLinkActionResult();
		result.createdAt = System.currentTimeMillis();
		result.blockId = block.blockId;
		result.content = actionLinksJson;
		result.type = action.type.name();
		for (String key : linkBlocks.keySet()) {
			LinkBlock lbk = linkBlocks.get(key);
			if (lbk.follow)
				result.linkBlocks.put(key, lbk);
		}
		return result;

	}

}
