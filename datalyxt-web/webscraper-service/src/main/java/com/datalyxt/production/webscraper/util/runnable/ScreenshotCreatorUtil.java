package com.datalyxt.production.webscraper.util.runnable;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.WebDriverFactoryException;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class ScreenshotCreatorUtil {
	public static void main(String[] args) {
		String use = "useage: java -jar xxxx.jar xxxxxx proxy:[true|false]";
		System.out.println(use);
		String url = "http://www.google.de";
		boolean proxy = true;
		if (args.length == 2) {
			try {
				url = args[0];
				System.out.println("url: " + url);
				String[] p2 = args[1].split(":");
				proxy = Boolean.valueOf(p2[1]);
				System.out.println("use proxy: " + proxy);
			} catch (Exception e) {
				System.out.println("parameter error. \n" + use);
			}
		} else
			System.out.println(use);

		WebDriver driver = null;
		try {
			if (proxy) {
				Properties properties = new Properties();
				properties.load(ScreenshotCreatorUtil.class.getClassLoader()
						.getResourceAsStream("proxy.properties"));
				String network_proxy_http = properties.getProperty("network_proxy_http");
				int network_proxy_port = Integer.valueOf(properties.getProperty("network_proxy_port"));
				int network_proxy_type = Integer.valueOf(properties.getProperty("network_proxy_type"));
				System.out.println("use network_proxy_http: [" + network_proxy_http + "] network_proxy_port: [" + network_proxy_port
						+ "] network_proxy_type: ["+ network_proxy_type	+ "]");

				driver = WebDriverFactory.createDriver(
						BrowserDriverType.firefox_unix, properties);
			} else
				driver = WebDriverFactory
						.createDriver(BrowserDriverType.firefox_unix);

			
		

			
			PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
			BrowsingConfig b = new BrowsingConfig();
			b.pageLoadTimeout = 60000;
			try {
				pageNavigationUtil
						.navigateToPage(driver, b , url);
			} catch (OpenPageUrlException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			File scrFile = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			
			byte[] imageInByte;
			BufferedImage originalImage = ImageIO.read(scrFile);

			// convert BufferedImage to byte array
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(originalImage, "png", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
			
			FileUtils.copyFile(scrFile, new File("screenshot.png"));

//			byte[] data = ((TakesScreenshot) driver)
//					.getScreenshotAs(OutputType.BYTES);
//			FileUtils.writeByteArrayToFile(new File("screenshot.png"), data);
		} catch ( IOException | WebDriverFactoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			if (driver != null) {
//				driver.quit();
				driver.close();

			}
		}

	}
}
