package com.datalyxt.production.webscraper.service.action;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.exception.processing.DownloadFileException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.config.BlockAction;

public interface FileActionStrategy {
	public ActionResultFile doAction(String url, BlockAction action, int timeout)
			throws CreateScreenshotException, UnknownBlockTypeException,
			DownloadFileException, OpenPageUrlException;

}
