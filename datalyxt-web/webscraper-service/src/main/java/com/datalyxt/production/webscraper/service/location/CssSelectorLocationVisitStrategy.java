package com.datalyxt.production.webscraper.service.location;

import java.util.Date;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.DataBlock;
import com.datalyxt.production.webscraper.model.runtime.KeyvalueBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.production.webscraper.model.runtime.TimeBlock;
import com.datalyxt.production.webscraper.service.block.content.paging.PagingBlockVisitStrategy;
import com.datalyxt.util.DateContentUtil;
import com.datalyxt.util.parser.LocationBasedPageParser;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.Page;
import com.datalyxt.webscraper.model.page.PageBlock;

public class CssSelectorLocationVisitStrategy implements
		ContentBlockVisitStrategy {

	LocationBasedPageParser parser = new LocationBasedPageParser();
	DateContentUtil dateContentUtil = new DateContentUtil();
	PagingBlockVisitStrategy pagingBlockVisitStrategy = new PagingBlockVisitStrategy();

	@Override
	public void visit(Block block, BlockConfig config, Page htmlPage, CleanHtmlDocumentScript script)
			throws NullBlockLocationException, DefinedBlockNotFound,
			BlockExtractionException, TimeBlockException, HtmlDocumentCreationException, PagingExtractionException {

		if (config.location == null || config.location.type == null)
			throw new NullBlockLocationException("block location is null");

		if (!config.location.type.equals(BlockLocationType.cssSelector))
			return;

		PageBlock pageBlock = parser.parse(htmlPage, config.location.value, htmlPage.dynamicParameters, script);

		if (block.type.equals(BlockType.dataBlock)) {

			DataBlock parsedBlock = (DataBlock) block;

			parsedBlock.locationValue = config.location.value;
			parsedBlock.blockHtml = pageBlock.blockHtml;
			parsedBlock.blockText = pageBlock.blockText;
			parsedBlock.blockStructure = pageBlock.blockStructure;
			parsedBlock.blockHtmlHash = pageBlock.blockHtmlHash;
			parsedBlock.blockTextHash = pageBlock.blockTextHash;
			parsedBlock.blockStructureHash = pageBlock.blockStructureHash;

			for (Link link : pageBlock.linkList.values()) {
				LinkBlock linkBlock = new LinkBlock();
				linkBlock.type = BlockType.linkBlock;
				linkBlock.link = link;
				linkBlock.text = link.linkText;
				parsedBlock.linkBlocks.put(link.url, linkBlock);
			}

		} else if (block.type.equals(BlockType.timeBlock)) {

			TimeBlock parsedBlock = (TimeBlock) block;

			parsedBlock.locationValue = config.location.value;
			parsedBlock.blockHtml = pageBlock.blockHtml;
			parsedBlock.blockText = pageBlock.blockText;
			parsedBlock.blockStructure = pageBlock.blockStructure;
			parsedBlock.blockHtmlHash = pageBlock.blockHtmlHash;
			parsedBlock.blockTextHash = pageBlock.blockTextHash;
			parsedBlock.blockStructureHash = pageBlock.blockStructureHash;

			Date date = dateContentUtil.parseDate(parsedBlock.blockText);
			parsedBlock.date = date;
		} else if (block.type.equals(BlockType.keyvalueBlock)) {

			KeyvalueBlock parsedBlock = (KeyvalueBlock) block;

			parsedBlock.locationValue = config.location.value;
			parsedBlock.blockHtml = pageBlock.blockHtml;
			parsedBlock.blockText = pageBlock.blockText;
			parsedBlock.blockStructure = pageBlock.blockStructure;
			parsedBlock.blockHtmlHash = pageBlock.blockHtmlHash;
			parsedBlock.blockTextHash = pageBlock.blockTextHash;
			parsedBlock.blockStructureHash = pageBlock.blockStructureHash;

		} else if (block.type.equals(BlockType.linkBlock)) {

			LinkBlock parsedBlock = (LinkBlock) block;
			parsedBlock.follow = config.follow;
			parsedBlock.locationValue = config.location.value;
			parsedBlock.blockHtml = pageBlock.blockHtml;
			parsedBlock.blockText = pageBlock.blockText;
			parsedBlock.blockStructure = pageBlock.blockStructure;
			parsedBlock.blockHtmlHash = pageBlock.blockHtmlHash;
			parsedBlock.blockTextHash = pageBlock.blockTextHash;
			parsedBlock.blockStructureHash = pageBlock.blockStructureHash;

			for (Link link : pageBlock.linkList.values()) {

				parsedBlock.link = link;
				parsedBlock.text = link.linkText;

				break;
			}
		}else if(block.type.equals(BlockType.pagingBlock)){
			PagingBlock parsedBlock = (PagingBlock)block; 
			parsedBlock.locationValue = config.location.value;
			parsedBlock.blockHtml = pageBlock.blockHtml;
			parsedBlock.blockText = pageBlock.blockText;
			parsedBlock.blockStructure = pageBlock.blockStructure;
			parsedBlock.blockHtmlHash = pageBlock.blockHtmlHash;
			parsedBlock.blockTextHash = pageBlock.blockTextHash;
			parsedBlock.blockStructureHash = pageBlock.blockStructureHash;
			
			pagingBlockVisitStrategy.visit(parsedBlock, config, htmlPage, script);
		}

	}
}
