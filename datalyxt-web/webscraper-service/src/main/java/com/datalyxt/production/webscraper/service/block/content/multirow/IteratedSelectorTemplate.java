package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.List;

import com.datalyxt.production.webscraper.model.config.BlockType;

public class IteratedSelectorTemplate {

	public int rowIdentifierPosition;
	public String rowIdentifier;
	public String cleanedRowIdentifier;
	
	public List<String> preIdentifier;
	public List<String> postIdentifier;
	public BlockType blockType;
	public String name;
	
	public IteratedSelectorTemplate(BlockType type, String name) {
		this.blockType = type;
		this.name = name;
	}

	public void setIteratedElement(String rowIdentifier, String cleanedRowIdentifier) {
		this.rowIdentifier = rowIdentifier;
		this.cleanedRowIdentifier = cleanedRowIdentifier;
	}

}
