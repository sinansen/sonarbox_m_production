package com.datalyxt.production.webscraper.service.parser;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.webscraper.model.page.Page;

public class PageCssSelectorParser {

	public Collection<BlockLocation> extractChildrenEleSelector(Document doc,
			Page page, Iterator<String> parentSelectors)
			throws BlockExtractionException, DefinedBlockNotFound {
		HashMap<String, BlockLocation> tobeCheckedCssSelector = new HashMap<String, BlockLocation>();
		String parentSelector = "";
		try {
			while (parentSelectors.hasNext()) {
				parentSelector = parentSelectors.next();
				Element element = doc.select(parentSelector).first();

				for (Element child : element.getAllElements()) {
					BlockLocation lo = new BlockLocation();
					lo.type = BlockLocationType.cssSelector;
					lo.value = child.cssSelector();
					lo.value = lo.value.replace(" ", "");
					tobeCheckedCssSelector.put(lo.value, lo);
				}
			}
		} catch (Exception e) {
			String error = ExceptionUtils.getStackTrace(e);
			throw new BlockExtractionException(
					"can't extrac children selector from parent: "
							+ parentSelector + " . " + error, e);
		}

		return tobeCheckedCssSelector.values();

	}

	public Collection<BlockLocation> extractChildrenEleSelector(Page page,
			Iterator<String> parentSelectors, CleanHtmlDocumentScript script)
			throws DefinedBlockNotFound, BlockExtractionException,
			HtmlDocumentCreationException {
		Document doc = HtmlDocumentUtil.getDocumentFromSource(page.pageUrl.url,
				page.getHtmlSelen(), script);
		return extractChildrenEleSelector(doc, page, parentSelectors);
	}

}
