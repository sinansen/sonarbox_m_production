package com.datalyxt.production.webscraper.service.action;

import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.webscraper.model.action.ActionMetaConstant;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.util.PageScreenshotUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.page.BlockScreenshot;

public class ScreenshotActionStrategy implements FileActionStrategy {

	private PageScreenshotUtil pageScreenshotUtil = new PageScreenshotUtil();

	private WebDriver webdriver;

	private PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();

	public ScreenshotActionStrategy(WebDriver webdriver) {
		this.webdriver = webdriver;
	}

	@Override
	public ActionResultFile doAction(String pageUrl, BlockAction action,
			int timeout) throws CreateScreenshotException,
			UnknownBlockTypeException, OpenPageUrlException {
		if (!BlockActionType.screenshot.equals(action.type))
			return null;
		BrowsingConfig cfg = new BrowsingConfig();
		cfg.pageLoadTimeout = timeout;
		pageNavigationUtil.navigateToPage(webdriver, cfg, pageUrl);

		BlockScreenshot screenshot = pageScreenshotUtil
				.createScreenshot(webdriver);

		ActionResultFile result = new ActionResultFile();

		result.createdAt = System.currentTimeMillis();
		result.size = screenshot.imgLength;
		result.type = ActionMetaConstant.screenshot_img;
		result.status = ActionMetaConstant.file_created;
		result.content = screenshot.img;

		return result;

	}

}
