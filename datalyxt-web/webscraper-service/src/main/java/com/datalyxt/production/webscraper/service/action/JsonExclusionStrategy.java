package com.datalyxt.production.webscraper.service.action;

import com.datalyxt.production.webscraper.model.runtime.Invisible;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

public class JsonExclusionStrategy implements ExclusionStrategy {

	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

	public boolean shouldSkipField(FieldAttributes f) {
		Invisible in = f.getAnnotation(Invisible.class);
		if (in != null) {
			return Invisible.invisible;
		} else
			return false;
	}

}
