package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.ArrayList;
import java.util.List;

public class RowBasedSelectorTemplate {
	public int rowIdentifierPosition;
	public List<String> rowIdentifier = new ArrayList<String>();
	public String cleanedRowIdentifier;
	
	public List<String> preIdentifier;
	
	
	public List<IteratedSelectorTemplate> columnTemplates = new ArrayList<>();
}
