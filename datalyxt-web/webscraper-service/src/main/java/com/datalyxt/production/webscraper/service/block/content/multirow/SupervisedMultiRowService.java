package com.datalyxt.production.webscraper.service.block.content.multirow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockLocationType;

public class SupervisedMultiRowService {

	public BlockTrainingData addSupervisiedDataColumn(
			Collection<BlockConfig> blockconfigs)
			throws UnsupportedBlockLocationException, ColumnTemplateException {

		BlockTrainingData blockTrainingData = new BlockTrainingData();

		blockTrainingData.columns = new ArrayList<>();
		int rows = 0;
		int columns = 0;
		for (BlockConfig blockconfig : blockconfigs) {

			SupervisedDataColumn dataColumn = new SupervisedDataColumn(
					UUID.randomUUID(), blockconfig.type, blockconfig.name);
			dataColumn.addSelector(blockconfig.location.value);
			if (blockconfig.supervised != null) {
				for (BlockLocation selector : blockconfig.supervised) {
					if (selector.type != BlockLocationType.cssSelector)
						throw new UnsupportedBlockLocationException(
								"SupervisedMultiRowService canot process location type: "
										+ selector.type);
					dataColumn.addSelector(selector.value);
				}
			}
			blockTrainingData.columns.add(dataColumn);
			rows = dataColumn.superVisionNodes.size();

		}
		columns = blockTrainingData.columns.size();
		if (rows == 1 && columns == 1) {
			blockTrainingData.type = TrainingDataType.singleElement;
		} else if (rows == 1 && columns > 1) {
			blockTrainingData.type = TrainingDataType.singleRow;
		} else if (rows > 1 && columns == 1) {
			blockTrainingData.type = TrainingDataType.singleColumn;
		} else if (rows > 1 && columns > 1) {
			blockTrainingData.type = TrainingDataType.multiElement;
		} else
			throw new ColumnTemplateException(
					"unsupported training data type: [row " + rows + ", column"
							+ columns + " ] ");

		return blockTrainingData;
	}

	public List<RowBasedSelectorTemplate> createRowBasedTemplate( MultiRowDetectionContext multiRowDetectionContext,
			BlockTrainingData blockTrainingData) throws ColumnTemplateException {

		return multiRowDetectionContext.createRowTemplate(blockTrainingData);
	}

	public BlockConfigWrapper identifyIteratedData(BlockLocation location,
			List<RowBasedSelectorTemplate> templates)
			throws UnsupportedBlockLocationException {
		if (location.type != BlockLocationType.cssSelector)
			throw new UnsupportedBlockLocationException(
					"SupervisedMultiRowService canot process location type: "
							+ location.type);
		String id = DigestUtils.md5Hex(location.value);
		Selector selector = new Selector(id, location.value);
		selector.initialize();
		for (RowBasedSelectorTemplate template : templates) {
			// contains row identifier ?
			IteratedToken possibleRowIdentifier = selector.iterationToken
					.get(template.rowIdentifierPosition);
			if (possibleRowIdentifier != null) {
				// if (possibleRowIdentifier.cleanedValue
				// .equals(template.cleanedRowIdentifier)) {
				// is preidentifier the same?
				List<String> preIdentifier = selector.allTokens.subList(0,
						template.rowIdentifierPosition);
				if (Arrays.deepEquals(preIdentifier.toArray(),
						template.preIdentifier.toArray())) {
					int lastIndex = selector.allTokens.size() - 1;
					boolean hasPostIdentifier = (lastIndex > template.rowIdentifierPosition);
					for (IteratedSelectorTemplate iteratedSelectorTemplate : template.columnTemplates) {
						if (iteratedSelectorTemplate.postIdentifier.isEmpty()) {
							// list ??
							if (!hasPostIdentifier) {
								BlockConfigWrapper blockConfigWrapper = new BlockConfigWrapper();
								BlockConfig blockConfig = new BlockConfig();
								blockConfig.location = location;
								blockConfig.type = iteratedSelectorTemplate.blockType;
								blockConfig.name = iteratedSelectorTemplate.name;

								blockConfigWrapper.blockConfig = blockConfig;
								blockConfigWrapper.rowIdentifier = possibleRowIdentifier;
								blockConfigWrapper.row = preIdentifier;
								blockConfigWrapper.row
										.add(possibleRowIdentifier.value);
								return blockConfigWrapper;
							}
						} else {
							// table ??
							if (hasPostIdentifier) {
								List<String> postIdentifier = selector.allTokens
										.subList(
												template.rowIdentifierPosition + 1,
												selector.allTokens.size());
								boolean isInColumn = Arrays.deepEquals(
										postIdentifier.toArray(),
										iteratedSelectorTemplate.postIdentifier
												.toArray());
								if (isInColumn) {
									BlockConfigWrapper blockConfigWrapper = new BlockConfigWrapper();
									BlockConfig blockConfig = new BlockConfig();
									blockConfig.location = location;
									blockConfig.type = iteratedSelectorTemplate.blockType;
									blockConfig.name = iteratedSelectorTemplate.name;

									blockConfigWrapper.blockConfig = blockConfig;
									blockConfigWrapper.rowIdentifier = possibleRowIdentifier;
									blockConfigWrapper.row = preIdentifier;
									blockConfigWrapper.row
											.add(possibleRowIdentifier.value);
									return blockConfigWrapper;
								}
							}
						}
					}

				}
				// }
			}

		}
		return null;
	}

}
