package com.datalyxt.production.webscraper.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.html.CreateScreenshotException;
import com.datalyxt.webscraper.model.page.BlockScreenshot;

public class PageScreenshotUtil {

	public synchronized BlockScreenshot createScreenshot(WebDriver driver) throws CreateScreenshotException {
		try {
			BlockScreenshot screenshot = new BlockScreenshot();

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

			byte[] imageInByte = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.BYTES);

			screenshot.img = imageInByte;
			screenshot.imgLength = imageInByte.length;

			return screenshot;
		} catch (Exception e) {
			throw new CreateScreenshotException(e);
		}
	}
}
