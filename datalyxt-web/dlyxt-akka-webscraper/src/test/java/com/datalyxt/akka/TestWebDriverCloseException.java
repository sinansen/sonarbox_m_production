package com.datalyxt.akka;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.SessionNotFoundException;
import org.openqa.selenium.remote.UnreachableBrowserException;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestWebDriverCloseException {
	@Test
	public void testClose() {
		try {
			WebDriver driver = WebDriverFactory
					.createDriver(BrowserDriverType.firefox_windows);
			driver.close();
			String url = "http://www.google.de";
			driver.get(url);
		} catch (UnreachableBrowserException exception) {
			OpenPageUrlException ope = new OpenPageUrlException("err",
					exception);
			if (ope.getCause() instanceof UnreachableBrowserException)
				System.out.println("<UnreachableBrowserException> \n");
			else
				System.out.println("unkown\n");
		}

	}

	@Test
	public void testQuit() {
		try {
			WebDriver driver = WebDriverFactory
					.createDriver(BrowserDriverType.firefox_windows);
			String url = "http://www.google.de";
			driver.quit();
			driver.get(url);
		} catch (SessionNotFoundException exception) {
			OpenPageUrlException ope = new OpenPageUrlException("err",
					exception);
			if (ope.getCause() instanceof SessionNotFoundException)
				System.out.println("<SessionNotFoundException> \n");
			else
				System.out.println("unkown\n");
		}
	}

}
