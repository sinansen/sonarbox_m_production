package com.datalyxt.akka.db;

import static org.junit.Assert.assertFalse;

import java.sql.SQLException;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class TestStorageAvailable {

	@Test
	public void testMySQLStorageAvailable() throws SQLException {
		String MYSQL_DB_URL = "jdbc:mysql://1.1.1.1:3306/dlyxtweb";
		String MYSQL_DB_USERNAME = "some";
		String MYSQL_DB_PASSWORD = "some";

		boolean isAvailable = false;
		MysqlDataSource datasource = null;
		datasource = new MysqlDataSource();
		datasource.setUrl(MYSQL_DB_URL);
		datasource.setUser(MYSQL_DB_USERNAME);
		datasource.setPassword(MYSQL_DB_PASSWORD);

		DAOFactory websearchRulesDAOFactory = new MysqlDAOFactory(datasource);
		try {
			isAvailable = websearchRulesDAOFactory.isStorageAvailable();
		} catch (DAOFactoryException e) {
			// TODO Auto-generated catch block
			isAvailable = false;
		}
		assertFalse(isAvailable);
	}

	@Test
	public void testDigit() {
		int number = 10; // = some int
		int userCode = number % 10;
		int sysCode = number - userCode;
		System.out.println(sysCode+" : "+userCode);


	}
}
