package com.datalyxt.akka.db;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.html.WebDriverFactoryException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webscraper.util.runnable.ScreenshotCreatorUtil;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;

public class TestSource {
	

	public static void main(String[] args) {
		Properties dbproperties = new Properties();

		try {
			dbproperties.load(TestSource.class.getClassLoader()
					.getResourceAsStream("db.properties"));
			DataSource datasource = MyDataSourceFactory
					.getMySQLDataSource(dbproperties);
			DAOFactory daoFactory = new MysqlDAOFactory(datasource);

			List<M_Source> srces = daoFactory.getM_SourceBackendDAO()
					.getSourcesByStatus(M_SourceStatus.activated);

			Properties properties = new Properties();
			properties.load(ScreenshotCreatorUtil.class.getClassLoader()
					.getResourceAsStream("proxy.properties"));
			String network_proxy_http = properties
					.getProperty("network_proxy_http");
			int network_proxy_port = Integer.valueOf(properties
					.getProperty("network_proxy_port"));
			int network_proxy_type = Integer.valueOf(properties
					.getProperty("network_proxy_type"));
			System.out.println("use network_proxy_http: [" + network_proxy_http
					+ "] network_proxy_port: [" + network_proxy_port
					+ "] network_proxy_type: [" + network_proxy_type + "]");

			WebDriver driver = WebDriverFactory.createDriver(
					BrowserDriverType.firefox_unix, properties);

			BrowsingConfig browsingConfig = new BrowsingConfig();
			browsingConfig.pageLoadTimeout = 30000;
			PageNavigationUtil pageNavigationUtil = new PageNavigationUtil();
			List<Data> data = new ArrayList<>();
			for (M_Source src : srces) {
				if (src.sourceName.contains("(Test)")) {
					long start = System.currentTimeMillis();
					try {
//						pageNavigationUtil.navigateToPage(driver,
//								browsingConfig, src.sourceUrlNormalized);
						driver.get(src.sourceUrlNormalized);
						long end = System.currentTimeMillis();
						long used = (end - start);
						Data d = new Data();
						d.id = src.sourceId;
						d.url = src.sourceUrlNormalized;
						d.usedTime = used;
						System.out.println(d.usedTime + "  " + d.url);
						data.add(d);
					} catch (Exception e) {
						long end = System.currentTimeMillis();
						long used = (end - start);
						Data d = new Data();
						d.id = src.sourceId;
						d.url = src.sourceUrlNormalized;
						d.usedTime = used;
						data.add(d);
						driver.close();
						driver = WebDriverFactory.createDriver(
								BrowserDriverType.firefox_unix, properties);
						
						System.out.println("timeout: "+d.usedTime + "  " + d.url);
					}

				}
			}
			generateCsvFile(data);
		} catch (IOException | M_SourceDBException | DAOFactoryException
				| SQLException | WebDriverFactoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void generateCsvFile(List<Data> data) {
		try {
			PrintWriter pw = new PrintWriter(new File("/home/test.csv"));
			StringBuilder sb = new StringBuilder();
			sb.append("id");
			sb.append(',');
			sb.append("URL");
			sb.append(',');
			sb.append("time");
			sb.append('\n');
			for (Data d : data) {
				sb.append(d.id);
				sb.append(',');
				sb.append(d.url + " ");
				sb.append(',');
				sb.append(d.usedTime);
				sb.append('\n');
			}
			pw.write(sb.toString());
			pw.close();
			System.out.println("done!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

class Data {
	public long usedTime;
	public long id;
	public String url;
}
