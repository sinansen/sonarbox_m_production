package com.datalyxt.akka;

import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import com.datalyxt.production.akka.WebsearchProps;
import com.google.gson.Gson;

public class TestWebPropertiesConfig {
	@Test
	public void testConfigJson() {
		try {
			InputStream in = TestWebPropertiesConfig.class.getClassLoader()
					.getResourceAsStream("websearch_properties_json.txt");
			String theString = IOUtils.toString(in, "utf-8");
			Gson gson = new Gson();
			WebsearchProps websearchProps = gson.fromJson(theString,
					WebsearchProps.class);
			assertTrue(websearchProps.frontier_access_db_interval_normal > 0);
			System.out.println(websearchProps.resumableCrawling);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
