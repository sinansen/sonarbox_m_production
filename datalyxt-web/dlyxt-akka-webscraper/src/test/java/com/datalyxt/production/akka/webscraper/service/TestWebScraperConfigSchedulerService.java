package com.datalyxt.production.akka.webscraper.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceConfigMessage;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceRule;
import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.rule.M_Rule;

public class TestWebScraperConfigSchedulerService {
	@Test
	public void testService() throws M_RuleExecutionDBException,
			DAOFactoryException, M_WebScraperPageDBException, IOException,
			SQLException {
		WebScraperConfigSchedulerService service = new WebScraperConfigSchedulerService();
		long fetchFromDBTime = 1458901966506L;

		Properties dbproperties = new Properties();
		dbproperties.load(TestWebScraperConfigSchedulerService.class
				.getClassLoader().getResourceAsStream("db.properties"));

		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(dbproperties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);

		boolean isFinished = service.isCurrentScanFinished(daoFactory,
				fetchFromDBTime);
		System.out.println(isFinished);
	}

	@Test
	public void testServiceDBConnection() {
		WebScraperConfigSchedulerService service = new WebScraperConfigSchedulerService();
		long fetchFromDBTime = 1458901966506L;

		Properties dbproperties = new Properties();
		try {
			dbproperties.load(TestWebScraperConfigSchedulerService.class
					.getClassLoader().getResourceAsStream("db.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DataSource datasource = null;
		try {
			datasource = MyDataSourceFactory.getMySQLDataSource(dbproperties);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);

		boolean isFinished = false;
		for (int i = 0; i < 100; i++) {
			try {
				System.out.println("access db");
				isFinished = service.isCurrentScanFinished(daoFactory,
						fetchFromDBTime);
				System.out.println("access db successful");
				Thread.sleep(5000);
			} catch (M_RuleExecutionDBException | DAOFactoryException
					| M_WebScraperPageDBException | InterruptedException e) {
				// TODO Auto-generated catch block
				System.out.println("access db failed");
				e.printStackTrace();
			}
		}
		System.out.println(isFinished);
	}

	@Test
	public void testGetInitial() {

		long fetchTimeFromDB = System.currentTimeMillis();

		WebScraperConfigSchedulerService webscraperConfigSchedulerService = new WebScraperConfigSchedulerService();

		Properties dbproperties = new Properties();
		try {
			dbproperties.load(TestWebScraperConfigSchedulerService.class
					.getClassLoader().getResourceAsStream("db.properties"));

			DataSource datasource = MyDataSourceFactory
					.getMySQLDataSource(dbproperties);
			DAOFactory daoFactory = new MysqlDAOFactory(datasource);

			List<M_Rule> rules = webscraperConfigSchedulerService
					.getRules(daoFactory);

			List<ScraperSourceRule> srces = webscraperConfigSchedulerService
					.getSourcesForExecution(rules, daoFactory);

			webscraperConfigSchedulerService.insertRecords(srces,
					fetchTimeFromDB, daoFactory);

			ScraperSourceConfigMessage sourcemsg = webscraperConfigSchedulerService
					.createSourceConfigMsg(fetchTimeFromDB, srces, daoFactory);
			System.out.println(sourcemsg.initialPages.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
