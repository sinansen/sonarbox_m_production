package com.datalyxt.production.akka.webscraper;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import com.datalyxt.exception.html.WebDriverFactoryException;
import com.datalyxt.production.akka.CrawlConfig;
import com.datalyxt.production.akka.WebsearchProps;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceConfigMessage;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.dao.impl.M_FileStorageDAOImpl;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webscraper.model.M_ProcessedWebScraperPage;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.google.gson.Gson;

public class WebScraperMain {
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static final Logger logger = LoggerFactory
			.getLogger(WebScraperMain.class);

	public static boolean isWindows() {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isUnix() {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS
				.indexOf("aix") > 0);

	}

	public static void main(String[] args) {

		// final Config config = ConfigFactory.load("webscraper");
		//
		// ActorSystem system = ActorSystem.create("DatalyxtWebScraperSystem",
		// config);

		ActorSystem system = ActorSystem.create("DatalyxtWebScraperSystem");
		Properties dbproperties = new Properties();
		Properties crawlingProperties = new Properties();
		boolean loadTrack = true;
		WebsearchProps websearchProps = null;
		try {
			dbproperties.load(WebScraperMain.class.getClassLoader()
					.getResourceAsStream("db.properties"));

			crawlingProperties.load(WebScraperMain.class.getClassLoader()
					.getResourceAsStream("webcrawler.properties"));

			InputStream in = WebScraperMain.class.getClassLoader()
					.getResourceAsStream("websearch.json");
			String theString = IOUtils.toString(in, "utf-8");
			Gson gson = new Gson();
			websearchProps = gson.fromJson(theString, WebsearchProps.class);

		} catch (Exception e) {
			logger.error("load system properties failed. ", e);
			loadTrack = false;
		}

		if (!loadTrack) {

			System.exit(0);
		}
		logger.info("worker time out in millis "
				+ websearchProps.worker_timeout);
		logger.info("max_allowed_processing_delay "
				+ websearchProps.max_allowed_processing_delay);
		websearchProps.driver_type = BrowserDriverType.firefox_windows;

		if (isUnix())
			websearchProps.driver_type = BrowserDriverType.firefox_unix;

		DataSource datasource = null;
		try {
			datasource = MyDataSourceFactory
					.getMySQLDataSource(dbproperties);
		} catch (SQLException e) {
			String error = ExceptionUtils.getStackTrace(e);
			logger.error("MyDataSourceFactory failed. "+error, e);
			System.exit(0);
		}

		DAOFactory schedulerDAOFactory = new MysqlDAOFactory(datasource);
		DAOFactory dispatcherDataDAOFactory = new MysqlDAOFactory(datasource);
		List<String> routees = new ArrayList<>();
		for (int i = 0; i < websearchProps.qt1v1_actor_amount; i++) {
			ActorRef qt1v1;
			try {
				qt1v1 = createQt1v1Actor(system, crawlingProperties,
						websearchProps, dbproperties, datasource, i);
				routees.add(qt1v1.path().toString());
			} catch (WebDriverFactoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error("create driver failed. " + e.getMessage(), e);
				System.exit(0);
			}

			// system.eventStream().subscribe(qt1v1, M_WebScraperPage.class);
		}
		System.out.println("routees: " + String.join(" , ", routees));
		ActorRef dispatcher = system.actorOf(Props.create(
				WebScraperPageDispatchActor.class, dispatcherDataDAOFactory,
				websearchProps, routees),
				SystemConfiguration.DISPATCHER_ACTOR_NAME_STRING);

		system.eventStream().subscribe(dispatcher,
				ScraperSourceConfigMessage.class);
		system.eventStream().subscribe(dispatcher,
				M_ProcessedWebScraperPage.class);

		ActorRef scheduler = system.actorOf(Props.create(
				WebScraperConfigSchedulerActor.class, websearchProps,
				schedulerDAOFactory),
				SystemConfiguration.WEBSEARCHSCHEDULER_ACTOR_NAME_STRING);

		// system.actorOf(Props.create(WebSearchChaosMonkey.class),
		// SystemConfiguration.WEBSEARC_CHOASMONKEY_ACTOR_NAME_STRING);

	}

	static ActorRef createQt1v1Actor(ActorSystem system,
			Properties crawlingProperties, WebsearchProps websearchProps,
			Properties dbproperties, DataSource datasource, int actorId) throws WebDriverFactoryException {

		CrawlConfig crawlconf = new CrawlConfig(crawlingProperties);
	
		M_FileStorageDAO fileStorageDAO = new M_FileStorageDAOImpl(dbproperties);
		DAOFactory qt1v1DataDAOFactory = new MysqlDAOFactory(datasource);

		ActorRef qt1v1 = system.actorOf(Props.create(QT1V1Actor.class,
				crawlconf, websearchProps,
				qt1v1DataDAOFactory, fileStorageDAO),
				SystemConfiguration.QT1V1_ACTOR_NAME_STRING + actorId);
		return qt1v1;
	}

}
