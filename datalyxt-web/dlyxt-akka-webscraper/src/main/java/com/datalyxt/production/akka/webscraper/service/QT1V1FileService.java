package com.datalyxt.production.akka.webscraper.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.production.akka.webscraper.service.fetcher.SeleniumPageFetcher;
import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.exception.processing.FileSizeExceededException;
import com.datalyxt.production.exception.webscraper.FileActionException;
import com.datalyxt.production.exception.webscraper.SelectorNotFoundException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webmodel.http.HttpConfig;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.production.webscraper.service.action.FileActionContext;
import com.datalyxt.production.webscraper.service.action.FileDownloadActionStrategy;
import com.datalyxt.production.webscraper.service.action.ScreenshotActionStrategy;

public class QT1V1FileService {

	private static final Logger logger = LoggerFactory
			.getLogger(QT1V1FileService.class);

	private FileActionContext fileActionContext;

	public QT1V1FileService(SeleniumPageFetcher fetcher, HttpConfig config) {
		fileActionContext = new FileActionContext();
		fileActionContext.addActionStrategy(BlockActionType.fileStorage,
				new FileDownloadActionStrategy(config));
		fileActionContext.addActionStrategy(BlockActionType.screenshot,
				new ScreenshotActionStrategy(fetcher.webDriver));
	}

	public void processDocumentPage(M_WebScraperPage inProcessPage,
			DAOFactory daoFactory, QT1V1ConfigService qt1v1ConfigService)
			throws M_QT1V1ResultDBException, DAOFactoryException,
			FileStorageException, FileActionException,
			FileSizeExceededException, OpenPageUrlException,
			SelectorNotFoundException {
		logger.info("process file " + inProcessPage.pageURL.url);
		// get config by hop
		List<PageBlockConfig> pageBlockConfigs = qt1v1ConfigService
				.getBlockConfigByHop(inProcessPage.pageHops,
						inProcessPage.ruleContent.blockConfigMain);

		if (pageBlockConfigs.isEmpty()) {
			throw new SelectorNotFoundException("no selector defined for page "
					+ inProcessPage.pageURL.url);
		}

		HashMap<BlockActionType, BlockAction> actionTypes = new HashMap<>();
		for (PageBlockConfig pageBlockConfig : pageBlockConfigs)
			for (BlockConfig config : pageBlockConfig.block)
				for (BlockAction action : config.action) {
					actionTypes.put(action.type, action);
				}

		for (BlockAction action : actionTypes.values()) {

			doAction(inProcessPage, action);

		}
		logger.info("finish process file " + inProcessPage.pageURL.url);
	}

	public void doAction(M_WebScraperPage inProcessPage, BlockAction action)
			throws M_QT1V1ResultDBException, DAOFactoryException,
			FileActionException, FileSizeExceededException,
			OpenPageUrlException {
		ActionResultFile result = null;
		int maxPageSize = inProcessPage.sourceConfig.qt1v1SourceConfig.maxPDFSize; // 10
		// MB
		// max
		int timeout = inProcessPage.sourceConfig.qt1v1SourceConfig.pageLoadTimeout;
		logger.info("QT1V1FileService doAction -> timeout value: " + timeout);
		result = fileActionContext.doAction(inProcessPage.pageURL.url, action,
				timeout);

		if (result == null)
			throw new FileActionException(
					"action result is null after doAction: "
							+ action.type.name() + "  "
							+ inProcessPage.pageURL.url);

		if (result.size > maxPageSize) {
			result.content = null;
			throw new FileSizeExceededException("file size limit exceeded: "
					+ inProcessPage.pageURL.url);
		}
		inProcessPage.actionResultFiles.add(result);
	}

}
