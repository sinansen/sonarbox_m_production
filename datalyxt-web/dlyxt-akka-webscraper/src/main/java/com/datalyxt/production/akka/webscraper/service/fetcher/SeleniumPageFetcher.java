package com.datalyxt.production.akka.webscraper.service.fetcher;

import java.util.Properties;

import org.apache.http.HttpStatus;
import org.openqa.selenium.WebDriver;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.extraction.PageFetchException;
import com.datalyxt.exception.html.WebDriverFactoryException;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.datalyxt.util.GetHost;
import com.datalyxt.util.PageNavigationUtil;
import com.datalyxt.util.WebDriverFactory;
import com.datalyxt.util.url.URLNormalizer;
import com.datalyxt.webmodel.user.BrowsingConfig;
import com.datalyxt.webscraper.model.BrowserDriverType;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.page.Page;

public class SeleniumPageFetcher implements Fetcher{

	PageNavigationUtil pageNavigationUtil;

	public WebDriver webDriver;

	private BrowserDriverType driverType;

	private Properties proxy;

	public SeleniumPageFetcher(BrowserDriverType driverType) {
		this.driverType = driverType;
		webDriver = WebDriverFactory.createDriver(driverType);
		pageNavigationUtil = new PageNavigationUtil();
	}

	public SeleniumPageFetcher(BrowserDriverType driverType, Properties proxy)
			throws WebDriverFactoryException {
		this.driverType = driverType;
		this.proxy = proxy;
		webDriver = WebDriverFactory.createDriver(driverType, proxy);
		pageNavigationUtil = new PageNavigationUtil();
	}

	public PageWrapper fetchPage(SourceConfig config, Link curURL)
			throws PageFetchException {

		PageWrapper pageWrapper = new PageWrapper();

		pageWrapper.pageHeader = new HtmlPageHeader();

		pageWrapper.page = new Page();
		pageWrapper.page.pageUrl = curURL;

		BrowsingConfig browsingConfig = new BrowsingConfig();
		boolean isTimeout = false;
		if (config != null) {
			if (config.qt1v2SourceConfig != null)
				browsingConfig.pageLoadTimeout = config.qt1v2SourceConfig.pageLoadTimeout;
			else if (config.qt1v1SourceConfig != null)
				browsingConfig.pageLoadTimeout = config.qt1v1SourceConfig.pageLoadTimeout;
		}
		try {
			pageNavigationUtil.navigateToPage(webDriver, browsingConfig,
					curURL.url);
		} catch (OpenPageUrlException e) {
			throw new PageFetchException("SeleniumPageFetcher error: "
					+ e.getMessage(), e);
//			isTimeout = true;
//			try {
//				Jsoup.parse(webDriver.getPageSource());
//			} catch (Exception ex) {
//				String message = "";
//				try {
//					closeDriver();
//				} catch (OpenPageUrlException e1) {
//					message = e1.getMessage();
//				}
//				try {
//					restart();
//				} catch (OpenPageUrlException e1) {
//					message = e1.getMessage();
//				}
//				message = message + " " + e.getMessage() + " try with jsoup: "
//						+ ex.getMessage();
//				throw new PageFetchException("SeleniumPageFetcher error: "
//						+ message, e);
//			}
		}

		// page.setFetchResponseHeaders(fetchResult.getResponseHeaders());
		// page.setStatusCode(statusCode);
		try {
			String url = webDriver.getCurrentUrl();
			url = URLNormalizer.getNormalizedURL(url);
			if (!url.equals(curURL.url)) {
				Link webURL = new Link();
				webURL.url = url;
				webURL.parentDepthTable.putAll(curURL.parentDepthTable);
				webURL.depth = curURL.depth;
				webURL.domain = GetHost.getDomain(url);
				webURL.isRedirected = true;
				pageWrapper.pageHeader.setRedirectedToUrl(webURL);
				pageWrapper.pageHeader.setRedirect(true);
			} else {
				pageWrapper.pageHeader.setRedirect(false);
				String html = webDriver.getPageSource();
				// byte[] contentData = html.getBytes(Charset.forName("UTF-8"));
				pageWrapper.page.setHtmlSelen(html);
			}
			if (!isTimeout)
				pageWrapper.pageHeader.setStatusCode(HttpStatus.SC_OK);
			else
				pageWrapper.pageHeader
						.setStatusCode(HttpStatus.SC_REQUEST_TIMEOUT);
			return pageWrapper;
		} catch (Exception e) {
			throw new PageFetchException("SeleniumPageFetcher error: "
					+ e.getMessage(), e);
		}

	}

	public void closeDriver() throws OpenPageUrlException {
		try {
			webDriver.quit();
//			webDriver.close();
		} catch (Exception e) {
			throw new OpenPageUrlException("can't close driver "
					+ e.getMessage(), e);
		}
	}

	public void restart() throws OpenPageUrlException {
		try {
			if (proxy == null)
				webDriver = WebDriverFactory.createDriver(driverType);
			else
				webDriver = WebDriverFactory.createDriver(driverType, proxy);
		} catch (Exception e) {
			throw new OpenPageUrlException("can't restart driver "
					+ e.getMessage(), e);
		}
	}

}
