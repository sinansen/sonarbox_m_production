package com.datalyxt.production.akka.webscraper.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpStatus;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.extraction.PageFetchException;
import com.datalyxt.exception.extraction.PageHeaderFetchException;
import com.datalyxt.exception.extraction.PageMimeTypeHandlerException;
import com.datalyxt.exception.extraction.PageParseException;
import com.datalyxt.language.LanguageUtil;
import com.datalyxt.production.akka.webscraper.service.fetcher.Fetcher;
import com.datalyxt.production.akka.webscraper.service.fetcher.HtmlPageHeader;
import com.datalyxt.production.akka.webscraper.service.fetcher.PageWrapper;
import com.datalyxt.production.akka.webscraper.service.fetcher.SeleniumPageFetcher;
import com.datalyxt.production.akka.webscraper.service.fetcher.SimpleHTMLPageHeaderFetcher;
import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.exception.db.M_PageDBException;
import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.exception.processing.FileSizeExceededException;
import com.datalyxt.production.exception.processing.HTMLPageProcessingException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.webscraper.BlockActionException;
import com.datalyxt.production.exception.webscraper.FileActionException;
import com.datalyxt.production.exception.webscraper.InvalidHTMLPageException;
import com.datalyxt.production.exception.webscraper.InvalidRedirectException;
import com.datalyxt.production.exception.webscraper.LanguageCheckException;
import com.datalyxt.production.exception.webscraper.ProcessPageBlockConfigException;
import com.datalyxt.production.exception.webscraper.SelectorNotFoundException;
import com.datalyxt.production.exception.webscraper.SourceConfigNotFoundException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_TraceReport;
import com.datalyxt.production.execution.monitoring.model.ReportMessage;
import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.execution.monitoring.model.Supervisor;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.datalyxt.production.webscraper.model.BlockRuntime;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.production.webscraper.model.action.ActionMetaConstant;
import com.datalyxt.production.webscraper.model.action.ActionResult;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.action.FSLocation;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.paging.PagingIndex;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.util.CheckLinkTypeUtil;
import com.datalyxt.util.HtmlSourceValidationUtil;
import com.datalyxt.util.ValidLink;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.link.LinkType;

public class QT1V1Service {

	private static final Logger logger = LoggerFactory
			.getLogger(QT1V1Service.class);

	private QT1V1ConfigService qt1v1ConfigService = new QT1V1ConfigService();

	public void processPage(M_WebScraperPage inProcessPage,
			SeleniumPageFetcher seleniumPageFetcher,
			SimpleHTMLPageHeaderFetcher htmlPageHeaderFetcher,
			DAOFactory configDAOFactory, QT1V1FileService qt1v1FileService,
			QT1V1HtmlService qt1v1HtmlService, LanguageUtil languageUtil)
			throws DAOFactoryException, HTMLPageProcessingException,
			PageFetchException, M_PageBlockDBException,
			M_QT1V1ResultDBException, M_PageBlockLinkDBException,
			FileStorageException, M_SourceDBException, PageParseException,
			PageHeaderFetchException, PageMimeTypeHandlerException,
			HtmlDocumentCreationException, M_PageDBException,
			SelectorNotFoundException, InvalidRedirectException,
			LanguageCheckException, SourceConfigNotFoundException,
			InvalidHTMLPageException, BlockActionException,
			ProcessPageBlockConfigException, FileActionException,
			FileSizeExceededException, OpenPageUrlException,
			TraceReportDBException, M_DomainBlackListDBException {

		logger.debug("HTMLpageHeaderfetcher start fetching header from: "
				+ inProcessPage.pageURL.url);

		long startFetch = System.currentTimeMillis();

		HtmlPageHeader htmlPageHeader;
		htmlPageHeader = fetchHtmlPageHeader(inProcessPage.pageURL,
				htmlPageHeaderFetcher);

		inProcessPage.fetchUsedTime = System.currentTimeMillis() - startFetch;

		logger.debug("HTMLpageHeaderfetcher finish fetching header from: "
				+ inProcessPage.pageURL.url);

		if (LinkType.html.equals(htmlPageHeader.contentType)) { //
			// do
			// html
			processHtmlPage(inProcessPage, seleniumPageFetcher,
					configDAOFactory, htmlPageHeader, qt1v1HtmlService,
					languageUtil);

		} else if (LinkType.mda_PDF.equals(htmlPageHeader.contentType)) {

			// global search document page

			qt1v1FileService.processDocumentPage(inProcessPage,
					configDAOFactory, qt1v1ConfigService);

		} else if (LinkType.mda_NA.equals(htmlPageHeader.contentType)
				&& htmlPageHeader.getStatusCode() != 200) {
			processHtmlPage(inProcessPage, seleniumPageFetcher,
					configDAOFactory, htmlPageHeader, qt1v1HtmlService,
					languageUtil);
		} else {
			throw new PageMimeTypeHandlerException("can't process url: "
					+ inProcessPage.pageURL.url + "  with mimetype: "
					+ htmlPageHeader.contentType);
		}

	}

	public void processHtmlPage(M_WebScraperPage inProcessPage,
			SeleniumPageFetcher fetcher, DAOFactory daoFactory,
			HtmlPageHeader pageHeader, QT1V1HtmlService qt1v1HtmlService,
			LanguageUtil languageUtil) throws DAOFactoryException,
			PageFetchException, M_PageBlockDBException,
			M_QT1V1ResultDBException, M_PageBlockLinkDBException,
			FileStorageException, M_SourceDBException, PageParseException,
			HtmlDocumentCreationException, M_PageDBException,
			PageFetchException, SelectorNotFoundException,
			InvalidRedirectException, LanguageCheckException,
			SourceConfigNotFoundException, InvalidHTMLPageException,
			BlockActionException, ProcessPageBlockConfigException,
			TraceReportDBException, M_DomainBlackListDBException {

		logger.debug("SeleniumPageFetcher start fetching page : "
				+ inProcessPage.pageURL.url);

		long startFetch = System.currentTimeMillis();

		PageWrapper pageWrapper = fetchSeleniumPage(inProcessPage.pageURL,
				fetcher, inProcessPage.sourceConfig);

		if (pageWrapper.pageHeader.getStatusCode() == HttpStatus.SC_REQUEST_TIMEOUT) {
			M_TraceReport traceReport = new M_TraceReport(
					Supervisor.QT1V1Actor.name(),  inProcessPage.fetchFromDBTime);
			traceReport.time = System.currentTimeMillis();
			traceReport.ruleId = inProcessPage.ruleId;
			traceReport.sourceId = inProcessPage.sourceId;
			traceReport.url = inProcessPage.pageURL.url;
			traceReport.message = ReportMessage.PageFetchException.name();
			traceReport.type = ReportType.SourceVisitReport_FetchPage;
			traceReport.error = "timeout: fetch page "
					+ inProcessPage.pageURL.url
					+ " . Use current content of browser and continue to process.";
			daoFactory.getTraceReportDAO().insertTraceReport(traceReport);
		}

		inProcessPage.fetchUsedTime = System.currentTimeMillis() - startFetch;

		logger.debug("SeleniumPageFetcher finish fetching page: "
				+ inProcessPage.pageURL.url);

		if (pageWrapper.pageHeader.isRedirect()) {

			if (inProcessPage.pageHops == 0) {
				String msg = "invalid redirect from main page: "
						+ inProcessPage.pageURL.url + " to "
						+ pageWrapper.pageHeader.getRedirectedToUrl().url
						+ " . try agin.";
				throw new InvalidRedirectException(msg);
			}

			Link link = pageWrapper.pageHeader.getRedirectedToUrl();
			CheckLinkTypeUtil.checkLinkType(link, inProcessPage.sourceDomain);
			link.contextType.addAll(inProcessPage.pageURL.contextType);
			// inProcessPage.pagelinks.put(link.url, link);
			inProcessPage.followLinks.put(link.url, link);
			inProcessPage.isRedirect = true;
			logger.debug(inProcessPage.pageURL.url + " is redirect to "
					+ pageWrapper.pageHeader.getRedirectedToUrl().url);
			return;
		}

		if (inProcessPage.sourceConfig == null) {
			logger.error("no source config found for sourceId: "
					+ inProcessPage.sourceId);
			throw new SourceConfigNotFoundException(
					"no source config found for sourceId: "
							+ inProcessPage.sourceId);
		}

		LanguageISO_639_1 language = checkLanguage(pageWrapper, languageUtil);

		if (!inProcessPage.sourceConfig.qt1v1SourceConfig.languages
				.contains(language)) {
			logger.debug("page language doesn't match source config: "
					+ inProcessPage.sourceId);

			throw new LanguageCheckException(
					"check Language failed. Page language " + language.name()
							+ " doesn't match source config: "
							+ inProcessPage.sourceId);
		}
		boolean isValidPage = validatePage(pageWrapper,
				inProcessPage.sourceConfig.qt1v1SourceConfig.maxHTMLSize);

		if (!isValidPage) {
			logger.debug(inProcessPage.pageURL.url
					+ " is not valid page 403/404");
			throw new InvalidHTMLPageException("invalid html page 403/404 "
					+ inProcessPage.pageURL.url);
		}

		pageWrapper.page.dynamicParameters = inProcessPage.sourceConfig.qt1v1SourceConfig.dynamicParameters;

		// pageWrapper = parsePage(pageWrapper, parser);
		pageWrapper.page.language = language.name();
		// inProcessPage.pagelinks = pageWrapper.page.linkList;

		qt1v1HtmlService.processHtmlPageConfig(pageWrapper.page, inProcessPage,
				daoFactory, qt1v1ConfigService);
	}

	private boolean validatePage(PageWrapper pageWrapper, int maxHtmlSize) {
		if (pageWrapper.page.getHtmlSelen().length() > maxHtmlSize)
			return false;

		if (HtmlSourceValidationUtil.is404page(pageWrapper.page.getHtmlSelen()))
			return false;

		if (HtmlSourceValidationUtil.is403page(pageWrapper.page.getHtmlSelen()))
			return false;

		return true;
	}

	public List<M_WebScraperPage> createNextRoundWebScraperPages(
			M_WebScraperPage inProcessPage, QT1_V1_RuleContent ruleContent) {

		logger.info("create page for block new links: "
				+ inProcessPage.followLinks.size() + " url: "
				+ inProcessPage.pageURL.url);
		int timesOfLastFetchUsedTime = 5;
		if(inProcessPage.sourceConfig!=null)
			if(inProcessPage.sourceConfig.qt1v1SourceConfig !=null)
				timesOfLastFetchUsedTime = inProcessPage.sourceConfig.qt1v1SourceConfig.timesOfLastFetchUsedTime;
		
		List<M_WebScraperPage> pages = new ArrayList<M_WebScraperPage>();
		for (String followdLink : inProcessPage.followLinks.keySet()) {

			M_WebScraperPage npage = new M_WebScraperPage();
			Link link = inProcessPage.followLinks.get(followdLink);
			if (link != null) {
				npage.pageURL = link;
				npage.sourceId = inProcessPage.sourceId;
				npage.sourceDomain = link.domain;
				npage.status = PageContentProcessStatus.page_created;
				npage.fetchFromDBTime = inProcessPage.fetchFromDBTime;
				npage.extractedAt = System.currentTimeMillis() + timesOfLastFetchUsedTime * inProcessPage.fetchUsedTime;
				npage.ruleContent = inProcessPage.ruleContent;
				if (inProcessPage.isRedirect) {
					npage.pageHops = inProcessPage.pageHops;
					npage.redirectFrom = inProcessPage.pageURL.url;
				} else
					npage.pageHops = inProcessPage.pageHops + 1;
				npage.sourceConfig = inProcessPage.sourceConfig;
				npage.ruleId = inProcessPage.ruleId;
				if (link.isRedirected)
					npage.pageHops = inProcessPage.pageHops;
				pages.add(npage);
			}

		}
		if (inProcessPage.visitedPaing != null
				&& inProcessPage.visitedPaing.size() <= inProcessPage.pages) {
			LinkBlock paele = getNextPagingLink(inProcessPage.visitedPaing,
					inProcessPage.pageURL.url);
			if (paele != null) {

				M_WebScraperPage npage = new M_WebScraperPage();
				Link link = paele.link;
				if (link != null) {
					npage.pageURL = link;
					npage.sourceId = inProcessPage.sourceId;
					npage.sourceDomain = link.domain;
					npage.status = PageContentProcessStatus.page_created;
					npage.fetchFromDBTime = inProcessPage.fetchFromDBTime;
					npage.extractedAt = System.currentTimeMillis();
					npage.ruleContent = inProcessPage.ruleContent;
					npage.pageHops = inProcessPage.pageHops;
					npage.sourceConfig = inProcessPage.sourceConfig;
					npage.ruleId = inProcessPage.ruleId;
					npage.visitedPaing = inProcessPage.visitedPaing;
					npage.pages = inProcessPage.pages;
					if (link.isRedirected)
						npage.pageHops = inProcessPage.pageHops;
					pages.add(npage);
				}
			}
		}

		logger.info("create pages for block new links: " + pages.size()
				+ " url: " + inProcessPage.pageURL.url);

		return pages;
	}

	private LinkBlock getNextPagingLink(
			HashMap<String, PagingBlock> visitedPaging, String currentPageURL) {
		LinkBlock next = null;
		if (visitedPaging.size() == 1) {
			for (PagingBlock pagingBlock : visitedPaging.values()) {
				LinkBlock firstHref = null;
				for (int i = 0; i < (pagingBlock.indexPages.size() - 1); i++) {
					PagingIndex index = pagingBlock.indexPages.get(i);
					if (firstHref == null
							&& !visitedPaging
									.containsKey(index.pagingElement.href)
							&& ValidLink.isHttp(index.pagingElement.href)) {
						firstHref = pagingBlock.linkBlocks
								.get(index.pagingElement.href);
					}

					if (index.pagingElement.href.equals(currentPageURL)) {
						PagingIndex indexRight = pagingBlock.indexPages
								.get(i + 1);
						String href = indexRight.pagingElement.href;
						if (!visitedPaging.containsKey(href)
								&& ValidLink.isHttp(href)) {
							next = pagingBlock.linkBlocks
									.get(indexRight.pagingElement.href);
							break;
						}
					}
				}

				if (next == null && !pagingBlock.indexPages.isEmpty()) {

					next = firstHref;
				}
				return next;
			}
		}

		for (PagingBlock blocks : visitedPaging.values()) {
			for (int i = 0; i < (blocks.indexPages.size() - 1); i++) {
				PagingIndex index = blocks.indexPages.get(i);
				if (index.pagingElement.href.equals(currentPageURL)) {
					PagingIndex indexRight = blocks.indexPages.get(i + 1);
					String href = indexRight.pagingElement.href;
					if (!visitedPaging.containsKey(href)
							&& ValidLink.isHttp(href)) {
						next = blocks.linkBlocks
								.get(indexRight.pagingElement.href);
						break;
					}
				}
			}
		}
		return next;
	}

	public void saveNewPages(M_WebScraperPage inProcessPage,
			List<M_WebScraperPage> nextPages, DAOFactory daoFactory)
			throws M_WebScraperPageDBException, DAOFactoryException {
		daoFactory.getM_WebScraperPageDAO().putWebScraperPages(nextPages);

	}

	private HtmlPageHeader fetchHtmlPageHeader(Link pageURL,
			SimpleHTMLPageHeaderFetcher headerFetcher)
			throws PageHeaderFetchException {

		return headerFetcher.fetchPageHeader(pageURL);
	}

	private PageWrapper fetchSeleniumPage(Link pageURL,
			Fetcher seliniumFetcher, SourceConfig sourceConfig)
			throws PageFetchException {
		PageWrapper pageWrapper = null;
		pageWrapper = seliniumFetcher.fetchPage(sourceConfig, pageURL);
		pageWrapper.page.visitedTime = System.currentTimeMillis();

		return pageWrapper;

	}

	private LanguageISO_639_1 checkLanguage(PageWrapper pageWrapper,
			LanguageUtil languageUtil) throws LanguageCheckException {
		Document doc = Jsoup.parse(pageWrapper.page.getHtmlSelen());
		try {
			String lang = languageUtil.getLanguage(doc.text());
			return LanguageISO_639_1.valueOf(lang);
		} catch (Exception e) {
			throw new LanguageCheckException("detect Language failed. "
					+ e.getMessage(), e);
		}
	}

	public boolean isBlockExtractionRequired(M_WebScraperPage inProcessPage)
			throws SelectorNotFoundException {
		BlockConfigMain mainConfig = inProcessPage.ruleContent.blockConfigMain;
		// get config by hop
		List<PageBlockConfig> pageBlockConfigs = qt1v1ConfigService
				.getBlockConfigByHop(inProcessPage.pageHops, mainConfig);

		if (pageBlockConfigs.isEmpty())
			throw new SelectorNotFoundException("no selector defined for page "
					+ inProcessPage.pageURL.url);

		boolean isBlockExtractionRequired = qt1v1ConfigService
				.isBlockExtractionRequired(pageBlockConfigs.get(0));
		logger.debug("is BlockExtraction Required for page : "
				+ inProcessPage.pageURL.url + "  " + isBlockExtractionRequired);
		return isBlockExtractionRequired;

	}

	// boolean isBlockExtractionRequired = qt1v1ConfigService
	// .isBlockExtractionRequired(pageBlockConfigs.get(0));
	// logger.debug("is BlockExtraction Required for page : "
	// + inProcessPage.pageURL.url + "  " + isBlockExtractionRequired);
	//
	// if (!isBlockExtractionRequired) {
	// qt1v1FileService.processDocumentPage(inProcessPage, daoFactory,
	// fileStorageDAO, qt1v1ConfigService);
	// return;
	// }

	public void processPagelevelAction(QT1V1FileService qt1v1FileService,
			BlockConfigMain mainConfig, M_FileStorageDAO fileStorageDAO,
			M_WebScraperPage inProcessPage, DAOFactory daoFactory)
			throws M_QT1V1ResultDBException, DAOFactoryException,
			TraceReportDBException, FileActionException,
			FileSizeExceededException, OpenPageUrlException {

		List<PageBlockConfig> pageBlockConfigs = qt1v1ConfigService
				.getBlockConfigByHop(inProcessPage.pageHops, mainConfig);
		BlockAction action = null;
		for (PageBlockConfig pageBlockConfig : pageBlockConfigs) {
			for (BlockConfig cfg : pageBlockConfig.block) {
				for (BlockAction act : cfg.action) {
					if (act.type.equals(BlockActionType.screenshot)) {
						action = act;
						break;
					}
				}
			}
		}
		if (action != null)
			qt1v1FileService.doAction(inProcessPage, action);

	}

	public void createActionResult(M_WebScraperPage inProcessPage,
			DAOFactory daoFactory) throws M_QT1V1ResultDBException,
			DAOFactoryException {
		ActionResult actionResult = new ActionResult();

		actionResult.ruleId = inProcessPage.ruleId;
		actionResult.sourceId = inProcessPage.sourceId;
		actionResult.scanId = inProcessPage.fetchFromDBTime;
		actionResult.normalizedURL = inProcessPage.pageURL.url;
		actionResult.hop = inProcessPage.pageHops;
		actionResult.createdAt = System.currentTimeMillis();

		daoFactory.getM_QT1V1ResultDAO().saveActionResult(actionResult);

		inProcessPage.actionResult = actionResult;
	}

	public void saveActionResultTexts(M_WebScraperPage inProcessPage,
			DAOFactory daoFactory, List<ActionResultText> actionResultTexts)
			throws M_QT1V1ResultDBException, DAOFactoryException {
		for (ActionResultText result : actionResultTexts) {
			result.resultId = inProcessPage.actionResult.id;
			daoFactory.getM_QT1V1ResultDAO().saveActionResultText(result);
		}
	}

	public void saveActionResultFiles(DAOFactory daoFactory,
			M_FileStorageDAO fileStorageDAO, M_WebScraperPage inProcessPage)
			throws FileStorageException, M_QT1V1ResultDBException,
			DAOFactoryException {

		FSLocation fsLocation = fileStorageDAO.getLocation();

		for (ActionResultFile actionResultFile : inProcessPage.actionResultFiles) {

			enrichResult(fsLocation, actionResultFile, inProcessPage);

			saveFileContent(actionResultFile, daoFactory, fileStorageDAO);

			saveFileMeta(actionResultFile, daoFactory);
		}
	}

	private void enrichResult(FSLocation fsLocation, ActionResultFile result,
			M_WebScraperPage inProcessPage) {

		String suffix = ActionMetaConstant.getFileSuffix(result.type);
		result.path = new String[3];
		if (fsLocation.useSFTP)
			result.path[0] = fsLocation.sftp_Directory;
		else
			result.path[0] = fsLocation.fsPath;
		result.path[1] = "s" + inProcessPage.sourceId;
		result.path[2] = "r" + inProcessPage.ruleId;
		result.name = DigestUtils.md5Hex("" + inProcessPage.ruleId + ":"
				+ inProcessPage.fetchFromDBTime + ":"
				+ inProcessPage.pageURL.url + ":" + result.createdAt)
				+ suffix;
		result.host = fsLocation.sftp_Host;
		result.resultId = inProcessPage.actionResult.id;
		if (result.dName == null) {
			String nm = inProcessPage.sourceDomain.replace(".", "_");
			result.dName = nm + suffix;
		}

	}

	private void saveFileContent(ActionResultFile result,
			DAOFactory daoFactory, M_FileStorageDAO fileStorageDAO)
			throws FileStorageException, M_QT1V1ResultDBException,
			DAOFactoryException {

		try {

			fileStorageDAO.writeFile(result.content, result.path, result.name);

		} catch (FileStorageException e) {
			result.status = ActionMetaConstant.file_storage_error;
			daoFactory.getM_QT1V1ResultDAO().updateActionResultFile(result);
			throw new FileStorageException("save file content failed. "
					+ e.getMessage(), e);
		}

	}

	private ActionResultFile saveFileMeta(ActionResultFile result,
			DAOFactory daoFactory) throws M_QT1V1ResultDBException,
			DAOFactoryException {
		daoFactory.getM_QT1V1ResultDAO().saveActionResultFile(result);
		return result;
	}

	public void removeCreatedNewPages(List<M_WebScraperPage> createdNewPages,
			DAOFactory daoFactory) throws M_WebScraperPageDBException,
			DAOFactoryException {
		daoFactory.getM_WebScraperPageDAO().deleteWebScraperPages(
				createdNewPages);

	}

	public void removeActionResult(M_WebScraperPage inProcessPage,
			DAOFactory daoFactory) throws M_QT1V1ResultDBException,
			DAOFactoryException {

		daoFactory.getM_QT1V1ResultDAO().deleteActionResult(inProcessPage);

	}

	public void removeBlockRuntime(M_WebScraperPage inProcessPage,
			DAOFactory daoFactory) throws DAOFactoryException,
			M_PageBlockDBException {
		List<Long> ids = new ArrayList<>();
		for (BlockRuntime blockRuntime : inProcessPage.blocks) {
			ids.add(blockRuntime.block.blockId);
		}
		daoFactory.getM_PageBlockDAO().deletePageBlocks(ids);

	}

	public List<ActionResultText> saveBlocks(M_WebScraperPage inProcessPage,
			DAOFactory daoFactory) throws M_PageBlockDBException,
			DAOFactoryException, M_PageBlockLinkDBException {
		List<ActionResultText> results = new ArrayList<>();
		for (BlockRuntime blockRuntime : inProcessPage.blocks) {
			daoFactory.getM_PageBlockDAO().insertPageBlock(blockRuntime.block,
					blockRuntime.blockMeta);
			daoFactory.getM_PageBlockLinkDAO().putLinkBlocks(
					blockRuntime.newLinks, blockRuntime.block.blockId);
			for (ActionResultText r : blockRuntime.actionResultTexts) {
				r.blockId = blockRuntime.block.blockId;
				results.add(r);
			}
		}
		return results;

	}

}
