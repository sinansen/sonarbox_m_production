/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datalyxt.production.akka.webscraper.service.fetcher;

import java.io.IOException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.extraction.PageHeaderFetchException;
import com.datalyxt.production.akka.CrawlConfig;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.link.LinkType;

public class SimpleHTMLPageHeaderFetcher {

	protected static final Logger logger = LoggerFactory
			.getLogger(SimpleHTMLPageHeaderFetcher.class);

	protected RequestConfig requestConfig;
	RegistryBuilder<ConnectionSocketFactory> connRegistryBuilder;

	private CrawlConfig config;

	public SimpleHTMLPageHeaderFetcher(CrawlConfig config) {
		this.config = config;
		if (config.isProxyEnable) {
			HttpHost proxy = new HttpHost(config.proxyHost, config.proxyPort, "http");

			requestConfig = RequestConfig.custom()
					.setExpectContinueEnabled(false)
					.setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
					.setRedirectsEnabled(false)
					.setSocketTimeout(config.getSocketTimeout())
					.setConnectTimeout(config.getConnectionTimeout())
					.setProxy(proxy).build();
		} else
			requestConfig = RequestConfig.custom()
					.setExpectContinueEnabled(false)
					.setCookieSpec(CookieSpecs.BROWSER_COMPATIBILITY)
					.setRedirectsEnabled(false)
					.setSocketTimeout(config.getSocketTimeout())
					.setConnectTimeout(config.getConnectionTimeout()).build();

		connRegistryBuilder = RegistryBuilder.create();
		connRegistryBuilder.register("http",
				PlainConnectionSocketFactory.INSTANCE);
		if (config.isIncludeHttpsPages()) {
			try { // Fixing:
					// https://code.google.com/p/crawler4j/issues/detail?id=174
				// By always trusting the ssl certificate
				SSLContext sslContext = SSLContexts.custom()
						.loadTrustMaterial(null, new TrustStrategy() {
							@Override
							public boolean isTrusted(
									final X509Certificate[] chain,
									String authType) {
								return true;
							}
						}).build();
				SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
						sslContext, NoopHostnameVerifier.INSTANCE);

				connRegistryBuilder.register("https", sslsf);
			} catch (Exception e) {
				logger.warn("Exception thrown while trying to register https");
				logger.debug("Stacktrace", e);
			}
		}

	}

	public HtmlPageHeader fetchPageHeader(Link curURL)
			throws PageHeaderFetchException {

		HtmlPageHeader pageheader = new HtmlPageHeader();
		pageheader.webURL = curURL;

		String toFetchURL = curURL.url;
		CloseableHttpClient httpClient = null;
		HttpGet get = null;
		try {

			Registry<ConnectionSocketFactory> connRegistry = connRegistryBuilder
					.build();
			BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(
					connRegistry);

			HttpClientBuilder clientBuilder = HttpClientBuilder.create();
			clientBuilder.setDefaultRequestConfig(requestConfig);
			clientBuilder.setConnectionManager(connectionManager);
			clientBuilder.setUserAgent(config.getUserAgentString());

			httpClient = clientBuilder.build();

			get = null;

			get = new HttpGet(toFetchURL);

			HttpResponse response;
			response = httpClient.execute(get);

			int statusCode = response.getStatusLine().getStatusCode();
			pageheader.setStatusCode(statusCode);
			Header header = response.getFirstHeader("Content-Type");
			if (header != null) {
				String mediaType = header.getValue();
				if (mediaType.contains("application/pdf")) {
					pageheader.contentType = LinkType.mda_PDF;
				} else if (mediaType.contains("application/msword")) {
					pageheader.contentType = LinkType.mda_Word;
				} else if (mediaType.contains("application/mspowerpoint")) {
					pageheader.contentType = LinkType.mda_Powerpoint;
				} else if (mediaType.contains("application/msexcel")) {
					pageheader.contentType = LinkType.mda_Excel;
				} else if (mediaType.contains("text/html")) {
					pageheader.contentType = LinkType.html;
				} else {
					pageheader.contentType = LinkType.mda_NA;
				}
			}

		} catch (Exception e) {
			throw new PageHeaderFetchException("can't check page header "
					+ toFetchURL + "  error: " + e.getMessage(), e);
		} finally {
			if (get != null)
				get.abort();
			if (httpClient != null)
				try {
					httpClient.close();
				} catch (IOException e) {
					throw new PageHeaderFetchException(
							"can't close httpclient when checking page header "
									+ toFetchURL + "  error: " + e.getMessage(),
							e);
				}
		}

		return pageheader;

	}

}