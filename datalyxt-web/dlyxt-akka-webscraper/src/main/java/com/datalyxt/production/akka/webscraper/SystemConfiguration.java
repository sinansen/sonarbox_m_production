package com.datalyxt.production.akka.webscraper;

public interface SystemConfiguration {
	public static final String scheduler_access_db_interval_normal = "scheduler_access_db_interval_normal";
	public static final String scheduler_access_db_interval_slow = "scheduler_access_db_interval_slow";
	public static final String frontier_access_db_interval_normal = "frontier_access_db_interval_normal";
	public static final String frontier_access_db_interval_slow = "frontier_access_db_interval_slow";
	
	public static final String domain_webpage_search_interval = "domain_webpage_search_interval";
	public static final String driver_type = "driver_type";
	public static final String germantagger_path = "germantagger_path";
	public static final String cassandra_add = "cassandra_add";
	public static final String Mongo_DB_Add = "Mongo_DB_Add";
	public static final String worker_max_retry = "worker_max_retry";
	public static final String group_size = "group_size";
	public static final String worker_timeout = "worker_timeout";
	
	public static final String websearchRootRestart_max_retry = "websearchRootRestart_max_retry";
	public static final String watcher_actor_heartbeat_frequency = "watcher_actor_heartbeat_frequency";
	public static final String db_heartbeat_interval = "db_heartbeat_interval";
	
	public static final String WEBSEARCHROOT_ACTOR_NAME_STRING = "websearchRoot";
	public static final String QT1V1_ACTOR_NAME_STRING = "qt1v1actor";
	public static final String DISPATCHER_ACTOR_NAME_STRING = "webscraperpagedispatchactor";
	public static final String WEBSEARCHSCHEDULER_ACTOR_NAME_STRING = "webscraperConfigScheduler";
	public static final String WEBSEARCHWATCHER_ACTOR_NAME_STRING = "webSearchWatcher";
	public static final String WEBSEARCHPROCESSINGMONITOR_ACTOR_NAME_STRING = "websearchProcessingMonitor";
	public static final String DATABASEWATCHER_ACTOR_NAME_STRING = "DataBaseWatcher";
	public static final String MSG_Tick = "tick";
	public static final String WEBSEARCH_CHOASMONKEY_ACTOR_NAME_STRING = "chaosmonkey";
	
	public static final String WEBSEARCH_CRAWLER_ACTOR_NAME_STRING = "websearchcrawler";
	public static final String WEBSEARCH_CRAWLER_ACTION_ACTOR_NAME_STRING = "websearchcrawleraction";
	public static final String WEBSEARCH_CRAWLER_PROCESS_ACTOR_NAME_STRING = "websearchcrawlerprocess";
	public static final String WEBSEARCH_CRAWLER_SCHEDULER_ACTOR_NAME_STRING = "websearchcrawlerscheduler";
	public static final String englishtagger_path = "englishtagger_path";
	public static final String WEBSEARCH_CRAWLER_ASSESSMENT_ACTOR_NAME_STRING = "contextassessmentactor";
}
