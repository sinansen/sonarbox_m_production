package com.datalyxt.production.akka.webscraper.message;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.production.webscraper.model.M_WebScraperPage;

public class ScraperSourceConfigMessage {

	public long fetchFromDBTime;
	public List<M_WebScraperPage> initialPages = new ArrayList<>();

}
