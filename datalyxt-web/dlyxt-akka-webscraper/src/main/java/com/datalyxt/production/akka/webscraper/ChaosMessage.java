package com.datalyxt.production.akka.webscraper;

import java.io.Serializable;

public class ChaosMessage implements Serializable{
	public boolean stopWebSearchWatcher = false;
	public boolean createWebSearchWatcherException = false;
	public boolean stopWebSearchRoot = false;
	public boolean stopDatabaseWatcher = false;
	public boolean stopWebSearchScheduler = false;
	public boolean stopWebSearchQT1V1Router = false;
	public boolean stopWebSearchQT1V2Router = false;

}
