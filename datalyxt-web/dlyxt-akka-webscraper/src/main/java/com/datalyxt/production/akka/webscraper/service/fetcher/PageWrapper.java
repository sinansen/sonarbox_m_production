package com.datalyxt.production.akka.webscraper.service.fetcher;

import com.datalyxt.webscraper.model.page.Page;

public class PageWrapper {
	public HtmlPageHeader pageHeader;
	public Page page;
}
