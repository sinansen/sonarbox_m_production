package com.datalyxt.production.akka.webscraper.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceConfigMessage;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceRule;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_RuleExecutionReport;
import com.datalyxt.production.execution.monitoring.model.RuleExecutionStatus;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.webscraper.model.link.Link;

public class WebScraperConfigSchedulerService {

	public long getFetchFromDBTimeofLastResumableScan(DAOFactory daoFactory,
			long actorStartedTimestamp) throws M_RuleExecutionDBException,
			DAOFactoryException {
		List<M_RuleExecutionReport> statsList = daoFactory
				.getM_RuleExecutionDAO().getLastExecutionReports(0L);
		if (statsList.isEmpty())
			return 0;

		M_RuleExecutionReport report = statsList.get(0);
		if (report.status == RuleExecutionStatus.execution_started) {
			if (report.fetchFromDbTime > actorStartedTimestamp)
				return report.fetchFromDbTime;
		}
		return 0;
	}

	public boolean isCurrentScanFinished(DAOFactory monitorDAOFactory,
			long fetchFromDBTime) throws M_RuleExecutionDBException,
			DAOFactoryException, M_WebScraperPageDBException {

		List<M_RuleExecutionReport> statsList = monitorDAOFactory
				.getM_RuleExecutionDAO().getLastExecutionReports(
						fetchFromDBTime);

		PageContentProcessStatus statusAsVisited = PageContentProcessStatus.visited;

		if (statsList.isEmpty()) {
			return true;
		}

		if (monitorDAOFactory.getM_WebScraperPageDAO().isAllPagesVisited(
				fetchFromDBTime, statusAsVisited)) {
			long maxVisitedAt = monitorDAOFactory.getM_WebScraperPageDAO()
					.getLatestVisitedAt(PageContentProcessStatus.visited,
							fetchFromDBTime);
			if (maxVisitedAt <= 0) {
				// System.out.println(" no visited page found !!!!");
				return true;
			} else {
				long wait = 100000L;
				long vistime = maxVisitedAt;
				long cur = System.currentTimeMillis();
				long diff = cur - vistime;

				if (wait >= diff) {
					return false;
				} else
					return true;
			}

		} else
			return false;
		// else {
		// long now = System.currentTimeMillis();
		// long timediff = now - stats.fetchFromDbTime;
		// stats.executionFinishTime = now;
		// if (timediff > 5L * DateTimeConstants.MILLIS_PER_MINUTE)
		// monitorDAOFactory.getM_RuleExecutionDAO()
		// .setRuleExecutionFinished(stats);
		//
		// }
	}

	public List<M_Rule> getRules(DAOFactory websearchRulesDAOFactory)
			throws M_RuleDBException, DAOFactoryException {
		HashSet<M_RuleType> type = new HashSet<M_RuleType>();
		type.add(M_RuleType.QT1V1);
		List<M_Rule> rules = websearchRulesDAOFactory.getM_RuleDAO().getRules(
				M_RuleStatus.activated, type);

		return rules;
	}

	public List<M_RuleExecutionReport> insertRecords(List<ScraperSourceRule> scraperSourceRules,
			long fetchTimeFromDB, DAOFactory monitorDAOFactory)
			throws M_RuleExecutionDBException, DAOFactoryException {

		List<M_RuleExecutionReport> statsList = new ArrayList<M_RuleExecutionReport>();

		for (ScraperSourceRule sourceRule : scraperSourceRules) {
			M_RuleExecutionReport stats = new M_RuleExecutionReport();
			stats.ruleId = sourceRule.rule.id;
			stats.sourceId = sourceRule.source.sourceId;
			
			stats.fetchFromDbTime = fetchTimeFromDB;
			stats.executionStartTime = fetchTimeFromDB;
			stats.executionFinishTime = fetchTimeFromDB;
			stats.status = RuleExecutionStatus.execution_started;
			stats.ruleType = M_RuleType.QT1V1;
			monitorDAOFactory.getM_RuleExecutionDAO().addM_RuleExecutionReport(
					stats);
			statsList.add(stats);
		}
		return statsList;
	}

	public List<ScraperSourceRule> getSourcesForExecution(List<M_Rule> ruleIds,
			DAOFactory configDAOFactory) throws M_SourceDBException,
			DAOFactoryException {
		List<ScraperSourceRule> allsources = new ArrayList<ScraperSourceRule>();
		for (M_Rule rule : ruleIds) {
			List<M_Source> sources = configDAOFactory.getM_SourceBackendDAO()
					.getSourcesForExecutionByRuleId(rule.id);
			for (M_Source source : sources)
				allsources.add(new ScraperSourceRule(source, rule));

		}
		return allsources;
	}



	
	public ScraperSourceConfigMessage createSourceConfigMsg(
			long fetchTimeFromDB, 	List<ScraperSourceRule> srces,
			DAOFactory websearchRulesDAOFactory) throws M_SourceDBException,
			DAOFactoryException {

		ScraperSourceConfigMessage sourceConfigMessage = new ScraperSourceConfigMessage();
		sourceConfigMessage.fetchFromDBTime = fetchTimeFromDB;



		for (ScraperSourceRule sourceRule : srces) {
			M_WebScraperPage npage = new M_WebScraperPage();
			Link link = new Link();
			link.url = sourceRule.source.sourceUrlNormalized;
			link.depth = 0;
			link.domain = sourceRule.source.sourceDomain;
			npage.pageURL = link;
			npage.sourceId = sourceRule.source.sourceId;
			npage.sourceDomain = link.domain;
			npage.status = PageContentProcessStatus.page_created;
			npage.fetchFromDBTime = fetchTimeFromDB;
			npage.extractedAt = System.currentTimeMillis();
			npage.ruleContent = (QT1_V1_RuleContent) sourceRule.rule.ruleContent;
			npage.pageHops = 0;
			npage.ruleId = sourceRule.rule.id;
			npage.sourceConfig = sourceRule.source.sourceConfig;
			sourceConfigMessage.initialPages.add(npage);
		}

		return sourceConfigMessage;
	}

	public ScraperSourceConfigMessage resumeLastScan(DAOFactory daoFactory,
			long fetchTimeFromDB) throws M_SourceDBException,
			DAOFactoryException, M_RuleDBException {

		List<M_Rule> rules = getRules(daoFactory);

		// crawlerConfigSchedulerService.insertRecords(ruleIds,
		// fetchTimeFromDB, monitorDAOFactory);
		List<ScraperSourceRule> srces = getSourcesForExecution(rules,
				daoFactory);
		
		ScraperSourceConfigMessage sourcemsgs = createSourceConfigMsg(
				fetchTimeFromDB, srces, daoFactory);

		return sourcemsgs;

	}

	public void setScanAsFinished(DAOFactory monitorDAOFactory,
			long fetchTimeFromDB) throws M_RuleExecutionDBException,
			DAOFactoryException {
		if (fetchTimeFromDB > 0)
			monitorDAOFactory.getM_RuleExecutionDAO()
					.updateRuleExecutionStatus(fetchTimeFromDB,
							RuleExecutionStatus.source_scan_finished);
	}

	public boolean shouldSendConfig(long fetchTimeFromDB,
			int scheduler_send_config_interval, boolean isCurrentScanfinished) throws DataBaseException, DAOFactoryException {
		if (!isCurrentScanfinished)
			return false;

	
		long now = System.currentTimeMillis();
		long should = fetchTimeFromDB + scheduler_send_config_interval;
		if (now >= should)
			return true;
		else
			return false;

	}

}
