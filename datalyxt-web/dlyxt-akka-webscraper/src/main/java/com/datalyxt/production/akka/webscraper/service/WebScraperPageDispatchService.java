package com.datalyxt.production.akka.webscraper.service;

import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;

public class WebScraperPageDispatchService {

	public List<M_WebScraperPage> getNextAccessablePages(DAOFactory daoFactory,
			long fetchFromDBTime, int limitOfInProcessingURL,
			long maxAllowedDelay) throws M_WebScraperPageDBException,
			DAOFactoryException {
		List<M_WebScraperPage> pages = new ArrayList<>();
		List<String> inProcessingStatus = new ArrayList<>();
		inProcessingStatus.add(PageContentProcessStatus.page_process_started
				.name());
		inProcessingStatus
				.add(PageContentProcessStatus.page_submitted_to_process.name());

		List<M_WebScraperPage> inProcessingPages = daoFactory
				.getM_WebScraperPageDAO().getInProcessingPagesTime(
						fetchFromDBTime, inProcessingStatus);
		long now = System.currentTimeMillis();
		long timediff = now - maxAllowedDelay;
		int inProcessing = 0;
		for (M_WebScraperPage page : inProcessingPages) {
			
			if (page.processAt < timediff) {
				//put this page back into processing-queue
				daoFactory.getM_WebScraperPageDAO().updateWebScraperPageStatus(fetchFromDBTime, page.pageId, PageContentProcessStatus.page_created, now);
			}else{
				inProcessing = inProcessing + 1 ;
			}
		}
		if (inProcessing >= limitOfInProcessingURL)
			return pages;

		int counter = (limitOfInProcessingURL - inProcessing);

		pages = daoFactory.getM_WebScraperPageDAO().getNextPageFIFO(
				fetchFromDBTime, counter);

		return pages;

	}
}
