package com.datalyxt.production.akka.webscraper;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;

import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import akka.actor.Cancellable;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.akka.WebsearchProps;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceConfigMessage;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceRule;
import com.datalyxt.production.akka.webscraper.service.WebScraperConfigSchedulerService;
import com.datalyxt.production.backend.Command;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.execution.monitoring.model.M_TraceReport;
import com.datalyxt.production.execution.monitoring.model.ReportMessage;
import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.execution.monitoring.model.Supervisor;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;

public class WebScraperConfigSchedulerActor extends UntypedActor {

	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	protected Cancellable tickTask;

	private FiniteDuration db_scan_interval;
	private FiniteDuration db_scan_start;

	protected DAOFactory daoFactory;

	protected WebScraperConfigSchedulerService webscraperConfigSchedulerService;

	private WebsearchProps websearchProps;

	boolean slowdownDBCheck = false;

	private int checkInterval;

	private long actorStartedTimestamp;

	private long fetchTimeFromDB;

	private boolean hasResumed = false;

	private boolean hasLoadAfterStart = false;

	private final String checkBackgroundStatus = "check_bk_status";

	public WebScraperConfigSchedulerActor(WebsearchProps properties,
			DAOFactory daoFactory) {

		this.websearchProps = properties;

		db_scan_interval = Duration.create(
				properties.scheduler_access_db_interval_normal,
				TimeUnit.MILLISECONDS);

		db_scan_start = Duration.create(properties.scheduler_access_db_start,
				TimeUnit.MILLISECONDS);

		this.daoFactory = daoFactory;
		this.webscraperConfigSchedulerService = new WebScraperConfigSchedulerService();
	}

	@Override
	public void preStart() {
		actorStartedTimestamp = System.currentTimeMillis();
		fetchTimeFromDB = actorStartedTimestamp;
		tickTask = getContext()
				.system()
				.scheduler()
				.schedule(db_scan_start, db_scan_interval, getSelf(),
						checkBackgroundStatus, getContext().dispatcher(), null);
	}

	@Override
	public void onReceive(Object message) {
		log.info("WebScraperConfigSchedulerActor receive a message. ");
		if (message.equals(checkBackgroundStatus)) {
			log.info("WebScraperConfigSchedulerActor check slow down. ");
			if (slowdownDBCheck) {
				checkInterval = checkInterval
						+ websearchProps.scheduler_access_db_interval_normal;
				if (checkInterval > websearchProps.scheduler_access_db_interval_slow) {
					checkInterval = 0;
					slowdownDBCheck = false;
				}
				return;
			}
			M_TraceReport traceReport = null;
			log.info("WebScraperConfigSchedulerActor check slow down passed. continue to process message. ");
			try {

				boolean isCurrentScanFinished = false;
				if (!hasResumed && websearchProps.resumableWebScraper) {

					long lastScan = webscraperConfigSchedulerService
							.getFetchFromDBTimeofLastResumableScan(daoFactory,
									actorStartedTimestamp);
					if (lastScan != 0) {
						fetchTimeFromDB = lastScan;
						ScraperSourceConfigMessage sourcemsg = webscraperConfigSchedulerService
								.resumeLastScan(daoFactory, fetchTimeFromDB);
						publishMessage(sourcemsg);
						log.info("last web search is not finished. continue....");
					}

					hasResumed = true;
				}

				isCurrentScanFinished = webscraperConfigSchedulerService
						.isCurrentScanFinished(daoFactory, fetchTimeFromDB);

				if (isCurrentScanFinished) {

					webscraperConfigSchedulerService.setScanAsFinished(
							daoFactory, fetchTimeFromDB);

					log.info("current web scan has finished. check send config interval. ");
					boolean shouldSendConfig = webscraperConfigSchedulerService
							.shouldSendConfig(
									fetchTimeFromDB,
									websearchProps.scheduler_send_config_interval,
									isCurrentScanFinished);
					log.info("current web scan has finished.  check send config interval is "
							+ shouldSendConfig);

					if (!hasLoadAfterStart) {
						log.info("system started. send initial configs. ");
						shouldSendConfig = true;
						hasLoadAfterStart = true;
					}

					int currentComand = daoFactory.getBackendControlDAO()
							.getCurrentCommand();


					if (currentComand == Command.pause)
						shouldSendConfig = false;
					log.info("current backend control command is "
							+ currentComand+"  shouldSendConfig: "+shouldSendConfig);
					
					if (shouldSendConfig) {
						log.info("current web scan has finished, republish all web scraper configs. ");
						fetchTimeFromDB = System.currentTimeMillis();

						List<M_Rule> rules = webscraperConfigSchedulerService
								.getRules(daoFactory);
						
						List<ScraperSourceRule> srces = webscraperConfigSchedulerService.getSourcesForExecution(rules,
								daoFactory);

						webscraperConfigSchedulerService.insertRecords(srces, fetchTimeFromDB, daoFactory);

						ScraperSourceConfigMessage sourcemsg = webscraperConfigSchedulerService
								.createSourceConfigMsg(fetchTimeFromDB, srces,
										daoFactory);

						for (M_WebScraperPage page : sourcemsg.initialPages) {
							daoFactory.getM_WebScraperPageDAO()
									.putWebScraperPage(page);
						}
						log.info("initial web scraper pages habe been created in database. "
								+ sourcemsg.initialPages.size());
						publishMessage(sourcemsg);
						isCurrentScanFinished = false;
					}
				} else
					log.info("current web scan is running. fetch from db time "
							+ fetchTimeFromDB);
			} catch (M_RuleExecutionDBException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(), fetchTimeFromDB);
				traceReport.time = System.currentTimeMillis();
				traceReport.message = ReportMessage.M_RuleExecutionDBException
						.name();
				traceReport.type = ReportType.DatabaseReport;

				traceReport.error = e.getMessage();
				log.error("M_RuleExecutionDBException " + e.getMessage(), e);

				slowdown();

			} catch (DataBaseException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(), fetchTimeFromDB);
				traceReport.time = System.currentTimeMillis();
				traceReport.message = ReportMessage.DataBaseException
						.name();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = e.getMessage();
				log.error("DataBaseException " + e.getMessage(), e);

				slowdown();

			} catch (DAOFactoryException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(), fetchTimeFromDB);
				traceReport.message = ReportMessage.DAOFactoryException
						.name();
				traceReport.time = System.currentTimeMillis();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = e.getMessage();
				log.error("DAOFactoryException " + e.getMessage(), e);

				slowdown();

			} catch (M_WebScraperPageDBException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(), fetchTimeFromDB);
				traceReport.message = ReportMessage.M_WebScraperPageDBException
						.name();
				traceReport.time = System.currentTimeMillis();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = ExceptionUtils.getStackTrace(e);
				log.error("M_WebScraperPageDBException " + e.getMessage(), e);

				slowdown();

			} catch (M_RuleDBException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(), fetchTimeFromDB);
				traceReport.message = ReportMessage.M_RuleDBException
						.name();
				traceReport.time = System.currentTimeMillis();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = ExceptionUtils.getStackTrace(e);
				log.error("M_RuleDBException " + e.getMessage(), e);

				slowdown();

			} catch (M_SourceDBException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(), fetchTimeFromDB);
				traceReport.message = ReportMessage.M_SourceDBException
						.name();
				traceReport.time = System.currentTimeMillis();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = ExceptionUtils.getStackTrace(e);

				log.error("M_SourceDBException " + e.getMessage(), e);

				slowdown();

			}catch(Exception e){
				traceReport = new M_TraceReport(
						Supervisor.WebScraperConfigSchedulerActor.name(),  fetchTimeFromDB);
				traceReport.message = "Unknown Exception";
				traceReport.time = System.currentTimeMillis();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = ExceptionUtils.getStackTrace(e);

				log.error("Unknown Exception " + traceReport.error, e);


			}

			if (traceReport != null) {
				log.error("log trace report " + traceReport.error);
				try {
					daoFactory.getTraceReportDAO().insertTraceReport(
							traceReport);
				} catch (TraceReportDBException | DAOFactoryException e) {
					log.error(
							"even trace report db is unavailable. "
									+ e.getMessage(), e);
					// getContext().system().shutdown();
				}
			}

		} else {
			log.info("WebScraperConfigSchedulerActor receive a unhandled message. ");
			unhandled(message);
		}

	}

	private void slowdown() {
		slowdownDBCheck = true;

	}

	private void publishMessage(Object msg) {
		getContext().system().eventStream().publish(msg);
	}

}
