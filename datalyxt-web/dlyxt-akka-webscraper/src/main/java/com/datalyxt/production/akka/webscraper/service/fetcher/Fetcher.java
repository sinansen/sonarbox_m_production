package com.datalyxt.production.akka.webscraper.service.fetcher;

import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.extraction.PageFetchException;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.datalyxt.webscraper.model.link.Link;

public interface Fetcher {
	public PageWrapper fetchPage(SourceConfig config, Link curURL) throws PageFetchException;

	public void restart() throws OpenPageUrlException ;
}
