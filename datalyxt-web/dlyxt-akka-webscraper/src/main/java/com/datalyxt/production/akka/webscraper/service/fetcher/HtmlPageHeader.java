/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.datalyxt.production.akka.webscraper.service.fetcher;

import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.link.LinkType;

/**
 * This class contains the data for a fetched and parsed page.
 *
 * @author Yasser Ganjisaffar [lastname at gmail dot com]
 */
public class HtmlPageHeader {

	public Link webURL;

	/**
	 * Redirection flag
	 */
	protected boolean redirect;

	/**
	 * The URL to which this page will be redirected to
	 */
	protected Link redirectedToUrl;

	/**
	 * Status of the page
	 */
	protected int statusCode;

	public LinkType contentType = LinkType.mda_NA;

	public boolean isRedirect() {
		return redirect;
	}

	public void setRedirect(boolean redirect) {
		this.redirect = redirect;
	}

	public Link getRedirectedToUrl() {
		return redirectedToUrl;
	}

	public void setRedirectedToUrl(Link webURL) {
		this.redirectedToUrl = webURL;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	

//	public String getContentType() {
//
//		return contentType;
//	}
//
//	public void setContentType(String contentType) {
//		this.contentType = contentType;
//	}
}