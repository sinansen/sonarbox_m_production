package com.datalyxt.production.akka.webscraper;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;

import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.RoundRobinGroup;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.akka.WebsearchProps;
import com.datalyxt.production.akka.webscraper.message.ScraperSourceConfigMessage;
import com.datalyxt.production.akka.webscraper.service.WebScraperPageDispatchService;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_TraceReport;
import com.datalyxt.production.execution.monitoring.model.ReportMessage;
import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.execution.monitoring.model.Supervisor;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webscraper.model.M_ProcessedWebScraperPage;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;

public class WebScraperPageDispatchActor extends UntypedActor {

	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	protected Cancellable tickTask;

	private DAOFactory daoFactory;

	private WebsearchProps websearchProps;

	long currentScan;

	private boolean slowdownDBCheck = false;

	private int checkInterval;

	WebScraperPageDispatchService webScraperPageDispatchService;

	private List<String> routees;

	private ActorRef router;

	public WebScraperPageDispatchActor(DAOFactory daoFactory,
			WebsearchProps websearchProps, List<String> routees) {
		this.daoFactory = daoFactory;
		this.websearchProps = websearchProps;
		this.webScraperPageDispatchService = new WebScraperPageDispatchService();
		this.routees = routees;
	}

	@Override
	public void preStart() {

		FiniteDuration check_pending_urls_interval = Duration.create(
				websearchProps.frontier_access_db_interval_normal,
				TimeUnit.MILLISECONDS);

		tickTask = getContext()
				.system()
				.scheduler()
				.schedule(check_pending_urls_interval,
						check_pending_urls_interval, getSelf(), "tick",
						getContext().dispatcher(), null);

		router = getContext().actorOf(new RoundRobinGroup(routees).props(),
				"router");
	}

	@Override
	public void onReceive(Object message) {

		M_TraceReport traceReport = null;
//		log.info("WebScraperPageDispatchActor receive a message.");
		if (message.equals("tick")) {
			if (slowdownDBCheck) {
				checkInterval = checkInterval
						+ websearchProps.frontier_access_db_interval_normal;
				log.info("slow down db check. current check Interval : "
						+ checkInterval);
				if (checkInterval > websearchProps.frontier_access_db_interval_slow) {
					checkInterval = 0;
					slowdownDBCheck = false;
				}
				return;
			}

			try {

				List<M_WebScraperPage> next = webScraperPageDispatchService
						.getNextAccessablePages(daoFactory, currentScan,
								websearchProps.frontier_publish_url_speed,
								websearchProps.max_allowed_processing_delay);

				for (M_WebScraperPage page : next) {

					log.info("get next webscraperpage: " + page.pageURL.url
							+ " status: " + page.status.name() + " processAt "
							+ page.processAt);
					publishMessage(page);
					page.status = PageContentProcessStatus.page_submitted_to_process;
					page.processAt = System.currentTimeMillis();
					daoFactory.getM_WebScraperPageDAO()
							.updateWebScraperPageStatus(page.fetchFromDBTime,
									page.pageId, page.status, page.processAt);

				}

			} catch (M_WebScraperPageDBException e) {

				traceReport = new M_TraceReport(
						Supervisor.WebScraperPageDispatchActor.name(), currentScan);
				traceReport.time = System.currentTimeMillis();
				traceReport.message = ReportMessage.M_WebScraperPageDBException
						.name();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = ExceptionUtils.getStackTrace(e);
				log.error("M_WebScraperPageDBException " + e.getMessage(), e);
				slowdown();
			} catch (DAOFactoryException e) {
				traceReport = new M_TraceReport(
						Supervisor.WebScraperPageDispatchActor.name(),  currentScan);
				traceReport.time = System.currentTimeMillis();
				traceReport.message = ReportMessage.DAOFactoryException
						.name();
				traceReport.type = ReportType.DatabaseReport;
				traceReport.error = e.getMessage();
				log.error("DAOFactoryException " + e.getMessage(), e);
				slowdown();
			}

			if (traceReport != null) {
				log.error("log trace report : " + traceReport.error);
				try {
					daoFactory.getTraceReportDAO().insertTraceReport(
							traceReport);
				} catch (TraceReportDBException | DAOFactoryException e) {
					log.error(e.getMessage(), e);
					// getContext().system().shutdown();
				}
			}

		} else if (message instanceof ScraperSourceConfigMessage) {

			ScraperSourceConfigMessage sourcemsg = (ScraperSourceConfigMessage) message;

			log.info(" receive ScraperSourceConfigMessage - fetch from db time:  "
					+ sourcemsg.fetchFromDBTime);

			if (currentScan < sourcemsg.fetchFromDBTime)
				currentScan = sourcemsg.fetchFromDBTime;

		} else if (message instanceof M_ProcessedWebScraperPage) {
			M_ProcessedWebScraperPage page = (M_ProcessedWebScraperPage) message;
			log.info(page.pageURL.url + " used fetch time: "
					+ page.fetchUsedTime + "  visitedAt: " + page.visitedAt
					+ "  fetchFromDBTime: " + page.fetchFromDBTime);
		} else {
			unhandled(message);
		}

	}

	private void slowdown() {
		slowdownDBCheck = true;

	}

	private void publishMessage(Object msg) {
		router.tell(msg, getSelf());
		// getContext().system().eventStream().publish(msg);

	}

}
