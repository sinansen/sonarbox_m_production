package com.datalyxt.production.akka;

import com.datalyxt.webscraper.model.BrowserDriverType;

public class WebsearchProps {

	public int scheduler_access_db_interval_normal = 60000;
	public int scheduler_access_db_interval_slow = 600000;
	public int scheduler_send_config_interval = 600000;
	
	public int frontier_access_db_interval_normal = 500;
	public int frontier_access_db_interval_slow = 500;
	public int frontier_publish_url_speed = 1;
	public int qt1v1_actor_access_db_interval_normal = 3000;
	public int domain_webpage_search_interval;
	public BrowserDriverType driver_type;
	public int scheduler_access_db_start = 4000;

	public String germantagger_path;
	public String englishtagger_path;

	public int worker_max_retry;
	public int group_size;
	public int worker_timeout;

	public int websearchRootRestart_max_retry;
	public int watcher_actor_heartbeat_frequency;
	public int db_heartbeat_interval;

	public int asses_visited_pages_interval_normal;
	public int asses_visited_pages_interval_slow;

	public String lang_profiles;

	public boolean resumableCrawling;
	public boolean resumableWebScraper;

	// used for submitted and processing_started pages, if they are not
	// processed during this delay, the status of them will be set to 'created'
	public long max_allowed_processing_delay = 120000;
	
	// used by web driver
	public boolean enable_proxy = false;
	public String network_proxy_http;
	public int network_proxy_port;
	public int network_proxy_type;
	
	//amount of qt1v1 actor instances
	public int qt1v1_actor_amount = 1 ;
	public long retry_delay = 15000;
	
}
