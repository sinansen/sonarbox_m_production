package com.datalyxt.production.akka.webscraper;

import static akka.dispatch.Futures.future;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.Callable;

import org.apache.commons.lang.exception.ExceptionUtils;

import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import akka.actor.Cancellable;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.util.Timeout;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.OpenPageUrlException;
import com.datalyxt.exception.extraction.PageFetchException;
import com.datalyxt.exception.extraction.PageHeaderFetchException;
import com.datalyxt.exception.extraction.PageMimeTypeHandlerException;
import com.datalyxt.exception.extraction.PageParseException;
import com.datalyxt.exception.extraction.WebDriverUnavailableException;
import com.datalyxt.exception.html.WebDriverFactoryException;
import com.datalyxt.language.LanguageUtil;
import com.datalyxt.production.akka.CrawlConfig;
import com.datalyxt.production.akka.WebsearchProps;
import com.datalyxt.production.akka.webscraper.service.QT1V1FileService;
import com.datalyxt.production.akka.webscraper.service.QT1V1HtmlService;
import com.datalyxt.production.akka.webscraper.service.QT1V1Service;
import com.datalyxt.production.akka.webscraper.service.fetcher.SeleniumPageFetcher;
import com.datalyxt.production.akka.webscraper.service.fetcher.SimpleHTMLPageHeaderFetcher;
import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.exception.db.M_PageDBException;
import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.exception.processing.FileSizeExceededException;
import com.datalyxt.production.exception.processing.HTMLPageProcessingException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.webscraper.BlockActionException;
import com.datalyxt.production.exception.webscraper.FileActionException;
import com.datalyxt.production.exception.webscraper.InvalidHTMLPageException;
import com.datalyxt.production.exception.webscraper.InvalidRedirectException;
import com.datalyxt.production.exception.webscraper.LanguageCheckException;
import com.datalyxt.production.exception.webscraper.ProcessPageBlockConfigException;
import com.datalyxt.production.exception.webscraper.SelectorNotFoundException;
import com.datalyxt.production.exception.webscraper.SourceConfigNotFoundException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_TraceReport;
import com.datalyxt.production.execution.monitoring.model.ReportMessage;
import com.datalyxt.production.execution.monitoring.model.ReportType;
import com.datalyxt.production.execution.monitoring.model.Supervisor;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webmodel.http.HttpConfig;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webscraper.model.M_ProcessedWebScraperPage;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;

public class QT1V1Actor extends UntypedActor {

	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private SeleniumPageFetcher fetcher;

	protected Cancellable tickTask;

	private DAOFactory daoFactory;

	private QT1V1Service qt1v1service;
	private QT1V1HtmlService qt1v1Htmlservice;
	private QT1V1FileService qt1v1Fileservice;

	private SimpleHTMLPageHeaderFetcher htmlPageHeaderFetcher;

	private CrawlConfig crawlerConfig;

	private Properties proxyProperties;

	Timeout timeout;

	private M_FileStorageDAO fileStorageDAO;

	private WebsearchProps websearchProps;

	LanguageUtil languageUtil;
	HttpConfig httpConfig;

	public QT1V1Actor(CrawlConfig crawlerConfig, WebsearchProps websearchProps,
			DAOFactory daoFactory, M_FileStorageDAO fileStorageDAO)
			throws WebDriverFactoryException {
		this.daoFactory = daoFactory;
		this.crawlerConfig = crawlerConfig;
		this.proxyProperties = new Properties();
		this.fileStorageDAO = fileStorageDAO;
		this.websearchProps = websearchProps;
		httpConfig = new HttpConfig();
		if (websearchProps.enable_proxy) {
			proxyProperties.setProperty("network_proxy_http",
					websearchProps.network_proxy_http);
			proxyProperties.setProperty("network_proxy_port", ""
					+ websearchProps.network_proxy_port);
			proxyProperties.setProperty("network_proxy_type", ""
					+ websearchProps.network_proxy_type);

			this.crawlerConfig.isProxyEnable = true;
			this.crawlerConfig.proxyHost = websearchProps.network_proxy_http;
			this.crawlerConfig.proxyPort = websearchProps.network_proxy_port;

			this.fetcher = new SeleniumPageFetcher(websearchProps.driver_type,
					proxyProperties);

			httpConfig.proxyEnabled = true;
			httpConfig.proxyHost = websearchProps.network_proxy_http;
			httpConfig.proxyPort = websearchProps.network_proxy_port;
			httpConfig.connectionTimeout = crawlerConfig.getConnectionTimeout();

		} else {
			this.fetcher = new SeleniumPageFetcher(websearchProps.driver_type);

		}

		languageUtil = LanguageUtil.getInstance(websearchProps.lang_profiles);

		this.qt1v1service = new QT1V1Service();
		this.qt1v1Htmlservice = new QT1V1HtmlService();
		this.qt1v1Fileservice = new QT1V1FileService(fetcher, httpConfig);

		this.htmlPageHeaderFetcher = new SimpleHTMLPageHeaderFetcher(
				crawlerConfig);

		timeout = new Timeout(Duration.create(websearchProps.worker_timeout,
				"millis"));

	}

	//
	// @Override
	// public void preStart() {
	//
	// FiniteDuration check_pending_urls_interval =
	// Duration.create(websearchProps.qt1v1_actor_access_db_interval_normal,
	// TimeUnit.MILLISECONDS);
	//
	// tickTask = getContext()
	// .system()
	// .scheduler()
	// .schedule(check_pending_urls_interval,
	// check_pending_urls_interval, getSelf(), "tick",
	// getContext().dispatcher(), null);
	// }

	@Override
	public void onReceive(Object message) {
		// if (message.equals("tick")) {
		// if(!inProcessing)
		// M_WebScraperPage inProcessPage = configDAOFactory.get
		// }
		if (message instanceof M_WebScraperPage) {
			M_WebScraperPage page = (M_WebScraperPage) message;
			try {

				Future<Boolean> f = future(new Callable<Boolean>() {
					public Boolean call() {
						return processInProcessPage(page);
					}
				}, getContext().dispatcher());

				Boolean result = (Boolean) Await.result(f, timeout.duration());

			} catch (Exception e) {
				String msg = ExceptionUtils.getStackTrace(e);
				try {
					restartFetcher(page);
					finalizeProcessing(page, false);
				} catch (Exception e1) {
					msg = msg + ExceptionUtils.getStackTrace(e1);
				}

				M_TraceReport traceReport = new M_TraceReport(
						Supervisor.QT1V1Actor.name(), page.fetchFromDBTime);
				traceReport.time = System.currentTimeMillis();
				traceReport.message = ReportMessage.ActorProcessingLinkTimeout
						.name();
				traceReport.type = ReportType.ActorReport;
				traceReport.ruleId = page.ruleId;
				traceReport.sourceId = page.sourceId;
				traceReport.url = page.pageURL.url;
				traceReport.error = "Actor time out - " + page.pageURL.url
						+ "  " + msg;
				log.error("Actor time out - " + page.pageURL.url + "  " + msg,
						e);
				saveTraceReport(traceReport);

			}

		} else {
			log.info("actor received unhandle message: "
					+ message.getClass().getName());
			unhandled(message);
		}

	}

	private void publishMessage(Object msg) {
		getContext().system().eventStream().publish(msg);
	}

	private void saveTraceReport(M_TraceReport traceReport) {
		if (traceReport != null && traceReport.getScanId()>0)
			try {
				daoFactory.getTraceReportDAO().insertTraceReport(traceReport);
			} catch (TraceReportDBException | DAOFactoryException e) {
				log.error(
						"even trace report db is unavailable. "
								+ e.getMessage(), e);
			}
	}

	public void restartFetcher(M_WebScraperPage inProcessPage)
			throws WebDriverUnavailableException {

		try {

			fetcher.closeDriver();
		} catch (OpenPageUrlException e) {
			M_TraceReport traceReport = new M_TraceReport(
					Supervisor.QT1V1Actor.name(), inProcessPage.fetchFromDBTime);
			traceReport.time = System.currentTimeMillis();
			traceReport.message = WebDriverUnavailableException.class.getName();
			traceReport.type = ReportType.SourceVisitReport;
			traceReport.ruleId = inProcessPage.ruleId;
			traceReport.sourceId = inProcessPage.sourceId;
			traceReport.url = inProcessPage.pageURL.url;
			
			String msg = ExceptionUtils.getStackTrace(e);
			traceReport.error = msg;
			log.error(
					"WebDriverUnavailableException: close driver in fetcher. "
							+ msg, e);
			saveTraceReport(traceReport);
		}
		try {
			htmlPageHeaderFetcher = new SimpleHTMLPageHeaderFetcher(
					crawlerConfig);
			fetcher.restart();
			qt1v1Fileservice = new QT1V1FileService(fetcher, httpConfig);
		} catch (Exception e) {
			throw new WebDriverUnavailableException(
					"can't restart driver for fetcher " + e.getMessage(), e);
		}

	}

	private synchronized boolean processInProcessPage(
			M_WebScraperPage inProcessPage) {

		boolean actionOneStart = false;
		boolean actionTwoStart = false;
		boolean actionThreeStart = false;
		boolean isPageFetchFailed = false;
		try {
			actionOneStart = true;
			extractActionResult(inProcessPage); // or perhaps //
			actionTwoStart = true;
			saveActionResult(inProcessPage);
			actionThreeStart = true;
			finalizeProcessing(inProcessPage, true);
		} catch (Exception e) {
			ReportType type = processException(e, inProcessPage);
			if (type.equals(ReportType.SourceVisitReport_FetchPage))
				isPageFetchFailed = true;
		} finally {
			if (!(actionOneStart && actionTwoStart && actionThreeStart)) {

				if (actionTwoStart) {
					try {
						reverseSaveActionResults(inProcessPage);
					} catch (M_QT1V1ResultDBException | DAOFactoryException
							| M_WebScraperPageDBException
							| M_PageBlockDBException
							| M_PageBlockLinkDBException e) {
						processException(e, inProcessPage);
					}
				}
				if (actionOneStart) {
					try {
						reverseExtracActionResult(inProcessPage,
								isPageFetchFailed);
					} catch (M_WebScraperPageDBException | DAOFactoryException
							| WebDriverUnavailableException
							| M_PageBlockLinkDBException e) {
						processException(e, inProcessPage);
					}
				}

			}
		}
		return true;
	}

	private void reverseSaveActionResults(M_WebScraperPage inProcessPage)
			throws M_QT1V1ResultDBException, DAOFactoryException,
			M_WebScraperPageDBException, M_PageBlockDBException,
			M_PageBlockLinkDBException {
		log.info("reverseSaveActionResults " + inProcessPage.pageURL.url);
		qt1v1service.removeActionResult(inProcessPage, daoFactory);
		qt1v1service.removeCreatedNewPages(inProcessPage.createdNewPages,
				daoFactory);
		qt1v1service.removeBlockRuntime(inProcessPage, daoFactory);
		inProcessPage.createdNewPages.clear();
		finalizeProcessing(inProcessPage, false);

	}

	private ReportType processException(Exception e,
			M_WebScraperPage inProcessPage) {
		ReportType reportType = getReportType(e);
		
		M_TraceReport traceReport = new M_TraceReport(
				Supervisor.QT1V1Actor.name(), inProcessPage.fetchFromDBTime);
		traceReport.time = System.currentTimeMillis();
		traceReport.message = "exception: " + e.getClass().getName();
		traceReport.url = inProcessPage.pageURL.url;
		traceReport.ruleId = inProcessPage.ruleId;
		traceReport.sourceId = inProcessPage.sourceId;
		traceReport.type = reportType;
		traceReport.error = ExceptionUtils.getStackTrace(e);
		log.error(traceReport.message + " :  " + traceReport.error, e);

		try {
			daoFactory.getTraceReportDAO().insertTraceReport(traceReport);
		} catch (TraceReportDBException | DAOFactoryException traceExeception) {
			log.error(
					"even trace report db is unavailable. "
							+ ExceptionUtils.getStackTrace(traceExeception),
					traceExeception);
		}
		return reportType;
	}

	private ReportType getReportType(Exception e) {
		if (e instanceof FileStorageException || e instanceof M_PageDBException
				|| e instanceof M_SourceDBException
				|| e instanceof M_QT1V1ResultDBException
				|| e instanceof M_PageBlockDBException
				|| e instanceof DAOFactoryException
				|| e instanceof M_WebScraperPageDBException
				|| e instanceof M_PageBlockLinkDBException
				|| e instanceof M_DomainBlackListDBException)
			return ReportType.DatabaseReport;
		if (e instanceof PageFetchException
				|| e instanceof PageHeaderFetchException
				|| e instanceof OpenPageUrlException
				|| e instanceof WebDriverUnavailableException)
			return ReportType.SourceVisitReport_FetchPage;
		if (e instanceof HtmlDocumentCreationException
				|| e instanceof PageParseException
				|| e instanceof PageMimeTypeHandlerException
				|| e instanceof HTMLPageProcessingException
				|| e instanceof InvalidRedirectException
				|| e instanceof ProcessPageBlockConfigException)
			return ReportType.SourceVisitReport_processing;

		return ReportType.UnknownType;
	}

	private void reverseExtracActionResult(M_WebScraperPage inProcessPage,
			boolean isPageFetchFaild) throws M_WebScraperPageDBException,
			DAOFactoryException, WebDriverUnavailableException,
			M_PageBlockLinkDBException {

		inProcessPage.actionResult = null;
		inProcessPage.actionResultFiles.clear();
		inProcessPage.blocks.clear();
		inProcessPage.createdNewPages.clear();
		inProcessPage.pageMeta = null;


		M_TraceReport traceReport = new M_TraceReport(
				Supervisor.QT1V1Actor.name(), inProcessPage.fetchFromDBTime);
		traceReport.time = System.currentTimeMillis();
		traceReport.url = inProcessPage.pageURL.url;
		traceReport.ruleId =  inProcessPage.ruleId;
		traceReport.sourceId = inProcessPage.sourceId;
		traceReport.message = "page retry";
		traceReport.type = ReportType.SourceVisitReport;

		if (inProcessPage.retry >= (inProcessPage.sourceConfig.qt1v1SourceConfig.maxRetry - 1)) {
			finalizeProcessing(inProcessPage, false);
			String msg = "page processing maximum retry reached . "
					+ inProcessPage.pageURL.url;

			traceReport.error = msg;

		} else {
			inProcessPage.retry = inProcessPage.retry + 1;
			inProcessPage.status = PageContentProcessStatus.page_created;
			inProcessPage.extractedAt = System.currentTimeMillis()
					+ websearchProps.retry_delay;
			daoFactory.getM_WebScraperPageDAO().updateWebScraperPageContent(
					inProcessPage);
			String msg = "page processing failed. Do retry ("
					+ inProcessPage.retry + ") after "
					+ +websearchProps.retry_delay + " millies . "
					+ inProcessPage.pageURL.url;

			traceReport.error = msg;

			if (isPageFetchFaild)
				restartFetcher(inProcessPage);
		}
		log.info(traceReport.error);
		saveTraceReport(traceReport);
		log.info("reverseExtracActionResult " + inProcessPage.pageURL.url);
	}

	private boolean finalizeProcessing(M_WebScraperPage inProcessPage,
			boolean success) throws M_WebScraperPageDBException,
			DAOFactoryException, M_PageBlockLinkDBException {
		M_ProcessedWebScraperPage processed = new M_ProcessedWebScraperPage();

		inProcessPage.status = PageContentProcessStatus.visited;
		inProcessPage.processAt = System.currentTimeMillis();
		inProcessPage.actionResult = null;
		inProcessPage.blocks.clear();
		inProcessPage.actionResultFiles.clear();
		inProcessPage.createdNewPages.clear();
		inProcessPage.pageMeta = null;

		daoFactory.getM_WebScraperPageDAO().updateWebScraperPageContent(
				inProcessPage);
		daoFactory.getM_PageBlockLinkDAO().updateLinkStatus(
				inProcessPage.pageURL.url, success);

		processed.visitedAt = inProcessPage.processAt;
		processed.pageURL = inProcessPage.pageURL;
		processed.fetchUsedTime = inProcessPage.fetchUsedTime;
		processed.sourceId = inProcessPage.sourceId;
		processed.fetchFromDBTime = inProcessPage.fetchFromDBTime;
		processed.ruleId = inProcessPage.ruleId;

		publishMessage(processed);

		return true;
	}

	private boolean saveActionResult(M_WebScraperPage inProcessPage)
			throws M_QT1V1ResultDBException, DAOFactoryException,
			FileStorageException, M_WebScraperPageDBException,
			M_PageBlockDBException, M_PageBlockLinkDBException,
			M_PageDBException {

		List<ActionResultText> txts = qt1v1service.saveBlocks(inProcessPage,
				daoFactory);
		qt1v1service.createActionResult(inProcessPage, daoFactory);
		qt1v1service.saveActionResultTexts(inProcessPage, daoFactory, txts);
		qt1v1service.saveActionResultFiles(daoFactory, fileStorageDAO,
				inProcessPage);

		if (!inProcessPage.createdNewPages.isEmpty())
			qt1v1service.saveNewPages(inProcessPage,
					inProcessPage.createdNewPages, daoFactory);
		if (inProcessPage.pageMeta != null)
			daoFactory.getM_PageDAO().savePageMeta(inProcessPage.pageMeta);

		return true;
	}

	private boolean extractActionResult(M_WebScraperPage inProcessPage)
			throws BlockExtractionException, M_WebScraperPageDBException,
			DAOFactoryException, HTMLPageProcessingException,
			PageFetchException, M_PageBlockDBException,
			M_QT1V1ResultDBException, M_PageBlockLinkDBException,
			FileStorageException, M_SourceDBException, PageParseException,
			PageHeaderFetchException, PageMimeTypeHandlerException,
			HtmlDocumentCreationException, M_PageDBException,
			SelectorNotFoundException, InvalidRedirectException,
			LanguageCheckException, SourceConfigNotFoundException,
			InvalidHTMLPageException, BlockActionException,
			ProcessPageBlockConfigException, FileActionException,
			FileSizeExceededException, OpenPageUrlException,
			TraceReportDBException, M_DomainBlackListDBException {

		log.info(getSelf().path().toString() + " start process page : "
				+ inProcessPage.pageURL.url);

		inProcessPage.status = PageContentProcessStatus.page_process_started;

		QT1_V1_RuleContent ruleContent = inProcessPage.ruleContent;

		if (ruleContent == null) {
			log.error(" no qt1_v1 rule found for page : "
					+ inProcessPage.pageURL.url);
			throw new BlockExtractionException(
					" no qt1_v1 rule found for page : "
							+ inProcessPage.pageURL.url);

		}

		inProcessPage.processAt = System.currentTimeMillis();
		daoFactory.getM_WebScraperPageDAO().updateWebScraperPageStatus(
				inProcessPage.fetchFromDBTime, inProcessPage.pageId,
				inProcessPage.status, inProcessPage.processAt);
		inProcessPage.processstarttime = System.currentTimeMillis();

		boolean isBlockExtractionRequiered = qt1v1service
				.isBlockExtractionRequired(inProcessPage);
		BlockConfigMain mainConfig = inProcessPage.ruleContent.blockConfigMain;

		qt1v1service.processPagelevelAction(qt1v1Fileservice, mainConfig,
				fileStorageDAO, inProcessPage, daoFactory);
		if (isBlockExtractionRequiered)
			qt1v1service.processPage(inProcessPage, fetcher,
					htmlPageHeaderFetcher, daoFactory, qt1v1Fileservice,
					qt1v1Htmlservice, languageUtil);

		inProcessPage.createdNewPages = qt1v1service
				.createNextRoundWebScraperPages(inProcessPage, ruleContent);

		log.info("process finished -- page : " + inProcessPage.pageURL.url);

		return true;
	}

}
