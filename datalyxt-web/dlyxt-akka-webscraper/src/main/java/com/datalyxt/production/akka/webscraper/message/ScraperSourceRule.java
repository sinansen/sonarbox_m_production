package com.datalyxt.production.akka.webscraper.message;

import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.source.M_Source;

public class ScraperSourceRule {

	public M_Source source;
	public M_Rule rule;
	
	public ScraperSourceRule(M_Source source, M_Rule rule) {
		this.source = source;
		this.rule = rule;
	}



}
