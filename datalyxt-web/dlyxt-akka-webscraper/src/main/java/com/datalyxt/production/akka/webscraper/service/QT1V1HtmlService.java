package com.datalyxt.production.akka.webscraper.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.bouncycastle.util.Strings;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.extraction.BlockExtractionException;
import com.datalyxt.exception.extraction.DefinedBlockNotFound;
import com.datalyxt.exception.extraction.PageFetchException;
import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.exception.db.M_PageDBException;
import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.exception.processing.HtmlDocumentCreationException;
import com.datalyxt.production.exception.rule.NullBlockLocationException;
import com.datalyxt.production.exception.webscraper.BlockActionException;
import com.datalyxt.production.exception.webscraper.ColumnTemplateException;
import com.datalyxt.production.exception.webscraper.FilterFailedException;
import com.datalyxt.production.exception.webscraper.PagingExtractionException;
import com.datalyxt.production.exception.webscraper.ProcessPageBlockConfigException;
import com.datalyxt.production.exception.webscraper.SelectorNotFoundException;
import com.datalyxt.production.exception.webscraper.TimeBlockException;
import com.datalyxt.production.exception.webscraper.UnknownBlockTypeException;
import com.datalyxt.production.exception.webscraper.UnsupportedBlockLocationException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.RuntimeDAOFactory;
import com.datalyxt.production.webdatabase.factory.SqliteDAOFactory;
import com.datalyxt.production.webmodel.source.Blacklist;
import com.datalyxt.production.webscraper.model.BlockRuntime;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.production.webscraper.model.action.ActionResultText;
import com.datalyxt.production.webscraper.model.action.FollowLinkActionResult;
import com.datalyxt.production.webscraper.model.config.BlockAction;
import com.datalyxt.production.webscraper.model.config.BlockActionType;
import com.datalyxt.production.webscraper.model.config.BlockConfig;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;
import com.datalyxt.production.webscraper.model.config.BlockLocation;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.config.Filter;
import com.datalyxt.production.webscraper.model.config.PageBlockConfig;
import com.datalyxt.production.webscraper.model.marker.CleanHtmlDocumentScript;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.BlockMeta;
import com.datalyxt.production.webscraper.model.runtime.DatasetBlock;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.production.webscraper.model.runtime.PageMeta;
import com.datalyxt.production.webscraper.model.runtime.PagingBlock;
import com.datalyxt.production.webscraper.service.QT1V1ConfigService;
import com.datalyxt.production.webscraper.service.action.BlockActionContext;
import com.datalyxt.production.webscraper.service.action.CleanTextActionStrategy;
import com.datalyxt.production.webscraper.service.action.FollowLinkActionStrategy;
import com.datalyxt.production.webscraper.service.action.FormattedTextActionStrategy;
import com.datalyxt.production.webscraper.service.action.JsonStorageActionStrategy;
import com.datalyxt.production.webscraper.service.action.LinkStorageActionStrategy;
import com.datalyxt.production.webscraper.service.block.BlockFactory;
import com.datalyxt.production.webscraper.service.block.content.BlockContentService;
import com.datalyxt.production.webscraper.service.block.content.BlockVisitService;
import com.datalyxt.production.webscraper.service.block.content.multirow.RowBasedSelectorTemplate;
import com.datalyxt.production.webscraper.service.condition.BlockContentSeen;
import com.datalyxt.production.webscraper.service.condition.ConditionService;
import com.datalyxt.production.webscraper.service.condition.DatasetTextService;
import com.datalyxt.production.webscraper.service.condition.LinkSeen;
import com.datalyxt.production.webscraper.service.filter.BlockFilter;
import com.datalyxt.production.webscraper.service.filter.LinkBlockFilter;
import com.datalyxt.production.webscraper.service.location.ContentBlockVisitContext;
import com.datalyxt.production.webscraper.service.location.CssSelectorLocationVisitStrategy;
import com.datalyxt.production.webscraper.service.parser.PageCssSelectorParser;
import com.datalyxt.util.HtmlDocumentUtil;
import com.datalyxt.util.JenkinsHash;
import com.datalyxt.webscraper.model.page.Page;

public class QT1V1HtmlService {

	private static final Logger logger = LoggerFactory
			.getLogger(QT1V1HtmlService.class);

	ContentBlockVisitContext cssCtx;
	BlockActionContext blockActionContext;

	public QT1V1HtmlService() {
		cssCtx = new ContentBlockVisitContext();
		cssCtx.setContentVisitStrategy(new CssSelectorLocationVisitStrategy());

		blockActionContext = new BlockActionContext();

		blockActionContext.addBlockActionStrategy(BlockActionType.cleanText,
				new CleanTextActionStrategy());
		blockActionContext.addBlockActionStrategy(
				BlockActionType.formattedText,
				new FormattedTextActionStrategy());
		blockActionContext.addBlockActionStrategy(BlockActionType.linkStorage,
				new LinkStorageActionStrategy());

		blockActionContext.addBlockActionStrategy(BlockActionType.structure,
				new JsonStorageActionStrategy());

		blockActionContext.addBlockActionStrategy(BlockActionType.followLinks,
				new FollowLinkActionStrategy());

	}

//	private ActionResult createActionResult(M_WebScraperPage inProcessPage) throws M_QT1V1ResultDBException {
//		ActionResult actionResult = new ActionResult();
//
//		actionResult.ruleId = inProcessPage.ruleId;
//		actionResult.sourceId = inProcessPage.sourceId;
//		actionResult.scanId = inProcessPage.fetchFromDBTime;
//		actionResult.normalizedURL = inProcessPage.pageURL.url;
//		actionResult.hop = inProcessPage.pageHops;
//		actionResult.createdAt = System.currentTimeMillis();
//
//		return actionResult;
//	}

	private PageMeta createPageMeta(Page page, long fetchFromDBTime)
			throws HtmlDocumentCreationException {
		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();
		script.enrichTextNode = false;
		Document doc = HtmlDocumentUtil.getDocumentFromSource(page.pageUrl.url,
				page.getHtmlSelen(), script);
		String domtree = "";
		Elements elements = doc.body().getAllElements();
		for (Element el : elements) {
			domtree += el.tagName();
		}
		JenkinsHash jenkinsHash = new JenkinsHash();
		byte[] content = Strings.toUTF8ByteArray(domtree);
		PageMeta pageMeta = new PageMeta();
		pageMeta.cleanedDomHash = jenkinsHash.hash(content);
		pageMeta.domTree = domtree;
		pageMeta.nUrlMd5 = DigestUtils.md5Hex(page.pageUrl.url);
		pageMeta.html = page.getHtmlSelen();
		pageMeta.scanId = fetchFromDBTime;
		pageMeta.nUrl = page.pageUrl.url;
		return pageMeta;
	}

	private boolean detectPageChange(PageMeta pageMeta, DAOFactory daoFactory)
			throws HtmlDocumentCreationException, M_PageDBException,
			DAOFactoryException {

		boolean isExsit = daoFactory.getM_PageDAO().isExsit(pageMeta);

		return isExsit;

	}

	private void saveErrorPageChange(PageMeta pageMeta, DAOFactory daoFactory)
			throws M_PageDBException, DAOFactoryException {

		pageMeta.timestamp = System.currentTimeMillis();
		daoFactory.getM_PageDAO().saveErrorPageMeta(pageMeta);
	}

	private void savePageChange(PageMeta pageMeta, M_WebScraperPage inProcessPage)
			throws M_PageDBException, DAOFactoryException {

		pageMeta.timestamp = System.currentTimeMillis();
		inProcessPage.pageMeta = pageMeta;
//		daoFactory.getM_PageDAO().savePageMeta(pageMeta);
//		PageChange pageChange = new PageChange();
//		pageChange.confirmed = 0;
//		pageChange.message = "page changed";
//		pageChange.nUrlMd5 = pageMeta.nUrlMd5;
//		pageChange.timestamp = System.currentTimeMillis();
//		daoFactory.getM_PageDAO().savePageChange(pageChange);

	}
	
	public void processHtmlPageConfig(Page page,
			M_WebScraperPage inProcessPage, DAOFactory daoFactory,
			QT1V1ConfigService qt1v1ConfigService) throws DAOFactoryException,
			M_PageBlockLinkDBException, M_PageBlockDBException,
			M_QT1V1ResultDBException, TraceReportDBException,
			M_SourceDBException, HtmlDocumentCreationException,
			M_PageDBException, PageFetchException, FileStorageException, SelectorNotFoundException, BlockActionException, ProcessPageBlockConfigException, M_DomainBlackListDBException {
		
		PageMeta pageMeta = createPageMeta(page, inProcessPage.fetchFromDBTime);
		boolean nochange = detectPageChange(pageMeta, daoFactory);
		
		boolean isRetry = inProcessPage.fetchPageRetry > 0;
		boolean isStartHop = inProcessPage.pageHops == 0;
		logger.info("is page retry. " + isRetry+" . isStartHop "+isStartHop+"  "+page.pageUrl.url);
		if (nochange && !isRetry && !isStartHop) {
			logger.info("page does not changed. " + page.pageUrl.url);
			return;
		}
		BlockConfigMain mainConfig = inProcessPage.ruleContent.blockConfigMain;

		// get config by hop
		List<PageBlockConfig> pageBlockConfigs = qt1v1ConfigService
				.getBlockConfigByHop(inProcessPage.pageHops, mainConfig);

		if (pageBlockConfigs.isEmpty()){
			logger.info("no page block config found for page: "
					+ inProcessPage.pageURL.url + "  hop: "
					+ inProcessPage.pageHops + " .  please check.");
			saveErrorPageChange(pageMeta , daoFactory);
			throw new SelectorNotFoundException("no selector defined for page "
					+ inProcessPage.pageURL.url);
		}

		logger.info("process current HtmlPageConfig- hop " + inProcessPage.pageHops
				+ "  url: " + inProcessPage.pageURL.url);

		//
		CleanHtmlDocumentScript scriptWithID = new CleanHtmlDocumentScript();
		scriptWithID.removeElementCssClassAttr = false;
		scriptWithID.removeElementIdAttr = false;
		
		List<PageBlockConfig> pageBlockCfgs = qt1v1ConfigService
				.selectValidPageBlockConfig(page.pageUrl.url,
						page.getHtmlSelen(), pageBlockConfigs, scriptWithID, true);

		if (pageBlockCfgs.isEmpty()) {
			String msg = "in QT1V1HtmlService: no valid page block config found for page: "
					+ inProcessPage.pageURL.url + "  hop: "
					+ inProcessPage.pageHops ;
			
			if (inProcessPage.isRedirect) {
				msg = msg + ". This page is edirected from: "
						+ inProcessPage.redirectFrom;
			}
			
			logger.info(msg);
			
			saveErrorPageChange(pageMeta , daoFactory);
	
			throw new SelectorNotFoundException(msg);
		}

		BlockFactory blockFactory = new BlockFactory();

		LinkSeen linkSeen = new LinkSeen();
		BlockContentSeen contentSeen = new BlockContentSeen();

		LinkBlockFilter dataBlockFilter = new LinkBlockFilter();

		// get block by hop
		logger.info("processHtmlPageConfig- processPageBlockConfig "
				+ inProcessPage.pageHops + "  url: "
				+ inProcessPage.pageURL.url);

		CleanHtmlDocumentScript script = new CleanHtmlDocumentScript();

		FollowableContent followableContent = processPageBlockConfig(page,
				pageBlockCfgs, daoFactory, qt1v1ConfigService, blockFactory,
				linkSeen, contentSeen, dataBlockFilter,
				inProcessPage, script);

		logger.info("processHtmlPageConfig- processPageBlockConfig finished"
				+ inProcessPage.pageHops + "  url: "
				+ inProcessPage.pageURL.url);

		logger.info("found new links " + followableContent.newLinks.size());

		for (LinkBlock linkBlock : followableContent.newLinks.values()) {
			inProcessPage.followLinks.put(linkBlock.link.url, linkBlock.link);
		}

		if (followableContent.pagingBlock != null) {
			inProcessPage.visitedPaing.put(inProcessPage.pageURL.url,
					followableContent.pagingBlock);
			inProcessPage.pages = followableContent.pagingBlock.pages;

		}

		savePageChange(pageMeta, inProcessPage);
	}

	class FollowableContent {
		HashMap<String, LinkBlock> newLinks = new HashMap<>();
		PagingBlock pagingBlock;
	}

	private FollowableContent processPageBlockConfig(Page page,
			List<PageBlockConfig> pageBlockConfigs, DAOFactory daoFactory,
			QT1V1ConfigService qt1v1ConfigService, BlockFactory blockFactory,
			LinkSeen linkSeen, BlockContentSeen contentSeen,
			LinkBlockFilter dataBlockFilter,
			M_WebScraperPage inProcessPage, CleanHtmlDocumentScript script)
			throws DAOFactoryException, M_PageBlockLinkDBException,
			M_PageBlockDBException, M_QT1V1ResultDBException,
			TraceReportDBException, M_SourceDBException, FileStorageException, BlockActionException, ProcessPageBlockConfigException, HtmlDocumentCreationException, M_PageDBException, M_DomainBlackListDBException {
		FollowableContent followableContent = new FollowableContent();
		try {

			BlockVisitService blockVisitService = new BlockVisitService(
					blockFactory, cssCtx);
			PageCssSelectorParser pageCssSelectorParser = new PageCssSelectorParser();
			ConditionService conditionService = new ConditionService(linkSeen,
					contentSeen);
			BlockContentService blockContentService = new BlockContentService();

			String urlHashcode = DigestUtils.md5Hex(inProcessPage.pageURL.url);

			int tmpBlockId = 1;

			RuntimeDAOFactory sqliteDaoFactory = SqliteDAOFactory.getInstance();
			sqliteDaoFactory.getM_BlockFilterDAO().createDB();

			BlockFilter blockFilter = new BlockFilter(sqliteDaoFactory,
					dataBlockFilter);

			// BlockLocationNormalizer blockLocationNormalizer = new
			// BlockLocationNormalizer();
			DatasetTextService datasetHash = new DatasetTextService();

			for (PageBlockConfig pageBlockConfig : pageBlockConfigs) {
				pageBlockConfig.url = inProcessPage.pageURL.url;
				HashMap<String, LinkBlock> followedLinkBlocks = new HashMap<String, LinkBlock>();

				// pageBlockConfig = blockLocationNormalizer.normalize(
				// pageBlockConfig, page, script);

				for (BlockConfig blockConfig : pageBlockConfig.block) {

					if (blockConfig.type.equals(BlockType.datasetBlock)) {
						if (blockConfig.multiRow) {

							Filter filter = blockConfig.filter;

							List<RowBasedSelectorTemplate> templates = blockVisitService
									.getTemplates(blockConfig);

							HashSet<String> pselectors = blockVisitService
									.getTemplatesParentSelectors(templates);

							Collection<BlockLocation> tobeCheckedCssSelector = pageCssSelectorParser
									.extractChildrenEleSelector(page,
											pselectors.iterator(), script);

							List<DatasetBlock> datasetBlocks = blockVisitService
									.visitMultiRow(blockConfig, page, script,
											tobeCheckedCssSelector, templates);

							int datasetTmpBlockId = 1;
							logger.info("before filter- datasetblocks contains "
									+ datasetBlocks.size() + " rows");
							if (filter != null) {
								datasetBlocks = blockFilter.doDatasetFilter(
										filter, datasetBlocks, urlHashcode);
								logger.info("after filter- datasetblocks contains "
										+ datasetBlocks.size() + " rows");
							}
							for (DatasetBlock datasetBlock : datasetBlocks) {

								datasetBlock.blockId = datasetTmpBlockId++;
								String dtext = datasetHash
										.getDatasetText(datasetBlock);

								datasetBlock.blockTextHash = dtext.hashCode();
								logger.info("dataset text:   " + dtext+"  hashcode: "+datasetBlock.blockTextHash);
								
								datasetBlock.blockText = dtext;
								HashMap<String, LinkBlock> followed = processBlock(
										urlHashcode, blockContentService,
										datasetBlock, filter, blockFilter,
										conditionService, blockConfig,
										daoFactory,
										inProcessPage);
								followedLinkBlocks.putAll(followed);

							}
						} else {

							Filter filter = blockConfig.filter;

							DatasetBlock block = (DatasetBlock) blockVisitService
									.visit(blockConfig, page, script);
							int datasetTmpBlockId = 1;
							List<DatasetBlock> datasetBlocks = new ArrayList<>();
							datasetBlocks.add(block);

							logger.info("before filter- datasetblocks contains "
									+ datasetBlocks.size() + " rows");
							if (filter != null) {
								datasetBlocks = blockFilter.doDatasetFilter(
										filter, datasetBlocks, urlHashcode);
								logger.info("after filter- datasetblocks contains "
										+ datasetBlocks.size() + " rows");
							}
							for (DatasetBlock datasetBlock : datasetBlocks) {

								datasetBlock.blockId = datasetTmpBlockId++;
								String dtext = datasetHash
										.getDatasetText(datasetBlock);
								logger.info("dataset text:   " + dtext);
								datasetBlock.blockTextHash = dtext.hashCode();
								datasetBlock.blockText = dtext;
								HashMap<String, LinkBlock> followed = processBlock(
										urlHashcode, blockContentService,
										datasetBlock, filter, blockFilter,
										conditionService, blockConfig,
										daoFactory,
										inProcessPage);
								followedLinkBlocks.putAll(followed);

							}
						}
					} else {

						Block block = blockVisitService.visit(blockConfig,
								page, script);

						block.blockId = tmpBlockId++;

						Filter filter = blockConfig.filter;

						HashMap<String, LinkBlock> followed = processBlock(
								urlHashcode, blockContentService, block,
								filter, blockFilter, conditionService,
								blockConfig, daoFactory,
								inProcessPage);

						followedLinkBlocks.putAll(followed);
						if (block.type.equals(BlockType.pagingBlock)) {
							followableContent.pagingBlock = (PagingBlock) block;
							followableContent.pagingBlock.pages = blockConfig.pages;
						}
					}
				}

				sqliteDaoFactory.getM_BlockFilterDAO().clearDB(urlHashcode);
				sqliteDaoFactory.closeConnection();

				followableContent.newLinks.putAll(followedLinkBlocks);
			}
		} catch (HtmlDocumentCreationException | FilterFailedException
				| UnknownBlockTypeException | NullBlockLocationException
				| DefinedBlockNotFound | BlockExtractionException
				| TimeBlockException | PagingExtractionException
				| UnsupportedBlockLocationException | ColumnTemplateException e) {

			String error = "page: " + inProcessPage.pageURL.url + " "
					+ ExceptionUtils.getStackTrace(e);
			
			PageMeta pageMeta = createPageMeta(page, inProcessPage.fetchFromDBTime);
			saveErrorPageChange(pageMeta , daoFactory);
			logger.error(error, e);
			
			throw new ProcessPageBlockConfigException(error, e);

		}
		return followableContent;

	}


	private HashSet<String> getBlackList(DAOFactory daoFactory)
			throws DAOFactoryException, M_DomainBlackListDBException {
		List<Blacklist> blacklistSources = daoFactory.getDomainBlackListDAO().getDomainBlackList();
		HashSet<String> blacklist = new HashSet<>();
		for (Blacklist s : blacklistSources)
			blacklist.add(s.url);
		return blacklist;

	}

	private HashMap<String, LinkBlock> processBlock(String urlHashcode,
			BlockContentService blockContentService, Block block,
			Filter filter, BlockFilter blockFilter,
			ConditionService conditionService, BlockConfig blockConfig,
			DAOFactory daoFactory,
			M_WebScraperPage inProcessPage) throws M_SourceDBException,
			DAOFactoryException, FilterFailedException,
			M_PageBlockLinkDBException, M_PageBlockDBException,
			M_QT1V1ResultDBException, TraceReportDBException, BlockActionException, M_DomainBlackListDBException {

		logger.info("processHtmlPageConfig- processBlock: location "
				+ blockConfig.location + "  url: " + inProcessPage.pageURL.url);

		HashMap<String, LinkBlock> followedLinkBlocks = new HashMap<String, LinkBlock>();
		HashMap<String, LinkBlock> linkBlocks = blockContentService
				.getLinkBlocks(block);

		HashSet<String> blacklist = getBlackList(daoFactory);

		if (filter != null) {
			linkBlocks = blockFilter.doBlockLinkFilter(filter, linkBlocks,
					blacklist);
			Block b = blockContentService.setLinkBlocks(block, linkBlocks);
			if(b == null){
				logger.info("after doBlockLinkFilter block is emmpty "+ inProcessPage.pageURL.url);
				return followedLinkBlocks;
			}else
				block = b;
		}

		boolean isConditionFullfilled = conditionService.isConditionFullfilled(
				blockConfig.condition, inProcessPage.pageURL.url, block,
				linkBlocks, daoFactory);

		logger.info("isConditionFullfilled : " + blockConfig.condition + " - "
				+ isConditionFullfilled);
		if (isConditionFullfilled) {
			BlockMeta blockMeta = new BlockMeta();
			blockMeta.pageUrl = inProcessPage.pageURL.url;
			blockMeta.ruleId = inProcessPage.ruleId;
			blockMeta.sourceId = inProcessPage.sourceId;


			BlockRuntime blockRuntime = new BlockRuntime();
			blockRuntime.blockMeta = blockMeta;
			blockRuntime.block = block;
			blockRuntime.newLinks = conditionService.getNewLinks(
			linkBlocks, daoFactory);

			for (BlockAction action : blockConfig.action) {

				// store datablock html

					if (!BlockActionType.fileStorage.equals(action.type)) {

						if (!BlockActionType.screenshot.equals(action.type)) {

							ActionResultText result = blockActionContext
									.doAction(block, action);
							if (result != null) {
								if (result instanceof FollowLinkActionResult) {
									followedLinkBlocks
											.putAll(((FollowLinkActionResult) result).linkBlocks);
								}
								blockRuntime.actionResultTexts.add(result);
								
//								if (inProcessPage.actionResult == null) {
//									inProcessPage.actionResult = createActionResult(
//											inProcessPage);
//
//								}
						
							}
						}

					}

			}
			inProcessPage.blocks.add(blockRuntime);
		}
		logger.info("finished processHtmlPageConfig- processBlock: location "
				+ blockConfig.location + "  url: " + inProcessPage.pageURL.url);

		return followedLinkBlocks;
	}


}
