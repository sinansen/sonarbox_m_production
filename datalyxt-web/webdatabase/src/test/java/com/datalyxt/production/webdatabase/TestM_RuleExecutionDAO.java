package com.datalyxt.production.webdatabase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.execution.monitoring.model.M_RuleExecutionReport;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;

public class TestM_RuleExecutionDAO {
	@Test
	public void testSetExectionFinished() throws IOException, M_RuleDBException, DAOFactoryException, M_RuleExecutionDBException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_RuleExecutionDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		M_RuleExecutionReport report = new M_RuleExecutionReport();
		report.ruleId = 19L;
		report.executionStartTime = System.currentTimeMillis();
		report.fetchFromDbTime = 0L;
		
		daoFactory.getM_RuleExecutionDAO().setRuleExecutionStart(report);
	}
	
	@Test
	public void testInsertRecord() throws IOException, M_RuleDBException, DAOFactoryException, M_RuleExecutionDBException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_RuleExecutionDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		M_RuleExecutionReport report = new M_RuleExecutionReport();
		report.ruleId = 19L;
		report.executionStartTime = System.currentTimeMillis();
		report.fetchFromDbTime = System.currentTimeMillis();
		report.executionFinishTime = System.currentTimeMillis();
		
		daoFactory.getM_RuleExecutionDAO().addM_RuleExecutionReport(report);
	}
	
}
