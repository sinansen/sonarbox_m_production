package com.datalyxt.production.webdatabase;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.source.M_Source;

public class TestM_SourceDAO {
	@Test
	public void testGetSourcesForExecutionByRuleId() throws IOException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_SourceDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		try {
			List<M_Source> sources = daoFactory.getM_SourceBackendDAO().getSourcesForExecutionByRuleId(19);
			assertTrue(sources.size() == 5);
		} catch (M_SourceDBException | DAOFactoryException e) {
			e.printStackTrace();
		}
	}
}
