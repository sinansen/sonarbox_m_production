package com.datalyxt.production.webdatabase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.rule.M_RuleType;

public class TestDBConnection {
	@Test
	public void testAutoConnection() throws IOException, SQLException {
		Properties properties = new Properties();
		properties.load(TestDBConnection.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		try {
			List<String> status = new ArrayList<String>();
			status.add(PageContentProcessStatus.page_process_started.name());
			status.add(PageContentProcessStatus.page_submitted_to_process.name());
			long fetchFromDBTime = System.currentTimeMillis();
			for (int i = 0; i < 100; i++) {
				System.out.println("access db "+i);
				try {
					daoFactory.getM_WebScraperPageDAO().getInProcessingPagesTime(
							fetchFromDBTime, status);
				} catch (M_WebScraperPageDBException | DAOFactoryException e) {
					System.out.println("access db failed "+i);
					e.printStackTrace();
				}
				System.out.println("access db successful "+i);
				Thread.sleep(5000);
			}
		} catch ( InterruptedException e) {
		
			e.printStackTrace();
		}
	}
	
	//daoFactory
	
	
	@Test
	public void testM_RuleExecutionDAOAutoConnection() throws IOException, SQLException {
		Properties properties = new Properties();
		properties.load(TestDBConnection.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		try {
			long fetchFromDBTime = System.currentTimeMillis();
			for (int i = 0; i < 100; i++) {
				System.out.println("access db "+i);
				try {
					daoFactory.getM_RuleExecutionDAO().getLastExecutionReports(0L);
				} catch (DAOFactoryException | M_RuleExecutionDBException e) {
					System.out.println("access db failed "+i);
					System.out.println(e.getMessage());
				}
				System.out.println("access db successful "+i);
				Thread.sleep(5000);
			}
		} catch ( InterruptedException e) {
		
			e.printStackTrace();
		}
	}
}
