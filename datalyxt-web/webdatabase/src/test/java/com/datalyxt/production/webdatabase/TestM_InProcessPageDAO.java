package com.datalyxt.production.webdatabase;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.InProcessPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_InProcessPage;
import com.datalyxt.production.globalsearch.context.DocumentContext;
import com.datalyxt.production.globalsearch.context.GlobalSearchContext;
import com.datalyxt.production.globalsearch.context.HTMLContext;
import com.datalyxt.production.globalsearch.page.DocumentPage;
import com.datalyxt.production.globalsearch.page.GlobalSearchPage;
import com.datalyxt.production.globalsearch.page.HTMLPage;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.webscraper.model.util.RuntimeTypeAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class TestM_InProcessPageDAO {
	@Test
	public void testIsAllVisisted() throws IOException, DAOFactoryException,
			InProcessPageDBException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_InProcessPageDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		HashSet<PageContentProcessStatus> statusAsVisited = new HashSet<PageContentProcessStatus>();
		statusAsVisited.add(PageContentProcessStatus.visited);
		statusAsVisited.add(PageContentProcessStatus.start_assess_cc);

		long fetchTimeFromDB = 1452235859213L;
		boolean result = daoFactory.getInProcessPagesDAO().isAllPagesVisited(
				fetchTimeFromDB, statusAsVisited);
		System.out.println(result);
	}

	@Test
	public void testGetLastVisited() throws IOException,
			InProcessPageDBException, DAOFactoryException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_InProcessPageDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		HashSet<PageContentProcessStatus> statusAsVisited = new HashSet<PageContentProcessStatus>();
		statusAsVisited.add(PageContentProcessStatus.visited);
		statusAsVisited.add(PageContentProcessStatus.start_assess_cc);

		long currentScan = 1452545638167L;
		daoFactory.getInProcessPagesDAO().getLatestVisitedAt(
				PageContentProcessStatus.visited, currentScan);
	}

	@Test
	public void testGetFirstVisited() throws IOException,
			InProcessPageDBException, DAOFactoryException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_InProcessPageDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		HashSet<PageContentProcessStatus> statusAsVisited = new HashSet<PageContentProcessStatus>();
		statusAsVisited.add(PageContentProcessStatus.visited);
		statusAsVisited.add(PageContentProcessStatus.start_assess_cc);
		HashSet<String> langs = new HashSet<>();
		langs.add("de");
		List<M_InProcessPage> pages = daoFactory
				.getInProcessPagesDAO().getFirstVisitedPagesBySource(langs);
		System.out.println(pages.size());
		assertTrue(!pages.isEmpty());
		System.out.println(pages.get(0).fetchFromDBTime);
	}

	@Test
	public void testJson() {

		RuntimeTypeAdapterFactory<GlobalSearchPage> runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory
				.of(GlobalSearchPage.class, "type")
				.registerSubtype(HTMLPage.class, "html")
				.registerSubtype(DocumentPage.class, "document");
		Gson gson = new GsonBuilder().registerTypeAdapterFactory(
				runtimeTypeAdapterFactory).create();

		List<GlobalSearchPage> animals = new ArrayList<>();
		animals.add(new HTMLPage());
		animals.add(new HTMLPage());
		animals.add(new DocumentPage());

		String json = gson.toJson(animals);
		Type listType = new TypeToken<List<GlobalSearchPage>>() {
		}.getType();
		List<GlobalSearchPage> fromJson = gson.fromJson(json, listType);
		for (GlobalSearchPage animal : fromJson) {
			if (animal instanceof HTMLPage) {
				System.out.println(" html");
			} else if (animal instanceof DocumentPage) {
				System.out.println(" doc");
			} else
				System.out.println("Class not found");
		}
	}

	@Test
	public void testInProcessPageJson() {

		Gson gson = new GsonBuilder().create();

		M_InProcessPage page = new M_InProcessPage();
		page.globalSearchPage = new HTMLPage();
		HTMLContext context = new HTMLContext();
		context.contextID = "c1";
		page.globalSearchPage.conID2Con.put("c1", context);
		String json = gson.toJson(page);
		System.out.println(json);

		RuntimeTypeAdapterFactory<GlobalSearchPage> pageRuntimeTypeAdapterFactory = RuntimeTypeAdapterFactory
				.of(GlobalSearchPage.class, "type")
				.registerSubtype(HTMLPage.class, "html")
				.registerSubtype(DocumentPage.class, "document");
		RuntimeTypeAdapterFactory<GlobalSearchContext> contextRuntimeTypeAdapterFactory = RuntimeTypeAdapterFactory
				.of(GlobalSearchContext.class, "type")
				.registerSubtype(HTMLContext.class, "html_context")
				.registerSubtype(DocumentContext.class, "document_context");

		gson = new GsonBuilder()
				.registerTypeAdapterFactory(pageRuntimeTypeAdapterFactory)
				.registerTypeAdapterFactory(contextRuntimeTypeAdapterFactory)
				.create();
		M_InProcessPage fromJson = gson.fromJson(json, M_InProcessPage.class);

		if (fromJson.globalSearchPage instanceof HTMLPage) {
			System.out.println(" html");
			for (GlobalSearchContext c : fromJson.globalSearchPage.conID2Con
					.values()) {
				HTMLContext ht = (HTMLContext) c;
				System.out.println("html-context");
			}
		} else if (fromJson.globalSearchPage instanceof DocumentPage) {
			System.out.println(" doc");
		} else
			System.out.println("Class not found");
	}
}
