package com.datalyxt.production.webdatabase;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.sql.DataSource;

import org.joda.time.DateTimeConstants;
import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleContent;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V2_RuleContent;
import com.datalyxt.production.webmodel.source.LanguageISO_639_1;
import com.google.gson.Gson;

public class TestM_RuleDAO {


	@Test
	public void testGetRule() throws IOException, M_RuleDBException,
			DAOFactoryException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_RuleDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		HashSet<M_RuleType> type = new HashSet<M_RuleType>();
		type.add(M_RuleType.QT1V1);
		type.add(M_RuleType.QT1V2);
		List<M_Rule> rules = daoFactory.getM_RuleDAO().getRules(
				M_RuleStatus.activated, type);
		assertTrue(!rules.isEmpty());
	}

	@Test
	public void testGetRuleID() throws IOException, M_RuleDBException,
			DAOFactoryException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_RuleDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		HashSet<M_RuleType> type = new HashSet<M_RuleType>();
		type.add(M_RuleType.QT1V1);
		type.add(M_RuleType.QT1V2);
		List<M_Rule> rules = daoFactory.getM_RuleDAO().getRuleIDs(
				M_RuleStatus.added, type);
		assertTrue(!rules.isEmpty());
	}


	


	@Test
	public void testConnectionKilled() throws IOException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_RuleDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		long since = System.currentTimeMillis() - 3L
				* DateTimeConstants.MILLIS_PER_HOUR;
		int count = 1;
		while (count < 1000) {
			count++;
			try {
				daoFactory.getM_RuleDAO().getRuleByRuleId(21L, 1L);
				Thread.sleep(2000);
			} catch (M_RuleDBException | DAOFactoryException
					| InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Test
	public void testRuleContentJson() {
		QT1_V2_RuleContent content = new QT1_V2_RuleContent();
		content.lang = new HashSet<>();
		content.lang.add(LanguageISO_639_1.de);
		content.lang.add(LanguageISO_639_1.en);
		Gson gson = new Gson();
		String json = gson.toJson(content, M_RuleContent.class);
		System.out.println(json);
	}
}
