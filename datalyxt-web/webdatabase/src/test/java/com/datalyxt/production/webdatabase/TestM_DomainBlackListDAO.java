package com.datalyxt.production.webdatabase;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import org.junit.Test;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.webdatabase.factory.DAOFactory;
import com.datalyxt.production.webdatabase.factory.MyDataSourceFactory;
import com.datalyxt.production.webdatabase.factory.MysqlDAOFactory;

public class TestM_DomainBlackListDAO {
	@Test
	public void testInsertIntoBlackList() throws IOException, SQLException {
		Properties properties = new Properties();
		properties.load(TestM_DomainBlackListDAO.class.getClassLoader()
				.getResourceAsStream("db.properties"));
		DataSource datasource = MyDataSourceFactory
				.getMySQLDataSource(properties);
		DAOFactory daoFactory = new MysqlDAOFactory(datasource);
		try {
			String url = "https://twitter.com/intent/tweet?text=Warenr%C3%BCckrufe&url=https%3A%2F%2Fwww.migros.ch%2Fde%2Fkontakt%2Fwarenrueckrufe.html&via=Migros";
			url = "https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.migros.ch%2Fit%2Fnews%2Frichiamo-prodotti%2F2015%2Frueckruf-schwingschleifer.html";
			String domain = "twitter.com";
			domain = "facebook.com";
			daoFactory.getDomainBlackListDAO().insert(url);
		} catch (DAOFactoryException | M_DomainBlackListDBException e) {
			e.printStackTrace();
		}
	}
}
