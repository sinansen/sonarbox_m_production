package com.datalyxt.production.webdatabase.dao;

import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.execution.monitoring.model.M_TraceReport;

public interface M_TraceReportDAO extends DataBaseDAO{
	public void insertTraceReport(M_TraceReport traceReport)
			throws TraceReportDBException;
}
