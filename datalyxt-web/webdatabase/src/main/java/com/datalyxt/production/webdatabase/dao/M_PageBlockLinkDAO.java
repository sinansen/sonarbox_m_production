package com.datalyxt.production.webdatabase.dao;

import java.util.HashMap;

import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.webscraper.model.link.Link;

public interface M_PageBlockLinkDAO extends DataBaseDAO {

	public boolean contains(Link link) throws M_PageBlockLinkDBException;

	public void putLinkBlocks(HashMap<String, LinkBlock> linkBlocks,
			long blockId) throws M_PageBlockLinkDBException;

	public void updateLinkStatus(String url, boolean success) throws M_PageBlockLinkDBException;

}
