package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.exception.db.InProcessPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_InProcessPage;
import com.datalyxt.production.globalsearch.context.DocumentContext;
import com.datalyxt.production.globalsearch.context.GlobalSearchContext;
import com.datalyxt.production.globalsearch.context.GlobalSearchContextClassType;
import com.datalyxt.production.globalsearch.context.HTMLContext;
import com.datalyxt.production.globalsearch.page.DocumentPage;
import com.datalyxt.production.globalsearch.page.GlobalSearchPage;
import com.datalyxt.production.globalsearch.page.GlobalSearchPageClassType;
import com.datalyxt.production.globalsearch.page.HTMLPage;
import com.datalyxt.production.webdatabase.dao.M_InProcessPagesDAO;
import com.datalyxt.webscraper.model.link.Link;
import com.datalyxt.webscraper.model.util.RuntimeTypeAdapterFactory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MysqlM_InProcessPagesDAOImpl implements M_InProcessPagesDAO {

	private Connection connection;
	private Gson gsonSerial;
	private Gson gsonDeserial;

	public MysqlM_InProcessPagesDAOImpl(Connection connection) {
		this.connection = connection;
		gsonSerial = new GsonBuilder().disableHtmlEscaping().create();
		RuntimeTypeAdapterFactory<GlobalSearchPage> pageRuntimeTypeAdapterFactory = RuntimeTypeAdapterFactory
				.of(GlobalSearchPage.class, "type")
				.registerSubtype(HTMLPage.class,
						GlobalSearchPageClassType.html.name())
				.registerSubtype(DocumentPage.class,
						GlobalSearchPageClassType.document.name());
		RuntimeTypeAdapterFactory<GlobalSearchContext> contextRuntimeTypeAdapterFactory = RuntimeTypeAdapterFactory
				.of(GlobalSearchContext.class, "type")
				.registerSubtype(HTMLContext.class,
						GlobalSearchContextClassType.html_context.name())
				.registerSubtype(DocumentContext.class,
						GlobalSearchContextClassType.document_context.name());

		gsonDeserial = new GsonBuilder()
				.registerTypeAdapterFactory(pageRuntimeTypeAdapterFactory)
				.registerTypeAdapterFactory(contextRuntimeTypeAdapterFactory)
				.create();
	}

	@Override
	public synchronized void putInProcessPage(M_InProcessPage curPage)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert IGNORE  into m_crawlerlinks (linkId, link, status, fetchFromDBTime, visitedAt, page, sourceId, extractedAt, domain, crawlerhops, contexthops) values (md5(?),?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, curPage.pageURL.url);
			preparedStmt.setString(2, curPage.pageURL.url);
			preparedStmt.setString(3, curPage.status.name());
			preparedStmt.setLong(4, curPage.fetchFromDBTime);
			preparedStmt.setLong(5, curPage.visitedAt);

			String content = gsonSerial.toJson(curPage, M_InProcessPage.class);

			preparedStmt.setString(6, content);
			preparedStmt.setLong(7, curPage.sourceId);
			preparedStmt.setLong(8, curPage.extractedAt);
			preparedStmt.setString(9, curPage.pageURL.domain);
			preparedStmt.setInt(10, curPage.crawlerHops);
			preparedStmt.setInt(11, curPage.contextHops);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new InProcessPageDBException("putInProcessPage: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("putInProcessPage: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public synchronized List<M_InProcessPage> getInProcessPages(
			PageContentProcessStatus status, long fetchtimeFromDB)
			throws InProcessPageDBException {

		List<M_InProcessPage> pages = new ArrayList<M_InProcessPage>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			// TODO wird zu Problem führen, wenn zu viele Page ausgewählt
			// werden.
			String queryString = " select page  from m_crawlerlinks where status= ? and fetchFromDBTime = ? ";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, status.name());
			preparedStmt.setLong(2, fetchtimeFromDB);
			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				String detail = resultSet.getString("page");
				M_InProcessPage page = gsonDeserial.fromJson(detail,
						M_InProcessPage.class);
				pages.add(page);
			}
		} catch (Exception e) {
			throw new InProcessPageDBException("getInProcessPages: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException("getInProcessPages: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("getInProcessPages: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return pages;
	}

	@Override
	public synchronized M_InProcessPage getInProcessPage(Link next)
			throws InProcessPageDBException {

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {

			// the mysql insert statement
			String queryString = " select page, max(fetchFromDBTime) from m_crawlerlinks where linkId = md5(?) ";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, next.url);

			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				String detail = resultSet.getString("page");
				M_InProcessPage page = gsonDeserial.fromJson(detail,
						M_InProcessPage.class);
				return page;
			}
		} catch (Exception e) {
			throw new InProcessPageDBException("getInProcessPage: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException("getInProcessPage: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("getInProcessPage: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return null;
	}

	@Override
	public synchronized void updateInProcessPageContent(M_InProcessPage curPage)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "update m_crawlerlinks set status = ? , visitedAt = ? , isContextPage = ? , fetchUsedTime = ? , page = ? , page_lang = ? , matched_keywords = ? where linkId = ? and fetchFromDBTime = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, curPage.status.name());
			preparedStmt.setLong(2, curPage.visitedAt);
			preparedStmt.setBoolean(3, curPage.isContextPage);
			preparedStmt.setLong(4, curPage.fetchUsedTime);
			String content = gsonSerial.toJson(curPage, M_InProcessPage.class);
			preparedStmt.setString(5, content);
			String lang = null;
			if (curPage.globalSearchPage != null)
				lang = curPage.globalSearchPage.language;
			preparedStmt.setString(6, lang);
			String matched_keywords = null;
			if (curPage.globalSearchPage != null)
				matched_keywords = curPage.globalSearchPage.kw2ConID.keySet()
						.toString();
			preparedStmt.setString(7, matched_keywords);
			preparedStmt.setString(8, curPage.linkId);
			preparedStmt.setLong(9, curPage.fetchFromDBTime);

			preparedStmt.execute();

		} catch (Exception e) {
			throw new InProcessPageDBException(
					"updateInProcessPageContent: for link: "
							+ curPage.pageURL.url + " : " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"updateInProcessPageContent: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public synchronized void updateInProcessPageStatus(long fetchFromDBTime,
			String linkId, PageContentProcessStatus status, long visitedAt)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "update m_crawlerlinks set status = ? , visitedAt = ? where linkId = ? and fetchFromDBTime = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, status.name());
			preparedStmt.setLong(2, visitedAt);
			preparedStmt.setString(3, linkId);
			preparedStmt.setLong(4, fetchFromDBTime);

			preparedStmt.execute();

		} catch (Exception e) {
			throw new InProcessPageDBException("updateInProcessPageStatus: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"updateInProcessPageStatus: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public synchronized boolean isAllPagesVisited(long fetchTimeFromDB,
			HashSet<PageContentProcessStatus> statusAsVisited)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "SELECT  DISTINCT(STATUS) as s   FROM m_crawlerlinks WHERE fetchFromDBTime = ?";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setLong(1, fetchTimeFromDB);
			resultSet = preparedStmt.executeQuery();
			HashSet<PageContentProcessStatus> result = new HashSet<PageContentProcessStatus>();

			while (resultSet.next()) {
				String status = resultSet.getString("s");
				result.add(PageContentProcessStatus.valueOf(status));
			}

			boolean finished = CollectionUtils.isSubCollection(result,
					statusAsVisited);
			// System.out.println("result : "+finished+"  status: "+result.toString());
			return finished;

		} catch (Exception e) {
			throw new InProcessPageDBException("isAllPagesVisited: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException("isAllPagesVisited: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("isAllPagesVisited: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public synchronized M_InProcessPage getNextPageFIFO(String domain,
			long crawlingAt, int limitOfInProcessingLink)
			throws InProcessPageDBException {

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			// String queryString =
			// " select count(link) as amt from m_crawlerlinks where fetchFromDBTime = ? and  ( status = ?  or status = ? )";
			// preparedStmt = connection.prepareStatement(queryString);
			// preparedStmt.setLong(1, crawlingAt);
			// preparedStmt.setString(2,
			// PageContentProcessStatus.page_submitted_to_process.name());
			// preparedStmt.setString(3,
			// PageContentProcessStatus.page_process_started.name());
			// resultSet = preparedStmt.executeQuery();
			// if (resultSet.next()) {
			// int currentInProccessing = resultSet.getInt("amt");
			// if (currentInProccessing >= limitOfInProcessingLink)
			// return null;
			//
			// }
			// preparedStmt.close();
			// resultSet.close();

			// the mysql insert statement
			String queryString = " select linkId, page from m_crawlerlinks where ( domain = ? or domain like ? )and fetchFromDBTime = ? and status = ? ORDER BY extractedAt ASC limit 1";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, domain);
			preparedStmt.setString(2, "%" + domain);
			preparedStmt.setLong(3, crawlingAt);
			preparedStmt.setString(4,
					PageContentProcessStatus.page_created.name());
			resultSet = preparedStmt.executeQuery();

			if (resultSet.next()) {
				String detail = resultSet.getString("page");

				M_InProcessPage page = gsonDeserial.fromJson(detail,
						M_InProcessPage.class);
				page.linkId = resultSet.getString("linkId");
				return page;
			}
		} catch (Exception e) {
			throw new InProcessPageDBException("getNextPageFIFO: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException("getNextPageFIFO: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("getNextPageFIFO: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return null;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException("isConnectionClosed: " + e);
		}
		return false;
	}

	@Override
	public synchronized Long getLatestVisitedAt(
			PageContentProcessStatus status, long fetchtimeFromDB)
			throws InProcessPageDBException {

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = " select max(visitedAt) as vis from m_crawlerlinks where status= ? and fetchFromDBTime = ? ";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, status.name());
			preparedStmt.setLong(2, fetchtimeFromDB);
			resultSet = preparedStmt.executeQuery();

			if (resultSet.next()) {
				Long maxVisitedAt = resultSet.getLong("vis");
				return maxVisitedAt;
			}
		} catch (Exception e) {
			throw new InProcessPageDBException(
					"getLatestVisitedInProcessPages: " + e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException(
							"getLatestVisitedInProcessPages: " + e.getMessage(),
							e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"getLatestVisitedInProcessPages: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return -1L;
	}

	@Override
	public synchronized List<M_InProcessPage> getInProcessPagesFromNewDomain(
			PageContentProcessStatus status, long fetchtimeFromDB)
			throws InProcessPageDBException {
		List<M_InProcessPage> pages = new ArrayList<M_InProcessPage>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = " select page , count(*) from m_crawlerlinks where status= ? and fetchFromDBTime = ? group by domain HAVING count(*) =1";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, status.name());
			preparedStmt.setLong(2, fetchtimeFromDB);
			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				String detail = resultSet.getString("page");
				M_InProcessPage page = gsonDeserial.fromJson(detail,
						M_InProcessPage.class);
				pages.add(page);
			}
		} catch (Exception e) {
			throw new InProcessPageDBException(
					"getInProcessPagesFromNewDomain: " + e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException(
							"getInProcessPagesFromNewDomain: " + e.getMessage(),
							e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"getInProcessPagesFromNewDomain: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return pages;
	}

	private void backupInProcessNewLinks(M_InProcessPage parent)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert IGNORE into m_tmp_new_links (linkId, link, fetchFromDBTime, amount_new_links ,new_links_json , sourceId) values (md5(?),?, ?, ?, ?, ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, parent.pageURL.url);
			preparedStmt.setString(2, parent.pageURL.url);
			preparedStmt.setLong(3, parent.fetchFromDBTime);
			preparedStmt.setInt(4, parent.contextpagelinks.size());

			String newLinkJson = gsonSerial.toJson(parent.contextpagelinks);

			preparedStmt.setString(5, newLinkJson);
			preparedStmt.setLong(6, parent.sourceId);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new InProcessPageDBException("backupInProcessNewLinks: "
					+ e.getMessage(), e);

		} finally {
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"backupInProcessNewLinks: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public synchronized void putInProcessPages(M_InProcessPage parent,
			List<M_InProcessPage> nextPages) throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;

		backupInProcessNewLinks(parent);

		int counter = 0;

		try {
			String queryString = "insert into m_crawlerlinks (linkId, link, status, fetchFromDBTime, visitedAt, page, sourceId, extractedAt, domain, crawlerhops, contexthops) values (md5(?),?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE "
					+ " crawlerhops = IF(crawlerhops > VALUES(crawlerhops), VALUES(crawlerhops), crawlerhops) , contexthops = IF(contexthops > VALUES(contexthops), VALUES(contexthops), contexthops)";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			String errorMsg = "";

			for (M_InProcessPage curPage : nextPages) {
				try {
					preparedStmt.setString(1, curPage.pageURL.url);
					preparedStmt.setString(2, curPage.pageURL.url);
					preparedStmt.setString(3, curPage.status.name());
					preparedStmt.setLong(4, curPage.fetchFromDBTime);
					preparedStmt.setLong(5, curPage.visitedAt);

					String content = gsonSerial.toJson(curPage,
							M_InProcessPage.class);

					preparedStmt.setString(6, content);
					preparedStmt.setLong(7, curPage.sourceId);
					preparedStmt.setLong(8, curPage.extractedAt);
					preparedStmt.setString(9, curPage.pageURL.domain);
					preparedStmt.setInt(10, curPage.crawlerHops);
					preparedStmt.setInt(11, curPage.contextHops);
					preparedStmt.execute();
					counter = counter + 1;

				} catch (Exception e) {
					errorMsg = errorMsg + " \n insert page error: url "
							+ curPage.pageURL.url + " " + e.getMessage();
				}
			}

			if (counter != nextPages.size())
				throw new InProcessPageDBException(
						"Inserting new pages contains error: "
								+ (nextPages.size() - counter) + " of "
								+ nextPages.size()
								+ " new In_ProcessPages were NOT inserted !!! "
								+ errorMsg);

		} catch (Exception e) {
			throw new InProcessPageDBException("putInProcessPages: "
					+ e.getMessage(), e);

		} finally {
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("putInProcessPages: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	class DomainCandidate implements Comparable<DomainCandidate> {
		long fetch_from_db_time;

		long sourceId;

		HashSet<PageContentProcessStatus> statuses = new HashSet<PageContentProcessStatus>();

		@Override
		public int compareTo(DomainCandidate o) {
			long diff = fetch_from_db_time - o.fetch_from_db_time;
			if (diff == 0)
				return 0;
			if (diff > 0)
				return 1;
			return -1;
		}

	}

	@Override
	public List<M_InProcessPage> getFirstVisitedPagesBySource(
			HashSet<String> langs) throws InProcessPageDBException {

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryDomains = " SELECT DISTINCT(STATUS) , fetchFromDBTime , sourceId FROM m_crawlerlinks WHERE STATUS != ? and status != ? order by fetchFromDBTime ASC";

			String queryPages = " select page from m_crawlerlinks where fetchFromDBTime = ? and sourceId = ? and status = ? and isContextPage = 1";

			preparedStmt = connection.prepareStatement(queryDomains);
			preparedStmt.setString(1,
					PageContentProcessStatus.assess_cc_finished.name());
			preparedStmt.setString(2,
					PageContentProcessStatus.start_assess_cc.name());
			resultSet = preparedStmt.executeQuery();

			HashMap<Long, HashSet<Long>> possibleCandidates = new HashMap<Long, HashSet<Long>>();
			HashMap<Long, HashSet<Long>> nonCandidates = new HashMap<Long, HashSet<Long>>();
			while (resultSet.next()) {

				long sourceId = resultSet.getLong("sourceId");
				long fetch_from_db_time = resultSet.getLong("fetchFromDBTime");
				String status = resultSet.getString("status");
				PageContentProcessStatus pageContentProcessStatus = PageContentProcessStatus
						.valueOf(status);
				if (!pageContentProcessStatus
						.equals(PageContentProcessStatus.visited)) {
					HashSet<Long> notfinsihedDomains = nonCandidates
							.get(fetch_from_db_time);
					if (notfinsihedDomains == null)
						notfinsihedDomains = new HashSet<>();
					notfinsihedDomains.add(sourceId);
					nonCandidates.put(fetch_from_db_time, notfinsihedDomains);
				} else {
					HashSet<Long> domains = possibleCandidates
							.get(fetch_from_db_time);
					if (domains == null)
						domains = new HashSet<>();
					domains.add(sourceId);
					possibleCandidates.put(fetch_from_db_time, domains);
				}

			}
			preparedStmt.close();
			resultSet.close();

			List<DomainCandidate> candidates = getCandidatesASC(
					possibleCandidates, nonCandidates);

			if (candidates.isEmpty())
				return Collections.emptyList();
			for (DomainCandidate candidate : candidates) {
				preparedStmt = connection.prepareStatement(queryPages);
				preparedStmt.setLong(1, candidate.fetch_from_db_time);
				preparedStmt.setLong(2, candidate.sourceId);
				preparedStmt.setString(3,
						PageContentProcessStatus.visited.name());
				resultSet = preparedStmt.executeQuery();

				List<M_InProcessPage> pages = new ArrayList<M_InProcessPage>();

				while (resultSet.next()) {

					String detail = resultSet.getString("page");
					M_InProcessPage page = gsonDeserial.fromJson(detail,
							M_InProcessPage.class);
					if (page.globalSearchPage != null) {
						if (langs.contains(page.globalSearchPage.language))
							pages.add(page);
					}

				}
				if (!pages.isEmpty())
					return pages;
			}
			return Collections.emptyList();
		} catch (Exception e) {
			throw new InProcessPageDBException("getFirstVisitedPagesBySource: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException(
							"getFirstVisitedPagesBySource: " + e.getMessage(),
							e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"getFirstVisitedPagesBySource: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	private List<DomainCandidate> getCandidatesASC(
			HashMap<Long, HashSet<Long>> possibleCandidates,
			HashMap<Long, HashSet<Long>> nonCandidates) {

		List<DomainCandidate> candidates = new ArrayList<>();
		if (possibleCandidates == null || possibleCandidates.isEmpty())
			return candidates;

		for (Long fetchTime : possibleCandidates.keySet()) {
			HashSet<Long> sources = possibleCandidates.get(fetchTime);
			for (Long sourceId : sources) {

				DomainCandidate selectedDomainCandidate = new DomainCandidate();
				selectedDomainCandidate.sourceId = sourceId;
				selectedDomainCandidate.fetch_from_db_time = fetchTime;

				if (nonCandidates.containsKey(fetchTime)) {
					HashSet<Long> notfinishedDomains = nonCandidates
							.get(fetchTime);
					if (!notfinishedDomains.contains(sourceId))
						candidates.add(selectedDomainCandidate);
				} else
					candidates.add(selectedDomainCandidate);
			}
		}

		Collections.sort(candidates);
		return candidates;
	}

	@Override
	public void updateInProcessPagesStatus(List<Link> pageURLs,
			long fetchFromDBTime, PageContentProcessStatus status)
			throws InProcessPageDBException {
		if (pageURLs == null || pageURLs.isEmpty())
			return;
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "update m_crawlerlinks set status = ? where linkId = md5(?) and fetchFromDBTime = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			for (Link pageURL : pageURLs) {
				preparedStmt.setString(1, status.name());
				preparedStmt.setString(2, pageURL.url);
				preparedStmt.setLong(3, fetchFromDBTime);
				preparedStmt.execute();
			}

		} catch (Exception e) {
			throw new InProcessPageDBException("updateInProcessPagesStatus: "
					+ e.getMessage(), e);

		} finally {
			if (preparedStmt != null) {
				try {
					connection.setAutoCommit(true);
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"updateInProcessPagesStatus: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public List<M_InProcessPage> getLatestVisitedDomains(long fetchtimeFromDB)
			throws InProcessPageDBException {
		List<M_InProcessPage> pages = new ArrayList<M_InProcessPage>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
//			String queryString = " select sourceId, domain,fetchUsedTime,  visitedAt,  max(visitedAt) as mvis from m_crawlerlinks where ( status= ? or status = ? ) and fetchFromDBTime = ? group by domain";
			String queryString = "SELECT t.sourceId, t.domain, t.fetchUsedTime , t.visitedAt "
					+ "FROM (SELECT domain , MAX(visitedAt) AS max_vis FROM m_crawlerlinks WHERE fetchFromDBTime = ? GROUP BY domain ) AS m "
					+ "INNER JOIN m_crawlerlinks AS t ON t.domain = m.domain AND t.visitedAt = m.max_vis AND t.fetchFromDBTime = ?" ;
			preparedStmt = connection.prepareStatement(queryString);
//			preparedStmt.setString(1, PageContentProcessStatus.visited.name());
//			preparedStmt.setString(2,
//					PageContentProcessStatus.page_created.name());
			preparedStmt.setLong(1, fetchtimeFromDB);
			preparedStmt.setLong(2, fetchtimeFromDB);
			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				String domain = resultSet.getString("domain");
				long visitedAt = resultSet.getLong("visitedAt");
				M_InProcessPage page = new M_InProcessPage();
				page.domain = domain;
				page.fetchUsedTime = resultSet.getLong("fetchUsedTime");
				page.sourceId = resultSet.getLong("sourceId");
				page.visitedAt = visitedAt;
				pages.add(page);
			}
		} catch (Exception e) {
			throw new InProcessPageDBException("getLatestVisitedDomains: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException(
							"getLatestVisitedDomains: " + e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"getLatestVisitedDomains: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
		return pages;
	}

	@Override
	public List<M_InProcessPage> getNewDomains(PageContentProcessStatus status,
			long fetchtimeFromDB) throws InProcessPageDBException {
		List<M_InProcessPage> pages = new ArrayList<M_InProcessPage>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = " select sourceId, domain, fetchUsedTime, visitedAt , count(*) from m_crawlerlinks where status= ? and fetchFromDBTime = ? group by domain "; // HAVING
																																												// count(*)
																																												// =1
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, status.name());
			preparedStmt.setLong(2, fetchtimeFromDB);
			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				String domain = resultSet.getString("domain");
				long visitedAt = resultSet.getLong("visitedAt");
				M_InProcessPage page = new M_InProcessPage();
				page.domain = domain;
				page.fetchUsedTime = resultSet.getLong("fetchUsedTime");
				page.sourceId = resultSet.getLong("sourceId");
				page.visitedAt = visitedAt;
				pages.add(page);
			}
		} catch (Exception e) {
			throw new InProcessPageDBException("getNewDomains: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException("getNewDomains: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("getNewDomains: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return pages;
	}

	@Override
	public void deleteOldPages(long currentScan, String sourceId)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "delete from m_crawlerlinks where fetchFromDBTime = ? and sourceId = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setLong(1, currentScan);
			preparedStmt.setString(2, sourceId);

			preparedStmt.execute();

		} catch (Exception e) {
			throw new InProcessPageDBException("deleteOldPages: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("deleteOldPages: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public int getInProcessingLinksAmount(long fetchFromDBTime)
			throws InProcessPageDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = " select count(link) as amt from m_crawlerlinks where fetchFromDBTime = ? and  ( status = ?  or status = ? )";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setLong(1, fetchFromDBTime);
			preparedStmt.setString(2,
					PageContentProcessStatus.page_submitted_to_process.name());
			preparedStmt.setString(3,
					PageContentProcessStatus.page_process_started.name());
			resultSet = preparedStmt.executeQuery();
			if (resultSet.next()) {
				int currentInProccessing = resultSet.getInt("amt");
				return currentInProccessing;
			}

		} catch (Exception e) {
			throw new InProcessPageDBException("hasReachedMaxLimit: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException("hasReachedMaxLimit: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException("hasReachedMaxLimit: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return 0;
	}

	@Override
	public List<M_InProcessPage> getFirstVisitedPagesBySourceBeta(
			HashSet<String> langs) throws InProcessPageDBException {

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String querySources = " SELECT DISTINCT STATUS , fetchFromDBTime , sourceId FROM m_crawlerlinks order by fetchFromDBTime ASC";

			String queryPages = " select page_lang, page from m_crawlerlinks where fetchFromDBTime = ? and sourceId = ? and status = ? and isContextPage = 1";

			preparedStmt = connection.prepareStatement(querySources);
			// preparedStmt.setString(1,
			// PageContentProcessStatus.assess_cc_finished.name());
			// preparedStmt.setString(2,
			// PageContentProcessStatus.start_assess_cc.name());
			resultSet = preparedStmt.executeQuery();

			HashMap<String, DomainCandidate> queryResults = new HashMap<String, DomainCandidate>();

			while (resultSet.next()) {

				long sourceId = resultSet.getLong("sourceId");
				long fetch_from_db_time = resultSet.getLong("fetchFromDBTime");
				String status = resultSet.getString("status");
				PageContentProcessStatus pageContentProcessStatus = PageContentProcessStatus
						.valueOf(status);
				String key = "" + fetch_from_db_time + ":" + sourceId;
				DomainCandidate domainCandidate = queryResults.get(key);
				if (domainCandidate == null) {
					domainCandidate = new DomainCandidate();
				}
				domainCandidate.sourceId = sourceId;
				domainCandidate.fetch_from_db_time = fetch_from_db_time;
				domainCandidate.statuses.add(pageContentProcessStatus);
				queryResults.put(key, domainCandidate);
			}
			preparedStmt.close();
			resultSet.close();

			if (queryResults.isEmpty())
				return Collections.emptyList();
			for (String candidate : queryResults.keySet()) {
				DomainCandidate domainCandidate = queryResults.get(candidate);
				if (domainCandidate.statuses.size() == 1
						&& domainCandidate.statuses
								.contains(PageContentProcessStatus.visited)) {

					preparedStmt = connection.prepareStatement(queryPages);
					preparedStmt.setLong(1, domainCandidate.fetch_from_db_time);
					preparedStmt.setLong(2, domainCandidate.sourceId);
					preparedStmt.setString(3,
							PageContentProcessStatus.visited.name());
					resultSet = preparedStmt.executeQuery();

					List<M_InProcessPage> pages = new ArrayList<M_InProcessPage>();

					while (resultSet.next()) {
						String lang = resultSet.getString("page_lang");
						if (langs.contains(lang)) {
							String detail = resultSet.getString("page");
							M_InProcessPage page = gsonDeserial.fromJson(
									detail, M_InProcessPage.class);
							pages.add(page);
						}
					}
					if (!pages.isEmpty())
						return pages;
				}
			}
			return Collections.emptyList();
		} catch (Exception e) {
			throw new InProcessPageDBException(
					"getFirstVisitedPagesBySourceBeta: " + e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new InProcessPageDBException(
							"getFirstVisitedPagesBySourceBeta: "
									+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new InProcessPageDBException(
							"getFirstVisitedPagesBySourceBeta: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}
}
