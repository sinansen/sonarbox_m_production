package com.datalyxt.production.webdatabase.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.webdatabase.dao.M_FileStorageDAO;
import com.datalyxt.production.webmodel.email.Attachement;
import com.datalyxt.production.webscraper.model.action.ActionMetaConstant;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.FSLocation;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

public class M_FileStorageDAOImpl implements M_FileStorageDAO {

	private FSLocation fsLocation;

	public M_FileStorageDAOImpl(Properties fsProperties) {
		fsLocation = new FSLocation();
		fsLocation.SSHPort = Integer.valueOf(fsProperties
				.getProperty("ssh_port"));
		fsLocation.sftp_Directory = fsProperties.getProperty("sftp_directory");
		fsLocation.sftp_Host = fsProperties.getProperty("sftp_host");
		fsLocation.sftp_Username = fsProperties.getProperty("sftp_username");
		fsLocation.sftp_Password = fsProperties.getProperty("sftp_password");
		fsLocation.useSFTP = Boolean.valueOf(fsProperties.getProperty(
				"use_sftp").trim());
		fsLocation.fsHost = fsProperties.getProperty("fs_host");
		fsLocation.fsPath = fsProperties.getProperty("fs_path");
	}

	@Override
	public boolean writeFile(byte[] content, String[] path, String fileName)
			throws FileStorageException {
		if (fsLocation.useSFTP)
			return writeFileBySFTP(content, path, fileName);
		else
			return writeFileByFS(content, path, fileName);

	}

	private boolean writeFileByFS(byte[] content, String[] path, String fileName)
			throws FileStorageException {
		try {

			Path location = FileSystems.getDefault().getPath(
					String.join("/", path));
			if (Files.notExists(location, LinkOption.NOFOLLOW_LINKS)) {
				location = Files.createDirectories(location);
			}
			Path file = FileSystems.getDefault().getPath(location.toString(),
					fileName);
			// if (Files.exists(file))
			// throw new FileStorageException(
			// "write file in linux failed. file: " + file.toString()
			// + " already exists. ");

			Files.write(file, content);

		} catch (Exception e) {
			throw new FileStorageException("write file in linux failed. path: "
					+ String.join("/", path) + " name: " + fileName + " "
					+ e.getMessage(), e);
		}
		return false;
	}

	private boolean writeFileBySFTP(byte[] content, String[] path,
			String fileName) throws FileStorageException {
		JSch obj_JSch = new JSch();
		Session obj_Session = null;
		String str_FileDirectory = "";
		try {
			obj_Session = obj_JSch.getSession(fsLocation.sftp_Username,
					fsLocation.sftp_Host);
			obj_Session.setPort(fsLocation.SSHPort);
			obj_Session.setPassword(fsLocation.sftp_Password);
			Properties obj_Properties = new Properties();
			obj_Properties.put("StrictHostKeyChecking", "no");
			obj_Session.setConfig(obj_Properties);
			obj_Session.connect();
			Channel obj_Channel = obj_Session.openChannel("sftp");
			obj_Channel.connect();
			ChannelSftp obj_SFTPChannel = (ChannelSftp) obj_Channel;
			enterPath(obj_SFTPChannel, fsLocation.sftp_Directory, path);
			InputStream obj_InputStream = new ByteArrayInputStream(content);
			obj_SFTPChannel.put(obj_InputStream, str_FileDirectory + fileName);
			obj_SFTPChannel.exit();
			obj_InputStream.close();
			obj_Channel.disconnect();
			obj_Session.disconnect();
			return true;
		} catch (Exception ex) {
			throw new FileStorageException("write file in linux failed. path: "
					+ str_FileDirectory + " name: " + fileName + " "
					+ ex.getMessage(), ex);
		}
	}

	private void enterPath(ChannelSftp obj_SFTPChannel, String str_Directory,
			String[] path) throws SftpException {
		obj_SFTPChannel.cd(str_Directory);
		for (String p : path) {
			try {
				obj_SFTPChannel.cd(p);
			} catch (SftpException e) {
				obj_SFTPChannel.mkdir(p);
				obj_SFTPChannel.cd(p);
			}
		}
	}

	@Override
	public byte[] readFile(String path, String fileName)
			throws FileStorageException {
		if (fsLocation.useSFTP)
			return readFileBySFTP(path, fileName);
		else
			return readFileByFS(path, fileName);
	}

	private byte[] readFileByFS(String path, String fileName)
			throws FileStorageException {
		try {

			Path location = FileSystems.getDefault().getPath(path);
			Path file = FileSystems.getDefault().getPath(location.toString(),
					fileName);
			if (Files.notExists(file))
				throw new FileStorageException(
						"read file from linux FS failed. path: " + path
								+ " file: " + file.toString()
								+ " does not exist. ");

			return Files.readAllBytes(file);
		} catch (Exception ex) {
			throw new FileStorageException(
					"read file from linux failed. path: " + path + " name: "
							+ fileName + ex.getMessage(), ex);
		}
	}

	private byte[] readFileBySFTP(String path, String fileName)
			throws FileStorageException {
		JSch obj_JSch = new JSch();
		Session obj_Session = null;
		String str_FileDirectory = fsLocation.sftp_Directory + path;
		try {

			obj_Session = obj_JSch.getSession(fsLocation.sftp_Username,
					fsLocation.sftp_Host);
			obj_Session.setPort(fsLocation.SSHPort);
			obj_Session.setPassword(fsLocation.sftp_Password);
			Properties obj_Properties = new Properties();
			obj_Properties.put("StrictHostKeyChecking", "no");
			obj_Session.setConfig(obj_Properties);
			obj_Session.connect();
			Channel obj_Channel = obj_Session.openChannel("sftp");
			obj_Channel.connect();
			ChannelSftp obj_SFTPChannel = (ChannelSftp) obj_Channel;
			obj_SFTPChannel.cd(str_FileDirectory);
			InputStream obj_InputStream = obj_SFTPChannel.get(fileName);
			byte[] resutl = IOUtils.toByteArray(obj_InputStream);
			obj_InputStream.close();
			obj_SFTPChannel.exit();
			obj_Channel.disconnect();
			obj_Session.disconnect();
			return resutl;
		} catch (Exception ex) {
			throw new FileStorageException(
					"read file from linux failed. path: " + str_FileDirectory
							+ " name: " + fileName + ex.getMessage(), ex);
		}
	}

	@Override
	public FSLocation getLocation() throws FileStorageException {
		return fsLocation;
	}

	@Override
	public List<Attachement> readFiles(List<ActionResultFile> fileMeta)
			throws FileStorageException {

		if (fsLocation.useSFTP)
			return readFilesBySFTP(fileMeta);
		else
			return readFilesByFS(fileMeta);
	}

	private List<Attachement> readFilesByFS(List<ActionResultFile> fileMeta)
			throws FileStorageException {
		List<Attachement> attachements = new ArrayList<>();
		String path = "";
		String fileName = "";
		try {

			for (ActionResultFile meta : fileMeta) {
				path = String.join("/", meta.path);
				fileName = meta.name;
				String mimetype = "image/png";
				if (meta.type == ActionMetaConstant.pdf) {
					mimetype = "application/pdf";
				}
				byte[] result = readFileByFS(path, fileName);
				Attachement attachement = new Attachement(result, mimetype);
				attachement.name = meta.dName;
				attachements.add(attachement);
			}
			return attachements;
		} catch (Exception ex) {
			throw new FileStorageException(
					"read file from linux failed. path: " + path + " name: "
							+ fileName + " " + ex.getMessage(), ex);
		}
	}

	private List<Attachement> readFilesBySFTP(List<ActionResultFile> fileMeta)
			throws FileStorageException {

		List<Attachement> attachements = new ArrayList<>();
		JSch obj_JSch = new JSch();
		Session obj_Session = null;
		String path = "";
		String fileName = "";
		try {

			obj_Session = obj_JSch.getSession(fsLocation.sftp_Username,
					fsLocation.sftp_Host);
			obj_Session.setPort(fsLocation.SSHPort);
			obj_Session.setPassword(fsLocation.sftp_Password);
			Properties obj_Properties = new Properties();
			obj_Properties.put("StrictHostKeyChecking", "no");
			obj_Session.setConfig(obj_Properties);
			obj_Session.connect();
			Channel obj_Channel = obj_Session.openChannel("sftp");
			obj_Channel.connect();
			ChannelSftp obj_SFTPChannel = (ChannelSftp) obj_Channel;

			for (ActionResultFile meta : fileMeta) {
				path = String.join("/", meta.path);
				fileName = meta.name;
				obj_SFTPChannel.cd(path);
				InputStream obj_InputStream = obj_SFTPChannel.get(fileName);
				byte[] result = IOUtils.toByteArray(obj_InputStream);
				obj_InputStream.close();
				String mimetype = "image/png";
				if (meta.type == ActionMetaConstant.pdf) {
					mimetype = "application/pdf";
				}
				Attachement attachement = new Attachement(result, mimetype);
				attachement.name = meta.dName;
				attachements.add(attachement);
			}
			obj_SFTPChannel.exit();
			obj_Channel.disconnect();
			obj_Session.disconnect();
			return attachements;
		} catch (Exception ex) {
			throw new FileStorageException(
					"read file from linux failed. path: " + path + " name: "
							+ fileName + " " + ex.getMessage(), ex);
		}
	}

}
