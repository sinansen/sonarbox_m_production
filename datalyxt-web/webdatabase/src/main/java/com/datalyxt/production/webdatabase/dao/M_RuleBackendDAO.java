package com.datalyxt.production.webdatabase.dao;

import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;

public interface M_RuleBackendDAO extends DataBaseDAO {

	public List<M_Rule> getRules(M_RuleStatus ruleStatus,
			HashSet<M_RuleType> ruleTypes) throws M_RuleDBException;

	public List<M_Rule> getRuleIDs(M_RuleStatus ruleStatus,
			HashSet<M_RuleType> types) throws M_RuleDBException;

	public List<M_Rule> getRules(M_RuleStatus status, long sourceId)
			throws M_RuleDBException;

	public M_Rule getRuleByRuleId(Long ruleId, Long ownerId)
			throws M_RuleDBException;

}
