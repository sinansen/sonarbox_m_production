package com.datalyxt.production.webdatabase.dao;

import java.util.HashMap;

import com.datalyxt.production.exception.db.M_SynonymsDBException;
import com.datalyxt.production.webmodel.rule.M_Synonym;

public interface M_SynonymsDAO extends DataBaseDAO{

	public  HashMap<String, M_Synonym> loadAllSynonyms() throws M_SynonymsDBException;

}
