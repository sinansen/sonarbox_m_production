package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_GlobalSearchPageDBException;
import com.datalyxt.production.globalsearch.ranking.RankingReport;

public interface M_GlobalSearchPageDAO extends DataBaseDAO{

	public void insertRankingReport(RankingReport rankingReport) throws M_GlobalSearchPageDBException;

	public List<RankingReport> getRankingReports(boolean since, long fetchFromDBTime) throws M_GlobalSearchPageDBException;

}
