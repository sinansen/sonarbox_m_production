package com.datalyxt.production.webdatabase.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.exception.db.M_ReceiverDBException;
import com.datalyxt.production.exception.db.M_ReceiverGroupDBException;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverGroup;
import com.datalyxt.production.receiver.M_ReceiverGroupStatus;
import com.datalyxt.production.receiver.M_ReceiverStatus;

public interface M_ReceiverDAO extends DataBaseDAO {
	public long insertReceiver(M_Receiver receiver)
			throws M_ReceiverDBException;

	public void modifyReceiver(String sql, Object[] params)
			throws M_ReceiverDBException;

	public boolean isReceiverExist(Long receiverId)
			throws M_ReceiverDBException;

	public List<M_Receiver> getReceiver(String sql, Object[] params)
			throws M_ReceiverDBException;

	public M_Receiver getReceiverByEmail(String email)
			throws M_ReceiverDBException;

	public M_Receiver getReceiverById(long receiverId, long ownerId)
			throws M_ReceiverDBException;

	public boolean isGroupNameExist(String groupName, long ownerId)
			throws M_ReceiverGroupDBException;

	public long insertReceiverGroup(M_ReceiverGroup receiverGroup)
			throws M_ReceiverGroupDBException;

	public void modifyReceiverGroup(String sql, Object[] params)
			throws M_ReceiverGroupDBException;

	public M_ReceiverGroup getReceiverGroupById(long groupId, long ownerId)
			throws M_ReceiverGroupDBException;

	public List<M_ReceiverGroup> getReceiverGroups(String sql, Object[] params)
			throws M_ReceiverGroupDBException;

	public int[] removeReceiverFromGroup(long groupId,
			HashSet<Long> groupMember, long ownerId)
			throws M_ReceiverGroupDBException;

	public int[] addReceiverToGroup(long groupId, HashSet<Long> groupMember,
			long ownerId) throws M_ReceiverGroupDBException;

	public int[] addReceiverToGroup(HashSet<Long> groupId, long groupMember,
			long ownerId) throws M_ReceiverGroupDBException;

	public List<M_Receiver> getReceiverByGroupId(long groupId)
			throws M_ReceiverGroupDBException;

	public List<M_Receiver> getReciverByStatus(M_ReceiverStatus activated)
			throws M_ReceiverDBException;

	public void removeReceiverFromAllGroup(long receiverId, long ownerId)
			throws M_ReceiverDBException;

	public boolean isGroupOwner(long groupId, long ownerId)
			throws M_ReceiverDBException;

	public int removeGroup(long groupId, long ownerId)
			throws M_ReceiverGroupDBException;

	public void changeReceiverStatus(long id, M_ReceiverStatus status,
			long ownerId) throws M_ReceiverDBException;

	public void changeGroupStatus(long id, M_ReceiverGroupStatus status,
			long ownerId) throws M_ReceiverGroupDBException;

	public void removeReceiver(long receiverId, long userId)
			throws M_ReceiverDBException;

	public List<M_ReceiverGroup> getReceiverGroups(long receiverId)
			throws M_ReceiverGroupDBException;

	public HashMap<Long, M_ReceiverGroup> getReceiverGroups(
			HashSet<Long> groupIds) throws M_ReceiverGroupDBException;

	public int[] removeReceiverFromGroup(HashSet<Long> groupIds,
			Long groupMember, long ownerId) throws M_ReceiverGroupDBException;

	public HashSet<Long> getReceiverByGroupIds(HashSet<Long> groupIds, long ownerId)
			throws M_ReceiverGroupDBException;
}
