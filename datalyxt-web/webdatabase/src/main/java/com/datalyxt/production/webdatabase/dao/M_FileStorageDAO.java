package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.FileStorageException;
import com.datalyxt.production.webmodel.email.Attachement;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.FSLocation;

public interface M_FileStorageDAO {
	public FSLocation getLocation() throws FileStorageException;

	public boolean writeFile(byte[] content, String[] path, String name)
			throws FileStorageException;

	public byte[] readFile(String path, String name)
			throws FileStorageException;

	public List<Attachement> readFiles(List<ActionResultFile> fileMeta)throws FileStorageException;
	
}
