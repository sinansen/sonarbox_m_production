package com.datalyxt.production.webdatabase.dao;

import com.datalyxt.exception.db.DataBaseException;

public interface M_BackendControlDAO extends DataBaseDAO{
	public int getCurrentCommand()
			throws DataBaseException;
}
