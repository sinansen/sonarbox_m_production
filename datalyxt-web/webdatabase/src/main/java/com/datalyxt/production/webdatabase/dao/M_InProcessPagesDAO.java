package com.datalyxt.production.webdatabase.dao;

import java.util.HashSet;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.exception.db.InProcessPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.execution.monitoring.model.M_InProcessPage;
import com.datalyxt.webscraper.model.link.Link;

public interface M_InProcessPagesDAO {

	public void putInProcessPage(M_InProcessPage curPage) throws InProcessPageDBException;

	public M_InProcessPage getInProcessPage(Link next)  throws InProcessPageDBException;

	public void updateInProcessPageStatus(long fetchFromDBTime, String linkId, PageContentProcessStatus status, long visitedAt) throws InProcessPageDBException;
	
	public void updateInProcessPageContent(M_InProcessPage inProcessPage) throws InProcessPageDBException;

	public List<M_InProcessPage> getInProcessPages( PageContentProcessStatus status, long fetchtimeFromDB) throws InProcessPageDBException;

	public boolean isAllPagesVisited(long fetchTimeFromDB, HashSet<PageContentProcessStatus> statusAsVisited) throws InProcessPageDBException;
	
	public List<M_InProcessPage>  getFirstVisitedPagesBySource(HashSet<String> langs) throws InProcessPageDBException;
	
	public M_InProcessPage getNextPageFIFO(String domain, long crawlingAt, int limitOfInProcessingLink)
			throws InProcessPageDBException;

	public boolean isConnectionClosed() throws DataBaseException;

	public Long getLatestVisitedAt(
			PageContentProcessStatus status, long currentScan)	throws InProcessPageDBException;

	public void putInProcessPages(M_InProcessPage parent, List<M_InProcessPage> nextPages) throws InProcessPageDBException;

	public List<M_InProcessPage> getInProcessPagesFromNewDomain(
			PageContentProcessStatus status, long fetchtimeFromDB)
			throws InProcessPageDBException;

	public void updateInProcessPagesStatus(List<Link> pageURLs, long fetchtimeFromDB,
			PageContentProcessStatus status) 	throws InProcessPageDBException;

	public List<M_InProcessPage> getLatestVisitedDomains(long currentScan)throws InProcessPageDBException;

	public List<M_InProcessPage> getNewDomains(
			PageContentProcessStatus pageCreated, long currentScan)throws InProcessPageDBException;

	public void deleteOldPages(long currentScan, String sourceId)throws InProcessPageDBException;

	public int getInProcessingLinksAmount(long fetchFromDBTime)throws InProcessPageDBException;

	List<M_InProcessPage> getFirstVisitedPagesBySourceBeta(HashSet<String> langs)
			throws InProcessPageDBException;

	
//	abstract public void  

}
