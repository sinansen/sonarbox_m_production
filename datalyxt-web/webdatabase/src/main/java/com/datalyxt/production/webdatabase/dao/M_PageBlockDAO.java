package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.BlockMeta;

public interface M_PageBlockDAO extends DataBaseDAO{

	public void insertPageBlock(Block block, BlockMeta blockMeta) throws M_PageBlockDBException;

	public boolean isExsit(Block block, BlockMeta blockMeta)throws M_PageBlockDBException;

	public void deletePageBlocks(List<Long> blockIds)throws M_PageBlockDBException;
}
