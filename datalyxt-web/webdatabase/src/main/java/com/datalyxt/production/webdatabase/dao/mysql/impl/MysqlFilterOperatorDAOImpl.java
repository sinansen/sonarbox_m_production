package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.webdatabase.dao.FilterOperatorDAO;

public class MysqlFilterOperatorDAOImpl implements FilterOperatorDAO {

	Connection connection;

	public MysqlFilterOperatorDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public HashSet<String> getOperators() throws DataBaseException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			String query = " SELECT *  from m_filter_operator ";

			preparedStmt = connection.prepareStatement(query);
			resultSet = preparedStmt.executeQuery();
			HashSet<String> operators = new HashSet<>();

			while (resultSet.next()) {
				String op = resultSet.getString("operator");
				operators.add(op);
			}

			return operators;

		} catch (Exception e) {
			throw new DataBaseException("getOperators failed. "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new DataBaseException("getOperators failed. "
							+ e.getMessage());
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new DataBaseException("getOperators failed. "
							+ sqlex.getMessage());
				}

				preparedStmt = null;
			}
		}
	}

}
