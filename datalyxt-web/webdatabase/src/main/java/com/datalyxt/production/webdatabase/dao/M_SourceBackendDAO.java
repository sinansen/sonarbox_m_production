package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;

public interface M_SourceBackendDAO extends DataBaseDAO {

	public List<M_Source> getSourcesForExecutionByRuleId(long ruleId)
			throws M_SourceDBException;
	

	public List<M_Source> getSourcesByStatus(M_SourceStatus status)
			throws M_SourceDBException;




	public M_Source getSourceById(long sourceId)throws M_SourceDBException;


}
