package com.datalyxt.production.webdatabase.factory;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.exception.db.MySQLUnavailableException;
import com.datalyxt.production.webdatabase.dao.EmailDAO;
import com.datalyxt.production.webdatabase.dao.M_BackendControlDAO;
import com.datalyxt.production.webdatabase.dao.M_DomainBlackListDAO;
import com.datalyxt.production.webdatabase.dao.M_GlobalSearchPageDAO;
import com.datalyxt.production.webdatabase.dao.M_InProcessPagesDAO;
import com.datalyxt.production.webdatabase.dao.M_PageBlockDAO;
import com.datalyxt.production.webdatabase.dao.M_PageBlockLinkDAO;
import com.datalyxt.production.webdatabase.dao.M_PageDAO;
import com.datalyxt.production.webdatabase.dao.M_QT1V1ResultDAO;
import com.datalyxt.production.webdatabase.dao.M_ReceiverDAO;
import com.datalyxt.production.webdatabase.dao.M_RuleBackendDAO;
import com.datalyxt.production.webdatabase.dao.M_RuleExecutionReportDAO;
import com.datalyxt.production.webdatabase.dao.M_SourceBackendDAO;
import com.datalyxt.production.webdatabase.dao.M_SynonymsDAO;
import com.datalyxt.production.webdatabase.dao.M_TraceReportDAO;
import com.datalyxt.production.webdatabase.dao.M_WebScraperPageDAO;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlEmailDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_BackendControlDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_DomainBlackListDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_GlobalSearchPageDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_InProcessPagesDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_PageBlockDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_PageBlockLinkDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_PageDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_QT1V1ResultDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_ReceiverDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_RuleBackendDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_RuleExecutionReportDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_SourceBackendDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_SynonymsDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_TraceReportDAOImpl;
import com.datalyxt.production.webdatabase.dao.mysql.impl.MysqlM_WebScraperPageDAOImpl;

public class MysqlDAOFactory implements DAOFactory {

	protected DataSource dataSource = null;
	protected Connection connection = null;

	protected M_RuleBackendDAO m_RuleDAO = null;
	protected M_SourceBackendDAO m_SourceDAO = null;
	protected M_RuleExecutionReportDAO m_RuleExecutionReportDAO = null;
	protected M_InProcessPagesDAO inProcessPagesDAO;
	protected M_TraceReportDAO traceReportDAO;
	protected M_SynonymsDAO synonymwordsDAO;
	protected M_GlobalSearchPageDAO globalSearchPageDAO;
	protected M_DomainBlackListDAO domainBlackListDAO;
	protected M_WebScraperPageDAO webscrpaerPageDAO;
	protected M_QT1V1ResultDAO qt1v1ResultDAO;
	protected M_PageBlockDAO pageBlockDAO;
	protected M_PageBlockLinkDAO pageBlockLinkDAO;
	protected M_PageDAO pageDAO;
	protected M_ReceiverDAO receiverDAO;
	protected EmailDAO emailDAO;
	protected M_BackendControlDAO backendControlDAO;

	public MysqlDAOFactory(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	protected Connection getConnection() throws DAOFactoryException {
		try {
			if (this.connection == null || this.connection.isClosed()) {

				this.connection = dataSource.getConnection();
			}
		} catch (SQLException e) {
			throw new DAOFactoryException(new MySQLUnavailableException(
					e.getMessage(), e));
		}

		return this.connection;
	}

	public void closeConnection() throws DAOFactoryException {

		try {
			getConnection().close();
		} catch (SQLException e) {
			throw new DAOFactoryException(e);
		}
	}

	@Override
	public M_RuleBackendDAO getM_RuleDAO() throws DAOFactoryException {
		if (this.m_RuleDAO == null) {
			this.m_RuleDAO = new MysqlM_RuleBackendDAOImpl(getConnection());
		}
		try {
			if (this.m_RuleDAO.isConnectionClosed())
				this.m_RuleDAO = new MysqlM_RuleBackendDAOImpl(getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.m_RuleDAO;
	}

	@Override
	public boolean isStorageAvailable() throws DAOFactoryException {
		try {
			if (connection != null && !connection.isClosed())
				return connection.isValid(3);
			else {
				Connection tryconnect = dataSource.getConnection();
				boolean available = tryconnect.isValid(3);
				tryconnect.close();
				return available;
			}
		} catch (SQLException e) {
			throw new DAOFactoryException(new MySQLUnavailableException(
					e.getMessage(), e));
		}
	}

	@Override
	public M_SourceBackendDAO getM_SourceBackendDAO()
			throws DAOFactoryException {
		if (this.m_SourceDAO == null) {
			this.m_SourceDAO = new MysqlM_SourceBackendDAOImpl(getConnection());
		}
		try {
			if (this.m_SourceDAO.isConnectionClosed())
				this.m_SourceDAO = new MysqlM_SourceBackendDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.m_SourceDAO;
	}

	@Override
	public M_RuleExecutionReportDAO getM_RuleExecutionDAO()
			throws DAOFactoryException {
		if (this.m_RuleExecutionReportDAO == null) {
			this.m_RuleExecutionReportDAO = new MysqlM_RuleExecutionReportDAOImpl(
					getConnection());
		}
		try {
			if (this.m_RuleExecutionReportDAO.isConnectionClosed())
				this.m_RuleExecutionReportDAO = new MysqlM_RuleExecutionReportDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.m_RuleExecutionReportDAO;
	}

	@Override
	public M_InProcessPagesDAO getInProcessPagesDAO()
			throws DAOFactoryException {
		if (this.inProcessPagesDAO == null) {
			this.inProcessPagesDAO = new MysqlM_InProcessPagesDAOImpl(
					getConnection());
		}
		try {
			if (this.inProcessPagesDAO.isConnectionClosed())
				this.inProcessPagesDAO = new MysqlM_InProcessPagesDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.inProcessPagesDAO;
	}

	@Override
	public M_TraceReportDAO getTraceReportDAO() throws DAOFactoryException {
		if (this.traceReportDAO == null) {
			this.traceReportDAO = new MysqlM_TraceReportDAOImpl(getConnection());
		}
		try {
			if (this.traceReportDAO.isConnectionClosed())
				this.traceReportDAO = new MysqlM_TraceReportDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.traceReportDAO;
	}

	@Override
	public M_SynonymsDAO getM_SynonymsDAO() throws DAOFactoryException {
		if (this.synonymwordsDAO == null) {
			this.synonymwordsDAO = new MysqlM_SynonymsDAOImpl(getConnection());
		}
		try {
			if (this.synonymwordsDAO.isConnectionClosed())
				this.synonymwordsDAO = new MysqlM_SynonymsDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.synonymwordsDAO;
	}

	@Override
	public M_GlobalSearchPageDAO getGlobalSearchPageDAO()
			throws DAOFactoryException {
		if (this.globalSearchPageDAO == null) {
			this.globalSearchPageDAO = new MysqlM_GlobalSearchPageDAOImpl(
					getConnection());
		}
		try {
			if (this.globalSearchPageDAO.isConnectionClosed())
				this.globalSearchPageDAO = new MysqlM_GlobalSearchPageDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.globalSearchPageDAO;
	}

	@Override
	public M_DomainBlackListDAO getDomainBlackListDAO()
			throws DAOFactoryException {
		if (this.domainBlackListDAO == null) {
			this.domainBlackListDAO = new MysqlM_DomainBlackListDAOImpl(
					getConnection());
		}
		try {
			if (this.domainBlackListDAO.isConnectionClosed())
				this.domainBlackListDAO = new MysqlM_DomainBlackListDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.domainBlackListDAO;
	}

	@Override
	public M_WebScraperPageDAO getM_WebScraperPageDAO()
			throws DAOFactoryException {
		if (this.webscrpaerPageDAO == null) {
			this.webscrpaerPageDAO = new MysqlM_WebScraperPageDAOImpl(
					getConnection());
		}
		try {
			if (this.webscrpaerPageDAO.isConnectionClosed())
				this.webscrpaerPageDAO = new MysqlM_WebScraperPageDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.webscrpaerPageDAO;
	}

	@Override
	public M_QT1V1ResultDAO getM_QT1V1ResultDAO() throws DAOFactoryException {
		if (this.qt1v1ResultDAO == null) {
			this.qt1v1ResultDAO = new MysqlM_QT1V1ResultDAOImpl(getConnection());
		}
		try {
			if (this.qt1v1ResultDAO.isConnectionClosed())
				this.qt1v1ResultDAO = new MysqlM_QT1V1ResultDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.qt1v1ResultDAO;
	}

	@Override
	public M_PageBlockDAO getM_PageBlockDAO() throws DAOFactoryException {
		if (this.pageBlockDAO == null) {
			this.pageBlockDAO = new MysqlM_PageBlockDAOImpl(getConnection());
		}
		try {
			if (this.pageBlockDAO.isConnectionClosed())
				this.pageBlockDAO = new MysqlM_PageBlockDAOImpl(getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.pageBlockDAO;
	}

	@Override
	public M_PageBlockLinkDAO getM_PageBlockLinkDAO()
			throws DAOFactoryException {
		if (this.pageBlockLinkDAO == null) {
			this.pageBlockLinkDAO = new MysqlM_PageBlockLinkDAOImpl(
					getConnection());
		}
		try {
			if (this.pageBlockLinkDAO.isConnectionClosed())
				this.pageBlockLinkDAO = new MysqlM_PageBlockLinkDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.pageBlockLinkDAO;
	}


	@Override
	public M_PageDAO getM_PageDAO() throws DAOFactoryException {
		if (this.pageDAO == null) {
			this.pageDAO = new MysqlM_PageDAOImpl(getConnection());
		}
		try {
			if (this.pageDAO.isConnectionClosed())
				this.pageDAO = new MysqlM_PageDAOImpl(getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.pageDAO;
	}

	@Override
	public M_ReceiverDAO getM_ReceiverDAO() throws DAOFactoryException {
		if (this.receiverDAO == null) {
			this.receiverDAO = new MysqlM_ReceiverDAOImpl(getConnection());
		}
		try {
			if (this.receiverDAO.isConnectionClosed())
				this.receiverDAO = new MysqlM_ReceiverDAOImpl(getConnection());

		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.receiverDAO;
	}

	@Override
	public EmailDAO getEmailDAO() throws DAOFactoryException {
		if (this.emailDAO == null) {
			this.emailDAO = new MysqlEmailDAOImpl(getConnection());
		}
		try {
			if (this.emailDAO.isConnectionClosed())
				this.emailDAO = new MysqlEmailDAOImpl(getConnection());

		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.emailDAO;
	}

	@Override
	public M_BackendControlDAO getBackendControlDAO()
			throws DAOFactoryException {
		if (this.backendControlDAO == null) {
			this.backendControlDAO = new MysqlM_BackendControlDAOImpl(
					getConnection());
		}
		try {
			if (this.backendControlDAO.isConnectionClosed())
				this.backendControlDAO = new MysqlM_BackendControlDAOImpl(
						getConnection());

		} catch (DataBaseException e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return this.backendControlDAO;
	}
}
