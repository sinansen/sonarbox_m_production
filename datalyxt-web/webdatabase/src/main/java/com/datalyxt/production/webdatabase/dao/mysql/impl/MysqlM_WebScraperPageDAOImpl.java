package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.exception.db.InProcessPageDBException;
import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.webdatabase.dao.M_WebScraperPageDAO;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.google.gson.Gson;

public class MysqlM_WebScraperPageDAOImpl implements M_WebScraperPageDAO {

	private Connection connection;

	public MysqlM_WebScraperPageDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public boolean isAllPagesVisited(long fetchFromDBTime,
			PageContentProcessStatus visited)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select page_id from m_webscraper_page where fetch_from_db_time = ? and status != ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, fetchFromDBTime);
			preparedStmt.setString(index++, visited.name());
			ResultSet resultSet = preparedStmt.executeQuery();
			boolean visitedAll = true;
			if (resultSet.next()) {
				visitedAll = false;
			}

			resultSet.close();

			return visitedAll;
		} catch (Exception e) {
			throw new M_WebScraperPageDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public long getLatestVisitedAt(PageContentProcessStatus visited,
			long fetchFromDBTime) throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select max(visited_at) as visited from m_webscraper_page where fetch_from_db_time = ? and status = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, fetchFromDBTime);
			preparedStmt.setString(index++, visited.name());
			ResultSet resultSet = preparedStmt.executeQuery();
			long maxVisited = 0;
			if (resultSet.next()) {
				maxVisited = resultSet.getLong("visited");
			}

			resultSet.close();

			return maxVisited;
		} catch (Exception e) {
			throw new M_WebScraperPageDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void putWebScraperPage(M_WebScraperPage curPage)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert IGNORE  into m_webscraper_page (link_time_md5, link, status, fetch_from_db_time, visited_at, page, source_id, extracted_at, domain, rule_id ) values (md5(?),?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);
			String link_time_md5 = curPage.pageURL.url
					+ curPage.fetchFromDBTime + curPage.ruleId;

			preparedStmt.setString(1, link_time_md5);
			preparedStmt.setString(2, curPage.pageURL.url);
			preparedStmt.setString(3, curPage.status.name());
			preparedStmt.setLong(4, curPage.fetchFromDBTime);
			preparedStmt.setLong(5, curPage.processAt);
			Gson gson = new Gson();
			String content = gson.toJson(curPage, M_WebScraperPage.class);

			preparedStmt.setString(6, content);
			preparedStmt.setLong(7, curPage.sourceId);
			preparedStmt.setLong(8, curPage.extractedAt);
			preparedStmt.setString(9, curPage.pageURL.domain);
			preparedStmt.setLong(10, curPage.ruleId);
			int affectedRows = preparedStmt.executeUpdate();

			if (affectedRows != 0) {

				try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						curPage.pageId = (generatedKeys.getLong(1));
					} else {
						throw new SQLException(
								"Creating user failed, no ID obtained.");
					}
				}
			}

		} catch (Exception e) {
			throw new M_WebScraperPageDBException("putWebScraperPage: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException("putWebScraperPage: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void updateWebScraperPageStatus(long fetchFromDBTime, long pageId,
			PageContentProcessStatus status, long visitedAt)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "update m_webscraper_page set status = ? , visited_at = ? where page_id = ? and fetch_from_db_time = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, status.name());
			preparedStmt.setLong(2, visitedAt);
			preparedStmt.setLong(3, pageId);
			preparedStmt.setLong(4, fetchFromDBTime);

			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_WebScraperPageDBException(
					"updateWebScraperPageStatus: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(
							"updateWebScraperPageStatus: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void updateWebScraperPageContent(M_WebScraperPage curPage)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "update m_webscraper_page set status = ? , visited_at = ?  , page = ? , page_lang = ?  , extracted_at = ? where page_id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setString(1, curPage.status.name());
			preparedStmt.setLong(2, curPage.processAt);
			Gson gson = new Gson();
			String content = gson.toJson(curPage, M_WebScraperPage.class);
			preparedStmt.setString(3, content);
			String lang = null;
			if (curPage.language != null)
				lang = curPage.language;
			preparedStmt.setString(4, lang);
			preparedStmt.setLong(5, curPage.extractedAt);
			preparedStmt.setLong(6, curPage.pageId);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_WebScraperPageDBException(
					"updateWebScraperPageContent: for link: "
							+ curPage.pageURL.url + " : " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(
							"updateWebScraperPageContent: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void deleteWebScraperPages(List<M_WebScraperPage> pages)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		try {
			connection.setAutoCommit(false);
			String queryString = "delete from m_webscraper_page where link_time_md5 = md5(?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			for(M_WebScraperPage page : pages){
				preparedStmt.setString(1, page.getLinkTimeValue());
				preparedStmt.execute();
			}
			connection.commit();

		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				throw new M_WebScraperPageDBException(
						"deleteWebScraperPages rollback failed: "
								+ e1.getMessage(), e1);
			}
			throw new M_WebScraperPageDBException(
					"delete WebScraperPages failed: "
							+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(
							"deleteWebScraperPages: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				throw new M_WebScraperPageDBException(
						"deleteWebScraperPages set auto commit as true failed: "
								+ e.getMessage(), e);
			}
		}

	}

	@Override
	public void putWebScraperPages(List<M_WebScraperPage> nextPages)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;

		int counter = 0;

		try {
			String queryString = "insert IGNORE  into m_webscraper_page (link_time_md5, link, status, fetch_from_db_time, visited_at, page, source_id, extracted_at, domain, rule_id ) values (md5(?),?, ?, ?, ?, ?, ?, ?, ?, ? ) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);

			String errorMsg = "";
			connection.setAutoCommit(false);
			for (M_WebScraperPage curPage : nextPages) {
					String link_time_md5 = curPage.pageURL.url
							+ curPage.fetchFromDBTime + curPage.ruleId;

					preparedStmt.setString(1, link_time_md5);
					preparedStmt.setString(2, curPage.pageURL.url);
					preparedStmt.setString(3, curPage.status.name());
					preparedStmt.setLong(4, curPage.fetchFromDBTime);
					preparedStmt.setLong(5, curPage.processAt);
					Gson gson = new Gson();
					String content = gson.toJson(curPage,
							M_WebScraperPage.class);

					preparedStmt.setString(6, content);
					preparedStmt.setLong(7, curPage.sourceId);
					preparedStmt.setLong(8, curPage.extractedAt);
					preparedStmt.setString(9, curPage.pageURL.domain);
					preparedStmt.setLong(10, curPage.ruleId);
					int affectedRows = preparedStmt.executeUpdate();

					if (affectedRows != 0) {

						try (ResultSet generatedKeys = preparedStmt
								.getGeneratedKeys()) {
							if (generatedKeys.next()) {
								curPage.pageId = (generatedKeys.getLong(1));
							} else {
								throw new SQLException(
										"Creating user failed, no ID obtained.");
							}
						}
					}
					counter = counter + 1;

			
			}
			if (counter != nextPages.size())
				throw new InProcessPageDBException(
						"Inserting new pages contains error: "
								+ (nextPages.size() - counter) + " of "
								+ nextPages.size()
								+ " new In_ProcessPages were NOT inserted !!! "
								+ errorMsg);
			connection.commit();
			
		} catch (Exception e) {
			 try{
				 if(connection!=null)
					 connection.rollback();
		      }catch(SQLException ex){
					throw new M_WebScraperPageDBException("putWebScraperPages rollback failed: "
							+ ex.getMessage(), ex);
		      }//end try
			throw new M_WebScraperPageDBException("putWebScraperPages: "
					+ e.getMessage(), e);

		} finally {
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(
							"putWebScraperPages: " + sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				throw new M_WebScraperPageDBException("putWebScraperPages set auto commit as true failed: "
						+ e.getMessage(), e);
			}

		}

	}

	@Override
	public List<M_WebScraperPage> getNextPageFIFO(long fetchFromDBTime,
			int counter) throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		List<M_WebScraperPage> pages = new ArrayList<>();
		try {
			long now = System.currentTimeMillis();
			String queryString = " select page_id, page from m_webscraper_page where fetch_from_db_time = ? and status = ? and extracted_at <= ? ORDER BY extracted_at ASC limit ?";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setLong(1, fetchFromDBTime);
			preparedStmt.setString(2,
					PageContentProcessStatus.page_created.name());
			preparedStmt.setLong(3, now);
			preparedStmt.setInt(4, counter);
			resultSet = preparedStmt.executeQuery();
			Gson gson = new Gson();
			while (resultSet.next()) {
				String detail = resultSet.getString("page");

				M_WebScraperPage page = gson.fromJson(detail,
						M_WebScraperPage.class);
				page.pageId = resultSet.getLong("page_id");
				pages.add(page);
			}
		} catch (Exception e) {
			throw new M_WebScraperPageDBException("getNextPageFIFO: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_WebScraperPageDBException("getNextPageFIFO: "
							+ e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException("getNextPageFIFO: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		return pages;
	}

	@Override
	public List<M_WebScraperPage> getInProcessingPagesTime(
			long fetchFromDBTime, List<String> inProcessingStatus)
			throws M_WebScraperPageDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			List<M_WebScraperPage> pages = new ArrayList<M_WebScraperPage>();
			if (inProcessingStatus == null || inProcessingStatus.isEmpty())
				return pages;
			String statusCondition = "";
			List<String> condition = new ArrayList<>();
			for (String s : inProcessingStatus) {
				s = " status = '" + s + "' ";
				condition.add(s);
			}
			statusCondition = String.join(" or ", condition);
			statusCondition = " and ( " + statusCondition + " ) ";
			String queryString = " select page_id, status, visited_at from m_webscraper_page where fetch_from_db_time = ?  "
					+ statusCondition;
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setLong(1, fetchFromDBTime);

			resultSet = preparedStmt.executeQuery();
			while (resultSet.next()) {
				M_WebScraperPage p = new M_WebScraperPage();
				p.pageId = resultSet.getLong("page_id");
				p.processAt = resultSet.getLong("visited_at");
				p.status = PageContentProcessStatus.valueOf(resultSet
						.getString("status"));
				pages.add(p);
			}
			return pages;
		} catch (Exception e) {
			throw new M_WebScraperPageDBException("getInProcessingPagesTime: "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_WebScraperPageDBException(
							"getInProcessingPagesTime: " + e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_WebScraperPageDBException(
							"getInProcessingPagesTime: " + sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

}
