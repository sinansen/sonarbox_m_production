package com.datalyxt.production.webdatabase.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;

public interface M_SourceAdminDAO extends DataBaseDAO {
	public long insertSource(M_Source source, long ownerId) throws M_SourceDBException;


	public void changeSourceStatus(long sourceId, M_SourceStatus status, long ownerId)
			throws M_SourceDBException;

	public void updateSourceConfig(M_Source source, long ownerId)throws M_SourceDBException;

	public boolean isSourceExist(String md5NormURL)throws M_SourceDBException;

	public void updateSourceStatusAndFeedback(String nurlMd5,
			M_SourceStatus status, String feedback , long ownerId)throws M_SourceDBException;

	public void modifySource(M_Source oldSource, M_Source newSource , long ownerId)throws M_SourceDBException;

	public M_Source getSourceById(long sourceId)throws M_SourceDBException;

	public M_Source getSourceByNmd5(String sid)throws M_SourceDBException;

	public List<M_Source> getSources(String sql, Object[] params)throws M_SourceDBException;

	public boolean isSourceExist(long id) throws M_SourceDBException ;

	public HashMap<Long, M_Source> getSources(HashSet<Long> sourceIds)throws M_SourceDBException ;
}
