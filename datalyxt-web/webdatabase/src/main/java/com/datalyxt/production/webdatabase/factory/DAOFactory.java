package com.datalyxt.production.webdatabase.factory;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.webdatabase.dao.EmailDAO;
import com.datalyxt.production.webdatabase.dao.M_BackendControlDAO;
import com.datalyxt.production.webdatabase.dao.M_DomainBlackListDAO;
import com.datalyxt.production.webdatabase.dao.M_GlobalSearchPageDAO;
import com.datalyxt.production.webdatabase.dao.M_InProcessPagesDAO;
import com.datalyxt.production.webdatabase.dao.M_PageBlockDAO;
import com.datalyxt.production.webdatabase.dao.M_PageBlockLinkDAO;
import com.datalyxt.production.webdatabase.dao.M_PageDAO;
import com.datalyxt.production.webdatabase.dao.M_QT1V1ResultDAO;
import com.datalyxt.production.webdatabase.dao.M_ReceiverDAO;
import com.datalyxt.production.webdatabase.dao.M_RuleBackendDAO;
import com.datalyxt.production.webdatabase.dao.M_RuleExecutionReportDAO;
import com.datalyxt.production.webdatabase.dao.M_SourceBackendDAO;
import com.datalyxt.production.webdatabase.dao.M_SynonymsDAO;
import com.datalyxt.production.webdatabase.dao.M_TraceReportDAO;
import com.datalyxt.production.webdatabase.dao.M_WebScraperPageDAO;

public interface DAOFactory {
	public void closeConnection() throws DAOFactoryException;

	public boolean isStorageAvailable() throws DAOFactoryException;

	public M_RuleBackendDAO getM_RuleDAO() throws DAOFactoryException;

	public M_SourceBackendDAO getM_SourceBackendDAO() throws DAOFactoryException;


	public M_RuleExecutionReportDAO getM_RuleExecutionDAO()
			throws DAOFactoryException;

	public M_InProcessPagesDAO getInProcessPagesDAO()
			throws DAOFactoryException;

	public M_TraceReportDAO getTraceReportDAO() throws DAOFactoryException;

	public M_SynonymsDAO getM_SynonymsDAO() throws DAOFactoryException;

	public M_GlobalSearchPageDAO getGlobalSearchPageDAO()
			throws DAOFactoryException;

	public M_DomainBlackListDAO getDomainBlackListDAO()
			throws DAOFactoryException;

	public M_WebScraperPageDAO getM_WebScraperPageDAO()
			throws DAOFactoryException;

	public M_QT1V1ResultDAO getM_QT1V1ResultDAO() throws DAOFactoryException;

	public M_PageBlockDAO getM_PageBlockDAO() throws DAOFactoryException;

	public M_PageBlockLinkDAO getM_PageBlockLinkDAO()
			throws DAOFactoryException;

	public M_PageDAO getM_PageDAO() throws DAOFactoryException;
	
	public M_ReceiverDAO getM_ReceiverDAO() throws DAOFactoryException;

	public EmailDAO getEmailDAO()throws DAOFactoryException;

	public M_BackendControlDAO getBackendControlDAO() throws DAOFactoryException;
}
