package com.datalyxt.production.webdatabase.dao;

import com.datalyxt.production.exception.db.M_PageDBException;
import com.datalyxt.production.webscraper.model.runtime.PageChange;
import com.datalyxt.production.webscraper.model.runtime.PageMeta;

public interface M_PageDAO extends DataBaseDAO {

	public void savePageMeta(PageMeta pageMeta) throws M_PageDBException;

	public void savePageChange(PageChange pageChange)
			throws M_PageDBException;

	public boolean isExsit(PageMeta pageMeta)throws M_PageDBException;

	public void saveErrorPageMeta(PageMeta pageMeta)throws M_PageDBException;

}
