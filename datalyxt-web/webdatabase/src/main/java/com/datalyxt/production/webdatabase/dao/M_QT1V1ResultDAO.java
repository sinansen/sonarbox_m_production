package com.datalyxt.production.webdatabase.dao;

import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.production.webscraper.model.action.ActionResult;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

public interface M_QT1V1ResultDAO extends DataBaseDAO {

	public void saveActionResult(ActionResult result)
			throws M_QT1V1ResultDBException;

	public ActionResult getActionResultById(long id)
			throws M_QT1V1ResultDBException;

	public void saveActionResultFile(ActionResultFile actionResultFile)
			throws M_QT1V1ResultDBException;

	public void saveActionResultText(ActionResultText actionResultText)
			throws M_QT1V1ResultDBException;

	public void updateActionResultFile(ActionResultFile actionResultFile)
			throws M_QT1V1ResultDBException;

	public ActionResultFile getActionResultFile(long id)
			throws M_QT1V1ResultDBException;

	public ActionResultText getActionResultText(long id)
			throws M_QT1V1ResultDBException;

	public List<Long> getActionResultTextIdsByResultId(Long resultId)
			throws M_QT1V1ResultDBException;

	public List<Long> getActionResultFileIdsByResultId(Long resultId)
			throws M_QT1V1ResultDBException;

	public List<ActionResultText> getActionResultTextsByResultId(long resultId)
			throws M_QT1V1ResultDBException;

	public List<ActionResultText> getActionResultTextByIds(
			HashSet<Long> messageIds) throws M_QT1V1ResultDBException;

	public List<ActionResultFile> getActionResultFileByIds(
			HashSet<Long> attachementFileIds) throws M_QT1V1ResultDBException;

	public List<ActionResult> getNewResults(int limit, boolean allParams)
			throws M_QT1V1ResultDBException;

	public List<String> getActionResultUrlsById(HashSet<Long> resultIds)
			throws M_QT1V1ResultDBException;

	public void updateValidation(long id, int code, boolean isFile,
			boolean isSystem) throws M_QT1V1ResultDBException;

	public void deleteActionResult(M_WebScraperPage inProcessPage)
			throws M_QT1V1ResultDBException;

}
