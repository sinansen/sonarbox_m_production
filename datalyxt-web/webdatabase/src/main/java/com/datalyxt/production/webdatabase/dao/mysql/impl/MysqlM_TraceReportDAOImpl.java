package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.TraceReportDBException;
import com.datalyxt.production.execution.monitoring.model.M_TraceReport;
import com.datalyxt.production.webdatabase.dao.M_TraceReportDAO;

public class MysqlM_TraceReportDAOImpl implements M_TraceReportDAO {
	private Connection connection;

	public MysqlM_TraceReportDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public synchronized void insertTraceReport(M_TraceReport report)
			throws TraceReportDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_rule_trace_report (scan_id, supervisor, rule_id, source_id, url, type, time, message, error) values ( ?, ?, ?,?,?, ?, ? ,?, ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setLong(1, report.getScanId());
			preparedStmt.setString(2, report.getSupervisor());
			preparedStmt.setLong(3, report.ruleId);
			preparedStmt.setLong(4, report.sourceId);
			preparedStmt.setString(5, report.url);
			preparedStmt.setString(6, report.type.name());
			preparedStmt
					.setTimestamp(7, new Timestamp(report.time));
			preparedStmt.setString(8, report.message);
			preparedStmt.setString(9, report.error);
			preparedStmt.execute();
		} catch (Exception e) {
			throw new TraceReportDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new TraceReportDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
	}
}
