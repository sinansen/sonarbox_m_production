package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_PageBlockDBException;
import com.datalyxt.production.webdatabase.dao.M_PageBlockDAO;
import com.datalyxt.production.webscraper.model.config.BlockType;
import com.datalyxt.production.webscraper.model.runtime.Block;
import com.datalyxt.production.webscraper.model.runtime.BlockMeta;
import com.datalyxt.production.webscraper.model.runtime.ContentBlock;
import com.google.gson.Gson;

public class MysqlM_PageBlockDAOImpl implements M_PageBlockDAO {

	private Connection connection;

	public MysqlM_PageBlockDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public void insertPageBlock(Block absBlock, BlockMeta blockMeta)
			throws M_PageBlockDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_page_block (n_url, created_at, n_url_md5,  block_structure_hash, block_text_hash, block_html_hash, block_structure, block_text, block_html, source_id, rule_id) values (?, ?,?,?,?,?,?,?,?,?,?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);
			if (absBlock.type.equals(BlockType.datasetBlock)) {

				int index = 1;
				preparedStmt.setString(index++, blockMeta.pageUrl);
				preparedStmt.setLong(index++, System.currentTimeMillis());
				String md5 = DigestUtils.md5Hex( blockMeta.pageUrl);
				preparedStmt.setString(index++, md5);
				preparedStmt.setInt(index++, 0);
				preparedStmt.setInt(index++, absBlock.blockTextHash);
				preparedStmt.setInt(index++, 0);
				preparedStmt.setString(index++, "");
				preparedStmt.setString(index++, absBlock.blockText);
				preparedStmt.setString(index++, "");

				
				preparedStmt.setLong(index++, blockMeta.sourceId);
				preparedStmt.setLong(index++, blockMeta.ruleId);


			} else {
				ContentBlock block = (ContentBlock) absBlock;

				int index = 1;
				preparedStmt.setString(index++, blockMeta.pageUrl);
				preparedStmt.setLong(index++, System.currentTimeMillis());
				String md5 = DigestUtils.md5Hex( blockMeta.pageUrl);
				preparedStmt.setString(index++, md5);
				preparedStmt.setInt(index++, block.blockStructureHash);
				preparedStmt.setInt(index++, block.blockTextHash);
				preparedStmt.setInt(index++, block.blockHtmlHash);
				preparedStmt.setString(index++, block.blockStructure);
				preparedStmt.setString(index++, block.blockText);
				preparedStmt.setString(index++, block.blockHtml);
			
				preparedStmt.setLong(index++, blockMeta.sourceId);
				preparedStmt.setLong(index++, blockMeta.ruleId);


			}
			int affectedRows = preparedStmt.executeUpdate();

			if (affectedRows != 0) {

				try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						absBlock.blockId = (generatedKeys.getLong(1));
					} else {
						throw new SQLException(
								"insert block failed.");
					}
				}
			}

		} catch (Exception e) {
			throw new M_PageBlockDBException("insertPageBlock failed: "+e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageBlockDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public boolean isExsit(Block absBlock, BlockMeta blockMeta)
			throws M_PageBlockDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {

			if (absBlock.type.equals(BlockType.datasetBlock)) {

				String queryString = "select id from m_page_block where  block_text_hash = ? and n_url_md5 = ? ";
				// create the mysql insert preparedstatement
				preparedStmt = connection.prepareStatement(queryString);
				int index = 1;
				preparedStmt.setInt(index++, absBlock.blockTextHash);
				String md5 = DigestUtils.md5Hex( blockMeta.pageUrl);
				preparedStmt.setString(index++, md5);
			} else {
				ContentBlock block = (ContentBlock) absBlock;
				String queryString = "select id from m_page_block where block_structure_hash = ? and block_text_hash = ? and block_html_hash = ? and n_url_md5 = ? ";
				// create the mysql insert preparedstatement
				preparedStmt = connection.prepareStatement(queryString);
				int index = 1;
				preparedStmt.setInt(index++, block.blockStructureHash);
				preparedStmt.setInt(index++, block.blockTextHash);
				preparedStmt.setInt(index++, block.blockHtmlHash);
				String md5 = DigestUtils.md5Hex( blockMeta.pageUrl);
				preparedStmt.setString(index++, md5);
			}
			resultSet = preparedStmt.executeQuery();
			boolean isExsit = false;
			if (resultSet.next())
				isExsit = true;

			resultSet.close();

			return isExsit;
		} catch (Exception e) {
			throw new M_PageBlockDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageBlockDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void deletePageBlocks(List<Long> blockIds) throws M_PageBlockDBException {
		PreparedStatement preparedStmt = null;
		try {
			connection.setAutoCommit(false);
			String queryString = "delete from m_page_block where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			for(Long bid : blockIds){
				preparedStmt.setLong(1, bid);
				preparedStmt.execute();
			}
			connection.commit();

		} catch (Exception e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				throw new M_PageBlockDBException(
						"deletePageBlocks rollback failed: "
								+ e1.getMessage(), e1);
			}
			throw new M_PageBlockDBException(
					" deletePageBlocks failed: "
							+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageBlockDBException(
							"deletePageBlocks: "
									+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				throw new M_PageBlockDBException(
						"deletePageBlocks set auto commit as true failed: "
								+ e.getMessage(), e);
			}
		}

		
	}

}
