package com.datalyxt.production.webdatabase.dao;

import java.util.HashSet;

import com.datalyxt.exception.db.DataBaseException;

public interface FilterOperatorDAO extends DataBaseDAO {
	public HashSet<String> getOperators() throws DataBaseException;
}