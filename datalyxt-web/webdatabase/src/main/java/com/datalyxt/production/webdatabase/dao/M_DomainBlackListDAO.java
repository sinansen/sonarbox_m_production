package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.webmodel.source.Blacklist;

public interface M_DomainBlackListDAO extends DataBaseDAO {
	
	public void delete(long id) throws M_DomainBlackListDBException;
	
	public void update(long id, String url) throws M_DomainBlackListDBException;
	
	public long insert(String url) throws M_DomainBlackListDBException;

	public List<Blacklist> getDomainBlackList()
			throws M_DomainBlackListDBException;
	
}
