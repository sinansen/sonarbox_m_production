package com.datalyxt.production.webdatabase.dao.sqlite.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.webdatabase.dao.M_BlockFilterDAO;
import com.datalyxt.production.webscraper.model.filter.BlockData;

public class SqliteM_BlockFilterDAOImpl implements M_BlockFilterDAO {

	private Connection connection;

	public SqliteM_BlockFilterDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (Exception e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	public void createDB() throws DataBaseException {
		try {
			Statement stmt = connection.createStatement();
			stmt.executeUpdate("DROP TABLE IF EXISTS datasetBlocks;");
			stmt.executeUpdate("CREATE TABLE datasetBlocks ( ID INT PRIMARY KEY , urlHashcode CHAR(32) , blockId bigint, name VARCHAR(255), textValue TEXT, numValue bigint);");
			stmt.close();
		} catch (Exception e) {
			throw new DataBaseException("create DB failed " + e.getMessage(), e);
		}
	}

	public void clearDB(String pageURLHashcode) throws DataBaseException {
		try {
			PreparedStatement ps = connection
					.prepareStatement("delete from datasetBlocks where urlHashcode = ?");

			ps.setString(1, pageURLHashcode);

			ps.execute();

			ps.close();
		} catch (Exception e) {
			throw new DataBaseException("clear DB failed " + e.getMessage(), e);
		}
	}

	public void saveBlock(List<BlockData> blockdata, String urlHashcode)
			throws DataBaseException {
		try {

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO datasetBlocks  (urlHashcode, blockId, name, textValue, numValue)  VALUES (?, ?, ?, ?, ?);");

			for (BlockData data : blockdata) {
				int index = 1;
				ps.setString(index++, urlHashcode);
				ps.setLong(index++, data.blockId);
				ps.setString(index++, data.name);
				ps.setString(index++, data.textValue);
				ps.setLong(index++, data.numValue);
				ps.addBatch();
			}

			connection.setAutoCommit(false);
			ps.executeBatch();
			connection.setAutoCommit(true);

			ps.close();
		} catch (Exception e) {
			throw new DataBaseException("saveBlock in mem DB failed "
					+ e.getMessage(), e);
		}
	}

	public List<Long> queryNum4Include(String name, String operator,
			long value, String urlHashcode) throws DataBaseException {
		List<Long> blockIds = new ArrayList<Long>();
		try {
			String base = "SELECT blockId FROM datasetBlocks where urlHashcode = ? and name = ? and numValue ";
			String query = base + operator + " ?; ";
			PreparedStatement ps = connection.prepareStatement(query);

			int index = 1;
			ps.setString(index++, urlHashcode);
			ps.setString(index++, name);
			ps.setLong(index++, value);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				blockIds.add(rs.getLong("blockId"));
			}
			rs.close();

		} catch (Exception e) {
			throw new DataBaseException("queryTime4Include in mem DB failed "
					+ e.getMessage(), e);
		}
		return blockIds;
	}

	public List<Long> queryText4Include(String name, String operator,
			String value, String urlHashcode) throws DataBaseException {
		List<Long> blockIds = new ArrayList<Long>();
		try {
			String base = "SELECT blockId FROM datasetBlocks where urlHashcode = ? and name = ? and textValue ";
			String query = base + operator + " ?; ";
			PreparedStatement ps = connection.prepareStatement(query);

			int index = 1;
			ps.setString(index++, urlHashcode);
			ps.setString(index++, name);
			ps.setString(index++, value);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				blockIds.add(rs.getLong("blockId"));
			}
			rs.close();

		} catch (Exception e) {
			throw new DataBaseException("queryText4Include in mem DB failed "
					+ e.getMessage(), e);
		}
		return blockIds;
	}

	@Override
	public void getAll() {
		try {
			String query = "SELECT * FROM datasetBlocks where textValue LIKE ? ;";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, "%Ortner%");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("name = " + rs.getString("name"));
				System.out.println("textValue = " + rs.getString("textValue"));
			}
			rs.close();
			connection.close();
		} catch (SQLException e) {
			System.err.println("Couldn't handle DB-Query");
			e.printStackTrace();
		}

	}

}
