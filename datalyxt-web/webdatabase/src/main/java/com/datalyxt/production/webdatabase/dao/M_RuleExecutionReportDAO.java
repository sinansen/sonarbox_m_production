package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.execution.monitoring.model.M_RuleExecutionReport;
import com.datalyxt.production.execution.monitoring.model.RuleExecutionStatus;

public interface M_RuleExecutionReportDAO extends DataBaseDAO{
	public void addM_RuleExecutionReport(M_RuleExecutionReport report)
			throws M_RuleExecutionDBException;

	public List<M_RuleExecutionReport> getLastExecutionReports(long timestamp) throws M_RuleExecutionDBException;

	public void setRuleExecutionFinished(M_RuleExecutionReport reports) throws M_RuleExecutionDBException;

	public void setRuleExecutionStart(M_RuleExecutionReport report) throws M_RuleExecutionDBException;

	public void updateRuleExecutionStatus(long fetchTimeFromDB,
			RuleExecutionStatus sourceScanFinished)  throws M_RuleExecutionDBException;
}
