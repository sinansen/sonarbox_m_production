package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.webdatabase.dao.M_BackendControlDAO;

public class MysqlM_BackendControlDAOImpl implements M_BackendControlDAO {

	private Connection connection;

	public MysqlM_BackendControlDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public int getCurrentCommand() throws DataBaseException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			String query = " SELECT command  from m_bk_control ";

			preparedStmt = connection.prepareStatement(query);
			resultSet = preparedStmt.executeQuery();
			List<Integer> commands = new ArrayList<>();

			while (resultSet.next()) {
				int command = resultSet.getInt("command");
				commands.add(command);
			}
			if (commands.size() != 1)
				throw new DataBaseException("only one command is allowed!!! ");

			return commands.get(0);

		} catch (Exception e) {
			throw new DataBaseException("getCurrentCommand failed. "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new DataBaseException("getCurrentCommand failed. "
							+ e.getMessage());
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new DataBaseException("getCurrentCommand failed. "
							+ sqlex.getMessage());
				}

				preparedStmt = null;
			}
		}
	}

}
