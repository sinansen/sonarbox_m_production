package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_SynonymsDBException;
import com.datalyxt.production.webdatabase.dao.M_SynonymsDAO;
import com.datalyxt.production.webmodel.rule.M_Synonym;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MysqlM_SynonymsDAOImpl implements M_SynonymsDAO {
	private Connection connection;

	public MysqlM_SynonymsDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public HashMap<String, M_Synonym> loadAllSynonyms()
			throws M_SynonymsDBException {
		HashMap<String, M_Synonym> synonystable = new HashMap<String, M_Synonym>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select * from m_synonyms ";

			preparedStmt = connection.prepareStatement(queryString);

			resultSet = preparedStmt.executeQuery();

			Gson gson = new Gson();

			while (resultSet.next()) {
				M_Synonym m_synonym = new M_Synonym();
				m_synonym.indexword = resultSet.getString("indexword");
				String synonymsJson = resultSet.getString("synonyms");
				
				m_synonym.indexword_stem = resultSet.getString("indexword_stem");
				
				String synonym_stemsJson = resultSet.getString("synonym_stems");

				Type type = new TypeToken<HashSet<String>>() {
				}.getType();
				
				m_synonym.synonyms = gson.fromJson(synonymsJson, type);

				m_synonym.synonym_stems = gson.fromJson(synonym_stemsJson, type);
				
				synonystable.put(m_synonym.indexword, m_synonym);

			}
		} catch (Exception e) {
			throw new M_SynonymsDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_SynonymsDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_SynonymsDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return synonystable;

	}

}
