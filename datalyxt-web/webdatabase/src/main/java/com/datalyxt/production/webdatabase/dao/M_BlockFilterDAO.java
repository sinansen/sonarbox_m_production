package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.webscraper.model.filter.BlockData;

public interface M_BlockFilterDAO extends DataBaseDAO {

	public void createDB() throws DataBaseException;

	public void clearDB(String pageURLHashcode) throws DataBaseException;

	public void saveBlock(List<BlockData> blockdata, String urlHashcode)
			throws DataBaseException;

	public List<Long> queryNum4Include(String name, String operator , long value, String urlHashcode)
			throws DataBaseException;

	public List<Long> queryText4Include(String name, String operator, String value,
			String urlHashcode) throws DataBaseException;

	public void getAll();

}
