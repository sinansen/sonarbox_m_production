package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_RuleExecutionDBException;
import com.datalyxt.production.execution.monitoring.model.M_RuleExecutionReport;
import com.datalyxt.production.execution.monitoring.model.RuleExecutionStatus;
import com.datalyxt.production.webdatabase.dao.M_RuleExecutionReportDAO;

public class MysqlM_RuleExecutionReportDAOImpl implements
		M_RuleExecutionReportDAO {

	private Connection connection;

	public MysqlM_RuleExecutionReportDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public synchronized void addM_RuleExecutionReport(
			M_RuleExecutionReport report) throws M_RuleExecutionDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_rules_execution_report ( rule_id,  scan_id, start_time, finish_time, status,  source_id) values (?, ?, ?,?,?, ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);

			preparedStmt.setLong(1, report.ruleId);

			preparedStmt.setLong(2, (report.fetchFromDbTime));

			preparedStmt.setTimestamp(3, new Timestamp(
					report.executionStartTime));

			preparedStmt.setTimestamp(4, new Timestamp(
					report.executionFinishTime));

			preparedStmt.setString(5, report.status.name());
			
			preparedStmt.setLong(6, report.sourceId);

			int affectedRows = preparedStmt.executeUpdate();

			if (affectedRows != 0) {

				try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						report.id = (generatedKeys.getLong(1));
					} else {
						throw new SQLException(
								"insert rule execution report failed.");
					}
				}
			}

		} catch (Exception e) {
			throw new M_RuleExecutionDBException("addM_RuleExecutionReport "+e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleExecutionDBException("addM_RuleExecutionReport "+sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public List<M_RuleExecutionReport> getLastExecutionReports(long timestamp)
			throws M_RuleExecutionDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		List<M_RuleExecutionReport> stats = new ArrayList<M_RuleExecutionReport>();

		try {

			// the mysql insert statement
			String queryString = " SELECT MAX(scan_id) AS fetch_from_db_time, any_value(status)   AS execution_status   FROM m_rules_execution_report where scan_id >= ? ";
			preparedStmt = connection.prepareStatement(queryString);
			preparedStmt.setLong(1, timestamp);
			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				M_RuleExecutionReport report = new M_RuleExecutionReport();
				// report.ruleId = resultSet.getLong("rule_id");
				report.fetchFromDbTime = resultSet
						.getLong("fetch_from_db_time");
				String s = resultSet.getString("execution_status");
				if (s != null) {
					report.status = RuleExecutionStatus.valueOf(s);
					stats.add(report);
				}
			}
			return stats;
		} catch (Exception e) {
			throw new M_RuleExecutionDBException("getLastExecutionReports "+e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleExecutionDBException("getLastExecutionReports "+e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleExecutionDBException("getLastExecutionReports "+sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void setRuleExecutionFinished(M_RuleExecutionReport report)
			throws M_RuleExecutionDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {

			// the mysql insert statement
			String updateString = " update m_rules_execution_report set finish_time = ? where rule_id = ? and scan_id = ?";
			preparedStmt = connection.prepareStatement(updateString);
			preparedStmt.setTimestamp(1, new Timestamp(
					report.executionFinishTime));
			preparedStmt.setLong(2, report.ruleId);
			preparedStmt.setLong(3, (report.fetchFromDbTime));

			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_RuleExecutionDBException("setRuleExecutionFinished "+e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleExecutionDBException("setRuleExecutionFinished "+e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleExecutionDBException("setRuleExecutionFinished "+sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void setRuleExecutionStart(M_RuleExecutionReport report)
			throws M_RuleExecutionDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {

			// the mysql insert statement
			String updateString = " update m_rules_execution_report set start_time = ? where rule_id = ? and scan_id = ? and start_time = finish_time ";
			preparedStmt = connection.prepareStatement(updateString);
			preparedStmt.setTimestamp(1, new Timestamp(
					report.executionStartTime));
			preparedStmt.setLong(2, report.ruleId);
			preparedStmt.setLong(3, (report.fetchFromDbTime));

			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_RuleExecutionDBException("setRuleExecutionStart "+e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleExecutionDBException("setRuleExecutionStart "+e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleExecutionDBException("setRuleExecutionStart "+sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void updateRuleExecutionStatus(long fetchTimeFromDB,
			RuleExecutionStatus status)
			throws M_RuleExecutionDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {

			// the mysql insert statement
			String updateString = " update m_rules_execution_report set status = ?, finish_time = ? where scan_id = ? and status = ? ";
			preparedStmt = connection.prepareStatement(updateString);
			preparedStmt.setString(1, status.name());
			preparedStmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			preparedStmt.setLong(3, fetchTimeFromDB);
			preparedStmt.setString(4, RuleExecutionStatus.execution_started.name());
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_RuleExecutionDBException("In method <updateRuleExecutionStatus> "+e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleExecutionDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleExecutionDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

		
	}

}
