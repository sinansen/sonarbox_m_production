package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_DomainBlackListDBException;
import com.datalyxt.production.webdatabase.dao.M_DomainBlackListDAO;
import com.datalyxt.production.webmodel.source.Blacklist;

public class MysqlM_DomainBlackListDAOImpl implements M_DomainBlackListDAO {

	private Connection connection;

	public MysqlM_DomainBlackListDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public long insert(String url) throws M_DomainBlackListDBException {
		PreparedStatement preparedStmt = null;
		long id = 0;
		try {

			String queryString = "insert into m_domain_blacklist (linkId, link, domain) values ( md5(?),?,?) ";

			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);

			preparedStmt.setString(1, url);
			preparedStmt.setString(2, url);

			int affectedRows = preparedStmt.executeUpdate();

			if (affectedRows != 0) {

				try (ResultSet generatedKeys = preparedStmt.getGeneratedKeys()) {
					if (generatedKeys.next()) {
						id = (generatedKeys.getLong(1));
					} else {
						throw new SQLException(
								"Creating user failed, no ID obtained.");
					}
				}
			}
			return id;
		} catch (Exception e) {
			throw new M_DomainBlackListDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_DomainBlackListDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public List<Blacklist> getDomainBlackList()
			throws M_DomainBlackListDBException {
		List<Blacklist> links = new ArrayList<>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select url, id from m_link_blacklist";

			preparedStmt = connection.prepareStatement(queryString);

			resultSet = preparedStmt.executeQuery();
			while (resultSet.next()) {
				Blacklist bl = new Blacklist();
				bl.url = resultSet.getString("url");
				bl.id = resultSet.getLong("id");
				links.add(bl);
			}
		} catch (Exception e) {
			throw new M_DomainBlackListDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_DomainBlackListDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_DomainBlackListDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

		return links;
	}

	@Override
	public void delete(long id) throws M_DomainBlackListDBException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(long id, String url) throws M_DomainBlackListDBException {
		// TODO Auto-generated method stub
		
	}

}
