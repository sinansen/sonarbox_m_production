package com.datalyxt.production.webdatabase.factory;

import java.sql.Connection;
import java.sql.DriverManager;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.webdatabase.dao.M_BlockFilterDAO;
import com.datalyxt.production.webdatabase.dao.sqlite.impl.SqliteM_BlockFilterDAOImpl;

public class SqliteDAOFactory implements RuntimeDAOFactory {

	protected M_BlockFilterDAO blockFilterDAO;

	private static SqliteDAOFactory dbcontroller = null;
	private static Connection connection;
	private static final String DB_PATH = System.getProperty("user.home") + "/"
			+ "qt1v1block.db";

	private SqliteDAOFactory() throws DAOFactoryException {
		initDBConnection();
	}

	public static SqliteDAOFactory getInstance() throws DAOFactoryException {
		if (dbcontroller == null) {
			dbcontroller = new SqliteDAOFactory();
		}
		return dbcontroller;
	}

	private void initDBConnection() throws DAOFactoryException {
		try {

			Class.forName("org.sqlite.JDBC");

			if (connection != null)
				return;

			connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);

		} catch (Exception e) {
			throw new DAOFactoryException(e);
		}

	}

	protected Connection getConnection() throws DAOFactoryException {
		try {
			if (connection == null || connection.isClosed()) {

				connection = DriverManager.getConnection("jdbc:sqlite:"
						+ DB_PATH);
			}
		} catch (Exception e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}
		return connection;
	}

	@Override
	public void closeConnection() throws DAOFactoryException {
		try {
			if (!connection.isClosed() && connection != null) {
				connection.close();
			}
		} catch (Exception e) {
			throw new DAOFactoryException(e.getMessage(), e);
		}

	}


	@Override
	public M_BlockFilterDAO getM_BlockFilterDAO() throws DAOFactoryException {
		if (this.blockFilterDAO == null) {
			this.blockFilterDAO = new SqliteM_BlockFilterDAOImpl(
					getConnection());
		}
		try {
			if (this.blockFilterDAO.isConnectionClosed())
				this.blockFilterDAO = new SqliteM_BlockFilterDAOImpl(
						getConnection());
		} catch (DataBaseException e) {
			throw new DAOFactoryException("getM_BlockFilterDAO "+e.getMessage(), e);
		}
		return this.blockFilterDAO;
	}

}
