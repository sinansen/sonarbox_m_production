package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_ActivityDBException;
import com.datalyxt.production.log.M_Activity;

public interface M_ActivityAdminDAO extends DataBaseDAO {

	M_Activity insert(M_Activity activity) throws M_ActivityDBException;

	M_Activity getById(long id) throws M_ActivityDBException;

	List<M_Activity> list(String sql, Object[] parameter)
			throws M_ActivityDBException;

}
