package com.datalyxt.production.webdatabase.factory;

import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class MyDataSourceFactory {

	protected DataSource dataSource;

	public static DataSource getMySQLDataSource(Properties properties) throws SQLException {
		MysqlDataSource mysqlDS = null;
		mysqlDS = new MysqlDataSource();
		mysqlDS.setUrl(properties.getProperty("MYSQL_DB_URL"));
		mysqlDS.setUser(properties.getProperty("MYSQL_DB_USERNAME"));
		mysqlDS.setPassword(properties.getProperty("MYSQL_DB_PASSWORD"));
		return mysqlDS;
	}

}
