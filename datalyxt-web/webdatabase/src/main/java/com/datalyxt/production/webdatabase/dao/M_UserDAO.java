package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_UserDBException;
import com.datalyxt.production.user.M_User;

public interface M_UserDAO {
	public void saveUser(M_User user) throws M_UserDBException;

	public void updateUser(M_User user) throws M_UserDBException;

	public boolean isValidUser(M_User user) throws M_UserDBException;

	public boolean isUserExist(M_User user) throws M_UserDBException;

	public List<String> getAllUserNames() throws M_UserDBException;

	public  M_User findByUserEmail(String email) throws M_UserDBException;

}
