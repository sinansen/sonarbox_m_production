package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_GlobalSearchPageDBException;
import com.datalyxt.production.globalsearch.ranking.RankingReport;
import com.datalyxt.production.webdatabase.dao.M_GlobalSearchPageDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MysqlM_GlobalSearchPageDAOImpl implements M_GlobalSearchPageDAO {

	private Connection connection;
	private Gson gson;

	public MysqlM_GlobalSearchPageDAOImpl(Connection connection) {
		this.connection = connection;
		gson = new GsonBuilder().disableHtmlEscaping().create();
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public void insertRankingReport(RankingReport rankingReport)
			throws M_GlobalSearchPageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_ranking_reports ( source_id, keyword, fetch_from_db_time, report, lang, created_at) values (?,?,?,?, ?, ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, rankingReport.sourceId);
			preparedStmt.setString(2, rankingReport.keyword);
			preparedStmt.setLong(3, rankingReport.fetchFromDBTime);
			String content_json = gson.toJson(rankingReport,
					RankingReport.class);

			preparedStmt.setString(4, content_json);
			preparedStmt.setString(5, rankingReport.lang);
			preparedStmt.setLong(6, rankingReport.timestamp);
			preparedStmt.executeUpdate();

		} catch (Exception e) {
			throw new M_GlobalSearchPageDBException("sourceId: "+rankingReport.sourceId+"  keyword: "+rankingReport.keyword+"  fetchFromDBTime: "+rankingReport.fetchFromDBTime+" -- "+e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_GlobalSearchPageDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public List<RankingReport> getRankingReports(boolean since,
			long fetchFromDBTime) throws M_GlobalSearchPageDBException {
		List<RankingReport> reports = new ArrayList<RankingReport>();

		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select report from m_ranking_reports where fetch_from_db_time >= ? ";
			if (!since)
				queryString = queryString.replace(">=", "=");
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setLong(1, fetchFromDBTime);
			ResultSet resultSet = preparedStmt.executeQuery();
			while (resultSet.next()) {
				String json = resultSet.getString("report");
				RankingReport report = gson.fromJson(json, RankingReport.class);
				reports.add(report);
			}
			resultSet.close();
		} catch (Exception e) {
			throw new M_GlobalSearchPageDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_GlobalSearchPageDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
		return reports;
	}

}
