package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_QT1V1ResultDBException;
import com.datalyxt.production.webdatabase.dao.M_QT1V1ResultDAO;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;
import com.datalyxt.production.webscraper.model.action.ActionResult;
import com.datalyxt.production.webscraper.model.action.ActionResultFile;
import com.datalyxt.production.webscraper.model.action.ActionResultText;

public class MysqlM_QT1V1ResultDAOImpl implements M_QT1V1ResultDAO {

	private Connection connection;

	public MysqlM_QT1V1ResultDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public void saveActionResult(ActionResult result)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_qt1v1_result (source_id, rule_id, n_url, created_at, scan_id, hop) values (?, ? , ? ,? , ?, ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			preparedStmt.setLong(index++, result.sourceId);
			preparedStmt.setLong(index++, result.ruleId);
			preparedStmt.setString(index++, result.normalizedURL);
			preparedStmt.setLong(index++, result.createdAt);
			preparedStmt.setLong(index++, result.scanId);
			preparedStmt.setInt(index++, result.hop);

			preparedStmt.execute();

			ResultSet rs = preparedStmt.getGeneratedKeys();
			if (rs.next()) {
				result.id = rs.getLong(1);
			}
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("saveActionResult failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public ActionResult getActionResultById(long id)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select * from m_qt1v1_result where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, id);
			ResultSet resultSet = preparedStmt.executeQuery();
			ActionResult result = new ActionResult();
			if (resultSet.next()) {
				result.sourceId = resultSet.getLong("source_id");
				result.scanId = resultSet.getLong("scan_id");
				result.id = id;
				result.createdAt = resultSet.getLong("created_at");
				result.ruleId = resultSet.getLong("rule_id");
				result.normalizedURL = resultSet.getString("n_url");
			}
			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void saveActionResultFile(ActionResultFile result)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_qt1v1_file (id, path, name, result_id, status, host, size, type, created_at, d_name, host_path_md5) values (?, ?, ? , ? ,? , ?, ? ,? ,?, ? , ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, result.id);
			String path = String.join("/", result.path);
			preparedStmt.setString(index++, path);
			preparedStmt.setString(index++, result.name);
			preparedStmt.setLong(index++, result.resultId);
			preparedStmt.setInt(index++, result.status);
			preparedStmt.setString(index++, result.host);
			preparedStmt.setInt(index++, result.size);
			preparedStmt.setInt(index++, result.type);
			preparedStmt.setLong(index++, result.createdAt);
			preparedStmt.setString(index++, result.dName);
			String hostPathMd5 = DigestUtils.md5Hex(result.host + ":" + path);
			preparedStmt.setString(index++, hostPathMd5);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("saveActionResultFile failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void updateActionResultFile(ActionResultFile resultFile)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String update = "update m_qt1v1_file set status = ? where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(update);
			int index = 1;
			preparedStmt.setInt(index++, resultFile.status);
			preparedStmt.setLong(index++, resultFile.resultId);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"updateActionResultFile failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public ActionResultFile getActionResultFile(long id)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select * from m_qt1v1_file where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, id);
			ResultSet resultSet = preparedStmt.executeQuery();
			ActionResultFile result = new ActionResultFile();
			// id, path, name, result_id, status, host, size, type, created_at
			if (resultSet.next()) {
				result.id = resultSet.getLong("id");
				result.createdAt = resultSet.getLong("created_at");
				result.resultId = resultSet.getLong("result_id");
				result.status = resultSet.getInt("status");
				result.host = resultSet.getString("host");
				String path = resultSet.getString("path");
				result.path = path.split("/");
				result.name = resultSet.getString("name");
				result.size = resultSet.getInt("size");
				result.type = resultSet.getInt("type");
				result.dName = resultSet.getString("d_name");
			}
			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"get action result file failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void saveActionResultText(ActionResultText result)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_qt1v1_text (type, content, block_id, result_id, created_at) values (?, ? , ? ,? , ? ) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setString(index++, result.type);
			preparedStmt.setString(index++, result.content);
			preparedStmt.setLong(index++, result.blockId);
			preparedStmt.setLong(index++, result.resultId);

			preparedStmt.setLong(index++, result.createdAt);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("saveActionResultText failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public ActionResultText getActionResultText(long id)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String update = "select * from m_qt1v1_text where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(update);
			int index = 1;
			preparedStmt.setLong(index++, id);
			ResultSet resultSet = preparedStmt.executeQuery();
			ActionResultText result = new ActionResultText();

			if (resultSet.next()) {
				result.id = resultSet.getLong("id");
				result.createdAt = resultSet.getLong("created_at");
				result.resultId = resultSet.getLong("result_id");
				result.blockId = resultSet.getLong("block_id");
				result.content = resultSet.getString("content");
				result.type = resultSet.getString("type");
			}
			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("getActionResultText failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public List<Long> getActionResultTextIdsByResultId(Long resultId)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String query = "select id from m_qt1v1_text where result_id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);
			int index = 1;
			preparedStmt.setLong(index++, resultId);
			ResultSet resultSet = preparedStmt.executeQuery();
			List<Long> result = new ArrayList<Long>();

			while (resultSet.next()) {
				long id = resultSet.getLong("id");
				result.add(id);
			}

			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"getActionResultTextIds failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public List<Long> getActionResultFileIdsByResultId(Long resultId)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String query = "select id from m_qt1v1_file where result_id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);
			int index = 1;
			preparedStmt.setLong(index++, resultId);
			ResultSet resultSet = preparedStmt.executeQuery();
			List<Long> result = new ArrayList<Long>();

			while (resultSet.next()) {
				long id = resultSet.getLong("id");
				result.add(id);
			}

			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"getActionResultFileIds failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public List<ActionResultText> getActionResultTextsByResultId(long resultId)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {

			String query = "select * from m_qt1v1_text where result_id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);
			int index = 1;
			preparedStmt.setLong(index++, resultId);
			ResultSet resultSet = preparedStmt.executeQuery();
			List<ActionResultText> result = new ArrayList<ActionResultText>();

			while (resultSet.next()) {
				ActionResultText actionResultText = new ActionResultText();
				actionResultText.id = resultSet.getLong("id");
				actionResultText.blockId = resultSet.getLong("block_id");
				actionResultText.content = resultSet.getString("content");
				actionResultText.createdAt = resultSet.getLong("created_at");
				actionResultText.resultId = resultSet.getLong("result_id");
				actionResultText.type = resultSet.getString("type");
				actionResultText.id = resultSet.getLong("id");

				result.add(actionResultText);
			}

			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"getActionResultTextIds failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public List<ActionResultText> getActionResultTextByIds(
			HashSet<Long> messageIds) throws M_QT1V1ResultDBException {
		List<ActionResultText> result = new ArrayList<ActionResultText>();

		PreparedStatement preparedStmt = null;
		try {
			if (messageIds == null || messageIds.isEmpty())
				return result;

			String query = "select * from m_qt1v1_text where id in ";

			List<String> newList = new ArrayList<String>(messageIds.size());
			for (Long key : messageIds) {
				newList.add(String.valueOf(key));
			}

			String keys = String.join(",", newList);

			query = query + " ( " + keys + " )";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				ActionResultText actionResultText = new ActionResultText();
				actionResultText.id = resultSet.getLong("id");
				actionResultText.blockId = resultSet.getLong("block_id");
				actionResultText.content = resultSet.getString("content");
				actionResultText.createdAt = resultSet.getLong("created_at");
				actionResultText.resultId = resultSet.getLong("result_id");
				actionResultText.type = resultSet.getString("type");
				actionResultText.id = resultSet.getLong("id");

				result.add(actionResultText);
			}

			resultSet.close();
			return result;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"getActionResultTextIds failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public List<ActionResultFile> getActionResultFileByIds(
			HashSet<Long> attachementFileIds) throws M_QT1V1ResultDBException {
		List<ActionResultFile> files = new ArrayList<ActionResultFile>();

		PreparedStatement preparedStmt = null;
		try {
			if (attachementFileIds == null || attachementFileIds.isEmpty())
				return files;

			String query = "select * from m_qt1v1_file where id in ";

			List<String> newList = new ArrayList<String>(
					attachementFileIds.size());
			for (Long key : attachementFileIds) {
				newList.add(String.valueOf(key));
			}

			String keys = String.join(",", newList);

			query = query + " ( " + keys + " )";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				ActionResultFile result = new ActionResultFile();
				result.id = resultSet.getLong("id");
				result.createdAt = resultSet.getLong("created_at");
				result.resultId = resultSet.getLong("result_id");
				result.status = resultSet.getInt("status");
				result.host = resultSet.getString("host");
				String path = resultSet.getString("path");
				result.path = path.split("/");
				result.name = resultSet.getString("name");
				result.size = resultSet.getInt("size");
				result.type = resultSet.getInt("type");
				result.dName = resultSet.getString("d_name");
				files.add(result);
			}

			resultSet.close();
			return files;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"getActionResultFileByIds failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public List<ActionResult> getNewResults(int limit, boolean allParams)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		List<ActionResult> results = new ArrayList<>();
		try {

			String sql1 = "SELECT t1.txt_id , t2.f_id FROM ( SELECT MAX(content_id) AS txt_id FROM m_email_result_mapping WHERE content_type = 0 ) t1 , ( SELECT MAX(content_id) AS f_id FROM m_email_result_mapping WHERE content_type = 1 ) t2 ";
			
			long max_tid = 0;
			long max_fid = 0;
			

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(sql1);

			

			resultSet = preparedStmt.executeQuery();
			
			while (resultSet.next()) {
				
				max_tid = resultSet.getLong("txt_id");
				max_fid = resultSet.getLong("f_id");
			}
			preparedStmt.close();
			resultSet.close();
			String sql2 = "select result_id from m_qt1v1_text where id > ? union select result_id from m_qt1v1_file where id > ?";

			preparedStmt = connection.prepareStatement(sql2);
			preparedStmt.setLong(1, max_tid);
			preparedStmt.setLong(2, max_fid);
			resultSet = preparedStmt.executeQuery();
			
			while (resultSet.next()) {
				ActionResult result = new ActionResult();
				result.id = resultSet.getLong("result_id");
//				if (allParams) {
//					result.sourceId = resultSet.getLong("source_id");
//					result.scanId = resultSet.getLong("scan_id");
//
//					result.createdAt = resultSet.getLong("created_at");
//					result.ruleId = resultSet.getLong("rule_id");
//					result.normalizedURL = resultSet.getString("n_url");
//				}
				results.add(result);
			}
			return results;

		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("Could not getNewResults. "
					+ e.getMessage(), e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_QT1V1ResultDBException(e.getMessage());
				}
			}
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public List<String> getActionResultUrlsById(HashSet<Long> resultIds)
			throws M_QT1V1ResultDBException {
		List<String> urls = new ArrayList<>();

		PreparedStatement preparedStmt = null;
		try {
			if (resultIds == null || resultIds.isEmpty())
				return urls;

			String query = "select * from m_qt1v1_result where id in ";

			List<String> newList = new ArrayList<String>(resultIds.size());
			for (Long key : resultIds) {
				newList.add(String.valueOf(key));
			}

			String keys = String.join(",", newList);

			query = query + " ( " + keys + " )";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			ResultSet resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				String url = resultSet.getString("n_url");

				urls.add(url);
			}

			resultSet.close();
			return urls;
		} catch (Exception e) {
			throw new M_QT1V1ResultDBException(
					"getActionResultUrlsById failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void updateValidation(long id, int code, boolean isFile,
			boolean isSystem) throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt = null;
		try {
			if (isSystem)
				if (code >= 100 || code < 10)
					return;

			if (!isSystem)
				if (code >= 10 || code < 0)
					return;

			String updateSql = null;

			if (isFile) {
				if (isSystem) {
					updateSql = "update m_qt1v1_file set validation = ( validation%10 + ? ) where id = ? ";
				} else {
					updateSql = "update m_qt1v1_file set validation = ( validation - validation%10 + ? ) where id = ? ";
				}
			} else {
				if (isSystem) {
					updateSql = "update m_qt1v1_text set validation = ( validation%10 + ? ) where id = ? ";
				} else {
					updateSql = "update m_qt1v1_text set validation = ( validation - validation%10 + ? ) where id = ? ";

				}
			}
			preparedStmt = connection.prepareStatement(updateSql);
			int index = 1;
			preparedStmt.setInt(index++, code);
			preparedStmt.setLong(index++, id);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_QT1V1ResultDBException("updateValidation failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void deleteActionResult(M_WebScraperPage inProcessPage)
			throws M_QT1V1ResultDBException {
		PreparedStatement preparedStmt4ActionResult = null;
		try {
			String deleteResult = "delete from m_qt1v1_result where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt4ActionResult = connection
					.prepareStatement(deleteResult);

			preparedStmt4ActionResult.setLong(1, inProcessPage.actionResult.id);
			preparedStmt4ActionResult.execute();

		} catch (Exception e) {

			throw new M_QT1V1ResultDBException(
					"updateActionResultFile failed: " + e.getMessage(), e);

		} finally {

			if (preparedStmt4ActionResult != null) {
				try {
					preparedStmt4ActionResult.close();
				} catch (SQLException sqlex) {
					throw new M_QT1V1ResultDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt4ActionResult = null;
			}

		}

	}

}
