package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.webdatabase.dao.M_RuleBackendDAO;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webmodel.rule.M_RuleType;
import com.datalyxt.production.webmodel.rule.QT1_V1_RuleContent;
import com.datalyxt.production.webmodel.rule.QT1_V2_RuleContent;
import com.google.gson.Gson;

public class MysqlM_RuleBackendDAOImpl implements M_RuleBackendDAO {

	private Connection connection;

	public MysqlM_RuleBackendDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public List<M_Rule> getRuleIDs(M_RuleStatus ruleStatus,
			HashSet<M_RuleType> ruleTypes) throws M_RuleDBException {
		List<M_Rule> ruleIds = new ArrayList<M_Rule>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select id from m_rules where status = ? ";
			String conditionWithType = "";
			boolean hasCombination = false;
			for (M_RuleType type : ruleTypes) {
				if (!hasCombination) {
					conditionWithType = " '" + type.name() + "' "
							+ conditionWithType;
					hasCombination = true;
				} else
					conditionWithType = " '" + type.name() + "' , "
							+ conditionWithType + " ";

			}

			String typquery = " and type in ( " + conditionWithType + " ) ";
			if (!ruleTypes.isEmpty())
				queryString = queryString + typquery;

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, ruleStatus.name());

			resultSet = preparedStmt.executeQuery();

			while (resultSet.next()) {
				M_Rule rule = new M_Rule();
				rule.id = resultSet.getLong("id");
				ruleIds.add(rule);

			}
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return ruleIds;
	}

	@Override
	public List<M_Rule> getRules(M_RuleStatus ruleStatus,
			HashSet<M_RuleType> ruleTypes) throws M_RuleDBException {
		List<M_Rule> rules = new ArrayList<M_Rule>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select * from m_rules where status = ? ";

			String conditionWithType = "";
			boolean hasCombination = false;
			for (M_RuleType type : ruleTypes) {
				if (!hasCombination) {
					conditionWithType = " '" + type.name() + "' "
							+ conditionWithType;
					hasCombination = true;
				} else
					conditionWithType = " '" + type.name() + "' , "
							+ conditionWithType + " ";

			}

			String typquery = " and type in ( " + conditionWithType + " ) ";
			if (!ruleTypes.isEmpty())
				queryString = queryString + typquery;

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, ruleStatus.name());

			resultSet = preparedStmt.executeQuery();

			Gson gson = new Gson();

			while (resultSet.next()) {
				M_Rule rule = new M_Rule();

				rule.id = resultSet.getLong("id");

				rule.name = resultSet.getString("name");

				rule.ownerId = resultSet.getLong("owner_id");

				rule.feedback = resultSet.getString("feedback");

				rule.createdAt = resultSet.getTimestamp("created_at").getTime();

				rule.ruleType = M_RuleType.valueOf(resultSet.getString("type"));

				rule.status = M_RuleStatus.valueOf(resultSet
						.getString("status"));

				String rule_content = resultSet.getString("content");
				if (rule.ruleType.equals(M_RuleType.QT1V1))
					rule.ruleContent = gson.fromJson(rule_content,
							QT1_V1_RuleContent.class);
				if (rule.ruleType.equals(M_RuleType.QT1V2))
					rule.ruleContent = gson.fromJson(rule_content,
							QT1_V2_RuleContent.class);

				rules.add(rule);
			}
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return rules;
	}


	@Override
	public List<M_Rule> getRules(M_RuleStatus ruleStatus, long sourceId)
			throws M_RuleDBException {
		List<M_Rule> rules = new ArrayList<M_Rule>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select * from m_rules where status = ? and id in ( select rule_id from m_rules_source_mapping where source_id = ? ) ";

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, ruleStatus.name());
			preparedStmt.setLong(2, sourceId);

			resultSet = preparedStmt.executeQuery();

			Gson gson = new Gson();

			while (resultSet.next()) {
				M_Rule rule = new M_Rule();

				rule.id = resultSet.getLong("id");

				rule.name = resultSet.getString("name");

				rule.ownerId = resultSet.getLong("owner_id");

				rule.feedback = resultSet.getString("feedback");

				rule.createdAt = resultSet.getTimestamp("created_at").getTime();

				rule.ruleType = M_RuleType.valueOf(resultSet
						.getString("type"));

				rule.status = M_RuleStatus.valueOf(resultSet
						.getString("status"));

				String rule_content = resultSet.getString("content");
				if (rule.ruleType.equals(M_RuleType.QT1V1))
					rule.ruleContent = gson.fromJson(rule_content,
							QT1_V1_RuleContent.class);
				if (rule.ruleType.equals(M_RuleType.QT1V2))
					rule.ruleContent = gson.fromJson(rule_content,
							QT1_V2_RuleContent.class);

				rules.add(rule);
			}
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return rules;

	}

	@Override
	public M_Rule getRuleByRuleId(Long ruleId, Long ownerId)
			throws M_RuleDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select * from m_rules where id = ? and owner_id = ?  ";

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setLong(1, ruleId);
			preparedStmt.setLong(2, ownerId);

			resultSet = preparedStmt.executeQuery();

			Gson gson = new Gson();

			if (resultSet.next()) {
				M_Rule rule = new M_Rule();

				rule.id = resultSet.getLong("id");

				rule.name = resultSet.getString("name");

				rule.ownerId = resultSet.getLong("owner");

				rule.feedback = resultSet.getString("feedback");

				rule.createdAt = resultSet.getTimestamp("created_at").getTime();

				rule.ruleType = M_RuleType.valueOf(resultSet
						.getString("type"));

				rule.status = M_RuleStatus.valueOf(resultSet
						.getString("status"));

				String rule_content = resultSet.getString("content");
				if (rule.ruleType.equals(M_RuleType.QT1V1))
					rule.ruleContent = gson.fromJson(rule_content,
							QT1_V1_RuleContent.class);
				if (rule.ruleType.equals(M_RuleType.QT1V2))
					rule.ruleContent = gson.fromJson(rule_content,
							QT1_V2_RuleContent.class);

				return rule;
			}
			return null;
		} catch (Exception e) {
			throw new M_RuleDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_RuleDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_RuleDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

}
