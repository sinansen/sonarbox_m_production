package com.datalyxt.production.webdatabase.dao;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.exception.db.EmailDBException;
import com.datalyxt.production.webmodel.email.Email;
import com.datalyxt.production.webmodel.email.EmailAction;

public interface EmailDAO extends DataBaseDAO{
	
	public void createEmail(Email email, HashSet<Long> receiverIds, long ownerId) throws EmailDBException;
	
	public int updateEmailStatus(EmailAction emailAction) throws EmailDBException;
	
	public int incRetry(EmailAction emailAction, long ownerId) throws EmailDBException;
	
	public List<Email> getEmailsByStatus(int status, long ownerId) throws EmailDBException;

	public Email getEmailById(long id, long ownerId) throws EmailDBException;
	
	public HashMap<Long, List<EmailAction>> getEmailActionsByStatus(int status, long ownerId) throws EmailDBException;

	public void saveEmailAction(EmailAction e, long ownerId)throws EmailDBException;

	public void modifyEmail(Email email, long userId)throws EmailDBException;

	public boolean hasSameEmail(Email email, long userId)throws EmailDBException;

	public void removeEmail(long id, long userId)throws EmailDBException;

}
