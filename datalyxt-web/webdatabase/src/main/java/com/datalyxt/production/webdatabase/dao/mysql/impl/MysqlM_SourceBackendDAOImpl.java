package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_SourceDBException;
import com.datalyxt.production.webdatabase.dao.M_SourceBackendDAO;
import com.datalyxt.production.webmodel.source.M_Source;
import com.datalyxt.production.webmodel.source.M_SourceStatus;
import com.datalyxt.production.webmodel.source.SourceConfig;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class MysqlM_SourceBackendDAOImpl implements M_SourceBackendDAO {

	private Connection connection;

	public MysqlM_SourceBackendDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	private List<M_Source> readResultsets(ResultSet resultSet)
			throws JsonSyntaxException, SQLException {
		List<M_Source> sources = new ArrayList<M_Source>();
		Gson gson = new Gson();

		while (resultSet.next()) {
			M_Source source = new M_Source();

			source.sourceId = resultSet.getLong("id");

			source.sourceName = resultSet.getString("name");

			source.sourceUrl = resultSet.getString("url");

			source.sourceDomain = resultSet.getString("domain");

			source.sourceUrlNormalized = resultSet
					.getString("url_normalized");

			source.sourceCreatedAt = resultSet
					.getTimestamp("created_at").getTime();

			source.sourceStatus = M_SourceStatus.valueOf(resultSet
					.getString("status"));

			String config_content = resultSet.getString("config");

			source.sourceConfig = gson.fromJson(config_content,
					SourceConfig.class);

			sources.add(source);
		}
		return sources;

	}

	@Override
	public List<M_Source> getSourcesByStatus(M_SourceStatus status)
			throws M_SourceDBException {
		List<M_Source> sources = new ArrayList<M_Source>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select * from m_source where status = ?";

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setString(1, status.name());

			resultSet = preparedStmt.executeQuery();
			sources = readResultsets(resultSet);

		} catch (Exception e) {
			throw new M_SourceDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_SourceDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_SourceDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return sources;
	}

	@Override
	public List<M_Source> getSourcesForExecutionByRuleId(long ruleId)
			throws M_SourceDBException {
		List<M_Source> sources = new ArrayList<M_Source>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select id, name, url_normalized, config, domain from m_source where id in ( select source_id from m_rules_source_mapping where rule_id = ? )";

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setLong(1, ruleId);

			resultSet = preparedStmt.executeQuery();

			Gson gson = new Gson();

			while (resultSet.next()) {
				M_Source source = new M_Source();

				source.sourceId = resultSet.getLong("id");
				source.sourceDomain = resultSet.getString("domain");

				source.sourceUrlNormalized = resultSet
						.getString("url_normalized");

				String config_content = resultSet.getString("config");

				source.sourceConfig = gson.fromJson(config_content,
						SourceConfig.class);

				sources.add(source);
			}

		} catch (Exception e) {
			throw new M_SourceDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_SourceDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_SourceDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return sources;
	}

	@Override
	public M_Source getSourceById(long sourceId) throws M_SourceDBException {
		List<M_Source> sources = new ArrayList<M_Source>();

		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;

		try {
			String queryString = "select * from m_source where id  = ? ";

			preparedStmt = connection.prepareStatement(queryString);

			preparedStmt.setLong(1, sourceId);

			resultSet = preparedStmt.executeQuery();
			sources = readResultsets(resultSet);
		} catch (Exception e) {
			throw new M_SourceDBException(e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new M_SourceDBException(e.getMessage(), e);
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_SourceDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
		if (sources.isEmpty())
			return null;
		else
			return sources.get(0);
	}

}
