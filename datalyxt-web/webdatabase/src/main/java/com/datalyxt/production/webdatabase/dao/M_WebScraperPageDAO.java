package com.datalyxt.production.webdatabase.dao;

import java.util.List;

import com.datalyxt.production.exception.db.M_WebScraperPageDBException;
import com.datalyxt.production.execution.PageContentProcessStatus;
import com.datalyxt.production.webscraper.model.M_WebScraperPage;

public interface M_WebScraperPageDAO extends DataBaseDAO {

	public boolean isAllPagesVisited(long fetchFromDBTime,
			PageContentProcessStatus visited)
			throws M_WebScraperPageDBException;

	public long getLatestVisitedAt(PageContentProcessStatus visited,
			long fetchFromDBTime) throws M_WebScraperPageDBException;

	public void putWebScraperPage(M_WebScraperPage page)
			throws M_WebScraperPageDBException;

	public void updateWebScraperPageStatus(long fetchFromDBTime, long pageId,
			PageContentProcessStatus status, long visitedAt)
			throws M_WebScraperPageDBException;

	public void updateWebScraperPageContent(M_WebScraperPage inProcessPage)
			throws M_WebScraperPageDBException;

	public void putWebScraperPages(List<M_WebScraperPage> nextPages)
			throws M_WebScraperPageDBException;

	public List<M_WebScraperPage> getNextPageFIFO(long fetchFromDBTime, int counter)
			throws M_WebScraperPageDBException;

	public List<M_WebScraperPage> getInProcessingPagesTime(long fetchFromDBTime, List<String> inProcessingStatus)
			throws M_WebScraperPageDBException;

	public void deleteWebScraperPages(List<M_WebScraperPage> pages)
			throws M_WebScraperPageDBException;

}
