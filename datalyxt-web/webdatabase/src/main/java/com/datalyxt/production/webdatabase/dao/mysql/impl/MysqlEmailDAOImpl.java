package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.exception.db.EmailDBException;
import com.datalyxt.production.webdatabase.dao.EmailDAO;
import com.datalyxt.production.webmodel.email.Email;
import com.datalyxt.production.webmodel.email.EmailAction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MysqlEmailDAOImpl implements EmailDAO {

	Connection connection;

	public MysqlEmailDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public int incRetry(EmailAction emailAction, long ownerId)
			throws EmailDBException {
		PreparedStatement preparedStmt = null;
		try {
			long now = System.currentTimeMillis();

			// the mysql insert statement
			String query = " UPDATE m_email_receiver set numRetries=numRetries+1, time = ? WHERE email_id = ? and receiver_id = ? ";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			int index = 1;

			preparedStmt.setTimestamp(index++, new Timestamp(now));
			preparedStmt.setLong(index++, emailAction.emailId);
			preparedStmt.setLong(index++, emailAction.receiverId);
			// execute the preparedstatement
			int execute = preparedStmt.executeUpdate();

			return execute;

		} catch (SQLException e) {
			throw new EmailDBException("Could not incRetry: " + e.getMessage(),
					e);
		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}
	}

	private List<Email> writeResultSet(ResultSet resultSet) throws SQLException {
		List<Email> returnList = new ArrayList<>();

		while (resultSet.next()) {

			Email email = new Email();
			email.id = resultSet.getLong("id");
			email.subject = resultSet.getString("subject");

			Gson gson = new Gson();

			Type listType = new TypeToken<ArrayList<Long>>() {
			}.getType();

			String message = resultSet.getString("message");
			email.messageIds.addAll(gson.fromJson(message, listType));

			String attach = resultSet.getString("attachment");
			email.attachementFileIds.addAll(gson.fromJson(attach, listType));

			email.lastUpdateDate = resultSet.getLong("lastupdated");
			email.created = resultSet.getLong("created");

			returnList.add(email);
		}
		return returnList;

	}

	@Override
	public void createEmail(Email email, HashSet<Long> receiverIds, long ownerId) throws EmailDBException {
		PreparedStatement preparedStmt = null;
		try {

			long now = System.currentTimeMillis();
			
			List<Long> mIds = new ArrayList<>();
			mIds.addAll(email.messageIds);
			Collections.sort(mIds);
			List<Long> atIds = new ArrayList<>();
			atIds.addAll(email.attachementFileIds);
			Collections.sort(atIds);
			String msg_md5 = DigestUtils.md5Hex(mIds.toString());
			String att_md5 = DigestUtils.md5Hex(atIds.toString());

			// the mysql insert statement
			String queryString = "insert into m_email_pool ( created, subject, message, attachment, lastupdated, msg_md5, att_md5, owner_id) values (?, ? , ?, ?, ?, ?, ?, ?);";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString,
					Statement.RETURN_GENERATED_KEYS);
			int index = 1;
			preparedStmt.setLong(index++, now);
			preparedStmt.setString(index++, email.subject);
			Gson gson = new Gson();
			String msg = gson.toJson(email.messageIds);
			preparedStmt.setString(index++, msg);
			String attch = gson.toJson(email.attachementFileIds);
			preparedStmt.setString(index++, attch);
			preparedStmt.setLong(index++, now);
			preparedStmt.setString(index++, msg_md5);
			preparedStmt.setString(index++, att_md5);
			preparedStmt.setLong(index++, ownerId);
			// execute the preparedstatement
			preparedStmt.execute();
			
			

			ResultSet rs = preparedStmt.getGeneratedKeys();
			if (rs.next()) {
				email.id = rs.getLong(1);
			}
			
			
			
			String map_sql = "INSERT INTO m_email_result_mapping (email_id, content_id, content_type, owner_id)"
					+ " VALUES (?, ?, ?, ?)";
			connection.setAutoCommit(false);
			PreparedStatement preparedStmt1;
			if (!email.messageIds.isEmpty()) {
				preparedStmt1 = connection.prepareStatement(map_sql);
				
				for (Long contentId : email.messageIds) {
					index = 1;
					preparedStmt1.setLong(index++, email.id);
					preparedStmt1.setLong(index++, contentId);
					preparedStmt1.setInt(index++, 0);
					preparedStmt1.setLong(index++, 1);
					preparedStmt1.execute();
				}

			}
			if (!email.attachementFileIds.isEmpty()) {
				preparedStmt1 = connection.prepareStatement(map_sql);

				for (Long contentId : email.attachementFileIds) {
					index = 1;
					preparedStmt1.setLong(index++, email.id);
					preparedStmt1.setLong(index++, contentId);
					preparedStmt1.setInt(index++, 1);
					preparedStmt1.setLong(index++, 1);
					preparedStmt1.execute();
				}
			}
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			throw new EmailDBException("Could not insert Email. "
					+ e.getMessage(), e);
		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public int updateEmailStatus(EmailAction emailAction)
			throws EmailDBException {
		PreparedStatement preparedStmt = null;
		try {
			long now = System.currentTimeMillis();

			// the mysql insert statement
			String query = " UPDATE m_email_receiver SET status=?, time = ? WHERE email_id = ? and receiver_id = ? ";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			int index = 1;

			preparedStmt.setInt(index++, emailAction.status);
			preparedStmt.setTimestamp(index++, new Timestamp(now));
			preparedStmt.setLong(index++, emailAction.emailId);
			preparedStmt.setLong(index++, emailAction.receiverId);

			// execute the preparedstatement
			int execute = preparedStmt.executeUpdate();

			return execute;

		} catch (SQLException e) {
			throw new EmailDBException("Could not update Email status. "
					+ e.getMessage(), e);
		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public List<Email> getEmailsByStatus(int status, long ownerId)
			throws EmailDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {

			String query = " SELECT *  from m_email_pool WHERE id in ( select distinct(email_id) from m_email_receiver where status = ? ) ";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			preparedStmt.setInt(1, status);

			resultSet = preparedStmt.executeQuery();

			List<Email> writeResultSet = writeResultSet(resultSet);
			return writeResultSet;

		} catch (Exception e) {
			throw new EmailDBException("Could not get Emails By Status."
					+ e.getMessage(), e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new EmailDBException(e.getMessage());
				}
			}
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public Email getEmailById(long id, long ownerId) throws EmailDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {
			String query = " SELECT *  from m_email_pool WHERE id = ? ";

			preparedStmt = connection.prepareStatement(query);
			preparedStmt.setLong(1, id);
			resultSet = preparedStmt.executeQuery();
			List<Email> emails = writeResultSet(resultSet);

			if (emails != null && !emails.isEmpty())
				return emails.get(0);
			return null;

		} catch (Exception e) {
			throw new EmailDBException("Email not found with id: " + id + " "
					+ e.getMessage(), e);
		} finally {

			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new EmailDBException(e.getMessage());
				}
			}

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}
		}

	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public HashMap<Long, List<EmailAction>> getEmailActionsByStatus(int status,
			long ownerId) throws EmailDBException {
		PreparedStatement preparedStmt = null;
		ResultSet resultSet = null;
		try {

			String query = " select * from m_email_receiver where status = ?";

			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(query);

			preparedStmt.setInt(1, status);

			resultSet = preparedStmt.executeQuery();
			HashMap<Long, List<EmailAction>> result = new HashMap<Long, List<EmailAction>>();
			while (resultSet.next()) {
				EmailAction emailAction = new EmailAction();
				emailAction.emailId = resultSet.getLong("email_id");
				emailAction.receiverId = resultSet.getLong("receiver_id");
				emailAction.numretries = resultSet.getInt("numretries");
				emailAction.status = resultSet.getInt("status");
				emailAction.time = resultSet.getTimestamp("time").getTime();

				List<EmailAction> emailActions = result
						.get(emailAction.emailId);
				if (emailActions == null)
					emailActions = new ArrayList<>();
				emailActions.add(emailAction);
				result.put(emailAction.emailId, emailActions);

			}
			return result;

		} catch (Exception e) {
			throw new EmailDBException("Could not get EmailActions By Status. "
					+ e.getMessage(), e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					throw new EmailDBException(e.getMessage());
				}
			}
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void saveEmailAction(EmailAction e, long ownerId)
			throws EmailDBException {
		PreparedStatement preparedStmt = null;
		try {

			// the mysql insert statement
			String queryString = "insert into m_email_receiver ( email_id, receiver_id, time, status) values (?, ?, ?, ?);";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, e.emailId);
			preparedStmt.setLong(index++, e.receiverId);
			preparedStmt.setTimestamp(index++, new Timestamp(e.time));
			preparedStmt.setInt(index++, e.status);
			// execute the preparedstatement
			preparedStmt.execute();

		} catch (SQLException ex) {
			throw new EmailDBException("Could not saveEmailAction . "
					+ ex.getMessage(), ex);
		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new EmailDBException(sqlex.getMessage());
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void modifyEmail(Email email, long userId) throws EmailDBException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean hasSameEmail(Email email, long userId)
			throws EmailDBException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void removeEmail(long id, long userId) throws EmailDBException {
		// TODO Auto-generated method stub
		
	}

}
