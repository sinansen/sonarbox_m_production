package com.datalyxt.production.webdatabase.factory;

import com.datalyxt.exception.db.DAOFactoryException;
import com.datalyxt.production.webdatabase.dao.M_BlockFilterDAO;

public interface RuntimeDAOFactory {
	public void closeConnection() throws DAOFactoryException;

	public M_BlockFilterDAO getM_BlockFilterDAO() throws DAOFactoryException;

}
