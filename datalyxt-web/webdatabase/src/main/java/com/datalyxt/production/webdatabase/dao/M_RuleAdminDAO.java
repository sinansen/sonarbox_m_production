package com.datalyxt.production.webdatabase.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import com.datalyxt.production.exception.db.M_RuleDBException;
import com.datalyxt.production.webmodel.rule.M_Rule;
import com.datalyxt.production.webmodel.rule.M_RuleStatus;
import com.datalyxt.production.webscraper.model.config.BlockConfigMain;

public interface M_RuleAdminDAO extends DataBaseDAO {

	public long addRule(M_Rule rule) throws M_RuleDBException;

	public void changeRuleStatus(long ruleId, M_RuleStatus status, long lastModified)
			throws M_RuleDBException;


	public List<M_Rule> getRules(String sql, Object[] params, long ownerId)
			throws M_RuleDBException;

	public boolean isRuleExsits(M_Rule rule) throws M_RuleDBException;

	public M_Rule getRuleByRuleId(Long ruleId, Long ownerId)
			throws M_RuleDBException;

	public int modifyRule(String sql, Object[] params) throws M_RuleDBException;

	public int[] modifyRuleSourceMapping(M_Rule rule) throws M_RuleDBException;

	public int[] deleteRuleSourceMapping(Collection<Long> tobeDeletedSourceIds,
			long userId, long ruleId) throws M_RuleDBException;

	public void updateSelector(long ruleId, BlockConfigMain cfg, long ownerId) throws M_RuleDBException;

	public HashMap<Long, M_Rule> getRules(HashSet<Long> rids)  throws M_RuleDBException;

}
