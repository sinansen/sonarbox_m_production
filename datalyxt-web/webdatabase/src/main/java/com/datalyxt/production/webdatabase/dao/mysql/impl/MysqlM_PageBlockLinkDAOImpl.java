package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_PageBlockLinkDBException;
import com.datalyxt.production.webdatabase.dao.M_PageBlockLinkDAO;
import com.datalyxt.production.webscraper.model.runtime.LinkBlock;
import com.datalyxt.webscraper.model.link.Link;

public class MysqlM_PageBlockLinkDAOImpl implements M_PageBlockLinkDAO {

	private Connection connection;

	public MysqlM_PageBlockLinkDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public boolean contains(Link link) throws M_PageBlockLinkDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select link from m_pageblock_link where link_md5 = ? and ( status = -1 or status = 1) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			String md5 = DigestUtils.md5Hex(link.url);
			preparedStmt.setString(index++, md5);
			ResultSet resultSet = preparedStmt.executeQuery();
			boolean contained = false;
			while (resultSet.next()) {
				String url = resultSet.getString("link");
				if (url.equals(link.url)) {
					contained = true;
					break;
				}
			}

			resultSet.close();
			return contained;
		} catch (Exception e) {
			throw new M_PageBlockLinkDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageBlockLinkDBException(sqlex.getMessage(),
							sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void putLinkBlocks(HashMap<String, LinkBlock> linkBlocks,
			long blockId) throws M_PageBlockLinkDBException {
		PreparedStatement preparedStmt = null;

		int counter = 0;

		try {
			String queryString = "insert into m_pageblock_link (link_md5, link, block_id, created_at ) values (?, ?, ?, ? ) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			String errorMsg = "";

			long now = System.currentTimeMillis();

			for (String url : linkBlocks.keySet()) {
				try {
					String hash = DigestUtils.md5Hex(url);

					preparedStmt.setString(1, hash);
					preparedStmt.setString(2, url);
					preparedStmt.setLong(3, blockId);
					preparedStmt.setTimestamp(4, new Timestamp(now));

					preparedStmt.executeUpdate();

					counter = counter + 1;

				} catch (Exception e) {
					errorMsg = errorMsg + " \n insert link error: url " + url
							+ " " + e.getMessage();
				}
			}

		} catch (Exception e) {
			throw new M_PageBlockLinkDBException("putLinks: " + e.getMessage(),
					e);

		} finally {
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageBlockLinkDBException("putLinks: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void updateLinkStatus(String url, boolean success)
			throws M_PageBlockLinkDBException {
		PreparedStatement preparedStmt = null;

		try {
			String queryString = "update m_pageblock_link set status = ? where  link_md5 = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			String hash = DigestUtils.md5Hex(url);
			int status = 0;
			if (success)
				status = 1;
			preparedStmt.setInt(1, status);
			preparedStmt.setString(2, hash);

			preparedStmt.executeUpdate();

		} catch (Exception e) {
			throw new M_PageBlockLinkDBException("updateLinkStatus: "
					+ e.getMessage(), e);

		} finally {
			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageBlockLinkDBException("updateLinkStatus: "
							+ sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

}
