package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_ReceiverDBException;
import com.datalyxt.production.exception.db.M_ReceiverGroupDBException;
import com.datalyxt.production.receiver.M_Receiver;
import com.datalyxt.production.receiver.M_ReceiverGroup;
import com.datalyxt.production.receiver.M_ReceiverGroupStatus;
import com.datalyxt.production.receiver.M_ReceiverStatus;
import com.datalyxt.production.webdatabase.dao.M_ReceiverDAO;

public class MysqlM_ReceiverDAOImpl implements M_ReceiverDAO {

	private static Logger logger = LoggerFactory
			.getLogger(MysqlM_ReceiverDAOImpl.class);

	private Connection connection;

	public MysqlM_ReceiverDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public long insertReceiver(M_Receiver receiver)
			throws M_ReceiverDBException {
		return -1;
	}

	@Override
	public void modifyReceiver(String sql, Object[] params)
			throws M_ReceiverDBException {

	}

	@Override
	public M_Receiver getReceiverByEmail(String email)
			throws M_ReceiverDBException {

		return null;

	}

	@Override
	public boolean isReceiverExist(Long receiverId)
			throws M_ReceiverDBException {
		return false;
	}

	@Override
	public List<M_Receiver> getReceiver(String sql, Object[] params)
			throws M_ReceiverDBException {
		List<M_Receiver> receivers = new ArrayList<M_Receiver>();

		return receivers;
	}

	@Override
	public M_Receiver getReceiverById(long receiverId, long ownerId)
			throws M_ReceiverDBException {

		PreparedStatement preparedStmt = null;
		try {
			String queryString = "select  e_mail from m_receiver where id = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, receiverId);
			ResultSet result = preparedStmt.executeQuery();
			M_Receiver receiver = null;
			if (result.next()) {
				receiver = new M_Receiver();
				receiver.email = result.getString("e_mail");
				receiver.id = receiverId;
			}
			result.close();

			return receiver;
		} catch (Exception e) {
			throw new M_ReceiverDBException("getReceiverById failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_ReceiverDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public boolean isGroupNameExist(String groupName, long ownerId)
			throws M_ReceiverGroupDBException {
		return false;

	}

	@Override
	public long insertReceiverGroup(M_ReceiverGroup receiverGroup)
			throws M_ReceiverGroupDBException {
		return 0;
	}

	@Override
	public void modifyReceiverGroup(String sql, Object[] params)
			throws M_ReceiverGroupDBException {

	}

	@Override
	public M_ReceiverGroup getReceiverGroupById(long groupId, long ownerId)
			throws M_ReceiverGroupDBException {

		return null;
	}

	@Override
	public List<M_ReceiverGroup> getReceiverGroups(String sql, Object[] params)
			throws M_ReceiverGroupDBException {
		List<M_ReceiverGroup> groups = new ArrayList<M_ReceiverGroup>();

		return groups;
	}

	@Override
	public int[] removeReceiverFromGroup(long groupId, HashSet<Long> groupMember, long ownerId)
			throws M_ReceiverGroupDBException {
		return null;

	}

	@Override
	public int[] addReceiverToGroup(long groupId, HashSet<Long> groupMember, long ownerId)
			throws M_ReceiverGroupDBException {
		return null;

	}

	@Override
	public List<M_Receiver> getReceiverByGroupId(long groupId)
			throws M_ReceiverGroupDBException {

		List<M_Receiver> receivers = new ArrayList<M_Receiver>();

		return receivers;
	}

	@Override
	public List<M_Receiver> getReciverByStatus(M_ReceiverStatus activated)
			throws M_ReceiverDBException {
		List<M_Receiver> receivers = new ArrayList<>();
		PreparedStatement preparedStmt = null;
		try {
			String queryString = "select id, e_mail from m_receiver where status = ?  ;";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);

			int index = 1;
			preparedStmt.setString(index++, activated.name());

			ResultSet result = preparedStmt.executeQuery();
			while (result.next()) {
				M_Receiver receiver = new M_Receiver();
				receiver.email = result.getString("e_mail");
				receiver.id = result.getLong("id");
				receivers.add(receiver);
			}
			result.close();
		} catch (Exception e) {
			throw new M_ReceiverDBException("getReciverByStatus failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_ReceiverDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		return receivers;
	}

	@Override
	public void removeReceiverFromAllGroup(long receiverId, long userId)
			throws M_ReceiverDBException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isGroupOwner(long groupId, long ownerId)
			throws M_ReceiverDBException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int removeGroup(long groupId, long ownerId)
			throws M_ReceiverGroupDBException {
		return 0;

	}

	@Override
	public void changeReceiverStatus(long id, M_ReceiverStatus status,
			long ownerId) throws M_ReceiverDBException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeGroupStatus(long id, M_ReceiverGroupStatus status,
			long ownerId) throws M_ReceiverGroupDBException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeReceiver(long receiverId, long userId)
			throws M_ReceiverDBException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[] addReceiverToGroup(HashSet<Long> groupId, long groupMember,
			long ownerId) throws M_ReceiverGroupDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<M_ReceiverGroup> getReceiverGroups(long receiverId)
			throws M_ReceiverGroupDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<Long, M_ReceiverGroup> getReceiverGroups(
			HashSet<Long> groupIds) throws M_ReceiverGroupDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] removeReceiverFromGroup(HashSet<Long> groupIds,
			Long groupMember, long ownerId) throws M_ReceiverGroupDBException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashSet<Long> getReceiverByGroupIds(HashSet<Long> groupIds, long ownerId)
			throws M_ReceiverGroupDBException {
		// TODO Auto-generated method stub
		return null;
	}

}