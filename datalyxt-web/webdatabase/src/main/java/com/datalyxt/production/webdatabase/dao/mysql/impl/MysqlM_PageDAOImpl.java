package com.datalyxt.production.webdatabase.dao.mysql.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.datalyxt.exception.db.DataBaseException;
import com.datalyxt.production.exception.db.M_PageDBException;
import com.datalyxt.production.webdatabase.dao.M_PageDAO;
import com.datalyxt.production.webscraper.model.runtime.PageChange;
import com.datalyxt.production.webscraper.model.runtime.PageMeta;

public class MysqlM_PageDAOImpl implements M_PageDAO {

	private Connection connection;

	public MysqlM_PageDAOImpl(Connection connection) {
		this.connection = connection;
	}

	@Override
	public synchronized boolean isConnectionClosed() throws DataBaseException {
		try {
			if (connection == null || connection.isClosed())
				return true;
		} catch (SQLException e) {
			throw new DataBaseException(e);
		}
		return false;
	}

	@Override
	public void savePageMeta(PageMeta pageMeta) throws M_PageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_page_meta (n_url_md5, dom_tree, html, timestamp, cleaned_dom_hash, n_url, scan_id) values (?, COMPRESS(?) , COMPRESS(?) ,? , ? , ? , ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setString(index++, pageMeta.nUrlMd5);
			preparedStmt.setString(index++, pageMeta.domTree);
			preparedStmt.setString(index++, pageMeta.html);
			preparedStmt.setLong(index++, pageMeta.timestamp);
			preparedStmt.setLong(index++, pageMeta.cleanedDomHash);
			preparedStmt.setString(index++, pageMeta.nUrl);
			preparedStmt.setLong(index++, pageMeta.scanId);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_PageDBException("savePageMeta failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public void savePageChange(PageChange pageChange) throws M_PageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_page_change (n_url_md5, message, confirmed, timestamp) values (?, ? , ? ,?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setString(index++, pageChange.nUrlMd5);
			preparedStmt.setString(index++, pageChange.message);
			preparedStmt.setInt(index++, pageChange.confirmed);
			preparedStmt.setLong(index++, pageChange.timestamp);

			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_PageDBException("savePageChange failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

	}

	@Override
	public boolean isExsit(PageMeta pageMeta) throws M_PageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "select id from m_page_meta where cleaned_dom_hash = ? and n_url_md5 = ? ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setLong(index++, pageMeta.cleanedDomHash);
			preparedStmt.setString(index++, pageMeta.nUrlMd5);
			ResultSet resultSet = preparedStmt.executeQuery();
			boolean contained = false;
			if (resultSet.next()) {
				contained = true;
			}

			resultSet.close();
			return contained;
		} catch (Exception e) {
			throw new M_PageDBException(e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}
	}

	@Override
	public void saveErrorPageMeta(PageMeta pageMeta) throws M_PageDBException {
		PreparedStatement preparedStmt = null;
		try {

			String queryString = "insert into m_page_meta_error (n_url_md5, dom_tree, html, timestamp, cleaned_dom_hash, n_url, scan_id) values (?, COMPRESS(?) , COMPRESS(?) ,? , ? , ? , ?) ";
			// create the mysql insert preparedstatement
			preparedStmt = connection.prepareStatement(queryString);
			int index = 1;
			preparedStmt.setString(index++, pageMeta.nUrlMd5);
			preparedStmt.setString(index++, pageMeta.domTree);
			preparedStmt.setString(index++, pageMeta.html);
			preparedStmt.setLong(index++, pageMeta.timestamp);
			preparedStmt.setLong(index++, pageMeta.cleanedDomHash);
			preparedStmt.setString(index++, pageMeta.nUrl);
			preparedStmt.setLong(index++, pageMeta.scanId);
			preparedStmt.execute();

		} catch (Exception e) {
			throw new M_PageDBException("saveErrorPageMeta failed: "
					+ e.getMessage(), e);

		} finally {

			if (preparedStmt != null) {
				try {
					preparedStmt.close();
				} catch (SQLException sqlex) {
					throw new M_PageDBException(sqlex.getMessage(), sqlex);
				}

				preparedStmt = null;
			}

		}

		
	}

}
