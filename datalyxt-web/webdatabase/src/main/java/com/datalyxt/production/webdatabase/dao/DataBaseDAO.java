package com.datalyxt.production.webdatabase.dao;

import com.datalyxt.exception.db.DataBaseException;

public interface DataBaseDAO {
	public boolean isConnectionClosed() throws DataBaseException;
}
